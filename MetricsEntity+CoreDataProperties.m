//
//  MetricsEntity+CoreDataProperties.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2020/1/5.
//  Copyright © 2020 jesse. All rights reserved.
//
//

#import "MetricsEntity+CoreDataProperties.h"

@implementation MetricsEntity (CoreDataProperties)

+ (NSFetchRequest<MetricsEntity *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"MetricsEntity"];
}

@dynamic duration;
@dynamic endDate;
@dynamic obser;
@dynamic redirectCount;
@dynamic startDate;
@dynamic urlStr;

@end
