//
//  MetricsEntity+CoreDataProperties.h
//  Bet365
//
//  Created by 蛤蟆吉 on 2020/1/5.
//  Copyright © 2020 jesse. All rights reserved.
//
//

#import "MetricsEntity+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MetricsEntity (CoreDataProperties)

+ (NSFetchRequest<MetricsEntity *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *duration;
@property (nullable, nonatomic, copy) NSNumber *endDate;
@property (nullable, nonatomic, copy) NSString *obser;
@property (nullable, nonatomic, copy) NSNumber *redirectCount;
@property (nullable, nonatomic, copy) NSNumber *startDate;
@property (nullable, nonatomic, copy) NSString *urlStr;

@end

NS_ASSUME_NONNULL_END
