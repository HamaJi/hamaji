//
//  ImmediateModel.h
//  Bet365
//
//  Created by luke on 2019/10/21.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImmediateModel : NSObject
@property (nonatomic,strong) NSNumber *acceptRemoveFlag;
@property (nonatomic,strong) NSString *addTime;
@property (nonatomic,strong) NSString *messageContent;
@property (nonatomic,strong) NSString *immediateFlag;
@property (nonatomic,strong) NSNumber *readStatus;
@property (nonatomic,strong) NSString *messageTitle;
@property (nonatomic,strong) NSString *operatorAccount;
@property (nonatomic,strong) NSString *userAccount;
@property (nonatomic,strong) NSNumber *sendRemoveFlag;
@property (nonatomic,strong) NSNumber *userId;
@end

NS_ASSUME_NONNULL_END
