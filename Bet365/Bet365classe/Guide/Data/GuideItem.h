//
//  GuideItem.h
//  Bet365
//
//  Created by HHH on 2018/10/20.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GuideItem : NSObject

@property (nonatomic,strong)UIView *toView;

@property (nonatomic,assign)CGPoint fromPoint;

@property (nonatomic,assign)CGPoint toAnchorPoint;

+(instancetype)creatGuideItem:(UIView*)fromeView
                       ToView:(UIView *)toView
                ToAnchorPoint:(CGPoint)toAnchorPoint;
@end
