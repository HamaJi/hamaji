//
//  GuideItem.m
//  Bet365
//
//  Created by HHH on 2018/10/20.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "GuideItem.h"

@implementation GuideItem
+(instancetype)creatGuideItem:(UIView*)fromeView ToView:(UIView *)toView ToAnchorPoint:(CGPoint)toAnchorPoint{
    
    GuideItem *item = [[GuideItem alloc] init];
    
    item.toView = toView;
    
    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
    item.fromPoint = [fromeView convertRect: fromeView.bounds toView:window].origin;

    item.toAnchorPoint = toAnchorPoint;
    
    return item;
}
@end
