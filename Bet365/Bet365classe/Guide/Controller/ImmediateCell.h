//
//  ImmediateCell.h
//  Bet365
//
//  Created by luke on 2019/10/21.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ImmediateModel;

NS_ASSUME_NONNULL_BEGIN

@interface ImmediateCell : UITableViewCell
@property (nonatomic,strong) ImmediateModel *model;
@end

NS_ASSUME_NONNULL_END
