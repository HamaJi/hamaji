//
//  NoteViewController.h
//  Bet365
//
//  Created by luke on 2018/10/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, NoteViewType) {
    /** 首页弹窗**/
    NoteViewType_Index = 0,
    /** 登录弹窗**/
    NoteViewType_Login,
    /** 注册弹窗*/
    NoteViewType_Register,
};

@protocol NoteViewControllerDelegate <NSObject>

- (void)notePushViewControllerWithHref:(NSString *)href;

@end

@interface NoteViewController : Bet365ViewController
/**  */
@property (nonatomic,assign) NoteViewType type;
/**  */
@property (nonatomic,weak) id<NoteViewControllerDelegate>delegate;

@end
