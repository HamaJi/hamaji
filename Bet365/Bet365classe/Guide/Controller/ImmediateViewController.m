//
//  ImmediateViewController.m
//  Bet365
//
//  Created by luke on 2019/10/21.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "ImmediateViewController.h"
#import "ImmediateCell.h"
#import "ImmediateModel.h"
#import "PresentingAlphaAnimator.h"
#import "DismissingAlphaAnimator.h"

@interface ImmediateViewController ()
<UIViewControllerTransitioningDelegate,
UITableViewDelegate,
UITableViewDataSource>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableLayoutHeight;
@property (weak, nonatomic) IBOutlet UIButton *btn;

@end

RouterKey *const kRouterImmediate = @"RouterImmediate";

@implementation ImmediateViewController

+ (void)load
{
    [UIViewController registerJumpRouterKey:kRouterImmediate toHandle:^(NSDictionary *parameters) {
        if (USER_DATA_MANAGER.Immediatedatas.count > 0) {
            if ([[NAVI_MANAGER getCurrentVC].presentationController.presentedViewController isKindOfClass:ImmediateViewController.class]) {
                ImmediateViewController *vc = (ImmediateViewController *)[NAVI_MANAGER getCurrentVC].presentationController.presentedViewController;
                vc.datas = USER_DATA_MANAGER.Immediatedatas;
                [vc.tableView reloadData];
            }else{
                ImmediateViewController *vc = [[ImmediateViewController alloc] initWithNibName:@"ImmediateViewController" bundle:nil];
                vc.transitioningDelegate = vc;
                vc.datas = USER_DATA_MANAGER.Immediatedatas;
                vc.modalPresentationStyle = UIModalPresentationCustom;
                [[NAVI_MANAGER getCurrentVC] presentViewController:vc animated:YES completion:nil];
            }
        }
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerNib:[UINib nibWithNibName:@"ImmediateCell" bundle:nil] forCellReuseIdentifier:@"ImmediateCell"];
    [self.tableView reloadData];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.btn borderForColor:JesseGrayColor(200) borderWidth:0.5 borderType:UIBorderSideTypeTop];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.datas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ImmediateCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ImmediateCell"];
    cell.model = self.datas[indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        dispatch_async(dispatch_get_main_queue(), ^{
            __block CGFloat tableHeight = 0;
            [tableView.visibleCells enumerateObjectsUsingBlock:^(__kindof UITableViewCell * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                tableHeight += obj.frame.size.height;
            }];
            if (tableHeight > HEIGHT - 80 - 40) {
                tableHeight = HEIGHT - 80 - 40;
            }
            self.tableLayoutHeight.constant = tableHeight;
        });
    }
}

- (IBAction)dismissRemove:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIViewControllerTransitioningDelegate
-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    return [DismissingAlphaAnimator new];
}
-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    return [PresentingAlphaAnimator new];
}

@end
