//
//  BouncedViewController.m
//  Bet365
//
//  Created by jesse on 2018/12/30.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "BouncedViewController.h"
#import "NetWorkMannager+Account.h"
@interface BouncedViewController ()
@property (weak, nonatomic) IBOutlet UITextField *scan_Filed;
@property (weak, nonatomic) IBOutlet UIButton *code_Button;
@property (strong,nonatomic) UIImage *codeImage;//验证码
@property (weak, nonatomic) IBOutlet UIButton *sure_Button;
@end

@implementation BouncedViewController

/**生命周期**/
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //replaceLayer
    [self addObserve];//添加观察者
    [self setClolorForGlobal];
    [self getVCodeNetTask];//刷新验证码
}
- (IBAction)cancal_sel:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)sure_Sel:(UIButton *)sender {
    [self checkCodeLength];
}
- (IBAction)code_sel:(id)sender {
    [self getVCodeNetTask];
}
-(void)checkCodeLength{
    NSString *code = self.scan_Filed.text;
    if(code.length>0){
        
        [self dismissViewControllerAnimated:YES completion:^{
            [self.delegate surePushControllerComplement:code gameType:self.gameType liveC:self.liveCode GameKind:self.gameKind];
        }];
    }else{
        [SVProgressHUD showErrorWithStatus:@"验证码为空"];
    }
}
-(void)addObserve{
    @weakify(self);
    RACSignal *signal = [RACObserve(self, codeImage) distinctUntilChanged];
    [[RACSignal combineLatest:@[signal] reduce:^id _Nonnull(UIImage *strCode){
        return @((strCode!=nil));
    }]subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        BOOL value = [x boolValue];
        if (value) {
            [self.code_Button setBackgroundImage:self.codeImage forState:UIControlStateNormal];
        }
    }];
}
/**获取验证码**/
-(void)getVCodeNetTask{
    @weakify(self);
    [NET_DATA_MANAGER requestGetVCodeByRandom:YES Success:^(id responseObject) {
        @strongify(self);
        if (responseObject) {
            if([responseObject isKindOfClass:[UIImage class]]){
                self.codeImage = (UIImage *)responseObject;
            }
        }
    } failure:^(NSError *error) {
    }];
}
/**代理**/
/**懒加载**/
-(void)setClolorForGlobal{
    [self.sure_Button setBackgroundColor:JesseColor(255, 0, 4)];
    [self.scan_Filed setTintColor:[UIColor blackColor]];
}
@end
