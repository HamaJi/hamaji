//
//  GuideViewController.m
//  Bet365
//
//  Created by HHH on 2018/10/20.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "GuideViewController.h"

@interface GuideViewController ()

@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (nonatomic,assign)CGPoint position;

@property (nonatomic,strong)NSMutableArray <NSMutableArray <NSString *>*>*imgList;

@property (nonatomic)UIView *fromView;
@end

@implementation GuideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    dispatch_async(dispatch_get_main_queue(), ^{
        @weakify(self);
        [[RACObserve(self, index) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
            if ([x integerValue]) {
                @strongify(self);
                [self.contentView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    [obj removeFromSuperview];
                }];
                if (self.handle) {
                    self.fromView = self.handle(self.index);
                }
                [self drawGuideView];
            }
        }];
    });
    self.contentView.backgroundColor = COLOR_WITH_HEX_ALP(0x000000, 0.75);
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

-(void)drawGuideView{
    UIImageView *firstView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[[self.imgList safeObjectAtIndex:self.index - 1] safeObjectAtIndex:0]]];
    [self.contentView addSubview:firstView];
    firstView.center = self.position;
    
    UIImageView *secView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[[self.imgList safeObjectAtIndex:self.index - 1] safeObjectAtIndex:1]]];
    [self.contentView addSubview:secView];
    secView.ycz_x = firstView.center.x - secView.bounds.size.width;
    if (secView.ycz_x < 0) {
        secView.ycz_x = firstView.center.x;
    }
    secView.ycz_y = firstView.ycz_y + firstView.ycz_height + 10;
    
    UIImageView *thrView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[[self.imgList safeObjectAtIndex:self.index - 1] safeObjectAtIndex:2]]];
    [self.contentView addSubview:thrView];
    thrView.center = CGPointMake(secView.center.x,
                                 secView.ycz_y + secView.ycz_height + thrView.bounds.size.height / 2.0);
    if (thrView.ycz_x < 0) {
        thrView.ycz_x = 0;
    }else if(CGRectGetMaxX(thrView.frame) > WIDTH){
        thrView.ycz_x = WIDTH - thrView.ycz_width;
    }
    [self setUpBottomButton];
}

-(void)setUpBottomButton{
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *imag = [UIImage imageNamed:@"guide_chat_close"];
    if (self.index == GuideIndex_CHAT_ENTER) {
        imag = [UIImage imageNamed:@"guide_chat_know"];
    }
    [closeBtn setImage:imag forState:UIControlStateNormal];
    [self.contentView addSubview:closeBtn];
    [closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView).offset(-20);
    }];
    [closeBtn addTarget:self action:@selector(stepCloseAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if (self.index == GuideIndex_CHAT_OPENRESULTS ||
        self.index == GuideIndex_CHAT_LOTTERY) {
        UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [nextBtn setImage:[UIImage imageNamed:@"guide_chat_next"] forState:UIControlStateNormal];
        [self.contentView addSubview:nextBtn];
        [nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.bottom.equalTo(closeBtn.mas_top).offset(-20);
        }];
        [nextBtn addTarget:self action:@selector(stepNextAction:) forControlEvents:UIControlEventTouchUpInside];
    }
}


-(void)setFromView:(UIView *)fromView{
    self.position = [[[[UIApplication sharedApplication] delegate] window] convertPoint:fromView.center fromView:fromView.superview];
}

#pragma mark - Events
-(void)stepNextAction:(UIButton *)sender{
    self.index+=1;
}
-(void)stepCloseAction:(UIButton *)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - GET/SET
-(NSMutableArray<NSMutableArray <NSString *>*> *)imgList{
    if (!_imgList) {
        _imgList = [[NSMutableArray alloc] init];
        for (int i = 1; i<=4; i++) {
            NSMutableArray *arr = @[].mutableCopy;
            for (int j = 0; j<3; j++) {
                [arr addObject:[NSString stringWithFormat:@"%@%i",guideImgPathHeader(i),j]];
            }
            [_imgList addObject:arr];
        }
    }
    return _imgList;
}

NSString *guideImgPathHeader(GuideIndex index){
    NSString *str;
    switch (index) {
        case GuideIndex_CHAT_ENTER:
            str = @"guide_chat_enter_";
            break;
        case GuideIndex_CHAT_OPENRESULTS:
            str = @"guide_chat_openresults_";
            break;
        case GuideIndex_CHAT_LOTTERY:
            str = @"guide_chat_lottery_";
            break;
        case GuideIndex_CHAT_NAVIMORE:
            str = @"guide_chat_navimore_";
            break;
        default:
            str = nil;
            break;
    }
    return str;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
