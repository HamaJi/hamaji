//
//  SafeguardViewController.h
//  Bet365
//
//  Created by adnin on 2019/4/1.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ViewControllerJumpType) {
    /** **/
    JumpType_present = 0,
    /** **/
    JumpType_push
};

@interface SafeguardViewController : Bet365ViewController

@property (nonatomic)NSString *htmlString;
/**  */
@property (nonatomic,assign) ViewControllerJumpType jumpType;

@end
