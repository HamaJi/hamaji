//
//  BouncedViewController.h
//  Bet365
//
//  Created by jesse on 2018/12/30.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol BounceControllerDelegate <NSObject>
- (void)surePushControllerComplement:(NSString *)complementStr gameType:(NSString *)gameType liveC:(NSString *)liveCode GameKind:(NSString *)gameKind;
@end
@interface BouncedViewController : Bet365ViewController
@property (nonatomic,weak)id<BounceControllerDelegate> delegate;
/*gameType*/
@property (nonatomic,copy)NSString *gameType;
/*liveCode*/
@property (nonatomic,copy)NSString *liveCode;

@property (nonatomic,copy)NSString *gameKind;
@end
