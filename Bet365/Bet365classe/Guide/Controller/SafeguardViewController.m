//
//  SafeguardViewController.m
//  Bet365
//
//  Created by adnin on 2019/4/1.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "SafeguardViewController.h"

@interface SafeguardViewController ()
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation SafeguardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.webView loadHTMLString:self.htmlString baseURL:[NSURL URLWithString:SerVer_Url]];
    if (self.jumpType == JumpType_push) {
        [self.navigationController.navigationBar setBarTintColor:JesseColor(196, 7, 26)];
        @weakify(self);
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem createItemsByImageStr:@"navigation_back" Titile:@"返回" Position:LXMImagePositionLeft TapHandle:^(UIGestureRecognizer *gestureRecoginzer) {
            @strongify(self);
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
