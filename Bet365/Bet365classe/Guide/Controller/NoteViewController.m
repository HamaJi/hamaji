//
//  NoteViewController.m
//  Bet365
//
//  Created by luke on 2018/10/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NoteViewController.h"
#import <DTCoreText.h>
#import "DTAnimatedGIF.h"
#import "Bet365RechargeHeaderView.h"
#import "ZSDTCoreTextCell.h"

static NSString * const CoreTextCellID = @"ZSDTCoreTextCell";
@interface NoteViewController ()
<UITableViewDataSource,
UITableViewDelegate,
DTAttributedTextContentViewDelegate,
DTLazyImageViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *noticeContentView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
//类似tabelView的缓冲池，用于存放图片大小
@property (nonatomic, strong) NSCache *imageSizeCache;
@property (nonatomic,strong)NSCache *cellCache;
@property (nonatomic,assign)BOOL isScrolling;
/**  */
@property (nonatomic,strong) NSMutableArray *dataArrs;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *noteViewHeightLayout;
@property (weak, nonatomic) IBOutlet UILabel *titleLb;

@property (weak, nonatomic) IBOutlet UIView *headContentView;

@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (nonatomic,strong) NSMutableDictionary *cellHeightData;

@end

@implementation NoteViewController
-(void)dealloc{
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    UIImage *img = [UIImage imageNamed:@ "chat_close_icon" ];
    img = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.closeBtn setImage:img forState:UIControlStateNormal];
    [self.closeBtn setTintColor:[UIColor skinNaviTintColor]];
    UIImageView *bg = [[UIImageView alloc] initWithImage:[UIImage skinGradientHorNaviImage]];
    [self.headContentView insertSubview:bg atIndex:0];
    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(bg.superview);
    }];
    self.titleLb.textColor = [UIColor skinTextTitileColor];
//    self.noticeContentView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    _imageSizeCache = [[NSCache alloc] init];
    _cellCache = [[NSCache alloc] init];
    _cellCache.totalCostLimit = 10;
    _cellCache.countLimit = 10;
    self.tableView.sectionHeaderHeight = 40;
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerClass:[ZSDTCoreTextCell class] forCellReuseIdentifier:CoreTextCellID];
    [self.tableView registerNib:[UINib nibWithNibName:@"Bet365RechargeHeaderView" bundle:nil] forHeaderFooterViewReuseIdentifier:@"Bet365RechargeHeaderView"];
}

- (IBAction)removeClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataArrs.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NoticeModel *model = self.dataArrs[section];
    if (model.isNote) {
        return 1;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //富文本单元格
    //自定义方法，创建富文本单元格
    ZSDTCoreTextCell *dtCell = (ZSDTCoreTextCell *) [self tableView:tableView prepareCellForIndexPath:indexPath];
    return dtCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    ZSDTCoreTextCell *cell = (ZSDTCoreTextCell *)[self tableView:tableView prepareCellForIndexPath:indexPath];
    CGFloat cellHeight = [cell requiredRowHeightInTableView:tableView];
    return cellHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    Bet365RechargeHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Bet365RechargeHeaderView"];
    NoticeModel *model = self.dataArrs[section];
    headerView.noticeModel = model;
    headerView.selectBlock = ^{
        model.isNote = !model.isNote;
        [self.tableView reloadData];
//        [tableView reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationNone];
    };
    return headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NoticeModel *model = self.dataArrs[indexPath.section];
    NSArray *hrefArrs = [model.noticeContent regularExpressionWithPattern:@"href=\".*?\""];
    __block NSString *hrefStr = nil;
    [hrefArrs enumerateObjectsUsingBlock:^(NSTextCheckingResult  *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        hrefStr = [model.noticeContent substringWithRange:obj.range];
    }];
    if (hrefStr.length > 0) {
        NSRange range = NSMakeRange(6, hrefStr.length - 7);
        hrefStr = [hrefStr substringWithRange:range];
        if (self.delegate && [self.delegate respondsToSelector:@selector(notePushViewControllerWithHref:)]) {
            [self dismissViewControllerAnimated:YES completion:^{
                [self.delegate notePushViewControllerWithHref:hrefStr];
            }];
        }
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = [NSString stringWithFormat:@"%li",indexPath.section];
    [self.cellHeightData safeSetObject:@(cell.frame.size.height) forKey:key];
    __block CGFloat viewHeight = 0;
    [self.dataArrs enumerateObjectsUsingBlock:^(NoticeModel * _Nonnull model, NSUInteger idx, BOOL * _Nonnull stop) {
        if (model.isNote) {
            viewHeight += [self.cellHeightData[StringFormatWithInteger(idx)] floatValue];
        }
        if (viewHeight > HEIGHT - 130) {
            viewHeight = HEIGHT - 130;
        }
        self.noteViewHeightLayout.constant = viewHeight;
    }];
}

#pragma mark - DTAttributedTextContentViewDelegate
//对于没有在Html标签里设置宽高的图片，在这里为其设置占位
- (UIView *)attributedTextContentView:(DTAttributedTextContentView *)attributedTextContentView viewForAttachment:(DTTextAttachment *)attachment frame:(CGRect)frame{
    
    if([attachment isKindOfClass:[DTImageTextAttachment class]]){
        NSString *imageURL = [NSString stringWithFormat:@"%@", attachment.contentURL];
        
        DTLazyImageView *imageView = [[DTLazyImageView alloc] initWithFrame:frame];
        imageView.delegate = self;
        imageView.image = [(DTImageTextAttachment *)attachment image];
        imageView.contentView = attributedTextContentView;
        imageView.url = attachment.contentURL;
        //处理gif图片
        if ([imageURL containsString:@"gif"]) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSData *gifData = [NSData dataWithContentsOfURL:attachment.contentURL];
                dispatch_async(dispatch_get_main_queue(), ^{
                    imageView.image = DTAnimatedGIFFromData(gifData);
                });
            });
        }
        return imageView;
    }
    return nil;
}

//对于无宽高懒加载得到的图片，缓存记录其大小,然后执行表视图更新
- (void)lazyImageView:(DTLazyImageView *)lazyImageView didChangeImageSize:(CGSize)size{
    BOOL needUpdate = NO;
    NSURL *url = lazyImageView.url;
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"contentURL == %@", url];
    for (DTTextAttachment *oneAttachment in [lazyImageView.contentView.layoutFrame textAttachmentsWithPredicate:pred]){
        CGSize originalSize = oneAttachment.originalSize;
        if (!originalSize.height || !originalSize.width) {
            oneAttachment.originalSize = size;
            NSValue *sizeValue = [_imageSizeCache objectForKey:oneAttachment.contentURL];
            if (!sizeValue) {
                //将图片大小记录在缓存中，但是这种图片的原始尺寸可能很大，所以这里设置图片的最大宽
                //并且计算高
                CGFloat aspectRatio = size.height / size.width;
                CGFloat width = WIDTH - 70 - 15*2;
                CGFloat height = width * aspectRatio;
                CGSize newSize = CGSizeMake(width, height);
                [_imageSizeCache setObject:[NSValue valueWithCGSize:newSize]forKey:url];
            }
            needUpdate = YES;
        }
        if (CGSizeEqualToSize(oneAttachment.originalSize, CGSizeZero)){
            
        }
    }
    
    if (needUpdate){
        //有新的图片尺寸被缓存记录的时候，需要刷新表视图
        //[self reloadCurrentCell];
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(reloadCurrentCell) object:nil];
        self.isScrolling = NO;
        [self performSelector:@selector(reloadCurrentCell) withObject:nil afterDelay:0.2];
    }
}
#pragma mark - private Methods
//创建富文本单元格，并更新单元格上的数据
- (ZSDTCoreTextCell *)tableView:(UITableView *)tableView prepareCellForIndexPath:(NSIndexPath *)indexPath{
    NSString *key = [NSString stringWithFormat:@"dtCoreTextCellKEY%ld-%ld", (long)indexPath.section, (long)indexPath.row];
    ZSDTCoreTextCell *cell = [_cellCache objectForKey:key];
    if (!cell){
        cell = [[ZSDTCoreTextCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CoreTextCellID];
        cell.attributedTextContextView.edgeInsets = UIEdgeInsetsMake(5, 15, 5, 15);
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.hasFixedRowHeight = NO;
        cell.textDelegate = self;
        cell.attributedTextContextView.shouldDrawImages = YES;
        //记录在缓存中
        [_cellCache setObject:cell forKey:key];
    }
    //2.设置数据
    //2.1为富文本单元格设置Html数据
    NoticeModel *model = self.dataArrs[indexPath.section];
    NSArray *resultArr = [model.noticeContent regularExpressionWithPattern:@"style=\"width.*?\""];
    __block NSMutableString *str = [[NSMutableString alloc] initWithString:model.noticeContent];
    [resultArr enumerateObjectsUsingBlock:^(NSTextCheckingResult * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSRange range = obj.range;
        if (range.location + range.length <= str.length) {
            [str deleteCharactersInRange:obj.range];
        }
    }];
    NSMutableDictionary *options = [NSMutableDictionary dictionary];
    [options setObject:[NSNumber numberWithFloat:IS_IPHONE_5s ? 13 : 15] forKey:DTDefaultFontSize];
    [cell setHTMLString:str options:options];
    
    //2.2为每个占位图(图片)设置大小，并更新
    for (DTTextAttachment *oneAttachment in cell.attributedTextContextView.layoutFrame.textAttachments) {
        NSValue *sizeValue = [_imageSizeCache objectForKey:oneAttachment.contentURL];
        if (sizeValue) {
            cell.attributedTextContextView.layouter=nil;
            oneAttachment.displaySize = [sizeValue CGSizeValue];
            [cell.attributedTextContextView relayoutText];
        }
    }
    [cell.attributedTextContextView relayoutText];
    return cell;
}

- (void)reloadCurrentCell{
    //如果当前表视图在滑动就不执行刷新，因为滑动时候会自动调用表视图的刷新方法
    if (self.isScrolling) {
        return;
    }
    //如果当前表视图没有在滑动，就手动刷新当前在屏幕显示的单元格
    NSArray *indexPaths = [self.tableView indexPathsForVisibleRows];
    if(indexPaths){
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
        });
    }
}

////显示动画，表视图停止滑动之后调用；没有动画此方法不调用
//- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
//    _isScrolling = NO;
////   [self reloadCurrentCell];
//}
//
////手指拖动表视图，表视图停止滑动之后调用
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
//    _isScrolling = NO;
////    [self reloadCurrentCell];
//}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    _isScrolling = YES;
}

- (void)setType:(NoteViewType)type
{
    _type = type;
    self.dataArrs = nil;
    [self.tableView reloadData];
}

#pragma mark - SET/GET
- (NSMutableArray *)dataArrs
{
    if (!_dataArrs) {
        _dataArrs = [NSMutableArray array];
        switch (self.type) {
            case NoteViewType_Index:
            {
                self.titleLb.text = @"平台公告";
                [BET_CONFIG.noticeModel.index_notice enumerateObjectsUsingBlock:^(NoticeModel *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (idx < 1) {
                        obj.isNote = YES;
                    }
                    [_dataArrs addObject:obj];
                }];
            }
                break;
            case NoteViewType_Login:
            {
                self.titleLb.text = @"登录公告";
                [BET_CONFIG.noticeModel.login_notice enumerateObjectsUsingBlock:^(NoticeModel *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (idx < 1) {
                        obj.isNote = YES;
                    }
                    [_dataArrs addObject:obj];
                }];
            }
                break;
            case NoteViewType_Register:
            {
                self.titleLb.text = @"注册公告";
                [BET_CONFIG.noticeModel.register_notice enumerateObjectsUsingBlock:^(NoticeModel *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (idx < 1) {
                        obj.isNote = YES;
                    }
                    [_dataArrs addObject:obj];
                }];
            }
                break;
            default:
                break;
        }
    }
    return _dataArrs;
}

- (NSMutableDictionary *)cellHeightData
{
    if (!_cellHeightData) {
        _cellHeightData = @{}.mutableCopy;
    }
    return _cellHeightData;
}

@end
