//
//  ImmediateCell.m
//  Bet365
//
//  Created by luke on 2019/10/21.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "ImmediateCell.h"
#import "ImmediateModel.h"

@interface ImmediateCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLb;
@property (weak, nonatomic) IBOutlet UILabel *contentLb;

@end

@implementation ImmediateCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(ImmediateModel *)model
{
    _model = model;
    self.titleLb.text = model.messageTitle;
    self.contentLb.text = model.messageContent;
}

@end
