//
//  ImmediateViewController.h
//  Bet365
//
//  Created by luke on 2019/10/21.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ImmediateModel;
NS_ASSUME_NONNULL_BEGIN

@interface ImmediateViewController : UIViewController
@property (nonatomic,strong) NSArray <ImmediateModel *> *datas;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

NS_ASSUME_NONNULL_END
