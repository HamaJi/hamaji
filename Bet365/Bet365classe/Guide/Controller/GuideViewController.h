//
//  GuideViewController.h
//  Bet365
//
//  Created by HHH on 2018/10/20.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GuideItem.h"

typedef NS_ENUM(NSUInteger, GuideIndex) {
    GuideIndex_NONE,
    /** 聊天入口*/
    GuideIndex_CHAT_ENTER,
    /** 聊天开奖记录*/
    GuideIndex_CHAT_OPENRESULTS,
    /** 聊天彩票*/
    GuideIndex_CHAT_LOTTERY,
    /** 聊天navi更多*/
    GuideIndex_CHAT_NAVIMORE,
};

typedef UIView *(^GuideNextBlock)(GuideIndex idx);

@interface GuideViewController : Bet365ViewController

@property (nonatomic,assign)GuideIndex index;

@property (nonatomic,copy)GuideNextBlock handle;


@end
