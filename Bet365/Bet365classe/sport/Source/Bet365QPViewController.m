//
//  Bet365QPViewController.m
//  Bet365
//
//  Created by luke on 2018/6/23.
//  Copyright © 2018年 jesse. All rights reserved.
//
#import "Bet365QPViewController.h"
#import "HMSegmentedControl.h"
#import "LukeLiveViewController.h"
#import "BasicWebViewController.h"
#import "LukeGameItem.h"

#define liveRedirect @"%@/liveRedirect.html?liveCode=%@&gameType=%@"
#define JbqstUrl @"/api/live/qst?liveCode=%@&page=1&rows=1000"

@interface QPCollectionCell:UICollectionViewCell
- (void)setCellItem:(KYQPGameItem *)item type:(NSString *)type;
@end
@interface QPCollectionCell()<SDWebImageManagerDelegate>
@property(strong,nonatomic)UIImageView *gameLogo;
@property(strong,nonatomic)UILabel *gameTitleLabel;
@end
@implementation QPCollectionCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self gameLogo];
        [self gameTitleLabel];
    }
    return self;
}

-(UIImageView *)gameLogo{
    if (_gameLogo==nil) {
        _gameLogo = [[UIImageView alloc]init];
        _gameLogo.contentMode = UIViewContentModeScaleAspectFit;
        [SDWebImageManager sharedManager].delegate = self;
        [self.contentView addSubview:_gameLogo];
        [_gameLogo mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(self.contentView).offset(10);
            make.trailing.equalTo(self.contentView).offset(-10);
            make.top.equalTo(self.contentView).offset(10);
            make.height.mas_equalTo(_gameLogo.mas_width);
        }];
    }
    return _gameLogo;
}

-(UILabel *)gameTitleLabel{
    if (_gameTitleLabel==nil) {
        _gameTitleLabel = [jesseGuideUIConfig getCurrenLableUiByRect:CGRectMake(0, 0, 0, 0) AndLabelTitle:@"" AndTitleColor:[UIColor blackColor] AndTextAlignment:NSTextAlignmentCenter AndIsAdWidth:YES AndBackColor:[UIColor clearColor] AndUseMasonry:YES];
        _gameTitleLabel.font = [UIFont systemFontOfSize:IS_IPHONE_5s ? 12 : 14];
        [self.contentView addSubview:_gameTitleLabel];
        [_gameTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView).offset(-5);
        }];
    }
    return _gameTitleLabel;
}

- (void)setCellItem:(KYQPGameItem *)item type:(NSString *)type
{
    self.gameTitleLabel.text = item.chineseName;
    [self.gameLogo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/data/%@Picture/%@",ProJect,type,item.imageName]] placeholderImage:[UIImage imageNamed:@"gameLoading"]];
}

@end

static NSString * const QPCollectionCellID = @"QPCollectionCell";

@interface Bet365QPViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
/**  */
@property (nonatomic,copy) NSString *type;
/** 开源棋牌数组 */
@property (nonatomic,strong) NSArray *kyqpArrs;
/**  */
@property (nonatomic,weak) HMSegmentedControl *segment;
/**  */
@property (nonatomic,strong) UICollectionView *collectionView;

@end

@implementation Bet365QPViewController
- (UICollectionView *)collectionView
{
    if (_collectionView == nil) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        CGFloat collecWidth = WIDTH / 4;
        layout.itemSize = CGSizeMake(collecWidth,collecWidth * 1.2);
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        [_collectionView registerClass:[QPCollectionCell class] forCellWithReuseIdentifier:QPCollectionCellID];
        [self.view addSubview:_collectionView];
        [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.bottom.equalTo(self.view);
            make.top.equalTo(self.segment.mas_bottom);
        }];
    }
    return _collectionView;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.kyqpArrs.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    QPCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:QPCollectionCellID forIndexPath:indexPath];
    [cell setCellItem:self.kyqpArrs[indexPath.item] type:self.type];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (JesseAppdelegate.CurrenIsLogStaus) {
        KYQPGameItem *item = self.kyqpArrs[indexPath.item];
        BasicWebViewController *webVC = [[BasicWebViewController alloc] init];
        webVC.liveCode = item.liveCode;
        webVC.gameType = item.gameType;
        [self.navigationController pushViewController:webVC animated:YES];
    }else{
        [self.navigationController pushViewController:[[jesseLogBet365ViewController alloc] init] animated:YES];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.type = @"jb";
    [self setupUI];
    [self queryLiveData];
}

- (void)setupUI
{
    HMSegmentedControl *segment = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"金宝棋牌",@"开元棋牌"]];
    segment.selectionIndicatorColor = [UIColor redColor];
    segment.selectionIndicatorHeight = 2;
    segment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segment.selectionStyle = HMSegmentedControlSegmentWidthStyleDynamic;
    segment.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor redColor],NSFontAttributeName : [UIFont systemFontOfSize:IS_IPHONE_5s ? 15 : 17]};
    segment.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor],NSFontAttributeName : [UIFont systemFontOfSize:IS_IPHONE_5s ? 15 : 17]};
    [segment addTarget:self action:@selector(segmentClick:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segment];
    [segment mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.equalTo(self.view);
        make.top.equalTo(self.view).offset(STATUSANDNAVGATIONHEIGHT);
        make.height.mas_equalTo(50);
    }];
    self.segment = segment;
}

- (void)segmentClick:(HMSegmentedControl *)segment
{
    switch (segment.selectedSegmentIndex) {
        case 0:
            self.type = @"jb";
            [self queryLiveData];
            break;
        case 1:
            self.type = @"ky";
            [self queryLiveData];
            break;
        default:
            break;
    }
}

- (void)queryLiveData
{
    [SVProgressHUD show];
    [NET_DATA_MANAGER GET:[NSString stringWithFormat:JbqstUrl,self.type] RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:^(id responseObject) {
        [SVProgressHUD dismiss];
        self.kyqpArrs = [KYQPGameItem mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
        [self.collectionView reloadData];
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"%@",error);
    }];
}

@end
