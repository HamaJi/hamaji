//
//  Bet365TuLongController.m
//  Bet365
//
//  Created by luke on 2019/6/6.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "Bet365TuLongController.h"
#import "TuLongDataModel.h"
#import "Bet365TuLongGameCell.h"
#import "NetWorkMannager+lottery.h"
#import "Bet365TuLongGameHeaderView.h"
#import "DragSwipeButton.h"
#import "PresentingBottomAnimator.h"
#import "DismissingBottomAnimator.h"
#import "Bet365CustomController.h"

@interface Bet365TuLongController ()
<UITableViewDataSource,
UITableViewDelegate,
TuLongGameBetHandelDelegate,
GameHeaderViewDelegate,
UIViewControllerTransitioningDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSArray *gameArrs;
@property (nonatomic,assign) TuLongCellType type;
@property(strong,nonatomic)DragSwipeButton *swipeButton;
@property (nonatomic,strong) UILabel *titleLb;
@end

@implementation Bet365TuLongController

+(void)load{
    [MGJRouter registerURLPattern:ROUTER_TULONG toHandler:^(NSDictionary *routerParameters) {
        Bet365TuLongController *tulong = [[Bet365TuLongController alloc]init];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:tulong animated:YES];
        tulong.hidesBottomBarWhenPushed = YES;
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.tableView.backgroundColor = JesseColor(239, 239, 244);
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.tableHeaderView = self.titleLb;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"Bet365TuLongGameCell" bundle:nil] forCellReuseIdentifier:@"Bet365TuLongGameCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"Bet365TuLongGameHeaderView" bundle:nil] forHeaderFooterViewReuseIdentifier:@"Bet365TuLongGameHeaderView"];
    [self getData];
    [self swipeButton];
}

- (void)getData
{
    if (self.type == TuLongCellType_custom) {
        [SVProgressHUD showWithStatus:@"加载中..."];
        [NET_DATA_MANAGER requestGetTuLongWithgameIds:AUTHDATA_MANAGER.tulongGame ResponseCache:nil success:^(id responseObject) {
            self.gameArrs = [TuLongDataModel mj_objectArrayWithKeyValuesArray:responseObject];
            [self.tableView reloadData];
            [SVProgressHUD dismiss];
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
            [SVProgressHUD dismiss];
        }];
    }else{
        [SVProgressHUD showWithStatus:@"加载中..."];
        [NET_DATA_MANAGER requestGetTuLongRankResponseCache:nil success:^(id responseObject) {
            self.gameArrs = [TuLongDesModel mj_objectArrayWithKeyValuesArray:responseObject];
            [self.tableView reloadData];
            [SVProgressHUD dismiss];
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
            [SVProgressHUD dismiss];
        }];
    }
}

#pragma mark - UITableViewDataSource,UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.type == TuLongCellType_custom) {
        return self.gameArrs.count;
    }else{
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.type == TuLongCellType_custom) {
        TuLongDataModel *model = self.gameArrs[section];
        if (!model.open) {
            return model.lmcl.count;
        }else{
            return 0;
        }
    }else{
        return self.gameArrs.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Bet365TuLongGameCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Bet365TuLongGameCell"];
    cell.delegate = self;
    if (self.type == TuLongCellType_custom) {
        TuLongDataModel *model = self.gameArrs[indexPath.section];
        cell.model = model.lmcl[indexPath.row];
    }else{
        cell.model = self.gameArrs[indexPath.row];
    }
    if (indexPath.row % 2 == 0) {
        cell.backgroundColor = [UIColor whiteColor];
    }else{
        cell.backgroundColor = JesseGrayColor(248);
    }
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.type == TuLongCellType_custom) {
        Bet365TuLongGameHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Bet365TuLongGameHeaderView"];
        headerView.delegate = self;
        headerView.model = self.gameArrs[section];
        return headerView;
    }else{
        return [UIView new];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return self.type == TuLongCellType_custom ? 50 : 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

#pragma mark - GameHeaderViewDelegate
- (void)gameHeaderViewTap:(TuLongDataModel *)model
{
    model.open = !model.open;
    NSInteger section = [self.gameArrs indexOfObject:model];
    [self.tableView reloadSection:section withRowAnimation:UITableViewRowAnimationMiddle];
}

#pragma mark - TuLongGameBetHandelDelegate
- (void)gameBetHandelWithCell:(Bet365TuLongGameCell *)cell
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    TuLongDesModel *desModel = nil;
    if (self.type == TuLongCellType_custom) {
        TuLongDataModel *dataModel = self.gameArrs[indexPath.section];
        desModel = dataModel.lmcl[indexPath.row];
    }else{
        desModel = self.gameArrs[indexPath.row];
    }
    BET_CONFIG.successlongDragon = YES;
//    JesseAppdelegate.tulongEnterId = desModel.code;
    LOTTERY_FACTORY.tulongEnterId = desModel.code;
    [self homePushEnterFactory:YES FactoryGameId:[NSString stringWithFormat:@"%@",desModel.gameId] TuLongEnterValue:YES];
}

- (void)swipePresentCustom
{
    Bet365CustomController *vc = [[Bet365CustomController alloc] init];
    @weakify(self);
    vc.block = ^{
        @strongify(self);
        if (self.type == TuLongCellType_custom) {
            [self getData];
        }
    };
    vc.transitioningDelegate = self;
    vc.modalPresentationStyle = UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark - UIViewControllerTransitioningDelegate
-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    return [DismissingBottomAnimator new];
}
-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    return [PresentingBottomAnimator new];
}

#pragma mark - SET/GET
-(DragSwipeButton *)swipeButton{
    if (!_swipeButton) {
        NSMutableArray *arr = [NSMutableArray array];
        NSArray *titles = @[@"混排",@"排行",@"自定义"];
        @weakify(self);
        for (int i = 0; i<titles.count; i++) {
            [arr addObject:[DragLaunchItem creatLaunchItemWithTitile:titles[i]
                                                               Image:[UIImage imageNamed:[NSString stringWithFormat:@"dragSwipe_%i",i]]
                                                              Handle:^BOOL(NSUInteger idx) {
                                                                  @strongify(self);
                                                                  switch (idx) {
                                                                      case 0:
                                                                          self.type = TuLongCellType_rank;
                                                                          [self getData];
                                                                          break;
                                                                      case 1:
                                                                          self.type = TuLongCellType_custom;
                                                                          [self getData];
                                                                          break;
                                                                      case 2:
                                                                          [self swipePresentCustom];
                                                                          break;
                                                                      default:
                                                                          break;
                                                                  }
                                                                  return YES;
                                                              }]];
        }
        _swipeButton = [[DragSwipeButton alloc] initWithPoint:CGPointMake(WIDTH-25.0, HEIGHT/2) DragLaunchItemList:arr];
        [self.view addSubview:_swipeButton];
    }
    return _swipeButton;
}

- (UILabel *)titleLb
{
    if (!_titleLb) {
        _titleLb = [[UILabel alloc] init];
        _titleLb.numberOfLines = 0;
        _titleLb.font = [UIFont systemFontOfSize:15];
        _titleLb.text = @"⭐️友情提醒:点击或拖动正下方圆球按钮可自定义显示彩种";
        _titleLb.width = WIDTH;
        [_titleLb sizeToFit];
    }
    return _titleLb;
}

@end
