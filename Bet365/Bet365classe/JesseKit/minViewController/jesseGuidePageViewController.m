//
//  jesseGuidePageViewController.m
//  Bet365
//
//  Created by jesse on 2017/9/9.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "jesseGuidePageViewController.h"
#import "jesseGuideUIConfig.h"
#import <CoreText/CTFontDescriptor.h>
#import <Masonry.h>
#define FirstDisTanceHeight 120
#define SecondDistanceHeight 80
#define ThirdDistanceHeight 80
#define fourthDistanceHeight 50
@protocol enterAppDelegate<NSObject>
-(void)enterApp;/**代理进入APP**/
@end
@interface superScrollview : UIScrollView
@property(assign,nonatomic)BOOL isUpdataVersion;/**Property:是否为更新版本内容**/
-(void)target:(id)DoTask;
-(void)responsePage:(CGFloat)content andPage:(int)page;/**触发修改当前对应index任务**/
-(void)responsePageForPageFour:(CGFloat)contenX;
-(void)responsePage:(CGFloat)contentXAndForThree;/**响应Page3**/
@property(nonatomic,weak)id <enterAppDelegate> EnterDelegate;
@end
@interface superScrollview()<SDCycleScrollViewDelegate>
{
    
    /**PageOne**/
    UIView *_backViewOne;
    UIImageView *_logoImageOne;
    UIImageView *_iponeImageOne;
    UIImageView *_alertViewOne;
    UIButton *alertTextOne;
    UILabel *_alertViewOneInsetBelowOne;
    UIImageView *_alertViewOneInsetThanOneBelow;
    CGFloat BolwOneY;
    CGFloat BolwTwoY;
    /**PageTwo**/
    UIView *_backViewTwo;
    UIImageView *backtwoLogImage;
    UIImageView *backTwoIphoneFormImage;
    UILabel *backTwoDesLabel;
    UIImageView *backTwoDesUnderImage;
    /**PageThree**/
    UIView *_backViewThree;
    UIImageView *backThreeLogImage;
    UIImageView *backThreeIphoneForm;
    SDCycleScrollView *SDCycleview;
    UIButton *_buttonAlertBack;
    UILabel *desAlertForPageThree;
    /**PageFour**/
    UIView *_backViewFour;
    UIImageView *BasicImage;
    UIButton *enterAppButton;
    UIImageView *logDesImageView;
    UIImageView *markImage;
    UILabel *marklabelforFourPage;
    UIImageView *RunimgView;
}
@property(nonatomic,strong)id target;
@end
@implementation superScrollview
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self detailUI];
    }
    return self;
}
-(void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{

}
-(void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index{
        CATransition *annimation = [CATransition animation];
        annimation.duration = 1.2;
        annimation.type = @"rippleEffect";
        annimation.subtype= kCATransitionFromRight;
        [cycleScrollView.layer addAnimation:annimation forKey:nil];
       desAlertForPageThree.text = [NSString stringWithFormat:@"致快发所有会员:\n%@",[[jesseGuideUIConfig initShareManager] exportUpdateBy:2][2][index]];
}
/**处理当前UI部分**/
-(void)detailUI{
/**PageOne**/
    _backViewOne = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    [self addSubview:_backViewOne];
    _logoImageOne = [jesseGuideUIConfig getCurrenImageUiRect:self.bounds AndImage:[UIImage imageNamed:@"kfcpLog"] AnduseMasonry:YES];
    [_backViewOne addSubview:_logoImageOne];
    [_logoImageOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_backViewOne.mas_top).offset(40);
        make.centerX.mas_equalTo(_backViewOne.mas_centerX);
        make.width.mas_equalTo(130);
        make.height.mas_equalTo(50);
        [self layoutIfNeeded];
    }];
    _iponeImageOne = [jesseGuideUIConfig getCurrenImageUiRect:self.bounds AndImage:[UIImage imageNamed:@"form"] AnduseMasonry:YES];
    [_backViewOne addSubview:_iponeImageOne];
    [_iponeImageOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_backViewOne);
        make.top.mas_equalTo(_logoImageOne.mas_bottom).offset(20);
        make.height.mas_equalTo(HEIGHT-FirstDisTanceHeight-SecondDistanceHeight-ThirdDistanceHeight-fourthDistanceHeight);
        make.width.mas_equalTo(_iponeImageOne.mas_height).multipliedBy(0.5);
        [self layoutIfNeeded];
    }];
    UIImageView *updateImage = [jesseGuideUIConfig getCurrenImageUiRect:self.bounds AndImage:[UIImage imageNamed:[[jesseGuideUIConfig initShareManager] exportUpdateBy:0][1]] AnduseMasonry:YES];
    if ([[jesseGuideUIConfig initShareManager] exportCurrenUpdateMark])[updateImage sd_setImageWithURL:[NSURL URLWithString:[[jesseGuideUIConfig initShareManager] exportUpdateBy:0][1]] placeholderImage:[UIImage imageNamed:@"guide"]];
    [_iponeImageOne addSubview:updateImage];
    [updateImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(_iponeImageOne).insets([jesseGuideUIConfig currenUiEdgeSize]);
    }];
    UIButton *alertButton = [jesseGuideUIConfig getCurrenButtonUiByRect:self.bounds AndButtonTitle:[[jesseGuideUIConfig initShareManager] exportUpdateBy:0][0]AndTitleColor:[UIColor whiteColor] AndtextAlignment:NSTextAlignmentCenter And:@selector(zz) Andtarget:self AndlayerCorner:0 AndUseMasonry:YES];
    UIFont *font = [UIFont fontWithName:@"DFWaWaSC-W5" size:13];
    [alertButton.titleLabel setFont:font];
    alertButton.enabled = NO;
    alertButton.titleLabel.numberOfLines = 0;
    [alertButton setBackgroundImage:[UIImage imageNamed:@"alertOne"] forState:UIControlStateNormal];
    [_backViewOne addSubview:alertButton];
    [alertButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_iponeImageOne.mas_right).offset(0);
        make.right.mas_equalTo(_backViewOne.mas_right).offset(-5);
        make.bottom.mas_equalTo(_iponeImageOne.mas_top).offset(50);
        make.height.mas_equalTo(alertButton.mas_width).multipliedBy(1.5);
        [self layoutIfNeeded];
    }];
    alertTextOne = alertButton;
    _alertViewOneInsetThanOneBelow = [jesseGuideUIConfig getCurrenImageUiRect:self.bounds AndImage:[UIImage imageNamed:@"textA"] AnduseMasonry:YES];
    [_backViewOne addSubview:_alertViewOneInsetThanOneBelow];
    [_alertViewOneInsetThanOneBelow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_backViewOne);
        make.bottom.mas_equalTo(_backViewOne.mas_bottom).offset(-55);
        make.height.mas_equalTo(50);
        make.width.and.height.lessThanOrEqualTo(_backViewOne);
        [self layoutIfNeeded];
    }];
    _alertViewOneInsetBelowOne = [jesseGuideUIConfig getCurrenLableUiByRect:self.bounds AndLabelTitle:[[jesseGuideUIConfig initShareManager] exportUpdateBy:0][2] AndTitleColor:JesseColor(153, 153, 153) AndTextAlignment:NSTextAlignmentCenter AndIsAdWidth:YES AndBackColor:[UIColor clearColor] AndUseMasonry:YES];
    _alertViewOneInsetBelowOne.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:15];
    _alertViewOneInsetBelowOne.numberOfLines=0;
    [_backViewOne addSubview:_alertViewOneInsetBelowOne];
    [_alertViewOneInsetBelowOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(_alertViewOneInsetThanOneBelow.mas_top).offset(2);
        make.centerX.equalTo(_backViewOne);
        make.top.mas_equalTo(_iponeImageOne.mas_bottom).offset(5);
        make.width.lessThanOrEqualTo(_iponeImageOne);
        [self layoutIfNeeded];
    }];
    /**PageTwo**/
    _backViewTwo = [[UIView alloc]initWithFrame:CGRectMake(WIDTH, 0, WIDTH, HEIGHT)];
    [self addSubview:_backViewTwo];
    backtwoLogImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 170, 60)];
    backtwoLogImage.image = [UIImage imageNamed:@"kfcpLog"];
    backtwoLogImage.center = CGPointMake(WIDTH/2, 70);
    [_backViewTwo addSubview:backtwoLogImage];
    backTwoIphoneFormImage = [[UIImageView alloc]init];
    backTwoIphoneFormImage.image = [UIImage imageNamed:@"3_01"];
    [_backViewTwo addSubview:backTwoIphoneFormImage];
    [backTwoIphoneFormImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_backViewTwo);
        make.top.mas_equalTo(backtwoLogImage.mas_bottom).offset(20);
        make.height.mas_equalTo(HEIGHT-FirstDisTanceHeight-SecondDistanceHeight-ThirdDistanceHeight-fourthDistanceHeight);
        make.width.mas_equalTo(backTwoIphoneFormImage.mas_height).multipliedBy(0.5);
        [self layoutIfNeeded];
    }];
    UIImageView *updateTwoImage = [jesseGuideUIConfig getCurrenImageUiRect:self.bounds AndImage:[UIImage imageNamed:[[jesseGuideUIConfig initShareManager] exportUpdateBy:1][0]] AnduseMasonry:YES];
    if([[jesseGuideUIConfig initShareManager] exportCurrenUpdateMark])[updateTwoImage sd_setImageWithURL:[NSURL URLWithString:[[jesseGuideUIConfig initShareManager] exportUpdateBy:1][0]] placeholderImage:[UIImage imageNamed:@"guide"]];
    [backTwoIphoneFormImage addSubview:updateTwoImage];
    [updateTwoImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(backTwoIphoneFormImage).insets([jesseGuideUIConfig exportPagetwoEdge]);
    }];
    backTwoDesUnderImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 60)];
    backTwoDesUnderImage.image = [UIImage imageNamed:@"3_05Z"];
    backTwoDesUnderImage.center = CGPointMake(WIDTH/2, HEIGHT-55-30);
    backTwoDesUnderImage.contentMode = UIViewContentModeScaleAspectFit;
    [_backViewTwo addSubview:backTwoDesUnderImage];
    backTwoDesLabel = [jesseGuideUIConfig getCurrenLableUiByRect:self.bounds AndLabelTitle:[[jesseGuideUIConfig initShareManager] exportUpdateBy:1][1] AndTitleColor: JesseColor(153, 153, 153) AndTextAlignment:NSTextAlignmentCenter AndIsAdWidth:YES AndBackColor:[UIColor clearColor] AndUseMasonry:YES];
    backTwoDesLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:16];
    backTwoDesLabel.numberOfLines = 0;
    [_backViewTwo addSubview:backTwoDesLabel];
    [backTwoDesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(backTwoIphoneFormImage.mas_bottom).offset(0);
        make.bottom.mas_equalTo(backTwoDesUnderImage.mas_top).offset(0);
        make.centerX.equalTo(_backViewTwo);
        make.width.lessThanOrEqualTo(backTwoIphoneFormImage);
    }];
    /**Page3**/
    _backViewThree = [[UIView alloc]initWithFrame:CGRectMake(WIDTH*2, 0, WIDTH, HEIGHT)];
    [self addSubview:_backViewThree];
    backThreeLogImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 170, 60)];
    backThreeLogImage.image = [UIImage imageNamed:@"kfcpLog"];
    backThreeLogImage.center = CGPointMake(WIDTH/2, 70);
    [_backViewThree addSubview:backThreeLogImage];
    backThreeIphoneForm = [jesseGuideUIConfig getCurrenImageUiRect:self.bounds AndImage:[UIImage imageNamed:@"form"] AnduseMasonry:YES];
    [_backViewThree addSubview:backThreeIphoneForm];
    [backThreeIphoneForm mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_backViewThree);
        make.top.mas_equalTo(backThreeLogImage.mas_bottom).offset(20);
        make.height.mas_equalTo(HEIGHT-FirstDisTanceHeight-SecondDistanceHeight-ThirdDistanceHeight-fourthDistanceHeight);
        make.width.mas_equalTo(backThreeIphoneForm.mas_height).multipliedBy(0.5);
        [self layoutIfNeeded];
    }];
    SDCycleview = [[SDCycleScrollView alloc]init];
    SDCycleview.delegate = self;
    SDCycleview.pageControlStyle = SDCycleScrollViewPageContolStyleNone;
    SDCycleview.autoScrollTimeInterval = 4.0;
    SDCycleview.imageURLStringsGroup = [[jesseGuideUIConfig initShareManager] exportUpdateBy:2][0];
    SDCycleview.placeholderImage = [UIImage imageNamed:@"guide"];
    SDCycleview.localizationImageNamesGroup = [[jesseGuideUIConfig initShareManager] exportUpdateBy:2][0];
    SDCycleview.titlesGroup = [[jesseGuideUIConfig initShareManager] exportUpdateBy:2][1];
    SDCycleview.titleLabelTextFont = [UIFont fontWithName:@"DFWaWaSC-W5" size:15];
    [_backViewThree addSubview:SDCycleview];
    [SDCycleview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(backThreeIphoneForm).insets([jesseGuideUIConfig currenUiEdgeSize]);
    }];
    
    _buttonAlertBack = [jesseGuideUIConfig getCurrenButtonUiByRect:self.bounds AndButtonTitle:@"" AndTitleColor:[UIColor blackColor] AndtextAlignment:NSTextAlignmentLeft And:@selector(zz) Andtarget:self AndlayerCorner:1.0 AndUseMasonry:YES];
    _buttonAlertBack.titleLabel.numberOfLines = 0;
    _buttonAlertBack.titleLabel.adjustsFontSizeToFitWidth = YES;
    _buttonAlertBack.enabled = NO;
    [_buttonAlertBack setBackgroundImage:[UIImage imageNamed:@"tableAlert"] forState:UIControlStateNormal];
    _buttonAlertBack.contentMode = UIViewContentModeScaleAspectFit;
    [_backViewThree addSubview:_buttonAlertBack];
    [_buttonAlertBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(backThreeIphoneForm.mas_bottom).offset(10);
        make.bottom.mas_equalTo(_backViewThree.mas_bottom).offset(-20);
        make.width.mas_equalTo(200);
        make.centerX.equalTo(_backViewThree);
        [self layoutIfNeeded];
    }];
    desAlertForPageThree = [jesseGuideUIConfig getCurrenLableUiByRect:self.bounds AndLabelTitle:@"" AndTitleColor:[UIColor blackColor] AndTextAlignment:NSTextAlignmentCenter AndIsAdWidth:NO AndBackColor:[UIColor clearColor] AndUseMasonry:YES];
    desAlertForPageThree.text = @"致所有快发用户:";
    desAlertForPageThree.numberOfLines = 0;
    desAlertForPageThree.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:15];
    [_buttonAlertBack addSubview:desAlertForPageThree];
    [desAlertForPageThree mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(_buttonAlertBack).insets(UIEdgeInsetsMake(12, 2, 100,2));
    }];
    /**Page4**/
    _backViewFour = [[UIView alloc]initWithFrame:CGRectMake(WIDTH*3, 0, WIDTH, HEIGHT)];
    [self addSubview:_backViewFour];
    _backViewFour.alpha = 0;
    BasicImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, WIDTH*2, WIDTH*2)];
    BasicImage.image = [UIImage imageNamed:@"5_05"];
    BasicImage.center = CGPointMake(WIDTH/2, HEIGHT-60);
    [self rotate360DegreeWithImageView:BasicImage];
    [_backViewFour addSubview:BasicImage];
    enterAppButton = [jesseGuideUIConfig getCurrenButtonUiByRect:CGRectMake(0, 0, 150, 35) AndButtonTitle:[[jesseGuideUIConfig initShareManager] exportCurrenUpdateMark]?@"更新应用":@"进入App" AndTitleColor:[UIColor whiteColor] AndtextAlignment:NSTextAlignmentCenter And:@selector(enterClick) Andtarget:self AndlayerCorner:2 AndUseMasonry:NO];
    enterAppButton.titleLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17];
    enterAppButton.layer.borderColor = [UIColor whiteColor].CGColor;
    enterAppButton.layer.borderWidth = 1;
    enterAppButton.center = CGPointMake(WIDTH/2, HEIGHT-60);
    [_backViewFour addSubview:enterAppButton];
    logDesImageView = [[UIImageView alloc]init];
    logDesImageView.image = [UIImage imageNamed:@"5_03z"];
    logDesImageView.contentMode = UIViewContentModeScaleAspectFit;
    [_backViewFour addSubview:logDesImageView];
    [logDesImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(enterAppButton.mas_top).offset(-25);
        make.centerX.equalTo(_backViewFour);
        make.width.mas_equalTo(enterAppButton.mas_width).multipliedBy(1.3);
        [self layoutIfNeeded];
    }];
    markImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 170)];
    markImage.image = [UIImage imageNamed:@"5_01"];
    markImage.contentMode = UIViewContentModeScaleAspectFit;
    [_backViewFour addSubview:markImage];
    marklabelforFourPage = [jesseGuideUIConfig getCurrenLableUiByRect:CGRectMake(0, 120, WIDTH, 50) AndLabelTitle:[[jesseGuideUIConfig initShareManager] exportUpdateBy:3][0] AndTitleColor:JesseColor(153,153,153) AndTextAlignment:NSTextAlignmentCenter AndIsAdWidth:YES AndBackColor:[UIColor clearColor] AndUseMasonry:NO];
    marklabelforFourPage.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:20];
    [_backViewFour addSubview:marklabelforFourPage];
    RunimgView = [[UIImageView alloc]init];
     [_backViewFour addSubview:RunimgView];
    [RunimgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(enterAppButton.mas_top).offset(0);
        make.left.mas_equalTo(enterAppButton.mas_left).offset(10);
        make.width.mas_equalTo(35);
        make.height.mas_equalTo(35);
        [self setNeedsLayout];
    }];
    [self tomAnimationWithName:@"run" count:9];
}
/**进入App**/
-(void)enterClick{
   [self.EnterDelegate enterApp];
}
-(void)zz{
}
/**响应方法**/
-(void)responsePage:(CGFloat)content andPage:(int)page{
        _iponeImageOne.alpha = (WIDTH/2-content)/(WIDTH/2);
        alertTextOne.alpha = (WIDTH/2-content)/(WIDTH/2);
        _alertViewOneInsetThanOneBelow.alpha =(WIDTH/2-content)/(WIDTH/2);
        _alertViewOneInsetBelowOne.alpha = (WIDTH/2-content)/(WIDTH/2);
        CATransform3D  trans= CATransform3DMakeScale(((WIDTH/2-content)/(WIDTH/2)), ((WIDTH/2-content)/(WIDTH/2)), ((WIDTH/2-content)/(WIDTH/2)));
        alertTextOne.layer.sublayerTransform = trans;
        _alertViewOneInsetBelowOne.transform = CGAffineTransformMakeScale(((WIDTH/2-content)/(WIDTH/2)), ((WIDTH/2-content)/(WIDTH/2)));
        _alertViewOneInsetThanOneBelow.transform = CGAffineTransformMakeScale(((WIDTH/2-content)/(WIDTH/2)), ((WIDTH/2-content)/(WIDTH/2)));;
        [self layoutIfNeeded];
}
/**ForPageFour**/
-(void)responsePageForPageFour:(CGFloat)contenX{
    _backViewFour.alpha = 1-(WIDTH*3-contenX)/(WIDTH/2);
    BasicImage.transform =CGAffineTransformMakeScale(1-(WIDTH*3-contenX)/(WIDTH/2), 1-(WIDTH*3-contenX)/(WIDTH/2));
    logDesImageView.transform = CGAffineTransformMakeScale(1-(WIDTH*3-contenX)/(WIDTH/2), 1-(WIDTH*3-contenX)/(WIDTH/2));
    enterAppButton.transform = CGAffineTransformMakeScale(1-(WIDTH*3-contenX)/(WIDTH/2), 1-(WIDTH*3-contenX)/(WIDTH/2));
    RunimgView.transform = CGAffineTransformMakeScale(1-(WIDTH*3-contenX)/(WIDTH/2), 1-(WIDTH*3-contenX)/(WIDTH/2));
    markImage.frame = CGRectMake(0, -60*(WIDTH*3-contenX)/(WIDTH/2), WIDTH, 170);
    [self layoutIfNeeded];
}
/**ForPageTwo**/
-(void)responsePageForPageTwo:(CGFloat)contentX{
    
    if (contentX<=WIDTH) {
        backTwoDesUnderImage.center = CGPointMake(WIDTH/2,(HEIGHT-55-30)+60*(1-(contentX-WIDTH/2)/(WIDTH/2)));

        if ((contentX-WIDTH/2)/(WIDTH/2)<0.2) {
            backTwoIphoneFormImage.alpha =0.0;
            backTwoDesUnderImage.alpha = 0.0;
            backTwoDesLabel.alpha = 0.0;
        }else{
            backTwoIphoneFormImage.alpha =(contentX-WIDTH/2)/(WIDTH/2);
            backTwoDesUnderImage.alpha = (contentX-WIDTH/2)/(WIDTH/2);
            backTwoDesLabel.alpha = (contentX-WIDTH/2)/(WIDTH/2);
        }
        backtwoLogImage.alpha =(contentX-WIDTH/2)/(WIDTH/2);
    }else{
        backTwoDesUnderImage.center = CGPointMake(WIDTH/2,(HEIGHT-55-30)+80*(contentX-WIDTH/2)/(WIDTH/2));
   
        if (1-(contentX-WIDTH)/(WIDTH/2)<0.2) {
            backTwoIphoneFormImage.alpha = 0.0;
            backTwoDesUnderImage.alpha = 0.0;
            backTwoDesLabel.alpha = 0.0;
        }else{
         backTwoIphoneFormImage.alpha = 1-(contentX-WIDTH)/(WIDTH/2);
            backTwoDesUnderImage.alpha = 1-(contentX-WIDTH)/(WIDTH/2);
            backTwoDesLabel.alpha = 1-(contentX-WIDTH)/(WIDTH/2);

        }
        backtwoLogImage.alpha = 1-(contentX-WIDTH)/(WIDTH/2);
    }
    [self layoutIfNeeded];
}
/**Page3Response**/
-(void)responsePage:(CGFloat)contentXAndForThree{
    if (contentXAndForThree<=WIDTH*2) {
        backThreeLogImage.alpha = (contentXAndForThree-WIDTH*3/2)/(WIDTH/2);
        backThreeIphoneForm.alpha = (contentXAndForThree-WIDTH*3/2)/(WIDTH/2);
        _buttonAlertBack.transform = CGAffineTransformMakeScale((contentXAndForThree-WIDTH*3/2)/(WIDTH/2), (contentXAndForThree-WIDTH*3/2)/(WIDTH/2));
    }else{

        backThreeLogImage.alpha = 2-((contentXAndForThree-WIDTH*3/2)/(WIDTH/2));
        backThreeIphoneForm.alpha = 2-((contentXAndForThree-WIDTH*3/2)/(WIDTH/2));
        _buttonAlertBack.transform = CGAffineTransformMakeScale(2-((contentXAndForThree-WIDTH*3/2)/(WIDTH/2)),2-((contentXAndForThree-WIDTH*3/2)/(WIDTH/2)));
    }
    [self layoutIfNeeded];
}
#pragma mark --- UIImageView显示gif动画
- (void)tomAnimationWithName:(NSString *)name count:(int)count
{
    
    // 如果正在动画，直接退出
    if ([RunimgView isAnimating]) return;
    // 动画图片的数组
    NSMutableArray *arrayM = [[NSMutableArray alloc]init];
    // 添加动画播放的图片
    for (int i = 0; i < count; i++) {
        // 图像名称
        NSString *imageName = [NSString stringWithFormat:@"%@%d", name, i+1];

        UIImage *image = [UIImage imageNamed:imageName];
        [arrayM addObject:image];
    }
    // 设置动画数组
    RunimgView.animationImages = arrayM;
    // 重复1次
    RunimgView.animationRepeatCount = 0;
    // 动画时长
    RunimgView.animationDuration = RunimgView.animationImages.count * 0.05;
    // 开始动画
    [RunimgView startAnimating];

}



#pragma mark --- 旋转动画
- (void)rotate360DegreeWithImageView:(UIImageView *)imageView
{
    CABasicAnimation *animation = [CABasicAnimation
                                   animationWithKeyPath: @"transform" ];
    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    //围绕Z轴旋转，垂直与屏幕
    animation.toValue = [NSValue valueWithCATransform3D:
                         CATransform3DMakeRotation(M_PI, 0.0, 0.0, 1.0) ];
    animation.duration = 5;
    //旋转效果累计，先转180度，接着再旋转180度，从而实现360旋转
    animation.cumulative = YES;
    animation.repeatCount = MAXFLOAT;
    [imageView.layer addAnimation:animation forKey:nil];
}
-(void)target:(id)DoTask{
    self.target = DoTask;
}
@end
#define CONTENTCOUNT 4
#define PAGEWIDTH 100
@interface jesseGuidePageViewController ()<UIScrollViewDelegate,SDCycleScrollViewDelegate,enterAppDelegate,newGudieScrollviewDelegate>
{
    CGFloat contentX;
    superScrollview *_superScroll;
    UIPageControl *_currenPageControl;
    UIButton *_jumpButton;
}
-(void)CreatScrollviewAndPage;/**创建父式**/
@end

@implementation jesseGuidePageViewController
/**当前UI处理**/
-(void)CreatScrollviewAndPage{
    /**Page点**/
    _currenPageControl = [[UIPageControl alloc]init];
    _currenPageControl.bounds= CGRectMake(0, 0, 100, 30);
    _currenPageControl.center = CGPointMake(WIDTH*0.5, HEIGHT-20);
    _currenPageControl.numberOfPages = CONTENTCOUNT;
    _currenPageControl.pageIndicatorTintColor = JesseColor(230, 230, 230);
    _currenPageControl.currentPageIndicatorTintColor = [UIColor orangeColor];
    [self.view addSubview:_currenPageControl];
    _superScroll = [[superScrollview alloc]init];
    [_superScroll target:self];
    _superScroll.EnterDelegate = self;
    _superScroll.frame = CGRectMake(0, 0, WIDTH, HEIGHT);
    _superScroll.delegate = self;
    _superScroll.contentSize = CGSizeMake(WIDTH*CONTENTCOUNT, 0);
    _superScroll.bounces = NO;
    _superScroll.pagingEnabled = YES;
    _superScroll.showsVerticalScrollIndicator = NO;
    _superScroll.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:_superScroll];

}
-(NewGuideFatherView *)father{
    if (_father==nil) {
        NSArray *local = @[[UIImage imageNamed:@"fristone"],[UIImage imageNamed:@"fristtwo"],[UIImage imageNamed:@"fristthree"]];
        NSArray *updateCache = [NSArray array];
        if (self.scrollType!=fristEnterType) {
            /*
            NSArray *cache = [[NSUserDefaults standardUserDefaults] objectForKey:@"updateCacheData"];
            NSMutableArray *imageArr = [[NSMutableArray alloc]init];
            for (int i = 0; i<cache.count; i++) {
                NSDictionary *obj = cache[i];
                UIImage *image = [UIImage imageWithData:obj[@"ImageData"]];
                [imageArr addObject:image];
            }
            updateCache = imageArr;
             */
            updateCache = local;
        }
        _father = [[NewGuideFatherView alloc]initCurrenFrame:CGRectMake(0, 0, WIDTH, HEIGHT) loadCurrenImage:(self.scrollType==fristEnterType)?local:updateCache AndLoadType:self.scrollType];
        _father.delegate = self;
        _father.dataSource = self;
        [self.view addSubview:_father];
        _currenPageControl = [[UIPageControl alloc]init];
        _currenPageControl.frame = CGRectMake(0, 0, 100, 20);
        _currenPageControl.center = CGPointMake(WIDTH/2, HEIGHT-20);
        _currenPageControl.numberOfPages = (self.scrollType==fristEnterType)?3:updateCache.count;
        _currenPageControl.pageIndicatorTintColor = JesseColor(253, 131, 3);
        _currenPageControl.currentPageIndicatorTintColor = JesseColor(231, 61, 47);
        [self.view addSubview:_currenPageControl];
        if (self.scrollType!=updateAppType) {
            _jumpButton = [jesseGuideUIConfig getCurrenButtonUiByRect:CGRectMake(0, 0, 0, 0) AndButtonTitle:@"跳过" AndTitleColor:[UIColor lightGrayColor] AndtextAlignment:NSTextAlignmentCenter And:@selector(jumpClick) Andtarget:self AndlayerCorner:0 AndUseMasonry:YES];
            [UIApplication sharedApplication].keyWindow.userInteractionEnabled = YES;
            _jumpButton.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:4];
            _jumpButton.titleLabel.font = [UIFont boldSystemFontOfSize:13];
            [self.view addSubview:_jumpButton];
            [_jumpButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(self.view.mas_right).offset(-15);
                make.bottom.mas_equalTo(self.view.mas_bottom).offset(-30);
                make.width.mas_equalTo(48);
                make.height.mas_equalTo(22);
            }];
            _jumpButton.layer.cornerRadius = 11;
            _jumpButton.layer.masksToBounds = YES;
            _jumpButton.layer.borderColor = JesseColor(230, 230, 230).CGColor;
            _jumpButton.layer.borderWidth = 1.5;
        }

    }
    return _father;
}
/**传输值**/
-(void)bearingGuide:(loadScrollviewType)scrollType{
    self.scrollType = scrollType;
    [self father];
}
-(void)touchNormalMethond:(loadScrollviewType)scrollType{
    if (self.scrollType==fristEnterType) {
        BET_CONFIG.userSettingData.hasStep = YES;
        [BET_CONFIG.userSettingData save];
        self.enterBlock();
    }else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.model.download]];
        exit(0);
    }
}
/**跳过**/
-(void)jumpClick{
    BET_CONFIG.userSettingData.hasStep = YES;
    [BET_CONFIG.userSettingData save];
    self.enterBlock();
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden:YES];
   // self.view.backgroundColor = JesseColor(120, 28, 40);
   // [self CreatScrollviewAndPage];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    [self.navigationController.navigationBar setHidden:NO];
}
/**代理**/
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    int page = scrollView.contentOffset.x/scrollView.bounds.size.width;
    _currenPageControl.currentPage = page;
}
/**进入APP代理**/
-(void)enterApp{
    if ([[jesseGuideUIConfig initShareManager] exportCurrenUpdateMark]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"温馨提示" message:[NSString stringWithFormat:@"此次更新App内容为%@",self.appStorage] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *sureAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[UIApplication sharedApplication] openURL:self.downUrl];
        }];
        [alert addAction:sureAction];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        BET_CONFIG.userSettingData.hasStep = YES;
        [BET_CONFIG.userSettingData save];
        if (!JesseAppdelegate.isfinishHomeLoad) {
            [JesseAppdelegate addLoadFinishObserver];
            [JesseAppdelegate setFinishBlock:^{
                self.enterBlock();
            }];
            [SVProgressHUD dismiss];
            [SVProgressHUD showWithStatus:@"加载中..."];
            return;
        }
        [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self removeFromParentViewController];
        self.enterBlock();
    
    }
}
/**判断当前字体是否下载过**/
-(BOOL)isFontThisName:(NSString *)FontName
{
    UIFont *Nfont = [UIFont fontWithName:FontName size:13];
    if (Nfont&&([Nfont.fontName compare:FontName]==NSOrderedSame||[Nfont.familyName compare:FontName]==NSOrderedSame)) {
        return YES;
    }else{
        return NO;
    }
}

@end
