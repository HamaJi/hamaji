//
//  Bet365OpenTimeCell.m
//  Bet365
//
//  Created by luke on 2019/5/28.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "Bet365OpenTimeCell.h"

@interface Bet365OpenTimeCell ()
@property (weak, nonatomic) IBOutlet UILabel *openTimeLb;

@end

@implementation Bet365OpenTimeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)openTimeCode:(NSString *)code json:(NSDictionary *)openTime
{
    @weakify(self);
    [[openTime allKeys] enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        @strongify(self);
        if ([code isEqualToString:obj]) {
            self.openTimeLb.text = openTime[obj];
        }
    }];
}

@end
