//
//  DragLaunchItem.m
//  Bet365
//
//  Created by HHH on 2018/8/9.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "DragLaunchItem.h"

@implementation DragLaunchItem
+(instancetype)creatLaunchItemWithTitile:(NSString *)titile Image:(UIImage *)img Handle:(DragLaunchTapHandle)handle{
    DragLaunchItem *item = [DragLaunchItem new];
    item.titile = titile;
    item.image = img;
    item.tapHandle = handle;
    return item;
}

@end
