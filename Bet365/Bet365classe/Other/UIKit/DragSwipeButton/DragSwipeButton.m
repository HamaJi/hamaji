//
//  DragSwipeButton.m
//  Bet365
//
//  Created by HHH on 2018/8/9.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "DragSwipeButton.h"
#define DragBtnSize CGSizeMake(70, 70)
#define DragBtnMinification (3.0 / 5.0)
#define DragBetweenSpacing DragBtnSize.height / 2.0
#define SUPERVIEW_SIZE (self.superview.bounds.size)

static float const animationDuration = 0.3f;
typedef NS_ENUM(NSInteger, DragTranslationHorizantol) {
    /** 左**/
    DragTranslationHorizantol_LEFT = 1,
    /** 右**/
    DragTranslationHorizantol_RIGHT = 2,
};
@interface DragSwipeButton()<UIGestureRecognizerDelegate,CAAnimationDelegate>{

}
#pragma makr - UI
@property (nonatomic,strong)NSMutableArray <UIButton *>*buttonList;
@property (nonatomic,strong)UIButton *iconBtn;
@property (nonatomic,strong)UIImageView *marskImg;


@property (nonatomic,strong)NSArray <DragLaunchItem *>* items;
@property (nonatomic)NSUInteger lumpCount;
@property (nonatomic,assign)DragTranslationHorizantol tanslation;

/**
 自旋动画
 */
@property (nonatomic,strong)CABasicAnimation *revolveAnimation;

/**
 平移伸展动画集合
 */
@property (nonatomic,strong)NSMutableArray <CABasicAnimation *>* postionLaunchAnimationList;

/**
 平移收缩动画集合
 */
@property (nonatomic,strong)NSMutableArray <CABasicAnimation *>* postionCloseAnimationList;
@end
@implementation DragSwipeButton
-(instancetype)initWithPoint:(CGPoint)point DragLaunchItemList:(NSArray <DragLaunchItem *>*)items{
//    CGPoint cachePoint = CGPointMake(BET_CONFIG.userSettingData.dragSwipeOriginX ? BET_CONFIG.userSettingData.dragSwipeOriginX - DragBtnSize.width * 0.5 : point.x,
//
//        BET_CONFIG.userSettingData.dragSwipeOriginY ? BET_CONFIG.userSettingData.dragSwipeOriginY - DragBtnSize.height * 0.5: point.y);
//    CGPoint cachePoint = CGPointMake(BET_CONFIG.userSettingData.dragSwipeOriginX,
//                                     BET_CONFIG.userSettingData.dragSwipeOriginY);
    if (self = [super initWithFrame:CGRectMake(point.x,
                                               point.y,
                                               DragBtnSize.width,
                                               DragBtnSize.height)]) {
        
        self.items = items;
        [self setUI];
        [self addSwipeObserver];
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0.5 animations:^{
                self.center = [self updateCenter:point];
            }];
        });
    }
    return self;
}

-(void)addSwipeObserver{
    [[RACObserve(self,superview) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self addGestureRecognizer];
        });
    }];
}
-(void)addGestureRecognizer{
    [self removeAllPanGestures];
    self.userInteractionEnabled = YES;
    @weakify(self);
    [self addPanGestureRecognizer:^(UIPanGestureRecognizer *recognizer, NSString *gestureId) {
        @strongify(self);
        [self handlePanGesture:recognizer];
    }];
}
-(void)setUI{
    
    UIImageView *bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dragSwipeBtnBg"]];
    [self addSubview:bg];
    [bg.layer setAnchorPoint:CGPointMake(108.0/241.0, 108.0/247.0)];
    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(DragBtnSize.width);
        make.height.equalTo(bg.mas_width).multipliedBy(241.0/247.0);
        make.center.equalTo(self);
    }];
    
    [self.iconBtn addTarget:self action:@selector(tapAction:) forControlEvents:UIControlEventTouchUpInside];
    
}

#pragma mark - Public
-(void)luanchItems{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self removeAllPanGestures];
        self.iconBtn.selected = YES;
        self.iconBtn.enabled = NO;
        [self.superview insertSubview:self.marskImg atIndex:[self.superview.subviews indexOfObject:self]];
        self.marskImg.alpha = 0;
        [UIView animateWithDuration:animationDuration animations:^{
            self.iconBtn.transform = CGAffineTransformMakeRotation(M_PI * 1.25);
            self.marskImg.alpha = 1;
        } completion:^(BOOL finished) {
            self.iconBtn.enabled = YES;
        }];
        [self.buttonList enumerateObjectsUsingBlock:^(UIButton * _Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
            [button.layer addAnimation:self.postionLaunchAnimationList[idx] forKey:@"DRAG_PO_LAU"];
            [button.layer addAnimation:self.revolveAnimation forKey:@"DRAG_REV"];
        }];
    });
}
-(void)closeItems{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.iconBtn.selected = NO;
        self.iconBtn.enabled = NO;
        
        [UIView animateWithDuration:animationDuration animations:^{
            self.iconBtn.transform = CGAffineTransformIdentity;
            self.marskImg.alpha = 0;
        } completion:^(BOOL finished) {
            [self addGestureRecognizer];
            [self cleamItems];
            [self.marskImg removeFromSuperview];
        }];
        [self.buttonList enumerateObjectsUsingBlock:^(UIButton * _Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
            [button.layer addAnimation:self.postionCloseAnimationList[idx] forKey:@"DRAG_PO_CLO"];
            [button.layer addAnimation:self.revolveAnimation forKey:@"DRAG_REV"];
        }];
    });
}

#pragma mark - CAAnimationDelegate
-(void)animationDidStart:(CAAnimation *)anim{
    
}
-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    if (flag) {
        [self.buttonList enumerateObjectsUsingBlock:^(UIButton * _Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
            CGPoint toPoint = arcPoint([self getProgressAtIndex:idx]);
            button.center = CGPointMake(self.center.x + toPoint.x, self.center.y + toPoint.y);
            [button.layer removeAllAnimations];
        }];
    }
}

#pragma mark - Private
-(float)getProgressAtIndex:(NSUInteger)idx{
    float progress = 0.0;
    float total = (0.5 / (1.0*self.lumpCount));
    if (self.tanslation == DragTranslationHorizantol_LEFT) {
        if (self.items.count == 0) {
            progress =  0.5 / 2.0;
        }else if (self.items.count <= 3) {
            
            progress =  total * (idx + 1);
        }else{
            progress =  total * idx;
        }
    }else if(self.tanslation == DragTranslationHorizantol_RIGHT){
        if (self.items.count == 0) {
            progress =  1.0 - 0.5 / 2.0;
        }else if (self.items.count <= 3) {
            progress =  1.0 - total * (idx + 1);
        }else{
            progress =  1.0 - total * idx;
        }
    }
    return progress;
}
-(void)cleamItems{
    self.iconBtn.enabled = YES;
    [self.buttonList enumerateObjectsUsingBlock:^(UIButton * _Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
        [button.layer removeAllAnimations];
        [button removeFromSuperview];
    }];
    self.buttonList = nil;
    self.postionLaunchAnimationList = nil;
    self.postionCloseAnimationList = nil;
    
}
-(CGPoint)updateCenter:(CGPoint)center{
    CGFloat spacing = fabs(arcPoint([self getProgressAtIndex:0]).y) + DragBtnSize.height * DragBtnMinification;
    CGPoint recognizePoint = CGPointMake(center.x, center.y - STATUSAND_NAVGATION_HEIGHT);
    CGPoint stopPoint;
    if (recognizePoint.x < SUPERVIEW_SIZE.width / 2.0) {
        
        if (recognizePoint.y <= SUPERVIEW_SIZE.height/2.0) {
            //左上
            self.tanslation = DragTranslationHorizantol_LEFT;
            CGFloat y = recognizePoint.y - spacing > 0 ? recognizePoint.y + STATUSAND_NAVGATION_HEIGHT : STATUSAND_NAVGATION_HEIGHT + spacing;
            stopPoint = CGPointMake(self.frame.size.width/2.0,y);
        }else{
            //左下
            self.tanslation = DragTranslationHorizantol_LEFT;
            CGFloat y = recognizePoint.y + STATUSAND_NAVGATION_HEIGHT + spacing >= SUPERVIEW_SIZE.height ? SUPERVIEW_SIZE.height - spacing : recognizePoint.y + STATUSAND_NAVGATION_HEIGHT;
            stopPoint = CGPointMake(self.frame.size.width/2.0,y);
        }
    }else{
        if (recognizePoint.y <= SUPERVIEW_SIZE.height/2.0) {
            //右上
            self.tanslation = DragTranslationHorizantol_RIGHT;
            CGFloat y = recognizePoint.y - spacing > 0 ? recognizePoint.y + STATUSAND_NAVGATION_HEIGHT : STATUSAND_NAVGATION_HEIGHT + spacing;
            stopPoint = CGPointMake(SUPERVIEW_SIZE.width - self.frame.size.width/2.0,y);
        }else{
            //右下
            self.tanslation = DragTranslationHorizantol_RIGHT;
            CGFloat y = recognizePoint.y + STATUSAND_NAVGATION_HEIGHT + spacing >= SUPERVIEW_SIZE.height ? SUPERVIEW_SIZE.height - spacing : recognizePoint.y + STATUSAND_NAVGATION_HEIGHT;
            stopPoint = CGPointMake(SUPERVIEW_SIZE.width - self.frame.size.width/2.0,y);
        }
    }
    
    //超出屏幕下边缘
    
    if (stopPoint.y + self.frame.size.height >= SUPERVIEW_SIZE.height) {
        
        stopPoint = CGPointMake(stopPoint.x,
                                SUPERVIEW_SIZE.height - self.frame.size.height/2.0 - spacing);
    }
    if (stopPoint.x - self.frame.size.width/2.0 <= 0) {
        stopPoint = CGPointMake(self.frame.size.width/2.0, stopPoint.y);
    }
    if (stopPoint.x + self.frame.size.width/2.0 >= SUPERVIEW_SIZE.width) {
        stopPoint = CGPointMake(SUPERVIEW_SIZE.width - self.frame.size.width/2.0, stopPoint.y);
    }
    if (stopPoint.y - self.frame.size.height/2.0 <= 0) {
        stopPoint = CGPointMake(stopPoint.x,
                                self.frame.size.height/2.0 + STATUSAND_NAVGATION_HEIGHT + spacing);
    }
    BET_CONFIG.userSettingData.dragSwipeOriginX = stopPoint.x;
    BET_CONFIG.userSettingData.dragSwipeOriginY = stopPoint.y;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [BET_CONFIG.userSettingData save];
    });
    return stopPoint;
}
#pragma mark - Event
-(void)tapAction:(UIButton *)sender{
    if (!sender.selected) {
        [self luanchItems];
    }else{
        [self closeItems];
    }
}
-(void)tapItemsAction:(UIButton *)sender{
    if (self.items[sender.tag].tapHandle && self.items[sender.tag].tapHandle(sender.tag)) {
        [self closeItems];
    }
}
- (void)handlePanGesture:(UIPanGestureRecognizer *)recognizer
{
    //移动状态
    UIGestureRecognizerState recState =  recognizer.state;
    
    switch (recState) {
        case UIGestureRecognizerStateBegan:
            
            break;
        case UIGestureRecognizerStateChanged:
        {
            CGPoint translation = [recognizer translationInView:self.superview];
            recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x, recognizer.view.center.y + translation.y);
            NSLog(@"%@",NSStringFromCGPoint(recognizer.view.center));
            
        }
            break;
        case UIGestureRecognizerStateEnded:
        {
            
            [UIView animateWithDuration:0.5 animations:^{
                recognizer.view.center = [self updateCenter:recognizer.view.center];
            }];
        }
            break;
            
        default:
            break;
    }
    
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.superview];
}
#pragma mark - GET/SET

-(CABasicAnimation *)revolveAnimation{
    if (!_revolveAnimation) {
        _revolveAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        _revolveAnimation.fromValue = [NSNumber numberWithFloat:0.f];
        _revolveAnimation.toValue = [NSNumber numberWithFloat: M_PI *2.0];
        _revolveAnimation.removedOnCompletion = NO;
        _revolveAnimation.duration = animationDuration;
        _revolveAnimation.fillMode = kCAFillModeForwards;
    }
    return _revolveAnimation;
}

-(NSMutableArray<CABasicAnimation *> *)postionLaunchAnimationList{
    if (!_postionLaunchAnimationList) {
        _postionLaunchAnimationList = [NSMutableArray array];
        [self.buttonList enumerateObjectsUsingBlock:^(UIButton * _Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
            CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
            animation.fromValue = [NSValue valueWithCGPoint:button.layer.position];
            CGPoint toPoint = arcPoint([self getProgressAtIndex:idx]);
            animation.toValue = [NSValue valueWithCGPoint:CGPointMake(self.center.x + toPoint.x, self.center.y + toPoint.y)];
            animation.duration = animationDuration;
            animation.removedOnCompletion = NO;
            animation.fillMode = kCAFillModeForwards;
            animation.delegate = self;
            [_postionLaunchAnimationList addObject:animation];
        }];
        
    }
    return _postionLaunchAnimationList;
}

-(NSMutableArray<CABasicAnimation *> *)postionCloseAnimationList{
    if (!_postionCloseAnimationList) {
        _postionCloseAnimationList = [NSMutableArray array];
        [self.buttonList enumerateObjectsUsingBlock:^(UIButton * _Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
            CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
            CGPoint toPoint = arcPoint([self getProgressAtIndex:idx]);
            animation.fromValue = [NSValue valueWithCGPoint:CGPointMake(self.center.x + toPoint.x, self.center.y + toPoint.y)];
            animation.toValue = [NSValue valueWithCGPoint:self.layer.position];
            animation.duration = animationDuration;
            animation.removedOnCompletion = NO;
            animation.fillMode = kCAFillModeForwards;
            animation.delegate = self;
            
            [_postionCloseAnimationList addObject:animation];
        }];
    }
    return _postionCloseAnimationList;
}
-(NSMutableArray<UIButton *> *)buttonList{
    if (!_buttonList) {
        _buttonList = [NSMutableArray array];
        [self.items enumerateObjectsUsingBlock:^(DragLaunchItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.clipsToBounds = YES;
            btn.size = CGSizeMake(DragBtnSize.width*DragBtnMinification,
                                  DragBtnSize.height*DragBtnMinification);
            [btn.layer setAnchorPoint:CGPointMake(0.5,0.5)];
            btn.layer.cornerRadius = btn.size.height / 2.0;
            btn.center = self.center;
            NSInteger containIndex = [self.superview.subviews indexOfObject:self];
            [self.superview insertSubview:btn atIndex:containIndex];
            [btn setImage:obj.image forState:UIControlStateNormal];
            [btn setTitle:obj.titile forState:UIControlStateNormal];
            [btn.titleLabel setFont:[UIFont systemFontOfSize:9]];
            [btn setTitleColor:COLOR_WITH_HEX(0xffffff) forState:UIControlStateNormal];
            [btn setImagePosition:LXMImagePositionTop spacing:3];
            [_buttonList addObject:btn];
            btn.tag = idx;
            [btn setBackgroundImage:[UIImage imageWithColor:COLOR_WITH_HEX(0xEB4C5C)] forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(tapItemsAction:) forControlEvents:UIControlEventTouchUpInside];
        }];
    }
    return _buttonList;
}

-(NSUInteger)lumpCount{
    if (self.items.count <= 3) {
        return (self.items.count + 1);
    }else{
        return (self.items.count - 1);
    }
}
-(UIButton *)iconBtn{
    if (!_iconBtn) {
        _iconBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_iconBtn setImage:[UIImage imageNamed:@"dragSwipCustom"] forState:UIControlStateNormal];
        [self addSubview:_iconBtn];
        [_iconBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self);
            make.size.mas_equalTo(DragBtnSize);
        }];
    }
    return _iconBtn;
}
-(UIImageView *)marskImg{
    if(!_marskImg){
        _marskImg = [[UIImageView alloc] init];
        [_marskImg setBackgroundColor:COLOR_WITH_HEX_ALP(0x000000, 0.5)];
        _marskImg.frame = CGRectMake(0, 0, WIDTH, HEIGHT);
        _marskImg.userInteractionEnabled = YES;
        @weakify(self);
        [_marskImg addTapGestureRecognizer:^(UITapGestureRecognizer *recognizer, NSString *gestureId) {
            @strongify(self);
            [self closeItems];
        }];
    }
    return _marskImg;
}
#pragma mark ======================================
CGPoint itemPoin(CGPoint dragPoint){
    CGFloat x = dragPoint.x + (DragBtnSize.width - DragBtnSize.width*DragBtnMinification) / 2.0;
    CGFloat y = dragPoint.y + (DragBtnSize.height - DragBtnSize.height*DragBtnMinification) / 2.0;
    return CGPointMake(x, y);
}

CGPoint arcPoint(float progress){
    CGFloat angle = M_PI*2.0*progress;
    float radius = (DragBtnSize.width + DragBtnSize.width*DragBtnMinification) / 2.0 + DragBetweenSpacing;
    int index = (angle)/M_PI_2;//第X象限
    float needAngle = angle - index*M_PI_2;
    float x = 0,y = 0;
    switch (index) {
        case 0:
            NSLog(@"第一象限");
            x = radius + sinf(needAngle)*radius;
            y = radius - cosf(needAngle)*radius;
            break;
        case 1:
            NSLog(@"第二象限");
            x = radius + cosf(needAngle)*radius;
            y = radius + sinf(needAngle)*radius;
            break;
        case 2:
            NSLog(@"第三象限");
            x = radius - sinf(needAngle)*radius;
            y = radius + cosf(needAngle)*radius;
            break;
        case 3:
            NSLog(@"第四象限");
            x = radius - cosf(needAngle)*radius;
            y = radius - sinf(needAngle)*radius;
            break;
            
        default:
            return CGPointMake(0.0, -radius);
    }
    x -= radius;
    y -= radius;
    return  CGPointMake(x, y);
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


@end
