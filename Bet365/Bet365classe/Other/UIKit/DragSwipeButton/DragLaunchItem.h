//
//  DragLaunchItem.h
//  Bet365
//
//  Created by HHH on 2018/8/9.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
/** 缓存的Block */
typedef BOOL(^DragLaunchTapHandle)(NSUInteger idx);
@interface DragLaunchItem : NSObject
@property (nonatomic,strong)UIImage *image;
@property (nonatomic,strong)NSString *titile;
@property (nonatomic,copy)DragLaunchTapHandle tapHandle;

+(instancetype)creatLaunchItemWithTitile:(NSString *)titile Image:(UIImage *)img Handle:(DragLaunchTapHandle)handle;
@end
