//
//  DragSwipeButton.h
//  Bet365
//
//  Created by HHH on 2018/8/9.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DragLaunchItem.h"
@interface DragSwipeButton : UIView

-(instancetype)initWithPoint:(CGPoint)point DragLaunchItemList:(NSArray <DragLaunchItem *>*)items;

-(void)luanchItems;

-(void)closeItems;

@end
