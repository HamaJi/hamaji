//
//  MarqueeView.h
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/18.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class MarqueeView;

@protocol MarqueeViewDataSource <NSObject>

@required
-(NSUInteger)numberOfItemFromMarqueeView:(MarqueeView *)view;

@optional
-(CGFloat)heightOfItemFromMarqueeView:(MarqueeView *)view AtIndex:(NSUInteger)index;

-(UITableViewCell *)marqueeView:(MarqueeView *)view MachiningCell:(UITableViewCell *)machiningCell AtIndex:(NSUInteger)index;

-(UIColor *)colorOfItemFromMarqueeView:(MarqueeView *)view AtIndex:(NSUInteger)index;

-(NSAttributedString *)attributedTitileOfItemFromMarqueeView:(MarqueeView *)view AtIndex:(NSUInteger)index;

@end

@interface MarqueeView : UIView

@property (nonatomic,weak)IBOutlet id<MarqueeViewDataSource> dataSource;

@property (nonatomic,weak) Class cellClass;

-(void)marqueeStartAnimation;

-(void)marqueeStopAnimation;

@end

NS_ASSUME_NONNULL_END
