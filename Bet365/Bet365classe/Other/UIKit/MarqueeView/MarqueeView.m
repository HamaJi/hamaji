//
//  MarqueeView.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/18.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "MarqueeView.h"
#import "InfiniteScrollTableView.h"
#import <pop/POP.h>

@interface MarqueeCell : UITableViewCell

@property (nonatomic,strong) UILabel *titileLabel;

@end

@implementation MarqueeCell

-(UILabel *)titileLabel{
    if (!_titileLabel) {
        _titileLabel = [UILabel setAllocLabelWithText:@"" FontOfSize:13 rgbColor:0x000000];
        _titileLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_titileLabel];
        [_titileLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView);
        }];
    }
    return _titileLabel;
}

@end

@interface MarqueeView ()
<UITableViewDelegate,
UITableViewDataSource>

@property (nonatomic,assign) NSUInteger srollIdx;

@property (nonatomic,strong) InfiniteScrollTableView *tableView;

@property (nonatomic,assign) BOOL scrolling;

@end

@implementation MarqueeView


-(instancetype)init{
    if (self = [super init]) {
        [self addObserver];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self addObserver];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addObserver];
    }
    return self;
}

-(void)addObserver{
    self.tableView.delegate  = self;
    self.tableView.dataSource = self;
    @weakify(self);
    [[[RACSignal interval:0.01 onScheduler:[RACScheduler mainThreadScheduler]] takeUntil:self.rac_willDeallocSignal] subscribeNext:^(id x) {
        @strongify(self);
        if (self.scrolling) {
            CGPoint contentOffset  = self.tableView.contentOffset;
            contentOffset.y += 0.3;
            [self.tableView setContentOffset:contentOffset];
        }
    }];
}

-(void)marqueeStartAnimation{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self pri_reloadData];
        self.scrolling = YES;
    });
}

-(void)marqueeStopAnimation{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.scrolling = NO;
    });
}
#pragma mark - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(numberOfItemFromMarqueeView:)]) {
        return [self.dataSource numberOfItemFromMarqueeView:self];
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(heightOfItemFromMarqueeView:AtIndex:)]) {
        return [self.dataSource heightOfItemFromMarqueeView:self AtIndex:indexPath.row];
    }
    return 20.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MarqueeCell"];
    cell.contentView.backgroundColor = [UIColor clearColor];
    void(^noneBlock)() = ^(){
        MarqueeCell *mCell = (MarqueeCell *)cell;
        if ([self.dataSource respondsToSelector:@selector(attributedTitileOfItemFromMarqueeView:AtIndex:)]) {
            NSAttributedString *attributed = [self.dataSource attributedTitileOfItemFromMarqueeView:self AtIndex:indexPath.row];
            mCell.titileLabel.attributedText = attributed;
        }
        if ([self.dataSource respondsToSelector:@selector(colorOfItemFromMarqueeView:AtIndex:)]) {
            UIColor *color = [self.dataSource colorOfItemFromMarqueeView:self AtIndex:indexPath.row];
            mCell.backgroundColor = color ? color : [UIColor clearColor];
        }
    };
    if (self.dataSource) {
        if ([self.dataSource respondsToSelector:@selector(marqueeView:MachiningCell:AtIndex:)]) {
            return [self.dataSource marqueeView:self MachiningCell:cell AtIndex:indexPath.row];
        }
    }
    noneBlock();
    return cell;
}

#pragma mark - UITableViewDataSource
-(InfiniteScrollTableView *)tableView{
    if (!_tableView) {
        _tableView = [[InfiniteScrollTableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.scrollEnabled = NO;
        _tableView.userInteractionEnabled = NO;
        [_tableView setShowsVerticalScrollIndicator:NO];
        [_tableView registerClass:MarqueeCell.class forCellReuseIdentifier:@"MarqueeCell"];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.separatorColor = [UIColor clearColor];
        [self addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return _tableView;
}


-(void)reloadData{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(pri_reloadData) object:nil];
    [self performSelector:@selector(pri_reloadData) withObject:nil afterDelay:0.1];
}

-(void)pri_reloadData{
    [self.tableView reloadData];
}

#pragma mark - GET/SET
-(void)setCellClass:(Class)cellClass{
    _cellClass = cellClass;
    [self.tableView registerClass:cellClass forCellReuseIdentifier:@"MarqueeCell"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = UIColor.clearColor;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
