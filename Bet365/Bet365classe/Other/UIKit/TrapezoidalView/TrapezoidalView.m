//
//  TrapezoidalView.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/11.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "TrapezoidalView.h"

@implementation TrapezoidalView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)drawRect:(CGRect)rect{
    CGPoint topLeft = CGPointMake(0 + self.topLeftOffset.marginHorizontal, 0 + self.topLeftOffset.marginVertical);
    CGPoint topRight = CGPointMake(rect.size.width - self.topRightOffset.marginHorizontal, 0 + self.topRightOffset.marginVertical);
    CGPoint bottomLeft = CGPointMake(0 + self.bottomLeftOffset.marginHorizontal, rect.size.height - self.bottomLeftOffset.marginVertical);
    CGPoint bottomRight = CGPointMake(rect.size.width - self.bottomRightOffset.marginHorizontal, rect.size.height - self.bottomRightOffset.marginVertical);
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    
    [shapeLayer setFillColor:[self.backgroundColor CGColor]];
    CGMutablePathRef path = CGPathCreateMutable();
    
    CGPathMoveToPoint(path, nil, topLeft.x, topLeft.y);
    CGPathAddLineToPoint(path, nil, topRight.x, topRight.y);
    CGPathAddLineToPoint(path, nil, bottomRight.x, bottomRight.y);
    CGPathAddLineToPoint(path, nil, bottomLeft.x, bottomLeft.y);
    CGPathAddLineToPoint(path, nil, topLeft.x, topLeft.y);
    
    CGPathCloseSubpath(path);
    [shapeLayer setPath:path];
    CFRelease(path);
    self.layer.mask = shapeLayer;
}

@end
