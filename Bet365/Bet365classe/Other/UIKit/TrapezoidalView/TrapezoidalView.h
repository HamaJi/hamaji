//
//  TrapezoidalView.h
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/11.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef struct TrapezoidalOffset {
    CGFloat marginHorizontal, marginVertical;
} TrapezoidalOffset;

UIKIT_STATIC_INLINE TrapezoidalOffset TrapezoidalOffsetMake(CGFloat marginHorizontal,CGFloat marginVertical) {
    TrapezoidalOffset offset = {marginHorizontal, marginVertical};
    return offset;
}

@interface TrapezoidalView : UIView

@property (nonatomic,assign) TrapezoidalOffset topLeftOffset;

@property (nonatomic,assign) TrapezoidalOffset topRightOffset;

@property (nonatomic,assign) TrapezoidalOffset bottomLeftOffset;

@property (nonatomic,assign) TrapezoidalOffset bottomRightOffset;

@end

NS_ASSUME_NONNULL_END
