//
//  TurnTableView.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/16.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "TurnTableView.h"
#import "TrapezoidalView.h"
#import <pop/POP.h>
#define DEGREES_TO_RADIANS(x) ({\
CGFloat angle = 0.0;\
if (x < 0) {\
    angle = 360.0 + x;\
}else{\
    angle = x;\
}\
(angle * M_PI/180.0);\
})\

@interface NSString (VerticalString)

- (NSString *)verticalString;

@end

@implementation NSString (VerticalString)


- (NSString *)verticalString{
    NSMutableString * str = [[NSMutableString alloc] initWithString:self];
    NSInteger count = str.length;
    for (int i = 1; i < count; i ++) {
        [str insertString:@"\n" atIndex:i*2 - 1];
    }
    return str;
}

@end

@interface TurnTableView()<CAAnimationDelegate>

@property (nonatomic,strong) UIImageView *rotateBgView;

@property (nonatomic,strong) UIView *rotateContenView;

@property (nonatomic,strong) UIButton *subMitBtn;

@property (nonatomic,strong) CADisplayLink *link;

@property (nonatomic,assign) BOOL scrolling;

@property (nonatomic,copy) void(^rotationCompleted)();

@end

@implementation TurnTableView

-(instancetype)init{
    if (self = [super init]) {
        [self setUI];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self setUI];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setUI];
    }
    return self;
}

-(void)reloadData{
    if (!self.dataSource) {
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
//        [self.rotateContenView.layer removeAllSublayers];
//        [self.rotateContenView removeAllSubviews];
        NSUInteger count = 0;
        if ([self.dataSource respondsToSelector:@selector(turnTableSubViewCount)]) {
            count = [self.dataSource turnTableSubViewCount];
        }
        if (!count) {
            return;
        }
        CGFloat radius = self.rotateContenView.bounds.size.width / 2.0;
        CGPoint center = CGPointMake(self.rotateContenView.bounds.size.width / 2.0, self.rotateContenView.bounds.size.height / 2.0);
        for (int i = 0; i < count; i++) {
            CGFloat beginAngle = 0.0;
            if ([self.dataSource respondsToSelector:@selector(turnTableSubViewBeginAngleFrom:)]) {
                beginAngle = [self.dataSource turnTableSubViewBeginAngleFrom:i];
            }
            CGFloat endAngle = 0.0;
            if ([self.dataSource respondsToSelector:@selector(turnTableSubViewEndAngleFrom:)]) {
                endAngle = [self.dataSource turnTableSubViewEndAngleFrom:i];
            }
            
            UIColor *color = [UIColor skinViewKitSelColor];
            if ([self.dataSource respondsToSelector:@selector(turnTableSubViewColorFrom:)]) {
                color = [self.dataSource turnTableSubViewColorFrom:i];
            }

            CAShapeLayer *layer = [CAShapeLayer layer];
            
            UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:center radius:radius startAngle:DEGREES_TO_RADIANS(beginAngle - 90) endAngle:DEGREES_TO_RADIANS(endAngle - 90) clockwise:YES];
            [path addLineToPoint:center];
            [path closePath];
            layer.path= path.CGPath;
            layer.fillColor= color.CGColor;
            layer.strokeColor= [UIColor clearColor].CGColor;
            [self.rotateContenView.layer addSublayer:layer];
            
            NSString *titile = [@"" verticalString];
            if ([self.dataSource respondsToSelector:@selector(turnTableSubViewTextTitileFrom:)]) {
                titile = [[[self.dataSource turnTableSubViewTextTitileFrom:i] stringByAppendingString:@"      "] verticalString];
            }

            UIFont *font = [UIFont systemFontOfSize:15];
            if ([self.dataSource respondsToSelector:@selector(turnTableSubViewTextFontFrom:)]) {
                font = [self.dataSource turnTableSubViewTextFontFrom:i];
            }

            UIColor *textColor = [UIColor whiteColor];
            if ([self.dataSource respondsToSelector:@selector(turnTableSubViewTextColorFrom:)]) {
                textColor = [self.dataSource turnTableSubViewTextColorFrom:i];
            }
            
            UILabel *label = [[UILabel alloc] init];
            label.font = font;
            label.layer.anchorPoint = CGPointMake(0.5, 1);
            label.text = titile;
            label.numberOfLines = 0;
            label.textAlignment = NSTextAlignmentLeft;
            label.textColor = textColor;
            [label sizeToFit];
            [self.rotateContenView addSubview:label];

            label.size = CGSizeMake(font.pointSize, radius);
            CGFloat between = endAngle - beginAngle;
            label.center = center;
            label.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(beginAngle + between / 2.0));
            NSLog(@"");
        }
    });
}

-(void)transform:(CGFloat)angle Completed:(void(^)())completed{
    if (self.scrolling) {
        return;
    }
    self.scrolling = YES;
    self.rotationCompleted = completed;
    if (_link) {
        [self.link removeFromRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
        self.link = nil;
    }
    CGFloat mAngle = 360.0 - angle;
    
    CGFloat perAngle = M_PI/180.0;
    
    NSInteger turnsNum = arc4random()%4+2;//控制圈数
    CGFloat rotation = mAngle * perAngle + 360 * perAngle * turnsNum;
    
    CABasicAnimation* rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat:rotation];
    rotationAnimation.duration = 5.0;
    rotationAnimation.cumulative = YES;
    rotationAnimation.delegate = self;
    //由快变慢
    rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    rotationAnimation.fillMode=kCAFillModeForwards;
    rotationAnimation.removedOnCompletion = NO;
    [self.rotateContenView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    
    [self.link addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];

}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    self.scrolling = NO;
    self.rotationCompleted ? self.rotationCompleted() : nil;
    [self.link removeFromRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    self.link = nil;
}
#pragma mark - Private
-(void)setUI{
    self.enable = YES;
    self.backgroundColor = [UIColor whiteColor];
    
    [self addSubview:self.rotateBgView];
    [self.rotateBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    
    [self addSubview:self.rotateContenView];
    [self.rotateContenView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self).offset(15);
        make.bottom.right.equalTo(self).offset(-15);
    }];
    
    [self addSubview:self.subMitBtn];
    [self.subMitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.subMitBtn.mas_height).multipliedBy(170.0 / 225.0);
        make.centerX.equalTo(self);
        make.centerY.equalTo(self).offset(-10);
        make.height.mas_equalTo(60);
    }];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.clipsToBounds = YES;
        self.cornerRadius = self.bounds.size.width / 2.0;
        
        self.rotateContenView.clipsToBounds = YES;
        self.rotateContenView.cornerRadius = self.rotateContenView.bounds.size.width / 2.0;
        
        [self reloadData];
        
    });
}


-(CGPoint)calcCircleCoordinateWithAngle : (CGFloat) angle andWithRadius: (CGFloat) radius{
    CGFloat x2 = radius*cosf(DEGREES_TO_RADIANS(angle));
    CGFloat y2 = radius*sinf(DEGREES_TO_RADIANS(angle));
    CGPoint center = CGPointMake(self.rotateContenView.bounds.size.width / 2.0, self.rotateContenView.bounds.size.height / 2.0);
    return CGPointMake(center.x-x2, center.y-y2);
}

#pragma mark - Events
-(void)submitAction:(UIButton *)sender{
    if (self.enable && !self.scrolling && self.delegate && [self.delegate respondsToSelector:@selector(didTapTransformTurnTableView:)]) {
        [self.delegate didTapTransformTurnTableView:self];
    }
}

-(void)updateDisplayLink{
//    CABasicAnimation *anim = [self.rotateContenView.layer animationForKey:@"rotationAnimation"];
//    NSLog(@"toValue = %@",anim.fromValue);
}
#pragma mark - GET/SET
-(UIImageView *)rotateBgView{
    if (!_rotateBgView) {
        _rotateBgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"turntable_canva"]];
    }
    return _rotateBgView;
}
-(UIView *)rotateContenView{
    if (!_rotateContenView) {
        _rotateContenView = [[UIView alloc] init];
        
    }
    return _rotateContenView;
}

-(UIButton *)subMitBtn{
    if (!_subMitBtn) {
        _subMitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_subMitBtn setImage:[UIImage imageNamed:@"turntable_hand_icon"] forState:UIControlStateNormal];
        [_subMitBtn addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _subMitBtn;
}

-(CADisplayLink *)link{
    if (!_link) {
        _link = [CADisplayLink displayLinkWithTarget:self selector:@selector(updateDisplayLink)];
        _link.frameInterval = 60.0 / 19.0;
    }
    return _link;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
