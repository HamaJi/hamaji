//
//  TurnTableView.h
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/16.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class TurnTableView;

@protocol TurnTableViewDataSorce <NSObject>

-(NSUInteger)turnTableSubViewCount;

-(CGFloat)turnTableSubViewBeginAngleFrom:(NSUInteger)index;

-(CGFloat)turnTableSubViewEndAngleFrom:(NSUInteger)index;

-(NSString *)turnTableSubViewTextTitileFrom:(NSUInteger)index;

-(UIFont *)turnTableSubViewTextFontFrom:(NSUInteger)index;

-(UIColor *)turnTableSubViewTextColorFrom:(NSUInteger)index;

-(UIColor *)turnTableSubViewColorFrom:(NSUInteger)index;

@end

@protocol TurnTableViewDelegate <NSObject>

-(void)didTapTransformTurnTableView:(TurnTableView *)view;

@end

@interface TurnTableView : UIView

@property (nonatomic,weak) id<TurnTableViewDataSorce> dataSource;

@property (nonatomic,weak) id<TurnTableViewDelegate> delegate;

@property (nonatomic,assign) BOOL enable;
-(void)reloadData;

-(void)transform:(CGFloat)angle Completed:(void(^)())completed;



@end

NS_ASSUME_NONNULL_END
