//
//  BETAttributedTextView.m
//  Bet365
//
//  Created by luke on 2018/10/31.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "BETAttributedTextView.h"
#import <DTAnimatedGIF.h>

@interface BETAttributedTextView ()
<DTAttributedTextContentViewDelegate,
DTLazyImageViewDelegate>
@property (nonatomic,copy) NSString *html;
@property (nonatomic, assign) CGRect viewMaxRect;
@property (nonatomic,strong) DTAttributedTextView *attributedTextView;
@end

@implementation BETAttributedTextView

+ (instancetype)attributedTextViewWithMaxRect:(CGRect)viewMaxRect Html:(NSString *)html
{
    BETAttributedTextView *textView = [[BETAttributedTextView alloc] init];
    textView.viewMaxRect = viewMaxRect;
    [textView setNoticeHtml:html];
    return textView;
}

- (void)setNoticeHtml:(NSString *)noticeContent
{
    NSArray *resultArr = [noticeContent regularExpressionWithPattern:@"style=\"width.*?\""];
    __block NSMutableString *str = [[NSMutableString alloc] initWithString:noticeContent];
    [resultArr enumerateObjectsUsingBlock:^(NSTextCheckingResult * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [str deleteCharactersInRange:obj.range];
    }];
    self.html = str;
    self.attributedTextView.attributedString = [self getAttributedStringWithHtml:self.html];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]){
        [self addSubview:self.attributedTextView];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.attributedTextView.frame = self.bounds;
    CGSize textSize = [self getAttributedTextHeightHtml:self.html with_viewMaxRect:self.viewMaxRect];
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.sizeBlock){
            self.sizeBlock(textSize);
        }
    });
}

//处理图片
- (UIView *)attributedTextContentView:(DTAttributedTextContentView *)attributedTextContentView viewForAttachment:(DTTextAttachment *)attachment frame:(CGRect)frame{
    if([attachment isKindOfClass:[DTImageTextAttachment class]]){
        NSString *imageURL = [NSString stringWithFormat:@"%@", attachment.contentURL];
        DTLazyImageView *imageView = [[DTLazyImageView alloc] initWithFrame:frame];
        imageView.delegate = self;
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.image = [(DTImageTextAttachment *)attachment image];
        imageView.url = attachment.contentURL;
        if ([imageURL containsString:@"gif"]) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSData *gifData = [NSData dataWithContentsOfURL:attachment.contentURL];
                dispatch_async(dispatch_get_main_queue(), ^{
                    imageView.image = DTAnimatedGIFFromData(gifData);
                });
            });
        }
        // if there is a hyperlink then add a link button on top of this image
        if (attachment.hyperLinkURL)
        {
            imageView.userInteractionEnabled = YES;
            DTLinkButton *button = [[DTLinkButton alloc] initWithFrame:imageView.bounds];
            button.URL = attachment.hyperLinkURL;
            button.minimumHitSize = CGSizeMake(25, 25); // adjusts it's bounds so that button is always large enough
            button.GUID = attachment.hyperLinkGUID;
            [button addTarget:self action:@selector(linkPushed:) forControlEvents:UIControlEventTouchUpInside];
            [imageView addSubview:button];
        }
        return imageView;
    }
    return nil;
}

//懒加载获取图片大小
- (void)lazyImageView:(DTLazyImageView *)lazyImageView didChangeImageSize:(CGSize)size {
    NSURL *url = lazyImageView.url;
    CGSize imageSize = size;
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"contentURL == %@", url];
    BOOL didUpdate = NO;
    // update all attachments that match this URL (possibly multiple images with same size)
    for (DTTextAttachment *oneAttachment in [self.attributedTextView.attributedTextContentView.layoutFrame textAttachmentsWithPredicate:pred])
    {
        // update attachments that have no original size, that also sets the display size
        if (CGSizeEqualToSize(oneAttachment.originalSize, CGSizeZero))
        {
            oneAttachment.originalSize = imageSize;
            [self configNoSizeImageView:url.absoluteString size:imageSize];
            didUpdate = YES;
        }
    }
    
    //    if (didUpdate){
    //        self.attributedTextView.attributedTextContentView.layouter = nil;
    //        [self.attributedTextView relayoutText];
    //    }
}


// 字符串中一些图片没有宽高，需要这里拿到宽高后再去reload
- (void)configNoSizeImageView:(NSString *)url size:(CGSize)size
{
    CGFloat imgSizeScale = size.height/size.width;
    CGFloat widthPx = _viewMaxRect.size.width;
    CGFloat heightPx = widthPx * imgSizeScale;
    NSString *imageInfo = [NSString stringWithFormat:@"src=\"%@\"",url];
    NSString *sizeString = [NSString stringWithFormat:@" style=\"width:%.fpx; height:%.fpx;\"",widthPx,heightPx];
    NSString *newImageInfo = [NSString stringWithFormat:@"src=\"%@\"%@",url,sizeString];
    
    if ([self.html containsString:imageInfo]) {
        NSString *newHtml = [self.html stringByReplacingOccurrencesOfString:imageInfo withString:newImageInfo];
        self.html = newHtml;
        CGSize textSize = [self getAttributedTextHeightHtml:self.html with_viewMaxRect:self.viewMaxRect];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.sizeBlock){
                self.sizeBlock(textSize);
            }
        });
        self.attributedTextView.height = textSize.height;
        self.attributedTextView.attributedString = [self getAttributedStringWithHtml:self.html];
        [self.attributedTextView relayoutText];
    }
}

- (void)linkPushed:(DTLinkButton *)button
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(AttributedTextViewLinkPushedWithLink:)]) {
        [self.delegate AttributedTextViewLinkPushedWithLink:[NSString stringWithFormat:@"%@",button.URL]];
    }
}

#pragma mark - private Methods
//使用HtmlString,和最大左右间距，计算视图的高度
- (CGSize)getAttributedTextHeightHtml:(NSString *)htmlString with_viewMaxRect:(CGRect)_viewMaxRect{
    //获取富文本
    NSAttributedString *attributedString =  [self getAttributedStringWithHtml:htmlString];
    //获取布局器
    DTCoreTextLayouter *layouter = [[DTCoreTextLayouter alloc] initWithAttributedString:attributedString];
    NSRange entireString = NSMakeRange(0, [attributedString length]);
    //获取Frame
    DTCoreTextLayoutFrame *layoutFrame = [layouter layoutFrameWithRect:_viewMaxRect range:entireString];
    //得到大小
    CGSize sizeNeeded = [layoutFrame frame].size;
    return sizeNeeded;
}

#pragma mark - private Methods
- (DTAttributedTextView *)attributedTextView{
    if (_attributedTextView == nil) {
        _attributedTextView = [[DTAttributedTextView alloc] initWithFrame:CGRectZero];
        _attributedTextView.textDelegate = self;
    }
    return _attributedTextView;
}

- (NSAttributedString *)getAttributedStringWithHtml:(NSString *)htmlString{
    //获取富文本
    NSData *data = [htmlString dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *options = [NSMutableDictionary dictionary];
    [options setObject:[NSNumber numberWithFloat:IS_IPHONE_5s ? 15 : 17] forKey:DTDefaultFontSize];
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithHTMLData:data options:options documentAttributes:NULL];
    return attributedString;
}

@end
