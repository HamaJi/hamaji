//
//  Bet365NoticeView.h
//  Bet365
//
//  Created by luke on 2018/9/2.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Bet365NoticeView : UIView

+ (void)showWithNoticeTitle:(NSString *)noticeTitle noticeContent:(NSString *)noticeContent;

@end
