//
//  Bet365NoticeView.m
//  Bet365
//
//  Created by luke on 2018/9/2.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Bet365NoticeView.h"
#import "KFCPActivityController.h"
#import "BETAttributedTextView.h"

@interface Bet365NoticeView ()
<AttributedTextViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *titleLb;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *attLbHeightLayout;
@property (weak, nonatomic) IBOutlet UIView *attributedView;
@property (nonatomic,strong) BETAttributedTextView *textView;
@end

@implementation Bet365NoticeView
+ (void)showWithNoticeTitle:(NSString *)noticeTitle noticeContent:(NSString *)noticeContent
{
    Bet365NoticeView *noticeView = [Bet365NoticeView instanceViewFromXib];
    noticeView.frame = [UIApplication sharedApplication].keyWindow.bounds;
    [[UIApplication sharedApplication].keyWindow addSubview:noticeView];
    [noticeView setNoticeTitle:noticeTitle noticeContent:noticeContent];
}

- (void)setNoticeTitle:(NSString *)noticeTitle noticeContent:(NSString *)noticeContent
{
    self.titleLb.text = noticeTitle;
    CGRect viewMaxRect = CGRectMake(0, 0, WIDTH - 70, CGFLOAT_HEIGHT_UNKNOWN);
    @weakify(self);
    self.textView = [BETAttributedTextView attributedTextViewWithMaxRect:viewMaxRect Html:noticeContent];
    self.textView.sizeBlock = ^(CGSize textSize) {
        @strongify(self);
        if (textSize.height > HEIGHT - 250) {
            [UIView animateWithDuration:0.25 animations:^{
                self.attLbHeightLayout.constant = HEIGHT - 250;
            }];
            self.textView.frame = CGRectMake(viewMaxRect.origin.x, viewMaxRect.origin.y, viewMaxRect.size.width, HEIGHT - 250);
        }else{
            [UIView animateWithDuration:0.25 animations:^{
                self.attLbHeightLayout.constant = textSize.height;
            }];
            self.textView.frame = CGRectMake(viewMaxRect.origin.x, viewMaxRect.origin.y, viewMaxRect.size.width, textSize.height);
        }
    };
    self.textView.delegate = self;
    [self.attributedView addSubview:self.textView];
}

#pragma mark - AttributedTextViewDelegate
- (void)AttributedTextViewLinkPushedWithLink:(NSString *)link
{
    [self sureClick:nil];
    if ([link hasPrefix:@"http"]) {
        KFCPActivityController *vc = [[KFCPActivityController alloc] init];
        [vc loadWebBy:link];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }else{
        if ([link containsString:@"game"]) {
            NSCharacterSet* nonDigits =[[NSCharacterSet decimalDigitCharacterSet] invertedSet];
            int remainSecond =[[link stringByTrimmingCharactersInSet:nonDigits] intValue];
            [UIViewController enterLotteryByLotteryId:[NSString stringWithFormat:@"%d",remainSecond]];
        }else{
            KFCPActivityController *vc = [[KFCPActivityController alloc] init];
            [vc loadhongBaoUrl:[NSString stringWithFormat:@"%@%@?_t=%@",SerVer_Url,BET_CONFIG.common_config.yhhdPath,[LukeUserAdapter getNowTimeTimestamp]]];
            [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
            vc.hidesBottomBarWhenPushed = YES;
        }
    }
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    }
    return self;
}

- (IBAction)sureClick:(id)sender {
    [UIView animateWithDuration:0.25 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
