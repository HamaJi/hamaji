//
//  Bet365NoticeModel.m
//  Bet365
//
//  Created by luke on 2018/9/2.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Bet365NoticeModel.h"

@implementation NoticeModel

-(id)mj_newValueFromOldValue:(id)oldValue property:(MJProperty *)property{
    if ([property.name isEqualToString:@"noticeContent"]) {
        if (oldValue) {
            return [((NSString *)oldValue) removeSpaceAndNewline];
        }
    }
    return oldValue;
}

@end


@implementation Bet365NoticeModel

+ (NSDictionary *)mj_objectClassInArray
{
    return @{@"index_notice"    : @"NoticeModel",
             @"roll_notice"     : @"NoticeModel",
             @"login_notice"    : @"NoticeModel",
             @"register_notice" : @"NoticeModel",
             @"lottery_notice"  : @"NoticeModel",
             @"chat_notice"     : @"NoticeModel",
             @"rech_notice"     : @"NoticeModel",
             @"rech_domain"     : @"NoticeModel"
             };
}


@end
