//
//  Bet365NoticeModel.h
//  Bet365
//
//  Created by luke on 2018/9/2.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveObjC/ReactiveObjC.h>


@interface NoticeModel : NSObject
/**  */
@property (nonatomic,assign) NSInteger noticeId;
/**  */
@property (nonatomic,copy) NSString *noticeTitle;
/**  */
@property (nonatomic,copy) NSString *noticeContent;
/**  */
@property (nonatomic,copy) NSString *noticeType;
/**  */
@property (nonatomic,copy) NSString *beginDate;
/**  */
@property (nonatomic,copy) NSString *endDate;
/**  */
@property (nonatomic,copy) NSString *operatorAccount;
/**  */
@property (nonatomic,copy) NSString *addTime;
/**  */
@property (nonatomic,copy) NSString *updateTime;
/**  */
@property (nonatomic,assign) NSInteger status;
/** 是否展开 */
@property (nonatomic,assign) BOOL isNote;
@end

@interface Bet365NoticeModel : NSObject
/**  */
@property (nonatomic,strong) NSArray <NoticeModel *>*index_notice;
/**  */
@property (nonatomic,strong) NSArray <NoticeModel *>*roll_notice;
/**  */
@property (nonatomic,strong) NSArray <NoticeModel *>*login_notice;
/**  */
@property (nonatomic,strong) NSArray <NoticeModel *>*register_notice;
/**  */
@property (nonatomic,strong) NSArray <NoticeModel *>*lottery_notice;

@property (nonatomic,strong) NSArray <NoticeModel *>*chat_notice;

@property (nonatomic,strong) NSArray <NoticeModel *>*rech_notice;

@property (nonatomic,strong) NSArray <NoticeModel *>*rech_domain;

@end
