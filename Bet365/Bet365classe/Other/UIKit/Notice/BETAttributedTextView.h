//
//  BETAttributedTextView.h
//  Bet365
//
//  Created by luke on 2018/10/31.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <DTCoreText.h>

typedef void (^SizeBlock)(CGSize textSize);

@protocol AttributedTextViewDelegate <NSObject>

@optional
- (void)AttributedTextViewLinkPushedWithLink:(NSString *)link;

@end

@interface BETAttributedTextView : UIView

+ (instancetype)attributedTextViewWithMaxRect:(CGRect)viewMaxRect Html:(NSString *)html;

/**  */
@property (nonatomic,copy) SizeBlock sizeBlock;
/** 代理 */
@property (nonatomic,weak) id<AttributedTextViewDelegate>delegate;

@end
