//
//  SwitchSlideButton.m
//  Bet365
//
//  Created by super_小鸡 on 2019/8/11.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "SwitchSlideButton.h"
#import <pop/POP.h>

@interface SwitchSlideButton()

@property (strong, nonatomic) UIImageView *bgImageView;

@property (strong, nonatomic) UIImageView *slideView;

@property (strong, nonatomic) NSMutableArray <UILabel *>*labelList;

@end

@implementation SwitchSlideButton

-(instancetype)init{
    
    if (self = [super init]) {
        [self creatUI];
        [self addObeser];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self creatUI];
        [self addObeser];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self creatUI];
        [self addObeser];
    }
    return self;
}

-(void)setIndex:(NSUInteger)idx Animated:(BOOL)animated{
    if ((idx + 1) > self.list.count) {
        return;
    }
    _idx = idx;
    [self.labelList enumerateObjectsUsingBlock:^(UILabel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj setUserInteractionEnabled:NO];
    }];
    dispatch_async(dispatch_get_main_queue(), ^{
        UILabel *label = [self.labelList safeObjectAtIndex:_idx];
        if (animated) {
            POPSpringAnimation *positionAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionX];
            positionAnimation.toValue = @(label.center.x);
            positionAnimation.springBounciness = 10;
            [positionAnimation setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
                [self.labelList enumerateObjectsUsingBlock:^(UILabel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    [obj setUserInteractionEnabled:YES];
                }];
            }];
            [self.slideView.layer pop_addAnimation:positionAnimation forKey:@"positionAnimation"];
        }else{
            POPBasicAnimation *positionAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerPositionX];
            positionAnimation.toValue = @(label.center.x);
            positionAnimation.duration = 0.0;
            [positionAnimation setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
                [self.labelList enumerateObjectsUsingBlock:^(UILabel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    [obj setUserInteractionEnabled:YES];
                }];
            }];
        }
        [self.labelList enumerateObjectsUsingBlock:^(UILabel * _Nonnull obj, NSUInteger index, BOOL * _Nonnull stop) {
            if (_idx == index) {
                [obj setTextColor:self.titileSelColor];
            }else{
                [obj setTextColor:self.titileColor];
            }
        }];
    });
}

-(void)drawRect:(CGRect)rect{
    self.bgImageView.clipsToBounds = YES;
    self.bgImageView.layer.cornerRadius = rect.size.height / 2.0;
    self.slideView.layer.cornerRadius = rect.size.height / 2.0;
    [self.labelList enumerateObjectsUsingBlock:^(UILabel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.layer.cornerRadius = rect.size.height / 2.0;
    }];
}

-(void)addObeser{
    self.enable = YES;
    @weakify(self);
    
    [[RACObserve(self, slideBackgroundImage) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        dispatch_async(dispatch_get_main_queue(), ^{
            self.slideView.image = x;
        });
    }];
    
}

-(void)creatUI{
    self.bgImageView.backgroundColor = [UIColor whiteColor];
    self.backgroundColor = [UIColor clearColor];
    
}

-(void)setBackgroundColor:(UIColor *)backgroundColor{
    [super setBackgroundColor:[UIColor clearColor]];
    [self.bgImageView setBackgroundColor:backgroundColor];
}
-(UIImageView *)slideView{
    if (!_slideView) {
        _slideView = [[UIImageView alloc] init];
        _slideView.clipsToBounds = YES;
        if (self.subviews.count > 1) {
            [self insertSubview:_slideView atIndex:1];
        }else{
            [self addSubview:_slideView];
        }
        
        [self.slideView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.bottom.equalTo(self);
        }];
    }
    return _slideView;
}
-(UIImage *)slideBackgroundImage{
    if (!_slideBackgroundImage) {
        _slideBackgroundImage = [UIImage createImageWithColor:[UIColor skinViewContentColor]];
    }
    return _slideBackgroundImage;
}


-(void)setList:(NSArray<NSString *> *)list{
    _list = list;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.labelList enumerateObjectsUsingBlock:^(UILabel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [obj removeFromSuperview];
        }];
        [self.labelList removeAllObjects];
        
        
        @weakify(self);
        [_list enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            UILabel *label = [UILabel setAllocLabelWithText:obj FontOfSize:15 rgbColor:0x000000];
            [label setFont:self.font];
            label.textAlignment = NSTextAlignmentCenter;
            label.clipsToBounds = YES;
            label.layer.cornerRadius = self.bounds.size.height / 2.0;
            [self addSubview:label];
            [self.labelList addObject:label];
            label.userInteractionEnabled = YES;
            [label addTapGestureRecognizer:^(UITapGestureRecognizer *recognizer, NSString *gestureId) {
                @strongify(self);
                if (self.enable) {
                    if (self.delegate && [self.delegate respondsToSelector:@selector(tapUpSwitchSlideButton:Index:)]) {
                        if ([self.delegate tapUpSwitchSlideButton:self Index:[gestureId integerValue]]) {
                            [self setIndex:[gestureId integerValue] Animated:YES];
                        }
                    }else{
                        [self setIndex:[gestureId integerValue] Animated:YES];
                    }
                }
            } tapGestureId:[NSString stringWithFormat:@"%li",idx]];
        }];
        
        if (self.labelList.count > 1) {
            [self.labelList mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:0.0 leadSpacing:0.0 tailSpacing:0.0];
            [self.labelList mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.bottom.equalTo(self);
            }];
        }else if (self.labelList.count == 1){
            [self.labelList[0] mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self);
            }];
        }
        
        if (list) {
            [self.slideView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(self.labelList[0].mas_width);
            }];
        }else{
            [self.slideView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(self);
            }];
        }
        [self setIndex:self.idx Animated:NO];
    });
    
}

-(UIColor *)titileColor{
    if (!_titileColor) {
        _titileColor = [UIColor blackColor];
    }
    return _titileColor;
}

-(UIColor *)titileSelColor{
    if (!_titileSelColor) {
        _titileSelColor = [UIColor whiteColor];
    }
    return _titileSelColor;
}

-(UIFont *)font{
    if (!_font) {
        _font = [UIFont systemFontOfSize:15];
    }
    return _font;
}

-(NSMutableArray <UILabel *>*)labelList{
    if (!_labelList) {
        _labelList = @[].mutableCopy;
    }
    return _labelList;
}


-(UIImageView *)bgImageView{
    if (!_bgImageView) {
        _bgImageView = [[UIImageView alloc] init];
        [self addSubview:_bgImageView];
        [_bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return _bgImageView;
}


@end
