//
//  SwitchSlideButton.h
//  Bet365
//
//  Created by super_小鸡 on 2019/8/11.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class SwitchSlideButton;

@protocol SwitchSlideButtonDelegate <NSObject>

-(BOOL)tapUpSwitchSlideButton:(SwitchSlideButton *)button Index:(NSUInteger)idx;

@end

@interface SwitchSlideButton : UIView


@property (nonatomic,strong) UIImage *slideBackgroundImage;

@property (nonatomic,assign,readonly) NSUInteger idx;

@property (nonatomic,strong) NSArray <NSString *>* list;

@property (nonatomic,strong) UIColor *titileColor;

@property (nonatomic,strong) UIColor *titileSelColor;

@property (nonatomic,strong) UIFont *font;

@property (nonatomic,weak) id<SwitchSlideButtonDelegate> delegate;

@property (nonatomic,assign) BOOL enable;

-(void)setIndex:(NSUInteger)idx Animated:(BOOL)animated;

@end

NS_ASSUME_NONNULL_END
