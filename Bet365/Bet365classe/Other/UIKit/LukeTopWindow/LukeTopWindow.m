//
//  LukeTopWindow.m
//  Bet365
//
//  Created by luke on 17/9/1.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeTopWindow.h"

@interface LukeTopWindow ()
<UITableViewDelegate,
UITableViewDataSource,
UIGestureRecognizerDelegate>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *menuList;

@end

@implementation LukeTopWindow

-(instancetype)init{
    if (self = [super initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)]) {
        self.windowLevel = UIWindowLevelAlert + 1;
        self.backgroundColor = [UIColor clearColor];
        UIView *topView = [[UIView alloc] init];
        topView.backgroundColor = [UIColor clearColor];
        topView.frame = CGRectMake(WIDTH - 140, statusHeight + 22, 120, 20);
        [self addSubview:topView];
        UIView *buttonView = [[UIView alloc] init];
        buttonView.backgroundColor = [UIColor skinViewBgColor];
        buttonView.layer.cornerRadius = 5;
        buttonView.layer.masksToBounds = YES;
        [self addSubview:buttonView];
        [self makeKeyAndVisible];
        UIBezierPath *path = [UIBezierPath bezierPath];
        [path setLineWidth:1];
        CGPoint point1 = CGPointMake(85, 20);
        CGPoint point2 = CGPointMake(100, 5);
        CGPoint point3 = CGPointMake(115, 20);
        [path moveToPoint:point1];
        [path addLineToPoint:point2];
        [path addLineToPoint:point3];
        [path closePath];
        CAShapeLayer *layer = [[CAShapeLayer alloc] init];
        layer.path = path.CGPath;
        layer.fillColor = [UIColor skinViewBgColor].CGColor;
        layer.lineWidth = path.lineWidth;
        [topView.layer addSublayer:layer];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick)];
        tap.delegate = self;
        [self addGestureRecognizer:tap];
        CGFloat maxHeight = 0.0;
        CGFloat y = statusHeight + 39;
        if (self.menuList.count * 30 > HEIGHT - y - TABBAR_HEIGHT) {
            maxHeight = HEIGHT - y - TABBAR_HEIGHT;
        }else{
            maxHeight = self.menuList.count * 30;
        }
        [buttonView addSubview:self.tableView];
        buttonView.frame = CGRectMake(WIDTH - 140, statusHeight + 39, 120, maxHeight);
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(buttonView);
        }];
    }
    return self;
}

#pragma mark - UIGestureRecognizerDelegate
-(BOOL)gestureRecognizer:(UIGestureRecognizer*)gestureRecognizer shouldReceiveTouch:(UITouch*)touch {
    
    if([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"]){
        return NO;
    }
    return YES;
}

- (void)tapClick
{
    if ([self.delegate respondsToSelector:@selector(lukeTopWindowResignKeyWindow)]) {
        [self.delegate lukeTopWindowResignKeyWindow];
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.menuList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UITableViewCell"];
    }
    MenuTemplate *menu = self.menuList[indexPath.row];
    if ([menu.appLink isEqualToString:kRouterSoundSwitch]) {
        if (BET_CONFIG.userSettingData.isOpenMusic) {
            cell.textLabel.text = @"音效/关";
        }else{
            cell.textLabel.text = @"音效/开";
        }
    }else{
        cell.textLabel.text = menu.name;
    }
    cell.textLabel.textColor = [UIColor skinViewKitSelColor];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(lukeTopWindowTapActionBy:)]) {
        [self.delegate lukeTopWindowTapActionBy:[self.menuList safeObjectAtIndex:indexPath.row]];
    }
}

#pragma mark - SET/GET
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] init];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.rowHeight = 30;
        _tableView.tableFooterView = [UIView new];
        if ([_tableView respondsToSelector:@selector(layoutMargins)]) {
            _tableView.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        if ([_tableView respondsToSelector:@selector(separatorInset)]) {
            _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }
    }
    return _tableView;
}

- (NSMutableArray *)menuList
{
    if (!_menuList) {
        _menuList = [NSMutableArray array];
        [BET_CONFIG.menuList enumerateObjectsUsingBlock:^(MenuTemplate * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.appLink containsString:kRouterLogin] && [USER_DATA_MANAGER isLogin]) {
                
            }else if ([obj.appLink containsString:kRouterExit] && ![USER_DATA_MANAGER isLogin]) {
                
            }else if ([obj.appLink hasSuffix:@"lineDetection"]) {
                
            }else{
                [_menuList addObject:obj];
            }
        }];
    }
    return _menuList;
}

@end


