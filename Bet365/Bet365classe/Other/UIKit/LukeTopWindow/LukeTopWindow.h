//
//  LukeTopWindow.h
//  Bet365
//
//  Created by luke on 17/9/1.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainAllocationModel.h"
@protocol LukeTopWindowDelegate <NSObject>

- (void)lukeTopWindowResignKeyWindow;

- (void)lukeTopWindowTapActionBy:(MenuTemplate*)menuTemplate;

@end

@interface LukeTopWindow : UIWindow

/** 代理 */
@property (nonatomic,weak) id<LukeTopWindowDelegate>delegate;

@end
