//
//  WaterFallUCollectionViewFlowLayout.h
//  Bet365
//
//  Created by super_小鸡 on 2019/9/4.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    VerticalType,   //垂直 : 固定列数，高度不固定，高度随着图片宽度决定。
    HorizontalType  //水平 : 高度固定，宽度不固定，宽度随着图片高度决定。
} DirectionType; //瀑布流类型

@interface WaterFallUCollectionViewFlowLayout : UICollectionViewLayout
/**
 *  行高(水平瀑布流时),默认为100
 */
@property (nonatomic, assign) CGFloat rowHeight;
/**
 *  单元格宽度(垂直瀑布流时)
 */
@property (nonatomic, assign, readonly) CGFloat itemWidth;
/**
 *  列数 : 默认为3
 */
@property (nonatomic, assign) NSInteger numberOfColumns;

/**
 *  内边距 : 每一列之间的间距 (top, left, bottom, right)默认为{10, 10, 10, 10};
 */
@property (nonatomic, assign) UIEdgeInsets insets;

/**
 *  每一行之间的间距 : 默认为10
 */
@property (nonatomic, assign) CGFloat rowGap;

/**
 *  每一列之间的间距 : 默认为10
 */
@property (nonatomic, assign) CGFloat columnGap;

/**
 *  高度数组 : 存储所有item的高度
 */
@property (nonatomic, strong) NSArray *itemHeights;

/**
 *  宽度数组 : 存储所有item的宽度
 */
@property (nonatomic, strong) NSArray *itemWidths;

/**
 *  瀑布流类型 : 分为水平瀑布流 和 垂直瀑布流
 */
@property (nonatomic, assign) DirectionType type;

@end
