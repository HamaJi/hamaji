//
//  UICollectionSectionColorReusableView.m
//  Bet365
//
//  Created by super_小鸡 on 2019/6/25.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "UICollectionSectionColorReusableView.h"

@implementation UICollectionSectionColorReusableView

- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    [super applyLayoutAttributes:layoutAttributes];
    if ([layoutAttributes isKindOfClass:[UICollectionViewLayoutColorAttributes class]]) {
        UICollectionViewLayoutColorAttributes *attr = (UICollectionViewLayoutColorAttributes *)layoutAttributes;
        self.backgroundColor = attr.backgroundColor;
    }
}

@end
