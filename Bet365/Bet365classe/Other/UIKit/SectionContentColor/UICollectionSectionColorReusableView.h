//
//  UICollectionSectionColorReusableView.h
//  Bet365
//
//  Created by super_小鸡 on 2019/6/25.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICollectionViewLayoutColorAttributes.h"

NS_ASSUME_NONNULL_BEGIN

@interface UICollectionSectionColorReusableView : UICollectionReusableView

@end

NS_ASSUME_NONNULL_END
