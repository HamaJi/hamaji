//
//  PresentingAlphaAnimator.m
//  Bet365
//
//  Created by HHH on 2018/10/20.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "PresentingAlphaAnimator.h"
#import <POP/POP.h>

@implementation PresentingAlphaAnimator

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext{
    return 0.5f;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext{
    UIView *fromView = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey].view;
    fromView.tintAdjustmentMode = UIViewTintAdjustmentModeDimmed;
    fromView.userInteractionEnabled = NO;
    
    UIView *toView = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey].view;
    toView.frame = CGRectMake(0,
                              0,
                              CGRectGetWidth(transitionContext.containerView.bounds),
                              CGRectGetHeight(transitionContext.containerView.bounds));
    toView.center = CGPointMake(transitionContext.containerView.center.x, transitionContext.containerView.center.y);
    toView.alpha = 0;
    
    [transitionContext.containerView addSubview:toView];
    
    POPBasicAnimation *opacityAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    opacityAnimation.toValue = @(1.0);
    [opacityAnimation setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        [transitionContext completeTransition:YES];
    }];
    
    [toView.layer pop_addAnimation:opacityAnimation forKey:@"opacityAnimation"];
}

@end
