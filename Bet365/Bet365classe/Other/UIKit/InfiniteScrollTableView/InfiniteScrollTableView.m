//
//  InfiniteScrollTableView.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/18.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "InfiniteScrollTableView.h"
#import "ISTableViewInterceptor.h"
@interface InfiniteScrollTableView ()

@property (nonatomic, strong) ISTableViewInterceptor * dataSourceInterceptor;
@property (nonatomic, assign) NSInteger actualRows;

@end

@implementation InfiniteScrollTableView

- (void)layoutSubviews {
    [self resetContentOffsetIfNeeded];
    [super layoutSubviews];
}

- (void)resetContentOffsetIfNeeded {
    if (self.visibleCells.count) {
        CGPoint contentOffset  = self.contentOffset;
        //scroll over top
        if (contentOffset.y < 0.0) {
            contentOffset.y = self.contentSize.height / 3.0;
        }
        //scroll over bottom
        else if (contentOffset.y >= (self.contentSize.height - self.bounds.size.height)) {
            contentOffset.y = self.contentSize.height / 3.0 - self.bounds.size.height;
        }
        [self setContentOffset: contentOffset];
    }
}

#pragma mark - GET/SET
- (void)setDataSource:(id<UITableViewDataSource>)dataSource {
    self.dataSourceInterceptor.receiver = dataSource;
    [super setDataSource:(id<UITableViewDataSource>)self.dataSourceInterceptor];
}

- (ISTableViewInterceptor *)dataSourceInterceptor {
    if (!_dataSourceInterceptor) {
        _dataSourceInterceptor = [[ISTableViewInterceptor alloc]init];
        _dataSourceInterceptor.middleMan = self;
    }
    return _dataSourceInterceptor;
}


#pragma mark - Override
- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    self.actualRows = [self.dataSourceInterceptor.receiver tableView:tableView numberOfRowsInSection:section];
    return self.actualRows * 3;
}

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSIndexPath * actualIndexPath = [NSIndexPath indexPathForRow:indexPath.row % self.actualRows inSection:indexPath.section];
    return [self.dataSourceInterceptor.receiver tableView:tableView cellForRowAtIndexPath:actualIndexPath];
}


@end
