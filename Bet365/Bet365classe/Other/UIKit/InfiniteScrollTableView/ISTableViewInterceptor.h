//
//  ISTableViewInterceptor.h
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/18.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ISTableViewInterceptor : NSObject

@property (nonatomic, weak) id receiver;

@property (nonatomic, weak) id middleMan;

@end

NS_ASSUME_NONNULL_END
