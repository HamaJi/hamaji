//
//  InfiniteScrollTableView.h
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/18.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InfiniteScrollTableView : UITableView

@end

NS_ASSUME_NONNULL_END
