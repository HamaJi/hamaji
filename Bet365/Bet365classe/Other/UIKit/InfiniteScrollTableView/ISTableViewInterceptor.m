//
//  ISTableViewInterceptor.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/18.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "ISTableViewInterceptor.h"

@implementation ISTableViewInterceptor

- (id)forwardingTargetForSelector:(SEL)aSelector {
    if ([self.middleMan respondsToSelector:aSelector]){
        return self.middleMan;
    }
    if ([self.receiver respondsToSelector:aSelector]){
        return self.receiver;
    }
    
    return [super forwardingTargetForSelector:aSelector];
}

- (BOOL)respondsToSelector:(SEL)aSelector {
    if ([self.middleMan respondsToSelector:aSelector]) {
        return YES;
    }
    if ([self.receiver respondsToSelector:aSelector]) {
        return YES;
    }
    return [super respondsToSelector:aSelector];
}
@end
