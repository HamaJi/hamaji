//
//  UINavigationController+Bet365.m
//  Bet365
//
//  Created by HHH on 2018/8/28.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "UINavigationController+Bet365.h"
#import "UIViewControllerSerializing.h"
#import "aji_hookMethodUtil.h"
#import <Aspects/Aspects.h>

@implementation UINavigationController (Bet365)

#pragma clang diagnostic pop



-(NSInteger)indexAtViewController:(UIViewController *)viewController{
    __block NSInteger index = NSNotFound;
    [self.viewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isEqual:viewController]) {
            index = idx;
            *stop = YES;
        }
    }];
    return index;
}

-(UIViewController *)getViewControllerWithClass:(Class)clasObject{
    __block id viewControoler = nil;
    [self.viewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:clasObject]) {
            viewControoler = obj;
            *stop = YES;
        }
    }];
    return viewControoler;
}
@end
