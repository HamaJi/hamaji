//
//  UINavigationController+Bet365.h
//  Bet365
//
//  Created by HHH on 2018/8/28.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Bet365)
-(NSInteger)indexAtViewController:(UIViewController *)viewController;

-(UIViewController *)getViewControllerWithClass:(Class)clasObject;
@end
