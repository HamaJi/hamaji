//
//  NSString+NONil.h
//  Bet365
//
//  Created by HHH on 2018/7/2.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NONil)

+ (NSString *)noNilWithString:(NSString *)string;

+ (NSInteger)noIntegerWithString:(NSString *)string;

+ (CGFloat)noFloatWithString:(NSString *)string;

- (BOOL)isNoBlankString;

@end
