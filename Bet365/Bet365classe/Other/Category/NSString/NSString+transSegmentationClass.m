//
//  NSString+transSegmentationClass.m
//  Bet365
//
//  Created by package_mr.chicken on 2019/1/14.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "NSString+transSegmentationClass.h"

@implementation NSString (transSegmentationClass)
-(NSArray *)securityParmater:(NSString *)complment{
    NSArray *keyArr = @[@"password",@"surepassword",@"verifyCode"];
    NSArray *valueArr = @[@"新密码",@"确认密码",@"验证码"];
//    [config_dic setDictionary:dict];
    if (!complment||[complment isKindOfClass:[NSNull class]]) {
        return @[keyArr,valueArr];
    }
    NSMutableArray *keyM = [NSMutableArray array];
    NSMutableArray *valueM = [NSMutableArray array];
    [keyM addObjectsFromArray:keyArr];
    [valueM addObjectsFromArray:valueArr];
    NSArray *frist_segment = [complment componentsSeparatedByString:@"|"];
    for (NSString *msg in frist_segment) {
        NSArray *last_segment = [msg componentsSeparatedByString:@"="];
        [valueM addObject:last_segment[1]];
        [keyM addObject:last_segment[0]];
    }
    return @[keyM,valueM];
}
-(NSMutableAttributedString *)atterSforProfitcalCulat:(NSString *)reward vaildSum:(NSString *)sumTotalM forDrawMoney:(NSString *)money{
    CGFloat profit = [reward floatValue]-([sumTotalM floatValue]-[money floatValue]);
    NSString *proFitS = [NSString stringWithFormat:@"盈亏 %.2f",profit];
    NSMutableAttributedString *atter = [[NSMutableAttributedString alloc] initWithString:proFitS];
    [atter setAttributes:@{NSForegroundColorAttributeName:[UIColor redColor]} range:NSMakeRange(0, proFitS.length)];
    NSRange range = [proFitS rangeOfString:@"盈亏"];
    [atter setAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]} range:range];
    return atter;
}

@end
