//
//  NSMutableAttributedString+Bet365.h
//  Bet365
//
//  Created by luke on 2018/8/16.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableAttributedString (Bet365)

+ (instancetype)setTitle:(NSString *)title colorTitle:(NSString *)colorTitle color:(UIColor *)color;

+ (instancetype)setTitle:(NSString*)title colorTitle:(NSString *)colorTitle color:(UIColor *)color font:(CGFloat)font;
@end
