//
//  NSStringHeader.h
//  Bet365
//
//  Created by HHH on 2018/7/4.
//  Copyright © 2018年 jesse. All rights reserved.
//

#ifndef NSStringHeader_h
#define NSStringHeader_h

#define StringFormatWithString(str) [NSString stringWithFormat:@"%@",str]
#define StringFormatWithStr(str1,str2) [NSString stringWithFormat:@"%@%@",str1,str2]
#define StringFormatWithFloat(f) [NSString stringWithFormat:@"%.2f",f]
#define StringFormatWithStrInteger(str,num) [NSString stringWithFormat:@"%@%ld",str,num]
#define StringFormatWithInteger(num) [NSString stringWithFormat:@"%ld",num]

#import "NSString+Aji.h"
#import "NSString+NONil.h"
#import "NSString+changeNum.h"
#import "NSMutableString+Aji.h"
#import "NSString+AES_Base64.h"
#import "NSMutableAttributedString+Bet365.h"
#import "NSAttributedString+Aji.h"
#import "NSString+MacRegexCategory.h"
#import "NSString+Transcoding.h"
#import "NSMutableAttributedString+Attributes.h"
#import "NSString+caclute_height.h"
#import "NSString+PassWord.h"
#endif /* NSStringHeader_h */
