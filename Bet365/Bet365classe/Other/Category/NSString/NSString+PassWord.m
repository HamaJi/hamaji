//
//  NSString+PassWord.m
//  Bet365
//
//  Created by adnin on 2019/4/3.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "NSString+PassWord.h"

@implementation NSString (PassWord)

-(BOOL)isPermissionllyPsw{
    
    NSUInteger min = BET_CONFIG.registerConfig.isComplexPassword ? 8 : 6;
    if (self.length < min) {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"至少输入%li位登录密码",min]];
        return NO;
    }
    
    NSUInteger max = BET_CONFIG.registerConfig.isComplexPassword ? 30 : 0;
//    NSUInteger max = BET_CONFIG.registerConfig.isComplexPassword ? 30 : 12;
    if (max != 0 && self.length > max) {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"登录密码长度不能超过%li位",max]];
        return NO;
    }
    
    if (!BET_CONFIG.registerConfig.isComplexPassword) {
        return YES;
    }
    
    __block BOOL isPermissionlly = [self checkIsHaveNumAndLetter];
    if (!isPermissionlly) {
        return isPermissionlly;
    }
    
    if (isPermissionlly) {
        
        [filterContinuanceEntryList() enumerateObjectsUsingBlock:^(NSString * _Nonnull str, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([self containsString:str]) {
                isPermissionlly = NO;
                [SVProgressHUD showErrorWithStatus:@"登录密码不能包含连3位续性数字,如:123"];
                *stop = YES;
            }
        }];
    }
    
    if (isPermissionlly) {
        [filterSameEntryList() enumerateObjectsUsingBlock:^(NSString * _Nonnull str, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([self containsString:str]) {
                isPermissionlly = NO;
                [SVProgressHUD showErrorWithStatus:@"登录密码不能包含3位重复性数字,如:000"];
                *stop = YES;
            }
        }];
    }
    return isPermissionlly;
}

-(BOOL)checkIsHaveNumAndLetter{
    /*数字*/
    NSRegularExpression *tNumRegularExpression = [NSRegularExpression regularExpressionWithPattern:@"[0-9]"options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger tNumMatchCount = [tNumRegularExpression numberOfMatchesInString:self
                                                                       options:NSMatchingReportProgress
                                                                         range:NSMakeRange(0, self.length)];
    /*英文*/
    NSRegularExpression *tLetterRegularExpression = [NSRegularExpression regularExpressionWithPattern:@"[A-Za-z]"options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger tLetterMatchCount = [tLetterRegularExpression numberOfMatchesInString:self
                                                                             options:NSMatchingReportProgress
                                                                               range:NSMakeRange(0, self.length)];
    
    /*空格*/
//    NSRegularExpression *tSpaceRegularExpression = [NSRegularExpression regularExpressionWithPattern:@"(?:^\\s+)|(?:\\s+$)" options:NSRegularExpressionCaseInsensitive error:nil];
//    NSUInteger tSpaceMatchCount = [tSpaceRegularExpression numberOfMatchesInString:self
//                                                                             options:NSMatchingReportProgress
//                                                                               range:NSMakeRange(0, self.length)];
//    
//    if (tSpaceMatchCount) {
//        [SVProgressHUD showErrorWithStatus:@"请勿输入空格"];
//        return NO;
//    }
    if (tNumMatchCount == self.length) {
        /* 全部符合数字，表示沒有英文*/
        [SVProgressHUD showErrorWithStatus:@"登录密码格式必须包含一个字母"];
        return NO;
    } else if (tLetterMatchCount == self.length) {
        /* 全部符合英文，表示沒有数字*/
        [SVProgressHUD showErrorWithStatus:@"登录密码格式必须包含一个数字"];
        return NO;
    } else if (tNumMatchCount + tLetterMatchCount == self.length) {
        //符合英文和符合数字条件的相加等于密码长度
        return YES;
    } else {
        BOOL isContainChinese = [self isContainChinese];
        if (isContainChinese) {
            [SVProgressHUD showErrorWithStatus:@"登录密码不能包含中文"];
        }
        return !isContainChinese;
    }
}

NSArray <NSString *>*filterContinuanceEntryList(){
    return @[@"123",
             @"234",
             @"345",
             @"456",
             @"567",
             @"678",
             @"789",
             @"890"];
}

NSArray <NSString *>*filterSameEntryList(){
    return @[@"000",
             @"111",
             @"222",
             @"333",
             @"444",
             @"555",
             @"666",
             @"777",
             @"888",
             @"999"];
}
@end
