//
//  NSString+changeNum.h
//  Lottery
//
//  Created by luke on 17/3/18.
//  Copyright © 2017年 pony. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (changeNum)

//自动将字符串数字三位一切割
+ (NSString *)ChangeNumberFormat:(NSString *)num;

- (BOOL)isContainChinese;

@end
