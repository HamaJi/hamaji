//
//  NSMutableAttributedString+Bet365.m
//  Bet365
//
//  Created by luke on 2018/8/16.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NSMutableAttributedString+Bet365.h"

@implementation NSMutableAttributedString (Bet365)

+ (instancetype)setTitle:(NSString *)title colorTitle:(NSString *)colorTitle color:(UIColor *)color
{
    NSString *titleStr = [NSString stringWithFormat:@"%@ %@",title,colorTitle];
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:titleStr];
    [attStr setAttributes:@{NSForegroundColorAttributeName : color} range:NSMakeRange(titleStr.length - colorTitle.length, colorTitle.length)];
    return attStr;
}

+ (instancetype)setTitle:(NSString *)title colorTitle:(NSString *)colorTitle color:(UIColor *)color font:(CGFloat)font
{
    NSString *titleStr = [NSString stringWithFormat:@"%@ %@",title,colorTitle];
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:titleStr];
    [attStr setAttributes:@{NSForegroundColorAttributeName : color,NSFontAttributeName : [UIFont systemFontOfSize:font]} range:NSMakeRange(titleStr.length - colorTitle.length, colorTitle.length)];
    return attStr;
}

@end
