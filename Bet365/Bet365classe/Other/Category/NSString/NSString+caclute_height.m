//
//  NSString+caclute_height.m
//  Bet365
//
//  Created by package_mr.chicken on 2019/3/17.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "NSString+caclute_height.h"

@implementation NSString (caclute_height)
-(CGFloat)trans_currenStringForLbHeight:(NSString *)content{
    CGRect descRect = [content boundingRectWithSize:CGSizeMake(WIDTH, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine |
                       NSStringDrawingUsesLineFragmentOrigin |
                       NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]} context:nil];
    return descRect.size.height;
}
@end
