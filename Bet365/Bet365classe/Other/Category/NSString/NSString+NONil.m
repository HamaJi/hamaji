//
//  NSString+NONil.m
//  Bet365
//
//  Created by HHH on 2018/7/2.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NSString+NONil.h"

@implementation NSString (NONil)
+ (NSString *)noNilWithString:(NSString *)string
{
    if(!string)
    {
        return @"";
    }
    if ([string isEqual:[NSNull null]]) {
        return @"";
    }
    //    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    return string;
}

+ (NSInteger)noIntegerWithString:(NSString *)string;
{
    NSInteger value = [string integerValue];
    if(!string)
    {
        return 0;
    }
    return value;
}

+ (CGFloat)noFloatWithString:(NSString *)string
{
    CGFloat value = [string floatValue];
    if(!string)
    {
        return 0;
    }
    return value;
}

- (BOOL)isNoBlankString
{
    if (!self) {
        return NO;
    }
    if ([self isKindOfClass:[NSNull class]]) {
        return NO;
    }
    if (!self.length) {
        return NO;
    }
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmedStr = [self stringByTrimmingCharactersInSet:set];
    if (!trimmedStr.length) {
        return NO;
    }
    return YES;
}

@end
