//
//  NSMutableString+Aji.m
//  Bet365
//
//  Created by HHH on 2018/7/30.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NSMutableString+Aji.h"

@implementation NSMutableString (Aji)
-(void)deleteString:(NSString *)str{
    if (!str.length) {
        return;
    }
    NSRange range = [self rangeOfString:str];
    if (range.length) {
        [self deleteCharactersInRange:range];
    }
    /*stringByReplacingOccurrencesOfString 无效*/
//    [self stringByReplacingOccurrencesOfString:str withString:@""];
}

-(void)deleteBetweenStarStr:(NSString *)starStr EndStr:(NSString *)endStr{
    
    NSRange startRange = [self rangeOfString:starStr];
    NSRange endRange = [self rangeOfString:endStr];
    if (startRange.length && endRange.length) {
        NSRange range = NSMakeRange(startRange.location, endRange.location + endRange.length -  startRange.location);
        [self deleteCharactersInRange:range];
        [self deleteBetweenStarStr:starStr EndStr:endStr];
    }
}
@end
