//
//  NSString+Transcoding.m
//  Bet365
//
//  Created by jesse on 2018/10/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NSString+Transcoding.h"

@implementation NSString (Transcoding)
- (NSString *)transcodingUTF:(NSString *)string{
   
    return [string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}
@end
