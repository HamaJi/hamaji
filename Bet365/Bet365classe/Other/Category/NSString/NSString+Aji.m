//
//  NSString+Aji.m
//  Bet365
//
//  Created by HHH on 2018/7/4.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NSString+Aji.h"
#import <CommonCrypto/CommonDigest.h>
@implementation NSString (Aji)

- (CGFloat)stringHeightWithFont:(UIFont *)font width:(CGFloat)width{
    CGSize size = CGSizeMake(width, CGFLOAT_MAX);
    NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName,nil];
    size =[self boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin |NSStringDrawingUsesFontLeading attributes:dic context:nil].size;
    return size.height;
}

-(CGFloat)stringWidthWithFont:(UIFont *)font{
    if (!font && !self.length) {
        return 0.0;
    }
    CGSize size = CGSizeMake(2000,2000);
    CGSize labelsize = [self boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font} context:nil].size;
    return labelsize.width;
}

- (NSAttributedString *)htmlStr{
    if (!self.length) {
        return nil;
    }
    NSAttributedString*attributeString;
    NSData *htmlData = [self dataUsingEncoding:NSUTF8StringEncoding];
    if (!htmlData) {
        return nil;
    }
    NSDictionary *importParams = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]};
    NSError*error =nil;
    attributeString = [[NSAttributedString alloc]initWithData:htmlData options:importParams documentAttributes:NULL error:&error];
    return attributeString;

}

#pragma mark--密码MD5加密
- (NSString *)MD5{
    const char *variable1 = [self UTF8String];
    unsigned char variable2[CC_MD5_DIGEST_LENGTH];
    CC_MD5(variable1, (CC_LONG)strlen(variable1), variable2);
    
    NSMutableString *variable3 = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH];
    for (NSInteger i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [variable3 appendFormat:@"%02x", variable2[i]];
    }
    return variable3;
}

- (NSArray *)regularExpressionWithPattern:(NSString *)pattern
{
    NSRegularExpression *regular = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:nil];
    return [regular matchesInString:self options:0 range:NSMakeRange(0, self.length)];
}

-(NSString *)replaceUnicode {
    NSString *tempStr1 = [self stringByReplacingOccurrencesOfString:@"\\u" withString:@"\\U"];
    NSString *tempStr2 = [tempStr1 stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSString *tempStr3 = [[@"\"" stringByAppendingString:tempStr2] stringByAppendingString:@"\""];
    NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
    NSString* returnStr = [NSPropertyListSerialization propertyListFromData:tempData
                                                           mutabilityOption:NSPropertyListImmutable
                                                                     format:NULL
                                                           errorDescription:NULL];
    return [returnStr stringByReplacingOccurrencesOfString:@"\\r\\n" withString:@"\n"];
    
}
+(NSString*)timeFormat:(NSInteger)time{
    if (time >=10) {
        return [NSString stringWithFormat:@"%li",time];
    }else{
        if (time < 0) {
            NSLog(@"低啦");
            return @"00";
        }
        return [NSString stringWithFormat:@"0%li",time];
    }
}


+ (NSString *)getMMSSFromSeconds:(NSInteger)seconds{
    //format of minute
    NSString *str_minute = [NSString stringWithFormat:@"%02ld",seconds/60];
    //format of second
    NSString *str_second = [NSString stringWithFormat:@"%02ld",seconds%60];
    //format of time
    NSString *format_time = [NSString stringWithFormat:@"%@:%@",str_minute,str_second];
    return format_time;
}

- (BOOL)isElectronicGame
{
    NSArray *elecArrs = @[@"mg",@"pt",@"cq9",@"jdb",@"hb",@"pg",@"agele",@"bbinele",@"pt2",@"gg",@"wzdz"];
    for (NSString *eleStr in elecArrs) {
        if ([eleStr isEqualToString:self]) {
            return YES;
        }
    }
    return NO;
}
    
+ (NSString *)roundingWithFloat:(NSNumber *)money
{
    NSString *temp = [NSString stringWithFormat:@"%@",money];
    NSDecimalNumber *numResult = [NSDecimalNumber decimalNumberWithString:temp];
    NSDecimalNumberHandler *roundUp = [NSDecimalNumberHandler
                                       decimalNumberHandlerWithRoundingMode:NSRoundDown
                                       scale:2
                                       raiseOnExactness:NO
                                       raiseOnOverflow:NO
                                       raiseOnUnderflow:NO
                                       raiseOnDivideByZero:YES];
    return [[numResult decimalNumberByRoundingAccordingToBehavior:roundUp] stringValue];
//    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//    formatter.roundingMode = NSNumberFormatterRoundFloor;
//    formatter.maximumFractionDigits = 2;
//    return [formatter stringFromNumber:money];
}

- (NSDecimalNumber *)decimalNumberRounding
{
    NSDecimalNumber *numResult = [NSDecimalNumber decimalNumberWithString:self];
    NSDecimalNumberHandler *roundUp = [NSDecimalNumberHandler
                                       decimalNumberHandlerWithRoundingMode:NSRoundDown
                                       scale:2
                                       raiseOnExactness:NO
                                       raiseOnOverflow:NO
                                       raiseOnUnderflow:NO
                                       raiseOnDivideByZero:YES];
    return [numResult decimalNumberByRoundingAccordingToBehavior:roundUp];
    //    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    //    formatter.roundingMode = NSNumberFormatterRoundFloor;
    //    formatter.maximumFractionDigits = 2;
    //    return [formatter stringFromNumber:money];
}

- (NSString *)decimalNumberRoundingString
{
    return [[self decimalNumberRounding] stringValue];
}

- (NSString *)URLEncodedString
{
    // CharactersToBeEscaped = @":/?&=;+!@#$()~',*";
    // CharactersToLeaveUnescaped = @"[].";
    
    NSString *unencodedString = self;
    NSString *encodedString = (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                              (CFStringRef)unencodedString,
                                                              NULL,
                                                              (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
    
    return encodedString;
}

- (NSTimeInterval)compareOtherSystemTimeWithformat:(NSString *)format
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.dateFormat = format;
    NSDate *nowDate = [NSDate date];
    NSDate *otherDate = [dateFormat dateFromString:self];
    return [otherDate timeIntervalSinceDate:nowDate];
}

-(NSString *) stringByStrippingHTML {
    NSRange r;
    NSString *s = [self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

@end
