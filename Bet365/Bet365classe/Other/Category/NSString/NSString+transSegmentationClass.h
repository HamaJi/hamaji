//
//  NSString+transSegmentationClass.h
//  Bet365
//
//  Created by package_mr.chicken on 2019/1/14.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (transSegmentationClass)
/*通过分割返回security验证配置信息*/
-(NSArray *)securityParmater:(NSString *)complment;
//修改副文本
-(NSMutableAttributedString *)atterSforProfitcalCulat:(NSString*)reward vaildSum:(NSString *)sumTotalM forDrawMoney:(NSString *)money;
@end

NS_ASSUME_NONNULL_END
