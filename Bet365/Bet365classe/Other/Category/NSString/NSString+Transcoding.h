//
//  NSString+Transcoding.h
//  Bet365
//
//  Created by jesse on 2018/10/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Transcoding)
-(NSString *)transcodingUTF:(NSString *)string;
@end
