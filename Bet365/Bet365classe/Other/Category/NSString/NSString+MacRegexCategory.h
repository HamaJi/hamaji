//
//  NSString+MacRegexCategory.h
//

#import <Foundation/Foundation.h>
/**
 *  正则表达式简单说明
 *  语法：
 .       匹配除换行符以外的任意字符
 \w      匹配字母或数字或下划线或汉字
 \s      匹配任意的空白符
 \d      匹配数字
 \b      匹配单词的开始或结束
 ^       匹配字符串的开始
 $       匹配字符串的结束
 *       重复零次或更多次
 +       重复一次或更多次
 ?       重复零次或一次
 {n} 	重复n次
 {n,} 	重复n次或更多次
 {n,m} 	重复n到m次
 \W      匹配任意不是字母，数字，下划线，汉字的字符
 \S      匹配任意不是空白符的字符
 \D      匹配任意非数字的字符
 \B      匹配不是单词开头或结束的位置
 [^x] 	匹配除了x以外的任意字符
 [^aeiou]匹配除了aeiou这几个字母以外的任意字符
 *?      重复任意次，但尽可能少重复
 +?      重复1次或更多次，但尽可能少重复
 ??      重复0次或1次，但尽可能少重复
 {n,m}? 	重复n到m次，但尽可能少重复
 {n,}? 	重复n次以上，但尽可能少重复
 \a      报警字符(打印它的效果是电脑嘀一声)
 \b      通常是单词分界位置，但如果在字符类里使用代表退格
 \t      制表符，Tab
 \r      回车
 \v      竖向制表符
 \f      换页符
 \n      换行符
 \e      Escape
 \0nn 	ASCII代码中八进制代码为nn的字符
 \xnn 	ASCII代码中十六进制代码为nn的字符
 \unnnn 	Unicode代码中十六进制代码为nnnn的字符
 \cN 	ASCII控制字符。比如\cC代表Ctrl+C
 \A      字符串开头(类似^，但不受处理多行选项的影响)
 \Z      字符串结尾或行尾(不受处理多行选项的影响)
 \z      字符串结尾(类似$，但不受处理多行选项的影响)
 \G      当前搜索的开头
 \p{name} 	Unicode中命名为name的字符类，例如\p{IsGreek}
 (?>exp) 	贪婪子表达式
 (?<x>-<y>exp) 	平衡组
 (?im-nsx:exp) 	在子表达式exp中改变处理选项
 (?im-nsx)       为表达式后面的部分改变处理选项
 (?(exp)yes|no) 	把exp当作零宽正向先行断言，如果在这个位置能匹配，使用yes作为此组的表达式；否则使用no
 (?(exp)yes) 	同上，只是使用空表达式作为no
 (?(name)yes|no) 如果命名为name的组捕获到了内容，使用yes作为表达式；否则使用no
 (?(name)yes) 	同上，只是使用空表达式作为no
 
 捕获
 (exp)               匹配exp,并捕获文本到自动命名的组里
 (?<name>exp)        匹配exp,并捕获文本到名称为name的组里，也可以写成(?'name'exp)
 (?:exp)             匹配exp,不捕获匹配的文本，也不给此分组分配组号
 零宽断言
 (?=exp)             匹配exp前面的位置
 (?<=exp)            匹配exp后面的位置
 (?!exp)             匹配后面跟的不是exp的位置
 (?<!exp)            匹配前面不是exp的位置
 注释
 (?#comment)         这种类型的分组不对正则表达式的处理产生任何影响，用于提供注释让人阅读
 
 *  表达式：\(?0\d{2}[) -]?\d{8}
 *  这个表达式可以匹配几种格式的电话号码，像(010)88886666，或022-22334455，或02912345678等。
 *  我们对它进行一些分析吧：
 *  首先是一个转义字符\(,它能出现0次或1次(?),然后是一个0，后面跟着2个数字(\d{2})，然后是)或-或空格中的一个，它出现1次或不出现(?)，
 *  最后是8个数字(\d{8})
 */

#import <Foundation/Foundation.h>
typedef enum {
    ENG_YMD = 0,
    ENG_YMD_HMS,
    CHN_YMD,
    CHN_YMD_HMS,
}TimeType;


typedef enum {
    ENG_DMN = 0,
    CHN_DMN,
    ENG_D
}HouseTimeType;


typedef NS_ENUM(NSUInteger, CanadaHousePayTimeType) {    // date and time format styles
    CanadaHousePayTimeType_ENG_YMD,
    CanadaHousePayTimeType_ENG_MD,
    CanadaHousePayTimeType_CHN_YMD,
    CanadaHousePayTimeType_CHN_MD
};

@interface NSString (MacRegexCategory)
//获得当前时间戳
+ (NSString *)getCurrentTimeIntervalStr;
//获取文本最大长度
-(CGSize)getTextWidthWithFont:(UIFont*)font;

-(id)toArrayOrNSDictionaryWithJsonStr;

- (BOOL)isValidateByRegex:(NSString *)regex;
/*
 * 根据指定的参数名，从URL中找出并返回对应的参数值。
 */
- (NSString *)getValueStringFromUrl;
/**
 根据时间格式的字符串转换nsdate
 
 @return
 */
-(NSDate *)getDateWithTimeStr;
/**
 获取字符窗时间
 
 @param time NSInteger时间戳
 @param timeType 转换时间格式
 @return 返回时间自负串
 */
+ (NSString *)timeWithTimeIntervalWithInteger:(NSInteger )time TimeType:(TimeType)timeType;


/**
 获取字符窗时间 --
 
 @param time NSInteger时间戳
 @param timeType 转换时间格式
 @return 返回时间自负串
 */
+ (NSString *)hTimeWithTimeIntervalWithInteger:(NSInteger )time TimeType:(HouseTimeType)timeType;

/**
 获取分秒
 
 @param seconds NSInteger秒数
 @return 返回时间自负串
 */
+(NSString *)getMMSSFromSS:(NSInteger )seconds;
/**
 加盐加密
 @return 加盐加密返回
 */
-(NSString*)md5SaltString;
/**
 md5加密
 
 @return 加密返回
 */
- (NSString *) md5;

-(NSString *)gbkReplace;
/**
 计算指定宽度的文本框显示该字符窗所需要的高度
 
 @param width 文本框框度
 @param font 系统字体大小
 @return 文本框最大高度
 */
- (CGSize)getLabelSizeWithWidth:(float)width andFont:(UIFont *)font;
/**
 *  手机号码的有效性:分电信、联通、移动和小灵通
 */
- (BOOL)isMobileNumberClassification;
//判断手机号码，固话，小灵通有效性
-(BOOL)isTelePhoneNumber;
/**
 *  手机号有效性
 */
- (BOOL)isMobileNumber;

/**
 *  邮箱的有效性
 */
- (BOOL)isEmailAddress;

/**
 *  简单的身份证有效性
 *
 */
- (BOOL)simpleVerifyIdentityCardNum;

/**
 *  精确的身份证号码有效性检测
 *
 *  @param value 身份证号
 */
+ (BOOL)accurateVerifyIDCardNumber:(NSString *)value;

/**
 *  车牌号的有效性
 */
- (BOOL)isCarNumber;

/**
 *  银行卡的有效性
 */
- (BOOL)bankCardluhmCheck;

/**
 *  IP地址有效性
 */
- (BOOL)isIPAddress;

/**
 *  Mac地址有效性
 */
- (BOOL)isMacAddress;

/**
 *  网址有效性
 */
- (BOOL)isValidUrl;

/**
 *  是否纯汉子
 */
- (BOOL)isValidChinese;


/**
 是否包含字符
 */
-(BOOL)isContainSpecialSymbol;


/**
 *  邮政编码
 */
- (BOOL)isValidPostalcode;

/**
 *  工商税号
 */
- (BOOL)isValidTaxNo;

/**
 @brief     是否符合最小长度、最长长度，是否包含中文,首字母是否可以为数字
 @param     minLenth 账号最小长度
 @param     maxLenth 账号最长长度
 @param     containChinese 是否包含中文
 @param     firstCannotBeDigtal 首字母不能为数字
 @return    正则验证成功返回YES, 否则返回NO
 */
- (BOOL)isValidWithMinLenth:(NSInteger)minLenth
                   maxLenth:(NSInteger)maxLenth
             containChinese:(BOOL)containChinese
        firstCannotBeDigtal:(BOOL)firstCannotBeDigtal;

/**
 @brief     是否符合最小长度、最长长度，是否包含中文,数字，字母，其他字符，首字母是否可以为数字
 @param     minLenth 账号最小长度
 @param     maxLenth 账号最长长度
 @param     containChinese 是否包含中文
 @param     containDigtal   包含数字
 @param     containLetter   包含字母
 @param     containOtherCharacter   其他字符
 @param     firstCannotBeDigtal 首字母不能为数字
 @return    正则验证成功返回YES, 否则返回NO
 */
- (BOOL)isValidWithMinLenth:(NSInteger)minLenth
                   maxLenth:(NSInteger)maxLenth
             containChinese:(BOOL)containChinese
              containDigtal:(BOOL)containDigtal
              containLetter:(BOOL)containLetter
      containOtherCharacter:(NSString *)containOtherCharacter
        firstCannotBeDigtal:(BOOL)firstCannotBeDigtal;
/**
 移除字符串所有空格，换行
 
 @return <#return value description#>
 */

- (NSString *)removeSpaceAndNewline;


+ (NSString *)payTimeWithTimeIntervalWithInteger:(NSInteger )time TimeType:(CanadaHousePayTimeType)timeType;

/**
 正则表达只包含数字和字母
 **/
-(BOOL)isVaildOnlyContainDigtalAndLetterWithMinLenth:(NSInteger)mainLenth MaxLenth:(NSInteger)maxLenth;
/**
 获取textfiled时时输入情况
 @param filedValue当前输入框文本
 @param value 当前输入文本
 @param range 当前文本代理local length
 @return 返回最终文本
 */
-(NSString *)addObserverzfiledScanString:(NSString *)filedValue forReplaceValue:(NSString *)value forParameterRange:(NSRange)range;
/**
 获取字符串时间
 
 @param time NSInteger时间戳 /1000
 @param timeType 转换时间格式
 @return 返回时间字符串
 */
+ (NSString *)timeWithTimeIntervalWithIntegerx1:(NSInteger )time TimeType:(TimeType)timeType;


/**
 字节长度，中文判断

 @return <#return value description#>
 */
-(NSUInteger)textLength;

- (BOOL)deptNumInputShouldNumber:(NSString *)str;

/**
 是否为数字

 @return <#return value description#>
 */
- (BOOL)isShouldNumber;

/**
 是否为整数

 @return <#return value description#>
 */
- (BOOL)isValidateNumber;

- (BOOL)includeChinese;//判断是否含有汉字

- (BOOL)SpecialCharacters;

@end
