//
//  NSString+Aji.h
//  Bet365
//
//  Created by HHH on 2018/7/4.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

#define URL_PATTERN  @"http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&=]*)?"  //校验网址

@interface NSString (Aji)


/**
 根据总宽、字体大小获取文本高度

 @param font 字体
 @param width 可现实宽度
 @return 高度
 */
- (CGFloat)stringHeightWithFont:(UIFont *)font width:(CGFloat)width;

/**
 根据字体获取单行文本的宽度

 @param font 字体
 @return 高度
 */
-(CGFloat)stringWidthWithFont:(UIFont *)font;

- (NSAttributedString *)htmlStr;

- (NSString *)MD5;

- (NSArray *)regularExpressionWithPattern:(NSString *)pattern;

-(NSString *)replaceUnicode ;

+(NSString*)timeFormat:(NSInteger)time;

+ (NSString *)getMMSSFromSeconds:(NSInteger)seconds;

/**
 是否是电子游戏

 @return bool
 */
- (BOOL)isElectronicGame;

    
/**
 金额保留两位有效数字不四舍五入
 
 @param money 金额
 @return
 */
+ (NSString *)roundingWithFloat:(NSNumber *)money;

- (NSDecimalNumber *)decimalNumberRounding;

- (NSString *)decimalNumberRoundingString;

- (NSString *)URLEncodedString;

- (NSTimeInterval)compareOtherSystemTimeWithformat:(NSString *)format;

- (NSString *) stringByStrippingHTML;

@end
