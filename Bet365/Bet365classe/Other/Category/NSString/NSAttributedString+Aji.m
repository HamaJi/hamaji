//
//  NSAttributedString+Aji.m
//  Bet365
//
//  Created by HHH on 2018/9/21.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NSAttributedString+Aji.h"

@implementation NSAttributedString (Aji)
- (CGFloat)stringHeightWithWidth:(CGFloat)width{
    CGSize size = CGSizeMake(width, CGFLOAT_MAX);
    [self boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    return size.height;
}
@end
