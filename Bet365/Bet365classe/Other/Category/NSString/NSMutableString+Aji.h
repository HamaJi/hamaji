//
//  NSMutableString+Aji.h
//  Bet365
//
//  Created by HHH on 2018/7/30.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableString (Aji)

-(void)deleteString:(NSString *)str;

-(void)deleteBetweenStarStr:(NSString *)starStr EndStr:(NSString *)endStr;
@end
