//
//  UIButton+Aji.h
//  Bet365
//
//  Created by luke on 2018/8/16.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Aji)

+ (UIButton *)addButtonWithTitle:(NSString *)title font:(CGFloat)font color:(UIColor *)color target:(id)target action:(SEL)action;

@end
