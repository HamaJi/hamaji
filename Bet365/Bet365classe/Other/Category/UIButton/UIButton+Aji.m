//
//  UIButton+Aji.m
//  Bet365
//
//  Created by luke on 2018/8/16.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "UIButton+Aji.h"

@implementation UIButton (Aji)

+ (UIButton *)addButtonWithTitle:(NSString *)title font:(CGFloat)font color:(UIColor *)color target:(id)target action:(SEL)action
{
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font = [UIFont systemFontOfSize:font];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:color forState:UIControlStateNormal];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return btn;
}


@end
