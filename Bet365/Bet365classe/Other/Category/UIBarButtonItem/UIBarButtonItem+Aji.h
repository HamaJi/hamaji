//
//  UIBarButtonItem+Aji.h
//  Bet365
//
//  Created by HHH on 2018/8/1.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (Aji)
+(instancetype)createItemsByImageStr:(NSString *)imageStr Titile:(NSString *)titile Position:(LXMImagePosition)position TapHandle:(GestureActionBlock)handle;
@end
