//
//  UIBarButtonItem+Aji.m
//  Bet365
//
//  Created by HHH on 2018/8/1.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "UIBarButtonItem+Aji.h"

@implementation UIBarButtonItem (Aji)
+(instancetype)createItemsByImageStr:(NSString *)imageStr Titile:(NSString *)titile Position:(LXMImagePosition)position TapHandle:(GestureActionBlock)handle{
    UIImage *img=[[UIImage imageNamed:imageStr] imageTintedWithColor:[UIColor skinNaviTintColor]];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.titleLabel.font = [UIFont systemFontOfSize:IS_IPHONE_5s ? 14 : 16];
    [button setImage:img forState:UIControlStateNormal];
    [button setImage:img forState:UIControlStateHighlighted];
    [button setTitle:titile forState:UIControlStateNormal];
    [button setFrame:CGRectMake(0, 0, [titile stringWidthWithFont:button.titleLabel.font] + img.size.width + 10, img.size.height)];
    
    [button setTitleColor:[UIColor skinNaviTintColor] forState:UIControlStateNormal];
    [button setImagePosition:position spacing:5];
    [button addTapActionWithBlock:handle];

    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    return barButton;
    
}
@end
