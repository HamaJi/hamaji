//
//  NSObject+Aji.m
//  Bet365
//
//  Created by HHH on 2018/7/4.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NSObject+Aji.h"

@implementation NSObject (Aji)

-(NSArray <NSString *>*)aji_getAllkey{
    NSMutableArray *props = [NSMutableArray array];
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList([self class], &outCount);
    for (i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        NSString *propertyName = [[NSString alloc] initWithCString:property_getName(property) encoding:NSUTF8StringEncoding];
        if (propertyName) [props addObject:propertyName];
    }
    free(properties);
    return props;
}


@end
