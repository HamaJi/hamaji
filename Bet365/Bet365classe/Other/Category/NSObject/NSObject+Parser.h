//
//  NSObject+Parser.h
//  cardPreferential
//
//  Created by Deep river on 16-4-10.
//  Copyright (c) 2013年 Deep river. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Parser)

-(id)initWithDict:(NSDictionary *)dict;

-(NSString *)stringFromDict:(NSDictionary *)dict forKey:(NSString *)key;
@end
