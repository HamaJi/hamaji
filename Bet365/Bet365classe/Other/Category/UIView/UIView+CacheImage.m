//
//  UIView+CacheImage.m
//  Bet365
//
//  Created by jesse on 2018/10/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "UIView+CacheImage.h"

@implementation UIView (CacheImage)
- (void)returnImageBy:(NSString *)url CacheOrUpdateCompleted:(void (^)(UIImage *))Completerd{
    UIImage *cacheImage = [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:url];
    if (!cacheImage) {

        
        [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:url] options:SDWebImageDownloaderContinueInBackground progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
            
        } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
            if (finished&&!error) {
                Completerd?Completerd(image):nil;
            }
        }];
    }else{
        Completerd?Completerd(cacheImage):nil;
    }
}
@end
