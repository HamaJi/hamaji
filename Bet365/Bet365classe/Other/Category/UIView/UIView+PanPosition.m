//
//  UIView+SwipePosition.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/12/4.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "UIView+PanPosition.h"
#import <Aspects/Aspects.h>

#define SwipePositionPanGestureId   @"SwipePositionPanGestureId"

@interface UIView ()

@end

@implementation UIView (PanPosition)


-(BOOL)beAllowedPanPostion{
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

-(void)setBeAllowedPanPostion:(BOOL)beAllowedPanPostion{
    if (beAllowedPanPostion) {
        self.userInteractionEnabled = YES;
        @weakify(self);
        [self addPanGestureRecognizer:^(UIPanGestureRecognizer *recognizer, NSString *gestureId) {
            @strongify(self);
            [self handlePanGesture:recognizer];
        } panGestureId:SwipePositionPanGestureId minimumNumberOfTouches:1 maximumNumberOfTouches:NSUIntegerMax];
    }else{
        self.userInteractionEnabled = YES;
        [self removeAllPanGestures];
    }
    objc_setAssociatedObject(self, @selector(beAllowedPanPostion), @(beAllowedPanPostion), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(UIEdgeInsets)safePanAreaInsets{
    return [objc_getAssociatedObject(self, _cmd) UIEdgeInsetsValue];
}

-(void)setSafePanAreaInsets:(UIEdgeInsets)safePanAreaInsets{
    objc_setAssociatedObject(self, @selector(safePanAreaInsets), [NSValue valueWithUIEdgeInsets:safePanAreaInsets], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)handlePanGesture:(UIPanGestureRecognizer *)recognizer{
    
    UIGestureRecognizerState recState =  recognizer.state;
    switch (recState) {
        case UIGestureRecognizerStateBegan:
            
            break;
        case UIGestureRecognizerStateChanged:
        {
 
            CGPoint translation = [recognizer translationInView:self.superview];
            recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x, recognizer.view.center.y + translation.y);
            NSLog(@"%@",NSStringFromCGPoint(recognizer.view.center));
            
        }
            break;
        case UIGestureRecognizerStateEnded:
        {
            [UIView animateWithDuration:0.2 animations:^{
                recognizer.view.center = [self updateCenter:recognizer.view.center];
            }];
            
        }
            break;
            
        default:
            break;
    }
    
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.superview];
}

-(CGPoint)updateCenter:(CGPoint)center{
    CGPoint stopPoint = CGPointMake(center.x, center.y);
    if (stopPoint.y + self.frame.size.height/2.0 >= (self.superview.bounds.size.height - self.safePanAreaInsets.bottom)) {
        stopPoint = CGPointMake(stopPoint.x,
                                (self.superview.bounds.size.height - self.safePanAreaInsets.bottom) - self.frame.size.height/2.0);
    }
    if (stopPoint.x - self.frame.size.width/2.0 <= self.safePanAreaInsets.left) {
        stopPoint = CGPointMake(self.frame.size.width/2.0 + self.safePanAreaInsets.left, stopPoint.y);
    }
    if (stopPoint.x + self.frame.size.width/2.0 >= (self.superview.bounds.size.width - self.safePanAreaInsets.right)) {
        stopPoint = CGPointMake((self.superview.bounds.size.width - self.safePanAreaInsets.right) - self.frame.size.width/2.0, stopPoint.y);
    }
    if (stopPoint.y - self.frame.size.height/2.0 <= self.safePanAreaInsets.top) {
        stopPoint = CGPointMake(stopPoint.x,
                                self.frame.size.height/2.0 + self.safePanAreaInsets.top);
    }
    UIView <UIViewPanPositionSerializing>*view = self;
    if ([view respondsToSelector:@selector(panAtPosition:)]) {
        [view panAtPosition:stopPoint];
    }
    return stopPoint;
}

@end
