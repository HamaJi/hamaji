//
//  UIView+Aji.h
//  Bet365
//
//  Created by HHH on 2018/7/4.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Aji)

IB_DESIGNABLE
@property (nonatomic, assign)CGFloat x;
@property (nonatomic, assign)CGFloat y;

@property(nonatomic, assign) IBInspectable CGFloat borderWidth;

@property(nonatomic, assign) IBInspectable UIColor *borderColor;

@property(nonatomic, assign) IBInspectable CGFloat cornerRadius;

+ (instancetype)instantiateFromNib;

@end
