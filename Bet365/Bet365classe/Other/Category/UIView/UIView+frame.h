//
//  UIView+frame.h
//  CAIPIAO
//
//  Created by apple on 17/2/16.
//  Copyright © 2017年 pony. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (frame)

@property CGFloat ycz_x;

@property CGFloat ycz_y;

@property CGFloat ycz_width;

@property CGFloat ycz_height;

@property CGFloat ycz_centerX;

@property CGFloat ycz_centerY;

@property CGFloat bottom;

@property CGFloat top;

@property CGFloat right;

@property CGSize  size;
+ (instancetype)instanceViewFromXib;

+ (UIViewController *)findViewController:(UIView *)sourceView;

@end
