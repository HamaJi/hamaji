//
//  UIView+CacheImage.h
//  Bet365
//
//  Created by jesse on 2018/10/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (CacheImage)
-(void)returnImageBy:(NSString *)url CacheOrUpdateCompleted:(void(^)(UIImage *imageCache))Completerd;
@end
