//
//  UIView+shadow.m
//  Bet365
//
//  Created by luke on 2018/9/18.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "UIView+shadow.h"

@implementation UIView (shadow)

- (UIView *)shadowForColor:(UIColor *)color shadowWidth:(CGFloat)shadowWidth shadowType:(UIViewShadowType)shadowType
{
    self.layer.shadowColor = color.CGColor;
    self.layer.shadowOpacity = 0.8;
    self.layer.shadowOffset = CGSizeMake(0, 0);
    UIBezierPath *path = [UIBezierPath bezierPath];
    float width = self.bounds.size.width;
    float height = self.bounds.size.height;
    float x = self.bounds.origin.x;
    float y = self.bounds.origin.y;
    float addWH = shadowWidth;
    CGPoint topLeft      = self.bounds.origin;
    CGPoint topMiddle    = CGPointMake(x+(width/2),y-addWH);
    CGPoint topRight     = CGPointMake(x+width,y);
    CGPoint rightMiddle  = CGPointMake(x+width+addWH,y+(height/2));
    CGPoint bottomRight  = CGPointMake(x+width,y+height);
    CGPoint bottomMiddle = CGPointMake(x+(width/2),y+height+addWH);
    CGPoint bottomLeft   = CGPointMake(x,y+height);
    CGPoint leftMiddle   = CGPointMake(x-addWH,y+(height/2));
    if (shadowType == UIViewShadowTypeAll) {
        [path moveToPoint:topLeft];
        [path addQuadCurveToPoint:topRight
                     controlPoint:topMiddle];
        [path addQuadCurveToPoint:bottomRight
                     controlPoint:rightMiddle];
        [path addQuadCurveToPoint:bottomLeft
                     controlPoint:bottomMiddle];
        [path addQuadCurveToPoint:topLeft
                     controlPoint:leftMiddle];
        self.layer.shadowPath = path.CGPath;
        return self;
    }
    if (shadowType & UIViewShadowTypeTop) {
        [path moveToPoint:topLeft];
        [path addQuadCurveToPoint:topRight
                     controlPoint:topMiddle];
    }
    if (shadowType & UIViewShadowTypeBottom) {
        [path moveToPoint:bottomLeft];
        [path addQuadCurveToPoint:bottomRight
                     controlPoint:bottomMiddle];
    }
    if (shadowType & UIViewShadowTypeLeft) {
        [path moveToPoint:topLeft];
        [path addQuadCurveToPoint:bottomLeft
                     controlPoint:leftMiddle];
    }
    if (shadowType & UIViewShadowTypeRight) {
        [path moveToPoint:topRight];
        [path addQuadCurveToPoint:rightMiddle
                     controlPoint:bottomRight];
    }
    self.layer.shadowPath = path.CGPath;
    return self;
}

@end
