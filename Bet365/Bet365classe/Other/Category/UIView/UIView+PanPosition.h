//
//  UIView+SwipePosition.h
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/12/4.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol UIViewPanPositionSerializing <NSObject>

@required

-(void)panAtPosition:(CGPoint)position;

@end

@interface UIView (PanPosition)

@property (nonatomic)BOOL beAllowedPanPostion;

@property (nonatomic)UIEdgeInsets safePanAreaInsets;

//@property (nonatomic) NSString *cachePositionKey;

@end

NS_ASSUME_NONNULL_END
