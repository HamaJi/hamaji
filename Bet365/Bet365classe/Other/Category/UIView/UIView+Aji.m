//
//  UIView+Aji.m
//  Bet365
//
//  Created by HHH on 2018/7/4.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "UIView+Aji.h"

@implementation UIView (Aji)

-(CGFloat)borderWidth{
    return self.borderWidth;
}
-(void)setBorderWidth:(CGFloat)borderWidth{
    self.layer.borderWidth = borderWidth;
}


- (UIColor *)borderColor{
    return self.borderColor;
    
}

- (CGFloat)cornerRadius{
    return self.cornerRadius;
}



- (void)setBorderColor:(UIColor *)borderColor{
    self.layer.borderColor = borderColor.CGColor;
}

- (void)setCornerRadius:(CGFloat)cornerRadius{
    self.layer.cornerRadius = cornerRadius;
    self.layer.masksToBounds = YES;
}

+ (instancetype)instantiateFromNib{
    NSString *name = [NSString stringWithFormat:@"%@", [self class]];
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:name owner:nil options:nil];
    return [views firstObject];
}
@end
