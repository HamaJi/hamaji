//
//  UIView+frame.m
//  CAIPIAO
//
//  Created by apple on 17/2/16.
//  Copyright © 2017年 pony. All rights reserved.
//

#import "UIView+frame.h"

@implementation UIView (frame)

//获取父控制器
+ (UIViewController *)findViewController:(UIView *)sourceView
{
    id target=sourceView;
    while (target) {
        target = ((UIResponder *)target).nextResponder;
        if ([target isKindOfClass:[UIViewController class]]) {
            break;
        }
    }
    return target;
}

+ (instancetype)instanceViewFromXib
{
    return [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil].firstObject;
}

- (CGFloat)ycz_x
{
    return self.frame.origin.x;
}

- (void)setYcz_x:(CGFloat)ycz_x
{
    CGRect rect = self.frame;
    rect.origin.x = ycz_x;
    self.frame = rect;
}

- (CGFloat)ycz_y
{
    return self.frame.origin.y;
}

- (void)setYcz_y:(CGFloat)ycz_y
{
    CGRect rect = self.frame;
    rect.origin.y = ycz_y;
    self.frame = rect;
}

- (CGFloat)ycz_width
{
    return self.frame.size.width;
}

- (void)setYcz_width:(CGFloat)ycz_width
{
    CGRect rect = self.frame;
    rect.size.width = ycz_width;
    self.frame = rect;
}

- (CGFloat)ycz_height
{
    return self.frame.size.height;
}

- (void)setYcz_height:(CGFloat)ycz_height
{
    CGRect rect = self.frame;
    rect.size.height = ycz_height;
    self.frame = rect;
}

- (CGFloat)ycz_centerX
{
    return self.center.x;
}

- (void)setYcz_centerX:(CGFloat)ycz_centerX
{
    CGPoint point = self.center;
    point.x = ycz_centerX;
    self.center = point;
}

- (CGFloat)ycz_centerY
{
    return self.center.y;
}

- (void)setYcz_centerY:(CGFloat)ycz_centerY
{
    CGPoint point = self.center;
    point.y = ycz_centerY;
    self.center = point;
}
-(void)setBottom:(CGFloat)bottom{
    CGRect frame = self.frame;
    frame.origin.y = bottom-frame.size.height;
    self.frame = frame;
}
-(CGFloat)bottom{
    return self.frame.origin.y+self.frame.size.height;
}
-(void)setRight:(CGFloat)right{
    CGRect frame = self.frame;
    frame.origin.x = right-frame.size.width;
    self.frame = frame;
}
-(CGFloat)right{
    return self.frame.origin.x+self.frame.size.width;
}
-(void)setSize:(CGSize)size{
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}
-(CGSize)size{
    return self.frame.size;
}
@end
