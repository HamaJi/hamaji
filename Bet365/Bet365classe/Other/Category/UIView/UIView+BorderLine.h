//
//  UIView+BorderLine.h
//  Bet365
//
//  Created by luke on 2018/2/1.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_OPTIONS(NSUInteger, UIBorderSideType) {
    UIBorderSideTypeAll  = 0,
    UIBorderSideTypeTop = 1 << 0,
    UIBorderSideTypeBottom = 1 << 1,
    UIBorderSideTypeLeft = 1 << 2,
    UIBorderSideTypeRight = 1 << 3,
};

@interface UIView (BorderLine)

- (UIView *)borderForColor:(UIColor *)color borderWidth:(CGFloat)borderWidth borderType:(UIBorderSideType)borderType;

/**
 横向虚线

 @param lineLength 单个虚线宽度
 @param lineSpacing 虚线间距
 @param lineColor 虚线颜色
 */
- (void)drawDashLine:(CGFloat)lineLength lineSpacing:(int)lineSpacing lineColor:(UIColor *)lineColor;
@end
