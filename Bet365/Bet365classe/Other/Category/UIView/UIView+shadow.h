//
//  UIView+shadow.h
//  Bet365
//
//  Created by luke on 2018/9/18.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_OPTIONS(NSUInteger, UIViewShadowType) {
    UIViewShadowTypeAll  = 0,                  //阴影四周都有
    UIViewShadowTypeTop = 1 << 0,              //阴影上
    UIViewShadowTypeBottom = 1 << 1,           //阴影下
    UIViewShadowTypeLeft = 1 << 2,             //阴影左
    UIViewShadowTypeRight = 1 << 3,            //阴影右
};

@interface UIView (shadow)

- (UIView *)shadowForColor:(UIColor *)color shadowWidth:(CGFloat)shadowWidth shadowType:(UIViewShadowType)shadowType;

@end
