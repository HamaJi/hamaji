//
//  UIView+lotteryTypeJD.m
//  Bet365
//
//  Created by jesse on 2018/10/19.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "UIView+lotteryTypeJD.h"
@implementation UIView (lotteryTypeJD)
//获取frame值
-(CGFloat)judgeTitleViewFrame:(LotteryType)type{
    CGFloat normalH = 100;
    CGFloat doubleH = 128;
    CGFloat thirdH = 156;
    kFrameType fmType =  [self screeningType:type];
    return (fmType==NMType)?normalH:(fmType==DBType)?doubleH:thirdH;
}
//获取当前frmaeType
-(kFrameType)getFrameType:(LotteryType)type{
    return [self screeningType:type];
}
//获取当前是否为特殊类型
-(BOOL)getSpecailValue:(LotteryType)type{
    return (type==LotteryType_CQSSC)?YES:NO;
}
-(kFrameType)screeningType:(LotteryType)type{
//    if (IS_IPHONE_5s) {
//        if (type == LotteryType_CQSSC||type == LotteryType_GD11TO5||type == LotteryType_GDHL10||type == LotteryType_BJPK10) {
//            return DBType;
//        }else if (type==LotteryType_BJKL8){
//            return THType;
//        }else{
//            return NMType;
//        }
//    }else{
//        if (type == LotteryType_SIX||type == LotteryType_BJPK10||type == LotteryType_BJKL8||type == LotteryType_GDHL10) {
//            if (WIDTH>=414.0) {
//                return THType;
//            }
//            return DBType;
//        }else{
//            return NMType;
//        }
//    }
    if (type == LotteryType_SIX||
        type == LotteryType_BJPK10||
        type == LotteryType_BJKL8||
        type == LotteryType_GDHL10) {
        if (WIDTH>=414.0) {
            return THType;
        }
        return DBType;
    }else{
        return NMType;
    }
}
@end
