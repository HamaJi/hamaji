//
//  UIView+lotteryTypeJD.h
//  Bet365
//
//  Created by jesse on 2018/10/19.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSUInteger,kFrameType){
    NMType = 1,//正常
    DBType = 2,//双层
    THType = 3 //特殊
};
@interface UIView (lotteryTypeJD)
//判断当前彩种titleView修改移位(judge彩种，屏幕大小)
-(CGFloat)judgeTitleViewFrame:(LotteryType)type;
-(kFrameType)getFrameType:(LotteryType)type;
-(BOOL)getSpecailValue:(LotteryType)type;
@end
