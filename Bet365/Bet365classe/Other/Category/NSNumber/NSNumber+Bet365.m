//
//  NSNumber+Bet365.m
//  Bet365
//
//  Created by super_小鸡 on 2019/6/10.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "NSNumber+Bet365.h"

@implementation NSNumber (Bet365)

- (NSDecimalNumber *)decimalNumberByRoundingDown{
    NSString *temp = [NSString stringWithFormat:@"%@",[self stringValue]];
    NSDecimalNumber *numResult = [NSDecimalNumber decimalNumberWithString:temp];
    NSDecimalNumberHandler *roundUp = [NSDecimalNumberHandler
                                       decimalNumberHandlerWithRoundingMode:NSRoundDown
                                       scale:2
                                       raiseOnExactness:NO
                                       raiseOnOverflow:NO
                                       raiseOnUnderflow:NO
                                       raiseOnDivideByZero:YES];
    NSDecimalNumber *decimalNaumber = [numResult decimalNumberByRoundingAccordingToBehavior:roundUp];
    return decimalNaumber;
}

- (NSString *)decimalNumberByRoundingDownString{
    NSString *temp = [NSString stringWithFormat:@"%@",[self stringValue]];
    NSDecimalNumber *numResult = [NSDecimalNumber decimalNumberWithString:temp];
    NSDecimalNumberHandler *roundUp = [NSDecimalNumberHandler
                                       decimalNumberHandlerWithRoundingMode:NSRoundDown
                                       scale:2
                                       raiseOnExactness:NO
                                       raiseOnOverflow:NO
                                       raiseOnUnderflow:NO
                                       raiseOnDivideByZero:YES];
    NSDecimalNumber *decimalNaumber = [numResult decimalNumberByRoundingAccordingToBehavior:roundUp];
    return [decimalNaumber stringValue];
}

+ (NSDecimalNumber *)decimalNumber:(double)value ByRoundingDown:(NSUInteger)count{
    NSString *temp = [NSString stringWithFormat:@"%lf",value];
    NSDecimalNumber *numResult = [NSDecimalNumber decimalNumberWithString:temp];
    NSDecimalNumberHandler *roundUp = [NSDecimalNumberHandler
                                       decimalNumberHandlerWithRoundingMode:NSRoundDown
                                       scale:count
                                       raiseOnExactness:NO
                                       raiseOnOverflow:NO
                                       raiseOnUnderflow:NO
                                       raiseOnDivideByZero:YES];
    NSDecimalNumber *decimalNaumber = [numResult decimalNumberByRoundingAccordingToBehavior:roundUp];
    return decimalNaumber;
}
@end
