//
//  NSNumber+Bet365.h
//  Bet365
//
//  Created by super_小鸡 on 2019/6/10.
//  Copyright © 2019 jesse. All rights reserved.
//



NS_ASSUME_NONNULL_BEGIN

@interface NSNumber (Bet365)

- (NSDecimalNumber *)decimalNumberByRoundingDown;


- (NSString *)decimalNumberByRoundingDownString;

+ (NSDecimalNumber *)decimalNumber:(double)value ByRoundingDown:(NSUInteger)count;

@end

NS_ASSUME_NONNULL_END
