//
//  UIAlertAction+Tag.m
//  Bet365
//
//  Created by HHH on 2018/7/25.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "UIAlertAction+Tag.h"

@implementation UIAlertAction (Tag)
-(NSInteger)tag{
    return [objc_getAssociatedObject(self, _cmd) integerValue];
}
-(void)setTag:(NSInteger)tag{
    objc_setAssociatedObject(self, @selector(tag), @(tag), OBJC_ASSOCIATION_ASSIGN);
}
@end
