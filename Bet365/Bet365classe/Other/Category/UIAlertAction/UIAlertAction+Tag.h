//
//  UIAlertAction+Tag.h
//  Bet365
//
//  Created by HHH on 2018/7/25.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertAction (Tag)

@property (nonatomic)NSInteger tag;

@end
