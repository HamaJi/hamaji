//
//  CALayer+LayerColor.m
//  batzb
//
//  Created by Deep river on 2016/11/18.
//  Copyright © 2016年 DeepRiver. All rights reserved.
//

#import "CALayer+LayerColor.h"

@implementation CALayer (LayerColor)

- (void)setBorderColorFromUIColor:(UIColor *)color{
    self.borderColor = color.CGColor;
}

@end
