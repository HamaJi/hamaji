//
//  CALayer+LayerColor.h
//  batzb
//
//  Created by Deep river on 2016/11/18.
//  Copyright © 2016年 DeepRiver. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CALayer (LayerColor)

- (void)setBorderColorFromUIColor:(UIColor *)color;

@end
