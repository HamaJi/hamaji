//
//  UIViewController+ShareInstance.h
//  Bet365
//
//  Created by adnin on 2019/2/28.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController ()

-(instancetype)shareInstanceViewController:(NSString *)type;

@end
