//
//  UIViewController+Aji.h
//  Bet365
//
//  Created by HHH on 2018/8/1.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BRPickerView.h>

@interface UIViewController (Aji)

/**
 是否已加入ViewController
 */
@property (nonatomic)BOOL isContainAtWindow;

- (void)showDatePickerdefaultSelValue:(NSString *)defaultSelValue
                              minDate:(NSDate *)minDate
                          resultBlock:(BRDateResultBlock)resultBlock;

- (void)alertWithTitle:(NSString *)title message:(NSString *)message cancelActionTitle:(NSString *)cancelActionTitle cancelHandler:(void (^ __nullable)(UIAlertAction *action))cancelHandler sureActionTitle:(NSString *)sureActionTitle sureHandler:(void (^ __nullable)(UIAlertAction *action))sureHandler;

- (void)alertWithSureTitle:(NSString *)title message:(NSString *)message;

- (void)createRightBarItemWithTitle:(NSString *_Nullable)title action:(SEL)action;

- (void)creatLeftBarItemWithImage:(NSString *)imageName action:(SEL)action;

- (void)setBackgroundColorWithColor:(UIColor *_Nullable)color;


- (void)createLeftBarItemWithTitle:(NSString *)title textColor:(UIColor *)textColor action:(SEL)action;

+(BOOL)isCurrentViewControllerVisible:(UIViewController *)viewController;


-(void)presentPopViewController:(UIViewController *)viewControllerToPresent completion:(void (^)(void))completion;

@end
