//
//  NaviHeaderView.h
//  Bet365
//
//  Created by HHH on 2018/7/31.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NaviHeaderView : UIView


-(instancetype)initWithMoney:(NSNumber *)money Account:(NSString *)account;

@property (nonatomic,strong)NSNumber *money;

@property (nonatomic,copy)NSString *account;
@end
