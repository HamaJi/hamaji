//
//  UIViewController+TabBar.m
//  Bet365
//
//  Created by HHH on 2018/8/1.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "UIViewController+TabBar.h"

@implementation UIViewController (TabBar)
-(void)setTabBarItemImage:(id)image selectedImage:(id)selectImage placheImage:(UIImage *)placheImage title:(NSString *)titleString {
    UITabBarItem *tabBarItem = [[UITabBarItem alloc ]init];
    tabBarItem.title=titleString;
    self.tabBarItem=tabBarItem;
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    
    if ([image isKindOfClass:[UIImage class]]) {
        tabBarItem.image=[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }else if ([image isKindOfClass:[NSString class]]){
        NSString *imageStr = image;
        if (![imageStr hasPrefix:@"http"]) {
            tabBarItem.image=[[UIImage imageNamed:imageStr] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        }else{
            tabBarItem.image=[placheImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            @weakify(self);
            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:imageStr] options:SDWebImageDownloaderContinueInBackground progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
                
            } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                if (!error && finished) {
                    @strongify(self);
                    self.tabBarItem.image = [[UIImage imageNamed:@"tabbarJ_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                }
            }];
        }
    }
    
    if ([selectImage isKindOfClass:[UIImage class]]) {
        tabBarItem.selectedImage=[selectImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }else if ([selectImage isKindOfClass:[NSString class]]){
        NSString *imageStr = selectImage;
        if (![imageStr hasPrefix:@"http"]) {
            tabBarItem.selectedImage=[[UIImage imageNamed:imageStr] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        }else{
            tabBarItem.selectedImage=[placheImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            @weakify(self);
            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:imageStr] options:SDWebImageDownloaderContinueInBackground progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
                
            } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                if (!error && finished) {
                    @strongify(self);
                    self.tabBarItem.selectedImage = [[UIImage imageNamed:@"tabbarJ_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                }
            }];
        }
    }
}



@end
