//
//  UIViewController+queryContent.m
//  Bet365
//
//  Created by jesse on 2019/1/1.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "UIViewController+queryContent.h"
#import "Bet365SecurityViewController.h"
#import "Bet365FootBallViewController.h"
#import "CustomWebViewController.h"
@implementation UIViewController (queryContent)



- (NSArray *)queryContentAttribute:(NSArray *)contentArr attribute:(id)buteComplement property:(NSString *)property{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"game == %@",buteComplement];
    NSArray *judge_tempArr = [contentArr filteredArrayUsingPredicate:predicate];
    return (judge_tempArr.count>0)?judge_tempArr:nil;
}

-(void)queryCurrenGameJson:(NSMutableArray *)cateSource for:(NSString *)completionCate{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cate == %@",completionCate];
    NSArray *judge_tempArr = [LOTTERY_FACTORY.lotteryData filteredArrayUsingPredicate:predicate];
    if (judge_tempArr.count==0)return;
    for (KFCPHomeGameJsonModel *all_model in judge_tempArr) {
        for (NSInteger i = 0; i<LOTTERY_FACTORY.allOpenInfo.nowGameValueArr.count; i++) {
            NSArray *config_arr = LOTTERY_FACTORY.allOpenInfo.nowGameValueArr[i];
            lotteryPreModel *model = config_arr[1];
            if ([all_model.GameId isEqualToString:model.gameId]) {
                [cateSource addObject:config_arr];
            }
        }
    }
}
- (void)queryDescriptionGame:(NSMutableArray *)cateSource for:(NSString *)completionCate{
    if ([cateSource[[completionCate intValue]] isKindOfClass:[NSArray class]])return;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cate == %@",completionCate];
    NSArray *judge_tempArr = [LOTTERY_FACTORY.lotteryData filteredArrayUsingPredicate:predicate];
    [cateSource replaceObjectAtIndex:[completionCate intValue] withObject:judge_tempArr];
}
- (void)surePushControllerComplement:(NSString *)complementStr gameType:(NSString *)gameType liveC:(NSString *)liveCode GameKind:(NSString *)gameKind{
        
    //蛤蟆吉TODO 试玩游戏跳转
    if ([liveCode isEqualToString:@"ft"] ||
              [liveCode isEqualToString:@"bk"]){
        Bet365FootBallViewController *vc = [[Bet365FootBallViewController alloc] init];
        vc.sportType = liveCode;
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed  = YES;
    }else{
        CustomWebViewController *vc = [[CustomWebViewController alloc] initWithNibName:@"CustomWebViewController" bundle:nil];
        [vc loadCode:liveCode Type:gameType Kind:gameKind ValiCode:complementStr Operation:CustomWebRequestOperation_WK];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
        vc.isOpenSafari = YES;
    }
}

- (void)bounceAlertTestPlayForParamaterType:(NSString *)gameType forLiveCode:(NSString *)liveCode GameKind:(NSString *)kind{
    
    if ([self detectionAGtest:liveCode type:gameType GameKind:kind]) {
        BouncedViewController *bounce_Controller = [[BouncedViewController alloc] init];
        bounce_Controller.delegate = self;
        [bounce_Controller setGameType:gameType];
        [bounce_Controller setLiveCode:liveCode];
        [bounce_Controller setGameKind:kind];
        bounce_Controller.transitioningDelegate = self;
        bounce_Controller.modalPresentationStyle = UIModalPresentationCustom;
        [self presentViewController:bounce_Controller animated:YES completion:nil];
    }
}
/**初始化密码安全验证*/
- (void)initPwdAlertPushParameter:(NSArray *)complment withForpersonMessage:(NSDictionary *)messageComplement clickOther:(void (^)())complement{
    Bet365SecurityViewController *security_c = [[Bet365SecurityViewController alloc] init];
    security_c.customC = ^{
        complement();
    };
    security_c.backC = ^{
        [[NAVI_MANAGER getCurrentVC].navigationController popToRootViewControllerAnimated:NO];
        [NAVI_MANAGER getCurrentVC].tabBarController.selectedIndex = 0;
    };
    [security_c setSecurity_msg:complment];
    [security_c setPersonDic:[[NSMutableDictionary alloc]initWithDictionary:messageComplement]];
    security_c.transitioningDelegate = self;
    security_c.modalPresentationStyle = UIModalPresentationCustom;
    [self presentViewController:security_c animated:YES completion:nil];
}
/**查询当前id是否存在进行广告**/
-(void)queryGameForAdver:(NSString *)gameId Completion:(void (^)(advertModel *adver_m,NSInteger index))completions{
    if ([BET_CONFIG.adver_config safeObjectAtIndex:0]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"gameId == %@",gameId];
        NSArray *preResultArr = [BET_CONFIG.adver_config filteredArrayUsingPredicate:predicate];
        if ([preResultArr safeObjectAtIndex:0]) {
            advertModel *adverM = preResultArr[0];
            if(adverM.enterValue)return;
            completions?completions(adverM,[BET_CONFIG.adver_config indexOfObject:adverM]):nil;
        }
    }
}

-(BOOL)detectionAGtest:(NSString *)liveCode type:(NSString *)gameType GameKind:(NSString *)kind{
    if (!USER_DATA_MANAGER.isLogin || [USER_DATA_MANAGER.userInfoData.type isEqualToString:@"TEST"] || [USER_DATA_MANAGER.userInfoData.type isEqualToString:@"VHY"]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(game = %@) AND (firstKind = %@)",liveCode,kind];
        TestKindQuery *kindModel = [[ALL_DATA.all filteredArrayUsingPredicate:predicate] safeObjectAtIndex:0];
        if (kindModel.testOpen == 2) {
            //允许用户试玩
            return YES;
        }else{
            if (!USER_DATA_MANAGER.isLogin) {
                [SVProgressHUD showErrorWithStatus:@"请登陆"];
            }else if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"TEST"]){
                [SVProgressHUD showErrorWithStatus:@"该游戏不支持试玩"];
            }else if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"VHY"]){
                [SVProgressHUD showErrorWithStatus:@"您的账号没有权限，请联系您的上级开通"];
            }
            return NO;
        }
    }
    return YES;
}

- (BOOL)testPlayForGameType:(NSString *)gameType liveCode:(NSString *)liveCode gameKind:(NSString *)kind
{
    
    if ([liveCode isEqualToString:@"lucky"] &&
        USER_DATA_MANAGER.isLogin) {
        //幸运棋牌特殊处理，只要有登陆，不关乎会员类型，都可以进入
        return YES;
    }else if (!USER_DATA_MANAGER.isLogin || [USER_DATA_MANAGER.userInfoData.type isEqualToString:@"TEST"] || [USER_DATA_MANAGER.userInfoData.type isEqualToString:@"VHY"]) {
        if ([self detectionAGtest:liveCode type:gameType GameKind:kind]) {
            [self bounceAlertTestPlayForParamaterType:gameType forLiveCode:liveCode GameKind:kind];
        }
        return NO;
    }
    return YES;
}

#pragma mark - UIViewControllerTransitioningDelegate
-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
        return [DismissingTopAnimator new];
}
-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
        return [PresentingTopAnimator new];
}
@end
