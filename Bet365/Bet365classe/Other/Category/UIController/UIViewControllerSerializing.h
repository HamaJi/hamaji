//
//  UIViewControllerSerializing.h
//  Bet365
//
//  Created by adnin on 2019/3/1.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UIViewControllerSerializing <NSObject>

@required

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters;

@end
