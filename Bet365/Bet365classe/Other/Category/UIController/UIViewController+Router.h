//
//  UIViewController+Router.h
//  Bet365
//
//  Created by adnin on 2019/2/28.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainAllocationModel.h"

#if UIKIT_STRING_ENUMS
typedef NSString RouterKey NS_EXTENSIBLE_STRING_ENUM;
#else
typedef NSString RouterKey;
#endif

FOUNDATION_EXTERN RouterKey *const kRouterHome;

FOUNDATION_EXTERN RouterKey *const kRouterRecordCpbetlist;

FOUNDATION_EXTERN RouterKey *const kRouterChatIndex;

FOUNDATION_EXTERN RouterKey *const kRouterChatRoom;

FOUNDATION_EXTERN RouterKey *const kRouterChatRoot;

FOUNDATION_EXTERN RouterKey *const kRouterWithdrwalOrder;

FOUNDATION_EXTERN RouterKey *const kRouterUserBank;

FOUNDATION_EXTERN RouterKey *const kRouterNotice;

FOUNDATION_EXTERN RouterKey *const kRouterChatScrollBottom;

FOUNDATION_EXTERN RouterKey *const kRouterwzGlory;

FOUNDATION_EXTERN RouterKey *const kRouterWzryBetRecord;

FOUNDATION_EXTERN RouterKey *const kRouterSportsGame;

FOUNDATION_EXTERN RouterKey *const kRouterZXKF;

FOUNDATION_EXTERN RouterKey *const kRouterEleDelite;

FOUNDATION_EXTERN RouterKey *const kRouterKYgame;

FOUNDATION_EXTERN RouterKey *const kRouterJBgame;

FOUNDATION_EXTERN RouterKey *const kRouterElectronic;

FOUNDATION_EXTERN RouterKey *const kRouterLive;

FOUNDATION_EXTERN RouterKey *const kRouterChess;

FOUNDATION_EXTERN RouterKey *const kRouterFish;

FOUNDATION_EXTERN RouterKey *const kRouterSports;

FOUNDATION_EXTERN RouterKey *const kRouterGaming;

FOUNDATION_EXTERN RouterKey *const kRouterTulongban;

FOUNDATION_EXTERN RouterKey *const kRouterLottery;

FOUNDATION_EXTERN RouterKey *const kRouterYHHD;

FOUNDATION_EXTERN RouterKey *const kRouterHTTPWEB;

FOUNDATION_EXTERN RouterKey *const kRouterHongbao;

FOUNDATION_EXTERN RouterKey *const kRouterAgentDeclare;

FOUNDATION_EXTERN RouterKey *const kRouterGCDT;

FOUNDATION_EXTERN RouterKey *const kRouterMoreLottery;

FOUNDATION_EXTERN RouterKey *const kRouterKJ;

FOUNDATION_EXTERN RouterKey *const kRouterZS;

FOUNDATION_EXTERN RouterKey *const kRouterActivityHall;

FOUNDATION_EXTERN RouterKey *const kRouterActivityPrmt;

FOUNDATION_EXTERN RouterKey *const kRouterActivityMine;

FOUNDATION_EXTERN RouterKey *const kRouterActivityLottery;

FOUNDATION_EXTERN RouterKey *const kRouterActivityRedenvelope;

FOUNDATION_EXTERN RouterKey *const kRouterRecharge;

FOUNDATION_EXTERN RouterKey *const kRouterCenter;

FOUNDATION_EXTERN RouterKey *const kRouterConver;

FOUNDATION_EXTERN RouterKey *const kRouterLoan;

FOUNDATION_EXTERN RouterKey *const kRouterLiveBetRecord;

FOUNDATION_EXTERN RouterKey *const kRouterMessage;

FOUNDATION_EXTERN RouterKey *const kRouterAgentPersonal;

FOUNDATION_EXTERN RouterKey *const kRouterAgentShowTeam;

FOUNDATION_EXTERN RouterKey *const kRouterAgentSubList;

FOUNDATION_EXTERN RouterKey *const kRouterAgentAddUser;

FOUNDATION_EXTERN RouterKey *const kRouterDraw;

FOUNDATION_EXTERN RouterKey *const kRouterBills;

FOUNDATION_EXTERN RouterKey *const kRouterAgentSpread;

FOUNDATION_EXTERN RouterKey *const kRouterChaseRecord;

FOUNDATION_EXTERN RouterKey *const kRouterPersonalData;

FOUNDATION_EXTERN RouterKey *const kRouterRechargeRcord;

FOUNDATION_EXTERN RouterKey *const kRouterRegister;

FOUNDATION_EXTERN RouterKey *const kRouterForget;

FOUNDATION_EXTERN RouterKey *const kRouterRegisterTest;

FOUNDATION_EXTERN RouterKey *const kRouterLogin;

FOUNDATION_EXTERN RouterKey *const kRouterSoundSwitch;

FOUNDATION_EXTERN RouterKey *const kRouterCleanCache;

FOUNDATION_EXTERN RouterKey *const kRouterMetrics;

FOUNDATION_EXTERN RouterKey *const kRouterExit;

FOUNDATION_EXTERN RouterKey *const kRouterGameCenter;

FOUNDATION_EXTERN RouterKey *const kRouterBetRecordChoose;

FOUNDATION_EXTERN RouterKey *const kRouterPersonalData;

FOUNDATION_EXTERN RouterKey *const kRouterRedpage;

FOUNDATION_EXTERN RouterKey *const kRouterRedPageWeekend;

FOUNDATION_EXTERN RouterKey *const kRouterRedPageRain;

FOUNDATION_EXTERN RouterKey *const kRouterSportsRules;

FOUNDATION_EXTERN RouterKey *const kRouterLotteryRules;

FOUNDATION_EXTERN RouterKey *const kRouterPlayExplanation;

FOUNDATION_EXTERN RouterKey *const kRouterPlayMethords;

FOUNDATION_EXTERN RouterKey *const kRouterYEBao;

FOUNDATION_EXTERN RouterKey *const kRouterLend;

FOUNDATION_EXPORT RouterKey *const kRouterGamesLobby;

FOUNDATION_EXPORT RouterKey *const kRouterSchemes;

FOUNDATION_EXPORT RouterKey *const kRouterLiveRedirect;

FOUNDATION_EXPORT RouterKey *const kRouterNoticeIndex;

FOUNDATION_EXPORT RouterKey *const kRouterNoticeLogin;

FOUNDATION_EXPORT RouterKey *const kRouterNoticeRegister;

FOUNDATION_EXPORT RouterKey *const kRouterImmediate;

FOUNDATION_EXPORT RouterKey *const kRouterGrowthValueDetailed;

FOUNDATION_EXPORT RouterKey *const kRouterTurnTable;

FOUNDATION_EXPORT RouterKey *const kRouterRedpageRule;

FOUNDATION_EXPORT RouterKey *const kRouterYHPrmt;



@interface UIViewController (Router)

@property (nonatomic)RouterKey *instanceRouterKey;

+(void)registerJumpRouterKey:(RouterKey *)routerKey toHandle:(void(^)(NSDictionary *parameters))handler;

+(void)registerInstanceRouterKey:(RouterKey *)routerKey toHandle:(UIViewController *(^)(NSDictionary *parameters))handler;

+(void)routerJumpToUrl:(RouterKey *)url;

+(void)routerJumpToAttr:(MenuAttrTemplate *)attr;

+(void)routerJumpToCode:(NSString *)code Type:(NSString *)type Kind:(NSString *)kind;

/**
 路由拼接

 @param url sport/game
 @param parse /bk
 */
+(void)routerJumpToUrl:(RouterKey *)url AppendingParse:(NSString *)parse;

+(void)routerShareInstanceByUrl:(RouterKey *)url toHandle:(void(^)(UIViewController *vc))handler;

+(BOOL)hasRegisterShareInstance:(RouterKey *)url;

+(BOOL)hasRegisterJump:(RouterKey *)url;

@end
