//
//  UIViewController+Bet365.h
//  Bet365
//
//  Created by HHH on 2018/8/1.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LukeTopWindow.h"
#import "NaviHeaderView.h"
#import "CWLateralSlideAnimator.h"
#import "UIViewController+CWLateralSlide.h"



@interface UIViewController (Bet365)

-(void)showGuidChatEnter;

-(void)palyRuleAction;

-(void)palyRuleActionForSubGame:(NSString *)gameId type:(currenGameType)type;

#pragma mark - 彩票跳转
+ (void)enterLotteryByLotteryId:(NSString *)lotteryId;

+ (void)enterLotteryByLotteryId:(NSString *)lotteryId isCredit:(BOOL)credit TuLongEnterValue:(BOOL)tulongValue;

#pragma mark - UserInfoUpdate
-(void)info_updateLoginStatus;

-(void)info_updateInfoData;

@end
