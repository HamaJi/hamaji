//
//  UICollectionViewController+tulongtool.m
//  Bet365
//
//  Created by jesse on 2018/8/3.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "UICollectionViewController+tulongtool.h"
#import "Bet365AlertSheet.h"
#import "Bet365Config.h"
@implementation UICollectionViewController (tulongtool)
-(void)presentToolTulong{
    [Bet365AlertSheet showChooseAlert:@"下注成功" Message:@"是否返回长龙排行榜" Items:@[@"取消",@"确定"] Handler:^(NSInteger index) {
        if (index==1) {
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            BET_CONFIG.successlongDragon = NO;
        }
    }];
}
@end
