//
//  UIViewController+queryContent.h
//  Bet365
//
//  Created by jesse on 2019/1/1.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BouncedViewController.h"
#import "advertModel.h"
@interface UIViewController (queryContent)<UIViewControllerTransitioningDelegate,BounceControllerDelegate>
- (NSArray *)queryContentAttribute:(NSArray *)contentArr attribute:(id)buteComplement property:(NSString *)property;
/**Alert**/
- (void)bounceAlertTestPlayForParamaterType:(NSString *)gameType forLiveCode:(NSString *)liveCode GameKind:(NSString *)kind;
/**pushinit_pwd密码初始化跳转**/
- (void)initPwdAlertPushParameter:(NSArray *)complment withForpersonMessage:(NSDictionary *)messageComplement clickOther:(void(^)())complement;
/*queryGameForAdver*/
- (void)queryGameForAdver:(NSString *)gameId Completion:(void(^)(advertModel *adver_m,NSInteger index))completions;
/*query_lottery*/
- (void)queryCurrenGameJson:(NSMutableArray *)cateSource for:(NSString *)completionCate;
/*query_Gamelottery*/
- (void)queryDescriptionGame:(NSMutableArray *)cateSource for:(NSString *)completionCate;
- (BOOL)testPlayForGameType:(NSString *)gameType liveCode:(NSString *)liveCode gameKind:(NSString *)kind;
@end
