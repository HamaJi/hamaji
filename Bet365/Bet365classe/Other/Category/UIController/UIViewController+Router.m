//
//  UIViewController+Router.m
//  Bet365
//
//  Created by adnin on 2019/2/28.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "UIViewController+Router.h"
#import <MGJRouter/MGJRouter.h>
#import "LukeSubUsersViewController.h"
#import "LukeReportMagViewController.h"
#import "LukeLiveViewController.h"
#import "KFCPActivityController.h"
#import "Bet365FootBallViewController.h"
#import "CustomWebViewController.h"
#import "YEBaoViewController.h"
#import <Crashlytics/Crashlytics.h>
#import "GcdtController.h"
#define JUMP_R0UTER(key)                                    \
(key ? [@"JUMP://" stringByAppendingString:key] : @"")     \


#define INSTANCE_R0UTER(key)                                \
(key ? [@"INSTANCE://" stringByAppendingString:key] : @"") \

RouterKey *const kRouterSoundSwitch = @"sound/switch";

RouterKey *const kRouterCleanCache = @"CleanCache";

RouterKey *const kRouterExit = @"exit";

@implementation UIViewController (Router)

+(void)load{
    
    [self registerJumpRouterKey:kRouterSoundSwitch toHandle:^(NSDictionary *parameters) {
        BET_CONFIG.userSettingData.isOpenMusic = !BET_CONFIG.userSettingData.isOpenMusic;
        [BET_CONFIG.userSettingData save];
    }];
    
    [self registerJumpRouterKey:kRouterExit toHandle:^(NSDictionary *parameters) {
        [USER_DATA_MANAGER loginOutCompleted:^(BOOL success) {
            [[NAVI_MANAGER getCurrentVC].navigationController popToRootViewControllerAnimated:YES];
            [JesseAppdelegate.tabbar setSelectedIndex:0];
        }];
    }];
    
    [self registerJumpRouterKey:kRouterCleanCache toHandle:^(NSDictionary *parameters) {
        [Bet365AlertSheet showChooseAlert:@"是否清空缓存" Message:@"清空App内所有数据，并且我们会关闭您的App，您可重新打开App获取相应配置" Items:@[@"直接清空",@"取消"] Handler:^(NSInteger index) {
            if (index == 0) {
                [Bet365Tool removeAllCacheOnCompletion:^{
                    [SVProgressHUD showSuccessWithStatus:@"清空成功"];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [CrashlyticsKit crash];
                    });
                }];
            }
        }];
    }];
}

+(void)registerJumpRouterKey:(RouterKey *)routerKey toHandle:(void(^)(NSDictionary *parameters))handler{
    
    [MGJRouter registerURLPattern:JUMP_R0UTER(routerKey) toHandler:^(NSDictionary *routerParameters) {
        NSMutableDictionary *parameters = @{}.mutableCopy;
        [parameters addEntriesFromDictionary:routerParameters[MGJRouterParameterUserInfo]];
        [parameters safeSetObject:routerKey forKey:@"routerKey"];
        BOOL allowed = ([[self class] respondsToSelector:@selector(beAllowedPushViewConrtoller:)] ?
                        [[self class] beAllowedPushViewConrtoller:parameters]:
                        YES);
        if ([routerParameters[MGJRouterParameterURL] isEqualToString:JUMP_R0UTER(routerKey)] && allowed) {
            
            NSUInteger idx = [TabBarTemplate tabBarIndexByRouterKey:routerKey];
            NSDictionary *info = routerParameters[MGJRouterParameterUserInfo];
            NSString *type = info[@"type"];
            if (idx != NSNotFound) {
                if (type.length > 0 && ![type isEqualToString:@"lucky"]) {
                    handler ? handler(routerParameters[MGJRouterParameterUserInfo]) : nil;
                }else{
                    [[NAVI_MANAGER getCurrentVC].navigationController popToRootViewControllerAnimated:YES];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [JesseAppdelegate.tabbar setSelectedIndex:idx];
                        UINavigationController *navi = [JesseAppdelegate.tabbar.childViewControllers safeObjectAtIndex:idx];
                        
                        if ([routerKey isEqualToString:kRouterGCDT]) {
                            UIViewController *vc = [navi.childViewControllers firstObject];
                            NSString *cate = [[routerParameters objectForKey:MGJRouterParameterUserInfo] objectForKey:@"cate"];
                            [((GcdtController *)vc) setCate:cate animate:YES];
                        }
                    });
                }
            }else{
                handler ? handler(routerParameters[MGJRouterParameterUserInfo]) : nil;
            }
            
        }
    }];
}

+(void)routerJumpToUrl:(RouterKey *)url{
    NSMutableString *mUrl = url.mutableCopy;
    if ([mUrl hasPrefix:@"#"]) {
        [mUrl deleteCharactersInRange:NSMakeRange(0, 1)];
    }
    if ([mUrl hasPrefix:@"/"]) {
        [mUrl deleteCharactersInRange:NSMakeRange(0, 1)];
    }
    NSMutableDictionary *paramers = [self routerUrlParamers:mUrl];
    if ([mUrl hasPrefix:@"http"]) {
        mUrl = kRouterHTTPWEB.mutableCopy;
    }else if ([mUrl hasSuffix:@".com"]){
        mUrl = kRouterHTTPWEB.mutableCopy;
    }
    else if(![self hasRegisterJump:mUrl]){
        mUrl = kRouterHTTPWEB.mutableCopy;
        [paramers setObject:[NSString stringWithFormat:@"%@%@",SerVer_Url,url] forKey:@"url"];
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"CMS ERROR,Request to the service ! \n ROUTER_ERROR:%@,",url.length ? url : @"NONE"]];
    }
    [MGJRouter openURL:JUMP_R0UTER(mUrl) withUserInfo:paramers completion:nil];
}

+(void)routerJumpToAttr:(MenuAttrTemplate *)attr{
    [self routerJumpToCode:attr.liveCode Type:attr.gameType Kind:attr.gameKind];
}

+(void)routerJumpToCode:(NSString *)code Type:(NSString *)type Kind:(NSString *)kind{
    
    NSMutableString *routerKey = kRouterSportsGame.mutableCopy;
    NSMutableDictionary *paramers = @{}.mutableCopy;
    if (code.length) {
        [paramers setObject:code forKey:@"code"];
    }
    if (type.length) {
        [paramers setObject:type forKey:@"type"];
    }
    if (kind.length) {
        [paramers setObject:kind forKey:@"kind"];
    }
    [MGJRouter openURL:JUMP_R0UTER(routerKey) withUserInfo:paramers completion:nil];
}

+(void)routerJumpToUrl:(RouterKey *)url AppendingParse:(NSString *)parse{
    if ([parse hasPrefix:@"/"]) {
        [self routerJumpToUrl:[url stringByAppendingString:parse]];
    }else{
        [self routerJumpToUrl:[NSString stringWithFormat:@"%@/%@",url,parse]];
    }
    
}

+(void)registerInstanceRouterKey:(RouterKey *)routerKey toHandle:(UIViewController *(^)(NSDictionary *parameters))handler{
    [MGJRouter registerURLPattern:INSTANCE_R0UTER(routerKey) toHandler:^(NSDictionary *routerParameters) {
        void(^block)(UIViewController *vc) = routerParameters[MGJRouterParameterUserInfo][@"block"];
        if (handler) {
            UIViewController *vc = handler(routerParameters[MGJRouterParameterUserInfo]);
            vc.instanceRouterKey = routerKey;
            block(vc);
        }else{
            block(nil);
        }
    }];
}

+(void)routerShareInstanceByUrl:(RouterKey *)url toHandle:(void(^)(UIViewController *vc))handler{
    NSMutableString *mUrl = url.mutableCopy;
    if ([mUrl hasPrefix:@"#"]) {
        [mUrl deleteCharactersInRange:NSMakeRange(0, 1)];
    }
    if ([mUrl hasPrefix:@"/"]) {
        [mUrl deleteCharactersInRange:NSMakeRange(0, 1)];
    }
    if (![self hasRegisterShareInstance:mUrl]) {
        [SVProgressHUD showErrorWithStatus:@"INSTANCE_R0UTER URL DEPLOY ERROR"];
         handler ? handler(nil) : nil;
        return;
    }
    if (handler) {
        NSMutableDictionary *paramers = [self routerUrlParamers:mUrl];
        [paramers setObject:handler forKey:@"block"];
        [MGJRouter openURL:INSTANCE_R0UTER(mUrl) withUserInfo:paramers completion:nil];
    }
}

+(BOOL)hasRegisterShareInstance:(RouterKey *)url{
    NSMutableString *mUrl = url.mutableCopy;
    [self routerUrlParamers:mUrl];
    if (!mUrl.length) {
        return NO;
    }
    return [MGJRouter canOpenURL:INSTANCE_R0UTER(mUrl)];
}

+(BOOL)hasRegisterJump:(RouterKey *)url{
    NSMutableString *mUrl = url.mutableCopy;
    [self routerUrlParamers:mUrl];
    if (!mUrl.length) {
        return NO;
    }
    return [MGJRouter canOpenURL:JUMP_R0UTER(mUrl)];
}


#pragma mark  - Private
+(NSMutableDictionary *)routerUrlParamers:(NSMutableString *)url{
    
    NSMutableDictionary *paramers = @{}.mutableCopy;
    __block NSMutableArray <NSString *>*appendingList = [[url componentsSeparatedByString:@"/"] mutableCopy];
//    RouterKey *head = [appendingList safeObjectAtIndex:0];

    void(^deleteSuffixWithoutRouterKey)(RouterKey *routerkey) = ^(RouterKey *routerkey){
        NSRange range = [url rangeOfString:routerkey];
        if (range.location == 0 &&
            range.length > 0) {
            NSString *paramerString = [url substringWithRange:NSMakeRange(range.length, url.length - range.length)];
            [url deleteCharactersInRange:NSMakeRange(range.length, url.length - range.length)];
//            [url deleteString:paramerString];
            if ([paramerString hasPrefix:@"/"]) {
                paramerString = [paramerString substringWithRange:NSMakeRange(1, paramerString.length - 1)];
                appendingList = [[paramerString componentsSeparatedByString:@"/"] mutableCopy];
            }else if ([paramerString hasPrefix:@"#/"]) {
                paramerString = [paramerString substringWithRange:NSMakeRange(2, paramerString.length - 2)];
                appendingList = [[paramerString componentsSeparatedByString:@"/"] mutableCopy];
            }else if ([paramerString hasPrefix:@"?"]) {
                
                paramerString = [paramerString substringWithRange:NSMakeRange(1, paramerString.length - 1)];
                NSArray *arr = [paramerString componentsSeparatedByCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"&"]];
                [arr enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    
                    
                    if ([obj isEqualToString:@"liveCode"]) {
                        [appendingList insertObject:[arr safeObjectAtIndex:idx + 1] atIndex:0];
                    }else if ([obj isEqualToString:@"gameType"]) {
                        [appendingList insertObject:[arr safeObjectAtIndex:idx + 1] atIndex:1];
                    }else if ([obj isEqualToString:@"gameKind"]) {
                        [appendingList insertObject:[arr safeObjectAtIndex:idx + 1] atIndex:2];
                    }
                }];
            }
//            else if ([paramerString hasPrefix:@"?"]) {
//                paramerString = [paramerString substringWithRange:NSMakeRange(1, paramerString.length - 1)];
//                NSArray *arr = [paramerString componentsSeparatedByCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"=&"]];
//                [arr enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                    if ([obj isEqualToString:@"liveCode"]) {
//                        [appendingList insertObject:[arr safeObjectAtIndex:idx + 1] atIndex:0];
//                    }else if ([obj isEqualToString:@"gameType"]) {
//                        [appendingList insertObject:[arr safeObjectAtIndex:idx + 1] atIndex:1];
//                    }else if ([obj isEqualToString:@"gameKind"]) {
//                        [appendingList insertObject:[arr safeObjectAtIndex:idx + 1] atIndex:2];
//                    }else if ([obj isEqualToString:@"firstKind"]) {
//                        [appendingList insertObject:[arr safeObjectAtIndex:idx + 1] atIndex:2];
//                    }
//                }];
//            }
        }
    };
    
    
    if ([url hasPrefix:kRouterGameCenter]) {
        deleteSuffixWithoutRouterKey(kRouterGameCenter);
        return paramers;
    }
    
    if ([url hasPrefix:kRouterGamesLobby]) {
        deleteSuffixWithoutRouterKey(kRouterGamesLobby);
        return paramers;
    }

    if ([url hasPrefix:kRouterGCDT]) {
        deleteSuffixWithoutRouterKey(kRouterGCDT);
        /** 购彩大厅*/
        [paramers safeSetObject:[appendingList safeObjectAtIndex:0] forKey:@"cate"];
        return paramers;
    }
    if ([url hasPrefix:kRouterLottery]) {
        deleteSuffixWithoutRouterKey(kRouterLottery);
        /** 彩票*/
        [paramers safeSetObject:[appendingList safeObjectAtIndex:0] forKey:@"lotteryID"];
        [paramers safeSetObject:[appendingList safeObjectAtIndex:1] forKey:@"playCode"];
        return paramers;
    }
    
    if ([url hasPrefix:kRouterLotteryRules]) {
        deleteSuffixWithoutRouterKey(kRouterLotteryRules);
        /** 彩票玩法说明*/
        [paramers safeSetObject:[appendingList safeObjectAtIndex:1] forKey:@"lotteryID"];
        [paramers safeSetObject:[appendingList safeObjectAtIndex:0] forKey:@"playCode"];
        return paramers;
    }
    
    if ([url hasPrefix:kRouterSportsGame]){
        deleteSuffixWithoutRouterKey(kRouterSportsGame);
        /** 体育*/
        [paramers safeSetObject:[appendingList safeObjectAtIndex:0] forKey:@"code"];
        [paramers safeSetObject:[appendingList safeObjectAtIndex:1] forKey:@"type"];
        [paramers safeSetObject:[appendingList safeObjectAtIndex:2] forKey:@"kind"];
        return paramers;
    }
    
    if ([url hasPrefix:kRouterAgentSubList]){
        deleteSuffixWithoutRouterKey(kRouterAgentSubList);
        /** 代理用户列表*/
        [paramers safeSetObject:[appendingList safeObjectAtIndex:0] forKey:@"account"];
        return paramers;
    }
    
    
    if ([url hasPrefix:kRouterBills]){
        deleteSuffixWithoutRouterKey(kRouterBills);
        /** 账变记录*/
        [paramers safeSetObject:[appendingList safeObjectAtIndex:0] forKey:@"account"];
        return paramers;
    }
    
    
    if ([url hasPrefix:kRouterElectronic]){
        deleteSuffixWithoutRouterKey(kRouterElectronic);
        /** 电子*/
        [paramers safeSetObject:[appendingList safeObjectAtIndex:0] forKey:@"type"];
        return paramers;
    }
    
    
    if ([url hasPrefix:kRouterEleDelite]){
        deleteSuffixWithoutRouterKey(kRouterEleDelite);
        /** 棋牌*/
        [paramers safeSetObject:[appendingList safeObjectAtIndex:0] forKey:@"type"];
        return paramers;
    }
    
    if ([url hasPrefix:kRouterLiveBetRecord]) {
        deleteSuffixWithoutRouterKey(kRouterLiveBetRecord);
        /** 记录*/
        [paramers safeSetObject:[appendingList safeObjectAtIndex:0] forKey:@"type"];
        return paramers;
    }
    
    if ([url hasPrefix:kRouterChatIndex]) {
        deleteSuffixWithoutRouterKey(kRouterChatIndex);
        /** room*/
        [paramers safeSetObject:[appendingList safeObjectAtIndex:1] forKey:@"room"];
        return paramers;
    }
    
    if ([url hasPrefix:kRouterBetRecordChoose]) {
        deleteSuffixWithoutRouterKey(kRouterBetRecordChoose);
        /** 记录*/
        [paramers safeSetObject:[appendingList safeObjectAtIndex:0] forKey:@"type"];
        return paramers;
    }
    
    if ([url hasPrefix:kRouterLiveRedirect]){

        [paramers safeSetObject:componentsUrlParamserFrom(url,@"liveCode") forKey:@"code"];
        [paramers safeSetObject:componentsUrlParamserFrom(url,@"gameType") forKey:@"type"];
        NSString *firstKind = componentsUrlParamserFrom(url,@"firstKind");
        NSString *gameKind = componentsUrlParamserFrom(url,@"gameKind");
        
        NSString *kind = firstKind;
        if (USER_DATA_MANAGER.isLogin &&
            ![USER_DATA_MANAGER.userInfoData.type isEqualToString:@"TEST"] &&
            ![USER_DATA_MANAGER.userInfoData.type isEqualToString:@"VHY"] &&
            [gameKind length]) {
            kind = gameKind;
        }
        if (!kind.length) {
            kind = firstKind;
        }
        [paramers safeSetObject:kind forKey:@"kind"];
        
        deleteSuffixWithoutRouterKey(kRouterLiveRedirect);
        return paramers;
    }

    if ([url hasPrefix:@"http"]) {
        [paramers setObject:url forKey:@"url"];
        return paramers;
    }else if ([url hasSuffix:@".com"]){
        [paramers setObject:[NSString stringWithFormat:@"%@%@",SerVer_Url,url] forKey:@"url"];
        return paramers;
    }
    return paramers;
}

#pragma mark - GET/SET
-(RouterKey *)instanceRouterKey{
    return objc_getAssociatedObject(self, _cmd);
}

-(void)setInstanceRouterKey:(RouterKey *)instanceRouterKey{
    objc_setAssociatedObject(self, @selector(instanceRouterKey), instanceRouterKey, OBJC_ASSOCIATION_COPY);
}

NSString *componentsUrlParamserFrom(NSString *URL,NSString *key){
    if (!URL.length ||
        !key.length) {
        return nil;
    }
    
    NSString *orgStr = [URL hasSuffix:@"&"] ? URL : [NSString stringWithFormat:@"%@&",URL];
    NSString *URLRegExPattern = [NSString stringWithFormat:@"(?<=\%@=).*?(?=\\&)",key];
    
    NSError *regExErr;
    NSRegularExpression *URLRegEx = [NSRegularExpression regularExpressionWithPattern:URLRegExPattern
                                                                              options:NSRegularExpressionCaseInsensitive
                                                                                error:&regExErr];
    NSString *value = nil;
    NSRange range = [URLRegEx rangeOfFirstMatchInString:orgStr
                                                options:0
                                                  range:NSMakeRange(0, orgStr.length)];
    if (!NSEqualRanges(range, NSMakeRange(NSNotFound, 0))) {
        value = [orgStr substringWithRange:range];
    }
    return value;
    NSLog(@"URL: %@", value);
}
@end
