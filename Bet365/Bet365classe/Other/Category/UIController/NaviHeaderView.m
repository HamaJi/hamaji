//
//  NaviHeaderView.m
//  Bet365
//
//  Created by HHH on 2018/7/31.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NaviHeaderView.h"
#define Spacing 3.0
@interface NaviHeaderView()
@property (nonatomic,strong)UILabel *moneyLabel;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIView *moneyContentView;
@end
@implementation NaviHeaderView

-(instancetype)initWithMoney:(NSNumber *)money Account:(NSString *)account{
    CGFloat nameWidth = [account stringWidthWithFont:[UIFont systemFontOfSize:10]];
    CGFloat moneyWidth = [StringFormatWithStr(@"¥", [NSString roundingWithFloat:money]) stringWidthWithFont:[UIFont systemFontOfSize:10]];
    if (self = [super initWithFrame:CGRectMake(0, 0,nameWidth + moneyWidth + 2*Spacing, 25)]) {
        self.titleLabel.text = account;
        self.moneyLabel.text = StringFormatWithStr(@"¥", [NSString roundingWithFloat:money]);
        self.backgroundColor = [UIColor clearColor];
        _money = money;
    }
    return self;
}


-(void)drawRect:(CGRect)rect{
    
}

#pragma mark - GET/SET
-(void)setMoney:(NSNumber *)money{
    _money = money;
    CGFloat moneyWidth = [StringFormatWithStr(@"¥", [NSString roundingWithFloat:money]) stringWidthWithFont:[UIFont systemFontOfSize:10]];
    CGFloat nameWidth = [self.account stringWidthWithFont:[UIFont systemFontOfSize:10]];
    self.moneyLabel.text = StringFormatWithStr(@"¥", [NSString roundingWithFloat:money]);
    self.width = nameWidth + moneyWidth + 2*Spacing;
    
}
-(void)setAccount:(NSString *)account{
    _account = account;
    CGFloat moneyWidth = [StringFormatWithStr(@"¥", [NSString roundingWithFloat:self.money]) stringWidthWithFont:[UIFont systemFontOfSize:10]];
    CGFloat nameWidth = [account stringWidthWithFont:[UIFont systemFontOfSize:10]];
    self.titleLabel.text = account;
    self.width = nameWidth + moneyWidth + 2*Spacing;
}
-(UIView *)moneyContentView{
    if (!_moneyContentView) {
        
        _moneyContentView = [[UIView alloc] init];
        [_moneyContentView sizeToFit];
        [_moneyContentView setBackgroundColor:[UIColor skinNaviTintColor]];
        _moneyContentView.clipsToBounds = YES;
        _moneyContentView.layer.cornerRadius = 8;
        [self addSubview:_moneyContentView];
        [_moneyContentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.titleLabel.mas_right);
            make.centerY.bottom.equalTo(self);
            make.right.equalTo(self);
        }];
    }
    return _moneyContentView;
}
-(UILabel *)moneyLabel{
    if (!_moneyLabel) {
        _moneyLabel = [UILabel setAllocLabelWithText:@"￥" FontOfSize:10 rgbColor:0xffffff];
        _moneyLabel.textColor = [UIColor skinNaviBgColor];
        [self.moneyContentView addSubview:_moneyLabel];
        [_moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.equalTo(self.moneyContentView).offset(Spacing);
            make.right.bottom.equalTo(self.moneyContentView).offset(-Spacing);
        }];
    }
    return _moneyLabel;
}
- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [UILabel setAllocLabelWithText:@"-" FontOfSize:10 rgbColor:0xffffff];
        _titleLabel.textColor = [UIColor skinNaviTintColor];
        [self addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.equalTo(self);
        }];
    }
    return _titleLabel;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
