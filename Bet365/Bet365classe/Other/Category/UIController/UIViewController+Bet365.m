//
//  UIViewController+Bet365.m
//  Bet365
//  Created by HHH on 2018/8/1.
//  Copyright © 2018年 jesse. All rights reserved.


#import "UIViewController+Bet365.h"
#import <Aspects/Aspects.h>
#import "KFCPActivityController.h"
#import "BetTotal365ViewController.h"
#import "jesseLogBet365ViewController.h"
#import <objc/message.h>

#import "LukeRegisterViewController.h"
#import "LukeTopWindow.h"
#import "LukeRecordViewController.h"
#import "jesseBet365KindTableViewController.h"
#import "BetTotal365ViewController.h"
#import "jesseGameIntroduce.h"
#import "Bet365RecordViewController.h"
#import "LukeLiveViewController.h"
#import "jesseSportTroduceViewController.h"
#import "ElecWZRYAllViewController.h"
#import "Bet365QPViewController.h"
#import "Bet365BettingResultViewController.h"
#import "SportsModuleShare.h"

#import "Bet365TransferController.h"
#import "Bet365SportBasicController.h"
#import "LukeSportRecordViewController.h"
#import "ChatGroupViewController.h"
#import "GuideViewController.h"
#import "PresentingAlphaAnimator.h"
#import "DismissingAlphaAnimator.h"

#import "KFCPMoveMentViewController.h"
#import "PersonPageViewController.h"

#import "Bet365FishController.h"
#import "CloudModel.h"
#import "Bet365FootBallViewController.h"
#import "GameRulesController.h"

@interface UIViewController ()<UIViewControllerTransitioningDelegate>

@end
@implementation UIViewController (Bet365)

#pragma mark - UIViewControllerTransitioningDelegate
- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source{
    
    return [PresentingAlphaAnimator new];
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    return [DismissingAlphaAnimator new];
}


#pragma mark - Action


-(void)showGuidChatEnter{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (BET_CONFIG.userSettingData.hasGuideChatEnter) {
            return;
        }
        BET_CONFIG.userSettingData.hasGuideChatEnter = YES;
        [BET_CONFIG.userSettingData save];
        __block UIView *chatBtn;
        [self.navigationItem.rightBarButtonItems enumerateObjectsUsingBlock:^(UIBarButtonItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.tag == 1) {
                chatBtn = obj.customView;
                *stop = YES;
            }
        }];
        if (chatBtn) {
            GuideViewController *vc = [[GuideViewController alloc] initWithNibName:@"GuideViewController" bundle:nil];
            vc.handle = ^UIView *(GuideIndex idx) {
                if (idx == GuideIndex_CHAT_ENTER) {
                    return chatBtn;
                }
                return nil;
            };
            vc.index = GuideIndex_CHAT_ENTER;
            
            vc.transitioningDelegate = self;
            vc.modalPresentationStyle = UIModalPresentationCustom;
            [[NAVI_MANAGER getCurrentVC] presentViewController:vc animated:YES completion:nil];
        }
    });
}





/**
 玩法说明
 */
-(void)palyRuleAction{
    if ([self isKindOfClass:[BetTotal365ViewController class]]) {
        return;
    }
    [self palyRuleActionForSubGame:nil type:CreDicPlayKindTYpe];
}

-(void)palyRuleActionForSubGame:(NSString *)gameId type:(currenGameType)type{
    
    INTRODUCE_SPORT.hidesBottomBarWhenPushed = YES;
    if ([[NAVI_MANAGER getCurrentVC].navigationController.visibleViewController isKindOfClass:[LukeLiveViewController class]] ||
        [[NAVI_MANAGER getCurrentVC].navigationController.visibleViewController isKindOfClass:[Bet365SportBasicController class]] ||
        [[NAVI_MANAGER getCurrentVC].navigationController.visibleViewController isKindOfClass:[LukeSportRecordViewController class]]||[[NAVI_MANAGER getCurrentVC].navigationController.visibleViewController isKindOfClass:[Bet365FootBallViewController class]]) {
        NSInteger sportsdx = [[NAVI_MANAGER getCurrentVC].navigationController indexAtViewController:INTRODUCE_SPORT];
        if (sportsdx != NSNotFound) {
            [[NAVI_MANAGER getCurrentVC].navigationController popToViewController:INTRODUCE_SPORT animated:YES];
        }else{
            [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:INTRODUCE_SPORT animated:YES];
        }
    }else if ([[NAVI_MANAGER getCurrentVC].navigationController.visibleViewController isKindOfClass:[GameRulesController class]]){
    } else if (![[NAVI_MANAGER getCurrentVC].navigationController.visibleViewController isKindOfClass:[GameRulesController class]]) {
        GameRulesController *vc = [[GameRulesController alloc] init];
        vc.lotteryId = gameId;
        vc.gameType = type;
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }
}



#pragma mark -  彩票

+ (void)enterLotteryByLotteryId:(NSString *)lotteryId{
    [self eLotteryFactory:lotteryId type:([BET_CONFIG.config.play_type_config integerValue]==1)?YES:NO copyTulongEnter:NO];
}

+ (void)enterLotteryByLotteryId:(NSString *)lotteryId isCredit:(BOOL)credit TuLongEnterValue:(BOOL)tulongValue{
    [self eLotteryFactory:lotteryId type:credit copyTulongEnter:tulongValue];
}

+ (void)eLotteryFactory:(NSString *)gameId
                   type:(BOOL)creditType
        copyTulongEnter:(BOOL)valueEnter{
    
    
    
    BetTotal365ViewController *totalC = [[BetTotal365ViewController alloc]init];
    totalC.view.backgroundColor = [UIColor whiteColor];
    totalC.isTuLongEnter = valueEnter;
    [totalC replacekindWayType];
    
    void (^addCreditChild)() = ^(){
        [totalC addChildController];
        [totalC fristIsOfficalBy:0];
    };
    void (^addOfficalChild)() = ^(){
        [totalC addofficalSonController];
        [totalC fristIsOfficalBy:1];
    };
    
    if (((NSArray *)LOTTERY_FACTORY.lotteryClassifyList[0]).count==0) {
        BET_CONFIG.kind_type = LotteryKind_CREDIT;
        addCreditChild();
    }else{
        //蛤蟆吉TODO
        LotteryKind kind_type = check_Status(gameId);
        BET_CONFIG.kind_type = kind_type;
        if (kind_type==LotteryKind_EXIST) {
            (creditType)?addCreditChild():addOfficalChild();
        }else{
            (kind_type==LotteryKind_CREDIT)?addCreditChild():addOfficalChild();
        }
    }
    [totalC pushKindTypeByParameterNumber:[NSString stringWithFormat:@"%@",gameId]];
    [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:totalC animated:YES];
    totalC.hidesBottomBarWhenPushed = YES;
}

+ (NSString *)getPushGameCurrenToken{
    __block NSString *token = nil;
    NSHTTPCookieStorage *cookiesManager = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    [cookiesManager.cookies enumerateObjectsUsingBlock:^(NSHTTPCookie * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.name isEqualToString:@"token"]) {
            token = [NSString stringWithFormat:@"%@=%@",obj.name,obj.value];
        }
    }];
    return token;
}
@end
