//
//  UIViewController+Aji.m
//  Bet365
//
//  Created by HHH on 2018/8/1.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "UIViewController+Aji.h"

@implementation UIViewController (Aji)
-(BOOL)isContainAtWindow{
    
    return ([self isViewLoaded]&&[self.navigationController indexAtViewController:self] != NSNotFound);
}

-(void)setIsContainAtWindow:(BOOL)isContainAtWindow{
    
}

- (void)showDatePickerdefaultSelValue:(NSString *)defaultSelValue minDate:(NSDate *)minDate resultBlock:(BRDateResultBlock)resultBlock
{
    [BRDatePickerView showDatePickerWithTitle:@"时间选择" dateType:BRDatePickerModeYMD defaultSelValue:defaultSelValue minDate:minDate maxDate:[NSDate date] isAutoSelect:NO themeColor:nil resultBlock:resultBlock];
}

- (void)alertWithTitle:(NSString *)title message:(NSString *)message cancelActionTitle:(NSString *)cancelActionTitle cancelHandler:(void (^ _Nullable)(UIAlertAction *))cancelHandler sureActionTitle:(NSString *)sureActionTitle sureHandler:(void (^ _Nullable)(UIAlertAction *))sureHandler
{
    [self showAlertWithTitle:title message:message cancelActionTitle:cancelActionTitle cancelHandler:cancelHandler sureActionTitle:sureActionTitle sureHandler:sureHandler];
}

- (void)alertWithSureTitle:(NSString *)title message:(NSString *)message
{
    [self showAlertWithTitle:title message:message cancelActionTitle:@"确定" cancelHandler:nil sureActionTitle:nil sureHandler:nil];
}

#pragma mark - private
- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message cancelActionTitle:(NSString *)cancelActionTitle cancelHandler:(void (^ _Nullable)(UIAlertAction *))cancelHandler sureActionTitle:(NSString *)sureActionTitle sureHandler:(void (^ _Nullable)(UIAlertAction *))sureHandler
{
    UIAlertController *alertVc = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    if (cancelActionTitle.length > 0) {
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:cancelActionTitle style:UIAlertActionStyleCancel handler:cancelHandler];
        [alertVc addAction:cancel];
    }
    if (sureActionTitle.length > 0) {
        UIAlertAction *sure = [UIAlertAction actionWithTitle:sureActionTitle style:UIAlertActionStyleDefault handler:sureHandler];
        [alertVc addAction:sure];
    }
    [self presentViewController:alertVc animated:YES completion:nil];
}

- (void)setBackgroundColorWithColor:(UIColor *)color
{
    self.view.backgroundColor = color;
}

- (void)setNavigationColorWithColor:(UIColor *)color
{
    [self.navigationController.navigationBar setBarTintColor:color];
}

- (void)createRightBarItemWithTitle:(NSString *)title action:(SEL)action
{
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStyleDone target:self action:action];
    [rightButton setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightButton;
}

- (void)createLeftBarItemWithTitle:(NSString *)title textColor:(UIColor *)textColor action:(SEL)action
{
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStyleDone target:self action:action];
    [leftButton setTitleTextAttributes:@{NSForegroundColorAttributeName : textColor} forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = leftButton;
}

+(BOOL)isCurrentViewControllerVisible:(UIViewController *)viewController
{
    return (viewController.isViewLoaded && viewController.view.window);
}

-(void)presentPopViewController:(UIViewController *)viewControllerToPresent completion:(void (^)(void))completion{

    viewControllerToPresent.transitioningDelegate = viewControllerToPresent;
    viewControllerToPresent.modalPresentationStyle = UIModalPresentationCustom;
    [self presentViewController:viewControllerToPresent animated:YES completion:completion];
}
@end
