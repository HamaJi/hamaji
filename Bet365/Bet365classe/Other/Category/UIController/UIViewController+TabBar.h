//
//  UIViewController+TabBar.h
//  Bet365
//
//  Created by HHH on 2018/8/1.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (TabBar)
-(void)setTabBarItemImage:(id)image selectedImage:(id)selectImage placheImage:(UIImage *)placheImage title:(NSString *)titleString;
@end
