//
//  UIViewController+alpha.m
//  Bet365
//
//  Created by HHH on 2018/7/15.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "UIViewController+alpha.h"


@implementation UIViewController (alpha)
- (void)setNavigationbarAlpha:(float)alpha {
    
    UIImageView *opaqueBarView = [self.navigationController.navigationBar viewWithTag:12387];

    float xAlpha = MIN( MAX(alpha, 0), 1.0);
    
    if (xAlpha >= 1.0) {
        
        [self.navigationController.navigationBar setBarTintColor:[UIColor skinNaviBgColor]];
        UIImage *image = [UIImage skinGradientVerNaviImage];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        if (opaqueBarView) {
            [opaqueBarView removeFromSuperview];
        }
    }else{
        [self.navigationController.navigationBar setBarTintColor:[UIColor clearColor]];
        [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
        if (!opaqueBarView) {
            opaqueBarView = [self creat12387opaqueBarView];
            
        }
        opaqueBarView.alpha = xAlpha;
    }
    [self.navigationController.navigationBar setShadowImage:[UIImage createImageWithColor:[UIColor clearColor]]];
}

-(UIImageView *)creat12387opaqueBarView{
    UIImageView *opaqueBarView = [UIImageView new];
    opaqueBarView.tag = 12387;
    UIImage *image = [UIImage skinGradientVerNaviImage];
    [opaqueBarView setImage:image];
    CGRect rc = self.navigationController.navigationBar.bounds;
    opaqueBarView.frame = CGRectMake(0, -50, CGRectGetWidth(rc), 50 + (is_IphoneX ? 88 : 64));
    for (UIView *subV in self.navigationController.navigationBar.subviews) {
        if (CGRectGetHeight(subV.bounds) == (is_IphoneX ? 88 : 64) && [subV isKindOfClass:[UIButton class]] == NO) {
            [subV insertSubview:opaqueBarView atIndex:0];
        }
    }
    return opaqueBarView;
}
@end
