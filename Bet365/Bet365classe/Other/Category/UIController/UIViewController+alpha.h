//
//  UIViewController+alpha.h
//  Bet365
//
//  Created by HHH on 2018/7/15.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (alpha)
- (void)setNavigationbarAlpha:(float)alpha;

@end
