//
//  NSData+AES.h
//  Bet365
//
//  Created by HHH on 2018/8/3.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (AES)
- (NSData *)aes256_encrypt:(NSString *)key;   //加密


- (NSData *)aes256_decrypt:(NSString *)key;   //解密



+ (NSString *)encryptAESCBC:(NSString *)inputString;
@end
