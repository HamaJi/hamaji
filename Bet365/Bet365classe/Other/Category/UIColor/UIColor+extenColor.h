//
//  UIColor+extenColor.h
//  Bet365
//
//  Created by jesse on 2018/1/30.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (extenColor)
+ (instancetype)colorWithHexString:(NSString *)hexString;

+ (UIColor *)pk10ColorWithNum:(NSInteger)num;

+ (UIColor *)hexColor:(NSString *)hexColor;
@end
