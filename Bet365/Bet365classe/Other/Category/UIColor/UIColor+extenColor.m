//
//  UIColor+extenColor.m
//  Bet365
//
//  Created by jesse on 2018/1/30.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "UIColor+extenColor.h"

@implementation UIColor (extenColor)
+ (instancetype)r:(uint8_t)r g:(uint8_t)g b:(uint8_t)b a:(uint8_t)a {
    return [self colorWithRed:r / 255. green:g / 255. blue:b / 255. alpha:a / 255.];
}

+ (instancetype)rgba:(NSUInteger)rgba {
    return [self r:(rgba >> 24)&0xFF g:(rgba >> 16)&0xFF b:(rgba >> 8)&0xFF a:rgba&0xFF];
}

+ (instancetype)colorWithHexString:(NSString *)hexString {
    if (!hexString)
        return nil;
    
    NSString* hex = [NSString stringWithString:hexString];
    if ([hex hasPrefix:@"#"])
        hex = [hex substringFromIndex:1];
    
    if (hex.length == 6)
        hex = [hex stringByAppendingString:@"FF"];
    else if (hex.length != 8)
        return nil;
    uint32_t rgba;
    NSScanner* scanner = [NSScanner scannerWithString:hex];
    [scanner scanHexInt:&rgba];
    return [UIColor rgba:rgba];
}
//color
+ (UIColor *)hexColor:(NSString*)hexColor{
    
    unsigned int red, green, blue, alpha;
    NSRange range;
    range.length = 2;
    @try {
        if ([hexColor hasPrefix:@"#"]) {
            hexColor = [hexColor stringByReplacingOccurrencesOfString:@"#" withString:@""];
        }
        range.location = 0;
        [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&red];
        range.location = 2;
        [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&green];
        range.location = 4;
        [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&blue];
        
        if ([hexColor length] > 6) {
            range.location = 6;
            [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&alpha];
        }
    }
    @catch (NSException * e) {
        //        [MAUIToolkit showMessage:[NSString stringWithFormat:@"颜色取值错误:%@,%@", [e name], [e reason]]];
        //        return [UIColor blackColor];
    }
    
    return [UIColor colorWithRed:(float)(red/255.0f) green:(float)(green/255.0f) blue:(float)(blue/255.0f) alpha:(float)(1.0f)];
}
+ (UIColor *)pk10ColorWithNum:(NSInteger)num
{
    switch (num) {
        case 1:
            return COLOR_WITH_HEX(0xEED605);
        case 2:
            return COLOR_WITH_HEX(0x0054ff);
        case 3:
            return COLOR_WITH_HEX(0x001868);
        case 4:
            return COLOR_WITH_HEX(0xff5b00);
        case 5:
            return COLOR_WITH_HEX(0x00c0ff);
        case 6:
            return COLOR_WITH_HEX(0x5d06f4);
        case 7:
            return COLOR_WITH_HEX(0xb2b2b2);
        case 8:
            return COLOR_WITH_HEX(0xf70400);
        case 9:
            return COLOR_WITH_HEX(0xad0000);
        case 10:
            return COLOR_WITH_HEX(0x0ad500);
        default:
            return COLOR_WITH_HEX(0xb2b2b2);
            break;
    }
}

@end
