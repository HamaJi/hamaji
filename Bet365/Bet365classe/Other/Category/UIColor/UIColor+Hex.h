//
//  UIColor+Hex.h
//  Bet365
//
//  Created by adnin on 2018/12/3.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#pragma mark--颜色的封装

#define COLOR_WITH_HEX(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define COLOR_WITH_HEX_ALP(rgbValue, alp) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:alp]

#define COLOR_WITH_HEXSTRING(rgbValue) ([UIColor colorWithMacHexString:rgbValue])

@interface UIColor (Hex)

+ (UIColor *)colorWithMacHexString:(NSString *)hexString;
@end
