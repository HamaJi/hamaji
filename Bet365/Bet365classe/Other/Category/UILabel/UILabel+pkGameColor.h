//
//  UILabel+pkGameColor.h
//  Bet365
//
//  Created by jesse on 2018/10/20.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (pkGameColor)
-(UIColor *)pk10ColorWithNum:(NSInteger)num;
@end
