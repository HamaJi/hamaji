//
//  UILabel+pkGameColor.m
//  Bet365
//
//  Created by jesse on 2018/10/20.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "UILabel+pkGameColor.h"

@implementation UILabel (pkGameColor)
- (UIColor *)pk10ColorWithNum:(NSInteger)num{
    switch (num) {
        case 1:
            return COLOR_WITH_HEX(0xEED605);
        case 2:
            return COLOR_WITH_HEX(0x0054ff);
        case 3:
            return COLOR_WITH_HEX(0x001868);
        case 4:
            return COLOR_WITH_HEX(0xff5b00);
        case 5:
            return COLOR_WITH_HEX(0x00c0ff);
        case 6:
            return COLOR_WITH_HEX(0x5d06f4);
        case 7:
            return COLOR_WITH_HEX(0xb2b2b2);
        case 8:
            return COLOR_WITH_HEX(0xf70400);
        case 9:
            return COLOR_WITH_HEX(0xad0000);
        case 10:
            return COLOR_WITH_HEX(0x0ad500);
        default:
            return COLOR_WITH_HEX(0xb2b2b2);
            break;
    }

}
@end
