//
//  UILabel+Aji.h
//  Bet365
//
//  Created by HHH on 2018/7/2.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Aji)

+ (UILabel *)setAllocLabelWithText:(NSString *)text FontOfSize:(NSUInteger)fontSize rgbColor:(int)rgbColor;

+ (UILabel *)addLabelWithFont:(CGFloat)font color:(UIColor *)color title:(NSString *)title;

@end
