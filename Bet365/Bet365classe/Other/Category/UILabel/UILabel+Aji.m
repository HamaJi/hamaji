//
//  UILabel+Aji.m
//  Bet365
//
//  Created by HHH on 2018/7/2.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "UILabel+Aji.h"


@implementation UILabel (Aji)
+ (UILabel *)setAllocLabelWithText:(NSString *)text FontOfSize:(NSUInteger)fontSize rgbColor:(int)rgbColor{
    UILabel *label = [[UILabel alloc]init];
    label.textColor = COLOR_WITH_HEX(rgbColor);
    label.font = [UIFont systemFontOfSize:fontSize];
    label.text = NSLocalizedString([NSString noNilWithString:text], nil);
    //    
    return label;
}

+ (UILabel *)addLabelWithFont:(CGFloat)font color:(UIColor *)color title:(NSString *)title
{
    UILabel *label = [[UILabel alloc] init];
    label.text = title;
    label.textColor = color;
    label.font = [UIFont systemFontOfSize:font];
    return label;
}

@end
