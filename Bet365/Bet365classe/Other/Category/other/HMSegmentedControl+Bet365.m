//
//  HMSegmentedControl+Bet365.m
//  Bet365
//
//  Created by luke on 2018/8/18.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "HMSegmentedControl+Bet365.h"

@implementation HMSegmentedControl (Bet365)

+ (instancetype)createSegmentWithBackgroundColor:(UIColor *)backgroundColor IndicatorColor:(UIColor *)IndicatorColor selectedTitleColor:(UIColor *)selectedTitleColor titleColor:(UIColor *)titleColor titleArrs:(NSArray *)titleArrs addTarget:(id)target action:(SEL)action
{
    HMSegmentedControl *segment = [[HMSegmentedControl alloc] init];
    segment.backgroundColor = backgroundColor ? backgroundColor : [UIColor whiteColor];
    segment.selectedSegmentIndex = 0;
    segment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segment.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    segment.selectionIndicatorHeight = 2;
    segment.selectionIndicatorColor = IndicatorColor;
    segment.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : selectedTitleColor,NSFontAttributeName : [UIFont systemFontOfSize:IS_IPHONE_5s ? 14 : 16]};
    segment.titleTextAttributes = @{NSForegroundColorAttributeName : titleColor ? titleColor : [UIColor blackColor],NSFontAttributeName : [UIFont systemFontOfSize:IS_IPHONE_5s ? 14 : 16]};
    if (titleArrs.count) {
        segment.sectionTitles = titleArrs;
    }
    [segment addTarget:target action:action forControlEvents:UIControlEventValueChanged];
    return segment;
}

@end
