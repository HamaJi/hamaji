//
//  HMSegmentedControl+Bet365.h
//  Bet365
//
//  Created by luke on 2018/8/18.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <HMSegmentedControl/HMSegmentedControl.h>
@interface HMSegmentedControl (Bet365)

+ (instancetype)createSegmentWithBackgroundColor:(UIColor *)backgroundColor IndicatorColor:(UIColor *)IndicatorColor selectedTitleColor:(UIColor *)selectedTitleColor titleColor:(UIColor *)titleColor titleArrs:(NSArray *)titleArrs addTarget:(id)target action:(SEL)action;

@end
