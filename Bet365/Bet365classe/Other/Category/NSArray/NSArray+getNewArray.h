//
//  NSArray+getNewArray.h
//  Bet365
//
//  Created by luke on 2018/8/12.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (getNewArray)

+ (NSArray *)getNewArrayWithArr:(NSArray *)arr key:(NSString *)key;

- (NSInteger)plusAllObject;

- (NSArray *)getNewArrayWithKey:(NSString *)key;

@end
