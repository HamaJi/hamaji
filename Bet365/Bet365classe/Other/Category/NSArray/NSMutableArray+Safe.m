//
//  NSMutableArray+Safe.m
//  Bet365
//
//  Created by HHH on 2018/8/18.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NSMutableArray+Safe.h"

@implementation NSMutableArray (Safe)
- (void)safeAddObject:(id)object{
    if (object) {
        [self addObject:object];
    }
}

-(void)safeReplaceObjectAtIndex:(NSUInteger)index withObject:(id)anObject{
    if (!anObject) {
        return;
    }
    if (self.count < index + 1) {
        return;
    }
    [self replaceObjectAtIndex:index withObject:anObject];
}

-(void)safeReplaceObjectsAtIndexes:(NSIndexSet *)indexes withObjects:(NSArray *)objects{
    if (!objects.count) {
        return;
    }
    if (!indexes.count) {
        return;
    }
    if (indexes.count != objects.count) {
        return;
    }
    [self replaceObjectsAtIndexes:indexes withObjects:objects];
}

-(void)safeReplaceObjectsInRange:(NSRange)range withObjectsFromArray:(NSArray *)otherArray{
    if (range.location < 0 || range.location + range.length > self.count) {
        return;
    }
    if (otherArray.count != range.length) {
        return;
    }
    [self replaceObjectsInRange:range withObjectsFromArray:otherArray];
}

-(void)safeAddObjectsFromArray:(NSArray *)objects{
    if (![objects isKindOfClass:[NSArray class]] ||
        !objects.count) {
        return;
    }
    [self addObjectsFromArray:objects];
}

@end
