//
//  NSArray+Safe.m
//  Bet365
//
//  Created by HHH on 2018/8/18.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NSArray+Safe.h"

@implementation NSArray (Safe)
- (id)safeObjectAtIndex:(NSUInteger)index {
    if (index < self.count) {
        id object = self[index];
        if (object == [NSNull null]) {
            return nil;
        }
        return object;
    }
    return nil;
}

-(id)safeSubarrayWithRange:(NSRange)range{
    if (range.location + range.length > self.count) {
        return nil;
    }
    if (range.location + range.length <= 0) {
        return nil;
    }
    return [self subarrayWithRange:range];
}
@end
