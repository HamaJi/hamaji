//
//  NSArray+Safe.h
//  Bet365
//
//  Created by HHH on 2018/8/18.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Safe)
- (id)safeObjectAtIndex:(NSUInteger)index ;

- (id)safeSubarrayWithRange:(NSRange)range;

@end
