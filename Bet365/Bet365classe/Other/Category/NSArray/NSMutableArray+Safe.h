//
//  NSMutableArray+Safe.h
//  Bet365
//
//  Created by HHH on 2018/8/18.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Safe)
- (void)safeAddObject:(id)object;

-(void)safeReplaceObjectAtIndex:(NSUInteger)index withObject:(id)anObject;

-(void)safeReplaceObjectsAtIndexes:(NSIndexSet *)indexes withObjects:(NSArray *)objects;

-(void)safeReplaceObjectsInRange:(NSRange)range withObjectsFromArray:(NSArray *)otherArray;

-(void)safeAddObjectsFromArray:(NSArray *)objects;
@end
