//
//  NSArray+getNewArray.m
//  Bet365
//
//  Created by luke on 2018/8/12.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NSArray+getNewArray.h"

@implementation NSArray (getNewArray)

+ (NSArray *)getNewArrayWithArr:(NSArray *)arr key:(NSString *)key
{
    NSMutableArray *tempArr = [NSMutableArray array];
    [arr enumerateObjectsUsingBlock:^(NSDictionary  * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [tempArr addObject:[[obj objectForKey:key] mutableCopy]];
    }];
    return tempArr;
}

- (NSInteger)plusAllObject
{
    __block NSInteger plus = 0;
    [self enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        plus += [obj integerValue];
    }];
    return plus;
}

- (NSArray *)getNewArrayWithKey:(NSString *)key
{
    NSMutableArray *tempArr = [NSMutableArray array];
    [self enumerateObjectsUsingBlock:^(NSDictionary  * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [tempArr addObject:[[obj objectForKey:key] mutableCopy]];
    }];
    return tempArr;
}

@end
