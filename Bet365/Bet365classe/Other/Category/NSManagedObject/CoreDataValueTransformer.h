//
//  CoreDataValueTransformer.h
//  Bet365
//
//  Created by HHH on 2018/11/7.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef id (^CoreDataValueTransformerBlock)(id value, BOOL *success, NSError **error);

@interface CoreDataValueTransformer : NSValueTransformer

+(instancetype)registerValueTransformerWithName:(NSString *)transformerName
                                   ForwardBlock:(CoreDataValueTransformerBlock)forwardBlock
                                   reverseBlock:(CoreDataValueTransformerBlock)reverseBlock;
@end
