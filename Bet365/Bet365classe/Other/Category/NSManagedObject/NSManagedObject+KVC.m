//
//  NSManagedObject+KVC.m
//  Bet365
//
//  Created by HHH on 2018/11/7.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NSManagedObject+KVC.h"
#import "NSManagedObject+PropertyValue.h"
#import "CoreDataValueTransformer.h"
#import "NSError+MTLModelException.h"
@implementation NSManagedObject (KVC)


+ (instancetype)modelWithDictionary:(NSDictionary *)dictionary error:(NSError **)error {
    return [[self alloc] creatEntytyFromJSONDictionary:dictionary error:error];
}

+(instancetype)creatEntytyFromJSONDictionary:(NSDictionary *)dictionary error:(NSError **)error{
    NSManagedObject *object =  [self MR_createEntity];
    
    for (NSString *key in dictionary) {
        __autoreleasing id value = [dictionary objectForKey:key];
        
        if ([value isEqual:NSNull.null]) value = nil;
        
        BOOL success = CoreDataValidateAndSetValue(object, key, value, YES, error);
        if (!success) return nil;
    }
    return object;
}




static BOOL CoreDataValidateAndSetValue(id obj, NSString *key, id value, BOOL forceUpdate, NSError **error) {
    __autoreleasing id validatedValue = value;
    
    @try {
        if (![obj validateValue:&validatedValue forKey:key error:error]) return NO;
        
        if (forceUpdate || value != validatedValue) {
            [obj setValue:validatedValue forKey:key];
        }
        
        return YES;
    } @catch (NSException *ex) {
        NSLog(@"*** 异常值(不包括在Model内) \"%@\" : %@", key, ex);

#if DEBUG
        @throw ex;
#else
        if (error != NULL) {
            *error = [NSError mtl_modelErrorWithException:ex];
        }
        
        return NO;
#endif
    }
}

@end
