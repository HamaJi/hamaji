//
//  CoreDataValueTransformer.m
//  Bet365
//
//  Created by HHH on 2018/11/7.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "CoreDataValueTransformer.h"
@interface CoreDataValueTransformer()

@property (nonatomic,copy)CoreDataValueTransformerBlock forwardBlock;

@property (nonatomic,copy)CoreDataValueTransformerBlock reverseBlock;

@end
@implementation CoreDataValueTransformer

+(instancetype)registerValueTransformerWithName:(NSString *)transformerName
                                   ForwardBlock:(CoreDataValueTransformerBlock)forwardBlock
                                   reverseBlock:(CoreDataValueTransformerBlock)reverseBlock{
    CoreDataValueTransformer *valueTransformer = (CoreDataValueTransformer *)[NSValueTransformer valueTransformerForName:transformerName];
    if (!valueTransformer) {
        [NSValueTransformer setValueTransformer:[[CoreDataValueTransformer alloc] initWithForwardBlock:forwardBlock reverseBlock:reverseBlock] forName:transformerName];
        valueTransformer = (CoreDataValueTransformer *)[NSValueTransformer valueTransformerForName:transformerName];
    }
    return valueTransformer;
}


- (id)initWithForwardBlock:(CoreDataValueTransformerBlock)forwardBlock
              reverseBlock:(CoreDataValueTransformerBlock)reverseBlock {
    
    NSParameterAssert(forwardBlock != nil);
    if (self = [super init]) {
        self.forwardBlock = [forwardBlock copy];
        self.reverseBlock = [reverseBlock copy];
    }
    
    return self;
}

+ (instancetype)transformerUsingForwardBlock:(CoreDataValueTransformerBlock)forwardBlock
                                reverseBlock:(CoreDataValueTransformerBlock)reverseBlock {
    return [[CoreDataValueTransformer alloc] initWithForwardBlock:forwardBlock
                                                     reverseBlock:reverseBlock];
}

#pragma mark - Public
- (id)transformedValue:(id)value success:(BOOL *)outerSuccess error:(NSError **)outerError {
    NSError *error = nil;
    BOOL success = YES;
    
    id transformedValue = self.forwardBlock(value, &success, &error);
    
    if (outerSuccess != NULL) *outerSuccess = success;
    if (outerError != NULL) *outerError = error;
    
    return transformedValue;
}

#pragma mark NSValueTransformer

+ (BOOL)allowsReverseTransformation {
    return NO;
}

+ (Class)transformedValueClass {
    return NSObject.class;
}

- (id)transformedValue:(id)value {
    NSError *error = nil;
    BOOL success = YES;
    
    return self.forwardBlock(value, &success, &error);
}


@end
