//
//  NSManagedObject+KVC.h
//  Bet365
//
//  Created by HHH on 2018/11/7.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (KVC)

+ (instancetype)modelWithDictionary:(NSDictionary *)dictionary error:(NSError **)error ;

@end
