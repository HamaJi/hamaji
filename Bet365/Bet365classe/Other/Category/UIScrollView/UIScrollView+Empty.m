//
//  UIScrollView+Empty.m
//  Bet365
//
//  Created by HHH on 2018/8/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "UIScrollView+Empty.h"
#import<objc/runtime.h>
#import "UIScrollView+EmptyDataSet.h"
#import <Aspects/Aspects.h>

@implementation UIScrollView (Empty)

+ (void)load{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [UIScrollView aspect_hookSelector:@selector(init) withOptions:AspectPositionAfter usingBlock:^(id<AspectInfo> aspectInfo){
            [aspectInfo.instance performSelector:@selector(bindingDelegate) withObject:nil];
        } error:nil];
        
        [UIScrollView aspect_hookSelector:@selector(initWithCoder:) withOptions:AspectPositionAfter usingBlock:^(id<AspectInfo> aspectInfo){
            [aspectInfo.instance performSelector:@selector(bindingDelegate) withObject:nil];
        } error:nil];
        
    });
}

-(void)defaultData{
    self.titleForEmpty = @"暂无数据";
    self.imageNameForEmpty = @"requestError";
    self.descriptionForEmpty = @"未找到相关信息";
}
#pragma mark - Public
-(void)emptyReloaData{
    [self reloadEmptyDataSet];
}
#pragma mark - private

-(void)bindingDelegate{
    [self defaultData];
    self.emptyDataSetSource = self;
    self.emptyDataSetDelegate = self;
}

-(void)private_ReloadEmptyDataSet{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.isShowEmpty) {
            [self reloadEmptyDataSet];
        }
    });
    
}

#pragma mark - DZNEmptyDataSetSource Methods
-(UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView{
    return [UIColor clearColor];
}
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView{
    NSString *text =self.titleForEmpty;
    if (!text) {
        return nil;
    }
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0f],
                                 NSForegroundColorAttributeName: [UIColor skinTextItemNorColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}
- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView{
    NSString *text = self.descriptionForEmpty;
    if (!text) {
        return nil;
    }
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:14.0f],
                                 NSForegroundColorAttributeName: [UIColor skinTextItemNorSubColor],
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView{

    return [UIImage imageNamed:self.imageNameForEmpty];
}

-(NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state{
    if (!self.titleButtonForEmpty.length ) {
        return nil;
    }
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:14.0f],
                                 NSForegroundColorAttributeName: [UIColor skinTextItemNorSubColor],
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:self.titleButtonForEmpty attributes:attributes];
}
-(UIImage *)buttonImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state{
    return self.imageNameButtonForEmpty ? [UIImage imageNamed:self.imageNameButtonForEmpty] : nil;
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView{
    return self.verticalOffsetForEmpty;
}
- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView{
    return 0.0f;
}
#pragma mark - DZNEmptyDataSetDelegate Methods

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView{
    BOOL isShowEmpty = self.isShowEmpty;
    return self.isShowEmpty;
}

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView{
    return YES;
}
-(void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view{
    //** 背景点击*/
    self.bgTapHandleForEmpty ? self.bgTapHandleForEmpty() : nil;
}
-(void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button{
    //** 按钮点击*/
    self.buttonTapHandleForEmpty ? self.buttonTapHandleForEmpty() : nil;
}
- (CAAnimation *)imageAnimationForEmptyDataSet:(UIScrollView *)scrollView{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath: @"transform"];
    
    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    animation.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeRotation(M_PI_2, 0.0, 0.0, 1.0)];
    
    animation.duration = 0.25;
    animation.cumulative = YES;
    animation.repeatCount = MAXFLOAT;
    
    return animation;
}

#pragma mark - GET/SET
-(BOOL)isShowEmpty{
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

-(void)setIsShowEmpty:(BOOL)isShowEmpty{
    objc_setAssociatedObject(self, @selector(isShowEmpty), @(isShowEmpty), OBJC_ASSOCIATION_ASSIGN);
    [self private_ReloadEmptyDataSet];
}
-(NSString *)titleForEmpty{
    return objc_getAssociatedObject(self, _cmd);
}
-(void)setTitleForEmpty:(NSString *)titleForEmpty{
    objc_setAssociatedObject(self, @selector(titleForEmpty), titleForEmpty, OBJC_ASSOCIATION_COPY);
    [self private_ReloadEmptyDataSet];
}

-(NSString *)descriptionForEmpty{
    return objc_getAssociatedObject(self, _cmd);
}

-(void)setDescriptionForEmpty:(NSString *)descriptionForEmpty{
    objc_setAssociatedObject(self, @selector(descriptionForEmpty), descriptionForEmpty, OBJC_ASSOCIATION_COPY);
    [self private_ReloadEmptyDataSet];
}

-(NSString *)imageNameForEmpty{
    return objc_getAssociatedObject(self, _cmd);
}

-(void)setImageNameForEmpty:(NSString *)imageNameForEmpty{
    objc_setAssociatedObject(self, @selector(imageNameForEmpty), imageNameForEmpty, OBJC_ASSOCIATION_COPY);
    [self private_ReloadEmptyDataSet];
}

-(CGFloat)verticalOffsetForEmpty{
    return [objc_getAssociatedObject(self, _cmd) floatValue];
}

-(void)setVerticalOffsetForEmpty:(CGFloat)verticalOffsetForEmpty{
    objc_setAssociatedObject(self, @selector(verticalOffsetForEmpty), @(verticalOffsetForEmpty), OBJC_ASSOCIATION_ASSIGN);
}

-(EmptyButtonTapHandle)buttonTapHandleForEmpty{
    return objc_getAssociatedObject(self, _cmd);
}

-(void)setButtonTapHandleForEmpty:(EmptyButtonTapHandle)buttonTapHandleForEmpty{
    objc_setAssociatedObject(self, @selector(buttonTapHandleForEmpty), buttonTapHandleForEmpty, OBJC_ASSOCIATION_COPY);
}

-(EmptyBackGroundTapHandle)bgTapHandleForEmpty{
    return objc_getAssociatedObject(self, _cmd);
}

-(void)setBgTapHandleForEmpty:(EmptyBackGroundTapHandle)bgTapHandleForEmpty{
    objc_setAssociatedObject(self, @selector(bgTapHandleForEmpty), bgTapHandleForEmpty, OBJC_ASSOCIATION_COPY);
}

-(NSString *)titleButtonForEmpty{
    return objc_getAssociatedObject(self, _cmd);
}

-(void)setTitleButtonForEmpty:(NSString *)titleButtonForEmpty{
    objc_setAssociatedObject(self, @selector(titleButtonForEmpty), titleButtonForEmpty, OBJC_ASSOCIATION_COPY);
}

-(NSString *)imageNameButtonForEmpty{
    return objc_getAssociatedObject(self, _cmd);
}

-(void)setImageNameButtonForEmpty:(NSString *)imageNameButtonForEmpty{
    objc_setAssociatedObject(self, @selector(imageNameButtonForEmpty), imageNameButtonForEmpty, OBJC_ASSOCIATION_COPY);
}
@end
