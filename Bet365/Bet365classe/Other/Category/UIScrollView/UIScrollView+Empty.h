//
//  UIScrollView+Empty.h
//  Bet365
//
//  Created by HHH on 2018/8/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "ScrollViewEmptyAction.h"
#import "UIScrollView+EmptyDataSet.h"
/** 按钮点击 */
typedef void(^EmptyButtonTapHandle)(void);

/** 背景点击 */
typedef void(^EmptyBackGroundTapHandle)(void);

@interface UIScrollView (Empty)<DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>

/**
 是否展示,可直接控制占位的显示
 */
@property (nonatomic) BOOL isShowEmpty;

/**
 标题
 */
@property (nonatomic) NSString *titleForEmpty;

/**
 按钮标题（如果设置了按钮图片将无法显示，第三方控件问题我不管！鸡哥管）
 */
@property (nonatomic) NSString *titleButtonForEmpty;

/**
 描述
 */
@property (nonatomic) NSString *descriptionForEmpty;

/**
 图片名
 */
@property (nonatomic) NSString *imageNameForEmpty;

/**
 按钮图片名
 */
@property (nonatomic) NSString *imageNameButtonForEmpty;

/**
 垂直偏移量
 */
@property (nonatomic) CGFloat verticalOffsetForEmpty;

/**
 按钮点击响应
 */
@property (nonatomic) EmptyButtonTapHandle buttonTapHandleForEmpty;

/**
 背景点击响应
 */
@property (nonatomic) EmptyBackGroundTapHandle bgTapHandleForEmpty;

/**
 刷新页面
 */
-(void)emptyReloaData;

@end
