//
//  UITabBar+Badge.h
//  Bet365
//
//  Created by HHH on 2018/8/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBar (Badge)
- (void)showBadgeOnItemIndex:(int)index; 

- (void)hideBadgeOnItemIndex:(int)index;
@end
