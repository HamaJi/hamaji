//
//  AppDelegate+Bet365.m
//  Bet365
//
//  Created by luke on 2018/9/1.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "AppDelegate+Bet365.h"

@implementation AppDelegate (Bet365)

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if (self.allowRotation) {
        return UIInterfaceOrientationMaskAll;
    }else{
        return UIInterfaceOrientationMaskPortrait;
    }
}

- (BOOL)shouldAutorotate
{
    if (self.allowRotation) {
        return YES;
    }
    return NO;
}

- (BOOL)allowRotation{
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

- (void)setAllowRotation:(BOOL)allowRotation{
    objc_setAssociatedObject(self, @selector(allowRotation), @(allowRotation), OBJC_ASSOCIATION_ASSIGN);
}

@end
