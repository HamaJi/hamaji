//
//  AppDelegate+Bet365.h
//  Bet365
//
//  Created by luke on 2018/9/1.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (Bet365)

/** 单独控制器是否允许旋转 */
@property (nonatomic,assign) BOOL allowRotation;

@end
