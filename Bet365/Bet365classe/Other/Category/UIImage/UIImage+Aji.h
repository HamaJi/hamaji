//
//  UIImage+Aji.h
//  Bet365
//
//  Created by HHH on 2018/7/8.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Aji)
#pragma mark - 根据颜色返回图片
//根据颜色返回图片
+(UIImage*)imageWithColor:(UIColor*)color;

+(UIImage*)imageWithColor:(UIColor*)color withSize:(CGSize)size;

+ (UIImage *)imageWithSmallGIFData:(NSData *)data scale:(CGFloat)scale ;

+ (NSString *)contentTypeWithImageData: (NSData *)data;
@end
