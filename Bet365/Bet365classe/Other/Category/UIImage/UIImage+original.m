//
//  UIImage+original.m
//  Lottery
//
//  Created by apple on 17/3/3.
//  Copyright © 2017年 pony. All rights reserved.
//

#import "UIImage+original.h"

@implementation UIImage (original)

+ (instancetype)originalWithImage:(NSString *)image
{
    UIImage *im = [UIImage imageNamed:image];
    
    return [im imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}

+ (instancetype)createImageWithColor:(UIColor *)color
{
    CGRect rect=CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}

/**
 *  根据图片url获取网络图片尺寸
 */
+ (CGSize)getImageSizeWithURL:(id)URL{
    NSURL * url = nil;
    if ([URL isKindOfClass:[NSURL class]]) {
        url = URL;
    }
    if ([URL isKindOfClass:[NSString class]]) {
        url = [NSURL URLWithString:URL];
    }
    if (!URL) {
        return CGSizeZero;
    }
    CGImageSourceRef imageSourceRef = CGImageSourceCreateWithURL((CFURLRef)url, NULL);
    CGFloat width = 0, height = 0;
    if (imageSourceRef) {
        CFDictionaryRef imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSourceRef, 0, NULL);
        //以下是对手机32位、64位的处理
        if (imageProperties != NULL) {
            CFNumberRef widthNumberRef = CFDictionaryGetValue(imageProperties, kCGImagePropertyPixelWidth);
#if defined(__LP64__) && __LP64__
            if (widthNumberRef != NULL) {
                CFNumberGetValue(widthNumberRef, kCFNumberFloat64Type, &width);
            }
            CFNumberRef heightNumberRef = CFDictionaryGetValue(imageProperties, kCGImagePropertyPixelHeight);
            if (heightNumberRef != NULL) {
                CFNumberGetValue(heightNumberRef, kCFNumberFloat64Type, &height);
            }
#else
            if (widthNumberRef != NULL) {
                CFNumberGetValue(widthNumberRef, kCFNumberFloat32Type, &width);
            }
            CFNumberRef heightNumberRef = CFDictionaryGetValue(imageProperties, kCGImagePropertyPixelHeight);
            if (heightNumberRef != NULL) {
                CFNumberGetValue(heightNumberRef, kCFNumberFloat32Type, &height);
            }
#endif
            CFRelease(imageProperties);
        }
        
        CFRelease(imageSourceRef);
    }
    return CGSizeMake(width, height);
}

- (UIImage *)contenCGImage{
    CGImageRef imageN = self.CGImage;
    CGFloat width = CGImageGetWidth(imageN)/2;
    CGImageRef ref = CGImageCreateWithImageInRect(imageN, CGRectMake(0, 0, width,CGImageGetHeight(imageN)));
    UIImage *resultImage = [UIImage imageWithCGImage:ref];
    CGImageRelease(ref);
    return resultImage;
}

+ (UIImage*)gradientImageWithSize:(CGSize)size andColors:(NSArray*)colors GradientType:(UIImageGradientType)gradientType{
    NSMutableArray *ar = [NSMutableArray array];
    
    CGPoint start = CGPointZero;
    CGPoint end = CGPointZero;
    if (gradientType == UIImageGradientType_HOZ) {
        start = CGPointMake(0.0, 0.0);
        end = CGPointMake(size.width, 0.0);
    }else if (gradientType == UIImageGradientType_VER) {
        start = CGPointMake(0.0, 0.0);
        end = CGPointMake(0.0, size.height);
    }
    for(UIColor *c in colors) {
        [ar addObject:(id)c.CGColor];
    }
    UIGraphicsBeginImageContextWithOptions(size, YES, 1);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGColorSpaceRef colorSpace = CGColorGetColorSpace([[colors firstObject] CGColor]);
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (CFArrayRef)ar, NULL);
    
    CGContextDrawLinearGradient(context, gradient, start, end, kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    CGGradientRelease(gradient);
    CGContextRestoreGState(context);
    CGColorSpaceRelease(colorSpace);
    UIGraphicsEndImageContext();
    return image;
}

@end
