//
//  UIImage+original.h
//  Lottery
//
//  Created by apple on 17/3/3.
//  Copyright © 2017年 pony. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, UIImageGradientType) {
    /** 横*/
    UIImageGradientType_HOZ = 0,
    /** 纵*/
    UIImageGradientType_VER,
};

@interface UIImage (original)

+ (instancetype)originalWithImage:(NSString *)image;

+ (instancetype)createImageWithColor:(UIColor *)color;

+ (CGSize)getImageSizeWithURL:(id)URL;

- (UIImage *)contenCGImage;



/**
 渐变图

 @param size 定高定宽
 @param colors 颜色集合
 @param gradientType 方向
 @return UIImage
 */
+ (UIImage*)gradientImageWithSize:(CGSize)size andColors:(NSArray <UIColor *>*)colors GradientType:(UIImageGradientType)gradientType;

@end
