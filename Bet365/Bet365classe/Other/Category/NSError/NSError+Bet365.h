//
//  NSError+Bet365.h
//  Bet365
//
//  Created by HHH on 2018/7/8.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 项目方法
 */
@interface NSError (Bet365)

@property (nonatomic) NSString *bet365RequestUrl;


/**
 登录认证信息失效UC/TOKEN_INVALID
 */
@property (nonatomic)BOOL tokenInvalid;

/**
 强制修改会员密码UC/NEED_CHANGE_PASSWORD
 */
@property (nonatomic)BOOL needChangePsw;

/**
 账号未找到UC/USER_NOT_EXIST
 */
@property (nonatomic)BOOL userNotExit;


/**
 密码错误UC/USER_LIMITED
 */
@property (nonatomic)BOOL userLimited;

/**
 验证码错误UC/VALICODE_ERROR
 */
@property (nonatomic)BOOL valicodeError;

/**
 验证码错误UC/VALICODE_ERROR
 */
@property (nonatomic)BOOL chatFailCode;

/**
 聊天室ROOM/NO_CHAT_ROOM
 */
@property (nonatomic)BOOL chatNoRoom;

/**
 密码初始化UC/INIT_PWD_VERIFY
 */
@property (nonatomic)BOOL init_pwd;


/**
 下注异常(赔率变换)BET/BET_ODDS_ERROR
 */
@property (nonatomic)BOOL bet_odds;

/**
 错误信息提示
 */
@property (nonatomic)NSString *msg;

@property (nonatomic)BOOL hasSignIn;

@property (nonatomic)BOOL LIMITED_ONLY_PHONE_LOGIN_ERROR;

/**
 归属地变化了UC/LOGIN_LIMIT_IPLOCATION
 */
@property (nonatomic)BOOL limit_IPlocation;

@property (nonatomic)BOOL limit_IPlocation_verify;

@property (nonatomic)NSString *url_link;

@property (nonatomic)NSString *message;

@end
