//
//  NSError+Bet365.m
//  Bet365
//
//  Created by HHH on 2018/7/8.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NSError+Bet365.h"
#import "NSDictionary+aji.h"
@implementation NSError (Bet365)

#pragma mark - Private
-(id)getBet365ErrorInfo{
    NSData *data = self.userInfo[@"com.alamofire.serialization.response.error.data"];
    if(!data){
        NSError *erro = self.userInfo[@"NSUnderlyingError"];
        data = erro.userInfo[@"com.alamofire.serialization.response.error.data"];
    }
    if (data) {
        return [NSDictionary aji_converHashByBit:data];
    }else{
        
        return nil;
    }
}

-(NSString *)bet365RequestUrl{
    return objc_getAssociatedObject(self, _cmd);
}

-(void)setBet365RequestUrl:(NSString *)bet365RequestUrl{
    
    NSMutableString *urlStr = [bet365RequestUrl mutableCopy];
    [urlStr deleteString:@"."];
    [urlStr deleteString:@"http://"];
    [urlStr deleteString:@"https://"];
    [urlStr deleteString:[BET_CONFIG.userSettingData.defaultAppID lowercaseStringWithLocale:[NSLocale currentLocale]]];
    [urlStr deleteString:@"tgapp"];
    [urlStr deleteString:@".com"];
    [urlStr deleteString:@"com"];
    
    objc_setAssociatedObject(self, @selector(bet365RequestUrl), urlStr, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    id data = [self getBet365ErrorInfo];
    if ([data isKindOfClass:[NSString class]]) {
        self.msg = data;
    }else if([data isKindOfClass:[NSDictionary class]]){
        self.LIMITED_ONLY_PHONE_LOGIN_ERROR = [((NSDictionary *)data)[@"code"] isEqualToString:@"UC/LIMITED_ONLY_PHONE_LOGIN_ERROR"];
        self.msg = ((NSDictionary *)data)[@"msg"];
        self.tokenInvalid = [((NSDictionary *)data)[@"code"] isEqualToString:@"UC/TOKEN_INVALID"];
        self.userNotExit = [((NSDictionary *)data)[@"code"] isEqualToString:@"UC/USER_NOT_EXIST"];
        self.userLimited = [((NSDictionary *)data)[@"code"] isEqualToString:@"UC/USER_LIMITED"];
        self.valicodeError = [((NSDictionary *)data)[@"code"] isEqualToString:@"UC/VALICODE_ERROR"];
        self.chatFailCode = [((NSDictionary *)data)[@"code"] isEqualToString:@"CHAT/MAINTAIN"];
        self.chatNoRoom = [((NSDictionary *)data)[@"code"] isEqualToString:@"ROOM/NO_CHAT_ROOM"];
        self.init_pwd = [((NSDictionary *)data)[@"code"] isEqualToString:@"UC/INIT_PWD_VERIFY"];
        self.needChangePsw = [((NSDictionary *)data)[@"code"] isEqualToString:@"UC/NEED_CHANGE_PASSWORD"];
        self.bet_odds = [((NSDictionary *)data)[@"code"] isEqualToString:@"BET/BET_ODDS_ERROR"];
        self.hasSignIn = [((NSDictionary *)data)[@"code"] isEqualToString:@"CHECKED"];
        self.limit_IPlocation = [((NSDictionary *)data)[@"code"] isEqualToString:@"UC/LOGIN_LIMIT_IPLOCATION"];
        self.limit_IPlocation_verify = [((NSDictionary *)data)[@"code"] isEqualToString:@"UC/LOGIN_LIMIT_IPLOCATION_VERIFY"];
        
        if (self.limit_IPlocation) {
            id msg = ((NSDictionary *)data)[@"msg"];
            if ([msg isKindOfClass:NSDictionary.class]) {
                if ([msg aji_keysContains:@"link"]) {
                    self.url_link = msg[@"link"];
                }
                if ([msg aji_keysContains:@"message"]) {
                    self.message = msg[@"message"];
                }
            }else if ([msg isKindOfClass:NSString.class]) {
                NSDictionary *dict = [msg mj_JSONObject];
                if ([dict aji_keysContains:@"link"]) {
                    self.url_link = dict[@"link"];
                }
                if ([dict aji_keysContains:@"message"]) {
                    self.message = dict[@"message"];
                }
            }
        }
    }
    if (!self.msg ||
        [self.msg isEqual:[NSNull null]] ||
        ![self.msg length]) {
        self.msg = [NSString stringWithFormat:@"URL=%@;DOMAIN=%@",urlStr,self.domain];
    }
}


-(BOOL)hasSignIn{
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

-(void)setHasSignIn:(BOOL)hasSignIn{
    objc_setAssociatedObject(self, @selector(hasSignIn), @(hasSignIn), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
-(BOOL)tokenInvalid{
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

-(void)setTokenInvalid:(BOOL)tokenInvalid{
    objc_setAssociatedObject(self, @selector(tokenInvalid), @(tokenInvalid), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)needChangePsw{
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

-(void)setNeedChangePsw:(BOOL)needChangePsw{
    objc_setAssociatedObject(self, @selector(needChangePsw), @(needChangePsw), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)init_pwd{
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

-(void)setInit_pwd:(BOOL)init_pwd{
    objc_setAssociatedObject(self, @selector(init_pwd), @(init_pwd), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
-(BOOL)userNotExit{
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

-(void)setUserNotExit:(BOOL)userNotExit{
    objc_setAssociatedObject(self, @selector(userNotExit), @(userNotExit), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)userLimited{
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

-(void)setUserLimited:(BOOL)userLimited{
    objc_setAssociatedObject(self, @selector(userLimited), @(userLimited), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)valicodeError
{
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

- (void)setValicodeError:(BOOL)valicodeError
{
    objc_setAssociatedObject(self, @selector(valicodeError), @(valicodeError), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)chatFailCode{
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

-(void)setChatFailCode:(BOOL)chatFailCode{
    objc_setAssociatedObject(self, @selector(chatFailCode), @(chatFailCode), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)chatNoRoom{
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

-(void)setChatNoRoom:(BOOL)chatNoRoom{
    objc_setAssociatedObject(self, @selector(chatNoRoom), @(chatNoRoom), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)bet_odds{
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

-(void)setBet_odds:(BOOL)bet_odds{
    objc_setAssociatedObject(self, @selector(bet_odds), @(bet_odds), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
-(NSString *)msg{
    return objc_getAssociatedObject(self, _cmd);
}
-(void)setMsg:(NSString *)msg{
    objc_setAssociatedObject(self, @selector(msg), msg, OBJC_ASSOCIATION_COPY);
}
-(BOOL)LIMITED_ONLY_PHONE_LOGIN_ERROR{
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}
-(void)setLIMITED_ONLY_PHONE_LOGIN_ERROR:(BOOL)LIMITED_ONLY_PHONE_LOGIN_ERROR{
    objc_setAssociatedObject(self, @selector(LIMITED_ONLY_PHONE_LOGIN_ERROR), @(LIMITED_ONLY_PHONE_LOGIN_ERROR), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)limit_IPlocation{
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

- (BOOL)limit_IPlocation_verify{
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

- (void)setLimit_IPlocation:(BOOL)limit_IPlocation{
    objc_setAssociatedObject(self, @selector(limit_IPlocation), @(limit_IPlocation), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setLimit_IPlocation_verify:(BOOL)limit_IPlocation_verify{
    objc_setAssociatedObject(self, @selector(limit_IPlocation_verify), @(limit_IPlocation_verify), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *)url_link{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setUrl_link:(NSString *)url_link{
    objc_setAssociatedObject(self, @selector(url_link), url_link, OBJC_ASSOCIATION_COPY);
}

- (NSString *)message{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setMessage:(NSString *)message{
    objc_setAssociatedObject(self, @selector(message), message, OBJC_ASSOCIATION_COPY);
}

@end
