//
//  NSMutableDictionary+Safe.m
//  Bet365
//
//  Created by HHH on 2018/8/18.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NSMutableDictionary+Safe.h"

@implementation NSMutableDictionary (Safe)
- (id)safeObjectForKey:(NSString *)aKey
{
    id object = [self objectForKey:aKey];
    if (object == [NSNull null]) {
        return nil;
    }
    return object;
}

- (void)safeSetObject:(id)anObject forKey:(NSString *)aKey{
    if(!aKey.length) {
        return;
    }
    if(anObject) {
        [self setObject:anObject forKey:aKey];
    }
}

- (void)safeRemoveObjectForKey:(id)aKey {
    if(aKey) {
        [self removeObjectForKey:aKey];
    }
}
@end
