//
//  NSDictionary+aji.m
//  Bet365
//
//  Created by HHH on 2018/7/8.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NSDictionary+aji.h"

@implementation NSDictionary (aji)

-(BOOL)aji_keysContains:(nonnull NSString *)key{
    return [[self allKeys] containsObject:key];
}

- (BOOL)aji_containsObjectForKey:(id)key {
    if (!key || [key isEqual:[NSNull null]]) return NO;
    return self[key] != nil;
}

- (NSString *)descriptionWithLocale:(id)locale{
    
    if (![self count]) {
        return @"";
    }
    NSString *tempStr1 =
    [[self description] stringByReplacingOccurrencesOfString:@"\\u"
                                                  withString:@"\\U"];
    NSString *tempStr2 =
    [tempStr1 stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSString *tempStr3 =
    [[@"\"" stringByAppendingString:tempStr2] stringByAppendingString:@"\""];
    NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str =
    
    [NSPropertyListSerialization propertyListWithData:tempData
                                              options:NSPropertyListImmutable
                                               format:NULL
                                                error:NULL];
    return str;
    
}

@end
