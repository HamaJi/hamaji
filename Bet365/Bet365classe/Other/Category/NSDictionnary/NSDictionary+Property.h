//
//  NSDictionary+Property.h
//  PK10
//
//  Created by luke on 17/4/8.
//  Copyright © 2017年 luke. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Property)

- (void)createPropertyCode;

@end
