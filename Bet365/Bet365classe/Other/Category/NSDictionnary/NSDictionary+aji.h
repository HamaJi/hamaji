//
//  NSDictionary+aji.h
//  Bet365
//
//  Created by HHH on 2018/7/8.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (aji)


/**
 keys是否包含某个Key

 @param key key
 @return BOOL
 */
-(BOOL)aji_keysContains:(nonnull NSString *)key;


/**
 key下value是否存在
 
 @param key key
 @return BOOL
 */
- (BOOL)aji_containsObjectForKey:(id)key;

- (NSString *)descriptionWithLocale:(id)locale;
@end
