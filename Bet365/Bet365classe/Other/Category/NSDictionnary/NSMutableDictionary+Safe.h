//
//  NSMutableDictionary+Safe.h
//  Bet365
//
//  Created by HHH on 2018/8/18.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (Safe)
- (id)safeObjectForKey:(NSString *)aKey;

- (void)safeSetObject:(id)anObject forKey:(NSString *)aKey;

- (void)safeRemoveObjectForKey:(NSString *)aKey;
@end
