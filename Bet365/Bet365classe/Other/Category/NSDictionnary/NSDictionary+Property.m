//
//  NSDictionary+Property.m
//  PK10
//
//  Created by luke on 17/4/8.
//  Copyright © 2017年 luke. All rights reserved.
//

#import "NSDictionary+Property.h"

@implementation NSDictionary (Property)

- (void)createPropertyCode
{
    NSMutableString *codes = [NSMutableString string];
    [self enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull value, BOOL * _Nonnull stop) {
        NSString *code;
        if ([value isKindOfClass:[NSString class]]) {
            code = [NSString stringWithFormat:@"@property (nonatomic,copy) NSString *%@;",key];
        }else if ([value isKindOfClass:NSClassFromString(@"__NSCFBoolean")]) {
            code = [NSString stringWithFormat:@"@property (nonatomic, assign) BOOL %@;",key];
        }else if ([value isKindOfClass:[NSArray class]]){
            code = [NSString stringWithFormat:@"@property (nonatomic,strong) NSArray *%@;",key];
        }else if ([value isKindOfClass:[NSDictionary class]]){
            code = [NSString stringWithFormat:@"@property (nonatomic,strong) NSDictionary *%@;",key];
        }else if ([value isKindOfClass:[NSNumber class]]){
            code = [NSString stringWithFormat:@"@property (nonatomic,assign) NSInteger %@;",key];
        }
        [codes appendFormat:@"\n%@\n",code];
    }];
    NSLog(@"%@",codes);
}

@end
