//
//  MetricsEntity+CoreDataClass.h
//  Bet365
//
//  Created by HamaJi on 2018/8/16.
//  Copyright © 2018年 jesse. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TransactionEntity;

NS_ASSUME_NONNULL_BEGIN

@interface MetricsEntity : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "MetricsEntity+CoreDataProperties.h"
