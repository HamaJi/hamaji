//
//  ChatRoomLotteryPinEntity+CoreDataProperties.h
//  Bet365
//
//  Created by adnin on 2020/3/31.
//  Copyright © 2020 jesse. All rights reserved.
//
//

#import "ChatRoomLotteryPinEntity+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ChatRoomLotteryPinEntity (CoreDataProperties)

+ (NSFetchRequest<ChatRoomLotteryPinEntity *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *roomid;
@property (nullable, nonatomic, copy) NSString *lotteryid;

@end

NS_ASSUME_NONNULL_END
