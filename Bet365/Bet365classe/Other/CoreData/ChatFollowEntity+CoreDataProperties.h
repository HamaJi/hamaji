//
//  ChatFollowEntity+CoreDataProperties.h
//  Bet365
//
//  Created by HHH on 2018/12/4.
//  Copyright © 2018年 jesse. All rights reserved.
//
//

#import "ChatFollowEntity+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ChatFollowEntity (CoreDataProperties)

+ (NSFetchRequest<ChatFollowEntity *> *)fetchRequest;

@property (nonatomic) int64_t userID;
@property (nonatomic) int64_t followUserID;
@property (nonatomic) int64_t roomID;

@end

NS_ASSUME_NONNULL_END
