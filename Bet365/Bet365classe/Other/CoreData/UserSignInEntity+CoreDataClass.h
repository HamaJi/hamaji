//
//  UserSignInEntity+CoreDataClass.h
//  Bet365
//
//  Created by super_小鸡 on 2019/6/7.
//  Copyright © 2019 jesse. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserSignInEntity : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "UserSignInEntity+CoreDataProperties.h"
