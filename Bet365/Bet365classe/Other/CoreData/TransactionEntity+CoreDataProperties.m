//
//  TransactionEntity+CoreDataProperties.m
//  Bet365
//
//  Created by HamaJi on 2018/8/16.
//  Copyright © 2018年 jesse. All rights reserved.
//
//

#import "TransactionEntity+CoreDataProperties.h"

@implementation TransactionEntity (CoreDataProperties)

+ (NSFetchRequest<TransactionEntity *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"TransactionEntity"];
}

@dynamic connectEndDate;
@dynamic connectStartDate;
@dynamic domainLookupEndDate;
@dynamic domainLookupStartDate;
@dynamic fetchStartDate;
@dynamic requestEndDate;
@dynamic requestStartDate;
@dynamic responseEndDate;
@dynamic responseStartDate;
@dynamic metrics;

@end
