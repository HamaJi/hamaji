//
//  UserSignInEntity+CoreDataProperties.m
//  Bet365
//
//  Created by super_小鸡 on 2019/6/7.
//  Copyright © 2019 jesse. All rights reserved.
//
//

#import "UserSignInEntity+CoreDataProperties.h"

@implementation UserSignInEntity (CoreDataProperties)

+ (NSFetchRequest<UserSignInEntity *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"UserSignInEntity"];
}

@dynamic account;
@dynamic signinTime;

@end
