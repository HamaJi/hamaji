//
//  ChatRoomLotteryPinEntity+CoreDataProperties.m
//  Bet365
//
//  Created by adnin on 2020/3/31.
//  Copyright © 2020 jesse. All rights reserved.
//
//

#import "ChatRoomLotteryPinEntity+CoreDataProperties.h"

@implementation ChatRoomLotteryPinEntity (CoreDataProperties)

+ (NSFetchRequest<ChatRoomLotteryPinEntity *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"ChatRoomLotteryPinEntity"];
}

@dynamic roomid;
@dynamic lotteryid;

@end
