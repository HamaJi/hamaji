//
//  ChatNetEmojiCacheEntity+CoreDataClass.h
//  Bet365
//
//  Created by adnin on 2020/4/26.
//  Copyright © 2020 jesse. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatNetEmojiCacheEntity : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "ChatNetEmojiCacheEntity+CoreDataProperties.h"
