//
//  RedPackEntity+CoreDataProperties.h
//  Bet365
//
//  Created by HHH on 2018/11/24.
//  Copyright © 2018年 jesse. All rights reserved.
//
//

#import "RedPackEntity+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface RedPackEntity (CoreDataProperties)

+ (NSFetchRequest<RedPackEntity *> *)fetchRequest;

@property (nonatomic) int64_t userID;
@property (nullable, nonatomic, copy) NSString *redpackID;
@property (nonatomic) int64_t roomID;
@property (nonatomic) int64_t timeStamp;

@end

NS_ASSUME_NONNULL_END
