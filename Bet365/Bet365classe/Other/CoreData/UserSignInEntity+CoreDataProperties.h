//
//  UserSignInEntity+CoreDataProperties.h
//  Bet365
//
//  Created by super_小鸡 on 2019/6/7.
//  Copyright © 2019 jesse. All rights reserved.
//
//

#import "UserSignInEntity+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface UserSignInEntity (CoreDataProperties)

+ (NSFetchRequest<UserSignInEntity *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *account;
@property (nullable, nonatomic, copy) NSDate *signinTime;

@end

NS_ASSUME_NONNULL_END
