//
//  TransactionEntity+CoreDataClass.h
//  Bet365
//
//  Created by HamaJi on 2018/8/16.
//  Copyright © 2018年 jesse. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MetricsEntity;

NS_ASSUME_NONNULL_BEGIN

@interface TransactionEntity : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TransactionEntity+CoreDataProperties.h"
