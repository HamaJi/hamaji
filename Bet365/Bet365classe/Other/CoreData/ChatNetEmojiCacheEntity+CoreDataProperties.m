//
//  ChatNetEmojiCacheEntity+CoreDataProperties.m
//  Bet365
//
//  Created by adnin on 2020/4/26.
//  Copyright © 2020 jesse. All rights reserved.
//
//

#import "ChatNetEmojiCacheEntity+CoreDataProperties.h"

@implementation ChatNetEmojiCacheEntity (CoreDataProperties)

+ (NSFetchRequest<ChatNetEmojiCacheEntity *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"ChatNetEmojiCacheEntity"];
}

@dynamic filePath;
@dynamic time;
@dynamic name;
@dynamic useCount;
@dynamic identifier;
@dynamic width;
@dynamic height;

@end
