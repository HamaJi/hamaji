//
//  RedPackEntity+CoreDataProperties.m
//  Bet365
//
//  Created by HHH on 2018/11/24.
//  Copyright © 2018年 jesse. All rights reserved.
//
//

#import "RedPackEntity+CoreDataProperties.h"

@implementation RedPackEntity (CoreDataProperties)

+ (NSFetchRequest<RedPackEntity *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"RedPackEntity"];
}

@dynamic userID;
@dynamic redpackID;
@dynamic roomID;
@dynamic timeStamp;

@end
