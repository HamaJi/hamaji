//
//  ChatFollowEntity+CoreDataClass.h
//  Bet365
//
//  Created by HHH on 2018/12/4.
//  Copyright © 2018年 jesse. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatFollowEntity : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "ChatFollowEntity+CoreDataProperties.h"
