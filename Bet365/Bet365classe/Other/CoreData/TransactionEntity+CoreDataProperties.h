//
//  TransactionEntity+CoreDataProperties.h
//  Bet365
//
//  Created by HamaJi on 2018/8/16.
//  Copyright © 2018年 jesse. All rights reserved.
//
//

#import "TransactionEntity+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TransactionEntity (CoreDataProperties)

+ (NSFetchRequest<TransactionEntity *> *)fetchRequest;

@property (nonatomic) int64_t connectEndDate;
@property (nonatomic) int64_t connectStartDate;
@property (nonatomic) int64_t domainLookupEndDate;
@property (nonatomic) int64_t domainLookupStartDate;
@property (nonatomic) int64_t fetchStartDate;
@property (nonatomic) int64_t requestEndDate;
@property (nonatomic) int64_t requestStartDate;
@property (nonatomic) int64_t responseEndDate;
@property (nonatomic) int64_t responseStartDate;
@property (nullable, nonatomic, retain) MetricsEntity *metrics;

@end

NS_ASSUME_NONNULL_END
