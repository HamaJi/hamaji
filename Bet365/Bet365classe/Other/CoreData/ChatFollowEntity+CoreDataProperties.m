//
//  ChatFollowEntity+CoreDataProperties.m
//  Bet365
//
//  Created by HHH on 2018/12/4.
//  Copyright © 2018年 jesse. All rights reserved.
//
//

#import "ChatFollowEntity+CoreDataProperties.h"

@implementation ChatFollowEntity (CoreDataProperties)

+ (NSFetchRequest<ChatFollowEntity *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"ChatFollowEntity"];
}

@dynamic userID;
@dynamic followUserID;
@dynamic roomID;

@end
