//
//  ChatNetEmojiCacheEntity+CoreDataProperties.h
//  Bet365
//
//  Created by adnin on 2020/4/26.
//  Copyright © 2020 jesse. All rights reserved.
//
//

#import "ChatNetEmojiCacheEntity+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ChatNetEmojiCacheEntity (CoreDataProperties)

+ (NSFetchRequest<ChatNetEmojiCacheEntity *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *filePath;
@property (nullable, nonatomic, copy) NSDate *time;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSNumber *useCount;
@property (nullable, nonatomic, copy) NSString *identifier;
@property (nullable, nonatomic, copy) NSNumber *width;
@property (nullable, nonatomic, copy) NSNumber *height;

@end

NS_ASSUME_NONNULL_END
