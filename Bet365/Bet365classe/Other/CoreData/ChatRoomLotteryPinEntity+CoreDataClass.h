//
//  ChatRoomLotteryPinEntity+CoreDataClass.h
//  Bet365
//
//  Created by adnin on 2020/3/31.
//  Copyright © 2020 jesse. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatRoomLotteryPinEntity : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "ChatRoomLotteryPinEntity+CoreDataProperties.h"
