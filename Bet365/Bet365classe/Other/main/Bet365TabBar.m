//
//  Bet365TabBar.m
//  Bet365
//
//  Created by adnin on 2019/3/3.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "Bet365TabBar.h"
#import <SDWebImage/UIButton+WebCache.h>

#define TABBAR_HEIGH 49.0

#define TABBARSCROLL_LEADING 17

#define TABBARSCROLL_WIDTH (WIDTH - TABBARSCROLL_LEADING *2.0)

#define TABBARSCROLL_MIN_COUNT  6

#define TABBARITEM_MIN_WIDHT (TABBARSCROLL_WIDTH / TABBARSCROLL_MIN_COUNT)

@interface Bet365TabBar()<UIScrollViewDelegate>

@property (strong,nonatomic)UIButton *leftBtn;

@property (strong,nonatomic)UIButton *rightBtn;

@property (strong,nonatomic)UIScrollView *scrollView;

@property (strong,nonatomic)NSMutableArray *mTemplateList;

@property (strong,nonatomic)NSMutableArray <UIButton *>*buttonList;

@end

@implementation Bet365TabBar

@dynamic templateList;

-(instancetype)init{
    if (self = [super init ]) {
        [self setUI];
        [self addObserver];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setUI];
        [self addObserver];
    }
    return self;
}

//- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
//    __block UIButton *pointBtn;
//    [self.buttonList enumerateObjectsUsingBlock:^(UIButton * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        CGPoint newP = [self convertPoint:point toView:obj];
//        if ( [obj pointInside:newP withEvent:event]) {
//            pointBtn = obj;
//        }
//    }];
//    if (!pointBtn) {
//        CGPoint newP = [self convertPoint:point toView:self.leftBtn];
//        if ( [self.leftBtn pointInside:newP withEvent:event]) {
//            return self.leftBtn;
//        }
//        newP = [self convertPoint:point toView:self.rightBtn];
//        if ( [self.rightBtn pointInside:newP withEvent:event]) {
//            return self.rightBtn;
//        }
//    }else{
//        return pointBtn;
//    }
//    return [super hitTest:point withEvent:event];
//}

#pragma mark - Private
-(void)addObserver{
    @weakify(self);
    [RACObserve(self, selectIdx) subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self scrollViewDidScroll:self.scrollView];
        });
        [self.buttonList enumerateObjectsUsingBlock:^(UIButton * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (idx == self.selectIdx) {
                [obj setEnabled:NO];
                [obj setTintColor:[UIColor skinTabSelColor]];
            }else{
                [obj setEnabled:YES];
                [obj setTintColor:[UIColor skinTabNorColor]];
            }
        }];
    }];
}
-(void)setUI{
    self.translucent = NO;
    [self addSubview:self.scrollView];
//    [self setFrame:CGRectMake(0, 0, WIDTH, is_IphoneX ? TABBAR_HEIGH + 34 : TABBAR_HEIGH)];
}

-(UIButton*)setTabBarButtonImage:(id)image selectedImage:(id)selectImage placheImage:(UIImage *)placheImage title:(NSString *)titleString Tag:(NSUInteger)tag{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    [button setContentMode:UIViewContentModeScaleAspectFill];
    [button setTitle:titleString forState:UIControlStateNormal];
    [button setTitle:titleString forState:UIControlStateDisabled];
    
    [button setTitleColor:[UIColor skinTabSelColor] forState:UIControlStateDisabled];
    [button setTitleColor:[UIColor skinTabNorColor] forState:UIControlStateNormal];

    [button.titleLabel setFont:[UIFont systemFontOfSize:10]];
    button.tag = tag;
    [button addTarget:self action:@selector(tapAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([image isKindOfClass:[UIImage class]]) {
        [button setImage:image forState:UIControlStateNormal];
    }else if ([image isKindOfClass:[NSString class]]){
        NSString *imageStr = image;
        if ([imageStr hasPrefix:@"http"]) {
            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:imageStr] options:SDWebImageDownloaderLowPriority progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                if (finished && image) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [button setImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
                        [button setImagePosition:LXMImagePositionTop spacing:5];
                    });
                }
            }];
        }else{
            UIImage *mImage = [[UIImage imageNamed:imageStr] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [button setImage:mImage forState:UIControlStateNormal];
        }
    }
    
    if ([selectImage isKindOfClass:[UIImage class]]) {
        [button setImage:image forState:UIControlStateDisabled];
    }else if ([selectImage isKindOfClass:[NSString class]]){
        NSString *imageStr = selectImage;
        if ([imageStr hasPrefix:@"http"]) {
            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:imageStr] options:SDWebImageDownloaderLowPriority progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                if (finished && image) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [button setImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateDisabled];
                        [button setImagePosition:LXMImagePositionTop spacing:5];
                    });
                }
            }];
        }else{
            UIImage *mImage = [[UIImage imageNamed:imageStr] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [button setImage:mImage forState:UIControlStateDisabled];
            
        }
    }
    
    [button setImagePosition:LXMImagePositionTop spacing:5];
    return button;
}

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.buttonList.count <= TABBARSCROLL_MIN_COUNT) {
        return;
    }
    CGFloat x = scrollView.contentOffset.x;
    [self.leftBtn setHidden:(x <= 0)];
    [self.rightBtn setHidden:(x + TABBARSCROLL_WIDTH - scrollView.contentSize.width > 0)];
    
}

#pragma mark - Events
-(void)tapAction:(UIButton *)sender{
//    if (self.selectIdx == sender.tag) {
//        return;
//    }
    self.selectIdx = sender.tag;
}

-(void)leftTapAction:(UIButton *)sender{
//    [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentOffset.x - TABBARSCROLL_WIDTH, self.scrollView.contentOffset.y) animated:YES];
}
-(void)rightTapAction:(UIButton *)sender{
    
//    [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentOffset.x + TABBARSCROLL_WIDTH, self.scrollView.contentOffset.y) animated:YES];
    
    
}
#pragma mark - GET/SET
-(void)setTemplateList:(NSArray<TabBarTemplate *> *)templateList{
    
    self.mTemplateList = templateList.mutableCopy;
    [self.buttonList removeAllObjects];
    [self.scrollView removeAllSubviews];
    __block CGFloat contentWidth = 0.0;
    
    [self.mTemplateList enumerateObjectsUsingBlock:^(TabBarTemplate * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIButton *button = [self setTabBarButtonImage:obj.icon
                                        selectedImage:obj.selIcon
                                          placheImage:[UIImage imageNamed:@"tabbar_mine_nor"]
                                                title:obj.name
                                                  Tag:idx];
        button.ycz_x = contentWidth;
        button.ycz_y = 0;
        button.ycz_height = TABBAR_HEIGH - 5;
        
        if (self.mTemplateList.count > 5) {
            button.ycz_width = TABBARITEM_MIN_WIDHT;
        }else{
            button.ycz_width = (TABBARSCROLL_WIDTH / self.mTemplateList.count);
        }
        contentWidth += button.ycz_width;
        [self.scrollView addSubview:button];
        [self.buttonList addObject:button];
    }];
    [self.scrollView setContentSize:CGSizeMake(contentWidth, 0)];
    [self.scrollView scrollToLeft];
    
}

-(NSArray<TabBarTemplate *> *)templateList{
    return self.mTemplateList;
}

-(UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(TABBARSCROLL_LEADING, 5, TABBARSCROLL_WIDTH, TABBAR_HEIGH - 5)];
        _scrollView.backgroundColor = [UIColor clearColor];
        _scrollView.delegate = self;
    }
    return _scrollView;
}

-(UIButton *)leftBtn{
    if (!_leftBtn) {
        _leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:_leftBtn];
        _leftBtn.ycz_x = 0;
        _leftBtn.ycz_y = 5;
        _leftBtn.ycz_width = TABBARSCROLL_LEADING;
        _leftBtn.ycz_height = TABBAR_HEIGH - 5;
        [_leftBtn addTarget:self action:@selector(leftTapAction:) forControlEvents:UIControlEventTouchUpInside];
        
        UIImage *mImage = [[UIImage imageNamed:@"tab_left"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_leftBtn setImage:mImage forState:UIControlStateNormal];
//        [_leftBtn setImage:mImage forState:UIControlStateDisabled];
        [_leftBtn setTintColor:[UIColor skinTabSelColor]];

    }
    return _leftBtn;
}



-(UIButton *)rightBtn{
    if (!_rightBtn) {
        _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];

        [self addSubview:_rightBtn];
        _rightBtn.ycz_x = TABBARSCROLL_LEADING + TABBARSCROLL_WIDTH;
        _rightBtn.ycz_y = 5;
        _rightBtn.ycz_width = TABBARSCROLL_LEADING;
        _rightBtn.ycz_height = TABBAR_HEIGH - 5;
        [_rightBtn addTarget:self action:@selector(rightTapAction:) forControlEvents:UIControlEventTouchUpInside];
        
        UIImage *mImage = [[UIImage imageNamed:@"tab_right"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_rightBtn setImage:mImage forState:UIControlStateNormal];
//        [_rightBtn setImage:mImage forState:UIControlStateDisabled];
        [_rightBtn setTintColor:[UIColor skinTabSelColor]];

    }
    return _rightBtn;
}
-(void)setTabBgColor:(UIColor *)tabBgColor{
    if (!tabBgColor) {
        return;
    }
    if ([_tabBgColor isEqual:tabBgColor]) {
        return;
    }
    _tabBgColor = tabBgColor;
    UIImage *img = [UIImage imageWithColor:_tabBgColor ? _tabBgColor : [UIColor blackColor]
                                        withSize:self.bounds.size];
    if ([BET_CONFIG.allocation.skinName isEqualToString:[UIColor skinGoldName]]) {
        
        img = [UIImage gradientImageWithSize:self.bounds.size
                                   andColors:@[[UIColor skinTabBgColor],COLOR_WITH_HEX(0x795548)]
                                GradientType:UIImageGradientType_VER];
    }

    [self setBackgroundImage:img];
}


-(NSMutableArray <UIButton *>*)buttonList{
    if (!_buttonList) {
        _buttonList = @[].mutableCopy;
    }
    return _buttonList;
}

-(void)setSelectIdx:(NSUInteger)selectIdx{
    if (self.tabDelegate && [self.tabDelegate respondsToSelector:@selector(bet365TabBar:WillSelectIndex:)]) {
        BOOL allowed = [self.tabDelegate bet365TabBar:self WillSelectIndex:selectIdx];
        if (allowed && [self.tabDelegate respondsToSelector:@selector(bet365TabBar:DidSelectIndex:)]) {
            [self.tabDelegate bet365TabBar:self DidSelectIndex:selectIdx];
            _selectIdx = selectIdx;
        }
    }
}
@end

