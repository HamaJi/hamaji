//
//  Bet365NavViewController.m
//  Bet365
//
//  Created by jesse on 2017/4/12.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "Bet365NavViewController.h"
#import "Bet365BettingResultViewController.h"
@interface Bet365NavViewController ()

@end

@implementation Bet365NavViewController

+ (void)initialize{
    
    if (self == [Bet365NavViewController class]) {
        

    }
    
}
-(instancetype)initWithRootViewController:(UIViewController *)rootViewController{
    if (self = [super initWithRootViewController:rootViewController]) {
//        [self setConfig];
    }
    return self;
}


-(void)setConfig{
    
    [self.navigationBar setTintColor:[UIColor skinNaviTintColor]];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName :[UIColor skinNaviTintColor]};
    UIImage *bg = [UIImage skinGradientVerNaviImage];
    self.navigationBar.contentMode = UIViewContentModeScaleToFill;
    [self.navigationBar setBackgroundImage:bg
                                       forBarMetrics:UIBarMetricsDefault];
}
//
//-(instancetype)init{





































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































//    if (self = [super init]) {
//        [self setUI];
//    }
//    return self;
//}
//-(instancetype)initWithCoder:(NSCoder *)aDecoder{
//    if (self = [super initWithCoder:aDecoder]) {
//        [self setUI];
//    }
//    return self;
//}
//
//-(void)setUI{
//    UINavigationBar *bar = [UINavigationBar appearanceWhenContainedIn:self.class, nil];
//    bar.barStyle = UIBarStyleBlack;
//    bar.tintColor = [UIColor whiteColor];
//    [bar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
//    [bar setBarTintColor:[UIColor skinNaviBgColor]];
//    [bar setShadowImage:[UIImage createImageWithColor:[UIColor clearColor]]];
//}


- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    viewController.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:nil action:nil];
//    if (self.viewControllers.count > 0) {
//        viewController.hidesBottomBarWhenPushed = YES;
//    }
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!self.viewControllers.count) {
            [super pushViewController:viewController animated:animated];
        }else{
            BOOL allowed = ([[viewController class] respondsToSelector:@selector(beAllowedPushViewConrtoller:)] ? [[viewController class] beAllowedPushViewConrtoller:nil] : YES);
            if (allowed) {
                [super pushViewController:viewController animated:animated];
            }
        }
    });
    
}
-(UIViewController *)popViewControllerAnimated:(BOOL)animated{
    [self filterContentSelector];
    return [super popViewControllerAnimated:animated];
}
- (void)filterContentSelector
{
    NSString *visible = NSStringFromClass([self.visibleViewController class]);
    if ([visible isEqualToString:@"Bet365BettingResultViewController"]){
        Bet365BettingResultViewController *result = (Bet365BettingResultViewController *)self.visibleViewController;
        [result popLastController];
    }else{
        if([visible isEqualToString:@"BetTotal365ViewController"])[self postNSNotifuser];
    }
}
-(void)cleanCustomCache
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSURLCache *cache = [NSURLCache sharedURLCache];
    [cache removeAllCachedResponses];
    [cache setDiskCapacity:0];
    [cache setMemoryCapacity:0];
}
//停止定时器
-(void)postNSNotifuser
{
    BetTotal365ViewController *tolalC = (BetTotal365ViewController *)self.visibleViewController;
    [tolalC deletedesKindView];
}
@end

