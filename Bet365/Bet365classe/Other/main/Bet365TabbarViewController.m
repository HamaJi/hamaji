//
//  Bet365TabbarViewController.m
//  Bet365
//
//  Created by jesse on 2017/4/12.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "Bet365TabbarViewController.h"
#import "KFCPMoveMentViewController.h"
#import "Bet365NavViewController.h"

#import "jesseLogBet365ViewController.h"
#import "HomePageFactory.h"
#import "Bet365TabBar.h"

#define HEIGHT_BOTTOM_MARGIN   (is_IphoneX?34:0)

@interface Bet365TabbarViewController ()
<
UITabBarControllerDelegate,
Bet365TabBarDelegate
>

@property (nonatomic,strong)Bet365TabBar *kTabBar;


@end

@implementation Bet365TabbarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addObser];

}
-(void)dealloc{
    
}
-(void)viewDidDisappear:(BOOL)animated{
    
}
- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self.tabBar.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:NSClassFromString(@"UITabBarButton")]) {
            [obj removeFromSuperview];
        }
    }];
    self.tabBar.frame = CGRectMake(0, HEIGHT-49-HEIGHT_BOTTOM_MARGIN, WIDTH, 49+HEIGHT_BOTTOM_MARGIN);
}

-(void)setSelectedIndex:(NSUInteger)selectedIndex{
    self.kTabBar.selectIdx = selectedIndex;
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    
    Class class = viewController.class;
    BOOL allowd = YES;
    if ([class respondsToSelector:@selector(beAllowedPushViewConrtoller:)]) {
        allowd = [class beAllowedPushViewConrtoller:nil];

    }
    return allowd; 
}

#pragma param - Bet365TabBarDelegate
-(BOOL)bet365TabBar:(Bet365TabBar *)tabBar WillSelectIndex:(NSUInteger)idx{
    UINavigationController *navi = [self.childViewControllers safeObjectAtIndex:idx];
    UIViewController *viewController = [navi.viewControllers safeObjectAtIndex:0];
    if (!viewController) {
        return NO;
    }
    return [self tabBarController:self shouldSelectViewController:viewController];
}

-(void)bet365TabBar:(Bet365TabBar *)tabBar DidSelectIndex:(NSUInteger)idx{
    [[NAVI_MANAGER getCurrentVC].navigationController popViewControllerAnimated:NO];
    [super setSelectedIndex:idx];
}

#pragma mark - Private
-(void)initUI{
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    self.delegate = self;
}

-(void)addObser{
    @weakify(self);
    
//    [BET_CONFIG.updateSubject subscribeNext:^(id  _Nullable x) {
//        @strongify(self);
//        if ([x integerValue] == NetReceiveObjType_RESOPONSE) {
//            static dispatch_once_t onceToken;
//            dispatch_once(&onceToken, ^{
//                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                    if (BET_CONFIG.noticeModel.index_notice.count > 0) {
//                        [self presentViewController:self.noteVC animated:YES completion:nil];
//                    }
//                });
//            });
//        }
//    }];
    
    [[RACObserve(USER_DATA_MANAGER, messegesCount) distinctUntilChanged]  subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        NSInteger index = [self viewControllers].count - 1;
        if (index > 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
//                [x integerValue] ?
//                [self.tabBar showBadgeOnItemIndex:index] :
//                [self.tabBar hideBadgeOnItemIndex:index];
#pragma clang diagnostic pop
            });
        }
    }];

    [[RACObserve(BET_CONFIG, allocation) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        dispatch_async(dispatch_get_main_queue(), ^{
            MainAllocationModel *allocation = (MainAllocationModel *)x;
            if (allocation) {

                [self.childViewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    [obj removeFromParentViewController];
                }];
//                [[UINavigationBar appearance] setBarTintColor:[UIColor skinNaviBgColor]];
                [[UINavigationBar appearance] setTintColor:[UIColor skinNaviTintColor]];
                [UINavigationBar appearance].translucent = YES;
                [UINavigationBar appearance].titleTextAttributes = @{NSForegroundColorAttributeName :[UIColor skinNaviTintColor]};
                UIImage *bg = [UIImage skinGradientVerNaviImage];
                [UINavigationBar appearance].contentMode = UIViewContentModeScaleToFill;
                [[UINavigationBar appearance] setBackgroundImage:bg
                                         forBarMetrics:UIBarMetricsDefault];
                [allocation.templateList_tabbar enumerateObjectsUsingBlock:^(TabBarTemplate * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {

                    
                    
                    [UIViewController routerShareInstanceByUrl:obj.type toHandle:^(UIViewController *vc) {
                        if (vc) {
                            Bet365NavViewController *nav = [[Bet365NavViewController alloc]initWithRootViewController:vc];
                            vc.tabBarItem = nil;
                            [self addChildViewController:nav];
                        }
                    }];
                }];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                    [self.tabBar setBackgroundImage:[UIImage createImageWithColor:COLOR_WITH_HEX(0xffffff)]];
                    
                    [self setValue:self.kTabBar forKey:@"tabBar"];
                    self.kTabBar.templateList = allocation.templateList_tabbar;
                    self.kTabBar.tabBgColor = [UIColor skinTabBgColor];
                    [self.kTabBar setSelectIdx:0];
                    [self.view layoutIfNeeded];
                });
            }
        });
    }];
}


-(Bet365TabBar *)kTabBar{
    if (!_kTabBar) {
        _kTabBar = [[Bet365TabBar alloc] init];
        _kTabBar.tabDelegate = self;
    }
    return _kTabBar;
}


@end
