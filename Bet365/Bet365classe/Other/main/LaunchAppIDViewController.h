//
//  LaunchAppIDViewController.h
//  Bet365
//
//  Created by adnin on 2019/7/24.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LaunchAppIDViewController : Bet365ViewController

@property (nonatomic,copy) void(^updateAppId)();

@property (nonatomic,strong) NSString *appID;

@end

NS_ASSUME_NONNULL_END
