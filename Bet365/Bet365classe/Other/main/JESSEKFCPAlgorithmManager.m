//
//  JESSEKFCPAlgorithmManager.m
//  Bet365
//
//  Created by jesse on 2017/6/1.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "JESSEKFCPAlgorithmManager.h"
#import <objc/message.h>
#import "Bet365OfficalResultModel.h"
static JESSEKFCPAlgorithmManager *manger = nil;
@interface JESSEKFCPAlgorithmManager()
@property(nonatomic,assign)int lenN;
@property(nonatomic,strong)NSMutableArray *rescurviceArr;
@property(nonatomic,strong)NSMutableArray *recursiveResultArr;
@property(nonatomic,assign)int indexC;//坐标控制组合
@property(nonatomic,assign)int R9count;//实用R9
@property(nonatomic,assign)int indexxx;
@end
@implementation JESSEKFCPAlgorithmManager
+(instancetype)shareCurrenManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manger = [[JESSEKFCPAlgorithmManager alloc]init];
    });
    return manger;
    
}
NSArray *CPrint(NSArray *ary,int len){
    
    NSMutableArray *result = [[NSMutableArray alloc]init];
    int aryLen = (int)ary.count;
    if (len>aryLen) {
        return result;
    }
    NSMutableArray *bs = [[NSMutableArray alloc]init];
    for (int i = 0; i<aryLen; i++) {
        [bs addObject:@"0"];
    }
    //初始化
    for (int i=0 ;i<len ; i++) {
        [bs replaceObjectAtIndex:i withObject:@"1"];
    }
    if (len==ary.count) {
        [result addObject:addCRecord(bs, ary)];
        return result;
    }
    BOOL flag = true;
    BOOL tempFlag = false;
    int pos = 0;
    int sum = 0;
    manger.indexC = 0;
    //首先找到第一个10组合，然后变成01.同时将左边所有的1移动到数组的最左边
    return doTask(sum, pos, tempFlag, result, flag, bs, ary, aryLen, len);
}
NSArray *doTask(int sum , int pos ,BOOL tempFiag,NSMutableArray *result,BOOL flag,NSMutableArray *bs,NSArray *ary,int aryLen,int len){
    sum = 0;
    pos = 0;
    tempFiag = YES;
    [result addObject:addCRecord(bs, ary)];
    return doSome(sum,pos,tempFiag,result,flag,bs,ary,aryLen,len);
}
NSArray *doSome(int sum , int pos ,BOOL tempFiag,NSMutableArray *result,BOOL flag,NSMutableArray *bs,NSArray *ary,int aryLen,int len){
    for (int i = 0; i<aryLen-1; i++) {
        if ([bs[i] integerValue]==1&&[bs[i+1] integerValue]==0) {
            [bs replaceObjectAtIndex:i withObject:@"0"];
            [bs replaceObjectAtIndex:i+1 withObject:@"1"];
            pos = i;
            break;
        }
    }
    //将左边的1全部移动到数组的最左边
    for (int i = 0 ; i<pos; i++) {
        if ([bs[i] integerValue]==1) {
            sum++;
        }
    }
    for (int i = 0 ; i<pos; i++) {
        if (i<sum) {
            [bs replaceObjectAtIndex:i withObject:@"1"];
        }else{
            [bs replaceObjectAtIndex:i withObject:@"0"];
        }
    }
    //检查是否所有的1都移动到了最友边
    for (int i = aryLen-len; i<aryLen; i++) {
        if ([bs[i] integerValue]==0) {
            tempFiag = false;
            break;
        }
    }
    while (!tempFiag) {
    tempFiag = !tempFiag;
    sum = 0;
    [result addObject:addCRecord(bs, ary)];
    doSome(sum, pos, tempFiag, result, flag, bs, ary, aryLen, len);
    break;
    }
    manger.indexC++;
    if (manger.indexC==1) {
        [result addObject:addCRecord(bs, ary)];
    }
    return result;
    
}
NSArray *addCRecord(NSArray *bs,NSArray *ary){
    NSMutableArray *record = [[NSMutableArray alloc]init];
    [bs enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *number = obj;
        if ([number integerValue]==1) {
            [record addObject:ary[idx]];
        }
    }];
    return record;
}
int numsToComb(NSArray *parameter){
    if (parameter.count==2) {
        return ((NSArray *)parameter[0]).count<[parameter[1] integerValue]?0:C((int)((NSArray *)parameter[0]).count, [parameter[1] intValue]);
    }
    NSMutableArray *repeatNum = [[NSMutableArray alloc]init];
    for (int i = 0; i<(int)((NSArray *)parameter[0]).count; i++) {
        if (arrayIndexOf(parameter[2], parameter[0][i])>-1) {
            [repeatNum addObject:parameter[0][i]];
        }
    }
    int repeatNumLen = (int)repeatNum.count;
    int row1Len = (int)((NSArray *)parameter[0]).count;
    int noRepeatNumLen = row1Len-repeatNumLen;
    int row2Len = (int)((NSArray *)parameter[2]).count;
    switch ([parameter[1] intValue]) {
        case 1:
        return C(noRepeatNumLen, 1)*C(row2Len, [parameter[3] intValue])+C(repeatNumLen, 1)*C(row2Len-1, [parameter[3] intValue]);
        break;
        case 2:
        return C(noRepeatNumLen, 2)*C(row2Len, [parameter[3] intValue])+C(noRepeatNumLen, 1)*C(repeatNumLen, 1)*C(row2Len-1, [parameter[3] intValue])+C(repeatNumLen, 2)*C(row2Len-2, [parameter[3] intValue]);
        break;
        default:
        break;
    }
    return 0;
}
NSArray *makeNumAry(int start,int end,bool needPadLeftZero){
    NSMutableArray *ary = [[NSMutableArray alloc]init];
    for (int i = start; i<=end; i++) {
        if (needPadLeftZero) {
            if (i<10) {
                [ary addObject:[NSString stringWithFormat:@"0%d",i]];
            }else{
                [ary addObject:[NSString stringWithFormat:@"%d",i]];
            }
        }else{
            [ary addObject:[NSString stringWithFormat:@"%d",i]];
        }
    }
    return ary;
}
#pragma mark--多余下注处理
int arrayIndexOf(NSArray *ary,NSString *num){
    for (int i = 0 ; i<ary.count; i++) {
        if ([ary[i] isEqualToString:num]) {
            return i;
        }
    }
    return -1;
}
#pragma mark--随机值处理
-(void)arcNumberAndUtilNumbe:(int)untilN AndUntilC:(int)untilPoor And:(void(^)(int arc))arcblock{
    int arc;
    arc = rand()%untilN;
    if (arc<untilPoor) {
        for (int randC = 0 ; randC<100; randC++) {
            arc = [manger arcUntil:untilN];
            if (arc>untilPoor) {
                break;
            }
        }
    }
    arcblock(arc);
}
-(int)arcUntil:(int)unit
{
    return rand()%unit;
}
#pragma mark--随机拿出当前的随机值
int ranDom(NSArray *range){
   __block int randNum;
    if (range!=nil) {
    [manger arcNumberAndUtilNumbe:[range[range.count-1] intValue]+1 AndUntilC:[range[0] intValue] And:^(int arc) {
        randNum = arc;
    }];
    }else{
        randNum = rand()%10;
    }
    return randNum;
}
#pragma mark--计算随机重复
NSArray *arrayGetDisTinctValues(NSArray *currenArr){
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    NSMutableArray *newArr = [[NSMutableArray alloc]init];
    for (int i= 0 ; i<currenArr.count; i++) {
        if (!dict[[NSString stringWithFormat:@"%@",currenArr[i]]]) {
            dict[[NSString stringWithFormat:@"%@",currenArr[i]]] = @"1";
            [newArr addObject:currenArr[i]];
        }
    }
    return newArr;
}
#pragma mark--索引检测
bool indexxStaus(NSString *algoruthm){
    NSString *algString = [NSString stringWithFormat:@"%@",algoruthm];
    if ([algString isEqualToString:@"R6"]) {
        BET_CONFIG.Special = YES;
    }
    NSArray *indexxArr = indexCurrenArr();
   __block bool currenStaus = false;
    [indexxArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([algString isEqualToString:obj]) {
            currenStaus = YES;
        }
    }];
    return currenStaus;
}
#pragma mark--单式索引检测
bool unitaryIndexStaus(NSString *algoruthm){
    __block bool staus = false;
    [UnitaryIndexArr() enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isEqualToString:algoruthm]) {
            staus = YES;
        }
    }];
    return staus;
}

#pragma mark-返回对应算法结果
NSArray *allAlgorithm (NSString *algorithm,NSArray *parameterArr){
    NSArray *betinfo;

    if ([algorithm isEqualToString:@"R4"]) {
        betinfo = R4(parameterArr[0], parameterArr[1], parameterArr[2], parameterArr[3], parameterArr[4], parameterArr[5]);
    }else if ([algorithm isEqualToString:@"R8"]){
        betinfo = R8(parameterArr[0], parameterArr[1], parameterArr[2]);
    }else if ([algorithm isEqualToString:@"R1"]){
        betinfo = R1(parameterArr[0], parameterArr[1], parameterArr[2]);
    }else if ([algorithm isEqualToString:@"danshi1"]){
        betinfo = danshi1(parameterArr[0], parameterArr[1], parameterArr[2], parameterArr[3]);
    }else if ([algorithm isEqualToString:@"danshi2"]){
        betinfo = danshi2(parameterArr[0], parameterArr[1], parameterArr[2], parameterArr[3]);
    }else if ([algorithm isEqualToString:@"R6"]){
        betinfo = R6(parameterArr[0], parameterArr[1]);
    }else if ([algorithm isEqualToString:@"R2"]){
        betinfo = R2(parameterArr[0], parameterArr[1]);
    }else if ([algorithm isEqualToString:@"R3"]){
        betinfo = R3(parameterArr[0], parameterArr[1], parameterArr[2], parameterArr[3]);
    }else if ([algorithm isEqualToString:@"R5"]){
        betinfo = R5(parameterArr[0], parameterArr[1]);
    }else if ([algorithm isEqualToString:@"R7"]){
        betinfo = R7(parameterArr[0], parameterArr[1], parameterArr[2], parameterArr[3]);
    }else if ([algorithm isEqualToString:@"R9"]){
        betinfo = R9(parameterArr[0], parameterArr[1]);
    }else if ([algorithm isEqualToString:@"R10"]){
        betinfo = R10(parameterArr[0], parameterArr[1]);
    }else if ([algorithm isEqualToString:@"R11"]){
        betinfo = R11(parameterArr[0], parameterArr[1]);
    }
    return betinfo;
}
NSArray *danshi1(NSString *content,NSString *openNumberLen,NSDictionary *tplConfig,NSString *min){
    if (content.length==0||!content) {
        return @[@"0",@""];
    }
    NSArray *contentArr = checkCurrenText(content);
    NSMutableArray *ary = [[NSMutableArray alloc]init];
    NSMutableArray *nums = [[NSMutableArray alloc]init];
    NSMutableArray *repeatAry = [[NSMutableArray alloc]init];
    if ([openNumberLen intValue]==1) {
        nums = [[NSMutableArray alloc]initWithArray:contentArr];
    }else{//两个号码分割
    NSArray *rang = ((NSArray *)tplConfig[@"sub"])[1];
        for (int i = 0 ; i+1<contentArr.count; i+=[openNumberLen intValue]) {
            NSString *numStr = [NSString stringWithFormat:@"%@%@",contentArr[i],contentArr[i+1]];
            int numInt = [numStr intValue];
            if (numInt>=[rang[0] intValue]&&numInt<=[rang[1] intValue]) {
                [nums addObject:numStr];
            }
        }
    }
    NSRange rang = [tplConfig [@"format"][0] rangeOfString:@"m"];
    bool canRepeat = (rang.location)?YES:NO;
    for (int i = 0; i+[min intValue]-1<nums.count; i+=[min intValue]) {
      
        //检查一注中是否有重复的号码
        NSMutableArray *currenA = [[NSMutableArray alloc]init];
        for (NSInteger j = i; j<i+[min integerValue]; j++) {
            [currenA addObject:nums[j]];
        }
        if (canRepeat||arrayGetDisTinctValues(currenA).count==[min integerValue]) {
            NSMutableString *bet = [[NSMutableString alloc]init];
            [currenA enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [bet appendFormat:@"%@,",obj];
            }];
            if (bet.length>0) {
                [bet deleteCharactersInRange:NSMakeRange(bet.length-1,1 )];
            }
            //检查是否有相同的下注数据
            if (arrayIndexOf(ary, bet)==-1) {
                [ary addObject:bet];
            }else{
                [repeatAry addObject:bet];
            }
        }
    }
    if (ary.count==0) {
        return @[@"0",@""];
    }
    return @[[NSString stringWithFormat:@"%ld",ary.count],StringBybet(ary, false)];
}
NSArray *danshi2(NSString *content,NSString *openNumberLen,NSDictionary *tplConfig,NSString *min){
  return danshi1(content, openNumberLen, tplConfig, min);
}
NSArray *checkCurrenText(NSString *desT){
    NSRegularExpression *ecPression  = [NSRegularExpression regularExpressionWithPattern:@"[0-9]" options:NSRegularExpressionCaseInsensitive error:nil];
    NSArray *numberArr = [ecPression matchesInString:desT options:NSMatchingReportProgress range:NSMakeRange(0, desT.length)];
    NSMutableArray *checkText = [[NSMutableArray alloc]init];
      [numberArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
          NSTextCheckingResult *result = obj;
          [checkText addObject:[desT substringWithRange:result.range]];
      }];
    return checkText;
}
NSArray *R1(NSDictionary *tplConfig,id betContent,NSString *min){
    min = [NSString stringWithFormat:@"%@",min];
    min = (min.length==0)?@"1":min;
    int count =1;
    NSMutableArray *content = [[NSMutableArray alloc]init];
    for (int i = 0; i<((NSArray *)betContent).count; i++) {
        if (((NSArray *)betContent).count<1) {
            return @[@"0",@""];
        }
        count *=C((int)((NSArray *)((NSArray *)betContent)[i]).count, [min intValue]);
        NSString *stringByres = (((NSArray *)betContent).count>1)?StringBybet(((NSArray *)betContent)[i], true):StringBybet(((NSArray *)betContent)[i], false);
        [content addObject:stringByres];
    }
    return @[[NSString stringWithFormat:@"%d",count],StringBybet(content, false)];
}
NSString *StringBybet(NSArray *stringByArr,bool isMaxOne){
    NSMutableString *StringBy = [[NSMutableString alloc]init];
    [stringByArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (isMaxOne) {
            [StringBy appendFormat:@"%@,",obj];
        }else{
            [StringBy appendFormat:@"%@|",obj];
        }
        }];
    if (StringBy.length>0) {
        [StringBy deleteCharactersInRange:NSMakeRange(StringBy.length-1, 1)];
    }
    return StringBy;
}
NSArray *R2(NSDictionary *tplConfig,id betContent){
    NSArray *Result = R1(tplConfig, betContent, @"");
    NSInteger count = [Result[0] integerValue]*((NSArray *)betContent).count;
    return @[[NSString stringWithFormat:@"%ld",count],Result[1]];
}
NSArray *R3(NSDictionary *tplConfig,id betContent,NSString *row1Num,NSString *row2Num){
    int rowLenth = (int)((NSArray *)betContent).count;
    if (rowLenth==1&&[NSString stringWithFormat:@"%@",row2Num].length==0) {
        return @[[NSString stringWithFormat:@"%d",numsToComb(@[(NSArray *)betContent[0],row1Num])],StringBybet((NSArray *)betContent[0], false)];
    }
    if (rowLenth==2&&[NSString stringWithFormat:@"%@",row2Num].length>0) {

        NSString *contenOne = StringBybet((NSArray *)betContent[0], true);
        NSString *contenTwo = StringBybet((NSArray *)betContent[1], true);
         return @[[NSString stringWithFormat:@"%d",numsToComb(@[(NSArray *)betContent[0],row1Num,(NSArray *)betContent[1],row2Num])],StringBybet(@[contenOne,contenTwo], false)];
    }
    return @[@"",@""];
}
NSArray *R4(NSDictionary *tplConfig,id betContent,NSString *Numlen,NSString *MinValue ,NSString *MaxValue,NSString *type){
    int betCount = 0;
    for (int i = 0; i<((NSArray *)betContent).count; i++) {
        betCount+=combToNums([((NSArray *)betContent)[i] intValue], [Numlen intValue], [MinValue intValue], [MaxValue intValue], type);
    }
    NSMutableString *content = [[NSMutableString alloc]init];
     [((NSArray *)betContent) enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
         [content appendFormat:@"%@|",obj];
    }];
    if (content.length>0) {
         [content deleteCharactersInRange:NSMakeRange(content.length-1,1 )];
    }
    return @[[NSString stringWithFormat:@"%d",betCount],content];
}
NSArray *R5(NSDictionary *tplConfig,id betContent){
    int count = 2*C((int)((NSArray *)betContent[0]).count, 2);
    NSString *content = StringBybet((NSArray *)betContent[0], false);
    return @[[NSString stringWithFormat:@"%d",count],content];
}
NSArray *R6(NSDictionary *tplConfig,id betContent){
    int count = 0;
    NSMutableArray *betAry = [[NSMutableArray alloc]init];
    NSMutableArray *betPos = [[NSMutableArray alloc]init];
    NSMutableArray *betPosNames =[[NSMutableArray alloc]init];
    for (int i = 0; i<((NSArray *)betContent).count; i++) {
        if (((NSArray *)betContent[i]).count==0) {
            continue;
        }
        count+=((NSArray *)betContent[i]).count;
        [betAry addObject:StringBybet(betContent[i], true)];
        [betPos addObject:[NSString stringWithFormat:@"%d",i]];
        [betPosNames addObject:tplConfig[@"sub"][0][i]];
    }
    ALGORIGH_MANAGER.R6BetposCount = (int)betPos.count;
    return @[[NSString stringWithFormat:@"%d",count],StringBybet(betAry, false),betPos,betPosNames];
}
NSArray *R7(NSDictionary *tplConfig,id betContent,NSString *zhuHeLen,NSString  *rowLen){
    rowLen = ([NSString stringWithFormat:@"%@",rowLen].length>0)?rowLen:@"1";
    NSMutableArray *betContentC = [[NSMutableArray alloc]init];
    for (int i=0; i<(int)((NSArray *)betContent).count; i++) {
        if (((NSArray *)((NSArray *)betContent)[i]).count>0) {
            NSMutableDictionary *posDic = [[NSMutableDictionary alloc]init];
            posDic[@"pos"] = [NSString stringWithFormat:@"%d",i];
            posDic[@"name"] = tplConfig[@"sub"][0][i];
            posDic[@"nums"] = ((NSArray *)betContent)[i];
            [betContentC addObject:posDic];
        }
    }
    if (betContentC.count<[zhuHeLen intValue]) {
        return @[@"0",@""];
    }
    betContent = betContentC;
    int betCount = 0;
    NSMutableArray *showBetContent = [[NSMutableArray alloc]init];
    NSMutableArray *betPosAry = [[NSMutableArray alloc]init];
    NSMutableArray *showBetPosName = [[NSMutableArray alloc]init];
    NSMutableArray *betNumberArr = [[NSMutableArray alloc]init];
    NSArray *zhuHeAry = CPrint(betContent, [zhuHeLen intValue]);
    
    for (int i = 0 ; i<zhuHeAry.count; i++) {
        int zhuHeBetCount = 1;
        for (int j = 0; j<(int)((NSArray *)zhuHeAry[i]).count; j++) {
            zhuHeBetCount *=C((int)((NSArray *)zhuHeAry[i][j][@"nums"]).count, [rowLen intValue])
            ;
    
        }
        [betNumberArr addObject:[NSString stringWithFormat:@"%d",zhuHeBetCount]];
        betCount+=zhuHeBetCount;
    }
    for (int i = 0; i<(int)((NSArray *)betContent).count; i++) {
        [showBetContent addObject:StringBybet(betContent[i][@"nums"], true)];
        [showBetPosName addObject:betContent[i][@"name"]];
        [betPosAry addObject:betContent[i][@"pos"]];
    }
    return @[[NSString stringWithFormat:@"%d",betCount],betNumberArr,zhuHeAry,StringBybet(showBetContent, false),showBetPosName,betPosAry];
}
NSArray *R8(NSDictionary *tplConfig,id betContent,id rowLen){
    rowLen = ([rowLen intValue]>0)?rowLen:@"1";
    int count = C((int)((NSArray *)betContent).count, [rowLen intValue]);
    NSMutableString *content = [[NSMutableString alloc]init];
    for (NSString *betS in (NSArray *)betContent) {
        [content appendFormat:@"%@|",betS];
    }
    if (((NSArray *)betContent).count>0) {
        [content deleteCharactersInRange:NSMakeRange(content.length-1, 1)];
    }
    return @[[NSString stringWithFormat:@"%d",count],content];
}
NSArray *R9(NSDictionary *tplConfig, id betContent){
    bool isRepeat = false;//是否重复
    manger.indexC = 0;
    manger.R9count=0;
    caculateR9(0, [[NSMutableArray alloc]init], isRepeat, betContent);
    NSMutableArray *content = [[NSMutableArray alloc]init];
    for (int i = 0; i<((NSArray *)betContent).count; i++) {
        NSArray *objArr = ((NSArray *)betContent)[i];
        NSMutableArray *comareContent = [[NSMutableArray alloc]init];
        for (int j = 0 ; j<objArr.count; j++) {
            [comareContent addObject:objArr[j]];
        }
        [content addObject:StringBybet(comareContent, true)];
    }
    return @[[NSString stringWithFormat:@"%d",manger.indexC],StringBybet(content, false)];
}
NSArray *R10(NSDictionary *tplConfig,id betContent){
    return @[[NSString stringWithFormat:@"%ld",((NSArray *)betContent).count],StringBybet((NSArray *)betContent, true)];
}
NSArray *R11(NSDictionary *tplConfig,id betContent){
    return @[[NSString stringWithFormat:@"%ld",((NSArray *)betContent).count],StringBybet((NSArray *)betContent, true)];
}
void caculateR9(int index,NSMutableArray *beforeList,bool isRepeat,NSArray *betContent){
    NSArray *list = betContent[index];
    for (int i=0; i<list.count; i++) {

        if (index<betContent.count-1) {
            NSMutableArray *bList = [[NSMutableArray alloc]initWithArray:beforeList];
            [bList addObject:list[i]];
      
            caculateR9(index+1, bList, isRepeat, betContent);
        }else{
            __block bool NowisRepeat = false;
                for (int c=0; c<beforeList.count; c++) {
                    for (int b=c+1;b<beforeList.count ; b++) {
                        if ([beforeList[c] integerValue]==[beforeList[b] integerValue]) {
                            NowisRepeat = true;
                            break;
                        }
                    }
                }
            
            if (NowisRepeat==false) {
                int second =[list[i] intValue];
                for (int cc=0; cc<beforeList.count; cc++) {
                    if (second==[beforeList[cc] integerValue]) {
                        NowisRepeat = true;
                        break;
                    }
                }
            }
            if (NowisRepeat) {
                
            }else{
               manger.indexC++;
            }
        }
    }
}
#pragma mark--所有算法框架组合
int factorial(int t){
    int e = 1;
    for (; t>0; t--) {
        e*=t;
    }
    return e;
}
int C(int t, int e){
    return (t==0||e==0||e>t)?0:factorial(t)/(factorial(e)*factorial(t-e));
}
int combToNums(int Num,int Numlen,int minValue ,int MaxValue,NSString *type){
    bool canRepeat = ([type isEqualToString:@"C"])?YES:NO;
    int betCount = 0;
    int numberlenth = Numlen;

    NSMutableDictionary *map = [[NSMutableDictionary alloc]init];;
    //==========================进行计算============================//
   return caculate(numberlenth,map, canRepeat, Num, betCount, minValue, MaxValue, 0,Numlen,[[NSMutableArray alloc]init]);
}
int caculate(int NumLenZ,NSMutableDictionary *map,bool canRepeat,int Num, int betCount,int minValue,int maxValue,int index,int len, NSMutableArray *beforeAry){
    manger.lenN = len;
    for (int testNum = minValue; testNum<=maxValue; testNum++) {
    //    JesseLog(@"HZ=====|||%d",index+testNum);
        if (manger.lenN-1>0) {
            [beforeAry addObject:[NSString stringWithFormat:@"%d",testNum]];
            caculate(NumLenZ,map,canRepeat,Num,betCount,minValue,maxValue,testNum+index,len-1,beforeAry);
        }else if (manger.lenN-1==0){
        [beforeAry addObject:[NSString stringWithFormat:@"%d",testNum]];
            if (beforeAry.count==(maxValue-minValue+1)*NumLenZ) {
                [manger.rescurviceArr removeAllObjects];
                [manger.recursiveResultArr removeAllObjects];
                manger.rescurviceArr = beforeAry;
                //算法进行计算
                [manger cucaluteLM:(int)manger.rescurviceArr.count AndCaculteK:NumLenZ And:@""];
                for (NSInteger i = 0; i<manger.recursiveResultArr.count; i++) {
                    if (caculte(manger.recursiveResultArr[i], Num)) {
                        if (canRepeat) {
                            NSString *lastString = [manger.recursiveResultArr[i] substringWithRange:NSMakeRange(((NSString *)(manger.recursiveResultArr[i])).length-1, 1)];
                            int sameIndex = 0;
                            NSString *checkRepet = manger.recursiveResultArr[i];
                            for (NSInteger i = 0; i<checkRepet.length; i++) {
                                if ([lastString isEqualToString:[checkRepet substringWithRange:NSMakeRange(i, 1)]]) {
                                    sameIndex++;
                                }
                            }
                            if (sameIndex==((NSString *)manger.recursiveResultArr[i]).length) {
                                break ;
                            }
                            //先进行排序《从小到大》
                            NSString *NumberSV = manger.recursiveResultArr[i];
                            NSMutableArray *currBrfore = [[NSMutableArray alloc]init];
                            for (NSInteger j=0; j<NumberSV.length; j++) {
                                [currBrfore addObject:[NumberSV substringWithRange:NSMakeRange(j, 1)]];
                            }
                           NSArray *curA =  [currBrfore sortedArrayUsingSelector:@selector(compare:)];

                            NSMutableString *d = [[NSMutableString alloc]init];
                            [curA enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                [d appendString:obj];
                            }];
                            if (![map[d] isEqualToString:@"1"]) {
                               betCount++;
                                map[d] = @"1";
                              //  JesseLog(@"%@",d);
                            }
                            //JesseLog(@"%@",d);
                        }else{
                            betCount++;
                        }
                    }
                }
            }
        }
    }

    //JesseLog(@"count===%d",betCount);
    return betCount;
}
#pragma mark--temp
-(void)cucaluteLM:(int)n AndCaculteK:(int)k And:(NSString *)temp
{
    for (int i=n; i>=k; i--) {
        if (k>1) {
            [self cucaluteLM:(int)i-1  AndCaculteK:k-1 And:[NSString stringWithFormat:@"%@%@",temp,self.rescurviceArr[i-1]]];
        }else{
            NSString *zodic = [NSString stringWithFormat:@"%@%@",temp,self.rescurviceArr[i-1]];
            [self.recursiveResultArr addObject:zodic];
        }
    }
    
}

#pragma mark-- 重新计算当值
bool caculte(NSString *currenS,int Num){
   __block int total = 0;
    
    for (NSInteger i = 0; i<currenS.length; i++) {
       total+=[[currenS substringWithRange:NSMakeRange(i, 1)] intValue];
    }
    if (total==Num) {
        return YES;
    }

    return NO;
}
#pragma mark--生成随机值
NSString *makeRandmNumber(NSDictionary *tplConfig,NSString *openNumberLen){
    NSArray *randomRange;
    NSString *randomFormat = ((NSArray *)tplConfig[@"format"])[0];
    NSString *result;
    bool isRadomNumber = YES;
    if ([((NSArray *)tplConfig[@"sub"])[1][0] isKindOfClass:[NSNumber class]]) {
        randomRange = ((NSArray *)tplConfig[@"sub"])[1];
    }else{
        randomRange = @[[NSString stringWithFormat:@"%d",0],[NSString stringWithFormat:@"%ld",((NSArray *)(tplConfig[@"sub"])[1]).count-1]];
        isRadomNumber = NO;
    }
    NSRange rang = [randomFormat rangeOfString:@"m"];
    bool notRepeat = (rang.length>0)?YES:NO;
    bool isGroupInput = tplConfig[@"isGroupInput"]==nil?NO:YES;
    NSRegularExpression *ecPression  = [NSRegularExpression regularExpressionWithPattern:@"[A-Za-z]" options:NSRegularExpressionCaseInsensitive error:nil];
    NSArray *numberArr = [ecPression matchesInString:randomFormat options:NSMatchingReportProgress range:NSMakeRange(0, randomFormat.length)];
    NSMutableString *mus = [[NSMutableString alloc]init];
    [numberArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSTextCheckingResult *result = obj;
        NSRange ange = result.range;
        [mus appendString:[randomFormat substringWithRange:ange]];
    }];
    bool isDefineEveryRange = ((NSArray *)tplConfig[@"sub"]).count>2&&mus.length+1==((NSArray *)tplConfig[@"sub"]).count;
    result = replaceFormart(randomFormat, randomRange, isRadomNumber, isDefineEveryRange, [openNumberLen intValue], tplConfig,notRepeat,isGroupInput);
    
    return result;
}
#pragma mark--随机算法框架（1）
id replaceFormart(NSString *formart,NSArray *rangArr,bool isRandNumber,bool isDefineEveryRange,int openNumberLen,NSDictionary *tplConfig,bool notRepet ,bool isGroupInput){
    bool isSymbol = false;
    NSString *symbol;
    NSArray *comArr;
    if (formart.length>1) {
        isSymbol = YES;
       symbol = [formart substringWithRange:NSMakeRange(1, 1)];
        if (([formart rangeOfString:@"|"].length>0)&&([formart rangeOfString:@","].length>0)) {
           comArr = [formart componentsSeparatedByString:@"|"];
        }
    }
    if (comArr.count>0&&comArr!=nil) {
        NSMutableArray *comReplaceArr = [[NSMutableArray alloc]initWithArray:comArr];
        NSMutableArray *checkArr = [[NSMutableArray alloc]init];
        for (int i = 0; i<comReplaceArr.count; i++) {
            NSString *repLaceS = comReplaceArr[i];
            NSRegularExpression *ecPression  = [NSRegularExpression regularExpressionWithPattern:@"[A-Za-z]" options:NSRegularExpressionCaseInsensitive error:nil];
            NSArray *numberArr = [ecPression matchesInString:repLaceS options:NSMatchingReportProgress range:NSMakeRange(0, repLaceS.length)];
            NSMutableString *mus = [[NSMutableString alloc]init];
            [numberArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSTextCheckingResult *result = obj;
                NSRange ange = result.range;
                [mus appendString:[formart substringWithRange:ange]];
            }];
            
            NSArray *sys = rangeSpeace(tplConfig, rangArr, mus, isRandNumber, openNumberLen, isDefineEveryRange);
            [checkArr addObject:sys];
            NSMutableString *reP = [[NSMutableString alloc]init];
            [sys enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [reP appendFormat:@"%@,",obj];
            }];
            if (reP.length>1) {
                [reP deleteCharactersInRange:NSMakeRange(reP.length-1, 1)];
            }
            mus = reP;
            NSInteger noRepeatArrLen =arrayGetDisTinctValues(sys).count;
            if ((notRepet&&noRepeatArrLen!=numberArr.count)||(isGroupInput&&noRepeatArrLen==1)) {
                return replaceFormart(formart, rangArr, isRandNumber, isDefineEveryRange, openNumberLen, tplConfig, notRepet, isGroupInput);
            }
            [comReplaceArr replaceObjectAtIndex:i withObject:mus];
        }
        if (tplConfig[@"algorithm"]&&[tplConfig[@"algorithm"][0]isEqualToString:@"R3"]) {
            NSMutableArray *algorith = [[NSMutableArray alloc]initWithArray:tplConfig[@"algorithm"]];
            [algorith removeObjectAtIndex:0];
            if (algorith.count<4) {
                for (int k = 0; k<4-algorith.count; k++) {
                    [algorith addObject:@""];
                }
            }
            if (!checkR3Numebr(tplConfig, checkArr, algorith[0], algorith[1])) {
                return replaceFormart(formart, rangArr, isRandNumber, isDefineEveryRange, openNumberLen, tplConfig, notRepet, isGroupInput);
            }
        }
        return StringBybet(comReplaceArr, false);
        
    }else{
        
        NSRegularExpression *ecPression  = [NSRegularExpression regularExpressionWithPattern:@"[A-Za-z]" options:NSRegularExpressionCaseInsensitive error:nil];
        NSArray *numberArr = [ecPression matchesInString:formart options:NSMatchingReportProgress range:NSMakeRange(0, formart.length)];
        NSMutableString *mus = [[NSMutableString alloc]init];
        [numberArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSTextCheckingResult *result = obj;
            NSRange ange = result.range;
            [mus appendString:[formart substringWithRange:ange]];
        }];
        int musLenth =(int)mus.length;
        NSMutableArray *SyspleArr = [[NSMutableArray alloc]init];
        for (int i = 0; i<musLenth; i++) {
            int num = ranDom(rangArr);
            NSString *numberRand;
            if (isRandNumber) {
                if ([[NSString stringWithFormat:@"%@",tplConfig[@"algorithm"][0]] isEqualToString:@"R1"]&&[tplConfig[@"tpl"]isEqualToString:@"num6"]) {
                    numberRand = [NSString stringWithFormat:@"%d",num];
                }else{
                numberRand = (openNumberLen==1)||num>=10?[NSString stringWithFormat:@"%d",num]:[NSString stringWithFormat:@"0%d",num];
                }
            }else if (isDefineEveryRange){
                if (i==0) {
                   numberRand = ((NSArray *)tplConfig[@"sub"])[1][num];
                 }else{
                numberRand = ((NSArray *)tplConfig[@"sub"])[2][num];
                }
            }else{
                numberRand = ((NSArray *)tplConfig[@"sub"])[1][num];
            }
            if (!(openNumberLen==1||num>=10)) {
                if ([[NSString stringWithFormat:@"%@",tplConfig[@"algorithm"][0]] isEqualToString:@"R1"]&&[tplConfig[@"tpl"]isEqualToString:@"num6"]) {
                [mus replaceCharactersInRange:NSMakeRange(i, 1) withString:numberRand];
                }else{
                [mus replaceCharactersInRange:NSMakeRange((i*2)!=0?i*2:0,1) withString:numberRand];
                }
                
            }else{
                [mus replaceCharactersInRange:NSMakeRange(i, 1) withString:numberRand];
            }
            [SyspleArr addObject:numberRand];
        }
        if (isSymbol) {
            NSMutableString *reP = [[NSMutableString alloc]init];
            [SyspleArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [reP appendFormat:@"%@%@",obj,symbol];
            }];
            if (reP.length>1) {
                [reP deleteCharactersInRange:NSMakeRange(reP.length-1, 1)];
            }
            mus = reP;
       
        }
        NSInteger noRepeatArrLen =arrayGetDisTinctValues(SyspleArr).count;
        if ((notRepet&&noRepeatArrLen!=numberArr.count)||(isGroupInput&&noRepeatArrLen==1)) {
            return replaceFormart(formart, rangArr, isRandNumber, isDefineEveryRange, openNumberLen, tplConfig, notRepet, isGroupInput);
        }else{
        if (tplConfig[@"algorithm"]&&[[NSString stringWithFormat:@"%@",tplConfig[@"algorithm"][0]] isEqualToString:@"R5"]&&![tplConfig[@"tpl"]isEqualToString:@"renxuan1"]) {
                NSMutableArray *arr = [[NSMutableArray alloc]init];
                [arr addObject:SyspleArr];
                return @[[NSString stringWithFormat:@"%d",checkR5Number(tplConfig,arr)],mus];
                
            }
            return mus;
        }

    }
}
NSArray *rangeSpeace(NSDictionary *tplConfig,NSArray *rangArr, NSMutableString *mus,bool isRandNumber,int openNumberLen,bool isDefineEveryRange){
    NSMutableArray *SyspleArr = [[NSMutableArray alloc]init];
    for (int i = 0; i<mus.length; i++) {
        int num = ranDom(rangArr);
        NSString *numberRand;
        if (isRandNumber) {
            numberRand = (openNumberLen==1)||num>=10?[NSString stringWithFormat:@"%d",num]:[NSString stringWithFormat:@"0+%d",num];
        }else if (isDefineEveryRange){
            numberRand = ((NSArray *)tplConfig[@"sub"])[num];
        }else{
            numberRand = ((NSArray *)tplConfig[@"sub"])[1][num];
        }
        [mus replaceCharactersInRange:NSMakeRange(i, 1) withString:numberRand];
        [SyspleArr addObject:[mus substringWithRange:NSMakeRange(i, 1)]];
    }
    return SyspleArr;
}
bool checkR3Numebr(NSDictionary *tplConfig,id betContent,NSString *row1Num,NSString *row2Num){
    NSArray *countA = R3(tplConfig, betContent, row1Num, row2Num);
    if ([countA[0] integerValue]<1) {
        return NO;
    }else{
        return YES;
    }
}
int checkR5Number(NSDictionary *tplConfig,id betContent){
    NSArray *countA = R5(tplConfig, betContent);
    return [countA[0] intValue];
}
#pragma mark--随机算法框架二
Bet365OfficalResultModel *makeRadomBetContent(NSDictionary *tplConfig,Bet365OfficalResultModel *betContent,NSString *openNumberLen){
    Bet365OfficalResultModel *betRandContent = [[Bet365OfficalResultModel alloc]init];
    NSString *contentRand = makeRandmNumber(tplConfig, openNumberLen);
    NSString *betPos;
    NSString *betPosNames;
    NSArray *needRandomPosTpls = @[@"hezhi2",@"renxuan1",@"danshi2"];
    if ([[NSString stringWithFormat:@"%@",tplConfig[@"algorithm"][0]]isEqualToString:@"R7"]||[[NSString stringWithFormat:@"%@",tplConfig[@"algorithm"][0]]isEqualToString:@"R6"]||arrayIndexOf(needRandomPosTpls, tplConfig[@"tpl"])>-1) {
        //生成随机下注位置
        
        int rowLen;
        NSArray *posValues;
        NSArray *posNames;
        if (arrayIndexOf(needRandomPosTpls, tplConfig[@"tpl"])>-1) {
            rowLen = (tplConfig[@"posLen"])?[tplConfig[@"posLen"] intValue]:[tplConfig[@"algorithm"][0] intValue];
            posValues = @[@"0",@"1",@"2",@"3",@"4"];
            posNames = @[@"万",@"千",@"百",@"十",@"个"];
        }else{
        rowLen = (((NSArray *)tplConfig[@"algorithm"]).count>1)?[tplConfig[@"algorithm"][1] intValue]:1;
        posValues = makeNumAry(0, (int)((NSArray *)tplConfig[@"sub"][0]).count-1, false);
        posNames = tplConfig[@"sub"][0];
        }
        NSMutableArray *randomPosIndexAry = [[NSMutableArray alloc]init];
        for (int i = 0; i<rowLen;) {
            int posIndex = ranDom(@[@"0",[NSString stringWithFormat:@"%ld",posValues.count-1]]);
            if (posValues[posIndex]&&posNames[posIndex]&&arrayIndexOf(randomPosIndexAry,[NSString stringWithFormat:@"%d",posIndex])==-1) {
                [randomPosIndexAry addObject:[NSString stringWithFormat:@"%d",posIndex]];
                i++;
            }
        }
        [randomPosIndexAry sortedArrayUsingSelector:@selector(compare:)];
        NSMutableArray *betPosAry = [[NSMutableArray alloc]init];
        NSMutableArray *posNamesAry = [[NSMutableArray alloc]init];
        for (int i = 0; i<randomPosIndexAry.count; i++) {
            [betPosAry addObject:posValues[[randomPosIndexAry[i] integerValue]]];
            [posNamesAry addObject:posNames[[randomPosIndexAry[i] integerValue]]];
        }
        betPos = StringBybet(betPosAry, true);
        betPosNames = StringBybet(posNamesAry, true);
    }
    int count = (tplConfig[@"count"])?[tplConfig[@"count"] intValue]:1;
    float canWinMoney;
    float playShowOdds = [betContent.currenOdds[0] floatValue];
    canWinMoney = (playShowOdds*betContent.totalBettingMoney)*betContent.mulite;
    betRandContent.unitype=betContent.unitype;
    betRandContent.mulite = betContent.mulite;
    betRandContent.currenOdds = betContent.currenOdds;
    betRandContent.rebet = betContent.rebet;
    betRandContent.code = betContent.code;
    betRandContent.name = betContent.name;
    
    betRandContent.NumberValue = contentRand;
    betRandContent.currenBettingNumber = count;
    betRandContent.poschoose = betPos;
    betRandContent.poschooseName = betPosNames;
    betRandContent.showContent=betPosNames?[NSString stringWithFormat:@"%@(%@)",contentRand,betPosNames]:contentRand;
    float money = 2;
    if (betContent.unitype==1) {
        money = 0.2;
    }else if (betContent.unitype==2){
        money = 0.02;
    }
   
    betRandContent.totalBettingMoney = count*money*betContent.mulite;
    betRandContent.profitMoney = canWinMoney;
    return betRandContent;
}
#pragma mark--入口第一阶梯
static inline NSArray *contentArr (id betContent ,NSDictionary *tplConFig,NSString *openNumberLen){
    JesseLog(@"sada打卡打卡");
    BET_CONFIG.Special = NO;
    if (!betContent) {
        return @[@"0",@""];
    }
    id betNums;
    bool algorithm = indexxStaus(((NSArray *)tplConFig[@"algorithm"])[0]);
    NSArray *betinfo;
    if ([betContent isKindOfClass:[NSArray class]]) {
        betNums = betContent;
    }else if ([betContent isKindOfClass:[NSString class]]){
        betNums = betContent;
    }else{
        betNums = betContent[@"nums"];
    }
    if (algorithm) {
        NSArray *tplCon = tplConFig[@"algorithm"];
        NSMutableArray *CurrenTap = [[NSMutableArray alloc]initWithArray:tplCon];
        [CurrenTap removeObjectAtIndex:0];
        [CurrenTap insertObject:tplConFig atIndex:0];
        [CurrenTap insertObject:betNums atIndex:1];
        if (CurrenTap.count<6) {
            int between = 6-(int)CurrenTap.count;
            for (int i = 0; i<between; i++) {
                [CurrenTap addObject:@""];
            }
        }
        betinfo = allAlgorithm(((NSArray *)tplConFig[@"algorithm"])[0],CurrenTap);
    }else if ([((NSArray *)tplConFig[@"algorithm"])[0] isKindOfClass:[NSNumber class]]){
        if (unitaryIndexStaus(tplConFig[@"tpl"])) {
            NSArray *currenPara = tplConFig[@"algorithm"];
            NSMutableArray *MuPara = [[NSMutableArray alloc]initWithArray:currenPara];
            [MuPara insertObject:betNums atIndex:0];
            [MuPara insertObject:openNumberLen atIndex:1];
            [MuPara insertObject:tplConFig atIndex:2];
            if (MuPara.count<6) {
                int between = 6-(int)MuPara.count;
                for (int i = 0; i<between; i++) {
                    [MuPara addObject:@""];
                }
            }

            betinfo = allAlgorithm(tplConFig[@"tpl"], MuPara);
        }else{
            JesseLog(@"该玩法没对应算法+%@",tplConFig[@"tpl"]);
        }
    }else{
        JesseLog(@"该玩法并没对应算法+%@",((NSArray *)tplConFig[@"algorithm"])[0]);
    }
    if (![betNums isKindOfClass:[NSArray class]]&&![betNums isKindOfClass:[NSString class]]){
        //========================暂且留空==========================//
    }
    //JesseLog(@"%@",betinfo);
    return betinfo;
    
}
#pragma mark--入口传递
-(void)postAlgorithmResolveBy:(id)betContent And:(NSDictionary *)tplConFig AndopenNumberLen:(NSString *)lenth returnBetCountBy:(void (^)(NSInteger , NSString *))betCountBlock
{
    NSArray *contentObj = contentArr(betContent, tplConFig, lenth);
    betCountBlock([contentObj[0] integerValue],contentObj[1]);
    
}
#pragma mark--specail特殊玩法入口
-(void)postSpecailAlgorithmResolveBy:(id)betContent And:(NSDictionary *)tplConFig AndopenNumberLen:(NSString *)lenth returnBetCountBy:(void (^)(NSInteger, NSString *, NSArray *, NSArray *))betCountBlock
{
    NSArray *contentObj = contentArr(betContent, tplConFig, lenth);
    if ([contentObj[0] intValue]==0) {
        betCountBlock(0,@"",@[],@[]);
        return;
    }
    betCountBlock([contentObj[0] intValue],contentObj[1],contentObj[2],contentObj[3]);
}
#pragma mark--speacailR7玩法特殊算法入口
-(void)postSpeacialR7kindAlgorithmResovleBy:(id)betContent And:(NSDictionary *)tplConFig AndopenNumberLen:(NSString *)lenth returnBetCountBy:(void (^)(NSInteger,NSArray *, NSArray *, NSString *, NSArray *, NSArray *))betCountBlock
{
    NSArray *contentR7 = contentArr(betContent, tplConFig, lenth);
    if ([contentR7[0] integerValue]==0) {
        betCountBlock(0,@[],@[],@"",@[],@[]);
        return;
    }
    betCountBlock([contentR7[0]intValue],contentR7[1],contentR7[2],contentR7[3],contentR7[4],contentR7[5]);
}
#pragma mark--随机算法框架（1）入口传递
-(void)makeRandmNumberBy:(NSDictionary *)tplConfig And:(NSString *)openNumberLen ByContent:(void (^)(NSString *))contensBlock
{
  NSString *contentS = makeRandmNumber(tplConfig, openNumberLen);
    contensBlock(contentS);
}
#pragma mark--随机算法二入口
-(void)makeSpecalRandmNumberBy:(NSDictionary *)tplConfig And:(Bet365OfficalResultModel *)cotent And:(NSString *)openNumber returnContenModel:(void (^)(Bet365OfficalResultModel *))RandModel
{
    Bet365OfficalResultModel *randM = makeRadomBetContent(tplConfig, cotent, openNumber);
    RandModel(randM);
}
#pragma mark--递归1切入算整合
-(NSDictionary *)resucvierDicBy:(NSArray *)desArr And:(int)len
{
    NSArray *resucvierArr = CPrint(desArr, len);
    return @{@"reducvierArr":resucvierArr};
}
NSArray *indexCurrenArr()
{
    return @[@"R1",@"R2",@"R3",@"R4",@"R5",@"R6",@"R7",@"R8",@"R9",@"R10",@"R11"];
}
NSArray *UnitaryIndexArr()
{
    return @[@"danshi1",@"danshi2"];
}
-(NSMutableArray *)rescurviceArr{return _rescurviceArr?_rescurviceArr:(_rescurviceArr=@[].mutableCopy);};
-(NSMutableArray *)recursiveResultArr{return _recursiveResultArr?_recursiveResultArr:(_recursiveResultArr=@[].mutableCopy);};
@end
