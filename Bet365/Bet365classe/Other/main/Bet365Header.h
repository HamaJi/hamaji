//
//  Bet365Header.h
//  Bet365
//
//  Created by jesse on 2017/4/12.
//  Copyright © 2017年 jesse. All rights reserved.
//

#ifndef Bet365Header_h
#define Bet365Header_h
#import "Bet365ViewController.h"
#import "UIViewController+Router.h"
#import "UINavigationController+Bet365.h"
#import "UIViewControllerSerializing.h"
#import "AjiRequestTaskMetricsTool.h"
#import <AFNetworking.h>
#import <Masonry.h>
#import "UIView+frame.h"
#import "UIImage+original.h"
#import "jesseCacheActiveModel.h"
#import "AppDelegate.h"
#import "JESSEKFCPAlgorithmManager.h"
#import "JESSEKFCPAlgorithmRequestManager.h"
#import "JHCollectionViewFlowLayout.h"
#import "jesseLogBet365ViewController.h"
#import "BetTotal365ViewController.h"
#import "jesseSixOpenPrizeBet365Model.h"
#import "jesseBet365periodModel.h"
#import "lotteryCurModel.h"
#import "lotteryPreModel.h"
#import <JGProgressHUD.h>
#import "jesseGuideUIConfig.h"
#import "Bet365NavViewController.h"
#import "jesseAvdioToolManager.h"
#import "LukeUserAdapter.h"
#import "AllLiveData.h"


#import "lotteryCofigModel.h"
#import "UIViewController+jesseStatusBar.h"
#import "UIColor+extenColor.h"
#import "luziModel.h"
#import "customLoadingTool.h"
#import <MagicalRecord/MagicalRecord.h>
#import "BaiduClient.h"

#pragma mark - UI
#import <SDWebImage/SDWebImage.h>
#import "UIColorHeader.h"
#import <TYAttributedLabel/TYAttributedLabel.h>
#import <MJExtension.h>
#import <MJRefresh.h>
#import <UIImageView+WebCache.h>
#import <SVProgressHUD.h>
#import <SDCycleScrollView.h>
#import "Bet365traceTextView.h"
#import "KFCPHomeAlertView.h"
#import "UICollectionViewController+tulongtool.h"
#import <BRPickerView.h>
#import <IQKeyboardManager.h>
#import "NoticeHeader.h"
#import <YYKit.h>
#import "TurnTableView.h"
#import "MarqueeView.h"
#pragma mark - Tool
#import <ReactiveObjC/ReactiveObjC.h>
#import "Bet365Tool.h"
#import "toolTimer.h"
#import "NetWorkMannager.h"
#import "NetMetricsModel.h"
//#import <GT/GT.h>
#import "SinglePixelLineView.h"

#pragma mark - Manager
#import "NavigationManager.h"
#import "UserDataManager.h"

#pragma mark - Gagetory

#import "UIViewHeader.h"
#import "UILabel+Aji.h"
#import "UIImage+Aji.h"
#import "NSDecimalNumberHeader.h"
#import "NSStringHeader.h"
#import "NSDateHeader.h"
#import "UIButton+LXMImagePosition.h"
#import "UIViewController+alpha.h"
#import "NSDictionaryHeader.h"
#import "NSErrorHeader.h"
#import "UIAlertAction+Tag.h"
#import "UIImageHeader.h"
#import "UIViewControllerHeader.h"
#import "UIBarButtonItemHeader.h"
#import "UITabBarHeader.h"
#import "NSObjectHeader.h"
#import "UIScrollViewHeader.h"
#import "CALayer+LayerColor.h"
#import <MJPopupViewController/UIViewController+MJPopupViewController.h>
#import "NSArray+getNewArray.h"
#import "UIViewController+Bet365.h"
#import "UIButton+Aji.h"
#import "NSArrayHeader.h"

#import "NavigationControllerHeader.h"
#import "AppDelegate+Bet365.h"
#import "UICollectionViewFlowLayoutHeader.h"
#import "PresentingTopAnimator.h"
#import "DismissingTopAnimator.h"
#import "UICollectionView+Fix.h"
#import "UIViewController+queryContent.h"

#pragma mark - Manager
#import "TimerManager.h"
#import "NavigationManager.h"
#import "UserDataManager.h"
#import "NotificationManager.h"
#import "cloudToolManager.h"
#import "Bet365AlertSheet.h"
#import "Bet365Config.h"
#import "AFNetSubManager.h"
#import "AFNetCustomManager.h"
#import "ChatUtility.h"

#define CODEURL jesse(@"/v/vUserCode")

#pragma mark--工程的配置
#ifdef DEBUG
#define JesseLog(...) NSLog(__VA_ARGS__)
#else
#define JesseLog(...)
#endif

#ifdef DEBUG
#define NSLog(...) NSLog(@"%s 第%d行 \n %@\n\n",__func__,__LINE__,[NSString stringWithFormat:__VA_ARGS__])
#else
#define NSLog(...)
#endif
#define BIWeakObj(o)   @autoreleasepool {} __weak typeof(o) o ## Weak = o;


#define JesseColor(r,g,b)[UIColor colorWithRed:(r/255.0) green:(g/255.0) blue:(b/255.0) alpha:1]
#define JesseGrayColor(g) JesseColor(g,g,g)

#define RegisterButtonDisElbleBackColor JesseColor(229, 145, 144)

#define PK10oneColor JesseColor(217, 83, 79)
#define PK10twoColor JesseColor(92,183,92)
#define PK10ThreeColor JesseColor(42,116,221)
#define PCDDOneColor JesseColor(42,116,221)
#define PCDDTwoColor JesseColor(92,184,92)
#define PCDDThreeColor JesseColor(217,83,79)
#define PCDDFourColor JesseColor(170,170,170)

#define WIDTH [UIScreen mainScreen].bounds.size.width
#define HEIGHT [UIScreen mainScreen].bounds.size.height
#define BettingUrl jesse(@"/api/bet")
#define jesse(r)[NSString stringWithFormat:@"%@%@",SerVer_Url,r]
#define SixOpenTimeCloseUrl jesse(@"/v/lottery/openInfo")//截止时间开奖接口
#define TraceNumberUrl jesse(@"/api/bet/trace")//追号接口
#define OfficalBettingUrl jesse(@"/api/bet/betG")//官方玩法下注
#define YCZGrayColor(g) YCZColor(g,g,g)
#define YCZColor(r,g,b)[UIColor colorWithRed:(r/255.0) green:(g/255.0) blue:(b/255.0) alpha:1]
#define JesseAppdelegate ((AppDelegate *)[UIApplication sharedApplication].delegate)
#define IS_IPHONE_6P WIDTH == 414.0
#define IS_IPHONE_5s WIDTH == 320.0
#define IS_IPHONE_8Plus 414.0
#define collectionWidth (WIDTH - 40) / 4
#define statusHeight [UIApplication sharedApplication].statusBarFrame.size.height
#define navHeight 44
#define SYSTEMVERSION [[[UIDevice currentDevice] systemVersion] floatValue]

#define SafeAreaBottomHeight (HEIGHT == 812.0 ? 34 : 0)
//#define is_IphoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)

#define is_IphoneX ([UIApplication sharedApplication].statusBarFrame.size.height>=44)
#define NAVGATION_HEIGHT (44.0f)
#define STATEBAR_HEIGHT ([UIApplication sharedApplication].statusBarFrame.size.height)
#define TABBAR_HEIGHT            (is_IphoneX ? 83.0f : 49.0f)
#define TABBARBOTTOMDISTANCE    (is_IphoneX ? 34.0f : 0.0f)
#define STATUSAND_NAVGATION_HEIGHT (NAVGATION_HEIGHT + STATEBAR_HEIGHT)
#define ViewSafeAreInsets(view) ({UIEdgeInsets insets; if(@available(iOS 11.0, *)) {insets = view.safeAreaInsets;} else {insets = UIEdgeInsetsZero;} insets;})
#define kStringIsEmpty(str) ([str isKindOfClass:[NSNull class]] || str == nil || [str length] < 1 ? YES : NO )
#endif /* Bet365Header_h */
#define IOS_10_LATER ([[[UIDevice currentDevice] systemVersion] integerValue] >= 10.0)
