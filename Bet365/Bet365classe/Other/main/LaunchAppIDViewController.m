//
//  LaunchAppIDViewController.m
//  Bet365
//
//  Created by adnin on 2019/7/24.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "LaunchAppIDViewController.h"
#import "NetCloudSwitchTool.h"
#import "UIViewController+LQNoPresent.h"
#import "HomePageFactory.h"

@interface LaunchAppIDViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *appIconImage;

@property (weak, nonatomic) IBOutlet UITextField *textField;

@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UILabel *titileLabel;
@property (weak, nonatomic) IBOutlet UILabel *toastLabel;

@end

@implementation LaunchAppIDViewController

RouterKey *const kRouterSchemes = @"schemes";

+(void)load{
    [UIViewController registerJumpRouterKey:kRouterSchemes toHandle:^(NSDictionary *parameters) {
        [JesseAppdelegate setRootViewControllerWithAppIdLaunch:nil];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];

#ifdef DEBUG
    self.titileLabel.text = @"令牌码";
    self.textField.placeholder = @"请输入令牌码";
    self.toastLabel.text = @"Tips:葡京娱乐场3APJ\n        测试站bugfixweb\n令牌码不区分大小写，如有提示令牌码查询异常的报错，联系客户端新建云端";
#else
    self.titileLabel.text = @"信息配置";
    self.textField.placeholder = @"请输入官网地址";
    self.toastLabel.text = @"我们将会从【官网地址】中获取额外的配置信息，若您有所疑问也可与我们取得联系";
#endif
    
    @weakify(self);
    [self.textField.rac_textSignal subscribeNext:^(NSString * _Nullable x) {
        @strongify(self);
        [self.submitBtn setEnabled:x.length];
    }];
    
    
    
    self.textField.delegate = self;
    
    
    
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)submitAction:(id)sender {
#ifdef DEBUG
    self.appID = self.textField.text;
#else
    [self openURL:self.textField.text];
#endif
    
    
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    return YES;
}

#pragma mark - Private
-(void)openURL:(NSString *)urlHeader{
    if (!urlHeader.length) {
        [SVProgressHUD showErrorWithStatus:@"请提交官网地址"];
        return;
    }
    NSMutableString *url = [urlHeader mutableCopy];
    [url deleteString:@" "];
    if ([urlHeader hasSuffix:@"/"]) {
        [url deleteCharactersInRange:NSMakeRange(urlHeader.length - 1, 1)];
    }
    if (![urlHeader hasSuffix:@"/startapp.html"]) {
        [url appendString:@"/startapp.html"];
    }
    if (![urlHeader hasPrefix:@"http"]) {
        [url insertString:@"https://" atIndex:0];
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

-(void)updateAppID{
    NSString *appID = [self.appID uppercaseString];
    if (BET_CONFIG.userSettingData.defaultAppID.length &&
        [BET_CONFIG.userSettingData.defaultAppID isEqualToString:appID]) {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"重复操作\n当前已为%@",appID]];

        return;
    }
    self.submitBtn.enabled = NO;
    void(^fixBlock)(NSMutableArray<CloudModel *> *models) = ^(NSMutableArray<CloudModel *> *models){
        
        [SVProgressHUD dismiss];
        
        CloudModel *model = [models safeObjectAtIndex:0];
    
        
        if (model) {
            /*
             * 修改iCon
             */
            NSString *iconName = [NSString stringWithFormat:@"%@_ICON",appID];
            UIImage *appIcon = [UIImage imageNamed:iconName];
            if (appIcon && [[UIApplication sharedApplication] supportsAlternateIcons]) {
                [[UIApplication sharedApplication] setAlternateIconName:iconName completionHandler:^(NSError * _Nullable error) {
                    if (error) {
                        NSLog(@"更换app图标发生错误了 ： %@",error);
                    }
                }];
            }
            
            /*
             * 修改域名
             */
            @weakify(self);
            [NET_CLOUD_SWITCH switchCloudUrlBy:models Completed:^(NSString *urlStr) {
                @strongify(self);
                self.submitBtn.enabled = NO;
                [SVProgressHUD dismiss];
                if (urlStr.length) {
                    [PPNetworkCache removeAllHttpCache];
                    [AppDefaults removeAllDefaults];
                    
                    [NET_DATA_MANAGER setServerUrl:urlStr];
                    [Bet365Tool getNetworkIPAddressCompleted:nil];
                    
                    
                    [BET_CONFIG getConfigCompleted:nil];
                    [HOME_FACTORY getAllRequest];
                    [LOTTERY_FACTORY getAllRequest];
                    
                    self.updateAppId ? self.updateAppId() : nil;
                }else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if ([NET_DATA_MANAGER currentNetworkStatus]) {
                            [SVProgressHUD showErrorWithStatus:@"节点加载失败,请联系管理员"];
                        }else{
                            [SVProgressHUD showErrorWithStatus:@"请检查您的网络环境"];
                        }
                    });
                }
                BET_CONFIG.userSettingData.defaultAppID = appID;
                [BET_CONFIG.userSettingData save];
            }];
        }else{
            [SVProgressHUD showErrorWithStatus:@"信息查询有误，请核实您的令牌码"];
        }
    };
    [SVProgressHUD showWithStatus:[NSString stringWithFormat:@"%@配置信息中...",appID]];
    [NET_CLOUD_SWITCH findCloudBy:appID Completed:^(NSMutableArray<CloudModel *> *models) {
        fixBlock(models);
    }];
}


-(void)setAppID:(NSString *)appID{
    _appID = appID;
    if ([appID length]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self updateAppID];
        });
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
