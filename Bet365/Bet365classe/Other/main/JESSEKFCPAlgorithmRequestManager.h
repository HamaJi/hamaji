//
//  JESSEKFCPAlgorithmRequestManager.h
//  Bet365
//
//  Created by jesse on 2017/6/2.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JESSEKFCPAlgorithmRequestManager : NSObject
//算法管理网络请求初始化
+(instancetype)initShareManagerRequest;
-(void)requestAlgorithmMessageBy:(NSString *)paramater And:(void(^)(bool isOk))trueBlock AndTaskArr:(NSMutableArray *)taskArr;
//-(void)checkCurrenLogStaus;//检测状态
-(void)cancleCurrenTask;/**清除当前正在进行的路子任务**/
//-(void)realTimeDetection:(void(^)(BOOL logState))LogingStateBlock;//实时检测状态
-(void)requestLongDragonData:(void(^)(BOOL requestState))LongDragonBlock AndGameID:(NSString *)gameID;//请求当前长龙数据
-(void)loadTestConfig:(void(^)(NSInteger num))configBlock;
-(void)requestLotteryWay:(void(^)(BOOL requestState))LotteryWay AndGameId:(NSString *)gameId;/**获取路子数据**/
-(void)requestCurrenSetConfig:(void(^)())successful andIsCache:(BOOL)cacheConfig;/**请求当前配置资料**/
@end
