//
//  JESSEKFCPAlgorithmManager.h
//  Bet365
//
//  Created by jesse on 2017/6/1.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Bet365OfficalResultModel.h"
#define ALGORIGH_MANAGER [JESSEKFCPAlgorithmManager shareCurrenManager]
@interface JESSEKFCPAlgorithmManager : NSObject
@property(nonatomic,assign)NSInteger R6BetposCount;
#pragma mark--入口创建
+(instancetype)shareCurrenManager;
//入口传递(普通)
-(void)postAlgorithmResolveBy:(id)betContent And:(NSDictionary *)tplConFig AndopenNumberLen:(NSString *)lenth returnBetCountBy:(void(^)(NSInteger betCount,NSString *content))betCountBlock;
//入口传递(针对特殊玩法例:<定位胆及重庆时时彩>)
-(void)postSpecailAlgorithmResolveBy:(id)betContent And:(NSDictionary *)tplConFig AndopenNumberLen:(NSString *)lenth returnBetCountBy:(void(^)(NSInteger betCount,NSString *content,NSArray *betPos,NSArray *betPosNames))betCountBlock;
//特殊算法(针对R7玩法)
-(void)postSpeacialR7kindAlgorithmResovleBy:(id)betContent And:(NSDictionary *)tplConFig AndopenNumberLen:(NSString *)lenth returnBetCountBy:(void(^)(NSInteger betCount,NSArray *betNumberArr,NSArray *contentA,NSString *content,NSArray *betPos,NSArray *betPosNames))betCountBlock;
//生成随机
-(void)makeRandmNumberBy:(NSDictionary *)tplConfig And:(NSString *)openNumberLen ByContent:(void(^)(NSString *contenS))contensBlock;
//随机算法入口二
-(void)makeSpecalRandmNumberBy:(NSDictionary *)tplConfig And:(Bet365OfficalResultModel *)cotent And:(NSString *)openNumber returnContenModel:(void(^)(Bet365OfficalResultModel *randModel))RandModel;
//递归算切入
-(NSDictionary *)resucvierDicBy:(NSArray *)desArr And:(int)len;
@end
