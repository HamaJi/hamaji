//
//  JESSEKFCPAlgorithmRequestManager.m
//  Bet365
//  Created by jesse on 2017/6/2.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "JESSEKFCPAlgorithmRequestManager.h"
#import "AppDelegate.h"
#import "jesseAlertView.h"
#import "AppDelegate.h"
#import "KFCPDragonModel.h"
#import "lotteryCofigModel.h"
#import <PINCache.h>
#import "luziModel.h"
#import "LotteryFactory.h"
@interface JESSEKFCPAlgorithmRequestManager ()
@property(nonatomic,strong)AFHTTPSessionManager *manager;
@property(strong,nonatomic)NSMutableArray *configSetArr;/**位置配置存储**/
@property(strong,nonatomic)NSMutableArray *LotteryWayURLTask;/**当前彩票路子请求任务**/
@end
JESSEKFCPAlgorithmRequestManager *manager = nil;
@implementation JESSEKFCPAlgorithmRequestManager
/**懒加载处理**/
-(AFHTTPSessionManager *)manager{
    if (_manager==nil) {
        _manager = [AFHTTPSessionManager manager];
        _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        _manager.responseSerializer.acceptableContentTypes = [self returnResponType];
    }
    return _manager;
}
-(NSMutableArray *)LotteryWayURLTask{
    return _LotteryWayURLTask?_LotteryWayURLTask:(_LotteryWayURLTask=@[].mutableCopy);
};
-(NSMutableArray *)configSetArr{
    return _configSetArr?_configSetArr:(_configSetArr=@[].mutableCopy);
    
};
+(instancetype)initShareManagerRequest
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[JESSEKFCPAlgorithmRequestManager alloc]init];
    });
    return manager;
}

/**
 获取官方玩法配置信息

 @param paramater 官方玩法游戏id
 @param trueBlock <#trueBlock description#>
 @param taskArr <#taskArr description#>
 */
-(void)requestAlgorithmMessageBy:(NSString *)paramater And:(void (^)(bool))trueBlock AndTaskArr:(NSMutableArray *)taskArr
{
    LOTTERY_FACTORY.tplConfig = nil;
    [[PINCache sharedCache] objectForKey:[NSString stringWithFormat:@"config%@",paramater] block:^(PINCache * _Nonnull cache, NSString * _Nonnull key, id  _Nullable object) {
        if (object==nil) {
            NSString *requestUrl = [NSString stringWithFormat:jesse(@"/data/json/official/config_%@.json"),paramater];
            [[self.manager operationQueue] cancelAllOperations];
            NSURLSessionTask *task = [self.manager GET:requestUrl parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                NSData *data = (NSData *)responseObject;
                NSDictionary *resDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                [[PINCache sharedCache] removeObjectForKey:[NSString stringWithFormat:@"config%@",paramater]];
                [[PINCache sharedCache] setObject:resDic forKey:[NSString stringWithFormat:@"config%@",paramater]];
                LOTTERY_FACTORY.tplConfig = resDic;
                trueBlock(true);
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                trueBlock(false);
            }];
            [taskArr addObject:task];
        }else{
            NSDictionary *resdic = [[PINCache sharedCache] objectForKey:[NSString stringWithFormat:@"config%@",paramater]];
            LOTTERY_FACTORY.tplConfig = resdic;
            trueBlock(true);
        }
    }];
}





#pragma mark--清除当前进行的长龙路子任务
-(void)cancleCurrenTask{
    [self.LotteryWayURLTask enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSURLSessionTask *task = obj;
        [task cancel];
    }];
    [self.LotteryWayURLTask removeAllObjects];
}
#pragma mark--请求长龙数据
-(void)requestLongDragonData:(void (^)(BOOL))LongDragonBlock AndGameID:(NSString *)gameID
{
    LOTTERY_FACTORY.longDragon = nil;
    AFNetCustomManager *manager = [AFNetCustomManager sharedClient];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [self returnResponType];
    
    NSURLSessionTask *task = [manager GET:jesse(@"/v/lottery/getLmcl") parameters:@{@"gameId":gameID} progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:
                              ^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                                  NSData *data = (id)responseObject;
                                  NSArray *dataArr = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                  NSMutableArray *LongDra = [[NSMutableArray alloc]init];
                                  [dataArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                      NSDictionary *dic = obj;
                                      KFCPDragonModel *dragonModel = [[KFCPDragonModel alloc]initWithDictionary:dic error:nil];
                                      [LongDra addObject:dragonModel];
                                  }];
                                  LOTTERY_FACTORY.longDragon = LongDra;
                                  LongDragonBlock(YES);
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  LongDragonBlock(NO);
                              }];
    [self.LotteryWayURLTask addObject:task];
}
/**请求路子数据**/
-(void)requestLotteryWay:(void (^)(BOOL))LotteryWay AndGameId:(NSString *)gameId{
    
    LOTTERY_FACTORY.lotteryWay = nil;
    AFNetCustomManager *manager = [AFNetCustomManager sharedClient];
    manager.responseSerializer= [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    NSURLSessionTask *task = [manager GET:jesse(@"/v/lottery/luzhi") parameters:@{@"gameId":gameId} progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSData *data = (NSData *)responseObject;
        NSArray *jsonDicArr = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        LOTTERY_FACTORY.lotteryWay = repalce6FromArry(jsonDicArr);
        LotteryWay(YES);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        LotteryWay(NO);
    }];
    [self.LotteryWayURLTask addObject:task];
}

/**请求当前位置配置**/
//-(void)requestCurrenSetConfig:(void (^)())successful andIsCache:(BOOL)cacheConfig{
//    AFNetSubManager *manager = [AFNetSubManager manager];
//    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    manager.responseSerializer.acceptableContentTypes = [self returnResponType];
//    void(^detailConfig)(NSArray *resData) = ^(NSArray *resData){
//        NSEnumerator *cofigEnum = [resData reverseObjectEnumerator];
//        for (NSDictionary *cofigDic in cofigEnum) {
//            lotteryCofigModel *model = [[lotteryCofigModel alloc]initWithDictionary:cofigDic error:nil];
//            [self.configSetArr addObject:model];
//        }
//        JesseAppdelegate.setConfig = self.configSetArr;
//        successful();
//    };
//    if (cacheConfig) {
//        [[PINCache sharedCache] objectForKey:@"playseting" block:^(PINCache * _Nonnull cache, NSString * _Nonnull key, id  _Nullable object) {
//            if (object==nil) {
//                [manager GET:jesse(@"/data/json/game_play_setting.json") parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
//                    
//                } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//                    [self.configSetArr removeAllObjects];
//                    NSData *data = (NSData *)responseObject;
//                    NSArray *responeDataArr = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
//                    [[PINCache sharedCache] removeObjectForKey:@"playseting"];
//                    [[PINCache sharedCache] setObject:responeDataArr forKey:@"playseting"];
//                    detailConfig(responeDataArr);
//                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//                    successful();
//                }];
//            }else{
//                NSArray *resData = object;
//                detailConfig(resData);
//            }
//        }];
//    }else{
//        [manager GET:jesse(@"/data/json/game_play_setting.json") parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
//            
//        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//            [self.configSetArr removeAllObjects];
//            NSData *data = (NSData *)responseObject;
//            NSArray *responeDataArr = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
//            [[PINCache sharedCache] removeObjectForKey:@"playseting"];
//            [[PINCache sharedCache] setObject:responeDataArr forKey:@"playseting"];
//            detailConfig(responeDataArr);
//        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//            successful();
//        }];
//        
//    }
//}
/**加载试玩配置**/
-(void)loadTestConfig:(void (^)(NSInteger))configBlock{
//    [JesseAppdelegate showHudLoadby:@"加载中..."];
    [SVProgressHUD showWithStatus:@"加载中..."];
    AFNetSubManager *net = [AFNetSubManager sharedClient];
    net.responseSerializer = [AFHTTPResponseSerializer serializer];
    net.requestSerializer = [AFHTTPRequestSerializer serializer];
    net.responseSerializer.acceptableContentTypes = [jesseGuideUIConfig returnResponType];
    [net GET:jesse(@"/data/json/limit/registerLimit.json") parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        [JesseAppdelegate showLoadDismiss];
        [SVProgressHUD dismiss];
        NSData *data = (NSData *)responseObject;
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        if ([dic[@"trailUserValidCode"] isKindOfClass:[NSNull class]]||!dic[@"trailUserValidCode"]) {
            configBlock(0);
        }else{
            configBlock([dic[@"trailUserValidCode"] integerValue]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [SVProgressHUD dismiss];
        [SVProgressHUD showErrorWithStatus:@"网络错误"];
//        [JesseAppdelegate showLoadDismiss];
//        [JesseAppdelegate showErrorOnlyText:@"网络错误" AndDelay:1.5f];
        configBlock(1);
    }];
}
-(NSSet *)returnResponType
{
    return [NSSet setWithObjects:@"text/json",@"text/html",@"text/plain",@"text/javascript",@"application/json", nil];
}
NSMutableArray <luziModel *>*repalce6FromArry(NSArray *jsonArr){
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    [jsonArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *objDic = obj;
        NSError *error;
        luziModel *model = [MTLJSONAdapter modelOfClass:[luziModel class] fromJSONDictionary:objDic error:&error];
        if (model) {
            NSMutableArray *list = [NSMutableArray array];
            NSMutableArray *surplus = [[NSMutableArray alloc] initWithArray:@[@[],
                                                                              @[],
                                                                              @[],
                                                                              @[],
                                                                              @[],
                                                                              @[]]];
            [model.luzhi enumerateObjectsUsingBlock:^(NSMutableArray * _Nonnull subList, NSUInteger idx, BOOL * _Nonnull stop) {
                NSUInteger currentIdx = getSubArrCurrentIndex(surplus);
                if (subList.count > currentIdx + 1) {
                    NSRange range = NSMakeRange(currentIdx + 1, subList.count - currentIdx - 1);
                    [surplus replaceObjectAtIndex:currentIdx withObject:[subList subarrayWithRange:range]];
                    [subList removeObjectsInRange:range];
                }else{
                    NSUInteger max = subList.count;
                    for (NSUInteger i = max; i<6; i++) {
                        if ([surplus[i] count] > 0) {
                            [subList addObject:[surplus[i] firstObject]];
                            NSArray *rArr = [surplus[i] subarrayWithRange:NSMakeRange(1, [surplus[i] count] - 1)];
                            [surplus replaceObjectAtIndex:i withObject:rArr];
                        }else{
                            [subList addObject:@""];
                        }
                    }
                }
                [list addObject:subList];
            }];
            if (hasSubs(surplus)) {
                NSMutableArray *otherList = [NSMutableArray array];
                for (int i = 0; i<6; i++) {
                    if ([surplus[i] count]) {
                        [otherList addObject:[surplus[i] firstObject]];
                        NSArray *rArr = [surplus[i] subarrayWithRange:NSMakeRange(1, [surplus[i] count] - 1)];
                        [surplus replaceObjectAtIndex:i withObject:rArr];
                    }else{
                        [otherList addObject:@""];
                    }
                }
                [list addObject:otherList];
            }
            model.luzhi = list;
        }
        model ? [arr addObject:model] : nil;
    }];
    return arr;
}

@end

