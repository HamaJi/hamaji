//
//  Bet365ViewController.h
//  Bet365
//
//  Created by super_小鸡 on 2019/10/7.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NaviHeaderView.h"
#import "LukeTopWindow.h"

NS_ASSUME_NONNULL_BEGIN

//typedef NS_OPTIONS(NSUInteger, Bet365NaviItemsStatus) {
//    Bet365NaviItemsStatus_ALL  = 0,
//    Bet365NaviItemsStatus_BALANCE,
//    Bet365NaviItemsStatus_MENUE,
//    Bet365NaviItemsStatus_CHAT,
//    Bet365NaviItemsStatus_NONE
//};

typedef NS_OPTIONS(NSUInteger, Bet365NaviRightItem) {
    Bet365NaviRightItemNone     = 0,
    Bet365NaviRightItemBalance  = 1 << 0,
    Bet365NaviRightItemChat     = 1 << 1,
    Bet365NaviRightItemMenu     = 1 << 2,
    Bet365NaviRightItemSetting  = 1 << 3,
    Bet365NaviRightItemAll      = Bet365NaviRightItemBalance | Bet365NaviRightItemChat | Bet365NaviRightItemMenu
};

typedef NS_OPTIONS(NSUInteger, Bet365NaviLogoItemStatus) {
    Bet365NaviLogoItemStatus_HIDDEN = 0,
    Bet365NaviLogoItemStatus_DEFAULTS = 1,
    
};

@interface Bet365ViewController : UIViewController

@property (nonatomic)NaviHeaderView *naviHeaderView;

@property (nonatomic)LukeTopWindow *topWindow;

@property (nonatomic)Bet365NaviRightItem naviRightItem;

@property (nonatomic)Bet365NaviLogoItemStatus naviLoginStatus;


/**
 添加监听
 */
-(void)bet365_addObserver;




@end

NS_ASSUME_NONNULL_END
