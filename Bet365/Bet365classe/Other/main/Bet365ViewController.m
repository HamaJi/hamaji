//
//  Bet365ViewController.m
//  Bet365
//
//  Created by super_小鸡 on 2019/10/7.
//  Copyright © 2019 jesse. All rights reserved.
//


#import "Bet365ViewController.h"
#import "UIViewController+Bet365.h"

@interface Bet365ViewController ()<LukeTopWindowDelegate>

@end

@implementation Bet365ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.naviRightItem = Bet365NaviRightItemAll;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self bet365_addObserver];
    [self addLeftPanGuide];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self updateNaviBalanceItems];
}

-(void)bet365_addObserver{
    @weakify(self);
    [[RACSignal combineLatest:@[[RACObserve(self,naviLoginStatus) distinctUntilChanged],
                                [RACObserve(BET_CONFIG.common_config, appLogo) distinctUntilChanged]] reduce:^id _Nonnull(NSNumber *status,NSString *appLogo){
                                    if ([status integerValue] == Bet365NaviLogoItemStatus_DEFAULTS &&appLogo.length) {
                                        return StringFormatWithStr(SerVer_Url, [appLogo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
                                    }
                                    return nil;
                                }] subscribeNext:^(id  _Nullable x) {
                                    @strongify(self);
                                    [self updateNaviImageItemsWithImgUrl:x];
                                }];
    
    RACSignal *moneySignal = [RACObserve(USER_DATA_MANAGER.userInfoData, money) distinctUntilChanged];
    
    RACSignal *accountSignal = [RACObserve(USER_DATA_MANAGER.userInfoData, account) distinctUntilChanged];
    
    RACSignal *chatSiganl = [RACObserve(BET_CONFIG.common_config,isShowChat) distinctUntilChanged];
    
    RACSignal *loginSignal = [RACObserve(USER_DATA_MANAGER,isLogin) distinctUntilChanged];
    
    [[RACSignal combineLatest:@[chatSiganl,loginSignal]] subscribeNext:^(RACTuple * _Nullable x) {
        @strongify(self);
        self.naviRightItem = self.naviRightItem;
    }];
    
    [[RACSignal combineLatest:@[moneySignal,accountSignal]] subscribeNext:^(RACTuple * _Nullable x) {
        @strongify(self);
        self.naviHeaderView.money = USER_DATA_MANAGER.userInfoData.money;
        self.naviHeaderView.account = USER_DATA_MANAGER.userInfoData.account;
    }];
    
    
    if (self.navigationController) {
        RACSignal *vcListSignal = [self.navigationController.viewControllers.rac_sequence.signal map:^id _Nullable(__kindof UIViewController * _Nullable value) {
            UIViewController *viewController = value;
            __block BOOL isKind = NO;
            [BET_CONFIG.allocation.templateList_tabbar enumerateObjectsUsingBlock:^(TabBarTemplate * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                Class class = NSClassFromString(obj.className);
                if ([viewController isKindOfClass:class]) {
                    isKind = YES;
                    *stop = YES;
                }
            }];
            return @(isKind);
        }];
        
        RACSignal *chatInfoSignal = [RACObserve(CHAT_UTIL, clientInfo) distinctUntilChanged];
        [[RACSignal combineLatest:@[chatSiganl,vcListSignal,chatInfoSignal] reduce:^id _Nonnull(NSNumber *isShowChat,NSNumber *isTabFirst,ChatInfoModel *chatInfo){
            return @([isShowChat boolValue] && [isTabFirst boolValue] && chatInfo!=nil);
        }] subscribeNext:^(id  _Nullable x) {
            BOOL isShowGuide = [x boolValue];
            @strongify(self);
            if (isShowGuide) {
                [self performSelector:@selector(showGuidChatEnter) withObject:nil afterDelay:1];
            }
        }];
    }
}

-(void)addLeftPanGuide{
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
}

-(void)updateNaviBalanceItems{
    self.naviRightItem = self.naviRightItem;
}

-(void)updateNaviImageItemsWithImgUrl:(NSString *)imgUrl{
    if (imgUrl) {
        UIImage *logoImage = [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:imgUrl];
        if (!logoImage) {
            
            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:imgUrl] options:SDWebImageDownloaderContinueInBackground progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
                
            } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                if (finished&&!error) {
                    [self setupNavgationLogoWithImage:image];
                }
            }];
        }else{
            [self setupNavgationLogoWithImage:logoImage];
        }
    }else{
        self.navigationItem.titleView = nil;
    }
}

#pragma mark - Private
- (void)setupNavgationLogoWithImage:(UIImage *)image{
    UIImageView *titleView =[[UIImageView alloc]initWithImage:image];
    titleView.contentMode = UIViewContentModeScaleAspectFit;
    CGFloat viewWidth = WIDTH / 3;
    CGFloat viewHeight = image.size.height / image.size.width * viewWidth;
    if (viewHeight > 40) {
        viewHeight = 40;
        viewWidth = image.size.width / image.size.height * viewHeight;
    }
    UIView *imageView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, viewWidth, viewHeight)];
    [imageView addSubview:titleView];
    titleView.frame = imageView.bounds;
    self.navigationItem.titleView = imageView;
}

#pragma mark - Events
-(void)showRightMenuAtIdx:(Bet365NaviRightItem)idx{
    if (idx & Bet365NaviRightItemBalance) {
        
    }else if (idx & Bet365NaviRightItemChat) {
        [UIViewController routerJumpToUrl:kRouterChatIndex];
    }else if (idx & Bet365NaviRightItemMenu) {
        self.topWindow.delegate = self;
        [UIView animateWithDuration:0.42 animations:^{
            self.topWindow.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        }];
    }else if (idx & Bet365NaviRightItemSetting) {
        
    }
}



#pragma mark - LukeTopWindowDelegate
- (void)lukeTopWindowResignKeyWindow{
    [self.topWindow resignKeyWindow];
    self.topWindow = nil;
}

- (void)lukeTopWindowTapActionBy:(MenuTemplate*)menuTemplate{
    [self lukeTopWindowResignKeyWindow];
//    [UIViewController routerJumpToUrl:kRouterTurnTable];
    if (menuTemplate.type == TemplateJumpType_ROUTER_ONLYKEY) {
        [UIViewController routerJumpToUrl:menuTemplate.appLink];
    }else if (menuTemplate.type == TemplateJumpType_ROUTER_PARAMERS){
        [UIViewController routerJumpToCode:menuTemplate.menuAttr.liveCode Type:menuTemplate.menuAttr.gameType Kind:menuTemplate.menuAttr.gameKind];
    }
}


#pragma mark - UserInfoUpdate
-(void)info_updateLoginStatus{
    
}
-(void)info_updateInfoData{
    
}

#pragma mark - GET/SET
-(void)setNaviRightItem:(Bet365NaviRightItem)naviRightItem{
    _naviRightItem = naviRightItem;
    NSMutableArray <UIBarButtonItem *>*items = @[].mutableCopy;
    [self.navigationItem.rightBarButtonItems enumerateObjectsUsingBlock:^(UIBarButtonItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.tag < 10000) {
            [items addObject:obj];
        }
    }];
    
    if (naviRightItem & Bet365NaviRightItemMenu) {
        @weakify(self);
        UIBarButtonItem *item = [UIBarButtonItem createItemsByImageStr:@"set" Titile:@"菜单" Position:LXMImagePositionLeft TapHandle:^(UIGestureRecognizer *gestureRecoginzer) {
            @strongify(self);
            [self showRightMenuAtIdx:Bet365NaviRightItemMenu];
        }];
        item.tag = 10000 + Bet365NaviRightItemMenu;
        [items addObject:item];
    }
    if (naviRightItem & Bet365NaviRightItemSetting) {
        
    }
    if (naviRightItem & Bet365NaviRightItemChat) {
        if (BET_CONFIG.common_config.isShowChat) {
            @weakify(self);
            UIBarButtonItem *item = [UIBarButtonItem createItemsByImageStr:@"chat_enterchat_icon" Titile:@"" Position:LXMImagePositionLeft TapHandle:^(UIGestureRecognizer *gestureRecoginzer) {
                @strongify(self);
                [self showRightMenuAtIdx:Bet365NaviRightItemChat];
            }];
            item.tag = 10000 + Bet365NaviRightItemChat;
            [items addObject:item];
        }
    }
    if (naviRightItem & Bet365NaviRightItemBalance) {
        if (USER_DATA_MANAGER.isLogin) {
            self.naviHeaderView.money = USER_DATA_MANAGER.userInfoData.money;
            self.naviHeaderView.account = USER_DATA_MANAGER.userInfoData.account;
            UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:self.naviHeaderView];
            item.tag = 10000 + Bet365NaviRightItemBalance;
            [items addObject:item];
        }
    }
    
    self.navigationItem.rightBarButtonItems = items;
}
-(NaviHeaderView *)naviHeaderView{
    if (!_naviHeaderView) {
        _naviHeaderView = [[NaviHeaderView alloc] initWithMoney:USER_DATA_MANAGER.userInfoData.money Account:USER_DATA_MANAGER.userInfoData.account];
    }
    return _naviHeaderView;
}

-(LukeTopWindow *)topWindow{
    if (!_topWindow) {
        _topWindow = [[LukeTopWindow alloc] init];
    }
    return _topWindow;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
