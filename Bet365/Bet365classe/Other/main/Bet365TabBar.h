//
//  Bet365TabBar.h
//  Bet365
//
//  Created by adnin on 2019/3/3.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Bet365TabBar;

@protocol Bet365TabBarDelegate<NSObject>

-(BOOL)bet365TabBar:(Bet365TabBar *)tabBar WillSelectIndex:(NSUInteger)idx;

-(void)bet365TabBar:(Bet365TabBar *)tabBar DidSelectIndex:(NSUInteger)idx;

@optional

@end

@interface Bet365TabBar : UITabBar

@property (nonatomic)NSArray <TabBarTemplate *>*templateList;

@property (nonatomic,strong)UIColor *tabBgColor;

@property (nonatomic,strong)UIColor *tabNorColor;

@property (nonatomic,strong)UIColor *tabSelColor;

@property (nonatomic,assign)NSUInteger selectIdx;

@property (nonatomic,weak)id<Bet365TabBarDelegate> tabDelegate;

@end
