//
//  TimerComManager.h
//  UCSDK
//
//  Created by Deep river on 2017/6/15.
//  Copyright © 2017年 com.uc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TimerKey.h"
#define TIMER_MANAGER [TimerManager sharedInstance]

@protocol TimerManagerDelegate <NSObject>
@optional
-(void)timerWithKey:(NSString *)key andMaxCount:(NSInteger)maxCount andRestCount:(NSInteger)count;
-(void)cycleTimerWithKey:(NSString *)key RestCount:(NSInteger)count;
@end
@interface TimerManager : NSObject


+(TimerManager *)sharedInstance;
+(void)destroyInstance;

/**
 分段执行

 @param count 最大次数
 @param key 唯一key
 @param reduceScope 间隔
 */
-(void)addTimerWithMaxCount:(NSInteger)count
                     andKey:(NSString *)key
              andReduceScope:(NSInteger)reduceScope;


/**
 无限

 @param key 唯一key
 @param reduceScope 间隔
 */
-(void)addCycleTimerWithKey:(NSString *)key andReduceScope:(NSInteger)reduceScope;
-(void)removeTimerWithKey:(NSString *)key;

-(void)addDelegate:(id)delegate;//调用此方法时要记得调用removeDelegate清除
-(void)removeDelegate:(id)delegate;

-(NSInteger)currentCountWithKey:(NSString *)key;
@end
