//
//  TimerComManager.m
//  UCSDK
//
//  Created by Deep river on 2017/6/15.
//  Copyright © 2017年 com.uc. All rights reserved.
//

#import "TimerManager.h"
#define TimerSpacing 1
static TimerManager *instance = nil;

@interface TimerManager (){
    ;
}
@property(nonatomic,strong)NSMutableArray <NSMutableDictionary *>*dataArray;
@property(nonatomic,strong)NSMutableArray *delegateArray;
@end

@implementation TimerManager
+(instancetype)sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[super alloc] init];
    });
    return instance;
}

+(void)destroyInstance{
    if(instance){
        instance = nil;
    }
}

-(id)init{
    if (self = [super init]) {
        self.dataArray = [[NSMutableArray alloc] init];
        self.delegateArray = [[NSMutableArray alloc] init];
        [self addObser];
    }
    return self;
}


-(void)addObser{
    @weakify(self);
    [[[RACSignal interval:TimerSpacing onScheduler:[RACScheduler mainThreadScheduler]] takeUntil:self.rac_willDeallocSignal ] subscribeNext:^(id x) {
        @strongify(self);
        [self updateCycleCycle];
        
    }];
}
-(void)updateCycleCycle{
    
    
    NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] init];
    [self.dataArray enumerateObjectsUsingBlock:^(NSMutableDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *key = obj[@"key"];
        
        
        NSInteger scope = [obj[@"reduceScope"] integerValue];
        NSInteger currentCount = [obj[@"currentCount"] integerValue];
        
        float a = [obj[@"com"] floatValue];
        a += TimerSpacing;
        //草tm的，float无法取模
        if ((a - ((int)a/scope) * scope) == 0) {
            //有限
            if (obj[@"maxCount"]) {
                
                
                NSInteger maxCount = [obj[@"maxCount"] integerValue];
                
                currentCount += 1;
                
                [obj setObject:[NSNumber numberWithInteger:currentCount] forKey:@"currentCount"];
                [obj removeObjectForKey:@"com"];
                
                for (id delegate in self.delegateArray) {
                    if([delegate respondsToSelector:@selector(timerWithKey:andMaxCount:andRestCount:)]){
                        [delegate timerWithKey:key andMaxCount:maxCount andRestCount:currentCount];
                    }
                }
                if(currentCount >= maxCount){
                    [indexSet addIndex:idx];
                }
            }else{
                //无限
                currentCount += 1;
                [obj setObject:[NSNumber numberWithInteger:currentCount] forKey:@"currentCount"];
                [obj removeObjectForKey:@"com"];
                for (id delegate in self.delegateArray) {
                    if ([delegate respondsToSelector:@selector(cycleTimerWithKey:RestCount:)]) {
                        [delegate cycleTimerWithKey:key RestCount:currentCount];
                    }
                }
            }
            
        }else{
            [obj setObject:[NSNumber numberWithFloat:a] forKey:@"com"];
        }
    }];
    [self.dataArray removeObjectsAtIndexes:indexSet];
}

-(void)addTimerWithMaxCount:(NSInteger)count
                     andKey:(NSString *)key
              andReduceScope:(NSInteger)reduceScope{
    [self removeTimerWithKey:key];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:[NSNumber numberWithInteger:count] forKey:@"maxCount"];
    [dic setObject:[NSNumber numberWithInteger:0] forKey:@"currentCount"];
    [dic setObject:key forKey:@"key"];
    [dic setObject:[NSNumber numberWithInteger:reduceScope] forKey:@"reduceScope"];
    [self.dataArray addObject:dic];
}

-(void)addCycleTimerWithKey:(NSString *)key andReduceScope:(NSInteger)reduceScope{
    [self removeTimerWithKey:key];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:[NSNumber numberWithInteger:0] forKey:@"currentCount"];
    [dic setObject:key forKey:@"key"];
    [dic setObject:[NSNumber numberWithInteger:reduceScope] forKey:@"reduceScope"];
    [self.dataArray addObject:dic];
}

-(void)removeTimerWithKey:(NSString *)key{
    [self.dataArray enumerateObjectsUsingBlock:^(NSMutableDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *k = [obj objectForKey:@"key"];
        if ([key isEqualToString:k]) {
            [self.dataArray removeObject:obj];
            *stop = YES;
        }
    }];
}

-(NSInteger)currentCountWithKey:(NSString *)key{
    __block NSInteger currentCount = 0;
    [self.dataArray enumerateObjectsUsingBlock:^(NSMutableDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *k = obj[@"key"];
        if([k isEqualToString:key]){
            currentCount = [obj[@"currentCount"] integerValue];
            *stop = YES;
        }
    }];
    return currentCount;
}

-(void)addDelegate:(id)delegate{
    if([self.delegateArray containsObject:delegate])
        return;
    [self.delegateArray safeAddObject:delegate];
}

-(void)removeDelegate:(id)delegate{
    if (![self.delegateArray containsObject:delegate])
        return;
    [self.delegateArray removeObject:delegate];
}


#pragma mark - GET/SET

@end
