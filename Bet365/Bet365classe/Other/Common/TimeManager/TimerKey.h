//
//  TimerKey.h
//  Bet365
//
//  Created by HHH on 2018/7/28.
//  Copyright © 2018年 jesse. All rights reserved.
//

#ifndef TimerKey_h
#define TimerKey_h

#define TIMER_USER_STATUS                @"TIMER_USER_STATUS" //用户状态

#define TIMER_HOME_WIN                   @"TIMER_HOME_WIN" //首页中奖轮询

#define TIMER_HOME_WINDATA_REPLACE       @"TIMER_HOME_WINDATA_REPLACE" //首页中奖数据更替
#define WZRY_MATCH                       @"WZRY_MATCH"  //王者荣耀赛事

#define WZRY_ODDS                        @"WZRY_ODDS"   //王者荣耀投注

#define SPORT_MATCH                      @"SPORT_MATCH"  //体育

#define SPORT_BET_MATCH                  @"SPORT_BET_MATCH"  //体育投注

#define USER_RECHARGEORDER               @"USER_RECHARGEORDER"  //充值订单

#define CAR_TIME                         @"CAR_TIME"  //赛车倒计时

#define CAR_TIME_PK10                    @"CAR_TIME_PK10"  //赛车请求数据

#define RED_PAG_TIME                     @"RED_PAG_TIME"  //红包

#define LOTTERY_INFO                     @"LOTTERY_INFO"  //彩票

#define LOTTERY_INFO_TIME                @"LOTTERY_INFO_TIME"  //彩票倒计时

#define TULONGBANG_TIME                  @"TULONGBANG_TIME"  //长龙排行

#define IMMEDIATE_TIME                   @"IMMEDIATE_TIME"  //个人推送消息

#endif /* TimerKey_h */
