//
//  NavigationManager.h
//  MACProject
//
//  Created by Deep river on 2017/3/29.
//  Copyright © 2017年 com.mackun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModuleRouterDefine.pch"
#define NAVI_MANAGER [NavigationManager sharedInstance]
@interface NavigationManager : NSObject

+ (NavigationManager *)sharedInstance;

+ (void)destroyInstance;

- (UIViewController *)getCurrentVC;

-(void)tokenInvalidPopRoot;

@end
