//
//  NavigationManager.m
//  MACProject
//
//  Created by Deep river on 2017/3/29.
//  Copyright © 2017年 com.mackun. All rights reserved.
//

#import "NavigationManager.h"



@implementation NavigationManager
static NavigationManager *instance = nil;
+ (NavigationManager *)sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[super alloc] init];
    });
    return instance;
}
+ (void)destroyInstance{
    instance = nil;
}
- (id)init{
    self = [super init];
    if (self)
    {
//        [self initLocation];
    }
    return self;
}
- (UIViewController *)getCurrentVC{
    UIViewController *result = nil;
    UIWindow * window = [[UIApplication sharedApplication].delegate window];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    id  nextResponder = nil;
    UIViewController *appRootVC=window.rootViewController;
    if (appRootVC.presentedViewController) {
        //多层present
        while (appRootVC.presentedViewController) {
            appRootVC = appRootVC.presentedViewController;
            if (appRootVC) {
                nextResponder = appRootVC;
            }else{
                break;
            }
        }
    }else{
        nextResponder = appRootVC;
//        UIView *frontView = [[window subviews] safeObjectAtIndex:0];
//        nextResponder = [frontView nextResponder];
    }
    
    if ([nextResponder isKindOfClass:[UITabBarController class]]){
        
        UITabBarController * tabbar = (UITabBarController *)nextResponder;
        NSArray *list = tabbar.childViewControllers;
        NSUInteger idx = tabbar.selectedIndex;
        if (idx != NSNotFound) {
            UINavigationController * nav = [list safeObjectAtIndex:idx];
            result=nav.childViewControllers.lastObject;
        }else{
            return tabbar;
        }
        
        
    }else if ([nextResponder isKindOfClass:[UINavigationController class]]){
        UIViewController * nav = (UIViewController *)nextResponder;
        result = nav.childViewControllers.lastObject;
    }else{
        result = nextResponder;
    }
    if ([result isKindOfClass:[UIViewController class]]) {
        return result;
    }
    return nil;
    
    
//    UIWindow* window = nil;
//       if (@available(iOS 13.0, *)) {
//           for (UIWindowScene* windowScene in [UIApplication sharedApplication].connectedScenes)
//           {
//              if (windowScene.activationState == UISceneActivationStateForegroundActive)
//              {
//                   window = windowScene.windows.firstObject;
//
//                   break;
//              }
//           }
//       }else{
//           #pragma clang diagnostic push
//           #pragma clang diagnostic ignored "-Wdeprecated-declarations"
//               // 这部分使用到的过期api
//            window = [UIApplication sharedApplication].keyWindow;
//           #pragma clang diagnostic pop
//       }
//    if([window.rootViewController isKindOfClass:NSNull.class]){
//        return nil;
//    }
//    id result = nil;
//    id nextResponder = window.rootViewController;
//
//    if ([nextResponder isKindOfClass:[UITabBarController class]]){
//
//        UITabBarController * tabbar = (UITabBarController *)nextResponder;
//        NSArray *list = tabbar.childViewControllers;
//        NSUInteger idx = tabbar.selectedIndex;
//        if (idx != NSNotFound) {
//            UINavigationController * nav = [list safeObjectAtIndex:idx];
//            result=nav.childViewControllers.lastObject;
//        }else{
//            return tabbar;
//        }
//
//
//    }else if ([nextResponder isKindOfClass:[UINavigationController class]]){
//        UIViewController * nav = (UIViewController *)nextResponder;
//        result = nav.childViewControllers.lastObject;
//    }else{
//        result = nextResponder;
//    }
//    if ([result isKindOfClass:[UIViewController class]]) {
//        return result;
//    }
//    return nil;
}

-(void)tokenInvalidPopRoot{
    
    [Bet365AlertSheet showChooseAlert:@"认证信息失效" Message:@"请重新登录" Items:@[@"重新登录",@"取消"] Handler:^(NSInteger index) {
        if ([NAVI_MANAGER getCurrentVC].presentingViewController) {
            [[NAVI_MANAGER getCurrentVC] dismissViewControllerAnimated:YES completion:nil];
        } else {
            [[NAVI_MANAGER getCurrentVC].navigationController popViewControllerAnimated:YES];
        }
        [[NAVI_MANAGER getCurrentVC].navigationController popToRootViewControllerAnimated:NO];
        dispatch_async(dispatch_get_main_queue(), ^{
            [JesseAppdelegate.tabbar setSelectedIndex:0];
            if (index == 0) {
                [USER_DATA_MANAGER shouldPush2Login];
            }
        });
    }];
}

@end
