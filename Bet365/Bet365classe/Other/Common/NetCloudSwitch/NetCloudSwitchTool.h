//
//  NetCloudSwitchTool.h
//  Bet365
//
//  Created by HHH on 2018/8/7.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CloudModel.h"

#define NET_CLOUD_SWITCH [NetCloudSwitchTool shareInstance]

typedef void(^NetCloudSwitchCompleted)(BOOL showTabbar);

@interface NetCloudSwitchTool : NSObject

+(instancetype)shareInstance;

-(void)requestAllCloud:(NetCloudSwitchCompleted)completed;

-(void)findCloudBy:(NSString *)appID Completed:(void(^)(NSMutableArray <CloudModel *>*models))cloudCompleted;

-(void)switchCloudUrlBy:(NSArray <CloudModel*>*)modelList Completed:(void(^)(NSString *urlStr))completed;


@property (nonatomic,strong)NSMutableArray <NSString *>*urlList;

@property (nonatomic,assign)BOOL hasRequest;
@end
