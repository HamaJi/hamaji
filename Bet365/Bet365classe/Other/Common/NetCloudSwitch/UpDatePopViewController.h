//
//  UpDatePopViewController.h
//  Bet365
//
//  Created by HHH on 2018/8/7.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CloudModel.h"
typedef NS_ENUM(NSUInteger, UpDatePopViewTapType) {
    /** 更新*/
    UpDatePopViewTapType_UPDATE,
    /** 关闭*/
    UpDatePopViewTapType_CLOSE,
};
typedef void(^UpDatePopViewTapAction)(UpDatePopViewTapType tapType);
@interface UpDatePopViewController : Bet365ViewController
@property (nonatomic,strong)CloudModel *model;
@property (nonatomic,copy)UpDatePopViewTapAction tapHandle;
@end
