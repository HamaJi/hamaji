//
//  NetCloudSwitchTool.m
//  Bet365
//
//  Created by HHH on 2018/8/7.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NetCloudSwitchTool.h"
#import "cloudToolManager.h"
#import "BaiduClient.h"

#import "DismissingBottomAnimator.h"
#import "PresentingBottomAnimator.h"

#import "UpDatePopViewController.h"
#import "RequestUrlHeader.h"
#import "NoteViewController.h"
@interface NetCloudSwitchTool()

@property(nonatomic,strong)AFHTTPSessionManager *manager;

@property (nonatomic,strong)NSURLSessionConfiguration *sessionConfig;

@property(nonatomic,strong)UpDatePopViewController *updateViewController;

@property(nonatomic,copy) NetCloudSwitchCompleted completed;

@end
@implementation NetCloudSwitchTool

static NetCloudSwitchTool *instance;

+(instancetype)shareInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[NetCloudSwitchTool alloc] init];
    });
    return instance;
}
#pragma mark - Public
-(void)findCloudBy:(NSString *)appID Completed:(void(^)(NSMutableArray <CloudModel *>*models))cloudCompleted{
    
    __block CloudModel *_leanModel;
    __block CloudModel *_baiduModel;
    
    dispatch_group_t group = dispatch_group_create();
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [self getLeanCloudByAppID:appID completed:^(CloudModel *model) {
            _leanModel = model;
            dispatch_semaphore_signal(sema);
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [self getBaiduCloudByAppID:appID completed:^(CloudModel *model) {
            _baiduModel = model;
            dispatch_semaphore_signal(sema);
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
#ifdef DEBUG
    
#else
    
#endif
    
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        NSMutableArray <CloudModel *>*models = @[].mutableCopy;
        [models safeAddObject:_baiduModel];
        [models safeAddObject:_leanModel];
        cloudCompleted ? cloudCompleted(models) : nil;
    });
}

-(void)requestAllCloud:(NetCloudSwitchCompleted)completed{
    
    self.completed = completed;
#ifdef DEBUG
    
#else
    if (!SerVer_Url.length &&
        BET_CONFIG.customURL.length) {
        [NET_DATA_MANAGER setServerUrl:BET_CONFIG.customURL];
    }
#endif
    self.hasRequest = YES;
    @weakify(self);
    [self findCloudBy:BET_CONFIG.userSettingData.defaultAppID Completed:^(NSMutableArray<CloudModel *> *models) {
        @strongify(self);
        NSMutableDictionary *mData = @{}.mutableCopy;
        [models enumerateObjectsUsingBlock:^(CloudModel * _Nonnull model, NSUInteger modelIdx, BOOL * _Nonnull modelStop) {
            [model.domain enumerateObjectsUsingBlock:^(NSString * _Nonnull url, NSUInteger idx, BOOL * _Nonnull stop) {
                [mData setObject:url forKey:url];
            }];
        }];
        [self.urlList addObjectsFromArray:mData.allValues];
#ifdef DEBUG
        
#else
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            [self showupdateview:models.firstObject];
        });
#endif
        [self switchCloudUrlBy:models Completed:^(NSString *urlStr) {
            if (urlStr.length) {
                [NET_DATA_MANAGER setServerUrl:urlStr];
            }
            self.completed ? self.completed(SerVer_Url.length > 0) : nil;
        }];
    }];
    
    
    [self findCloudBy:@"ACT_HEATSWITCH" Completed:^(NSMutableArray<CloudModel *> *models) {
#ifdef DEBUG
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            [self showupdateview:models.firstObject];
        });
#else
        
#endif
    }];

}


#pragma mark - Private
-(void)getLeanCloudByAppID:(NSString *)appID completed:(void(^)(CloudModel *model))completed{
    NSString *content = [[cloudToolManager initManager] getLeanObject:appID][LEAN_CLOUD_KEY_CONTENT];
    
    NSDictionary *data = [[content aci_decryptWithAESBykey:AES_KEY] mj_JSONObject];
    NSError *error;
    CloudModel *cloudModel = [MTLJSONAdapter modelOfClass:[CloudModel class] fromJSONDictionary:data error:&error];
    completed ? completed(cloudModel) : nil;
}

-(void)getBaiduCloudByAppID:(NSString *)appID completed:(void(^)(CloudModel *model))completed{
    [BAI_CLOUD_MANAGER getObjectByBucketName:Baidu_INFO_BUCKET_PATCH
                                  ObjectName:[appID aci_encryptWithAESBykey:AES_KEY]
                                     Success:^(BOSGetObjectResponse *object) {
                                         NSDictionary *data = [[[object.objectData.data mj_JSONString] aci_decryptWithAESBykey:AES_KEY] mj_JSONObject];
                                         NSError *error;
                                         CloudModel *cloudModel = [MTLJSONAdapter modelOfClass:[CloudModel class] fromJSONDictionary:data error:&error];
                                         if (!error) {
                                             completed ? completed(cloudModel) : nil;
                                         }
                                     } Fail:^(NSError *error) {
                                         completed ? completed(nil) : nil;
                                     }];
}

-(void)switchCloudUrlBy:(NSArray <CloudModel*>*)modelList Completed:(void(^)(NSString *urlStr))completed{
    if (!modelList.count) {
        completed ? completed(nil) : nil;
    }
    self.hasRequest = (modelList.count > 0);
    __block BOOL _isSet = NO;
    [self manager];
    dispatch_group_t group = dispatch_group_create();
    [modelList enumerateObjectsUsingBlock:^(CloudModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stopList) {
        [obj.domain enumerateObjectsUsingBlock:^(NSString*  _Nonnull url, NSUInteger idx, BOOL * _Nonnull stopUrl) {
            dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                dispatch_semaphore_t sema = dispatch_semaphore_create(0);
                [self.manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    if (!_isSet) {
                        _isSet = YES;
                        completed ? completed([task.originalRequest.URL absoluteString]) : nil;
                    }
                    dispatch_semaphore_signal(sema);
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    dispatch_semaphore_signal(sema);
                }];
                dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
            });
            
        }];
    }];
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        if(!_isSet){
            completed ? completed(nil) : nil;
        }
    });
}

-(void)showupdateview:(CloudModel *)model{
    if (BET_CONFIG.buildVersion < model.build) {
        self.updateViewController.model = model;
        UIViewController *vc = [NAVI_MANAGER getCurrentVC];
        if ([vc isKindOfClass:[NoteViewController class]]) {
            [[NAVI_MANAGER getCurrentVC] dismissViewControllerAnimated:YES completion:nil];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [JesseAppdelegate.tabbar presentViewController:self.updateViewController
                                                      animated:YES
                                                    completion:nil];
            });
        }else{
            [JesseAppdelegate.tabbar presentViewController:self.updateViewController
                                                  animated:YES
                                                completion:nil];
        }

    }
//#ifdef DEBUG
//    if (BET_CONFIG.buildVersion < model.actBuild) {
//        self.updateViewController.model = model;
//        @weakify(self);
//        [[NAVI_MANAGER getCurrentVC] dismissViewControllerAnimated:YES completion:^{
//            @strongify(self);
//            [JesseAppdelegate.tabbar presentViewController:self.updateViewController
//                                                  animated:YES
//                                                completion:nil];
//        }];
//    }
//#else
//
//#endif
}


#pragma mark - GET/SET
-(AFHTTPSessionManager *)manager
{
    if (_manager==nil) {
        _manager=[AFHTTPSessionManager manager];
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",
                                                              @"text/plain",
                                                              @"text/json",
                                                              @"application/json",
                                                              @"text/javascript",
                                                              @"image/jpeg", nil];
        _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        _manager.requestSerializer.timeoutInterval = 10;
    }
    return _manager;
}

-(UpDatePopViewController *)updateViewController{
    if (!_updateViewController) {
        _updateViewController = [[UpDatePopViewController alloc] initWithNibName:@"UpDatePopViewController" bundle:nil];
        _updateViewController.tapHandle = ^(UpDatePopViewTapType tapType) {
            if (tapType == UpDatePopViewTapType_UPDATE) {
                
            }else if (tapType == UpDatePopViewTapType_CLOSE){
                
            }
        };
    }
    return _updateViewController;
}
-(NSMutableArray<NSString *> *)urlList{
    if (!_urlList) {
        _urlList = [NSMutableArray array];
    }
    return _urlList;
}

@end
