//
//  UpDatePopViewController.m
//  Bet365
//
//  Created by HHH on 2018/8/7.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "UpDatePopViewController.h"
#import "PresentingBottomAnimator.h"
#import "DismissingBottomAnimator.h"

@interface UpDatePopViewController ()<UIViewControllerTransitioningDelegate>



@property (weak, nonatomic) IBOutlet UILabel *updateTitileLabel;

@property (weak, nonatomic) IBOutlet UILabel *updateSubLabel;

@property (weak, nonatomic) IBOutlet UIButton *updateBtn;

@property (weak, nonatomic) IBOutlet UIButton *cancleBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintWidthMulitier;

@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;

@end

@implementation UpDatePopViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self.transitioningDelegate = self;
        self.modalPresentationStyle = UIModalPresentationCustom;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.bgImageView setImage:[UIImage imageNamed:@"update_app_top_bg"]];
    
    [self.updateBtn setBackgroundColor:[UIColor skinViewKitSelColor]];
    [self.updateBtn setTitleColor:[UIColor skinViewKitNorColor] forState:UIControlStateNormal];
    
    [self.bgImageView setBackgroundColor:[UIColor skinNaviBgColor]];
    
    [self.cancleBtn setBackgroundColor:[UIColor skinViewKitNorColor]];
    [self.cancleBtn setTitleColor:[UIColor skinViewKitSelColor] forState:UIControlStateNormal];
    
    self.updateTitileLabel.text = _model.version;
    self.updateSubLabel.text = _model.updateDescription ? _model.updateDescription : @"";
    CGFloat multiplier = 0.0;
    if (_model.forcedUpdate == 1) {
        multiplier = 1;
    }else{
        multiplier = 0.5;
    }
    [self.updateBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.bottom.equalTo(self.updateBtn.superview);
        make.bottom.equalTo(self.cancleBtn);
        
        make.height.equalTo(self.cancleBtn);
        make.width.equalTo(self.updateBtn.superview.mas_width).multipliedBy(multiplier);
    }];
    [self.view layoutIfNeeded];
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)updateAction:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.model.download]];
    
    self.tapHandle ? self.tapHandle([(UIButton *)sender tag]) : nil;
}
- (IBAction)closeAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    self.tapHandle ? self.tapHandle([(UIButton *)sender tag]) : nil;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UIViewControllerTransitioningDelegate
- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source{
    
    return [PresentingBottomAnimator new];
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    return [DismissingBottomAnimator new];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
