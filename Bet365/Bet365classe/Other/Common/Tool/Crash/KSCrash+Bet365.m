//
//  KSCrash+Bet365.m
//  Bet365
//
//  Created by super_小鸡 on 2019/5/20.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "KSCrash+Bet365.h"
#import <Aspects/Aspects.h>

#import <KSCrash/KSCrash.h>
#import <KSCrash/KSCrashInstallation+Alert.h>
#import <KSCrash/KSCrashInstallationStandard.h>
#import <KSCrash/KSCrashInstallationQuincyHockey.h>
#import <KSCrash/KSCrashInstallationEmail.h>
#import <KSCrash/KSCrashInstallationVictory.h>
#import <KSCrash/KSCrashReportFilterAppleFmt.h>
#import <KSCrash/KSCrashReportSinkStandard.h>

#import "NetWorkMannager+Crash.h"
@implementation KSCrash (Bet365)

-(void)hookMetho{
    aji_swizzleInstanceMethod(self,@selector(sendReports:onCompletion:),@selector(aji_swizzleSendReports:onCompletion:));
}

- (void)aji_swizzleSendReports:(NSArray*) reports onCompletion:(KSCrashReportFilterCompletion) onCompletion{
    
    if (reports.count) {
        [reports enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isKindOfClass:[NSDictionary class]]) {
                [NET_DATA_MANAGER requestSendCrash:obj success:^(id responseObject) {
                    
                } failure:^(NSError *error) {
                    
                }];
            }
        }];
    }
    [self aji_swizzleSendReports:reports onCompletion:onCompletion];
    
}
@end
