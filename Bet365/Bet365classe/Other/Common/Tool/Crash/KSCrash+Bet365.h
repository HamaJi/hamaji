//
//  KSCrash+Bet365.h
//  Bet365
//
//  Created by super_小鸡 on 2019/5/20.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <KSCrash/KSCrash.h>
#import <KSCrash/KSCrashInstallation+Alert.h>
#import <KSCrash/KSCrashInstallationStandard.h>
#import <KSCrash/KSCrashInstallationQuincyHockey.h>
#import <KSCrash/KSCrashInstallationEmail.h>
#import <KSCrash/KSCrashInstallationVictory.h>
#import <KSCrash/KSCrashReportFilterAppleFmt.h>

NS_ASSUME_NONNULL_BEGIN

@interface KSCrash (Bet365)

-(void)hookMetho;

- (void) sendReports:(NSArray*) reports onCompletion:(KSCrashReportFilterCompletion) onCompletion;

@end

NS_ASSUME_NONNULL_END
