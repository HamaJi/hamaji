//
//  KSCrashReportSinkStandard+Bet365.m
//  Bet365
//
//  Created by super_小鸡 on 2019/5/20.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "KSCrashReportSinkStandard+Bet365.h"
#import <Aspects/Aspects.h>

@implementation KSCrashReportSinkStandard (Bet365)

+(void)load{
    
    [KSCrashReportSinkStandard aspect_hookSelector:@selector(sinkWithURL:) withOptions:AspectPositionAfter usingBlock:^(id<AspectInfo> aspectInfo,NSURL *url){
        id instance = aspectInfo.instance;
        
        aji_swizzleClassMethod([instance class],
                               @selector( filterReports:onCompletion:),
                               @selector( aji_swizzleFilterReports:onCompletion:));
        NSLog(@"");
    } error:nil];
}

- (void) aji_swizzleFilterReports:(NSArray*) reports
          onCompletion:(KSCrashReportFilterCompletion) onCompletion{
    
}


@end
