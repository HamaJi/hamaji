//
//  CrashSec.m
//  Bet365
//
//  Created by super_小鸡 on 2019/5/13.
//  Copyright © 2019 HHH. All rights reserved.
//

#import "CrashSec.h"

#import "KSCrash+Bet365.h"

@implementation CrashSec

void installUncaughtExceptionHandler(){
    KSCrashInstallation* installation = [CrashSec makeStandardInstallation];
    [installation install];
    KSCrash *ksCrash = [KSCrash sharedInstance];
    [ksCrash hookMetho];
#ifdef DEBUG
    ksCrash.deleteBehaviorAfterSendAll = KSCDeleteAlways;
#else
    ksCrash.deleteBehaviorAfterSendAll = KSCDeleteOnSucess;
#endif
    [installation sendAllReportsWithCompletion:^(NSArray* reports, BOOL completed, NSError* error){
        if(completed){
            
        }else{
            
        }
    }];
}

+ (KSCrashInstallation*) makeStandardInstallation{
    NSURL* url = [NSURL URLWithString:@"http://appadmin.yibofafa666.com/api/app/error/upload4Ios"];
    
    KSCrashInstallationStandard* standard = [KSCrashInstallationStandard sharedInstance];
    standard.url = url;
    
    return standard;
    
}


-(void)hookSink{
    
}

@end
