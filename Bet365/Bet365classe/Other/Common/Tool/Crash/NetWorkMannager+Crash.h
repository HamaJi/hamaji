//
//  NetWorkMannager+Crash.h
//  Bet365
//
//  Created by super_小鸡 on 2019/5/21.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "NetWorkMannager.h"

NS_ASSUME_NONNULL_BEGIN

@interface NetWorkMannager (Crash)

- (NSURLSessionTask *)requestSendCrash:(NSDictionary *)crashData
                               success:(HttpRequestSuccess)success
                               failure:(HttpRequestFailed)failure;

@end

NS_ASSUME_NONNULL_END
