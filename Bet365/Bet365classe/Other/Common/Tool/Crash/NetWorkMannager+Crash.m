//
//  NetWorkMannager+Crash.m
//  Bet365
//
//  Created by super_小鸡 on 2019/5/21.
//  Copyright © 2019 jesse. All rights reserved.
//
#import "NetWorkMannager+Crash.h"
#import "RequestUrlHeader.h"


@implementation NetWorkMannager (Crash)

- (NSURLSessionTask *)requestSendCrash:(NSDictionary *)crashData
                              success:(HttpRequestSuccess)success
                              failure:(HttpRequestFailed)failure{
    
    NSMutableDictionary *crash = [crashData[@"crash"] mutableCopy];
    NSArray <NSDictionary *>*threads = [crash objectForKey:@"threads"];
    NSMutableArray *crashedThreads = @[].mutableCopy;
    [threads enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj[@"crashed"] boolValue]) {
            [crashedThreads addObject:obj];
        }
    }];
    [crash safeRemoveObjectForKey:@"threads"];
    [crash safeSetObject:crashData[@"system"] forKey:@"system"];
    [crash safeSetObject:USER_DATA_MANAGER.userInfoData.account forKey:@"account"];
    [crash safeSetObject:crashedThreads forKey:@"threads"];
    NSString *crashJsonStr = [crash jk_JSONString];
    if (!crashJsonStr) {
        return nil;
    }
    AFHTTPSessionManager *manager = [self createAFHTTPSessionManagerByRequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON];
    return [manager POST:LOG_SEND_URL
              parameters:@{@"code":[NSString stringWithFormat:@"%@_CRASH",BET_CONFIG.userSettingData.defaultAppID],
                           @"log":crashJsonStr,
                           @"version":[NSString stringWithFormat:@"%li",BET_CONFIG.buildVersion]}
                progress:nil
                 success:^(NSURLSessionDataTask *_Nonnull task, id _Nonnull responseObject) {
                     
                 }
                 failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
                     
                 }];
}

@end
