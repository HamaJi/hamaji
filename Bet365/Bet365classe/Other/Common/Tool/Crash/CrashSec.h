//
//  CrashSec.h
//  Bet365
//
//  Created by super_小鸡 on 2019/5/13.
//  Copyright © 2019 HHH. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CrashSec : NSObject

void installUncaughtExceptionHandler();

@end

NS_ASSUME_NONNULL_END
