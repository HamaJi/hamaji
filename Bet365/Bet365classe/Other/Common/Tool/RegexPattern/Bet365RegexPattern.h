//
//  Bet365RegexPattern.h
//  Bet365
//
//  Created by luke on 2018/8/4.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Bet365RegexPattern : NSObject
//邮箱
+ (BOOL) validateEmail:(NSString *)email;
//手机号码验证
+ (BOOL) validateMobile:(NSString *)mobile;
//微信号
+ (BOOL)validateWechat:(NSString *)wechat;
//登录用户名
+ (BOOL)loginUserName:(NSString *)name;
//用户名
+ (BOOL) validateUserName:(NSString *)name;
//密码
+ (BOOL) validatePassword:(NSString *)passWord;
//验证QQ号
+(BOOL)validateIsValidQQ:(NSString *)targetString;
//验证只能输入n位的数字
+(BOOL)validateIsOnlyManyNumber:(NSString *)targetString withFigure:(NSInteger)figure;
//昵称
+ (BOOL) validateNickname:(NSString *)nickname;
//银行卡号
+(BOOL)isBankCard:(NSString *)cardNumber;

//正整数
+ (BOOL)validateNumber:(NSString *)number;
//匹配正浮点数
+ (BOOL)validateZhengNumber:(NSString *)number;

@end
