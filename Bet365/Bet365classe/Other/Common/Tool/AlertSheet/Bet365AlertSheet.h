//
//  Bet365AlertSheet.h
//  Bet365
//
//  Created by HHH on 2018/7/26.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMAlertView.h"


@interface Bet365AlertSheet : NSObject

/**
 选择弹窗
 
 @param titile 标题
 @param message 内容
 @param items 按钮Titiles
 @param handler 点击回调
 */
+(void)showChooseAlert:(NSString *)titile
               Message:(NSString *)message
                 Items:(NSArray <NSString *>*)items
               Handler:(MMPopupItemHandler)handler;

+(void)showChooseAlert:(NSString *)titile
      AttributeMessage:(NSAttributedString *)attributeMessage
                 Items:(NSArray <NSString *>*)items
               Handler:(MMPopupItemHandler)handler;


+(void)showInput:(NSString *)titile
         Message:(NSString *)detail
     Placeholder:(NSString *)placeholder
         Handler:(MMPopupInputHandler)handler;
@end
