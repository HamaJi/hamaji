//
//  Bet365AlertSheet.m
//  Bet365
//
//  Created by HHH on 2018/7/26.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Bet365AlertSheet.h"
@interface MMAlertView (Attributes)

-(instancetype)initWithTitle:(NSString *)title attributeDetail:(NSAttributedString *)detail items:(NSArray *)items;

@end

@implementation MMAlertView (Attributes)
-(instancetype)initWithTitle:(NSString *)title attributeDetail:(NSAttributedString *)detail items:(NSArray *)items{
    self = [super init];
    
    if ( self ){
        NSAssert(items.count>0, @"Could not find any items.");
        
        MMAlertViewConfig *config = [MMAlertViewConfig globalConfig];
        
        self.type = MMPopupTypeAlert;
        self.withKeyboard = NO;
        [self setValue:items forKey:@"actionItems"];
        
        self.layer.cornerRadius = config.cornerRadius;
        self.clipsToBounds = YES;
        self.backgroundColor = config.backgroundColor;
        self.layer.borderWidth = MM_SPLIT_WIDTH;
        self.layer.borderColor = config.splitColor.CGColor;
        
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(config.width);
        }];
        [self setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        [self setContentHuggingPriority:UILayoutPriorityFittingSizeLevel forAxis:UILayoutConstraintAxisVertical];
        
        MASViewAttribute *lastAttribute = self.mas_top;
        if ( title.length > 0 )
        {
            UILabel *titileLabel = [UILabel setAllocLabelWithText:title FontOfSize:17 rgbColor:0x000000];
            
            [self addSubview:titileLabel];
            [titileLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(lastAttribute).offset(config.innerMargin);
                make.left.right.equalTo(self).insets(UIEdgeInsetsMake(0, config.innerMargin, 0, config.innerMargin));
            }];
            titileLabel.textColor = config.titleColor;
            titileLabel.textAlignment = NSTextAlignmentCenter;
            
            titileLabel.numberOfLines = 0;
            titileLabel.backgroundColor = self.backgroundColor;
            
            lastAttribute = titileLabel.mas_bottom;
            [self setValue:titileLabel forKey:@"titleLabel"];
        }
        
        if ( detail.length > 0 )
        {
            UILabel *detailLabel = [UILabel setAllocLabelWithText:@"" FontOfSize:17 rgbColor:0x000000];
            detailLabel.numberOfLines = 0;
            [self addSubview:detailLabel];
            [detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(lastAttribute).offset(5);
                make.left.right.equalTo(self).insets(UIEdgeInsetsMake(0, config.innerMargin, 0, config.innerMargin));
            }];
            
            detailLabel.attributedText = detail;
            lastAttribute = detailLabel.mas_bottom;
            [self setValue:detailLabel forKey:@"detailLabel"];
        }
        
        UIView *buttonView = [UIView new];
        [self addSubview:buttonView];
        [buttonView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(lastAttribute).offset(config.innerMargin);
            make.left.right.equalTo(self);
        }];
        
        __block UIButton *firstButton = nil;
        __block UIButton *lastButton = nil;
        
        for ( NSInteger i = 0 ; i < items.count; ++i )
        {
            MMPopupItem *item = items[i];
            UIButton *btn = [UIButton mm_buttonWithTarget:self action:@selector(attribute_actionButton:)];
            [buttonView addSubview:btn];
            btn.tag = i;
            
            [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                
                if ( items.count <= 2 )
                {
                    make.top.bottom.equalTo(buttonView);
                    make.height.mas_equalTo(config.buttonHeight);
                    
                    if ( !firstButton )
                    {
                        firstButton = btn;
                        make.left.equalTo(buttonView.mas_left).offset(-MM_SPLIT_WIDTH);
                    }
                    else
                    {
                        make.left.equalTo(lastButton.mas_right).offset(-MM_SPLIT_WIDTH);
                        make.width.equalTo(firstButton);
                    }
                }
                else
                {
                    make.left.right.equalTo(buttonView);
                    make.height.mas_equalTo(config.buttonHeight);
                    
                    if ( !firstButton )
                    {
                        firstButton = btn;
                        make.top.equalTo(buttonView.mas_top).offset(-MM_SPLIT_WIDTH);
                    }
                    else
                    {
                        make.top.equalTo(lastButton.mas_bottom).offset(-MM_SPLIT_WIDTH);
                        make.width.equalTo(firstButton);
                    }
                }
                
                lastButton = btn;
            }];
            [btn setBackgroundImage:[UIImage mm_imageWithColor:self.backgroundColor] forState:UIControlStateNormal];
            [btn setBackgroundImage:[UIImage mm_imageWithColor:config.itemPressedColor] forState:UIControlStateHighlighted];
            [btn setTitle:item.title forState:UIControlStateNormal];
            [btn setTitleColor:item.highlight?config.itemHighlightColor:config.itemNormalColor forState:UIControlStateNormal];
            btn.layer.borderWidth = MM_SPLIT_WIDTH;
            btn.layer.borderColor = config.splitColor.CGColor;
            btn.titleLabel.font = (item==items.lastObject)?[UIFont boldSystemFontOfSize:config.buttonFontSize]:[UIFont systemFontOfSize:config.buttonFontSize];
        }
        [lastButton mas_updateConstraints:^(MASConstraintMaker *make) {
            
            if ( items.count <= 2 )
            {
                make.right.equalTo(buttonView.mas_right).offset(MM_SPLIT_WIDTH);
            }
            else
            {
                make.bottom.equalTo(buttonView.mas_bottom).offset(MM_SPLIT_WIDTH);
            }
            
        }];
        
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.bottom.equalTo(buttonView.mas_bottom);
            
        }];
        
        [self setValue:buttonView forKey:@"buttonView"];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(attribute_notifyTextChange:) name:UITextFieldTextDidChangeNotification object:nil];
    
    return self;
}

-(void)attribute_actionButton:(UIButton *)btn{
    SEL sel = NSSelectorFromString(@"actionButton:");
    ((void (*)(id, SEL,UIButton *))[self methodForSelector:sel])(self,sel,btn);
}

-(void)attribute_notifyTextChange:(NSNotification *)n{
    SEL sel = NSSelectorFromString(@"notifyTextChange:");
    ((void (*)(id, SEL,NSNotification *))[self methodForSelector:sel])(self,sel,n);
}

@end

//MMPopupItem
@implementation Bet365AlertSheet

+(void)showChooseAlert:(NSString *)titile
               Message:(NSString *)message
                 Items:(NSArray <NSString *>*)items
               Handler:(MMPopupItemHandler)handler{
    [MMPopupView hideAll];
    NSMutableArray *mItems = [NSMutableArray array];
    [items enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MMPopupItem *item;
        if (items.count - 1 == idx) {
            item = MMItemMake(obj, MMItemTypeHighlight, handler);
        }else{
            item = MMItemMake(obj, MMItemTypeNormal, handler);
        }
        [mItems addObject:item];
    }];
    MMAlertView *alertView = [[MMAlertView alloc] initWithTitle:titile detail:message items:mItems];
    alertView.attachedView.mm_dimBackgroundBlurEnabled = YES;
    alertView.attachedView.mm_dimBackgroundBlurEffectStyle = UIBlurEffectStyleExtraLight;
    [alertView show];
    
}

+(void)showChooseAlert:(NSString *)titile
      AttributeMessage:(NSAttributedString *)attributeMessage
                 Items:(NSArray <NSString *>*)items
               Handler:(MMPopupItemHandler)handler{
    [MMPopupView hideAll];
    NSMutableArray *mItems = [NSMutableArray array];
    [items enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MMPopupItem *item;
        if (items.count - 1 == idx) {
            item = MMItemMake(obj, MMItemTypeHighlight, handler);
        }else{
            item = MMItemMake(obj, MMItemTypeNormal, handler);
        }
        [mItems addObject:item];
    }];
    MMAlertView *alertView = [[MMAlertView alloc] initWithTitle:titile attributeDetail:attributeMessage items:mItems];
    alertView.attachedView.mm_dimBackgroundBlurEnabled = YES;
    alertView.attachedView.mm_dimBackgroundBlurEffectStyle = UIBlurEffectStyleDark;
    [alertView show];
    
}



+(void)showInput:(NSString *)titile Message:(NSString *)detail Placeholder:(NSString *)placeholder Handler:(MMPopupInputHandler)handler{
    MMAlertView *alertView = [[MMAlertView alloc] initWithInputTitle:titile detail:detail placeholder:placeholder handler:handler];
    alertView.attachedView.mm_dimBackgroundBlurEnabled = YES;
    alertView.attachedView.mm_dimBackgroundBlurEffectStyle = UIBlurEffectStyleDark;
    [alertView show];
}
@end
