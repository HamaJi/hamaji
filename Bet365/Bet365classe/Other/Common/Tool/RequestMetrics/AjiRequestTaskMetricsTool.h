//
//  AjiRequestTaskMetricsTool.h
//
//  Created by HHH on 2018/7/18.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFURLSessionManager+Metrics.h"
#import "NetMetricsModel.h"

#define METRICS_TOOL [AjiRequestTaskMetricsTool sharedInstance]

/**
 取时间差

 @param sinceDate 开始时间
 @param endDate 结束时间
 @return NSArray <MetricsObj*>*
 */
#define METRICS_TOOL_FINDE_BETWEEN(sinceDate,endDate) ([METRICS_TOOL findMetricsObjsByCriteria:[NSString stringWithFormat:@"WHERE startDate >= %li and startDate <%li",sinceDate,endDate]])



@protocol AjiRequestTaskMetricsToolDelegate <NSObject>
@optional
- (void)AJi_URLSessionTask:(NSURLSessionTask *)task didFinishCollectingMetrics:(NetMetricsModel *)metrics API_AVAILABLE(macosx(10.12), ios(10.0), watchos(3.0), tvos(10.0));
@end


@interface AjiRequestTaskMetricsTool : NSObject





+ (instancetype)sharedInstance;

- (void)aji_requestRecordByOberser:(id)obser Task:(NSURLSessionTask*)task Metrics:(NSURLSessionTaskMetrics*)metrics API_AVAILABLE(ios(10.0));

-(void)addDelegate:(id)obser;

-(void)removeDelegate:(id)obser;

-(void)getMetricEntityList:(void(^)(NSArray <NetMetricsModel *>*list))completed;

-(void)cleanAllMetrics;

- (void)requestSendError:(NSError *)error;

@end
