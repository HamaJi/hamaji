//
//  NetStatistics.m
//  Bet365
//
//  Created by HHH on 2018/7/14.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NetTaskStatistics.h"
#import "NetWorkMannager+Condition.h"
@interface NetTaskStatistics()<NSURLSessionDelegate,NSURLSessionTaskDelegate,AjiRequestTaskMetricsToolDelegate>

@property (nonatomic,strong)NSURLSessionConfiguration *sessionConfig;

@end

@implementation NetTaskStatistics

+(instancetype)shareInstance{
    static NetTaskStatistics *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[NetTaskStatistics alloc] init];
    });
    return instance;
}

-(instancetype)init{
    if (self = [super init]) {
        [METRICS_TOOL addDelegate:self];
    }
    return self;
}
-(void)dealloc{
    [METRICS_TOOL removeDelegate:self];
}
#pragma mark - Public

-(NSURLSessionDataTask *)runTaskByRequestUrlString:(NSString *)urlStr{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0f];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:self.sessionConfig delegate:self delegateQueue:nil];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request];
    [task resume];
    return task;
}

#pragma mark - Private

#pragma mark - NSURLSessionTaskDelegate
-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
    
}
-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didFinishCollectingMetrics:(NSURLSessionTaskMetrics *)metrics API_AVAILABLE(ios(10.0)){
    
}
#pragma mark - AjiRequestTaskMetricsToolDelegate
-(void)AJi_URLSessionTask:(NSURLSessionTask *)task didFinishCollectingMetrics:(NetMetricsModel *)metrics{
    if(self.delegate && [self.delegate respondsToSelector:@selector(url:didFinishCollectingMetrics:)]){
        [self.delegate url:task.originalRequest.URL.absoluteString didFinishCollectingMetrics:metrics];
    }
}
#pragma mark - GET/SET
-(NSURLSessionConfiguration *)sessionConfig{
    if (!_sessionConfig) {
        _sessionConfig = [NSURLSessionConfiguration ephemeralSessionConfiguration];
        _sessionConfig.allowsCellularAccess = YES;
        _sessionConfig.timeoutIntervalForRequest = 15.0f;
        _sessionConfig.timeoutIntervalForResource = 15.0f;
    }
    return _sessionConfig;
}

#pragma mark - Funtion
int textHexColor(NSInteger duration){
    int hexColor;
    switch (netReate(duration)) {
        case NetStatusRate_GREET:
            hexColor = 0xffffff;
            break;
        case NetStatusRate_GOOD:
            hexColor = 0x3CB371;
            break;
        case NetStatusRate_FINE:
            hexColor = 0xDAA520;
            break;
        case NetStatusRate_BAD:
            hexColor = 0xFF6347;
            break;
        default:
            hexColor = 0xFF6347;
            break;
    }
    return hexColor;
}

NetStatusRate netReate(NSInteger duration){
    NetStatusRate netReate;
    if (duration < 250) {
        netReate = NetStatusRate_GREET;
    }else if (duration < 500) {
        netReate = NetStatusRate_GOOD;
    }else if (duration<1000) {
        netReate = NetStatusRate_FINE;
    }else{
        netReate = NetStatusRate_BAD;
    }
    return netReate;
}

NSRange rangOfNetRate(NetStatusRate rate){
    switch (rate) {
        case NetStatusRate_GREET:
            return NSMakeRange(0, 250);
        case NetStatusRate_GOOD:
            return NSMakeRange(250, 500);
        case NetStatusRate_FINE:
            return NSMakeRange(500, 1000);
        default:
            return NSMakeRange(1000, 1000000);
    }
}

NSString *chnStringNetRate(NSInteger duration){
    return [netRateChnStrList() objectAtIndex:netReate(duration)];
}

NSArray <NSString *>*netRateChnStrList(){
    return @[@"优",@"良",@"一般",@"差"];
}


@end

