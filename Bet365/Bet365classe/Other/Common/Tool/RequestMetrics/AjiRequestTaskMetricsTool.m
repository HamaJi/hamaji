//
//  AjiRequestTaskMetricsTool.m
//  
//
//  Created by HHH on 2018/7/18.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "AjiRequestTaskMetricsTool.h"
#import "NSDate+Metrics.h"
#import "NSURLSession+Metrics.h"
#import "NetWorkMannager+Condition.h"
#import "JumpMetricsViewController.h"
#include <arpa/inet.h>
#include <net/if.h>
#include <ifaddrs.h>
#include <net/if_dl.h>
#import "NetWorkMannager+Condition.h"
#import "MetricsEntity+CoreDataClass.h"


@interface AjiRequestTaskMetricsTool()
{
    pthread_mutex_t _metricsListMutex;
}
@property (nonatomic,strong)NSMutableArray *delegateList;
@property (nonatomic,strong)NSMutableArray <NetMetricsModel *>*metricEntityList;

@end
@implementation AjiRequestTaskMetricsTool

+ (instancetype)sharedInstance {
    
    static AjiRequestTaskMetricsTool *instance;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
		instance = [[AjiRequestTaskMetricsTool alloc] init];
        
    });
    
    return instance;
}
#pragma mark - Public
-(instancetype)init{
    if (self = [super init]) {
        
        pthread_mutexattr_t msgAttr;
        pthread_mutexattr_init(&msgAttr);
        pthread_mutexattr_settype(&msgAttr, PTHREAD_MUTEX_DEFAULT);
        pthread_mutex_init(&_metricsListMutex, &msgAttr);
        pthread_mutexattr_destroy(&msgAttr);
        
        @weakify(self);
        [[RACSignal interval:10 onScheduler:[RACScheduler mainThreadScheduler]] subscribeNext:^(NSDate * _Nullable x) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                @strongify(self);
                [self saveEntry];
            });
        }];
    }
    return self;
}

-(void)dealloc{
    pthread_mutex_destroy(&_metricsListMutex);
}

-(void)aji_requestRecordByOberser:(id)obser Task:(NSURLSessionTask*)task Metrics:(NSURLSessionTaskMetrics*)metrics API_AVAILABLE(ios(10.0)){
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NetMetricsModel *NetMetricsModel = [self getMetricsObjByObser:obser Task:task Metrics:metrics];
        if([self.delegateList containsObject:obser]){
            if([obser respondsToSelector:@selector(AJi_URLSessionTask:didFinishCollectingMetrics:)]){
                [obser AJi_URLSessionTask:task didFinishCollectingMetrics:NetMetricsModel];
            }
        }
        if(![self.delegateList containsObject:obser] && NetMetricsModel){
            pthread_mutex_lock(&_metricsListMutex);
            [self.metricEntityList safeAddObject:NetMetricsModel];
            pthread_mutex_unlock(&_metricsListMutex);
        }
    });
    
    
}


-(void)addDelegate:(id)obser{
    if (obser && ![self.delegateList containsObject:obser]) {
        [self.delegateList addObject:obser];
    }
}

-(void)removeDelegate:(id)obser{
    if (obser && [self.delegateList containsObject:obser]) {
        [self.delegateList removeObject:obser];
    }
}

-(void)getMetricEntityList:(void(^)(NSArray <NetMetricsModel *>*list))completed{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSMutableArray <NetMetricsModel *>* list = self.metricEntityList.mutableCopy;
        NSTimeInterval now = [[[NSDate date] offsetHours:-12] timeIntervalSince1970];
        
        NSArray <MetricsEntity *>*entryList = [MetricsEntity MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"(startDate >= %li) AND (duration > 1)",now]];
        [entryList enumerateObjectsUsingBlock:^(MetricsEntity * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NetMetricsModel *model = [MTLManagedObjectAdapter modelOfClass:[NetMetricsModel class] fromManagedObject:obj error:nil];
            [list safeAddObject:model];
        }];
        
        [list sortUsingComparator:^NSComparisonResult(NetMetricsModel*  _Nonnull obj1, NetMetricsModel*  _Nonnull obj2) {
            if (obj1.startDate > obj2.startDate) {
                return NSOrderedAscending;
            }
            return NSOrderedDescending;
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            completed ? completed(list) : nil;
        });
    });
}

-(void)cleanAllMetrics{
    [MetricsEntity MR_truncateAll];
}
#pragma mark - Private



- (NetMetricsModel *)getMetricsObjByObser:(id)obser Task:(NSURLSessionTask*)task Metrics:(NSURLSessionTaskMetrics*)metrics API_AVAILABLE(ios(10.0)) {
    if(!metrics.transactionMetrics.count || !metrics.transactionMetrics.firstObject.response){
        return nil;
    }
    
    NetMetricsModel *metics = [[NetMetricsModel alloc] init];
    metics.obser = [NSString stringWithFormat:@"%s",object_getClassName(obser)];
    metics.urlStr = [[task.currentRequest.URL absoluteString] componentsSeparatedByString:@"?"].firstObject;

    metics.startDate = [metrics.taskInterval.startDate timeIntervalSince1970];
    metics.duration = metrics.taskInterval.duration * 1000;
    metics.endDate = [metrics.taskInterval.endDate timeIntervalSince1970];
    metics.redirectCount = metrics.redirectCount;
//    metics.transactionList = @[].mutableCopy;
//    [metrics.transactionMetrics enumerateObjectsUsingBlock:^(NSURLSessionTaskTransactionMetrics * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//
//        NetMetricsTransactionModel *transactionEntity = [[NetMetricsTransactionModel alloc] init];
//        transactionEntity.fetchStartDate =          [obj.fetchStartDate timeIntervalSince1970];
//        transactionEntity.domainLookupStartDate =   [obj.domainLookupStartDate timeIntervalSince1970];
//        transactionEntity.domainLookupEndDate =     [obj.domainLookupEndDate timeIntervalSince1970];
//        transactionEntity.connectStartDate =        [obj.connectStartDate timeIntervalSince1970];
//        transactionEntity.connectEndDate =          [obj.connectEndDate timeIntervalSince1970];
//        transactionEntity.requestStartDate =        [obj.requestStartDate timeIntervalSince1970];
//        transactionEntity.requestEndDate =          [obj.requestEndDate timeIntervalSince1970];
//        transactionEntity.responseStartDate =       [obj.responseStartDate timeIntervalSince1970];
//        transactionEntity.responseEndDate =         [obj.responseEndDate timeIntervalSince1970];
//        [metics.transactionList safeAddObject:transactionEntity];
//    }];
    return metics;
}

- (void)requestSendError:(NSError *)error{
    
    
    if (NET_DATA_MANAGER.status == PPNetworkStatusUnknown ||
        NET_DATA_MANAGER.status == PPNetworkStatusNotReachable) {
        return;
    }
//    if (error.tokenInvalid ||
//        error.userNotExit ||
//        error.userLimited ||
//        error.valicodeError ||
//        error.chatFailCode ||
//        error.chatNoRoom ||
//        error.init_pwd ||
//        error.needChangePsw) {
//        return;
//    }
    if (error.tokenInvalid) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [NET_DATA_MANAGER requestSendError:error];
        });
    }
    
}

-(void)saveEntry{
    pthread_mutex_lock(&_metricsListMutex);
    NSManagedObjectContext *context = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_defaultContext]];
    NSMutableArray <MetricsEntity *>*entryList = @[].mutableCopy;
    [self.metricEntityList enumerateObjectsUsingBlock:^(NetMetricsModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSError *error;
        MetricsEntity *entry = [MTLManagedObjectAdapter managedObjectFromModel:obj insertingIntoContext:context error:&error];
        [entryList safeAddObject:entry];
    }];
    [context MR_saveToPersistentStoreAndWait];
    [self.metricEntityList removeAllObjects];
    pthread_mutex_unlock(&_metricsListMutex);
}
#pragma mark - GET/SET
-(NSMutableArray*)delegateList{
    if (!_delegateList) {
        _delegateList = [[NSMutableArray alloc] init];
    }
    return _delegateList;
}

-(NSMutableArray<NetMetricsModel *> *)metricEntityList{
    if (!_metricEntityList) {
        _metricEntityList = @[].mutableCopy;
    }
    return _metricEntityList;
}


@end
