//
//  SDWebImageDownloaderOperation+Metrics.h
//  Bet365
//
//  Created by HHH on 2018/7/19.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <SDWebImage/SDWebImageDownloaderOperation.h>
@interface SDWebImageDownloaderOperation (Metrics)
-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didFinishCollectingMetrics:(NSURLSessionTaskMetrics *)metrics;
@end
