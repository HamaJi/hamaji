//
//  SDWebImageDownloader+Metrics.h
//  Bet365
//
//  Created by HHH on 2018/7/19.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "SDWebImageDownloader.h"
#import <SDWebImage/SDWebImageDownloader.h>
@interface SDWebImageDownloader (Metrics)
-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didFinishCollectingMetrics:(NSURLSessionTaskMetrics *)metrics;
@end
