//
//  NSURLSession+Metrics.m
//  FMDB
//
//  Created by HHH on 2018/7/19.
//

#import "NSURLSession+Metrics.h"
#import <objc/objc.h>
#import <objc/runtime.h>
@implementation NSURLSession (Metrics)
+(void)load{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        aji_swizzleClassMethod([self class],
                           @selector(sessionWithConfiguration:delegate:delegateQueue:),
                           @selector(aji_sessionWithConfiguration:delegate:delegateQueue:));
    });
}
+(NSURLSession *)aji_sessionWithConfiguration:(NSURLSessionConfiguration *)configuration
                                     delegate:(nullable id )delegate
                                delegateQueue:(nullable NSOperationQueue *)queue
{
    if (delegate) {
        NSURLDelegateProxy *proxy = [[NSURLDelegateProxy alloc] initWithTarget:delegate];
        NSURLSession *session = [NSURLSession aji_sessionWithConfiguration:configuration delegate:proxy delegateQueue:queue];
        session.delegateProxy = proxy;
        return session;
    }
    return [self aji_sessionWithConfiguration:configuration delegate:delegate delegateQueue:queue];
}

+(NSArray *)getAllMethods
{
    unsigned int methodCount =0;
    Method* methodList = class_copyMethodList([self class],&methodCount);
    NSMutableArray *methodsArray = [NSMutableArray arrayWithCapacity:methodCount];
    
    for(int i=0;i<methodCount;i++)
    {
        Method temp = methodList[i];
        IMP imp = method_getImplementation(temp);
        SEL name_f = method_getName(temp);
        const char* name_s =sel_getName(method_getName(temp));
        int arguments = method_getNumberOfArguments(temp);
        const char* encoding =method_getTypeEncoding(temp);
        NSLog(@"方法名：%@,参数个数：%d,编码方式：%@",[NSString stringWithUTF8String:name_s],
              arguments,
              [NSString stringWithUTF8String:encoding]);
        [methodsArray addObject:[NSString stringWithUTF8String:name_s]];
        
    }
    free(methodList);
    return methodsArray;
}



-(NSURLDelegateProxy *)delegateProxy{
    return objc_getAssociatedObject(self, _cmd);
}

-(void)setDelegateProxy:(NSURLDelegateProxy *)delegateProxy{
    objc_setAssociatedObject(self, @selector(delegateProxy), delegateProxy, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
@end
