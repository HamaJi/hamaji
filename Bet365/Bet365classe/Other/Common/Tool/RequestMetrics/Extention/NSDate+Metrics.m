//
//  NSDate+Metrics.m
//  FMDB
//
//  Created by Deep river on 2018/5/28.
//

#import "NSDate+Metrics.h"

@implementation NSDate (Metrics)
-(long long)getDateTimeTOMilliSeconds

{
    
    NSTimeInterval interval = [self timeIntervalSince1970];
    long long totalMilliseconds = interval*1000 ;
    return totalMilliseconds;
    
}
/**
 获取毫秒时间
 
 @return String时间格式
 */
- (NSString *)aji_stringMsec{
    static NSDateFormatter *formatterMsecDate;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatterMsecDate = [[NSDateFormatter alloc] init];
        [formatterMsecDate setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
        [formatterMsecDate setLocale:[NSLocale currentLocale]];
    });
    return [formatterMsecDate stringFromDate:self];
}
@end
