//
//  NSURLDelegateProxy.m
//  FMDB
//
//  Created by HHH on 2018/7/19.
//

#import "NSURLDelegateProxy.h"

@interface NSURLDelegateProxy()

@property(nonatomic, strong) id proxyTarget;


@end
@implementation NSURLDelegateProxy
#pragma mark - Public
-(instancetype)initWithTarget:(id)target{
    self.proxyTarget = target;
    return self;
}

#pragma mark - override
-(NSMethodSignature *)methodSignatureForSelector:(SEL)sel{
    NSMethodSignature *methodSignature = nil;
    if ( [self.proxyTarget respondsToSelector:sel]) {
        methodSignature = [self.proxyTarget methodSignatureForSelector:sel];
    }
    return methodSignature;
}
-(void)forwardInvocation:(NSInvocation *)invocation{
    SEL sel = [invocation selector];
    if ( [self.proxyTarget respondsToSelector:sel]) {
        [invocation invokeWithTarget:self.proxyTarget];
    }
}

#pragma mark ============== 捕获需求 ===========================
-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didFinishCollectingMetrics:(NSURLSessionTaskMetrics *)metrics{
    [[AjiRequestTaskMetricsTool sharedInstance] aji_requestRecordByOberser:self.proxyTarget
                                                                      Task:task                    Metrics:metrics];
}

@end
