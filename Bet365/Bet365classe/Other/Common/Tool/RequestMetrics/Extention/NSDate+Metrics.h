//
//  NSDate+Metrics.h
//  FMDB
//
//  Created by Deep river on 2018/7/19.
//

#import <Foundation/Foundation.h>

@interface NSDate (Metrics)

-(long long)getDateTimeTOMilliSeconds;
/**
 精确到毫秒级时间
 
 @return String时间格式
 */
- (NSString *)aji_stringMsec;
@end
