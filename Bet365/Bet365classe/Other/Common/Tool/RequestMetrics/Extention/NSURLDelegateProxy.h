//
//  NSURLDelegateProxy.h
//  FMDB
//
//  Created by HHH on 2018/7/19.
//

#import <Foundation/Foundation.h>

@interface NSURLDelegateProxy : NSProxy

-(instancetype)initWithTarget:(id)target;

@end
