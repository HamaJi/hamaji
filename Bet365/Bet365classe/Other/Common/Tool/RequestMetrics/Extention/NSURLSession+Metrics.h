//
//  NSURLSession+Metrics.h
//  FMDB
//
//  Created by HHH on 2018/7/19.
//

#import <Foundation/Foundation.h>
#import "NSURLDelegateProxy.h"

@interface NSURLSession (Metrics)
@property(nonatomic, strong) NSURLDelegateProxy *delegateProxy;

+(NSURLSession *)aji_sessionWithConfiguration:(NSURLSessionConfiguration *)configuration
                                     delegate:(nullable id )delegate
                                delegateQueue:(nullable NSOperationQueue *)queue;

+(NSArray *)getAllMethods;
@end
