//
//  NetWorkIpSuspendView.h
//  Bet365
//
//  Created by 蛤蟆吉 on 2020/1/13.
//  Copyright © 2020 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NetWorkIpSuspendView : UIView

@property (nonatomic)NSString *ip;

@end

NS_ASSUME_NONNULL_END
