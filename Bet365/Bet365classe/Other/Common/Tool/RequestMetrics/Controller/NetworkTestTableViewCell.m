//
//  NetworkTestTableViewCell.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2020/1/13.
//  Copyright © 2020 jesse. All rights reserved.
//

#import "NetworkTestTableViewCell.h"
#import "NetTaskStatistics.h"
@interface NetworkTestTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *titileLabel;

@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentTagLabel;
@property (weak, nonatomic) IBOutlet UILabel *spacingLabel;

@end

@implementation NetworkTestTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.spacingLabel.lineBreakMode = NSLineBreakByWordWrapping | NSLineBreakByCharWrapping;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setTitile:(NSString *)titile{
    self.titileLabel.text = titile;
}
-(void)setDuration:(double)duration{
    if (duration > 0) {
        self.durationLabel.text = [NSString stringWithFormat:@"%.0fms",duration];
    }else{
        self.durationLabel.text = @"网络异常";
    }
    
    self.durationLabel.textColor = COLOR_WITH_HEX(textHexColor(duration));
}

-(void)setCurrentUrl:(BOOL)currentUrl{
    self.currentTagLabel.text = currentUrl ? @"当前" : @"";
}
@end
