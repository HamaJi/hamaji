//
//  NetworkTestViewController.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2020/1/4.
//  Copyright © 2020 jesse. All rights reserved.
//

#import "NetworkTestViewController.h"
#import "NetTaskStatistics.h"
#import "NetCloudSwitchTool.h"
#import "UIViewControllerSerializing.h"
#import "AAChartKit.h"
#import "NetworkTestTableViewCell.h"
#import "NetWorkIpSuspendView.h"
#import "NetWorkMannager+Condition.h"
@interface NetworkTestViewController ()
<AAChartViewEventDelegate,
UITableViewDelegate,
UITableViewDataSource,
NetTaskStatisticsDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UILabel *deviceNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *systemVersionLabel;

@property (weak, nonatomic) IBOutlet UILabel *appBuild;

@property (weak, nonatomic) IBOutlet UILabel *ipLabel;

@property (weak, nonatomic) IBOutlet UILabel *metricsTitileLabel;
@property (weak, nonatomic) IBOutlet UIView *metricsLineContentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *metricsLineConstraintHeight;
@property (weak, nonatomic) IBOutlet UILabel *appNameLabel;

@property (weak, nonatomic) IBOutlet UIView *metricsBubbleContentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *metricsBubbleConstraintHeight;

@property (weak, nonatomic) IBOutlet UIView *campContentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *campConstraintHeight;
@property (weak, nonatomic) IBOutlet UITableView *campTableView;

@property (weak, nonatomic) IBOutlet UIView *appNodeContentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *appNodeConstraintHeight;
@property (weak, nonatomic) IBOutlet UITableView *appNodeTableView;

@property (nonatomic,strong) NetWorkIpSuspendView *ipView;

@property (strong, nonatomic) AAChartView *lineChartView;
@property (strong, nonatomic) AAChartView *bubbleChartView;

#pragma mark - Data
@property (nonatomic,strong) NSArray <NetMetricsModel *>*descStarTimeList;

@property (nonatomic, strong) AAChartModel *aaChartModel;

@property (nonatomic,strong) NSNumber *maxDuration;

@property (nonatomic,assign) double middleDuration;

@property (nonatomic,strong) NSArray <NetRequestCamp *> *requestCampList;

@property (nonatomic,strong) NSMutableArray <NSNumber *> *responseCampList;

@property (nonatomic,assign)NSInteger campIdx;

@property (nonatomic,strong)NSMutableArray <NetRequestNode *>*requestNodeList;

@property (nonatomic,strong) NSMutableArray <NSNumber *> *responseNodeList;

@property (nonatomic,assign)NSInteger nodeIdx;

@property (nonatomic,strong)NSMutableArray <NSURLSessionDataTask*>*taskList;

@end

RouterKey *const kRouterMetrics = @"metrics";

@implementation NetworkTestViewController

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    if ([[NAVI_MANAGER getCurrentVC].navigationController.visibleViewController isKindOfClass:[NetworkTestViewController  class]]) {
        return NO;
    }else if (!IOS_10_LATER){
        [Bet365AlertSheet showChooseAlert:@"请升级至最新iOS系统版本"
                                  Message:@"抱歉，当前系统版本暂时无法支持网络检测"
                                    Items:@[@"确定"]
                                  Handler:^(NSInteger index) {
                                      
                                  }];
        return NO;
    }
    return YES;
}

+(void)load{
    
    [self registerJumpRouterKey:kRouterMetrics toHandle:^(NSDictionary *parameters) {
        NetworkTestViewController *vc = [[NetworkTestViewController alloc] initWithNibName:@"NetworkTestViewController" bundle:nil];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }];
    
    [self registerInstanceRouterKey:kRouterMetrics toHandle:^UIViewController *(NSDictionary *parameters) {
        return [[NetworkTestViewController alloc] initWithNibName:@"NetworkTestViewController" bundle:nil];
        
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [NetTaskStatistics shareInstance].delegate = self;
    
    self.ipView.ip = [Bet365Tool shareInstance].ipStr;
    [self.scrollView addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.width.equalTo(self.scrollView);
    }];
    self.campConstraintHeight.constant = self.requestCampList.count * 30.0;
    self.appNodeConstraintHeight.constant = self.requestNodeList.count * 30.0;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.scrollView.contentSize = CGSizeMake(0, self.contentView.ycz_height);
    });
    self.deviceNameLabel.text = [Bet365Tool getDeviceName];
    self.systemVersionLabel.text = [UIDevice currentDevice].systemVersion;
    self.appBuild.text = [NSString stringWithFormat:@"%li",BET_CONFIG.buildVersion];
    self.ipLabel.text = [Bet365Tool shareInstance].ipStr;
    self.appNameLabel.text = [NSString stringWithFormat:@"【%@】平台域名延迟检测",BET_CONFIG.appName];
    @weakify(self);
    [[AjiRequestTaskMetricsTool sharedInstance] getMetricEntityList:^(NSArray<NetMetricsModel *> *list) {
        @strongify(self);
        self.descStarTimeList = nil;
        if (list.count >= 300) {
            self.descStarTimeList = [[list subarrayWithRange:NSMakeRange(0, 300)] mutableCopy];
        }else{
            self.descStarTimeList = [list mutableCopy];
        }

        self.metricsTitileLabel.text = [NSString stringWithFormat:@"近%li次数据日志",self.descStarTimeList.count];
        NSArray <NetMetricsModel *>*descDurationList = [self.descStarTimeList sortedArrayUsingComparator:^NSComparisonResult(NetMetricsModel *  _Nonnull obj1, NetMetricsModel *  _Nonnull obj2) {
            if (obj1.duration > obj2.duration) {
                return NSOrderedDescending;
            }
            return NSOrderedAscending;
        }];
        self.maxDuration = [NSNumber numberWithInteger:ceil(descDurationList.lastObject.duration)];
//        NSNumber *minXaxis = [NSNumber numberWithInteger:ceil(self.descStarTimeList.lastObject.startDate)];
//        NSNumber *maxXaxis = [NSNumber numberWithInteger:ceil(self.descStarTimeList.firstObject.startDate)];
        if (descDurationList.count <= 1) {
            self.middleDuration = ((NetMetricsModel *)[descDurationList safeObjectAtIndex:0]).duration;
        }else if (descDurationList.count % 2 != 0) {
            self.middleDuration = ((NetMetricsModel *)[descDurationList safeObjectAtIndex:(descDurationList.count + 1) / 2]).duration;
        }else{
            self.middleDuration = (((NetMetricsModel *)[descDurationList safeObjectAtIndex:descDurationList.count / 2]).duration + ((NetMetricsModel *)[descDurationList safeObjectAtIndex:descDurationList.count / 2 - 1]).duration) / 2.0;
        }
        [UIView animateWithDuration:0.2 animations:^{
            self.metricsLineConstraintHeight.constant = 400.0;
            self.metricsBubbleConstraintHeight.constant = 400;
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            self.scrollView.contentSize = CGSizeMake(0, self.contentView.ycz_height);
            [self metricsLineChartDataSouce:descDurationList];
            [self metricsBubbleChartDataSouce:descDurationList];
        }];

    }];
    [self requestCampUrl:^{
        self.nodeIdx = 1;
    }];
    
    [self requestNodeUrl:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [self submit];
        });
    }];
    self.campIdx = 1;
    self.nodeIdx = 0;
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void)metricsLineChartDataSouce:(NSArray <NetMetricsModel *>*)descDurationList{

    NSMutableArray <NSNumber *>*yAxisList = @[].mutableCopy;
    NSDecimalNumberHandler *decimalHandle = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain scale:2 raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:YES];
    [self.descStarTimeList enumerateObjectsUsingBlock:^(NetMetricsModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [yAxisList safeAddObject:[[NSDecimalNumber decimalNumberWithDecimal:[@(obj.duration) decimalValue]] decimalNumberByRoundingAccordingToBehavior:decimalHandle]];
    }];
    
    
    NSMutableArray <NSString *>*xAxisList = @[].mutableCopy;
    [self.descStarTimeList enumerateObjectsUsingBlock:^(NetMetricsModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *mmdd = [[NSDate dateWithTimeIntervalSince1970:obj.startDate] timeInfo];
        
        NSMutableString *urlStr = [obj.urlStr mutableCopy];
        [urlStr deleteString:@"."];
        [urlStr deleteString:@"http://"];
        [urlStr deleteString:@"https://"];
        [urlStr deleteString:[BET_CONFIG.userSettingData.defaultAppID lowercaseStringWithLocale:[NSLocale currentLocale]]];
        [urlStr deleteString:@"tgapp"];
        [urlStr deleteString:@".com"];
        [urlStr deleteString:@"com"];
        [urlStr insertString:[NSString stringWithFormat:@"%@:",mmdd] atIndex:0];
        [xAxisList safeAddObject:urlStr];
    }];
    
    AAChartModel *lineChartModel = AAChartModel.new
    .animationDurationSet(@800)
    .chartTypeSet(AAChartTypeLine)//图表类型
    .zoomTypeSet(AAChartZoomTypeX)
    .titleSet(@"")//图表主标题
    .subtitleSet(@"实时延迟")//图表副标题
    .subtitleFontColorSet(@"#FFFFFF")
    .colorsThemeSet(@[@"#1790FF"])//设置主体颜色数组
    .backgroundColorSet(@"#FFFFFF")
//    .touchEventEnabledSet(true)//支持用户点击事件
    .legendEnabledSet(NO)
    .markerSymbolStyleSet(AAChartSymbolStyleTypeDefault)//设置折线连接点样式为
    .tooltipValueSuffixSet(@"ms")
    .xAxisLabelsEnabledSet(NO)
    .xAxisCrosshairWidthSet(@01.2)//Zero width to disable crosshair by default
    .xAxisCrosshairColorSet(@"#778899")//浅石板灰准星线
    .xAxisCrosshairDashStyleTypeSet(AAChartLineDashStyleTypeSolid)
    .yAxisCrosshairColorSet(@"#778899")
    .yAxisAllowDecimalsSet(NO)
    .yAxisLineWidthSet(@1)//Y轴轴线线宽为0即是隐藏Y轴轴线
    .yAxisTitleSet(@"")//设置 Y 轴标题
    .yAxisGridLineWidthSet(@1)//y轴横向分割线宽度为0(即是隐藏分割线)
    .yAxisMaxSet(self.maxDuration)
    .yAxisMinSet(@0)
    .categoriesSet(xAxisList)//设置 X 轴坐标文字内容
    .yAxisTickIntervalSet(@(ceil([self.maxDuration doubleValue] / 20.0)))
    .seriesSet(@[
                 AASeriesElement.new
                 .nameSet(@"延迟")
                 .dataSet(yAxisList)
                 ]
               )
    .yAxisPlotLinesSet(@[AAPlotLinesElement.new
                         .colorSet(@"#7dffc0")
                         .dashStyleSet(AAChartLineDashStyleTypeLongDashDotDot)//样式：Dash,Dot,Solid等,默认Solid
                         .widthSet(@(2))
                         .valueSet(@(self.middleDuration))
                         .zIndexSet(@(1000)) //层叠,标示线在图表中显示的层叠级别，值越大，显示越向前
                         .labelSet(AALabel.new
                                   .textSet([NSString stringWithFormat:@"中位值:%.0f",self.middleDuration])
                                   .styleSet(AAStyle.new
                                             .colorSet(@"#7dffc0")))]);
    [self.lineChartView aa_drawChartWithChartModel:lineChartModel];
    
    
}

-(void)metricsBubbleChartDataSouce:(NSArray <NetMetricsModel *>*)descDurationList{
    NSMutableDictionary *data = @{}.mutableCopy;
    [descDurationList enumerateObjectsUsingBlock:^(NetMetricsModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSMutableString *urlStr = [obj.urlStr mutableCopy];
        [urlStr deleteString:@"."];
        [urlStr deleteString:@"http://"];
        [urlStr deleteString:@"https://"];
        [urlStr deleteString:[BET_CONFIG.userSettingData.defaultAppID lowercaseStringWithLocale:[NSLocale currentLocale]]];
        [urlStr deleteString:@"tgapp"];
        [urlStr deleteString:@".com"];
        [urlStr deleteString:@"com"];
        NSMutableArray *list = [[NSMutableArray alloc] initWithArray:[data objectForKey:urlStr]];
        [list addObject:obj];
        [data safeSetObject:list forKey:urlStr];
    }];
    NSArray <NSString *>* xAxisList = data.allKeys;
    
    NSMutableArray <AASeriesElement *>*yAxisList = @[].mutableCopy;
    for (int i = 0; i < 4; i++) {
        AASeriesElement * element = AASeriesElement.new;
        switch (i) {
            case 0:
                element.nameSet(@".json");
                break;
            case 1:
                element.nameSet(@".html");
                break;
            case 2:
                element.nameSet(@".png");
                break;
            default:
                element.nameSet(@"接口");
                break;
        }
        [yAxisList addObject:element];
    }
    NSDecimalNumberHandler *decimalHandle = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain scale:2 raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:YES];
    [xAxisList enumerateObjectsUsingBlock:^(NSString * _Nonnull url, NSUInteger idx, BOOL * _Nonnull stop) {
        //同接口网络耗时取中位数
        NSArray <NetMetricsModel *>* models = [(NSArray *)[data objectForKey:url] sortedArrayUsingComparator:^NSComparisonResult(NetMetricsModel *  _Nonnull obj1, NetMetricsModel *  _Nonnull obj2) {
            if (obj1.duration > obj2.duration) {
                return NSOrderedDescending;
            }
            return NSOrderedAscending;
        }];
        double middleDuration = 0.0;
        if (models.count <= 1) {
            middleDuration = ((NetMetricsModel *)[models safeObjectAtIndex:0]).duration;
        }else if (models.count % 2 != 0) {
            middleDuration = ((NetMetricsModel *)[models safeObjectAtIndex:(models.count + 1) / 2]).duration;
        }else{
            middleDuration = (((NetMetricsModel *)[models safeObjectAtIndex:models.count / 2]).duration + ((NetMetricsModel *)[models safeObjectAtIndex:models.count / 2 - 1]).duration) / 2.0;
        }
        NSUInteger yAxisListIndex = 0;
        if ([models.firstObject.urlStr hasSuffix:@".json"]) {
            yAxisListIndex = 0;
        }else if ([models.firstObject.urlStr hasSuffix:@".html"]) {
            yAxisListIndex = 1;
        }else if ([models.firstObject.urlStr hasSuffix:@".png"]) {
            yAxisListIndex = 2;
        }else{
            yAxisListIndex = 3;
        }

        AASeriesElement *element = [yAxisList safeObjectAtIndex:yAxisListIndex];
        NSMutableArray *dataSet = [[NSMutableArray alloc] initWithArray:element.data];
        NSDecimalNumber *duration = [[NSDecimalNumber decimalNumberWithDecimal:[@(middleDuration) decimalValue]] decimalNumberByRoundingAccordingToBehavior:decimalHandle];
        [dataSet addObject:@[@(idx),duration,@(models.count)]];
        element.dataSet(dataSet);
    }];
    
    AAChartModel *bubbleChartModel = AAChartModel.new
    .chartTypeSet(AAChartTypeBubble)
    .titleSet(@"")
    .subtitleSet(@"延迟分布")
    .subtitleFontColorSet(@"#FFFFFF")
    .yAxisTitleSet(@"")
//    .easyGradientColorsSet(true)
    .yAxisGridLineWidthSet(@0)
    .xAxisLabelsEnabledSet(NO)
    .categoriesSet(xAxisList)//设置 X 轴坐标文字内容
    .colorsThemeSet(@[@"#0c9674",@"#1790FF",@"#d11b5f",@"#facd32"])
    .seriesSet(yAxisList)
    .yAxisPlotLinesSet(@[AAPlotLinesElement.new
                         .colorSet(@"#7dffc0")
                         .dashStyleSet(AAChartLineDashStyleTypeLongDashDotDot)//样式：Dash,Dot,Solid等,默认Solid
                         .widthSet(@(2))
                         .valueSet(@(self.middleDuration))
                         .zIndexSet(@(1000)) //层叠,标示线在图表中显示的层叠级别，值越大，显示越向前
                         .labelSet(AALabel.new
                                   .textSet([NSString stringWithFormat:@"中位值:%.0f",self.middleDuration])
                                   .styleSet(AAStyle.new
                                             .colorSet(@"#7dffc0")))]);;
    
    AAOptions *aaOptions = [AAOptionsConstructor configureChartOptionsWithAAChartModel:bubbleChartModel];
    AATooltip *tooltip = aaOptions.tooltip;
    tooltip
    .useHTMLSet(true)
    .headerFormatSet(@"<b>{point.key}<br>")
    .pointFormatSet(@"<b>{point.y}</b>ms&nbsp<b>{point.z}</b>次请求");
    [self.bubbleChartView aa_drawChartWithOptions:aaOptions];
    
}


- (void)requestNodeUrl:(void(^)())completed{
    
    @weakify(self);
    [[RACObserve(self, nodeIdx) ignore:@(0)] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([x integerValue] > self.requestNodeList.count) {
                completed ? completed() : nil;
                return;
            }
            NSURLSessionDataTask *task = [[NetTaskStatistics shareInstance] runTaskByRequestUrlString:self.requestNodeList[[x integerValue] - 1].urlStr];
            [self.taskList addObject:task];
        });
        
        
    }];
}

-(void)requestCampUrl:(void(^)())completed{
    @weakify(self);
    [[RACObserve(self, campIdx) ignore:@(0)] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([x integerValue] > self.requestCampList.count) {
                completed ? completed() : nil;
                return;
            }
            NSURLSessionDataTask *task = [[NetTaskStatistics shareInstance] runTaskByRequestUrlString:self.requestCampList[[x integerValue] - 1].urlStr];
            [self.taskList addObject:task];
        });
        
    }];
    
}

#pragma mark - UITableViewDelegate

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([tableView isEqual:self.appNodeTableView]) {
        return self.responseNodeList.count;
    }else if ([tableView isEqual:self.campTableView]) {
        return self.responseCampList.count;
    }
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NetworkTestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NetworkTestTableViewCell"];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"NetworkTestTableViewCell" owner:self options:nil].firstObject;
    }
    NSString *titile = nil;
    NSNumber *duration = nil;
    if ([tableView isEqual:self.appNodeTableView]) {
        NetRequestNode *node = [self.requestNodeList safeObjectAtIndex:indexPath.row];
        NSMutableString *urlStr = [node.urlStr mutableCopy];
        [urlStr deleteString:@"."];
        [urlStr deleteString:@"http://"];
        [urlStr deleteString:@"https://"];
        [urlStr deleteString:[BET_CONFIG.userSettingData.defaultAppID lowercaseStringWithLocale:[NSLocale currentLocale]]];
        [urlStr deleteString:@"tgapp"];
        [urlStr deleteString:@".com"];
        [urlStr deleteString:@"com"];
        titile = [NSString stringWithFormat:@"节点%li(%@)网络检测",indexPath.row,urlStr];
        duration = [self.responseNodeList safeObjectAtIndex:indexPath.row];
        cell.currentUrl = [node.urlStr isEqualToString:SerVer_Url];
    }else if ([tableView isEqual:self.campTableView]) {
        NetRequestCamp *camp = [self.requestCampList safeObjectAtIndex:indexPath.row];
        titile = [NSString stringWithFormat:@"当前网络检测(%@)",camp.titile];
        duration = [self.responseCampList safeObjectAtIndex:indexPath.row];
    }
    cell.titile = titile;
    cell.duration = [duration doubleValue];
    return cell;
    
}
- (void)aaChartView:(AAChartView *)aaChartView moveOverEventWithMessage:(AAMoveOverEventMessageModel *)message{
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if ([scrollView isEqual:self.scrollView]) {
        if (scrollView.contentOffset.y > CGRectGetMaxY(self.ipLabel.frame)) {
            self.ipView.alpha = 1.0;
        }else{
            self.ipView.alpha = 0.0;
        }
    }
}
#pragma mark - NetTaskStatisticsDelegate
-(void)url:(NSString *)url didFinishCollectingMetrics:(NetMetricsModel *)metrics{
    [self.requestNodeList enumerateObjectsUsingBlock:^(NetRequestNode * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.urlStr isEqualToString:url]) {
            [self.responseNodeList addObject:@(metrics.duration)];
            [self.appNodeTableView reloadData];
            self.nodeIdx ++;
            *stop = YES;
        }
    }];
    [self.requestCampList enumerateObjectsUsingBlock:^(NetRequestCamp * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.urlStr isEqualToString:url]) {
            [self.responseCampList addObject:@(metrics.duration)];
            [self.campTableView reloadData];
            self.campIdx ++;
            *stop = YES;
        }
    }];
}

#pragma mark - GET/SET

-(AAChartView *)lineChartView{
    if (!_lineChartView) {
        _lineChartView = [[AAChartView alloc] init];
        _lineChartView.delegate = self;
        _lineChartView.scrollEnabled = NO;//禁用 AAChartView 滚动效果
        _lineChartView.backgroundColor = [UIColor skinViewScreenColor];
        _lineChartView.isClearBackgroundColor = YES;
        [self.metricsLineContentView addSubview:_lineChartView];
        [_lineChartView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.metricsLineContentView);
        }];
    }
    return _lineChartView;
}

-(AAChartView *)bubbleChartView{
    if (!_bubbleChartView) {
        _bubbleChartView = [[AAChartView alloc] init];
        _bubbleChartView.delegate = self;
        _bubbleChartView.scrollEnabled = NO;//禁用 AAChartView 滚动效果
        _bubbleChartView.backgroundColor = [UIColor skinViewScreenColor];
        _bubbleChartView.isClearBackgroundColor = YES;
        [self.metricsBubbleContentView addSubview:_bubbleChartView];
        [_bubbleChartView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.metricsBubbleContentView);
        }];
    }
    return _bubbleChartView;
}
-(NSArray<NetRequestCamp *> *)requestCampList{
    if (!_requestCampList) {
        _requestCampList = [NetRequestCamp creatCamps];
    }
    return _requestCampList;
}


-(NSMutableArray<NetRequestNode *> *)requestNodeList{
    if (!_requestNodeList) {
        NSMutableDictionary *nodeData = @{}.mutableCopy;
        [NET_CLOUD_SWITCH.urlList enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [nodeData setObject:obj forKey:obj];
        }];
        
        _requestNodeList = [NSMutableArray array];
        [nodeData.allValues enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NetRequestNode *node = [[NetRequestNode alloc] init];
            node.urlStr = obj;
            [_requestNodeList addObject:node];
        }];
        
    }
    return _requestNodeList;
}

-(NSMutableArray <NSURLSessionDataTask*>*)taskList{
    if (!_taskList) {
        _taskList= [NSMutableArray array];
    }
    return _taskList;
}

-(NSMutableArray<NSNumber *> *)responseCampList{
    if (!_responseCampList) {
        _responseCampList = @[].mutableCopy;
    }
    return _responseCampList;
}
-(NSMutableArray<NSNumber *> *)responseNodeList{
    if (!_responseNodeList) {
        _responseNodeList = @[].mutableCopy;
    }
    return _responseNodeList;
}

-(NetWorkIpSuspendView *)ipView{
    if (!_ipView) {
        _ipView = [[NetWorkIpSuspendView alloc] init];
        _ipView.alpha = 0.0;
        [self.view addSubview:_ipView];
        [_ipView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.view).offset(-10);
            make.top.equalTo(self.view).offset(10);
        }];
    }
    return _ipView;
}

#pragma mark - NetRequest
-(void)submit{
    if (self.requestCampList.count > self.responseCampList.count ||
        self.requestNodeList.count > self.responseNodeList.count) {
        return;
    }
    
    NSMutableDictionary *paramers = @{}.mutableCopy;
    [paramers safeSetObject:[Bet365Tool getDeviceName] forKey:@"deviceName"];
    [paramers safeSetObject:[UIDevice currentDevice].systemVersion forKey:@"systemVersion"];
    [paramers safeSetObject:@(BET_CONFIG.buildVersion) forKey:@"build"];
    [paramers safeSetObject:[Bet365Tool shareInstance].ipStr forKey:@"ip"];
    [paramers safeSetObject:SerVer_Url forKey:@"serverUrl"];
    
    NSError *error;
    NSArray *metricsList = [MTLJSONAdapter JSONArrayFromModels:self.descStarTimeList error:&error];
    [paramers safeSetObject:metricsList forKey:@"metricsList"];
    
    NSMutableDictionary *camp = @{}.mutableCopy;
    [self.responseCampList enumerateObjectsUsingBlock:^(NSNumber * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NetRequestCamp *model = [self.requestCampList safeObjectAtIndex:idx];
        [camp safeSetObject:obj forKey:model.urlStr];
    }];
    [paramers safeSetObject:camp forKey:@"campList"];
    
    NSMutableDictionary *node = @{}.mutableCopy;
    [self.responseNodeList enumerateObjectsUsingBlock:^(NSNumber * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NetRequestNode *model = [self.requestNodeList safeObjectAtIndex:idx];
        [node safeSetObject:obj forKey:model.urlStr];
    }];
    [paramers safeSetObject:node forKey:@"nodeList"];
    
    [NET_DATA_MANAGER requestSendMetrics:paramers];
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
