//
//  NetworkTestTableViewCell.h
//  Bet365
//
//  Created by 蛤蟆吉 on 2020/1/13.
//  Copyright © 2020 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NetworkTestTableViewCell : UITableViewCell

@property (nonatomic)NSString *titile;

@property (nonatomic)double duration;

@property (nonatomic)BOOL currentUrl;

@end

NS_ASSUME_NONNULL_END
