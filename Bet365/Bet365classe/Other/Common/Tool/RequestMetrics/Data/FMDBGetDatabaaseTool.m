//
//  Tool_FMDBGetDatabase.m
//
//
//  Created by HHH on 2018/7/10.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "FMDBGetDatabaaseTool.h"

@implementation FMDBGetDatabaaseTool

@synthesize dbQueue = _dbQueue;

static FMDBGetDatabaaseTool *tool_database = nil;

+ (instancetype)shareFMDBGetDatabaseTool{
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        tool_database = [[self alloc] init] ;
    }) ;
    return tool_database;
}
- (id)copyWithZone:(struct _NSZone *)zone{
    return [FMDBGetDatabaaseTool shareFMDBGetDatabaseTool];
}

+ (NSString *)dbPath{
    NSString *docsdir = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSFileManager *filemanage = [NSFileManager defaultManager];
    docsdir = [docsdir stringByAppendingPathComponent:@"AJIMDB"];
    BOOL isDir;
    BOOL exit =[filemanage fileExistsAtPath:docsdir isDirectory:&isDir];
    if (!exit || !isDir) {
        [filemanage createDirectoryAtPath:docsdir withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSString *dbpath = [docsdir stringByAppendingPathComponent:@"ajimdb.sqlite"];
    return dbpath;
}

- (FMDatabaseQueue *)dbQueue{
    if (_dbQueue == nil) {
        _dbQueue = [[FMDatabaseQueue alloc] initWithPath:[self.class dbPath]];
    }
    return _dbQueue;
}
@end
