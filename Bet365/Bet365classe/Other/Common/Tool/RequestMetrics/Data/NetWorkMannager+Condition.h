//
//  NetWorkMannager+StatisticsError.h
//  Bet365
//
//  Created by HHH on 2018/7/27.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NetWorkMannager.h"

@interface NetWorkMannager (Condition)

- (void)requestSendMetrics:(NSDictionary *)note;

/**
 网络错误上传

 @param error NSError
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (void)requestSendError:(NSError *)error;
@end
