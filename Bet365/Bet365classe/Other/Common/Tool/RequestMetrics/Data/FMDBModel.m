//
//  FMDBModel.m
//
//
//  Created by HHH on 2018/7/10.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "FMDBModel.h"
#import "FMDatabaseAdditions.h"
#import "FMDBGetDatabaaseTool.h"
#import <objc/runtime.h>

@implementation FMDBModel
/*
 c char         C unsigned char
 i int          I unsigned int
 l long         L unsigned long
 s short        S unsigned short
 d double       D unsigned double
 f float        F unsigned float
 q long long    Q unsigned long long
 B BOOL
 @ 对象类型 //指针 对象类型 如NSString 是@“NSString”
 
 
 64位下long 和long long 都是Tq
 SQLite 默认支持五种数据类型TEXT、INTEGER、REAL、BLOB、NULL
 */


+ (void)initialize{
    if (self != [FMDBModel self]) {
        [self createTable];
    }
}

- (instancetype)init{
    self = [super init];
    if (self) {
        NSDictionary *dic = [self.class getAllProperties];
        _columeNames = [[NSMutableArray alloc] initWithArray:[dic objectForKey:@"name"]];
        _columeTypes = [[NSMutableArray alloc] initWithArray:[dic objectForKey:@"type"]];
    }
    
    return self;
}

+ (BOOL)createTable{
    FMDatabase *db = [FMDatabase databaseWithPath:[FMDBGetDatabaaseTool dbPath]];
    if (![db open]) {
        NSLog(@"数据库打开失败!");
        return NO;
    }
    
    NSString *tableName = NSStringFromClass(self.class);
    NSString *columeAndType = [self.class getColumeAndTypeString];

    //** key*/
    NSString *sql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(%@);",tableName,columeAndType];
    if (![db executeUpdate:sql]) {
        return NO;
    }
    
    NSMutableArray *columns = [NSMutableArray array];
    FMResultSet *resultSet = [db getTableSchema:tableName];
    while ([resultSet next]) {
        //** value*/
        NSString *column = [resultSet stringForColumn:@"name"];
        [columns addObject:column];
    }
    NSDictionary *dict = [self.class getAllProperties];
    NSArray *properties = [dict objectForKey:@"name"];
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"NOT (SELF IN %@)",columns];
    NSArray *resultArray = [properties filteredArrayUsingPredicate:filterPredicate];
    
    for (NSString *column in resultArray) {
        NSUInteger index = [properties indexOfObject:column];
        NSString *proType = [[dict objectForKey:@"type"] objectAtIndex:index];
        NSString *fieldSql = [NSString stringWithFormat:@"%@ %@",column,proType];
        NSString *sql = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN %@ ",NSStringFromClass(self.class),fieldSql];
        if (![db executeUpdate:sql]) {
            return NO;
        }
    }
    [db close];
    return YES;
}

/**
 *  获取该类的所有属性以及属性对应的类型,并且存入字典中
 */
+ (NSDictionary *)getPropertys{
    NSMutableArray *proNames = [NSMutableArray array];
    NSMutableArray *proTypes = [NSMutableArray array];
    NSArray *theTransients = [[self class] transients];
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList([self class], &outCount);
    for (i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        NSString *propertyName = [NSString stringWithCString:property_getName(property) encoding:NSUTF8StringEncoding];
        if ([theTransients containsObject:propertyName]) {
            continue;
        }
        [proNames addObject:propertyName];
        NSString *propertyType = [NSString stringWithCString: property_getAttributes(property) encoding:NSUTF8StringEncoding];
        
        if ([propertyType hasPrefix:@"T@\"NSArray\""]) {//属性类型是数组
            [proTypes addObject:SQLBLOB];
        }else if ([propertyType hasPrefix:@"T@\"NSString\""]){//@:字符串
            [proTypes addObject:SQLTEXT];
        }else if ([propertyType hasPrefix:@"T@"]){//以T@开头的类型，除了数组，字符串，也就只剩下模型类型了
            [proTypes addObject:SQLMODEL];
        }else if ([propertyType hasPrefix:@"Ti"]||[propertyType hasPrefix:@"TI"]||[propertyType hasPrefix:@"Ts"]||[propertyType hasPrefix:@"TS"]||[propertyType hasPrefix:@"TB"]) {//i,I(integer):整形； s(short):短整形； B(BOOL):布尔；
            [proTypes addObject:SQLINTEGER];
        } else {
            [proTypes addObject:SQLREAL];
        }
        
    }
    free(properties);
    
    return [NSDictionary dictionaryWithObjectsAndKeys:proNames,@"name",proTypes,@"type",nil];
}

#pragma mark - override
/** 如果子类中有一些property不需要创建数据库字段，那么这个方法必须在子类中重写
 */
+ (NSArray *)transients{
    return [NSArray array];
}

//将属性名与属性类型拼接成sqlite语句：integer a,real b,...
+ (NSString *)getColumeAndTypeString{
    NSMutableString* pars = [NSMutableString string];
    NSDictionary *dict = [self.class getAllProperties];
    
    NSMutableArray *proNames = [dict objectForKey:@"name"];
    NSMutableArray *proTypes = [dict objectForKey:@"type"];
    
    for (int i=0; i< proNames.count; i++) {
        [pars appendFormat:@"%@ %@",[proNames objectAtIndex:i],[proTypes objectAtIndex:i]];
        if(i+1 != proNames.count)
        {
            [pars appendString:@","];
        }
    }
    return pars;
}

/** 获取模型中的所有属性，并且添加一个主键字段pk。这些数据都存入一个字典中 */
+ (NSDictionary *)getAllProperties{
    NSDictionary *dict = [self.class getPropertys];
    
    NSMutableArray *proNames = [NSMutableArray array];
    NSMutableArray *proTypes = [NSMutableArray array];
    [proNames addObject:primaryId];
    [proTypes addObject:[NSString stringWithFormat:@"%@ %@",SQLINTEGER,PrimaryKey]];
    [proNames addObjectsFromArray:[dict objectForKey:@"name"]];
    [proTypes addObjectsFromArray:[dict objectForKey:@"type"]];
    
    return [NSDictionary dictionaryWithObjectsAndKeys:proNames,@"name",proTypes,@"type",nil];
}



#pragma mark -
/**
 保存数据

 @return BOOL
 */
- (BOOL)save{
    NSString *tableName = NSStringFromClass(self.class);
    NSMutableString *keyString = [NSMutableString string];
    NSMutableString *valueString = [NSMutableString string];
    NSMutableArray *insertValues = [NSMutableArray  array];
    
    NSDictionary *dict = [self.class getAllProperties];
    NSMutableArray *proTypes = [dict objectForKey:@"type"];
    
    for (int i = 0; i < self.columeNames.count; i++) {
        NSString *proname = [self.columeNames objectAtIndex:i];
        //如果是主键，不处理
        if ([proname isEqualToString:primaryId]) {
            continue;
        }
        [keyString appendFormat:@"%@,", proname];
        [valueString appendString:@"?,"];
        id value = nil;
        if ([proTypes[i] isEqualToString:SQLBLOB]) {//数组存储前先反序列化为二进制数据
            NSArray *array = [self valueForKey:proname];
            value = [NSKeyedArchiver archivedDataWithRootObject:array];
        }else if([proTypes[i] isEqualToString:SQLMODEL]){//模型存储前先反序列化为二进制数据
            id model = [self valueForKey:proname];
            value = [NSKeyedArchiver archivedDataWithRootObject:model];
        }else {
            value = [self valueForKey:proname];
        }
        
        //属性值可能为空
        if (!value) {
            value = @"";
        }
        [insertValues addObject:value];
    }
    
    //删除最后的那个","
    [keyString deleteCharactersInRange:NSMakeRange(keyString.length - 1, 1)];
    [valueString deleteCharactersInRange:NSMakeRange(valueString.length - 1, 1)];
    
    FMDBGetDatabaaseTool *ykDB = [FMDBGetDatabaaseTool shareFMDBGetDatabaseTool];
    __block BOOL res = NO;
    [ykDB.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *sql = [NSString stringWithFormat:@"INSERT INTO %@(%@) VALUES (%@);", tableName, keyString, valueString];
        res = [db executeUpdate:sql withArgumentsInArray:insertValues];
        //获取数据库最后一个行的主键id
        self.pk = res?[NSNumber numberWithLongLong:db.lastInsertRowId].intValue:0;
        NSLog(res?@"插入成功":@"插入失败");
    }];
    return res;
}
#pragma mark - 应用事务来保存数据
/** 应用事务批量保存用户对象 */
+ (BOOL)saveObjects:(NSArray *)array
{
    for (FMDBModel *model in array) {
        if (![model isKindOfClass:[FMDBModel class]]) {
            return NO;
        }
    }
    
    __block BOOL res = YES;
    FMDBGetDatabaaseTool *jkDB = [FMDBGetDatabaaseTool shareFMDBGetDatabaseTool];
    [jkDB.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        for (FMDBModel *model in array) {
            NSString *tableName = NSStringFromClass(model.class);
            NSMutableString *keyString = [NSMutableString string];
            NSMutableString *valueString = [NSMutableString string];
            NSMutableArray *insertValues = [NSMutableArray  array];
            for (int i = 0; i < model.columeNames.count; i++) {
                NSString *proname = [model.columeNames objectAtIndex:i];
                if ([proname isEqualToString:primaryId]) {
                    continue;
                }
                [keyString appendFormat:@"%@,", proname];
                [valueString appendString:@"?,"];
                id value = [model valueForKey:proname];
                if (!value) {
                    value = @"";
                }
                [insertValues addObject:value];
            }
            [keyString deleteCharactersInRange:NSMakeRange(keyString.length - 1, 1)];
            [valueString deleteCharactersInRange:NSMakeRange(valueString.length - 1, 1)];
            
            NSString *sql = [NSString stringWithFormat:@"INSERT INTO %@(%@) VALUES (%@);", tableName, keyString, valueString];
            BOOL flag = [db executeUpdate:sql withArgumentsInArray:insertValues];
            model.pk = flag?[NSNumber numberWithLongLong:db.lastInsertRowId].intValue:0;
            if (!flag) {
                res = NO;
                *rollback = YES;
                return;
            }
        }
    }];
    return res;
}
#pragma mark - 删除数据.删除数据需要的资源：表名NSStringFromClass(self.class)，条件语句WHERE pk < 10

/**
 通过条件删除数据

 @param criteria SQL
 @return BOOL
 */
+ (BOOL)deleteObjectsByCriteria:(NSString *)criteria
{
    FMDBGetDatabaaseTool *jkDB = [FMDBGetDatabaaseTool shareFMDBGetDatabaseTool];
    __block BOOL res = NO;
    [jkDB.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *tableName = NSStringFromClass(self.class);
        NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ %@ ",tableName,criteria];
        res = [db executeUpdate:sql];
        NSLog(res?@"删除成功":@"删除失败");
    }];
    return res;
}

#pragma mark - 更新数据

//@"UPDATE %@ SET %@ WHERE %@ = ?;", tableName（表名）, keyString（属性名）([@" %@=?,", proname]?是占位符，每个没有赋值的字段都必须放一个占位符), primaryId（条件）；
//             [db executeUpdate:sql withArgumentsInArray:updateValues（数组中存储的是所有属性的值）] 高斐斐TODO;
- (BOOL)update
{
    FMDBGetDatabaaseTool *jkDB = [FMDBGetDatabaaseTool shareFMDBGetDatabaseTool];
    __block BOOL res = NO;
    [jkDB.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *tableName = NSStringFromClass(self.class);
        id primaryValue = [self valueForKey:primaryId];
        if (!primaryValue || primaryValue <= 0) {
            return ;
        }
        NSMutableString *keyString = [NSMutableString string];
        NSMutableArray *updateValues = [NSMutableArray  array];
        for (int i = 0; i < self.columeNames.count; i++) {
            NSString *proname = [self.columeNames objectAtIndex:i];
            if ([proname isEqualToString:primaryId]) {
                continue;
            }
            [keyString appendFormat:@" %@=?,", proname];
            id value = [self valueForKey:proname];
            if (!value) {
                value = @"";
            }
            [updateValues addObject:value];
        }
        [keyString deleteCharactersInRange:NSMakeRange(keyString.length - 1, 1)];
        NSString *sql = [NSString stringWithFormat:@"UPDATE %@ SET %@ WHERE %@ = ?;", tableName, keyString, primaryId];
        [updateValues addObject:primaryValue];
        res = [db executeUpdate:sql withArgumentsInArray:updateValues];
        NSLog(res?@"更新成功":@"更新失败");
    }];
    return res;
}


/**
 批量更新用户对象，主键对应

 @param array model集合
 @return BOOL
 */
+ (BOOL)updateObjects:(NSArray *)array
{
    for (FMDBModel *model in array) {
        if (![model isKindOfClass:[FMDBModel class]]) {
            return NO;
        }
    }
    __block BOOL res = YES;
    FMDBGetDatabaaseTool *jkDB = [FMDBGetDatabaaseTool shareFMDBGetDatabaseTool];
    [jkDB.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        for (FMDBModel *model in array) {
            NSString *tableName = NSStringFromClass(model.class);
            id primaryValue = [model valueForKey:primaryId];
            if (!primaryValue || primaryValue <= 0) {
                res = NO;
                *rollback = YES;
                return;
            }
            
            NSMutableString *keyString = [NSMutableString string];
            NSMutableArray *updateValues = [NSMutableArray  array];
            for (int i = 0; i < model.columeNames.count; i++) {
                NSString *proname = [model.columeNames objectAtIndex:i];
                if ([proname isEqualToString:primaryId]) {
                    continue;
                }
                [keyString appendFormat:@" %@=?,", proname];
                id value = [model valueForKey:proname];
                if (!value) {
                    value = @"";
                }
                [updateValues addObject:value];
            }
            
            [keyString deleteCharactersInRange:NSMakeRange(keyString.length - 1, 1)];
            NSString *sql = [NSString stringWithFormat:@"UPDATE %@ SET %@ WHERE %@=?;", tableName, keyString, primaryId];
            [updateValues addObject:primaryValue];
            BOOL flag = [db executeUpdate:sql withArgumentsInArray:updateValues];
            NSLog(flag?@"更新成功":@"更新失败");
            if (!flag) {
                res = NO;
                *rollback = YES;
                return;
            }
        }
    }];
    
    return res;
}


/**
 条件查询

 @param criteria sql语句
 @param max 至多
 @return NSArray
 */
+ (NSArray *)findByCriteria:(NSString *)criteria Max:(NSUInteger)max
{
    NSMutableArray *results = [[NSMutableArray alloc] initWithArray:[self.class findByCriteria:criteria]];
    if (results.count <= max) {
        return results;
    }else{
        [results removeObjectsInRange:NSMakeRange(0, results.count - max)];
        return results;
    }
    
    return results;
}

/**
 条件查询

 @param criteria sql语句
 @return Model集合
 */
+ (NSArray *)findByCriteria:(NSString *)criteria
{
    FMDBGetDatabaaseTool *jkDB = [FMDBGetDatabaaseTool shareFMDBGetDatabaseTool];
    NSMutableArray *users = [NSMutableArray array];
    [jkDB.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *tableName = NSStringFromClass(self.class);
        NSString *sql = criteria.length ? [NSString stringWithFormat:@"SELECT * FROM %@ %@ ",tableName,criteria] : [NSString stringWithFormat:@"SELECT * FROM %@",tableName];
        FMResultSet *resultSet = [db executeQuery:sql];

        while ([resultSet next]) {
            FMDBModel *model = [[self.class alloc] init];
            for (int i=0; i< model.columeNames.count; i++) {
                NSString *columeName = [model.columeNames objectAtIndex:i];
                NSString *columeType = [model.columeTypes objectAtIndex:i];
                if ([columeType isEqualToString:SQLTEXT]) {
                    [model setValue:[resultSet stringForColumn:columeName] forKey:columeName];
                } else {
                    [model setValue:[NSNumber numberWithLongLong:[resultSet longLongIntForColumn:columeName]] forKey:columeName];
                }
            }
            [users addObject:model];
            FMDBRelease(model);
        }
    }];
    
    return users;
}

+ (NSArray *)findAll
{
    FMDBGetDatabaaseTool *jkDB = [FMDBGetDatabaaseTool shareFMDBGetDatabaseTool];
    NSMutableArray *users = [NSMutableArray array];
    [jkDB.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *tableName = NSStringFromClass(self.class);
        NSString *sql = [NSString stringWithFormat:@"SELECT * FROM %@",tableName];
        FMResultSet *resultSet = [db executeQuery:sql];
        while ([resultSet next]) {
            FMDBModel *model = [[self.class alloc] init];
            for (int i=0; i< model.columeNames.count; i++) {
                NSString *columeName = [model.columeNames objectAtIndex:i];
                NSString *columeType = [model.columeTypes objectAtIndex:i];
                if ([columeType isEqualToString:SQLBLOB]) {
                    NSData *data = [resultSet dataForColumn:columeName];
                    NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                    [model setValue:array forKey:columeName];
                }else if ([columeType isEqualToString:SQLMODEL]){
                    NSData *data = [resultSet dataForColumn:columeName];
                    id mo = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                    [model setValue:mo forKey:columeName];
                }else if ([columeType isEqualToString:SQLTEXT]) {
                    [model setValue:[resultSet stringForColumn:columeName] forKey:columeName];
                } else {
                    [model setValue:[NSNumber numberWithLongLong:[resultSet longLongIntForColumn:columeName]] forKey:columeName];
                }
            }
            [users addObject:model];
            FMDBRelease(model);
        }
    }];
    
    return users;
}
- (NSString *)description
{
    NSString *result = @"";
    NSDictionary *dict = [self.class getAllProperties];
    NSMutableArray *proNames = [dict objectForKey:@"name"];
    for (int i = 0; i < proNames.count; i++) {
        NSString *proName = [proNames objectAtIndex:i];
        id  proValue = [self valueForKey:proName];
        result = [result stringByAppendingFormat:@"%@:%@\n",proName,proValue];
    }
    return result;
}

/**
 数据库中是否存在表

 @return BOOL
 */
+ (BOOL)isExistInTable
{
    __block BOOL res = NO;
    FMDBGetDatabaaseTool *jkDB = [FMDBGetDatabaaseTool shareFMDBGetDatabaseTool];
    [jkDB.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *tableName = NSStringFromClass(self.class);
        res = [db tableExists:tableName];
    }];
    return res;
}

/**
 取出数据库中，指定表名的所有字段名称

 @return NSArray
 */
+ (NSArray *)getColumns
{
    FMDBGetDatabaaseTool *jkDB = [FMDBGetDatabaaseTool shareFMDBGetDatabaseTool];
    NSMutableArray *columns = [NSMutableArray array];
    [jkDB.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *tableName = NSStringFromClass(self.class);
        FMResultSet *resultSet = [db getTableSchema:tableName];
        while ([resultSet next]) {
            NSString *column = [resultSet stringForColumn:@"name"];
            [columns addObject:column];
        }
    }];
    return [columns copy];
}

+ (BOOL)clearTable
{
    FMDBGetDatabaaseTool *jkDB = [FMDBGetDatabaaseTool shareFMDBGetDatabaseTool];
    __block BOOL res = NO;
    [jkDB.dbQueue inDatabase:^(FMDatabase *db) {
        NSString *tableName = NSStringFromClass(self.class);
        NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@",tableName];
        res = [db executeUpdate:sql];
        NSLog(res?@"清空成功":@"清空失败");
    }];
    return res;
}
@end
