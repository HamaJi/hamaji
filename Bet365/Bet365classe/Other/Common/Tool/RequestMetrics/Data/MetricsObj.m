//
//  MetricsObj.m
//  TYKit
//
//  Created by adnin on 2018/7/10.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "MetricsObj.h"
#import "FMDBGetDatabaaseTool.h"
@implementation MetricsObj
+ (NSArray *)findByCriteria:(NSString *)criteria{
    
    NSArray *arr = [super findByCriteria:criteria];
    [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MetricsObj *object = (MetricsObj*)obj;
        object.transactionMetric = [TransactionMetricObj findByCriteria:[NSString stringWithFormat:@"WHERE supPk = %d",object.pk]];
    }];
    return arr;
}
+ (NSArray *)findAll{
    NSArray *arr = [super findAll];
    [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MetricsObj *object = (MetricsObj*)obj;
        object.transactionMetric = [TransactionMetricObj findByCriteria:[NSString stringWithFormat:@"WHERE supPk = %d",object.pk]];
    }];
    return arr;
}
+(NSArray *)findByCriteria:(NSString *)criteria Max:(NSUInteger)max{
    NSArray *arr = [super findByCriteria:criteria Max:max];
    [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MetricsObj *object = (MetricsObj*)obj;
        object.transactionMetric = [TransactionMetricObj findByCriteria:[NSString stringWithFormat:@"WHERE supPk = %d",object.pk]];
    }];
    return arr;
}

+ (BOOL)saveObjects:(NSArray <MetricsObj*>*)array{
    [array enumerateObjectsUsingBlock:^(MetricsObj * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.transactionMetric = nil;
    }];
    return [super saveObjects:array];
}
-(BOOL)save{
    self.transactionMetric = nil;
    return [super save];
}
@end
