//
//  NetWorkMannager+StatisticsError.m
//  Bet365
//
//  Created by HHH on 2018/7/27.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NetWorkMannager+Condition.h"
#import "RequestUrlHeader.h"
#import "NSDate+Metrics.h"
#import "JumpMetricsViewController.h"

#define NET_ERROR_MAXCOUNT 15
#define NET_ERROR_MAXSPACINGTIME 120000

@interface NSError (time)

@property (nonatomic)NSDate *date;

@end

@implementation NSError (time)

-(NSDate *)date{
    return objc_getAssociatedObject(self, _cmd);
}
-(void)setDate:(NSDate *)date{
    objc_setAssociatedObject(self, @selector(date), date, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end

@implementation NetWorkMannager (Condition)



- (void)requestSendMetrics:(NSDictionary *)note{
    
    AFHTTPSessionManager *manager = [self createAFHTTPSessionManagerByRequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON];
    NSMutableDictionary *paramers = @{}.mutableCopy;
    [paramers safeSetObject:[NSString stringWithFormat:@"%@_NETTEST",BET_CONFIG.userSettingData.defaultAppID] forKey:@"code"];
    [paramers safeSetObject:[note mj_JSONString] forKey:@"log"];
    [paramers safeSetObject:[NSString stringWithFormat:@"%li",BET_CONFIG.buildVersion] forKey:@"version"];
    [manager POST:LOG_SEND_URL parameters:paramers progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [METRICS_TOOL cleanAllMetrics];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
- (void)requestSendError:(NSError *)error{
    [self quantitativeRequestSendError:error];
}
- (NSURLSessionTask *)quantitativeRequestSendError:(NSError *)error{
    
    
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:deviceName() forKey:@"deviceName"];
    [parameters setObject:currentDevice() forKey:@"currentDevice"];
    [parameters setObject:networkIPAddress() ? networkIPAddress() : @"" forKey:@"networkIPAddress"];
    if (error.tokenInvalid) {
        [parameters safeSetObject:USER_DATA_MANAGER.userInfoData.token forKey:@"token"];
    }
    NSMutableDictionary *errorData = error.userInfo.mutableCopy;
    [[error.userInfo allKeys] enumerateObjectsUsingBlock:^(NSErrorUserInfoKey  _Nonnull key, NSUInteger idx, BOOL * _Nonnull stop) {
        if (![[error.userInfo objectForKey:key] isKindOfClass:[NSString class]]) {
            [errorData removeObjectForKey:key];
        }
    }];
    [parameters setObject:[errorData jk_JSONString] forKey:@"errorInfo"];
    
    AFHTTPSessionManager *manager = [self createAFHTTPSessionManagerByRequestType:PPSerializerType_HTTP
                                                                     ResponseType:PPSerializerType_JSON];
    return [manager POST:LOG_SEND_URL
              parameters:@{@"code":[NSString stringWithFormat:@"%@_ERROR",BET_CONFIG.userSettingData.defaultAppID],
                           @"log":[parameters jk_JSONString],
                           @"version":[NSString stringWithFormat:@"%li",BET_CONFIG.buildVersion]}
                progress:nil
                 success:nil
                 failure:nil];
}

NSString *timeString(NSInteger timeStamp){
    return [[NSDate dateWithTimeIntervalSince1970:timeStamp] aji_stringMsec];
}
NSString *deviceName(){
    NSString *deviceName = [Bet365Tool getDeviceName];
    return deviceName.length ? deviceName : @"未知";
}

NSString *currentDevice(){
    return [UIDevice currentDevice].systemVersion;
}

NSString *networkIPAddress(){
    return [Bet365Tool shareInstance].ipStr;
}

@end
