//
//  TransactionMetricObj.h
//  
//
//  Created by adnin on 2018/7/10.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "FMDBModel.h"

@interface TransactionMetricObj : FMDBModel

//
@property (nonatomic, assign) int supPk;



/**
 任务开始从服务器或本地获取资源的时间
 */
@property (assign, nonatomic) NSInteger  fetchStartDate;

/**
 DNS 解析开始时间
 */
@property (assign, nonatomic) NSInteger  domainLookupStartDate;

/**
 DNS 解析完成时间
 */
@property (assign, nonatomic) NSInteger  domainLookupEndDate;

/**
 客户端与服务器建立 TCP 连接时间
 */
@property (assign, nonatomic) NSInteger  connectStartDate;

/**
 任务完成后立即建立与服务器的连接
 */
@property (assign, nonatomic) NSInteger  connectEndDate;

/**
 无论是从服务器还是从本地资源中检索，紧接在任务开始请求资源之前的时间
 */
@property (assign, nonatomic) NSInteger  requestStartDate;

/**
 任务完成后请求资源的时间，无论是从服务器还是从本地资源中检索
 */
@property (assign, nonatomic) NSInteger  requestEndDate;

/**
 紧接任务接收到服务器或本地资源响应的第一个字节后的时间
 */
@property (assign, nonatomic) NSInteger  responseStartDate;

/**
 任务接收到资源的最后一个字节后的时间
 */
@property (assign, nonatomic) NSInteger  responseEndDate;


+ (NSArray *)findByCriteria:(NSString *)criteria AtSuperPrimayKey:(int)primayKey;
-(BOOL)saveBySuperPrimayKey:(int)primayKey;
@end
