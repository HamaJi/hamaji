//
//  TransactionMetricObj.m
//
//
//  Created by adnin on 2018/7/10.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "TransactionMetricObj.h"
#import "FMDBGetDatabaaseTool.h"
@implementation TransactionMetricObj
+ (NSArray *)findByCriteria:(NSString *)criteria AtSuperPrimayKey:(int)primayKey{

    NSString *sql = primayKey ? [NSString stringWithFormat:@"%@ AND supPk='%d'",criteria,primayKey] : criteria;
    return [super findByCriteria:sql];
}
-(BOOL)saveBySuperPrimayKey:(int)primayKey{
    self.supPk = primayKey;
    return [super save];
}

@end
