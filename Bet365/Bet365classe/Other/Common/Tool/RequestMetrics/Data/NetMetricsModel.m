//
//  NetMetricsModel.m
//  Bet365
//
//  Created by adnin on 2019/5/11.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "NetMetricsModel.h"

@implementation NetMetricsModel


+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"duration" : @"duration",
             @"endDate" : @"endDate",
             @"obser" : @"obser",
             @"redirectCount" : @"redirectCount",
             @"startDate" : @"startDate",
             @"urlStr" : @"urlStr",
             };
}

+ (NSDictionary *)managedObjectKeysByPropertyKey {
    
    return @{@"duration" : @"duration",
             @"endDate" : @"endDate",
             @"obser" : @"obser",
             @"redirectCount" : @"redirectCount",
             @"startDate" : @"startDate",
             @"urlStr" : @"urlStr",
             };
}

+ (NSString *)managedObjectEntityName {
    return @"MetricsEntity";
}

//+ (NSSet *)propertyKeysForManagedObjectUniquing{
//    return [NSSet setWithObject:@"userId"];
//}
@end
