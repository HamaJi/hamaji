//
//  MetricsObj.h
//  TYKit
//
//  Created by adnin on 2018/7/10.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "FMDBModel.h"
#import "TransactionMetricObj.h"
@interface MetricsObj : FMDBModel

/**
 发起者
 */
@property (nonatomic,strong)NSString *obser;

/**
 请求地址
 */
@property (nonatomic,strong)NSString *urlStr;

/**
 请求发起时间
 */
@property (assign, nonatomic) NSInteger startDate;

/**
 请求周期
 */
@property (nonatomic,assign)NSInteger duration;

/**
 请求结束时间
 */
@property (assign, nonatomic) NSInteger  endDate;

/**
 重定向次数
 */
@property (assign, nonatomic) NSUInteger redirectCount;


/**
 细则(派生子类)
 */
@property (nonatomic , strong) NSArray<TransactionMetricObj *> * transactionMetric;


+ (NSArray *)findByCriteria:(NSString *)criteria;
+ (NSArray *)findAll;
+ (BOOL)saveObjects:(NSArray <MetricsObj*>*)array;
-(BOOL)save;
@end
