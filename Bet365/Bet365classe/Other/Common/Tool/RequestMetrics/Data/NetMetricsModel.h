//
//  NetMetricsModel.h
//  Bet365
//
//  Created by adnin on 2019/5/11.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "MTLModel.h"
#import "NetMetricsTransactionModel.h"
#import <MTLManagedObjectAdapter/MTLManagedObjectAdapter.h>
NS_ASSUME_NONNULL_BEGIN

@interface NetMetricsModel : MTLModel<MTLManagedObjectSerializing,MTLJSONSerializing>

@property (nonatomic,assign) double duration;
@property (nonatomic,assign) NSInteger endDate;
@property (nullable, nonatomic, copy) NSString *obser;
@property (nonatomic,assign) NSInteger redirectCount;
@property (nonatomic,assign) NSInteger startDate;
@property (nullable, nonatomic, copy) NSString *urlStr;
//@property (nullable, nonatomic, strong) NSMutableArray<NetMetricsTransactionModel *> *transactionList;

@end

NS_ASSUME_NONNULL_END
