//
//  NetRequestCamp.m
//  Bet365
//
//  Created by HHH on 2018/7/14.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NetRequestCamp.h"

@implementation NetRequestCamp
+(NSArray <NetRequestCamp *>*)creatCamps{
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *path = [bundle pathForResource:@"NetTaskUrls" ofType:@"plist"];
    NSArray <NSDictionary *>*datas = [NSArray arrayWithContentsOfFile:path];
    NSMutableArray *camps = [NSMutableArray array];
    [datas enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NetRequestCamp *camp = [[NetRequestCamp alloc] initWithTitile:obj[@"titile"] UrlStr:obj[@"urlStr"]];
        [camps addObject:camp];
    }];
    if (CHAT_UTIL.clientInfo) {
        NetRequestCamp *camp = [[NetRequestCamp alloc] initWithTitile:@"聊天室节点" UrlStr:CHAT_UTIL.clientInfo.chatUrl];
        [camps addObject:camp];
    }
    return camps;
}

-(instancetype)initWithTitile:(NSString *)titile UrlStr:(NSString *)urlStr{
    if (self = [super init]) {
        self.titile = titile;
        self.urlStr = urlStr;
    }
    return self;
}
@end
