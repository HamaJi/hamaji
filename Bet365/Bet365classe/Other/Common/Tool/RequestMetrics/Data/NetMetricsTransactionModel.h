//
//  NetMetricsTransactionModel.h
//  Bet365
//
//  Created by adnin on 2019/5/11.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "MTLModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface NetMetricsTransactionModel : MTLModel

@property (nonatomic,assign) NSInteger connectEndDate;
@property (nonatomic,assign) NSInteger connectStartDate;
@property (nonatomic,assign) NSInteger domainLookupEndDate;
@property (nonatomic,assign) NSInteger domainLookupStartDate;
@property (nonatomic,assign) NSInteger fetchStartDate;
@property (nonatomic,assign) NSInteger requestEndDate;
@property (nonatomic,assign) NSInteger requestStartDate;
@property (nonatomic,assign) NSInteger responseEndDate;
@property (nonatomic,assign) NSInteger responseStartDate;

@end

NS_ASSUME_NONNULL_END
