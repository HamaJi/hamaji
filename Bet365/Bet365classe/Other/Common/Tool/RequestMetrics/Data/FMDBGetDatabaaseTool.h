//
//  Tool_FMDBGetDatabase.h
//  
//
//  Created by HHH on 2018/7/10.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMDB/FMDB.h>
@interface FMDBGetDatabaaseTool : NSObject
@property(nonatomic,strong,readonly)FMDatabaseQueue *dbQueue;
+ (instancetype)shareFMDBGetDatabaseTool;
+ (NSString *)dbPath;
@end
