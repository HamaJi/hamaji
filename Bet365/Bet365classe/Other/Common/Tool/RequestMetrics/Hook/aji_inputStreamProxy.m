//
//  aji_inputStreamProxy.m
//  Bet365
//
//  Created by HamaJi on 2018/7/1.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "aji_inputStreamProxy.h"
@interface aji_inputStreamProxy(){
    
}
@end

@implementation aji_inputStreamProxy
-(instancetype)initWith:(id)stream{
    if (self = [super init]) {
        _stream = stream;
    }
    return self;
}
- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector{
    return [_stream methodSignatureForSelector:aSelector];
}
-(void)forwardInvocation:(NSInvocation *)anInvocation{
    [anInvocation invokeWithTarget:_stream];
}
- (NSInteger)read:(uint8_t *)buffer maxLength:(NSUInteger)len {
    NSInteger readSize = [_stream read:buffer maxLength:len];
    return readSize;
}
@end
