//
//  aji_taskMetricsTool.m
//  Bet365
//
//  Created by HamaJi on 2018/7/1.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "aji_taskMetricsTool.h"
#import "aji_inputStreamProxy.h"
#import "aji_hookMethodUtil.h"
#import <fishhook/fishhook.h>
#import <dlfcn.h>
@interface aji_taskMetricsTool(){
    
}
@end
static aji_taskMetricsTool *tool=nil;
@implementation aji_taskMetricsTool

+(instancetype)defultManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        tool = [[aji_taskMetricsTool alloc] init];
//        aji_HookMethod(cls, @selector(sessionWithConfiguration:delegate:delegateQueue:), cls, @selector(hook_sessionWithConfiguration:delegate:delegateQueue:),YES);
    });
    return tool;
}

#pragma mark - public
/**
 捕获，CFReadStreamCreateForHTTPRequest的hook 最好再appdelegate内调用
 */
-(void)binding{
    
    save_original_symbols();
    rebind_symbols((struct rebinding[1]){{"CFReadStreamCreateForHTTPRequest", aji_CFReadStreamCreateForHTTPRequest, (void *)& original_CFReadStreamCreateForHTTPRequest}}, 1);
}
#pragma mark - Private
static CFReadStreamRef (*original_CFReadStreamCreateForHTTPRequest)(CFAllocatorRef __nullable alloc,
                                                                    CFHTTPMessageRef request);

/**
 aji_inputStreamProxy 持有 original CFReadStreamRef，转发消息到 original CFReadStreamRef，在 read 方法中记录获取数据的大小
 */
static CFReadStreamRef aji_CFReadStreamCreateForHTTPRequest(CFAllocatorRef alloc,CFHTTPMessageRef request) {
    CFReadStreamRef originalCFStream = original_CFReadStreamCreateForHTTPRequest(alloc, request);
    NSInputStream *stream = (__bridge NSInputStream *)originalCFStream;
    aji_inputStreamProxy *outStream = [[aji_inputStreamProxy alloc] initWith:stream];
    CFRelease(originalCFStream);
    CFReadStreamRef result = (__bridge_retained CFReadStreamRef)outStream;
    return result;
}

void save_original_symbols() {
    original_CFReadStreamCreateForHTTPRequest = dlsym(RTLD_DEFAULT,"CFReadStreamCreateForHTTPRequest");
}

+ (NSURLSession *)aji_sessionWithConfiguration:(NSURLSessionConfiguration *)configuration delegate:(id<NSURLSessionDelegate>)delegate delegateQueue:(NSOperation *)queue{
    if (delegate) {
        aji_HookDelegateMethod([delegate class],
                               @selector(URLSession:task:didFinishCollectingMetrics:),
                               [self class],
                               @selector(aiji_URLSession:task:didFinishCollectingMetrics:),
                               @selector(aiji_none_URLSession:task:didFinishCollectingMetrics:));
    }
    return [self aji_sessionWithConfiguration:configuration delegate:delegate delegateQueue:queue];
}

#pragma mark - HookDelegate
-(void)aiji_URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didFinishCollectingMetrics:(NSURLSessionTaskMetrics *)metrics API_AVAILABLE(ios(10.0)){
    
}

-(void)aiji_none_URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didFinishCollectingMetrics:(NSURLSessionTaskMetrics *)metrics API_AVAILABLE(ios(10.0)){
    
}

@end
