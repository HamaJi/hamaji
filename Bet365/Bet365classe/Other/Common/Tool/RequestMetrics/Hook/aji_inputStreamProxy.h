//
//  aji_inputStreamProxy.h
//  Bet365
//
//  Created by HamaJi on 2018/7/1.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface aji_inputStreamProxy : NSObject
@property (strong,nonatomic)NSInputStream *stream;
-(instancetype)initWith:(id)stream;
@end
