//
//  NetStatistics.h
//  Bet365
//
//  Created by HHH on 2018/7/14.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetRequestCamp.h"
#import "NetRequestNode.h"
typedef NS_ENUM(NSUInteger, StatisticsRequestType) {
    /** 配置地址*/
    StatisticsRequestType_CAMP,
    /** 节点地址*/
    StatisticsRequestType_NODE,
};
typedef NS_ENUM(NSUInteger, NetStatusRate) {
    /** 优*/
    NetStatusRate_GREET = 0,
    /** 良*/
    NetStatusRate_GOOD,
    /** 一般*/
    NetStatusRate_FINE,
    /** 差*/
    NetStatusRate_BAD
};
@protocol NetTaskStatisticsDelegate <NSObject>

@optional

- (void)url:(NSString *)url didFinishCollectingMetrics:(NetMetricsModel *)metrics API_AVAILABLE(macosx(10.12), ios(10.0));

@end

/** 请求成功的Block */
typedef void(^StatisticsBlock)(NetMetricsModel *metrics);

@interface NetTaskStatistics : NSObject

@property(nonatomic,weak)id<NetTaskStatisticsDelegate>delegate;

+(instancetype)shareInstance;

-(NSURLSessionDataTask *)runTaskByRequestUrlString:(NSString *)urlStr;

int textHexColor(NSInteger duration);

NetStatusRate netReate(NSInteger duration);

NSRange rangOfNetRate(NetStatusRate rate);

NSString *chnStringNetRate(NSInteger duration);

NSArray <NSString *>*netRateChnStrList();

@end
