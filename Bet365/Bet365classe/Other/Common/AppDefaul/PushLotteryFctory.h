//
//  PushLotteryFctory.h
//  Bet365
//
//  Created by package_mr.chicken on 2019/3/9.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
#define PUSH_LOTTERY [PushLotteryFctory initManager]
@interface PushLotteryFctory : NSObject
+(instancetype)initManager;
- (void)enterLotteryFactory:(NSString *)gameId;
- (void)homePushEnterFactory:(BOOL)Type FactoryGameId:(NSString *)gameId TuLongEnterValue:(BOOL)tulongValue;
-(void)eLotteryFactory:(NSString *)gameId type:(BOOL)creditType copyTulongEnter:(BOOL)valueEnter;
- (NSString *)getPushGameCurrenToken;
@end

NS_ASSUME_NONNULL_END
