//
//  NetWorkMannager+Account.m
//  Bet365
//
//  Created by luke on 2018/7/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NetWorkMannager+Account.h"

@implementation NetWorkMannager (Account)

- (NSURLSessionTask *)requestLoginUrlParameters:(NSDictionary *)parameters success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self POST:LoginUrl RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestLoginFV:(NSString *)fv success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parame = @{}.mutableCopy;
    [parame safeSetObject:fv forKey:@"fv"];
    return [self POST:LoginFvUrl RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:parame responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestLoginPhoneUrlParameters:(NSDictionary *)parameters success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self POST:LoginPhoneUrl RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}


-(NSURLSessionTask *)requestRegisterUrlParameters:(NSDictionary *)parameters
                                          success:(HttpRequestSuccess)success
                                          failure:(HttpRequestFailed)failure{
    return [self POST:RegisterUrl RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}


- (NSURLSessionTask *)requestLogoutSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self POST:LogoutUrl RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}



- (NSURLSessionTask *)requestFundPwdByUserAccount:(nonnull NSString *)userAccount CashPassword:(nonnull NSString*)cashPassword vCode:(nonnull NSString *)yzmNum Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (userAccount) {
        [parameters setValue:userAccount forKey:@"userAccount"];
    }
    if (cashPassword) {
        [parameters setValue:cashPassword forKey:@"cashPassword"];
    }
    if (yzmNum) {
        [parameters setValue:yzmNum forKey:@"yzmNum"];
    }
    return [self POST:FundPwd RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestCheckUniqueType:(NSInteger)type val:(NSString *)val Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters =[NSMutableDictionary dictionary];
    [parameters setValue:@(type) forKey:@"type"];
    [parameters setValue:val forKey:@"val"];
    return [self GET:CHECKUNIQUE_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestPostUpdatePwdByOldPassword:(NSString *)oldPassword NewPassword:(NSString *)newPassword type:(PersonPassWordType)type Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (oldPassword) {
        [parameters setValue:oldPassword forKey:@"oldPassword"];
    }
    if (newPassword) {
        [parameters setValue:newPassword forKey:@"newPassword"];
    }
    return [self POST:type == PersonPassWordTypeLogin ? USER_UPDATE_PASSWORD : USER_UPDATE_FUNDPWD RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}

-(NSURLSessionTask *)requestPostModifyUserInfoWithfullName:(NSString *)fullName phone:(NSString *)phone qq:(NSString *)qq weixin:(NSString *)weixin birthday:(NSString *)birthday email:(NSString *)email smsCode:(NSString *)smsCode Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    if (fullName.length) {
        [parameters setValue:fullName forKey:@"fullName"];
    }
    if (phone.length) {
        [parameters setValue:phone forKey:@"phone"];
    }
    if (qq.length) {
        [parameters setValue:qq forKey:@"qq"];
    }
    if (weixin.length) {
        [parameters setValue:weixin forKey:@"weixin"];
    }
    if (birthday.length) {
        [parameters setValue:birthday forKey:@"birthday"];
    }
    if (email.length) {
        [parameters setValue:email forKey:@"email"];
    }
    if (smsCode.length) {
        [parameters setValue:smsCode forKey:@"smsCode"];
    }
    return [self POST:USER_MODIFY_USERINFO RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

-(NSURLSessionTask *)requestGetUserInfoVerifyType:(NSString *)phone Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters safeSetObject:phone forKey:@"phone"];
    [parameters safeSetObject:@"ios" forKey:@"clientType"];
    return [self POST:USER_VERIFYTYPE_USERINFO_PHONE RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
    
}



- (NSURLSessionTask *)requestEditUserInfoLimitCache:(HttpRequestCache)cache Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:USER_EDIT_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:cache success:success failure:failure];
}
- (NSURLSessionTask *)requestGetQueryOutMoneyIndexSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:USER_DRAW_MONEY_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestPostMoneyIndexWithcashMoney:(NSString *)cashMoney cashPassword:(NSString *)cashPassword yzmNum:(NSString *)yzmNum UserMemo:(NSString *)userMemo Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (cashMoney) {
        [parameters setValue:cashMoney forKey:@"cashMoney"];
    }
    if (cashPassword) {
        [parameters setValue:cashPassword forKey:@"cashPassword"];
    }
    if (yzmNum) {
        [parameters setValue:yzmNum forKey:@"yzmNum"];
    }
    [parameters safeSetObject:userMemo forKey:@"userMemo"];
    return [self POST:USER_SAVE_MONEY_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}

#pragma 个人数据
- (NSURLSessionTask *)requestInfoUrlSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{
    return [self GET:InfoUrl RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestTimeLoopStatusSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{
    return [self GET:InfoStatus_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:nil responseCache:nil success:success failure:failure];
    
}

- (NSURLSessionTask *)requestGetDeliverByAccount:(NSString *)account
                                          Page:(NSUInteger)page
                                          Rows:(NSUInteger)rows
                                     StartDate:(NSString *)startDate
                                       EndDate:(NSString *)endDate
                                        gameId:(NSString*)gameId
                                        status:(NSString *)status
                                         model:(NSString *)model
                                          result:(NSString *)result
                                       Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{
    
    NSMutableDictionary *parameters =[NSMutableDictionary dictionary];
    [parameters setValue:@(page) forKey:@"page"];
    [parameters setValue:@(rows) forKey:@"rows"];
    if (account) {
        [parameters setValue:account forKey:@"account"];
    }
    if (startDate) {
        [parameters setValue:startDate forKey:@"startDate"];
    }
    if (endDate) {
        [parameters setValue:endDate forKey:@"endDate"];
    }
    if (gameId) {
        [parameters setValue:gameId forKey:@"gameId"];
    }
    if (status) {
        [parameters setValue:status forKey:@"status"];
    }
    if (model) {
        [parameters setValue:model forKey:@"model"];
    }
    if (result) {
        [parameters setValue:result forKey:@"result"];
    }
    return [self GET:CP_RECORDS_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetPursueByAccount:(NSString *)account
                                            Page:(NSUInteger)page
                                            Rows:(NSUInteger)rows
                                       StartDate:(NSString *)startDate
                                         EndDate:(NSString *)endDate
                                          gameId:(NSString *)gameId
                                          result:(UserTraceStatus)result
                                         Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{
    
    NSMutableDictionary *parameters =[NSMutableDictionary dictionary];
    [parameters setValue:@(page) forKey:@"page"];
    [parameters setValue:@(rows) forKey:@"rows"];
    if (account) {
        [parameters setValue:account forKey:@"account"];
    }
    if (startDate) {
        [parameters setValue:startDate forKey:@"startDate"];
    }
    if (endDate) {
        [parameters setValue:endDate forKey:@"endDate"];
    }
    if (gameId) {
        [parameters setValue:gameId forKey:@"gameId"];
    }
    NSString *status = nil;
    if (result == UserTraceStatusWin) {
        status = @"1";
    }else if (result == UserTraceStatusLose) {
        status = @"0";
    }
    if (status) {
        [parameters setValue:status forKey:@"result"];
    }
    [parameters setValue:@"false" forKey:@"joinSubs"];
    return [self GET:USER_TRACE_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetTraceWithTraceOrderNo:(NSString *)traceOrderNo Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (traceOrderNo) {
        [parameters setValue:traceOrderNo forKey:@"traceOrderNo"];
    }
    return [self GET:USER_TRACE_TRUE_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestPostCpParameters:(NSDictionary *)parameters Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{
    return [self POST:CP_CANCEL_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetCpHistorySuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:CP_HISTORYDAY_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}


- (NSURLSessionTask *)requestGetCpHistoryListAccount:(NSString *)account Page:(NSUInteger)page Rows:(NSUInteger)rows date:(NSString *)date gameId:(NSString *)gameId model:(NSString *)model Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters =[NSMutableDictionary dictionary];
    [parameters setValue:@(page) forKey:@"page"];
    [parameters setValue:@(rows) forKey:@"rows"];
    if (account) {
        [parameters setValue:account forKey:@"account"];
    }
    if (date) {
        [parameters setValue:date forKey:@"date"];
    }
    if (gameId) {
        [parameters setValue:gameId forKey:@"gameId"];
    }
    if (model) {
        [parameters setValue:model forKey:@"model"];
    }
    return [self GET:CP_HISTORYLIST_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetGameStartDate:(NSString *)startDate EndDate:(NSString *)endDate gameCodes:(NSString *)gameCodes Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters =[NSMutableDictionary dictionary];
    if (startDate) {
        [parameters setValue:startDate forKey:@"startDate"];
    }
    if (endDate) {
        [parameters setValue:endDate forKey:@"endDate"];
    }
    if (gameCodes) {
        [parameters setValue:[gameCodes stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] forKey:@"gameCodes"];
    }
    return [self GET:GAME_RECORD_LIST_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetMessageListSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{

    return [self GET:UserMessageURL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetReadMessageWithId:(NSInteger)Id Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (Id) {
        [parameters setObject:@(Id) forKey:@"id"];
    }
    return [self GET:MESSAGE_READ_URlL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetRemoveMessageWithId:(NSInteger)Id Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (Id) {
        [parameters setObject:@(Id) forKey:@"id"];
    }
    return [self GET:MESSAGE_REMOVE_URlL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetNoticeListSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{
    
    return [self GET:UserNoticeURL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetUserDrawPage:(NSUInteger)page Rows:(NSUInteger)rows StartDate:(NSString *)startDate EndDate:(NSString *)endDate Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters =[NSMutableDictionary dictionary];
    [parameters setValue:@(page) forKey:@"page"];
    [parameters setValue:@(rows) forKey:@"rows"];
    if (startDate) {
        [parameters setValue:startDate forKey:@"beginDatetime"];
    }
    if (endDate) {
        [parameters setValue:endDate forKey:@"endDatetime"];
    }
    return [self GET:USER_DRAW_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetRwsWithUserId:(NSNumber *)userId PersonTeamRwsType:(PersonTeamRwsType)rwsType beginDate:(NSString *)beginDate endDate:(NSString *)endDate Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (userId) {
        [parameters setObject:userId forKey:@"userId"];
    }
    if (beginDate) {
        [parameters setObject:beginDate forKey:@"beginDate"];
    }
    if (endDate) {
        [parameters setObject:endDate forKey:@"endDate"];
    }
    return [self GET:rwsType == PersonTeamRwsType_Team ? USER_RWS_TEAM_URL : USER_RWS_PERSON_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetSubUsersWithStartDate:(NSString *)startDate endDate:(NSString *)endDate moneyFrom:(NSString *)moneyFrom moneyTo:(NSString *)moneyTo page:(NSInteger)page rows:(NSInteger)rows account:(NSString *)account subAccount:(NSString *)subAccount Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (startDate) {
        [parameters setObject:startDate forKey:@"startDate"];
    }
    if (endDate) {
        [parameters setObject:endDate forKey:@"endDate"];
    }
    if (moneyFrom) {
        [parameters setObject:moneyFrom forKey:@"moneyFrom"];
    }
    if (moneyTo) {
        [parameters setObject:moneyTo forKey:@"moneyTo"];
    }
    if (page) {
        [parameters setObject:@(page) forKey:@"page"];
    }
    if (rows) {
        [parameters setObject:@(page) forKey:@"page"];
    }
    if (account) {
        [parameters setObject:account forKey:@"account"];
    }
    if (subAccount) {
        [parameters setObject:subAccount forKey:@"subAccount"];
    }
    return [self GET:USER_SUBUSERS_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetUpdateRebateWithUserId:(NSNumber *)userId rebate:(NSString *)rebate Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (userId) {
        [parameters setObject:userId forKey:@"userId"];
    }
    if (rebate) {
        [parameters setObject:rebate forKey:@"rebate"];
    }
    return [self GET:USER_UPDATE_REBATE_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetRebateRangeWithUserId:(NSNumber *)userId Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (userId) {
        [parameters setObject:userId forKey:@"userId"];
    }
    return [self GET:USER_REBATE_RANGE_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestgetDlSubRebateRangeSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:USER_DLREBATE_RANGE_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestPostDlAddWithUserInfo:(NSDictionary *)userInfo  Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self POST:USER_ADD_REBATE_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:userInfo responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetUserWithdrawSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:USER_VALID_DRAW_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetUserBankSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:USER_BANK_DRAW_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestPostModifyUserInfoWithBankName:(NSString *)bankName cardNo:(NSString *)cardNo subAddress:(NSString *)subAddress fullName:(NSString *)fullName cashPassword:(NSString *)cashPassword type:(DrawBankCardType)type Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (bankName) {
        [parameters setObject:bankName forKey:@"bankName"];
    }
    if (cardNo) {
        [parameters setObject:cardNo forKey:@"cardNo"];
    }
    if (subAddress) {
        [parameters setObject:subAddress forKey:@"subAddress"];
    }
    if (fullName) {
        [parameters setObject:fullName forKey:@"fullName"];
    }
    if (cashPassword) {
        [parameters setObject:cashPassword forKey:@"cashPassword"];
    }
    NSString *url = nil;
    switch (type) {
        case DrawBankCardType_Add:
            url = USER_BANK_MODIFY_URL;
            break;
        case DrawBankCardType_Modify:
            url = USER_BANK_CARD_URL;
            break;
        case DrawBankCardType_Release:
            url = USER_BANK_RELEASE_URL;
            break;
        default:
            break;
    }
    return [self POST:url RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestPostUpdateByAgentIdUrlWithModel:(LukeSpreadItem *)model url:(NSString *)url Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (model) {
        [parameters setDictionary:[model mj_keyValues]];
    }
    if (url) {
        [parameters setObject:url forKey:@"url"];
    }
    return [self POST:USER_UPDATE_SPREAD_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetSpreadInfoWithPage:(NSInteger)page rows:(NSInteger)rows Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (page) {
        [parameters setObject:@(page) forKey:@"page"];
    }
    if (rows) {
        [parameters setObject:@(rows) forKey:@"rows"];
    }
    return [self GET:USER_SPREAD_INFO_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestPostRemoveSpreadInfoWithId:(NSInteger)Id Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (Id) {
        [parameters setObject:@(Id) forKey:@"id"];
    }
    return [self POST:USER_REMOVE_SPREAD_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestPostCreateSpreadInfoWithType:(SpreadInfoCreateType)type userType:(SpreadInfoUserType)userType externalUrl:(NSString *)externalUrl spreadType:(NSInteger)spreadType rebate:(NSString *)rebate code:(NSString *)code Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (type == SpreadInfoCreateTypeUrl) {
        [parameters setObject:@(0) forKey:@"type"];
    }else if (type == SpreadInfoCreateTypeWechat) {
        [parameters setObject:@(1) forKey:@"type"];
    }
    if (userType == SpreadInfoUserTypeHuiYuan) {
        [parameters setObject:@(0) forKey:@"userType"];
    }else if (userType == SpreadInfoUserTypeDaiLi) {
        [parameters setObject:@(1) forKey:@"userType"];
    }
    if (spreadType) {
        [parameters setObject:@(spreadType) forKey:@"spreadType"];
    }
    if (rebate) {
        [parameters setObject:rebate forKey:@"rebate"];
    }
    if (code) {
        [parameters setObject:code forKey:@"code"];
    }
    if (externalUrl) {
        [parameters setObject:externalUrl forKey:@"externalUrl"];
    }
    return [self POST:USER_SPREAD_NET_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetSpreadTypesSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:USER_SPREAD_TYPES_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetQrcodeWithContent:(NSString *)content Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (content) {
        [parameters setObject:content forKey:@"content"];
    }
    return [self GET:USER_QRCODE_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetAllGameSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:TheGameJsonList RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetLiveBrUserName:(NSString *)userName Page:(NSUInteger)page Rows:(NSUInteger)rows betStartDate:(NSString *)betStartDate betEndDate:(NSString *)betEndDate gameCode:(NSString *)gameCode Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (userName) {
        [parameters setValue:userName forKey:@"userName"];
    }
    [parameters setValue:@(page) forKey:@"page"];
    [parameters setValue:@(rows) forKey:@"rows"];
    if (betStartDate) {
        [parameters setValue:StringFormatWithStr(betStartDate, @" 00:00:00") forKey:@"betStartDate"];
    }
    if (betEndDate) {
        [parameters setValue:StringFormatWithStr(betEndDate, @" 23:59:59") forKey:@"betEndDate"];
    }
    if (gameCode) {
        [parameters setValue:gameCode forKey:@"gameCode"];
    }
    [parameters setValue:@(4) forKey:@"timeType"];
    return [self GET:USER_Live_BR_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetLiveQstPage:(NSUInteger)page Rows:(NSUInteger)rows liveCode:(NSString *)liveCode Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (page) {
        [parameters setValue:@(page) forKey:@"page"];
    }
    if (rows) {
        [parameters setValue:@(rows) forKey:@"rows"];
    }
    if (liveCode) {
        [parameters setValue:liveCode forKey:@"liveCode"];
    }
    return [self GET:JB_KY_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetRechargeWithPage:(NSUInteger)page Rows:(NSUInteger)rows dateFrom:(NSString *)dateFrom dateTo:(NSString *)dateTo mode:(NSString *)mode status:(PersonalRechargeOrderStatus)status Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (page) {
        [parameters setValue:@(page) forKey:@"page"];
    }
    if (rows) {
        [parameters setValue:@(rows) forKey:@"rows"];
    }
    if (dateFrom) {
        [parameters setValue:dateFrom forKey:@"dateFrom"];
    }
    if (dateTo) {
        [parameters setValue:dateTo forKey:@"dateTo"];
    }
    NSString *recharge = nil;
    if (status == PersonalRechargeOrderStatusFinish) {
        recharge = @"3";
    }else if (status == PersonalRechargeOrderStatusLose) {
        recharge = @"4";
    }
    if (recharge) {
        [parameters setValue:recharge forKey:@"status"];
    }
    if (mode) {
        [parameters setValue:mode forKey:@"mode"];
    }
    return [self GET:USER_RECHARGE_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetRechargeDiscountTypeListSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:USER_DISCOUNT_TYPE_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetPageBillWithAccount:(NSString *)account beginDate:(NSString *)beginDate endDate:(NSString *)endDate tranType:(NSString *)tranType page:(NSInteger)page rows:(NSInteger)rows Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (account) {
        [parameters setValue:account forKey:@"account"];
    }
    if (beginDate) {
        [parameters setValue:beginDate forKey:@"beginDate"];
    }
    if (account) {
        [parameters setValue:account forKey:@"account"];
    }
    if (endDate) {
        [parameters setValue:endDate forKey:@"endDate"];
    }
    if (tranType) {
        [parameters setValue:tranType forKey:@"tranType"];
    }
    if (page) {
        [parameters setValue:@(page) forKey:@"page"];
    }
    if (rows) {
        [parameters setValue:@(rows) forKey:@"rows"];
    }
    return [self GET:USER_PAGE_BILL_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetTrendWithModel:(KFCPHomeGameJsonModel *)model RequestCache:(HttpRequestCache)cache Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSString *js = @"";
    if (model.jsType && [model.jsType integerValue] == 1) {
        js = @"/js";
    }
    return [self GET:[NSString stringWithFormat:TREND_NUMTREND,js,model.code] RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:cache success:success failure:failure];
}

- (NSURLSessionTask *)requestGetTransferWithFrom:(NSString *)from to:(NSString *)to amount:(NSString *)amount Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (from) {
        [parameters setValue:from forKey:@"from"];
    }
    if (to) {
        [parameters setValue:to forKey:@"to"];
    }
    if (amount) {
        [parameters setValue:amount forKey:@"amount"];
    }
    return [self GET:USER_TRANSFER RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetGetBalanceWithLiveCode:(NSString *)liveCode Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (liveCode) {
        [parameters setValue:liveCode forKey:@"liveCode"];
    }
    return [self GET:USER_BALANCE RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetPayTypesSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:USER_PAYTYPES_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetPayAccountsWithPayType:(NSString *)payType Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (payType) {
        [parameters setValue:payType forKey:@"payType"];
    }
    return [self GET:USER_PAYACCOUNT_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetTpPayChannelsWithPayType:(NSString *)payType Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (payType) {
        [parameters setValue:payType forKey:@"payType"];
    }
    return [self GET:USER_PAYCHANNEL_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetRechargeConfigWithMode:(NSInteger)mode payType:(NSString *)payType Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (payType) {
        [parameters setValue:payType forKey:@"payType"];
    }
    [parameters setValue:@(mode) forKey:@"mode"];
    return [self GET:USER_PAYCONFIG_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestPostRechargeWithAmount:(NSString *)amount payAccountId:(NSInteger)payAccountId yzmNum:(NSString *)yzmNum remark:(NSString *)remark rechargeTime:(NSString *)rechargeTime rechargePerson:(NSString *)rechargePerson friendAccount:(NSString *)friendAccount Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (amount) {
        [parameters setValue:amount forKey:@"amount"];
    }
    if (yzmNum) {
        [parameters setValue:yzmNum forKey:@"yzmNum"];
    }
    if (remark) {
        [parameters setValue:[remark stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] forKey:@"remark"];
    }
    [parameters setValue:@(payAccountId) forKey:@"payAccountId"];
    if (rechargeTime) {
        [parameters setValue:[rechargeTime stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] forKey:@"rechargeTime"];
    }
    if (rechargePerson) {
        [parameters setValue:rechargePerson forKey:@"rechargePerson"];
    }
    if (friendAccount) {
        [parameters setValue:friendAccount forKey:@"friendAccount"];
    }
    return [self POST:USER_TRANSFER_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetRechargeOnlinePayWithAmount:(NSString *)amount payChannelId:(NSInteger)payChannelId yzmNum:(NSString *)yzmNum bankCode:(NSString *)bankCode Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (amount) {
        [parameters setValue:amount forKey:@"amount"];
    }
    if (yzmNum) {
        [parameters setValue:yzmNum forKey:@"yzmNum"];
    }
    [parameters setValue:@(payChannelId) forKey:@"payChannelId"];
    if (bankCode) {
        [parameters setValue:bankCode forKey:@"bankCode"];
    }
    return [self GET:USER_ONLINEPAY_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetCheckoutErrorCountWithType:(InfoCheckoutErrorCountType)type Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (type == InfoCheckoutErrorCountType_Login) {
        [parameters setValue:@(1) forKey:@"type"];
    }else if (type == InfoCheckoutErrorCountType_Register) {
        [parameters setValue:@(2) forKey:@"type"];
    }
    return [self GET:USER_CHECKOUTERROR_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}

#pragma mark - 配置信息
- (NSURLSessionTask *)requestLoginLimitSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{
    return [self GET:LoginLimit RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)postUserVerifyIpLocationFields:(NSDictionary *)parameters
                                         Success:(HttpRequestSuccess)success
                                         failure:(HttpRequestFailed)failure{
    
    return [self POST:VerifyIpLocationFields RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetLimitKeyName:(NSString *)key Cache:(HttpRequestCache)cache Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{
    NSMutableDictionary *paramers = @{}.mutableCopy;
    [paramers setObject:key forKey:@"name"];
    return [self GET:GET_LIMIET_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:paramers responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)postInitPwdParamter:(NSDictionary *)parameters Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self POST:INIT_PWD_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}
- (NSURLSessionTask *)requestRegisterLimitSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{
    return [self GET:RegisterLimit RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestRegisterDataLimitTextSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{
    return [self GET:RegisterDataLimit RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestSystemConfigSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{
    return [self GET:system_config RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestAdverLinksConfigSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{
    return [self GET:adverLink_config RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:nil responseCache:nil success:success failure:failure];
}

-(NSURLSessionTask *)requestTextRegisterUrlSuccess:(HttpRequestSuccess)success
                                           failure:(HttpRequestFailed)failure{
    ;
    return [self GET:TestPlayGameUrl RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:nil responseCache:nil success:success failure:failure];
}

-(NSURLSessionTask *)requestTestRegisterUserByPassword:(NSString *)password ValiCode:(NSString *)valiCode success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{
    NSMutableDictionary *parameters = @{}.mutableCopy;
    
    if (password.length) {
        [parameters setObject:password forKey:@"password"];
    }
    if (valiCode.length) {
        [parameters setObject:valiCode forKey:@"valiCode"];
    }
    
    return [self POST:REGISTER_TESTUSER_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

-(NSURLSessionTask *)requestRETAILSTWITCHURLresponseCache:(HttpRequestCache)responseCache
                                                  success:(HttpRequestSuccess)success
                                                  failure:(HttpRequestFailed)failure
{
    return [self GET:RETAILSTWITCHURL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:nil responseCache:responseCache success:success failure:failure];
}

- (NSURLSessionTask *)requestGetOpenStatusSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:WZRY_OPENSTATUS RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

#pragma mark - 验证码
- (NSURLSessionTask *)requestGetVCodeByRandom:(BOOL)isRandom Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSDictionary *parameter = nil;
    if (isRandom) {
        parameter = @{@"t":[NSString stringWithFormat:@"%.30f",arc4random()%1000000*0.000001]};
    }
    return [self GET:VCode RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:isRandom ? parameter : nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetVuserCodeSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:V_USRE_CODE RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:nil responseCache:nil success:success failure:failure];
}

-(NSURLSessionTask *)requestMainFlexTemplatesResponseCache:(HttpRequestCache)responseCache
                                                   success:(HttpRequestSuccess)success
                                                   failure:(HttpRequestFailed)failure{
    return [self GET:MAIN_FLEX_URL
         RequestType:PPSerializerType_HTTP
        ResponseType:PPSerializerType_HTTP
          parameters:nil responseCache:responseCache success:success failure:failure];
}

- (NSURLSessionTask *)requestMainFlexMenusCache:(HttpRequestCache)cache Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:MAIN_FLEX_MANUE_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:nil responseCache:cache success:success failure:failure];
}

-(NSURLSessionTask *)requestTheAnnouncementUrlresponseCache:(HttpRequestCache)responseCache
                                                    success:(HttpRequestSuccess)success
                                                    failure:(HttpRequestFailed)failure
{
    return [self GET:TheAnnouncementUrl RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:responseCache success:success failure:failure];
}

-(NSURLSessionTask *)requestGetSliderResponseCache:(HttpRequestCache)responseCache
                                           success:(HttpRequestSuccess)success
                                           failure:(HttpRequestFailed)failure
{
    return [self GET:SLIDER_JSON_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:responseCache success:success failure:failure];
}

- (NSURLSessionTask *)requestVerifyAuthorTypeAccount:(NSString *)account
                                               Phone:(NSString *)loginPhone
                                            Success:(HttpRequestSuccess)success
                                           failure:(HttpRequestFailed)failure
{
//    return [self GET:USER_LOFINVERIFY_LOGIN_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:nil responseCache:cache success:success failure:failure];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:@(1) forKey:@"version"];
    [parameters setObject:@"ios" forKey:@"clientType"];
    [parameters safeSetObject:account forKey:@"account"];
    [parameters safeSetObject:loginPhone forKey:@"loginPhone"];
    
    return [self GET:USER_LOFINVERIFY_LOGIN_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestChangeSMSVerifyTypePhone:(NSString *)phone
                                            Success:(HttpRequestSuccess)success
                                            failure:(HttpRequestFailed)failure
{
    //    return [self GET:USER_LOFINVERIFY_LOGIN_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:nil responseCache:cache success:success failure:failure];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:@(1) forKey:@"version"];
    [parameters setObject:@"ios" forKey:@"clientType"];
    [parameters safeSetObject:phone forKey:@"phone"];
    //    [parameters safeSetObject:loginPhone forKey:@"loginPhone"];
    
    return [self GET:USER_VERIFY_CHANGESMS_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}



- (NSURLSessionTask *)requestLoginSMScodeByPhone:(NSString *)phone
                                   Paramers:(NSDictionary *)paramser
                                            Success:(HttpRequestSuccess)success
                                            failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *mParamers = [[NSMutableDictionary alloc] initWithDict:paramser];
    [mParamers safeSetObject:phone forKey:@"phone"];
    //    return [self GET:USER_LOFINVERIFY_LOGIN_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:nil responseCache:cache success:success failure:failure];
    return [self POST:USER_SMS_LOGIN_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:mParamers responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestUpdateSMScodeByPhone:(NSString *)phone
                                        Paramers:(NSDictionary *)paramser
                                         Success:(HttpRequestSuccess)success
                                         failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *mParamers = [[NSMutableDictionary alloc] initWithDictionary:paramser];
    [mParamers safeSetObject:phone forKey:@"phone"];
    //    return [self GET:USER_LOFINVERIFY_LOGIN_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:nil responseCache:cache success:success failure:failure];
    return [self POST:USER_SMS_UPDATE_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:mParamers responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestCancleBindingPhone:(NSString *)phone
                                        smsCode:(NSString *)code
                                          Success:(HttpRequestSuccess)success
                                          failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *paramers = @{}.mutableCopy;;
    [paramers safeSetObject:phone forKey:@"loginPhone"];
    [paramers safeSetObject:code forKey:@"code"];
    return [self POST:USER_CANCLEPHONE_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:paramers responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestModifyBindingPhone:(NSString *)phone
                                        smsCode:(NSString *)code
                                        Success:(HttpRequestSuccess)success
                                        failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *paramers = @{}.mutableCopy;;
    [paramers safeSetObject:phone forKey:@"loginPhone"];
    [paramers safeSetObject:code forKey:@"code"];
    return [self POST:USER_MODIFYPHONE_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:paramers responseCache:nil success:success failure:failure];
}



- (NSURLSessionTask *)requestRigisterVerifyTypeSuccess:(HttpRequestSuccess)success
                                     failure:(HttpRequestFailed)failure
{
    //    return [self GET:USER_LOFINVERIFY_LOGIN_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:nil responseCache:cache success:success failure:failure];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:@(1) forKey:@"version"];
    [parameters setObject:@"ios" forKey:@"clientType"];
    return [self GET:USER_LOFINVERIFY_REGISTER_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}


- (NSURLSessionTask *)requestForceChangeNewPsw:(NSString *)newPsw Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:newPsw forKey:@"newPassword"];
    return [self POST:USER_FORCEUPDATEPSW_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}
- (NSURLSessionTask *)requestRechargCardCache:(HttpRequestCache)cache Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:USER_RECHARGECARD_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:nil responseCache:cache success:success failure:failure];
}

- (NSURLSessionTask *)requestPayTypeListCache:(HttpRequestCache)cache Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{
    return [self GET:USER_PAYTYPELIST_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:nil responseCache:cache success:success failure:failure];
}

- (NSURLSessionTask *)requestRechargeLimitCache:(HttpRequestCache)cache Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{
    return [self GET:USER_RECHARGELIMIT_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:nil responseCache:cache success:success failure:failure];
}





- (NSURLSessionTask *)requestPayAccountListWithPayType:(NSString *)payType Cache:(HttpRequestCache)cache Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{
    NSMutableDictionary *paramers = @{}.mutableCopy;
    [paramers safeSetObject:payType forKey:@"payType"];
    return [self GET:USER_PAYACCOUNT_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:paramers responseCache:cache success:success failure:failure];
}

- (NSURLSessionTask *)requestPayChannelListWithPayType:(NSString *)payType Cache:(HttpRequestCache)cache Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{
    NSMutableDictionary *paramers = @{}.mutableCopy;
    [paramers safeSetObject:payType forKey:@"payType"];
    return [self GET:USER_PAYCHANNEL_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:paramers responseCache:cache success:success failure:failure];
}


- (NSURLSessionTask *)requestGetRechargeConfigtWithPayType:(NSString *)payType Model:(RechargePayModelValue *)model ChannleID:(NSInteger)channelId Cache:(HttpRequestCache)cache Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{
    NSMutableDictionary *paramers = @{}.mutableCopy;
    [paramers safeSetObject:payType forKey:@"payType"];
    [paramers safeSetObject:model forKey:@"mode"];
    [paramers safeSetObject:@(channelId) forKey:@"channelId"];
    
    return [self GET:USER_PAYCONFIG_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:paramers responseCache:cache success:success failure:failure];
}

- (NSURLSessionTask *)requestGetTransListSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:USER_TRANSLIST_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}
-(NSURLSessionTask *)requestGetUserCenterMenuresponseCache:(HttpRequestCache)responseCache
                                                   success:(HttpRequestSuccess)success
                                                   failure:(HttpRequestFailed)failure
{
    return [self GET:USER_CENTER_MENUE_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:responseCache success:success failure:failure];
}
-(NSURLSessionTask *)requestGetGrowthConfigesponseCache:(HttpRequestCache)responseCache
                                                success:(HttpRequestSuccess)success
                                                failure:(HttpRequestFailed)failure
{
    return [self GET:USER_CENTER_GROWTH_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:responseCache success:success failure:failure];
}

-(NSURLSessionTask *)requestSubmitSiginInsuccess:(HttpRequestSuccess)success
                                         failure:(HttpRequestFailed)failure
{
    return [self GET:USER_CENTER_SIGIN_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}


-(NSURLSessionTask *)requestYEBaoInfosuccess:(HttpRequestSuccess)success
                                         failure:(HttpRequestFailed)failure
{
    return [self GET:USER_YEBAO_INFO_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

-(NSURLSessionTask *)requestYEBaoTransferIn:(double)money Success:(HttpRequestSuccess)success
                                     failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *paramers = @{}.mutableCopy;
    [paramers safeSetObject:@(money) forKey:@"money"];
    return [self POST:USER_YEBAO_TRANSFER_IN_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:paramers responseCache:nil success:success failure:failure];
}

-(NSURLSessionTask *)requestYEBaoTransferOut:(double)money Password:(NSString *)psw Success:(HttpRequestSuccess)success
                                     failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *paramers = @{}.mutableCopy;
    [paramers safeSetObject:@(money) forKey:@"money"];
    [paramers safeSetObject:psw forKey:@"cashPassword"];
    [paramers safeSetObject:@(1) forKey:@"v"];
    return [self POST:USER_YEBAO_TRANSFER_OUT_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:paramers responseCache:nil success:success failure:failure];
}

-(NSURLSessionTask *)requestYEBaoHistoryWithPage:(NSUInteger)page
                                            Rows:(NSUInteger)rows
                                         Success:(HttpRequestSuccess)success
                                     failure:(HttpRequestFailed)failure
{
    
    return [self GET:USER_YEBAO_TRANSFER_HISTORY_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:@{@"page":@(page),@"rows":@(rows)} responseCache:nil success:success failure:failure];
}


-(NSURLSessionTask *)getQueryAllTestFirstKindCache:(HttpRequestCache)responseCache
                                         Success:(HttpRequestSuccess)success
                                         failure:(HttpRequestFailed)failure
{
    
    return [self GET:ALL_QUERYGAME_LIST_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:responseCache success:success failure:failure];
}
-(NSURLSessionTask *)requestLaunchAdCache:(HttpRequestCache)cache
                                  Success:(HttpRequestSuccess)success
                                failure:(HttpRequestFailed)failure
{
    
    return [self GET:LAUNCH_AD_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:cache success:success failure:failure];
}

- (NSURLSessionTask *)requestGetImmediateMessageSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:[USER_DATA_MANAGER.userInfoData.userId stringValue] forKey:@"userId"];
    return [self GET:IMMEDIATE_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestPostUpdateMessageStatusSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:[USER_DATA_MANAGER.userInfoData.userId stringValue] forKey:@"userId"];
    return [self POST:UPDATE_IMMEDIATE_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestPostRegisterSMSCodeByPhone:(NSString *)phone Paramers:(NSDictionary *)paramser Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *mParamers = [NSMutableDictionary dictionary];
    [mParamers setObject:phone forKey:@"phone"];
    return [self POST:USER_SMS_REGISTER_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:mParamers responseCache:nil success:success failure:failure];
}

@end
