//
//  AppDefaults.m
//  Bet365
//
//  Created by HHH on 2018/7/7.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "AppDefaults.h"
#import "NSObject+Aji.h"
#import "AppSetting.h"
@implementation AppDefaults

-(instancetype)init{
    if (self = [super init]) {
        if ([self isKindOfClass:[AppSetting class]]) {
            
        }
        [self getDataAtUserDefaults];
    }
    return self;
}
/**
 保存
 */
-(void)save{

    NSDictionary *data = [self dictFromData];
    NSUserDefaults *stand = [NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *appDefaults = [[stand objectForKey:@"appDefaults"] mutableCopy];
    if (!appDefaults) {
        appDefaults = @{}.mutableCopy;
    }
    
    NSMutableString *className = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%s",object_getClassName(self)]];
    [className deleteString:@"NSKVONotifying_"];
    [appDefaults setObject:data forKey:className];
    
    [stand setObject:appDefaults forKey:@"appDefaults"];
    [stand synchronize];
}

/**
 清空
 */
-(void)cleanUserData{
    NSUserDefaults *stand = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *appDefaults = [[NSMutableDictionary alloc] initWithDict:[stand objectForKey:@"appDefaults"]];
    if (!appDefaults) {
        appDefaults = @{}.mutableCopy;
    }
    
    NSMutableString *className = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%s",object_getClassName(self)]];
    [className deleteString:@"NSKVONotifying_"];
    [appDefaults removeObjectForKey:className];
    [stand setObject:appDefaults forKey:@"appDefaults"];
    [stand synchronize];
    unsigned int count;
    Ivar *ivarList = class_copyIvarList(self.class, &count);
    for (int i = 0; i<count; i++) {
        Ivar ivar = ivarList[i];
        NSString *key = [[NSString stringWithUTF8String:ivar_getName(ivar)] substringFromIndex:1];
        NSString *type = [NSString stringWithUTF8String:ivar_getTypeEncoding(ivar)];
        if (type.length == 1) {
            //int float double cgflot NSInteger
            [self setValue:@(0) forKey:key];
        }else{
            [self setValue:nil forKey:key];
        }
    }
}

+(void)removeAllDefaults{
    NSUserDefaults *stand = [NSUserDefaults standardUserDefaults];
    [stand removeObjectForKey:@"appDefaults"];
    [stand synchronize];
}

/**
 从缓存读取
 */
-(void)getDataAtUserDefaults{
    NSUserDefaults *stand = [NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *appDefaults = [[NSMutableDictionary alloc] initWithDict:[stand objectForKey:@"appDefaults"]];
    if (!appDefaults) {
        appDefaults = @{}.mutableCopy;
    }
    
    NSMutableString *className = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%s",object_getClassName(self)]];
    [className deleteString:@"NSKVONotifying_"];
    NSMutableDictionary *userData = [[NSMutableDictionary alloc] initWithDict:[appDefaults objectForKey:className]];
    
    [self setDataValueForm:userData];
}


/**
 runtime.kvc
 
 @param dict 数据哈希
 */
-(void)setDataValueForm:(NSDictionary *)dict{
    unsigned int count;
    Ivar *ivarList = class_copyIvarList(self.class, &count);
    
    for (int i = 0; i < count; i++) {
        
        Ivar ivar = ivarList[i];
        NSString *name = [NSString stringWithUTF8String:ivar_getName(ivar)];
        NSString *key = [name substringFromIndex:1];
        
        NSString *type = [NSString stringWithUTF8String:ivar_getTypeEncoding(ivar)];
        __block BOOL isContaiDic = NO;
        [dict.allKeys enumerateObjectsUsingBlock:^(NSString *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isEqualToString:key]) {
                isContaiDic = YES;
                *stop = YES;
            }
        }];
        if (!isContaiDic) {
            continue;
        }
        
        id value = dict[key];
        if ([key isEqualToString:@"isDl"]) {
            NSLog(@"%@",value);
        }
        if ([value isKindOfClass:[NSDictionary class]]) {
            NSString *type = [NSString stringWithUTF8String:ivar_getTypeEncoding(ivar)];
            NSRange range = [type rangeOfString:@"\""];
            type = [type substringFromIndex:range.location + range.length];
            range = [type rangeOfString:@"\""];
            type = [type substringToIndex:range.location];
            Class modelClass = NSClassFromString(type);
            
            if (modelClass) {
                // 把字典转模型
                id model = [modelClass new];
                [model setDataValueForm:value];
                value = model;
            }
        }
        if ([value isKindOfClass:[NSArray class]]) {
            if ([self respondsToSelector:@selector(arrayContainModelClass)]) {
                id idSelf = self;
                
                NSString *type =  [idSelf arrayContainModelClass][key];
                Class classModel = NSClassFromString(type);
                
                NSMutableArray *arrM = [NSMutableArray array];
                for (NSDictionary *dict in value) {
                    id model =  [classModel new];
                    [model setDataValueForm:dict];
                    [arrM addObject:model];
                }
                value = arrM;
                
            }
        }
        if (value && ![value isEqual:[NSNull null]]) {
            [self setValue:value forKey:key];
        }else if([type isEqualToString:@"q"]||
                 [type isEqualToString:@"i"]||
                 [type isEqualToString:@"B"]){
            [self setValue:@(0) forKey:key];
        }else if ([type isEqualToString:@"f"] ||
                  [type isEqualToString:@"d"]){
            [self setValue:@(0.0) forKey:key];
        }else{
            [self setValue:nil forKey:key];
        }
    }
}





/**
 模型转Dic
 
 @return Dic
 */
- (NSDictionary *)dictFromData{
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    unsigned int varCount = 0;
    Ivar *variables = class_copyIvarList([self class], &varCount);
    for (int i = 0; i < varCount; i++) {
        Ivar var = variables[i];
        
        const char *varKey_char = ivar_getName(var);
        NSString *key = [NSString stringWithUTF8String:varKey_char];
        id value = [self valueForKey:key];
        if ([value isKindOfClass:[AppDefaults class]]) {
            value = [value dictFromData];
        }
        if ([value isKindOfClass:[NSArray class]]) {
            if ([self respondsToSelector:@selector(arrayContainModelClass)]){
                id idSelf = self;
                NSString *type =  [idSelf arrayContainModelClass][key];
                if (type) {
                    NSMutableArray *arrM = [NSMutableArray array];
                    [(NSArray *)value enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        NSDictionary *arrDic = [obj dictFromData];
                        arrDic ? [arrM addObject:arrDic] : nil;
                    }];
                    value = arrM;
                }
            }
        }
        if(value && ![value isEqual:[NSNull null]]){
            /** 剔除_*/
            [dict setValue:value forKey:[key stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"_"]]];
        }
    }
    return dict;
}


+(NSDictionary *)arrayContainModelClass{
    return nil;
}


@end
