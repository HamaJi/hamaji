//
//  PushLotteryFctory.m
//  Bet365
//
//  Created by package_mr.chicken on 2019/3/7.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "PushLotteryFctory.h"
static PushLotteryFctory *push_manager = nil;
@implementation PushLotteryFctory
+(instancetype)initManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        push_manager = [[PushLotteryFctory alloc] init];
    });
    return push_manager;
}

- (void)enterLotteryFactory:(NSString *)gameId{
    [self eLotteryFactory:gameId type:([BET_CONFIG.config.play_type_config integerValue]==1)?YES:NO copyTulongEnter:NO];
}
- (void)homePushEnterFactory:(BOOL)Type FactoryGameId:(NSString *)gameId TuLongEnterValue:(BOOL)tulongValue{
    [self eLotteryFactory:gameId type:Type copyTulongEnter:tulongValue];
}
-(void)eLotteryFactory:(NSString *)gameId type:(BOOL)creditType copyTulongEnter:(BOOL)valueEnter{
    
    BetTotal365ViewController *totalC = [[BetTotal365ViewController alloc]init];
    totalC.view.backgroundColor = [UIColor whiteColor];
    totalC.isTuLongEnter = valueEnter;
    [totalC replacekindWayType];
    void (^addCreditChild)() = ^(){
        [totalC addChildController];
        [totalC fristIsOfficalBy:0];
    };
    void (^addOfficalChild)() = ^(){
        [totalC addofficalSonController];
        [totalC fristIsOfficalBy:1];
    };
    if (((NSArray *)LOTTERY_FACTORY.lotteryClassifyList[0]).count==0) {
        BET_CONFIG.kind_type = LotteryKind_CREDIT;
        addCreditChild();
    }else{
        LotteryKind kind_type = check_Status(gameId);
        BET_CONFIG.kind_type = kind_type;
        if (kind_type==LotteryKind_EXIST) {
            (creditType)?addCreditChild():addOfficalChild();
        }else{
            (kind_type==LotteryKind_CREDIT)?addCreditChild():addOfficalChild();
        }
    }
    [totalC pushKindTypeByParameterNumber:[NSString stringWithFormat:@"%@",gameId]];
    [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:totalC animated:YES];
}
- (NSString *)getPushGameCurrenToken{
    __block NSString *token = nil;
    NSHTTPCookieStorage *cookiesManager = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    [cookiesManager.cookies enumerateObjectsUsingBlock:^(NSHTTPCookie * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.name isEqualToString:@"token"]) {
            token = [NSString stringWithFormat:@"%@=%@",obj.name,obj.value];
        }
    }];
    return token;
}
@end
