//
//  UserDataManager.h
//  Bet365
//
//  Created by HHH on 2018/7/7.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserInfoData.h"
#import "AuthData.h"
#import "RegisterInfoData.h"
#import "CongfigClusterData.h"
#import "Bet365Config.h"
#import "LukePushMessageItem.h"
#import "LukeNoticeItem.h"
#import "RegisterConfig.h"
#import "TimerManager.h"
#import "ImmediateModel.h"


#define USER_DATA_MANAGER [UserDataManager shareManager]

@interface UserDataManager : NSObject<TimerManagerDelegate>

@property (nonatomic,strong) UserInfoData *userInfoData;

@property (nonatomic,assign) NSUInteger deliverCount;

@property (nonatomic,assign) NSUInteger pursueCount;

@property (nonatomic,assign) NSUInteger messegesCount;
@property (nonatomic,strong) NSArray <ImmediateModel *> *Immediatedatas;
@property (nonatomic,assign) NSUInteger noticeCount;
/**
 是否登录
 
 @return BOOL
 */
@property (nonatomic,assign) BOOL isLogin;
+(instancetype)shareManager;
/**
 清空数据
 */
-(void)cleanData;
/**
 跳转至登录页面
 */
-(BOOL)shouldPush2Login;
/**
 退出
 */
-(void)loginOutCompleted:(void(^)(BOOL success))completed;




/**
 请求注册是否需要验证码

 */
-(void)requestGetRegisterLimit:(void(^)(BOOL success))completed;
/**
 请求用户信息
 
 @param completed 回调
 */
-(void)requestGetUserInfoCompleted:(void (^)(BOOL success))completed;



/**
 登陆

 @param paramers 参数
 @param completed 请求回掉
 @param needChangePswHandle 强制修改密码回掉
 */
-(void)requestLoginWithParamers:(NSDictionary *)paramers
                       Completed:(void(^)(BOOL success,NSError *error))completed
             NeedChangePswHandle:(void(^)(BOOL success))needChangePswHandle;

-(void)requestLoginFV:(NSString *)fv Completed:(void(^)(BOOL success,NSError *error))completed
NeedChangePswHandle:(void(^)(BOOL success))needChangePswHandle;

/**
 获取登录验证码
 
 */
-(void)getVCodeImgByRandom:(BOOL)isRandom Completed:(void(^)(UIImage * codeImg))completed;



/**
 获取金币（轮询）
 tips:此接口可判断互踢
 
 */
-(void)requestUserStatusCompleted:(void (^)(BOOL success))completed;


/**
 注册

 @param info 注册信息
 @param completed <#completed description#>
 */
-(void)requestRegisteBy:(RegisterInfoData *)info
              Completed:(void(^)(BOOL success))completed;

/**
 试玩注册

 @param completed <#completed description#>
 */
-(void)requestTestRegister:(void(^)(BOOL success))completed;
-(void)requestTestRegisterByPassword:(NSString *)password ValiCode:(NSString *)valiCode Completed:(void(^)(BOOL success))completed;


/**
 找回密码

 @param userAccount 用户找好
 @param cashPassword 取款密码
 @param yzmNum 验证码
 @param completed 成功回调
 */
-(void)requestFundPassWordByUserAccount:(nonnull NSString *)userAccount
                           CashPassword:(nonnull NSString*)cashPassword
                                  vCode:(nonnull NSString *)yzmNum
                              Completed:(void(^)(BOOL success))completed;


/**
 修改密码

 @param oldPwd 旧密码
 @param newPwd 新密码
 @param completed 成功回调
 */
-(void)requestUpdatePassWordByOldPwd:(nonnull NSString *)oldPwd
                              NewPwd:(nonnull NSString *)newPwd
                           Completed:(void(^)(BOOL success))completed;
/**
 获取投注记录数

 @param completed 回调个数
 */
-(void)requestGetDeliverUnreadCount:(void(^)(NSInteger count))completed;


/**
 获取追号记录

 @param completed 回调个数
 */
-(void)requestGetPursueUnreadCount:(void(^)(NSInteger count))completed;


/**
 游戏公告列表

 @param completed 回调集合
 */
-(void)requestGetNoticeList:(void(^)(NSArray <LukeNoticeItem *>*list))completed;

/**
 登陆成功
 
 @param completed 登陆成功后进行信息回调
 */
-(void)storageCurrenLoginMessage:(id)responseObject forCompleted:(void(^)(BOOL success))completed;


/**
 执行签到

 @param completed 成功回掉
 */
-(void)requestSubmitSigin:(void(^)(BOOL success,NSUInteger addGrowth))completed;
@end
