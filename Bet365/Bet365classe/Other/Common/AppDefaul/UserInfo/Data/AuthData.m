//
//  AuthData.m
//  Bet365
//
//  Created by HHH on 2018/7/7.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "AuthData.h"

@implementation AuthData

+ (instancetype)shareAuthData
{
    static AuthData *data = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        data = [[AuthData alloc] init];
    });
    return data;
}

- (NSString *)tulongGame
{
    if (!_tulongGame) {
        _tulongGame = @"22,26,70,71,45";
    }
    return _tulongGame;
}

@end
