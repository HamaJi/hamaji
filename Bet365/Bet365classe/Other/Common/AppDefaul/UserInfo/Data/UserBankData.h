//
//  UserBankData.h
//  Bet365
//
//  Created by HHH on 2018/8/8.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "AppDefaults.h"

@interface UserBankData : AppDefaults
@property(nonatomic,copy)NSString * userId;
@property(nonatomic,copy)NSString *bankName;
@property(nonatomic,copy)NSString *subAddress;
@property(nonatomic,copy)NSString *cardNo;
@property(nonatomic,copy)NSString *addTime;
@property(nonatomic,copy)NSString *updateTime;
@end
