//
//  UserData.h
//  Bet365
//
//  Created by HHH on 2018/7/7.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDefaults.h"
#import "UserBankData.h"
@interface UserInfoData : AppDefaults

@property (nonatomic,copy) NSString *cookie;

#pragma mark - userInfo

@property (nonatomic,strong) NSString *cookieSession;
/**
 账号
 */
@property (nonatomic, copy) NSString * account;

/**
 手机型号
 */
@property (nonatomic, copy) NSString * deviceType;

/**
 登录时间
 */
@property (nonatomic, copy) NSString * loginDate;


/**
 最后一次请求时间
 */
@property (nonatomic, copy) NSString * lastDate;


@property (nonatomic, copy) NSString * type;

/**
 登录IP
 */
@property (nonatomic, copy) NSString * loginIp;

/**
 昵称
 */
@property (nonatomic, copy) NSString * name;

/**
 凭证
 */
@property (nonatomic, copy) NSString * token;


/**
 <#Description#>
 */
@property (nonatomic, copy) NSNumber *expiresIn;

/**
 余额
 */
@property (nonatomic, strong) NSNumber *money;

/**
 用户id
 */
@property (nonatomic, copy) NSNumber *uid;
@property (nonatomic, copy) NSNumber *userId;

@property (nonatomic, copy) NSString *loginPhone;



@property (nonatomic, copy) NSString * phone;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSNumber * superID;
@property (nonatomic, copy) NSString *superName;
@property (nonatomic, copy) NSString *superPath;
@property (nonatomic, copy) NSNumber * state;
@property (nonatomic, copy) NSNumber * limitBet;
@property (nonatomic, copy) NSNumber * regWay;
@property (nonatomic, copy) NSString *regIP;
@property (nonatomic, copy) NSString *addTime;
@property (nonatomic, copy) NSString *browser;
@property (nonatomic, copy) NSString *operatingSystem;
@property (nonatomic, copy) NSString * userAgent;
@property (nonatomic, copy) NSString * wxUnionid;
@property (nonatomic, copy) NSString * gender;
@property (nonatomic, copy) NSString *hyLevel;
@property (nonatomic, copy) NSString *qq;
@property (nonatomic, copy) NSString *weixin;
@property (nonatomic, copy) NSString *receive;
@property (nonatomic, copy) NSString *intrCode;
@property (nonatomic, copy) NSString *recommendCode;
@property (nonatomic, copy) NSString *birthday;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *perfectInfo;
@property (nonatomic, copy) NSNumber * isDayWage;
@property (nonatomic, copy) NSNumber * tableIndex;
@property (nonatomic, copy) NSNumber * limitRech;
@property (nonatomic, copy) NSString *limited;
@property (nonatomic, copy) NSNumber * dlLevel;
@property (nonatomic, assign) BOOL  isDl;
@property (nonatomic, copy) NSNumber * rebate;
@property (nonatomic, copy) NSString *userMemo;
@property (nonatomic, copy) NSString *userColor;
@property (nonatomic, copy) NSString *userAmount;
@property (nonatomic, strong) NSNumber * transferStatus;

#pragma mark - extInfo

@property (nonatomic, copy) NSString * fullName;
@property (nonatomic, copy) NSNumber * points;
@property (nonatomic, copy) NSString * loginTime;
@property (nonatomic, copy) NSString * lastLoginTime;
@property (nonatomic, copy) NSString * lastLoginIP;
@property (nonatomic, copy) NSString * fk;
@property (nonatomic, copy) NSNumber * rechCount;
@property (nonatomic, copy) NSNumber * rechMoney;
@property (nonatomic, copy) NSString * rechTime;
@property (nonatomic, copy) NSNumber * uwCount;
@property (nonatomic, copy) NSNumber * uwMoney;
@property (nonatomic, copy) NSString * uwTime;
@property (nonatomic, copy) NSString * updateTime;
@property (nonatomic, copy) NSNumber * lowerNum;
@property (nonatomic, copy) NSNumber * teamNum;
@property (nonatomic, copy) NSString * incomeMoney;
@property (nonatomic, copy) NSNumber * requireDML;
@property (nonatomic, copy) NSString * relaxQuota;
@property (nonatomic, copy) NSString * fundPwd;
@property (nonatomic, copy) NSString * loanMoney;

#pragma mark - Other

@property (nonatomic, strong) UserBankData *userBank;

@property (nonatomic, copy) NSNumber * unReadCount;


/**
 BOOL
 */
@property (nonatomic, copy) NSNumber * isDefaultPassword;

#pragma mark - 蛤蟆吉自定义个人中心
@property (nonatomic, copy) NSNumber *growthLevel;

@property (nonatomic, copy) NSNumber *growthValue;

@end
