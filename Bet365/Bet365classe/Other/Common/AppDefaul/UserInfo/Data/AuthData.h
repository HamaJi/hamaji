//
//  AuthData.h
//  Bet365
//
//  Created by HHH on 2018/7/7.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "AppDefaults.h"

#define AUTHDATA_MANAGER [AuthData shareAuthData]

@interface AuthData : AppDefaults

+ (instancetype)shareAuthData;

/** 账号**/
@property (nonatomic,copy) NSString *account;
/** 密码**/
@property (nonatomic,copy) NSString *passWord;

@property (nonatomic,copy) NSString *phone;
/** 是否记住密码 */
@property (nonatomic,assign) BOOL isRemember;
/** 是否精简模式 */
@property (nonatomic,assign) BOOL streamline;
/**当前titleLogoUrl*/
@property (nonatomic,copy) NSString *logoUrl;
/** 是否跳转Safari */
@property (nonatomic,assign) BOOL isOpenSafari;
@property (nonatomic,copy) NSString *intr;
/** 彩票信用是否预设金额 */
@property (nonatomic,assign) BOOL preset;
/** 预设金额 */
@property (nonatomic,copy) NSString *presetMoney;
/** 屠龙榜 */
@property (nonatomic,copy) NSString *tulongGame;

@end
