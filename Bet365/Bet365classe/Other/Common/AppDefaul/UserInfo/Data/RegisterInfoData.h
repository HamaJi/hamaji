//
//  RegisterInfoData.h
//  Bet365
//
//  Created by HHH on 2018/7/13.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "AppDefaults.h"

@interface RegisterInfoData : AppDefaults

@property (nonatomic,strong)NSString *account;

@property (nonatomic,strong)NSString *password;

@property (nonatomic,strong)NSString *confirmPassword;

@property (nonatomic,strong)NSString *fundPwd;

@property (nonatomic,strong)NSString *intrCode;

@property (nonatomic,strong)NSString *vCode;

@property (nonatomic,strong)NSString *fullName;

@property (nonatomic,strong)NSString *birthday;

@property (nonatomic,strong)NSString *email;

@property (nonatomic,strong)NSString *phone;

@property (nonatomic,strong)NSString *qq;

@property (nonatomic,strong)NSString *weixin;

@property (nonatomic,strong)NSString *geetest_challenge;;

@property (nonatomic,strong)NSString *geetest_seccode;

@property (nonatomic,strong)NSString *geetest_validate;

@property (nonatomic,strong)NSString *Ticket;

@property (nonatomic,strong)NSString *Randstr;

@property (nonatomic,strong)NSString *registerPhoneCode;

@end
