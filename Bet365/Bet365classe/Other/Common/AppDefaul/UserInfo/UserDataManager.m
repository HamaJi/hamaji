//
//  UserDataManager.m
//  Bet365
//
//  Created by HHH on 2018/7/7.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "UserDataManager.h"
#import "NotificationManager.h"
#import "NotificationName.h"
#import "NetWorkMannager+Account.h"
#import "LukeMaskView.h"
#import <MJExtension/MJExtension.h>
#import "toolTimer.h"
#import "ForceUpdatePswViewController.h"

#import "UserSignInEntity+CoreDataClass.h"

static UserDataManager *instance = nil;
@interface UserDataManager()<UIViewControllerTransitioningDelegate>
@property (nonatomic,strong)NSDictionary *infoData;
@end
@implementation UserDataManager

+(instancetype)shareManager{
    
    static dispatch_once_t userDataManagerOnceToken;
    dispatch_once(&userDataManagerOnceToken, ^{
        instance = [[super alloc] init];
    });
    return instance;
}

- (instancetype)init{
    if (self = [super init]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self requestGetUserInfoCompleted:nil];
            [self starRunLoop];
            [TIMER_MANAGER addDelegate:self];
            [self observeLoginStatus];
        });
    }
    return self;
}

+(void)destory
{
    instance = nil;
}
#pragma mark - TimerManagerDelegate
-(void)cycleTimerWithKey:(NSString *)key RestCount:(NSInteger)count{
    
    
    
    if ([key isEqualToString:TIMER_USER_STATUS]) {
        [self requestUserStatusCompleted:^(BOOL success) {
            [NOTI_MANAGER postNotificationWithName:NOTIF_USERINFO_REFRESH Object:nil];
        }];
    }else if ([key isEqualToString:IMMEDIATE_TIME]) {
        [NET_DATA_MANAGER requestGetImmediateMessageSuccess:^(id responseObject) {
            self.Immediatedatas = [ImmediateModel mj_objectArrayWithKeyValuesArray:responseObject];
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIViewController routerJumpToUrl:kRouterImmediate];
            });
            if (self.Immediatedatas.count > 0) {
                [NET_DATA_MANAGER requestPostUpdateMessageStatusSuccess:^(id responseObject) {
                    
                } failure:^(NSError *error) {
                    
                }];
            }
        } failure:^(NSError *error) {
            
        }];
    }
}
#pragma mark - Public
/**
 跳转至登录页面
 */
-(BOOL)shouldPush2Login{
    if (!self.isLogin) {
        UIViewController *vc = [NAVI_MANAGER getCurrentVC];
        if ([vc isKindOfClass:[UIViewController class]]) {
            jesseLogBet365ViewController *vc = [[jesseLogBet365ViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
            
        }
    }
    return self.isLogin;
}


-(void)cleanData{
    [[NSHTTPCookieStorage sharedHTTPCookieStorage].cookies enumerateObjectsUsingBlock:^(NSHTTPCookie * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:obj];
    }];
    [CHAT_UTIL clean];
    [self.userInfoData cleanUserData];
    self.deliverCount = 0;
    self.pursueCount = 0;
    self.messegesCount = 0;
    [self stopRunloop];
    
    [NOTI_MANAGER postNotificationWithName:NOTIF_USER_LOGOUT Object:nil];
}
/**
 退出
 */
-(void)loginOutCompleted:(void(^)(BOOL success))completed{
    [[NAVI_MANAGER getCurrentVC].navigationController popToRootViewControllerAnimated:NO];
    dispatch_async(dispatch_get_main_queue(), ^{
        [JesseAppdelegate.tabbar setSelectedIndex:0];
    });
    completed ? completed (YES) : nil;
    [NET_DATA_MANAGER requestLogoutSuccess:^(id responseObject) {
        [self cleanData];
        BET_CONFIG.userSettingData.fv ? BET_CONFIG.userSettingData.fv = nil : nil;
        [BET_CONFIG.userSettingData save];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

/**
 获取登录验证码
 
 */
-(void)getVCodeImgByRandom:(BOOL)isRandom Completed:(void(^)(UIImage * codeImg))completed{
    
    [NET_DATA_MANAGER requestGetVCodeByRandom:isRandom Success:^(id responseObject) {
        if (responseObject) {
            completed ? completed(responseObject) : nil;
        }else{
            completed ? completed(nil) : nil;
        }
    } failure:^(NSError *error) {
        completed ? completed(nil) : nil;
    }];
    
}

#pragma mark - Private
-(void)observeLoginStatus{
    [[RACObserve(self.userInfoData, token) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        self.isLogin = (x != nil);
    }];
}
-(void)starRunLoop{
    if (!self.isLogin)
        return;
    [TIMER_MANAGER addCycleTimerWithKey:TIMER_USER_STATUS andReduceScope:7];
    [TIMER_MANAGER addCycleTimerWithKey:IMMEDIATE_TIME andReduceScope:7];
}
-(void)stopRunloop{
    [[toolTimer sharedInstance] cancelTimerWithName:@"lotteryCutDown"];//停止任务性定时器
    [[toolTimer sharedInstance] cancelTimerWithName:@"timeViewCaculte"];//停止任务性定时器
    [TIMER_MANAGER removeTimerWithKey:TIMER_USER_STATUS];
    [TIMER_MANAGER removeTimerWithKey:IMMEDIATE_TIME];
}

#pragma mark - UIViewControllerTransitioningDelegate
- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source{
    
    return [PresentingTopAnimator new];
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    return [DismissingTopAnimator new];
}

#pragma mark - GET/SET
-(BOOL)isLogin{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _isLogin = (self.userInfoData.account != nil && self.userInfoData.token);
    });
    return _isLogin;
}
-(UserInfoData *)userInfoData{
    if (!_userInfoData) {
        _userInfoData = [[UserInfoData alloc] init];
    }
    return _userInfoData;
}

#pragma mark - NetRequest

/**
 请求用户信息
 
 @param completed 回调
 */
-(void)requestGetUserInfoCompleted:(void (^)(BOOL success))completed{
    if (!self.isLogin){
        completed ? completed(NO) : nil;
        return;
    }
    [NET_DATA_MANAGER requestInfoUrlSuccess:^(id responseObject) {
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
        [[responseObject allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull key, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([dic[key] isKindOfClass:[NSDictionary class]] && ![key isEqualToString:@"userBank"]) {
                [dic removeObjectForKey:key];
            }
        }];
        [[responseObject allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull key, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([responseObject[key] isKindOfClass:[NSDictionary class]]) {
                NSMutableDictionary *data = [[responseObject objectForKey:key] mutableCopy];
                if ([key isEqualToString:@"extInfo"]) {
                    [data removeObjectForKey:@"fullName"];
                }
                [dic setValuesForKeysWithDictionary:data];
            }
        }];
        
        [self.userInfoData setDataValueForm:dic];
        [self.userInfoData save];
        
        completed ? completed(YES) : nil;
    } failure:^(NSError *error) {
        completed ? completed(NO) : nil;
        NSLog(@"%@",error);
    }];
}





-(void)requestGetRegisterLimit:(void(^)(BOOL success))completed{
    [NET_DATA_MANAGER requestRegisterLimitSuccess:^(id responseObject) {
        completed ? completed(([responseObject[@"register"] integerValue] == 1)) : nil;
    } failure:^(NSError *error) {
        completed ? completed(NO) : nil;
    }];
}



/**
 登陆

 @param paramers 登陆参数
 @param completed 成功回掉
 @param needChangePswHandle 强制修改密码回掉
 */
-(void)requestLoginWithParamers:(NSDictionary *)paramers
                     Completed:(void(^)(BOOL success,NSError *error))completed
           NeedChangePswHandle:(void(^)(BOOL success))needChangePswHandle{
    
    void(^loginRequestSuccess)(id responseObject) = ^(id responseObject){
        [self setCookieTokenStorage:LoginUrl];
        [self setFv:responseObject[@"fv"]];
        [self starRunLoop];
        [self storageCurrenLoginMessage:responseObject forCompleted:^(BOOL success) {
            completed?completed(success,nil):nil;
        }];
    };
    void(^loginRequestFailed)(NSError *error) = ^(NSError *error){
        [SVProgressHUD dismiss];
        if (error.needChangePsw) {
            /*
             登录后强制修改密码
             接口400依旧可获得 Cookie
             */
            @weakify(self);
            [Bet365AlertSheet showChooseAlert:@"请立即进行密码修改" Message:error.msg Items:@[@"确定"] Handler:^(NSInteger index) {
                [self setCookieTokenStorage:LoginUrl];
                __block ForceUpdatePswViewController *vc = [[ForceUpdatePswViewController alloc] initWithNibName:@"ForceUpdatePswViewController" bundle:nil];
                vc.transitioningDelegate = self;
                vc.modalPresentationStyle = UIModalPresentationCustom;
                [[NAVI_MANAGER getCurrentVC] presentViewController:vc animated:YES completion:nil];
                vc.submitHandle = ^(NSString *newPsw) {
                    @strongify(self);
                    NSString *psw = newPsw;
                    if (BET_CONFIG.config.user_password_type && [BET_CONFIG.config.user_password_type isEqualToString:@"0"]) {
                        psw = [newPsw MD5];
                    }
                    [self requestForceUpDataPsw:psw Completed:^(BOOL success) {
                        [self cleanData];
                        needChangePswHandle ? needChangePswHandle(success) : nil;
                    }];
                    
                };
                vc.serverHandle = ^{
                    needChangePswHandle ? needChangePswHandle(NO) : nil;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [UIViewController routerJumpToUrl:kRouterZXKF];
                        
                    });
                };
            }];
            
            //            _storageLogin(para);
            return;
        }
        
        completed ? completed(NO,error) : nil;
    };
    [SVProgressHUD showWithStatus:@"登录中"];
    if ([paramers containsObjectForKey:@"account"] &&
        [paramers containsObjectForKey:@"password"]) {
        [NET_DATA_MANAGER requestLoginUrlParameters:paramers success:^(id responseObject) {
            loginRequestSuccess(responseObject);
        } failure:^(NSError *error) {
            loginRequestFailed(error);
        }];
    }else if ([paramers containsObjectForKey:@"phone"] &&
              [paramers containsObjectForKey:@"code"]){
        [NET_DATA_MANAGER requestLoginPhoneUrlParameters:paramers success:^(id responseObject) {
            loginRequestSuccess(responseObject);
        } failure:^(NSError *error) {
            loginRequestFailed(error);
        }];
    }
}

-(void)requestLoginFV:(NSString *)fv Completed:(void(^)(BOOL success,NSError *error))completed
NeedChangePswHandle:(void(^)(BOOL success))needChangePswHandle{
    void(^loginRequestSuccess)(id responseObject) = ^(id responseObject){
        [self setCookieTokenStorage:LoginUrl];
        [self setFv:responseObject[@"fv"]];
        [self starRunLoop];
        [self storageCurrenLoginMessage:responseObject forCompleted:^(BOOL success) {
            completed?completed(success,nil):nil;
        }];
    };
    void(^loginRequestFailed)(NSError *error) = ^(NSError *error){
        [SVProgressHUD dismiss];
        BET_CONFIG.userSettingData.fv ? BET_CONFIG.userSettingData.fv = nil : nil;
        [BET_CONFIG.userSettingData save];
        if (error.needChangePsw) {
            /*
             登录后强制修改密码
             接口400依旧可获得 Cookie
             */
            @weakify(self);
            [Bet365AlertSheet showChooseAlert:@"请立即进行密码修改" Message:error.msg Items:@[@"确定"] Handler:^(NSInteger index) {
                [self setCookieTokenStorage:LoginUrl];
                __block ForceUpdatePswViewController *vc = [[ForceUpdatePswViewController alloc] initWithNibName:@"ForceUpdatePswViewController" bundle:nil];
                vc.transitioningDelegate = self;
                vc.modalPresentationStyle = UIModalPresentationCustom;
                [[NAVI_MANAGER getCurrentVC] presentViewController:vc animated:YES completion:nil];
                vc.submitHandle = ^(NSString *newPsw) {
                    @strongify(self);
                    NSString *psw = newPsw;
                    if (BET_CONFIG.config.user_password_type && [BET_CONFIG.config.user_password_type isEqualToString:@"0"]) {
                        psw = [newPsw MD5];
                    }
                    [self requestForceUpDataPsw:psw Completed:^(BOOL success) {
                        [self cleanData];
                        needChangePswHandle ? needChangePswHandle(success) : nil;
                    }];
                    
                };
                vc.serverHandle = ^{
                    needChangePswHandle ? needChangePswHandle(NO) : nil;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [UIViewController routerJumpToUrl:kRouterZXKF];
                        
                    });
                };
            }];
            
            //            _storageLogin(para);
            return;
        }
        
        completed ? completed(NO,error) : nil;
    };
    [SVProgressHUD showWithStatus:@"自动登录中"];
    [NET_DATA_MANAGER requestLoginFV:fv success:^(id responseObject) {
        loginRequestSuccess(responseObject);
    } failure:^(NSError *error) {
        loginRequestFailed(error);
    }];
    
}

-(void)setRequestCookies{
    NSHTTPCookieStorage* cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray<NSHTTPCookie *> *cookies = [cookieStorage cookiesForURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SerVer_Url,LoginUrl]]];
    [cookies enumerateObjectsUsingBlock:^(NSHTTPCookie * _Nonnull cookie, NSUInteger idx, BOOL * _Nonnull stop) {
        
        NSMutableDictionary *properties = [[cookie properties] mutableCopy];
        //将cookie过期时间设置为一年
        NSDate *expiresDate = [NSDate dateWithTimeIntervalSinceNow:3600*24*30*12];
        properties[NSHTTPCookieExpires] = expiresDate;
        //删除Cookies的discard字段，应用退出，会话结束的时候继续保留Cookies
        [properties removeObjectForKey:NSHTTPCookieDiscard];
        //重新设置改动后的Cookies
        [cookieStorage setCookie:[NSHTTPCookie cookieWithProperties:properties]];
        if ([properties[NSHTTPCookieName] isEqualToString:@"token"]) {
            NSString *token =properties[NSHTTPCookieValue];
            self.userInfoData.token = token;
        }
    }];
    
}

- (void)storageCurrenLoginMessage:(id)responseObject forCompleted:(void(^)(BOOL success))completed{
    BET_CONFIG.userSettingData.hasLogin = YES;
    [BET_CONFIG.userSettingData save];
    [self.userInfoData setDataValueForm:responseObject];
    
//    [CHAT_UTIL getChatClienInfo:^(ChatInfoModel *info) {
//        
//    }];
    //登录成功后请求用户个人信息
    [self  requestGetUserInfoCompleted:^(BOOL success) {
        
        if (success) {
            [SVProgressHUD showSuccessWithStatus:@"登陆成功"];
            [jesseGuideUIConfig initShareManager].alertValue = NO;
            //            if (BET_CONFIG.noticeModel.login_notice.count > 0) {
            //                NoticeModel *model = [BET_CONFIG.noticeModel.login_notice safeObjectAtIndex:0];
            //                [Bet365NoticeView showWithNoticeTitle:model.noticeTitle noticeContent:model.noticeContent];
            //            }
            
        }else{
            [SVProgressHUD showErrorWithStatus:@"用户信息请求失败，请重新登录"];
            [self cleanData];
        }
        completed(success);
    }];
}
-(void)requestRegisteBy:(RegisterInfoData *)info
              Completed:(void(^)(BOOL success))completed{
    
    [NET_DATA_MANAGER requestRegisterUrlParameters:[info dictFromData] success:^(id responseObject) {
        /** 认证信息**/
        AUTHDATA_MANAGER.account = info.account;
        AUTHDATA_MANAGER.passWord = nil;
        AUTHDATA_MANAGER.isRemember = NO;
        [AUTHDATA_MANAGER save];
        BET_CONFIG.userSettingData.hasLogin = YES;
        [BET_CONFIG.userSettingData save];
        [self setCookieTokenStorage:RegisterUrl];
        self.userInfoData.account = info.account;
        [self.userInfoData save];
        //登录成功后请求用户个人信息
//        [CHAT_UTIL getChatClienInfo:^(ChatInfoModel *info) {
//            
//        }];
        [self requestGetUserInfoCompleted:nil];
        [self starRunLoop];
        completed ? completed(YES) : nil;
    } failure:^(NSError *error) {
        completed ? completed(NO) : nil;
    }];
}

-(void)requestTestRegister:(void(^)(BOOL success))completed{
    //    [JesseAppdelegate showHudLoadby:@"注册中..."];
    [SVProgressHUD showWithStatus:@"注册中..."];
    [NET_DATA_MANAGER requestTextRegisterUrlSuccess:^(id responseObject) {
//        [JesseAppdelegate showLoadDismiss];
        [SVProgressHUD dismiss];
        [SVProgressHUD showSuccessWithStatus:@"注册成功"];
//        [JesseAppdelegate showImageLoad:@"success" AnddismissDelay:1.5f AndDesText:@"注册成功"];
        /** 用户信息**/
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
        [self.userInfoData setDataValueForm:data];
        
        BET_CONFIG.userSettingData.hasLogin = YES;
        [BET_CONFIG.userSettingData save];
        
        [self setCookieTokenStorage:TestPlayGameUrl];
        
        
        if (BET_CONFIG.noticeModel.login_notice.count > 0) {
            NoticeModel *model = [BET_CONFIG.noticeModel.login_notice safeObjectAtIndex:0];
            [Bet365NoticeView showWithNoticeTitle:model.noticeTitle noticeContent:model.noticeContent];
        }
        [self.userInfoData save];
        [[NAVI_MANAGER getCurrentVC].navigationController popToRootViewControllerAnimated:YES];
        //登录成功后请求用户个人信息
        [self starRunLoop];
//        [CHAT_UTIL getChatClienInfo:^(ChatInfoModel *info) {
//            
//        }];
        [self requestGetUserInfoCompleted:^(BOOL success) {
            completed ? completed(YES) : nil;
        }];
    }failure:^(NSError *error) {
        [SVProgressHUD dismiss];
//        [JesseAppdelegate showLoadDismiss];
        completed ? completed(NO) : nil;
    }];
}

-(void)requestTestRegisterByPassword:(NSString *)password ValiCode:(NSString *)valiCode Completed:(void(^)(BOOL success))completed{
//    [JesseAppdelegate showHudLoadby:@"注册中..."];
    [SVProgressHUD showWithStatus:@"注册中..."];
    [NET_DATA_MANAGER requestTestRegisterUserByPassword:password ValiCode:valiCode success:^(id responseObject) {
        [SVProgressHUD dismiss];
        [SVProgressHUD showSuccessWithStatus:@"注册成功"];
//        [JesseAppdelegate showLoadDismiss];
//        [JesseAppdelegate showImageLoad:@"success" AnddismissDelay:1.5f AndDesText:@"注册成功"];
        /** 用户信息**/
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
        [self.userInfoData setDataValueForm:data];
        
        BET_CONFIG.userSettingData.hasLogin = YES;
        [BET_CONFIG.userSettingData save];
        
        [self setCookieTokenStorage:REGISTER_TESTUSER_URL];
        if (BET_CONFIG.noticeModel.login_notice.count > 0) {
            NoticeModel *model = [BET_CONFIG.noticeModel.login_notice safeObjectAtIndex:0];
            [Bet365NoticeView showWithNoticeTitle:model.noticeTitle noticeContent:model.noticeContent];
        }
        [self.userInfoData save];
        [[NAVI_MANAGER getCurrentVC].navigationController popToRootViewControllerAnimated:YES];
        //登录成功后请求用户个人信息
        [self starRunLoop];
//        [CHAT_UTIL getChatClienInfo:^(ChatInfoModel *info) {
//            
//        }];
        [self requestGetUserInfoCompleted:^(BOOL success) {
            completed ? completed(YES) : nil;
        }];
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
//        [JesseAppdelegate showLoadDismiss];
        completed ? completed(NO) : nil;
    }];
    
}

/**
 获取金币（轮询）
 tips:此接口可判断互踢
 
 */
-(void)requestUserStatusCompleted:(void (^)(BOOL success))completed{
    [NET_DATA_MANAGER requestTimeLoopStatusSuccess:^(id responseObject) {
        if (![responseObject isKindOfClass:[NSDictionary class]]) {
            completed ? completed(NO) : nil;
            return;
        }
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
        self.messegesCount = [dic[@"message"] integerValue];
        [self.userInfoData setDataValueForm:dic];

        [self.userInfoData save];
        
        completed ? completed(YES) : nil;
    } failure:^(NSError *error) {
        completed ? completed(NO) : nil;
        NSLog(@"%@",error);
    }];
}

-(void)requestFundPassWordByUserAccount:(nonnull NSString *)userAccount
                           CashPassword:(nonnull NSString*)cashPassword
                                  vCode:(nonnull NSString *)yzmNum
                              Completed:(void(^)(BOOL success))completed{
    [NET_DATA_MANAGER requestFundPwdByUserAccount:userAccount CashPassword:cashPassword vCode:yzmNum Success:^(id responseObject) {
        [SVProgressHUD dismiss];
        [[NAVI_MANAGER getCurrentVC].navigationController popToRootViewControllerAnimated:YES];
        [LukeMaskView lukeMaskViewWithTitle:@"提交申请成功，请及时与客户人员联系"];
        completed ? completed(YES) : nil;
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        completed ? completed(NO) : nil;
    }];
}

-(void)requestUpdatePassWordByOldPwd:(nonnull NSString *)oldPwd
                              NewPwd:(nonnull NSString *)newPwd
                           Completed:(void(^)(BOOL success))completed{
    
}

-(void)requestGetDeliverUnreadCount:(void(^)(NSInteger count))completed{
    [NET_DATA_MANAGER requestGetDeliverByAccount:nil Page:1 Rows:100 StartDate:[[NSDate date] formatYMD] EndDate:[[NSDate date] formatYMD] gameId:nil status:nil model:nil result:nil Success:^(id responseObject) {
        NSUInteger count = [((NSDictionary *)responseObject)[@"totalCount"] integerValue];
        self.deliverCount = count;
        completed ? completed(count) : nil;
    } failure:^(NSError *error) {
        completed ? completed(0) : nil;
    }];
}

-(void)requestGetPursueUnreadCount:(void(^)(NSInteger count))completed{
    
    [NET_DATA_MANAGER requestGetPursueByAccount:nil Page:1 Rows:1 StartDate:nil EndDate:nil gameId:nil result:nil Success:^(id responseObject) {
        NSUInteger count = [((NSDictionary *)responseObject)[@"totalCount"] integerValue];
        self.pursueCount = count;
        completed ? completed(count) : nil;
    } failure:^(NSError *error) {
        completed ? completed(0) : nil;
    }];
}

-(void)requestGetNoticeList:(void(^)(NSArray <LukeNoticeItem *>*list))completed{
    [NET_DATA_MANAGER requestGetNoticeListSuccess:^(id responseObject) {
        NSArray <LukeNoticeItem *>*list = [LukeNoticeItem mj_objectArrayWithKeyValuesArray:responseObject];
        self.noticeCount = list.count;
        completed ? completed(list) : nil;
    } failure:^(NSError *error) {
        completed ? completed(nil) : nil;
    }];
}

-(void)requestForceUpDataPsw:(NSString *)newPsw Completed:(void(^)(BOOL success))completed{
    [NET_DATA_MANAGER requestForceChangeNewPsw:newPsw Success:^(id responseObject) {
        completed ? completed(YES) : nil;
    } failure:^(NSError *error) {
        completed ? completed(NO) : nil;
    }];
}

-(void)requestSubmitSigin:(void(^)(BOOL success,NSUInteger addGrowth))completed{
    NSManagedObjectContext *contex = [NSManagedObjectContext MR_defaultContext];
    void(^creatEntityHandle)(id responseObject) = ^(id responseObject){
        if (responseObject[@"addDesc"]) {
            [SVProgressHUD showSuccessWithStatus:responseObject[@"addDesc"]];
        }
        NSUInteger addGrowth = [responseObject[@"addGrowth"] integerValue];
        
        UserSignInEntity *entity = [UserSignInEntity MR_createEntityInContext:contex];
        @weakify(self);
        [contex MR_saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
            @strongify(self);
            UserSignInEntity *localentity = [entity MR_inContext:localContext];
            localentity.account = self.userInfoData.account;
            localentity.signinTime = [NSDate date];
        }];
        completed ? completed(responseObject != nil,addGrowth) : nil;
    };
    [NET_DATA_MANAGER requestSubmitSiginInsuccess:^(id responseObject) {
        
        creatEntityHandle(responseObject);
    } failure:^(NSError *error) {
        if (error.hasSignIn) {
            creatEntityHandle(nil);
        }else{
            completed ? completed(NO,0) : nil;
        }
    }];
    
}

-(void)setCookieTokenStorage:(NSString *)url{
    //** 用户信息**/
    NSHTTPCookieStorage* cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSMutableArray <NSHTTPCookie *> *cookies = [cookieStorage cookiesForURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SerVer_Url,url]]].mutableCopy;
    
    [cookies enumerateObjectsUsingBlock:^(NSHTTPCookie * _Nonnull cookie, NSUInteger idx, BOOL * _Nonnull stop) {
        NSMutableDictionary *properties = [[cookie properties] mutableCopy];
        
        //将cookie过期时间设置为一年
        NSDate *expiresDate = [NSDate dateWithTimeIntervalSinceNow:3600*24*30*12];
        properties[NSHTTPCookieExpires] = expiresDate;
        //删除Cookies的discard字段，应用退出，会话结束的时候继续保留Cookies
        [properties removeObjectForKey:NSHTTPCookieDiscard];
        //重新设置改动后的Cookies
        [cookieStorage setCookie:[NSHTTPCookie cookieWithProperties:properties]];
        if ([properties[NSHTTPCookieName] isEqualToString:@"token"]) {
            NSString *token =properties[NSHTTPCookieValue];
            self.userInfoData.token = token;
        }
    }];
}

-(void)setFv:(NSString *)fv{
    BET_CONFIG.userSettingData.fv = fv;
    [BET_CONFIG.userSettingData save];
}
@end

