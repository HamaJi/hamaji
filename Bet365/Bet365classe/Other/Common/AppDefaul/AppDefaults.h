//
//  AppDefaults.h
//  Bet365
//
//  Created by HHH on 2018/7/7.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppDefaults : NSObject

/**
 保存
 */
-(void)save;

/**
 清空
 */
-(void)cleanUserData;

+(void)removeAllDefaults;

/**
 从缓存读取
 */
-(void)getDataAtUserDefaults;

/**
 kvc
 
 @param dict 数据哈希
 */
-(void)setDataValueForm:(NSDictionary *)dict;

/**
 模型转Dic
 
 @return Dic
 */
- (NSDictionary *)dictFromData;
@end
