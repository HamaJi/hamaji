//
//  NetWorkMannager+Account.h
//  Bet365
//
//  Created by luke on 2018/7/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NetWorkMannager.h"
#import "RequestUrlHeader.h"
#import "LukeSpreadItem.h"
#import "LukeGameItem.h"
#import "RechargePayModel.h"



typedef NS_ENUM(NSInteger, InfoCheckoutErrorCountType) {
    /** 登录*/
    InfoCheckoutErrorCountType_Login,
    /** 注册*/
    InfoCheckoutErrorCountType_Register,
};

typedef NS_ENUM(NSInteger, DrawBankCardType) {
    /** 添加银行卡*/
    DrawBankCardType_Add,
    /** 编辑银行卡*/
    DrawBankCardType_Modify,
    /** 解除绑定银行卡*/
    DrawBankCardType_Release
};

typedef NS_ENUM(NSInteger, PersonPassWordType) {
    /** 登录密码*/
    PersonPassWordTypeLogin,
    /** 取款密码*/
    PersonPassWordTypeDraw
};

typedef NS_ENUM(NSInteger, PersonTeamRwsType) {
    /** 个人总览*/
    PersonTeamRwsType_Person,
    /** 团队总览*/
    PersonTeamRwsType_Team
};

typedef NS_ENUM(NSUInteger, PersonalRechargeOrderStatus) {
    /** 全部*/
    PersonalRechargeOrderStatusAll,
    /** 已完成*/
    PersonalRechargeOrderStatusFinish,
    /** 失败*/
    PersonalRechargeOrderStatusLose
};

typedef NS_ENUM(NSUInteger, UserTraceStatus) {
    /** 所有状态*/
    UserTraceStatusAll,
    /** 中奖*/
    UserTraceStatusWin,
    /** 未中奖*/
    UserTraceStatusLose
};

typedef NS_ENUM(NSUInteger, SpreadInfoCreateType) {
    /** 网页链接*/
    SpreadInfoCreateTypeUrl,
    /** 微信链接*/
    SpreadInfoCreateTypeWechat
};

typedef NS_ENUM(NSUInteger, SpreadInfoUserType) {
    /** 会员*/
    SpreadInfoUserTypeHuiYuan,
    /** 代理*/
    SpreadInfoUserTypeDaiLi
};

@interface NetWorkMannager (Account)
/**
 登录
 
 */
-(NSURLSessionTask *)requestLoginUrlParameters:(NSDictionary *)parameters
                                       success:(HttpRequestSuccess)success
                                       failure:(HttpRequestFailed)failure;

- (NSURLSessionTask *)requestLoginFV:(NSString *)fv success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

- (NSURLSessionTask *)requestLoginPhoneUrlParameters:(NSDictionary *)parameters
                                             success:(HttpRequestSuccess)success
                                             failure:(HttpRequestFailed)failure;

/**
 注册
 
 */
-(NSURLSessionTask *)requestRegisterUrlParameters:(NSDictionary *)parameters
                                          success:(HttpRequestSuccess)success
                                          failure:(HttpRequestFailed)failure;


/**
 获取试玩账户
 
 */
-(NSURLSessionTask *)requestTextRegisterUrlSuccess:(HttpRequestSuccess)success
                                           failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestTestRegisterUserByPassword:(NSString *)password ValiCode:(NSString *)valiCode success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;
/**
 获取注册配置（Text）
 
 */
- (NSURLSessionTask *)requestRegisterDataLimitTextSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;
/**
 退出登录
 
 */
- (NSURLSessionTask *)requestLogoutSuccess:(HttpRequestSuccess)success
                                   failure:(HttpRequestFailed)failure;




/**
 验证码
 
 @param isRandom 是否随机数
 @param success <#success description#>
 @param failure <#failure description#>
 @return <#return value description#>
 */
- (NSURLSessionTask *)requestGetVCodeByRandom:(BOOL)isRandom
                                      Success:(HttpRequestSuccess)success
                                      failure:(HttpRequestFailed)failure;

/**
 登录后验证码
 
 */
- (NSURLSessionTask *)requestGetVuserCodeSuccess:(HttpRequestSuccess)success
                                         failure:(HttpRequestFailed)failure;


/**
 找回密码
 
 @param userAccount 用户找好
 @param cashPassword 取款密码
 @param yzmNum 验证码
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestFundPwdByUserAccount:(nonnull NSString *)userAccount CashPassword:(nonnull NSString*)cashPassword vCode:(nonnull NSString *)yzmNum Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

/**
 注册校验
 
 @param type 类型
 @param val 传入的校验字符
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestCheckUniqueType:(NSInteger)type
                                         val:(nonnull NSString *)val
                                     Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;
/**
 修改密码
 
 @param oldPassword 旧密码
 @param newPassword 新密码
 */
- (NSURLSessionTask *)requestPostUpdatePwdByOldPassword:(nonnull NSString *)oldPassword
                                            NewPassword:(nonnull NSString*)newPassword
                                                   type:(PersonPassWordType)type
                                                Success:(HttpRequestSuccess)success
                                                failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestGetUserInfoVerifyType:(NSString *)phone Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;
/**
 完善个人信息
 
 @param fullName 姓名
 @param phone 电话
 @param qq QQ
 @param weixin 微信
 */
-(NSURLSessionTask *)requestPostModifyUserInfoWithfullName:(NSString *)fullName
                                                    phone:(NSString *)phone
                                                       qq:(NSString *)qq
                                                   weixin:(NSString *)weixin
                                                 birthday:(NSString *)birthday
                                                    email:(NSString *)email
                                                   smsCode:(NSString *)smsCode
                                                  Success:(HttpRequestSuccess)success
                                                  failure:(HttpRequestFailed)failure;
/**
 个人数据请求
 
 */
-(NSURLSessionTask *)requestInfoUrlSuccess:(HttpRequestSuccess)success
                                   failure:(HttpRequestFailed)failure;

/**
 金额&互踢
 
 */
- (NSURLSessionTask *)requestTimeLoopStatusSuccess:(HttpRequestSuccess)success
                                           failure:(HttpRequestFailed)failure;
- (NSURLSessionTask *)requestGetLimitKeyName:(NSString *)key Cache:(HttpRequestCache)cache Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

/**
 是否需要验证码
 
 */
- (NSURLSessionTask *)requestLoginLimitSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;


/*
 ip地址变更验证
 */
- (NSURLSessionTask *)postUserVerifyIpLocationFields:(NSDictionary *)parameters
                                             Success:(HttpRequestSuccess)success
                                             failure:(HttpRequestFailed)failure;
/**
 重设密码
 */
- (NSURLSessionTask *)postInitPwdParamter:(NSDictionary *)parameters Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;
/**
 注册是否需要验证码
 
 */
- (NSURLSessionTask *)requestRegisterLimitSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;
/**
 配置信息
 
 */
- (NSURLSessionTask *)requestSystemConfigSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;


/**
 判断王者荣耀接口

 @param success 成功回调
 @param failure 失败会调
 @return 任务
 */
- (NSURLSessionTask *)requestGetOpenStatusSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

/**
 投注记录
 
 @param account 账号名
 @param page 页码
 @param rows 页数
 @param startDate 开始时间
 @param endDate 结束时间
 @param gameId 游戏id
 @param status 不懂
 @param model 还是不懂
 @param success 成功回调
 @param failure 失败会调
 @return 任务
 */
- (NSURLSessionTask *)requestGetDeliverByAccount:(NSString *)account
                                            Page:(NSUInteger)page
                                            Rows:(NSUInteger)rows
                                       StartDate:(NSString *)startDate
                                         EndDate:(NSString *)endDate
                                          gameId:(NSString*)gameId
                                          status:(NSString *)status
                                           model:(NSString *)model
                                          result:(NSString *)result
                                         Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

/**
 彩票撤单
 
 @param parameters post请求参数
 @param success 成功回调
 @param failure 失败会调
 @return 任务
 */
- (NSURLSessionTask *)requestPostCpParameters:(NSDictionary *)parameters
                                      Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

/**
 彩票历史记录
 
 @param success 成功回调
 @param failure 失败会调
 @return 任务
 */
- (NSURLSessionTask *)requestGetCpHistorySuccess:(HttpRequestSuccess)success
                                         failure:(HttpRequestFailed)failure;

/**
 彩票历史投注记录详情
 
 @param account 账号名
 @param page 页码
 @param rows 页数
 @param gameId 游戏id
 @param model 还是不懂
 @param success 成功回调
 @param failure 失败会调
 @return 任务
 */
- (NSURLSessionTask *)requestGetCpHistoryListAccount:(NSString *)account
                                                Page:(NSUInteger)page
                                                Rows:(NSUInteger)rows
                                                date:(NSString *)date
                                              gameId:(NSString *)gameId
                                               model:(NSString *)model
                                             Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

/**
 真人电子体育投注记录
 
 @param startDate 开始时间
 @param endDate 结束时间
 @param gameCodes gameCodes
 @param success 成功回调
 @param failure 失败会调
 @return 任务
 */
- (NSURLSessionTask *)requestGetGameStartDate:(NSString *)startDate
                                      EndDate:(NSString *)endDate
                                    gameCodes:(NSString*)gameCodes
                                      Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;
/**
 追号记录
 
 @param account 账户名
 @param page 页码
 @param rows 页数
 @param startDate 开始时间
 @param endDate 结束时间
 @param gameId 游戏id
 @param result 不懂耶
 @param success 成功会调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetPursueByAccount:(NSString *)account
                                           Page:(NSUInteger)page
                                           Rows:(NSUInteger)rows
                                      StartDate:(NSString *)startDate
                                        EndDate:(NSString *)endDate
                                         gameId:(NSString *)gameId
                                         result:(UserTraceStatus)result
                                        Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;


/**
 追号详情
 
 @param traceOrderNo traceOrderNo
 @param success 成功会调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetTraceWithTraceOrderNo:(NSString *)traceOrderNo
                                              Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;
/**
 消息列表
 
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetMessageListSuccess:(HttpRequestSuccess)success
                                           failure:(HttpRequestFailed)failure;

/**
 读消息
 
 @param Id id
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetReadMessageWithId:(NSInteger)Id
                                          Success:(HttpRequestSuccess)success
                                          failure:(HttpRequestFailed)failure;

/**
 删除消息
 
 @param Id id
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetRemoveMessageWithId:(NSInteger)Id
                                            Success:(HttpRequestSuccess)success
                                            failure:(HttpRequestFailed)failure;

/*
 极速六合广告配置link
 
 @param success 成功回调
 @param failure 失败回调
 */
- (NSURLSessionTask *)requestAdverLinksConfigSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;


/**
 游戏公告列表
 
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetNoticeListSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestRETAILSTWITCHURLresponseCache:(HttpRequestCache)responseCache
                                                  success:(HttpRequestSuccess)success
                                                  failure:(HttpRequestFailed)failure;

/**
 提款订单
 
 @param page 页数
 @param rows 页码
 @param startDate 开始时间
 @param endDate 结束时间
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetUserDrawPage:(NSUInteger)page
                                        Rows:(NSUInteger)rows
                                   StartDate:(NSString *)startDate
                                     EndDate:(NSString *)endDate
                                     Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

/**
 个人总览，团队总览
 
 @param userId userId
 @param beginDate 开始时间
 @param endDate 结束时间
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetRwsWithUserId:(NSNumber *)userId
                            PersonTeamRwsType:(PersonTeamRwsType)rwsType
                                    beginDate:(NSString *)beginDate
                                      endDate:(NSString *)endDate
                                      Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;



/**
 用户列表
 
 @param startDate 开始时间
 @param endDate 结束时间
 @param moneyFrom 最小金额
 @param moneyTo 最大金额
 @param page 页数
 @param rows 20
 @param subAccount 用户subAccount
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetSubUsersWithStartDate:(NSString *)startDate
                                              endDate:(NSString *)endDate
                                            moneyFrom:(NSString *)moneyFrom
                                              moneyTo:(NSString *)moneyTo
                                                 page:(NSInteger)page
                                                 rows:(NSInteger)rows
                                              account:(NSString *)account
                                           subAccount:(NSString *)subAccount
                                              Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;


/**
 修改用户返点
 
 @param userId 用户ID
 @param rebate 返点
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetUpdateRebateWithUserId:(NSNumber *)userId
                                                rebate:(NSString *)rebate
                                               Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;


/**
 获取用户返点区间
 
 @param userId 用户ID
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetRebateRangeWithUserId:(NSNumber *)userId
                                              Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

/**
 在添加、编辑推广链接，以及添加会员时
 
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestgetDlSubRebateRangeSuccess:(HttpRequestSuccess)success
                                                failure:(HttpRequestFailed)failure;

/**
 注册管理
 
 @param success 成功回调
 @param failure 失败回调
 @return 任务 */
- (NSURLSessionTask *)requestPostDlAddWithUserInfo:(NSDictionary *)userInfo
                                       Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

/**
 存款信息
 
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetUserWithdrawSuccess:    (HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

/**
 获取银行信息
 
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetUserBankSuccess:        (HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

/**
 添加银行卡
 
 @param bankName 银行名称
 @param cardNo 卡号
 @param subAddress 银行地址
 @param fullName 真实姓名
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestPostModifyUserInfoWithBankName:(NSString *)bankName
                                                     cardNo:(NSString *)cardNo
                                                 subAddress:(NSString *)subAddress
                                                   fullName:(NSString *)fullName
                                               cashPassword:(NSString *)cashPassword
                                                       type:(DrawBankCardType)type
                                                    Success:(HttpRequestSuccess)success
                                                    failure:(HttpRequestFailed)failure;

/**
 修改推广链接
 
 @param model 模型
 @param url 推广网址
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestPostUpdateByAgentIdUrlWithModel:(LukeSpreadItem *)model
                                                         url:(NSString *)url
                                                     Success:(HttpRequestSuccess)success
                                                     failure:(HttpRequestFailed)failure;


/**
 获取推广详情
 
 @param page 页数
 @param rows 20
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetSpreadInfoWithPage:(NSInteger)page
                                              rows:(NSInteger)rows
                                           Success:(HttpRequestSuccess)success
                                           failure:(HttpRequestFailed)failure;


/**
 删除推广链接
 
 @param Id id
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestPostRemoveSpreadInfoWithId:(NSInteger)Id
                                                Success:(HttpRequestSuccess)success
                                                failure:(HttpRequestFailed)failure;

/**
 生成推广链接
 
 @param type 网页，微信
 @param userType 会员，代理
 @param externalUrl URL
 @param spreadType 推广链接类型
 @param rebate 反水
 @param code code
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestPostCreateSpreadInfoWithType:(SpreadInfoCreateType)type
                                                 userType:(SpreadInfoUserType)userType
                                              externalUrl:(NSString *)externalUrl
                                               spreadType:(NSInteger)spreadType
                                                   rebate:(NSString *)rebate
                                                     code:(NSString *)code
                                                  Success:(HttpRequestSuccess)success
                                                  failure:(HttpRequestFailed)failure;

/**
 推广页面
 
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetSpreadTypesSuccess:(HttpRequestSuccess)success
                                           failure:(HttpRequestFailed)failure;

/**
 生成二维码
 
 @param content url
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetQrcodeWithContent:(NSString *)content
                                          Success:(HttpRequestSuccess)success
                                          failure:(HttpRequestFailed)failure;

/**
 获取所有游戏数据
 
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetAllGameSuccess:(HttpRequestSuccess)success
                                       failure:(HttpRequestFailed)failure;

/**
 真人电子体育投注记录
 
 @param userName 用户名
 @param page 页数
 @param rows 10
 @param betStartDate 开始时间
 @param betEndDate 结束时间
 @param gameCode code
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetLiveBrUserName:(NSString *)userName
                                          Page:(NSUInteger)page
                                          Rows:(NSUInteger)rows
                                  betStartDate:(NSString *)betStartDate
                                    betEndDate:(NSString *)betEndDate
                                      gameCode:(NSString *)gameCode
                                       Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;


/**
 真人电子投注记录类型
 
 @param page 1
 @param rows 5000
 @param liveCode 类型
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetLiveQstPage:(NSUInteger)page
                                       Rows:(NSUInteger)rows
                                   liveCode:(NSString *)liveCode
                                    Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

/**
 充值订单
 
 @param page 页数
 @param rows 10
 @param dateFrom 开始时间
 @param dateTo 结束时间
 @param mode 类型
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetRechargeWithPage:(NSUInteger)page
                                            Rows:(NSUInteger)rows
                                        dateFrom:(NSString *)dateFrom
                                          dateTo:(NSString *)dateTo
                                            mode:(NSString *)mode
                                          status:(PersonalRechargeOrderStatus)status
                                         Success:(HttpRequestSuccess)success
                                         failure:(HttpRequestFailed)failure;

/**
 获取充值优惠类型
 
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetRechargeDiscountTypeListSuccess:(HttpRequestSuccess)success
                                                        failure:(HttpRequestFailed)failure;


/**
 账户明细
 
 @param account 用户帐号
 @param beginDate 开始时间
 @param endDate 结束时间
 @param tranType 类型
 @param page 页数
 @param rows 20
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetPageBillWithAccount:(NSString *)account
                                          beginDate:(NSString *)beginDate
                                            endDate:(NSString *)endDate
                                           tranType:(NSString *)tranType
                                               page:(NSInteger)page
                                               rows:(NSInteger)rows
                                            Success:(HttpRequestSuccess)success
                                            failure:(HttpRequestFailed)failure;

/**
 提款页面信息
 
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetQueryOutMoneyIndexSuccess:(HttpRequestSuccess)success
                                                  failure:(HttpRequestFailed)failure;

/**
 提现
 
 @param cashMoney 提现金额
 @param cashPassword 提款密码
 @param yzmNum 验证码
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestPostMoneyIndexWithcashMoney:(NSString *)cashMoney
                                            cashPassword:(NSString *)cashPassword
                                                  yzmNum:(NSString *)yzmNum
                                                UserMemo:(NSString *)userMemo
                                                 Success:(HttpRequestSuccess)success
                                                 failure:(HttpRequestFailed)failure;
/**
 走势请求
 
 @param model 模型
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetTrendWithModel:(KFCPHomeGameJsonModel *)model
                                  RequestCache:(HttpRequestCache)cache
                                       Success:(HttpRequestSuccess)success
                                       failure:(HttpRequestFailed)failure;

/**
 额度转换
 
 @param from 从
 @param to 到
 @param amount 金额
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetTransferWithFrom:(NSString *)from
                                              to:(NSString *)to
                                          amount:(NSString *)amount
                                         Success:(HttpRequestSuccess)success
                                         failure:(HttpRequestFailed)failure;

/**
 额度转换查询余额
 
 @param liveCode liveCode
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetGetBalanceWithLiveCode:(NSString *)liveCode
                                               Success:(HttpRequestSuccess)success
                                               failure:(HttpRequestFailed)failure;

/**
 获取充值类型
 
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetPayTypesSuccess:(HttpRequestSuccess)success
                                        failure:(HttpRequestFailed)failure;

/**
 具体充值类型
 
 @param payType payType
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetPayAccountsWithPayType:(NSString *)payType
                                               Success:(HttpRequestSuccess)success
                                               failure:(HttpRequestFailed)failure;

/**
 具体充值类型
 
 @param payType payType
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetTpPayChannelsWithPayType:(NSString *)payType
                                                 Success:(HttpRequestSuccess)success
                                                 failure:(HttpRequestFailed)failure;

/**
 判断充值请求
 
 @param mode
 @param payType
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetRechargeConfigWithMode:(NSInteger)mode
                                               payType:(NSString *)payType
                                               Success:(HttpRequestSuccess)success
                                               failure:(HttpRequestFailed)failure;

/**
 充值

 @param amount 金额
 @param payAccountId
 @param yzmNum 验证码
 @param remark 备注信息
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestPostRechargeWithAmount:(NSString *)amount
                                       payAccountId:(NSInteger)payAccountId
                                             yzmNum:(NSString *)yzmNum
                                             remark:(NSString *)remark
                                       rechargeTime:(NSString *)rechargeTime
                                     rechargePerson:(NSString *)rechargePerson
                                      friendAccount:(NSString *)friendAccount
                                            Success:(HttpRequestSuccess)success
                                            failure:(HttpRequestFailed)failure;

/**
 在线充值

 @param amount 金额
 @param payChannelId
 @param yzmNum 验证码
 @param bankCode 银行code
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetRechargeOnlinePayWithAmount:(NSString *)amount
                                               payChannelId:(NSInteger)payChannelId
                                                     yzmNum:(NSString *)yzmNum
                                                   bankCode:(NSString *)bankCode
                                                    Success:(HttpRequestSuccess)success
                                                    failure:(HttpRequestFailed)failure;

/**
 登录/注册验证码
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestGetCheckoutErrorCountWithType:(InfoCheckoutErrorCountType)type
                                                   Success:(HttpRequestSuccess)success
                                                   failure:(HttpRequestFailed)failure;

/**
 首页配置信息

 @param responseCache 缓存
 @param success 请求
 @param failure 失败
 @return 任务
 */
-(NSURLSessionTask *)requestMainFlexTemplatesResponseCache:(HttpRequestCache)responseCache
                                                   success:(HttpRequestSuccess)success
                                                   failure:(HttpRequestFailed)failure;

- (NSURLSessionTask *)requestMainFlexMenusCache:(HttpRequestCache)cache Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestTheAnnouncementUrlresponseCache:(HttpRequestCache)responseCache
                                                    success:(HttpRequestSuccess)success
                                                    failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestGetSliderResponseCache:(HttpRequestCache)responseCache
                                           success:(HttpRequestSuccess)success
                                           failure:(HttpRequestFailed)failure;

/**
 判断验证码为图片验证或腾讯验证
 @param success 成功回调
 @param failure 失败回调
 @return 任务
 */
- (NSURLSessionTask *)requestVerifyAuthorTypeAccount:(NSString *)account
                                               Phone:(NSString *)loginPhone
                                            Success:(HttpRequestSuccess)success
                                            failure:(HttpRequestFailed)failure;

- (NSURLSessionTask *)requestChangeSMSVerifyTypePhone:(NSString *)phone
                                        Success:(HttpRequestSuccess)success
                                        failure:(HttpRequestFailed)failure;

- (NSURLSessionTask *)requestLoginSMScodeByPhone:(NSString *)phone
                                   Paramers:(NSDictionary *)paramser
                                    Success:(HttpRequestSuccess)success
                                    failure:(HttpRequestFailed)failure;

- (NSURLSessionTask *)requestUpdateSMScodeByPhone:(NSString *)phone
                                         Paramers:(NSDictionary *)paramser
                                          Success:(HttpRequestSuccess)success
                                          failure:(HttpRequestFailed)failure;
- (NSURLSessionTask *)requestCancleBindingPhone:(NSString *)phone
                                        smsCode:(NSString *)code
                                        Success:(HttpRequestSuccess)success
                                        failure:(HttpRequestFailed)failure;

- (NSURLSessionTask *)requestModifyBindingPhone:(NSString *)phone
                                        smsCode:(NSString *)code
                                        Success:(HttpRequestSuccess)success
                                        failure:(HttpRequestFailed)failure;
/**
 <#Description#>
 @param success <#success description#>
 @param failure <#failure description#>
 @return <#return value description#>
 */
- (NSURLSessionTask *)requestRigisterVerifyTypeSuccess:(HttpRequestSuccess)success
                                               failure:(HttpRequestFailed)failure;

/**
 强制修改密码

 @param newPsw 新密码(需要判断是否 md5加密)
 */
- (NSURLSessionTask *)requestForceChangeNewPsw:(NSString *)newPsw Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

- (NSURLSessionTask *)requestRechargCardCache:(HttpRequestCache)cache Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

- (NSURLSessionTask *)requestPayTypeListCache:(HttpRequestCache)cache Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

- (NSURLSessionTask *)requestRechargeLimitCache:(HttpRequestCache)cache Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

- (NSURLSessionTask *)requestPayAccountListWithPayType:(NSString *)payType Cache:(HttpRequestCache)cache Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

- (NSURLSessionTask *)requestPayChannelListWithPayType:(NSString *)payType Cache:(HttpRequestCache)cache Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

- (NSURLSessionTask *)requestGetRechargeConfigtWithPayType:(NSString *)payType Model:(RechargePayModelValue *)model ChannleID:(NSInteger)channelId Cache:(HttpRequestCache)cache Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;
/**
 完善资料

 */
- (NSURLSessionTask *)requestEditUserInfoLimitCache:(HttpRequestCache)cache Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;


/**
 账变记录类型

 */
- (NSURLSessionTask *)requestGetTransListSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;



-(NSURLSessionTask *)requestGetUserCenterMenuresponseCache:(HttpRequestCache)responseCache
                                                   success:(HttpRequestSuccess)success
                                                   failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestGetGrowthConfigesponseCache:(HttpRequestCache)responseCache
                                                success:(HttpRequestSuccess)success
                                                failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestSubmitSiginInsuccess:(HttpRequestSuccess)success
                                         failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestYEBaoInfosuccess:(HttpRequestSuccess)success
                                     failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestYEBaoTransferIn:(double)money Success:(HttpRequestSuccess)success
                                    failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestYEBaoTransferOut:(double)money Password:(NSString *)psw Success:(HttpRequestSuccess)success
                                     failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestYEBaoHistoryWithPage:(NSUInteger)page
                                            Rows:(NSUInteger)rows
                                         Success:(HttpRequestSuccess)success
                                         failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)getQueryAllTestFirstKindCache:(HttpRequestCache)responseCache
                                           Success:(HttpRequestSuccess)success
                                           failure:(HttpRequestFailed)failure;
-(NSURLSessionTask *)requestLaunchAdCache:(HttpRequestCache)cache
                                  Success:(HttpRequestSuccess)success
                                  failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestGetImmediateMessageSuccess:(HttpRequestSuccess)success
                                               failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestPostUpdateMessageStatusSuccess:(HttpRequestSuccess)success
                                                   failure:(HttpRequestFailed)failure;

- (NSURLSessionTask *)requestPostRegisterSMSCodeByPhone:(NSString *)phone
                                         Paramers:(NSDictionary *)paramser
                                          Success:(HttpRequestSuccess)success
                                          failure:(HttpRequestFailed)failure;

@end
