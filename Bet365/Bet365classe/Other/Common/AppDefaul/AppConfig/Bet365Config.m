//
//  Bet365Config.m
//  Bet365
//
//  Created by HHH on 2018/7/15.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Bet365Config.h"
#import "NetWorkMannager+Account.h"
#import <TFHpple/TFHpple.h>
#import <MGJRouter/MGJRouter.h>
#import <XHLaunchAd/XHLaunchAd.h>


@interface Bet365Config(){
    
}
@property (nonatomic,strong)NSDictionary *infoData;
@end



@implementation Bet365Config


+(instancetype)shareInstance{
    static Bet365Config *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[Bet365Config alloc] init];
    });
    return instance;
}

-(instancetype)init{
    if (self = [super init]) {
        [self updateSubject];
        [self infoData];
        [self userSettingData];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (SerVer_Url.length) {
                [self getConfigCompleted:^(BOOL success) {
                    
                }];
            }
        });
        
        
    }
    return self;
}

#pragma mark - Private



-(void)getConfigCompleted:(void(^)(BOOL success))completed{
    if (!SerVer_Url.length){
        completed ? completed(NO) : nil;
        return;
    }
    
    __block NSInteger isOk= -1;
    
    dispatch_group_t group = dispatch_group_create();
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [self requestRegisterDataLimitText:^(BOOL success) {
            isOk != 0 ? (isOk = success) : 0;
            dispatch_semaphore_signal(sema);
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
//    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
//        [self getNoticeCompleted:^(BOOL success) {
//            isOk != 0 ? (isOk = success) : 0;
//            dispatch_semaphore_signal(sema);
//        }];
//        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
//    });
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [self requestGetSystemConfigCompleted:^(NSDictionary* data) {
            isOk != 0 ? (isOk = (data != nil)) : 0;
            if (data) {
                [self.config cleanUserData];
                [self.config setDataValueForm:data];
            }
            dispatch_semaphore_signal(sema);
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
//        if (BET_CONFIG.config.adLinks_Storage) {
//            self.adver_config = [advertModel mj_objectArrayWithKeyValuesArray:BET_CONFIG.config.adLinks_Storage];
//        }
        [self requestAdverLinksConfigCompleted:^(NSArray *data) {
            if (data) {
//                [configdata setObject:data forKey:@"adLinks_Storage"];
                self.adver_config = [advertModel mj_objectArrayWithKeyValuesArray:data];
            }
            dispatch_semaphore_signal(sema);
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });

    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [self getRedData:^(BOOL success) {
            isOk != 0 ? (isOk = success) : 0;
            dispatch_semaphore_signal(sema);
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [self getUserCenterMenueTempate:^(BOOL success, NetReceiveObjType type) {
            if (type != NetReceiveObjType_CACHE) {
                dispatch_semaphore_signal(sema);
            }
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [self getGrowthConfig:^(BOOL success, NetReceiveObjType type) {
            if (type != NetReceiveObjType_CACHE) {
                dispatch_semaphore_signal(sema);
            }
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [self getMainAllocationCompleted:^(BOOL success, NetReceiveObjType type) {
            if (type != NetReceiveObjType_CACHE) {
                dispatch_semaphore_signal(sema);
            }
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });

    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);

        [self getMainFlexMenuCompleted:^(BOOL success) {
            dispatch_semaphore_signal(sema);
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [self requestGetLimit:^(BOOL success, NetReceiveObjType type) {
            if (type != NetReceiveObjType_CACHE) {
                dispatch_semaphore_signal(sema);
            }
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        
        [self getSliderCompleted:^(BOOL success, NetReceiveObjType type) {
            if (type != NetReceiveObjType_CACHE) {
                dispatch_semaphore_signal(sema);
            }
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        
    });
    
    

    dispatch_async(dispatch_get_main_queue(), ^{
        completed ? [self.updateSubject sendNext:@(NetReceiveObjType_CACHE)] : nil;
    });
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [self.registerConfig save];
        [self.config save];
        [self.common_config save];
        self.configHasUpdate = (isOk == 1);
        completed ? [self.updateSubject sendNext:@(NetReceiveObjType_RESOPONSE)] : nil;
//        [self.updateSubject sendCompleted];
        completed ? completed(self.configHasUpdate) : nil;
    });

}
#pragma mark - GET/SET
-(NSDictionary *)infoData{
    if (!_infoData) {

        _infoData = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath(@"Info")];
        
        _buildVersion = [_infoData[@"CFBundleVersion"] integerValue];
        _jPushKey = [_infoData[@"AppdetegateJPushKey"] copy];
        _appId = [_infoData[@"AppId"] copy];
        _customURL = [_infoData[@"CustomURL"] copy];
        _customServiceRead = [_infoData[@"ServiceCustomReadConfig"] copy];
        _boardCode = [_infoData[@"ErrorBoradCode"] copy];
        _PromotionCodeState = [_infoData[@"PromotionCodeState"] copy];
        _PromotionCode = [_infoData[@"PromotionCode"] copy];
        _appName = [_infoData objectForKey:@"CFBundleDisplayName"];
    }
    return _infoData;
}
-(AppSetting *)userSettingData{
    if (!_userSettingData) {
        _userSettingData = [[AppSetting alloc] init];
    }
    return _userSettingData;
}
-(CongfigClusterData *)config{
    if (!_config) {
        _config = [[CongfigClusterData alloc] init];
    }
    return _config;
}
-(RegisterConfig *)registerConfig{
    if (!_registerConfig) {
        _registerConfig = [[RegisterConfig alloc] init];
    }
    return _registerConfig;
}
-(CommonConfig *)common_config{
    if (!_common_config) {
        _common_config = [[CommonConfig alloc] init];
    }
    return _common_config;
}

-(SkinConfig *)skin{
    if (!_skin) {
        _skin = [[SkinConfig alloc] init];
    }
    return _skin;
}

#pragma mark - NetRequest
/**
 获取系统配置信息
 */
-(void)requestGetSystemConfigCompleted:(void(^)(NSDictionary *data))completed{
    [NET_DATA_MANAGER requestSystemConfigSuccess:^(id responseObject) {
        NSDictionary *dic = (NSDictionary *)responseObject;
        completed ? completed(dic) : nil;
    } failure:^(NSError *error) {
        completed ? completed(nil) : nil;
    }];
}
/**
获取当前请求广告配置
 */
-(void)requestAdverLinksConfigCompleted:(void(^)(NSArray *data))completed{
    [NET_DATA_MANAGER requestAdverLinksConfigSuccess:^(id responseObject) {
        completed?completed(responseObject):nil;
    } failure:^(NSError *error) {
        completed?completed(nil):nil;
    }];
}
/**
 获取登录配置信息
 tips：是否需要验证码
 */
-(void)requestGetLoginLimit:(void(^)(NSNumber *vCode))completed{
    
    [NET_DATA_MANAGER requestLoginLimitSuccess:^(id responseObject) {
        NSDictionary *dic = (NSDictionary *)responseObject;
        completed ? completed(dic[@"vCode"]) : nil;
    } failure:^(NSError *error) {
        completed ? completed(nil) : nil;
    }];
}

-(void)requestGetLimit:(void(^)(BOOL success,NetReceiveObjType type))completed{
    
    
    void(^handle)(id repsonse,NetReceiveObjType type) = ^(id repsonse,NetReceiveObjType type){
        NSError *error;
        NSDictionary *data = [repsonse[@"value"] mj_JSONObject];
        LimitModel *model = [LimitModel mj_objectWithKeyValues:data];
        if (model) {
            self.limit = model;
        }
        completed ? completed(model != nil , type) : nil;
    };
    
    [NET_DATA_MANAGER requestGetLimitKeyName:@"userLoginLimit" Cache:^(id responseCache) {
        handle(responseCache,NetReceiveObjType_CACHE);
    } Success:^(id responseObject) {
        handle(responseObject,NetReceiveObjType_RESOPONSE);
    } failure:^(NSError *error) {
        handle(nil,NetReceiveObjType_FAIL);
    }];


}

/**
 注册配置信息

 */
-(void)requestRegisterDataLimitText:(void(^)(BOOL success))completed{
    [NET_DATA_MANAGER requestRegisterDataLimitTextSuccess:^(id responseObject) {
        [self.registerConfig cleanUserData];
        [self.registerConfig setDataValueForm:responseObject];
        completed ? completed(YES) : nil;
    } failure:^(NSError *error) {
        completed ? completed(NO) : nil;
    }];
}

-(void)getRedData:(void(^)(BOOL success))completed{
    [NET_DATA_MANAGER requestRETAILSTWITCHURLresponseCache:nil success:^(id responseObject) {
        TFHpple *pple = [[TFHpple alloc] initWithHTMLData:[responseObject dataUsingEncoding:NSUTF8StringEncoding]];
        NSArray *elementList = [pple searchWithXPathQuery:@"//script"];
        NSMutableDictionary *data = [NSMutableDictionary dictionary];
        for (TFHppleElement *element in elementList) {
            NSMutableString *mStr = [[NSMutableString alloc] initWithString:element.content];
            [mStr deleteString:@"var app_config ="];
            [mStr deleteString:@"var common_config ="];
            [mStr deleteBetweenStarStr:@"/*" EndStr:@"*/"];
            NSDictionary *mDic = [mStr jsonValueDecoded];
            if ([mDic isKindOfClass:[NSDictionary class]]) {
                [data addEntriesFromDictionary:mDic];
            }
        }
        [self.common_config cleanUserData];
        [self.common_config setDataValueForm:data];
        completed ? completed(YES) : nil;
    } failure:^(NSError *error) {
        completed ? completed(NO) : nil;
    }];
}

//-(void)getWZRYOpenStatusData:(void(^)(NSNumber *success))completed{
//    [NET_DATA_MANAGER requestGetOpenStatusSuccess:^(id responseObject) {
//        completed ? completed([NSNumber numberWithInteger:[responseObject[@"status"] integerValue]]) : nil;
//
//    } failure:^(NSError *error) {
//        completed ? completed(NO) : nil;
//    }];
//}

-(NSURLSessionTask *)getMainAllocationCompleted:(void(^)(BOOL success,NetReceiveObjType type))completed{
    
    NSString *flexPath = [[NSBundle mainBundle] pathForResource:@"FlexData.json" ofType:nil];
    NSData *flexData = [[NSData alloc] initWithContentsOfFile:flexPath];
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:flexData options:NSJSONReadingMutableContainers error:nil];
    
    void(^handle)(id obj,NetReceiveObjType type) = ^(id obj,NetReceiveObjType type){
        if (!obj || ![obj isKindOfClass:[NSDictionary class]]) {
            completed ? completed(NO,type) : nil;
            return;
        }
        __block NSError *error;
        NSMutableDictionary *fleDic = [obj mutableCopy];
        
        /** Tabbar走本地FlexData.json配置 **/
        if ([BET_CONFIG.userSettingData.defaultAppID isEqualToString:@"DQTY"]) {
            
        }else{
            [fleDic removeObjectForKey:@"main_tab"];
            [fleDic safeSetObject:jsonDic[@"main_tab"] forKey:@"main_tab"];
        }
        
        
        MainAllocationModel *allocation = [MTLJSONAdapter modelOfClass:[MainAllocationModel class] fromJSONDictionary:fleDic error:&error];
        if (error || allocation == nil) {
            
        }else{
            NSMutableArray *tabbarList = allocation.templateList_tabbar.mutableCopy;
            [allocation.templateList_tabbar enumerateObjectsUsingBlock:^(TabBarTemplate * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (![UIViewController hasRegisterShareInstance:obj.type]) {
                    [tabbarList removeObject:obj];
                }
            }];
            if (tabbarList.count > 6) {
                [tabbarList removeObjectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(6, tabbarList.count - 6)]];
            }
            allocation.templateList_tabbar = tabbarList;
            
            NSString *skinPath = [[NSBundle mainBundle] pathForResource:@"SkinData.json" ofType:nil];
            NSData *skinData = [[NSData alloc] initWithContentsOfFile:skinPath];
            
            NSString *skinName = allocation.skinName;
            NSDictionary *skinDic = [[NSJSONSerialization JSONObjectWithData:skinData options:NSJSONReadingMutableContainers error:nil] safeObjectForKey:skinName];
            SkinConfig *skin = [MTLJSONAdapter modelOfClass:SkinConfig.class fromJSONDictionary:skinDic error:&error];
            if (skin) {
                skin.skinName = skinName;
                self.skin = skin;
            }
            BET_CONFIG.allocation = allocation;
        }
        
        completed ? completed(BET_CONFIG.allocation != nil,type) : nil;
        
    };
    
    /** 首页样式走本地FlexData.json配置,旧版首页 **/
//    handle ? handle(jsonDic,NetReceiveObjType_CACHE) : nil;
//    return nil;
    
    return [NET_DATA_MANAGER requestMainFlexTemplatesResponseCache:^(id responseCache) {
        handle ? handle([((NSArray *)responseCache[@"index_flex"]) safeObjectAtIndex:0], NetReceiveObjType_CACHE) : nil;
    } success:^(id responseObject) {
        handle ? handle([((NSArray *)responseObject[@"index_flex"]) safeObjectAtIndex:0],NetReceiveObjType_RESOPONSE) : nil;
    } failure:^(NSError *error) {
        handle ? handle(nil,NetReceiveObjType_FAIL) : nil;
    }];
}

-(void)getMainFlexMenuCompleted:(void(^)(BOOL success))completed{
    void(^handle)(id obj,NetReceiveObjType type) = ^(id obj,NetReceiveObjType type){
        NSError *error;
        self.menuList = [NSMutableArray array];
        
        MenuTemplate *menuLogin =  [[MenuTemplate alloc] init];
        menuLogin.appLink = kRouterLogin;
        menuLogin.name = @"登录/注册";
        menuLogin.type = TemplateJumpType_ROUTER_ONLYKEY;
        [self.menuList addObject:menuLogin];
        
        [self.menuList addObjectsFromArray:[MTLJSONAdapter modelsOfClass:[MenuTemplate class] fromJSONArray:obj error:&error]];
        
        MenuTemplate *menuMetric =  [[MenuTemplate alloc] init];
        menuMetric.appLink = kRouterMetrics;
        menuMetric.name = @"网络检测";
        menuMetric.type = TemplateJumpType_ROUTER_ONLYKEY;
        [self.menuList addObject:menuMetric];
        
        MenuTemplate *menuExit =  [[MenuTemplate alloc] init];
        menuExit.appLink = kRouterExit;
        menuExit.name = @"安全退出";
        menuExit.type = TemplateJumpType_ROUTER_ONLYKEY;
        [self.menuList addObject:menuExit];
        
        MenuTemplate *menuSound =  [[MenuTemplate alloc] init];
        menuSound.appLink = kRouterSoundSwitch;
        menuSound.type = TemplateJumpType_ROUTER_ONLYKEY;
        menuSound.name = @"音效";
        [self.menuList addObject:menuSound];
        
        MenuTemplate *cleanCache = [[MenuTemplate alloc] init];
        cleanCache.appLink = kRouterCleanCache;
        cleanCache.type = TemplateJumpType_ROUTER_ONLYKEY;
        cleanCache.name = @"清理缓存";
        [self.menuList addObject:cleanCache];
#ifdef DEBUG
        MenuTemplate *schemes =  [[MenuTemplate alloc] init];
        schemes.appLink = kRouterSchemes;
        schemes.type = TemplateJumpType_ROUTER_ONLYKEY;
        schemes.name = @"Schemes";
        if (self.menuList.count) {
            [self.menuList insertObject:schemes atIndex:0];
        }else{
            [self.menuList addObject:schemes];
        }
        
#else
        //do sth.
#endif
        
//        MenuTemplate *menuRed =  [[MenuTemplate alloc] init];
//        menuRed.appLink = ROUTER_REDPAGE;
//        menuRed.type = @"1";
//        menuRed.name = @"红包";
//        [self.menuList addObject:menuRed];
        
        completed ? completed(error == nil) : nil;
    };
    [NET_DATA_MANAGER requestMainFlexMenusCache:^(id responseCache) {
        handle(responseCache,NetReceiveObjType_CACHE);
    } Success:^(id responseObject) {
        handle(responseObject,NetReceiveObjType_RESOPONSE);
    } failure:^(NSError *error) {
        completed ? completed(NO) : nil;
    }];
}


/** 公告栏数据 **/
- (NSURLSessionTask *)getNoticeCompleted:(void(^)(BOOL success))completed{
    /**AnnouncementDataCache**/
    void(^AnnouncementBlock)(NSDictionary *dataDic,NetReceiveObjType type) = ^(NSDictionary *dataDic,NetReceiveObjType type){
        if (!dataDic) {
            completed ? completed(NO) : nil;
            return ;
        }
        Bet365NoticeModel *model = [Bet365NoticeModel mj_objectWithKeyValues:dataDic];
        if (type != NetReceiveObjType_FAIL) {
            self.noticeModel = model;
        }
        completed(self.noticeModel != nil);
    };
    
    return [NET_DATA_MANAGER requestTheAnnouncementUrlresponseCache:^(id responseCache) {
//        AnnouncementBlock(responseCache,NetReceiveObjType_CACHE);
    } success:^(id responseObject) {
        AnnouncementBlock(responseObject,NetReceiveObjType_RESOPONSE);
    } failure:^(NSError *error) {
        AnnouncementBlock(nil,NetReceiveObjType_FAIL);
    }];
}

/** slider数据 **/
- (NSURLSessionTask *)getSliderCompleted:(void(^)(BOOL success,NetReceiveObjType type))completed{
    /**AnnouncementDataCache**/
    void(^block)(NSArray *list,NetReceiveObjType type) = ^(NSArray *list,NetReceiveObjType type){
        if (!list) {
            completed ? completed(NO,type) : nil;
            return ;
        }
        NSError *error;
        NSArray *arr = [MTLJSONAdapter modelsOfClass:SliderModel.class fromJSONArray:list error:&error];
        if (arr && !error) {
            self.sliderList = arr;
        }
        completed(self.sliderList != nil,type);
    };
    
    return [NET_DATA_MANAGER requestGetSliderResponseCache:^(id responseCache) {
        block(responseCache,NetReceiveObjType_CACHE);
    } success:^(id responseObject) {
        block(responseObject,NetReceiveObjType_RESOPONSE);
    } failure:^(NSError *error) {
        block(nil,NetReceiveObjType_FAIL);
    }];
}

-(void)getUserCenterMenueTempate:(void(^)(BOOL success,NetReceiveObjType type))complete{
    NSString *centerPath = [[NSBundle mainBundle] pathForResource:@"CenterMenuData.json" ofType:nil];
    NSData *centerData = [[NSData alloc] initWithContentsOfFile:centerPath];
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:centerData options:NSJSONReadingMutableContainers error:nil];
    
    void(^handle)(id obj,NetReceiveObjType type) = ^(id obj,NetReceiveObjType type){
        NSError *error;
        UserCenterTemplateModel *templateModel = [MTLJSONAdapter modelOfClass:[UserCenterTemplateModel class] fromJSONDictionary:obj error:&error];
        if (templateModel) {
            self.userCenterMenu = templateModel;
        }
        complete ? complete((!error && templateModel), type) : nil;
    };
    
    /** 个人中心样式走本地CenterMenuData.json配置,旧版个人中心 **/
//    handle ? handle(jsonDic,NetReceiveObjType_CACHE) : nil;
    
    [NET_DATA_MANAGER requestGetUserCenterMenuresponseCache:^(id responseCache) {
        handle(responseCache,NetReceiveObjType_CACHE);
    } success:^(id responseObject) {
        handle(responseObject,NetReceiveObjType_RESOPONSE);
    } failure:^(NSError *error) {
        handle(nil,NetReceiveObjType_FAIL);
    }];
}



-(void)getGrowthConfig:(void(^)(BOOL success,NetReceiveObjType type))complete{
    void(^handle)(id obj,NetReceiveObjType type) = ^(id obj,NetReceiveObjType type){
        NSError *error;
        GrowthConfigModel *configModel = [MTLJSONAdapter modelOfClass:[GrowthConfigModel class] fromJSONDictionary:obj[@"config"] error:&error];
        NSArray *levelList = [GrowthLevelModel mj_objectArrayWithKeyValuesArray:obj[@"levels"]];
        
        if (configModel) {
            self.growthConfigModel = configModel;
        }
        if (levelList.count) {
            self.growthLevelList = levelList;
        }
        complete ? complete(error == nil, type) : nil;
    };
    [NET_DATA_MANAGER requestGetGrowthConfigesponseCache:^(id responseCache) {
        handle(responseCache,NetReceiveObjType_CACHE);
    } success:^(id responseObject) {
        handle(responseObject,NetReceiveObjType_RESOPONSE);
    } failure:^(NSError *error) {
        handle(nil,NetReceiveObjType_FAIL);
    }];
}

-(void)getLaunchAdMent:(void(^)(LaunchAd *launchAd))completed{
    [NET_DATA_MANAGER requestLaunchAdCache:^(id responseCache) {
        NSError *error;
        LaunchAd *ad = [MTLJSONAdapter modelOfClass:LaunchAd.class fromJSONDictionary:responseCache error:&error];
        self.launchAd = ad;
        completed ? completed(ad) : nil;
    } Success:^(id responseObject) {
        NSError *error;
        LaunchAd *ad = [MTLJSONAdapter modelOfClass:LaunchAd.class fromJSONDictionary:responseObject error:&error];
        if (ad.image.length && !error) {
            [XHLaunchAd downLoadImageAndCacheWithURLArray:@[[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SerVer_Url,ad.image]]]];
        }
    } failure:^(NSError *error) {
        
    }];
}

-(RACSubject *)updateSubject{
    if (!_updateSubject) {
        _updateSubject = [RACSubject subject];
    }
    return _updateSubject;
}


@end
