//
//  SkinConfig.h
//  Bet365
//
//  Created by super_小鸡 on 2019/6/19.
//  Copyright © 2019 jesse. All rights reserved.
//
#import <Mantle/Mantle.h>

NS_ASSUME_NONNULL_BEGIN


@interface UIColor(Skin)

+(NSString *)skinDefaultName;

+(NSString *)skinPinkName;

+(NSString *)skinBlackName;

+(NSString *)skinGreenName;

+(NSString *)skinBrownName;

+(NSString *)skinBlueName;

+(NSString *)skinWihteName;

+(NSString *)skinGoldName;

+(NSString *)skinMintgreenName;

+(NSString *)skinCoffeeName;

+(UIColor *)skinNaviBgColor;

+(UIColor *)skinNaviTintColor;

+(UIColor *)skinTabBgColor;

+(UIColor *)skinTabNorColor;

+(UIColor *)skinTabSelColor;

+(UIColor *)skinViewScreenColor;

+(UIColor *)skinViewBgColor;

+(UIColor *)skinViewContentColor;

+(UIColor *)skinTextTitileColor;

+(UIColor *)skinTextHeadColor;

+(UIColor *)skinTextItemNorColor;

+(UIColor *)skinTextItemNorSubColor;

+(UIColor *)skinTextItemSelColor;

+(UIColor *)skinTextItemSelSubColor;

+(UIColor *)skinViewKitNorColor;

+(UIColor *)skinViewKitSelColor;

+(UIColor *)skinViewKitDisColor;

+(UIColor *)skinViewCusHeadBgColor;

+(UIColor *)skinViewCusBgColor;

+(UIColor *)skinViewQuickBgColor;




@end

@interface UIImage(Gradient)

+(UIImage *)skinGradientHorNaviImage;

+(UIImage *)skinGradientVerNaviImage;

+(UIImage *)skinGradientSelImage;

@end

@interface CAGradientLayer(Gradient)
+(CAGradientLayer *)gradientLayer:(CGRect)bounds FirColor:(UIColor *)firColor SecColor:(UIColor *)secColor;

+(CAGradientLayer *)bet35GradientLayer:(CGRect)bounds;
@end

@interface SkinConfig : MTLModel<MTLJSONSerializing>

@property (nonatomic,strong)NSString *skinName;

@property (nonatomic,strong)UIColor * navi_bg_color; //头部底色（基调色）
@property (nonatomic,strong)UIColor * navi_tint_color;

@property (nonatomic,strong)UIColor * tab_bg_color; //底部底色
@property (nonatomic,strong)UIColor * tab_nor_color; //底部字体颜色
@property (nonatomic,strong)UIColor * tab_sel_color; //底部图标渲染颜色

@property (nonatomic,strong)UIColor * view_screen_color; //视图主窗口颜色
@property (nonatomic,strong)UIColor * view_bg_color; //视图背景颜色
@property (nonatomic,strong)UIColor * view_contentview_color; //视图容器视图背景颜色

@property (nonatomic,strong)UIColor * text_titile_color; //标题字体颜色
@property (nonatomic,strong)UIColor * text_head_color; //组头字体颜色

@property (nonatomic,strong)UIColor * text_item_nor_color; //组单元主字体颜色
@property (nonatomic,strong)UIColor * text_item_nor_sub_color; //组单元付字体颜色

@property (nonatomic,strong)UIColor * text_item_sel_color; //组单元主字体选中颜色
@property (nonatomic,strong)UIColor * text_item_sel_sub_color;//组单元付字体选中颜色


@property (nonatomic,strong)UIColor * view_kit_nor_color; //视图控件颜色
@property (nonatomic,strong)UIColor * view_kit_sel_color; //视图控件选中颜色
@property (nonatomic,strong)UIColor * view_kit_dis_color; //视图控件禁选颜色

@property (nonatomic,strong)UIColor * view_cus_headBg_color;  //首页templateList_cus栏的section颜色
@property (nonatomic,strong)UIColor * view_cus_bg_color;      //首页templateList_cus栏的item颜色
@property (nonatomic,strong)UIColor * view_quick_bg_color;  //首页templateList_quick栏的item颜色

@property (nonatomic,strong)UIImage *gradientHorImage;

//@property (nonatomic,strong)UIImage *gradientVerImage;

+(NSString *)getSkinName:(NSString *)mainTheme;

+(NSString *)getMainTheme:(NSString *)skinName;


@end

NS_ASSUME_NONNULL_END
