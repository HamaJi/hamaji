//
//  MainFlexModel.m
//  Bet365
//
//  Created by adnin on 2018/11/27.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "MainAllocationModel.h"
#import <YYKit/YYKit.h>

@implementation MenuAttrTemplate

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"gameKind":@"gameKind",
             @"gameType":@"gameType",
             @"liveCode":@"liveCode"};
}

@end
@implementation MenuTemplate

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"name":@"menuName",
             @"appLink":@"menuLink",
             @"menuAttr":@"menuAttr",
             @"type":@"menuType"
             };
}


kJSONTransformerMethodImpTemplate(NSValueTransformer *, type, @selector(templateJumpTypeTransformer))

kJSONTransformerMethodImpTemplate(NSValueTransformer *, appLink, @selector(routerKeyTransformer))

kJSONTransformerMethodImpTemplate(NSValueTransformer *, menuAttr, @selector(templateAttrTransformer))

@end

@implementation TabBarTemplate

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"name":@"modelName",
             @"type": @"modelType",
             @"icon":@"modelImage",
             @"selIcon":@"modelActiveImage"
             };
}

//+ (NSValueTransformer *)iconJSONTransformer{
//    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
//            ^id(NSString * modelType){
//                NSMutableString *mStr = modelType.mutableCopy;
//                if ([mStr containsString:@"/views/image/static/img/tabbar/"]) {
//                    [mStr deleteString:@"/views/image/static/img/tabbar/"];
//                    [mStr deleteString:@".png"];
//                    if (mStr.length) {
//                        [mStr insertString:@"tabbar_" atIndex:0];
//                    }
//                }else{
//                    mStr = [NSString stringWithFormat:@"%@%@",SerVer_Url,modelType].mutableCopy;
//                }
//                return mStr;
//            } reverseBlock:^id(NSNumber * type) {
//                return nil;
//            }];
//}
//
//+ (NSValueTransformer *)selIconJSONTransformer{
//    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
//            ^id(NSString * modelActiveImage){
//                NSMutableString *mStr = modelActiveImage.mutableCopy;
//                if ([mStr containsString:@"/views/image/static/img/tabbar/"]) {
//                    [mStr deleteString:@"/views/image/static/img/tabbar/"];
//                    [mStr deleteString:@".png"];
//                    if (mStr.length) {
//                        [mStr insertString:@"tabbar_" atIndex:0];
//                    }
//                }else{
//                    
//                    mStr = [NSString stringWithFormat:@"%@%@",SerVer_Url,modelActiveImage].mutableCopy;
//                }
//                return mStr;
//            } reverseBlock:^id(NSNumber * type) {
//                return nil;
//            }];
//}

kJSONTransformerMethodImpTemplate(NSValueTransformer *, icon, @selector(iconTransformer))
//
kJSONTransformerMethodImpTemplate(NSValueTransformer *, selIcon, @selector(iconTransformer))

+(NSUInteger)tabBarIndexByRouterKey:(NSString *)routerKey{
    __block NSUInteger index = NSNotFound;
    [BET_CONFIG.allocation.templateList_tabbar enumerateObjectsUsingBlock:^(TabBarTemplate * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.type containsString:routerKey]) {
            index = idx;
            *stop = YES;
        }
    }];
    return index;
}

@end

@implementation HomeFlexTemplate

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"name":@"modelName",
             @"type": @"modelComponents"};
}
+ (NSValueTransformer *)typeJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString * modelComponents){
                NSNumber *type = getFlexTemplateType(modelComponents);
                return  type;
            } reverseBlock:^id(NSNumber * type) {
                return nil;
            }];
}


NSNumber *getFlexTemplateType(NSString *key){
    NSNumber *number = @{
                         @"AppCustomizeModel"  :       @(HomeFlexTemplateType_CUS),
                         @"AppIndexDpgame"     :       @(HomeFlexTemplateType_INDEX_DP),
                         @"AppIndexWinner"     :       @(HomeFlexTemplateType_INDEX_WINNER),
                         @"AppIndexQuick"      :       @(HomeFlexTemplateType_INDEX_QUICK),
                         @"AppIndexQuick2"     :       @(HomeFlexTemplateType_INDEX_QUICK2),
                         @"AppIndexNotice"     :       @(HomeFlexTemplateType_NOTICE),
                         @"AppIndexLottery"    :       @(HomeFlexTemplateType_LOTTERY),
                         @"AppIndexBanner"     :       @(HomeFlexTemplateType_BANNER),
                         @"AppIndexSlider2"    :       @(HomeFlexTemplateType_SLIDER),
                         @"AppIndexChat"       :       @(HomeFlexTemplateType_HORCANVA),
                         @"AppIndexGamesLobby" :       @(HomeFlexTemplateType_Lobby),
                         @"AppIndexGamesLobby3":       @(HomeFlexTemplateType_Lobby3),
                         @"AppOnLineCount"     :       @(HomeFlexTemplateType_AppOnLineCount),
                         }[key];
    if (!number) {
        return @(HomeFlexTemplateType_NONE);
    }
    return number;
}
@end

@implementation QuickTemplate

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"name":@"modelName",
             @"link": @"modelHref",
             @"imgUrl": @"modelImage",
             @"displayType":@"modelIsLogginedShow"
             };
}

+ (NSValueTransformer *)displayTypeJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString * modelIsLogginedShow){
                return @([modelIsLogginedShow integerValue]);
            } reverseBlock:^id(NSNumber * isLogin) {
                return [isLogin stringValue];
            }];
}

kJSONTransformerMethodImpTemplate(NSValueTransformer *, link, @selector(routerKeyTransformer))

kJSONTransformerMethodImpTemplate(NSValueTransformer *, attr, @selector(templateAttrTransformer))

kJSONTransformerMethodImpTemplate(NSValueTransformer *, imgUrl, @selector(iconTransformer))

@end


@implementation CusTemplateItem
+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"name":@"modelName",
             @"link": @"modelHref",
             @"appImg":@"modelImage",
             @"attr":@"menuAttr",
             @"type":@"modelType"
             };
}

kJSONTransformerMethodImpTemplate(NSValueTransformer *, type, @selector(templateJumpTypeTransformer))

kJSONTransformerMethodImpTemplate(NSValueTransformer *, link, @selector(routerKeyTransformer))

kJSONTransformerMethodImpTemplate(NSValueTransformer *, attr, @selector(templateAttrTransformer))

kJSONTransformerMethodImpTemplate(NSValueTransformer *, appImg, @selector(iconTransformer))

@end

@implementation CusTemplate

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"headName":@"modelName",
             @"headImage": @"modelImage",
             @"items":@"modelNext"};
}

+ (NSValueTransformer *)itemsJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSArray <NSDictionary *>*list){
                __block NSError *error;
                NSArray *items = [MTLJSONAdapter modelsOfClass:[CusTemplateItem class] fromJSONArray:list error:&error];
                return items;
            } reverseBlock:^id(NSArray <CusTemplateItem *>*list) {
                __block NSError *error;
                NSArray *items = [MTLJSONAdapter JSONArrayFromModels:list error:&error];
                return items;
            }];
}

kJSONTransformerMethodImpTemplate(NSValueTransformer *, headImage, @selector(iconTransformer))

@end

@implementation DpTemplate

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"name":@"modelName",
             @"icon":@"modelImage",
             @"hot":@"modelHot",
             @"link":@"modelHref"};
}

kJSONTransformerMethodImpTemplate(NSValueTransformer *, link, @selector(routerKeyTransformer))

kJSONTransformerMethodImpTemplate(NSValueTransformer *, icon, @selector(iconTransformer))

kJSONTransformerMethodImpTemplate(NSValueTransformer *, hot, @selector(iconTransformer))



@end



@implementation MainAllocationModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"templateList_tabbar":@"main_tab",
             @"templateList_flex": @"main_flex",
             @"templateList_quick":@"main_quick",
             @"templateList_cus":@"main_cus",
             @"templateList_dpGame":@"main_DpGame",
             @"allocationName":@"main_flex_name",
             @"skinName":@"main_theme"
             };
}

+ (NSValueTransformer *)skinNameJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString *main_theme){
                return [SkinConfig getSkinName:main_theme];
            } reverseBlock:^id(NSString *skinName) {
                return [SkinConfig getMainTheme:skinName];
            }];
}

+ (NSValueTransformer *)templateList_dpGameJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSArray <NSDictionary *>*list){
                __block NSError *error;
                NSArray *items = [MTLJSONAdapter modelsOfClass:[DpTemplate class] fromJSONArray:list error:&error];
                return items;
            } reverseBlock:^id(NSArray <DpTemplate *>*list) {
                
                __block NSError *error;
                NSArray *items = [MTLJSONAdapter JSONArrayFromModels:list error:&error];
                return items;
            }];
}

+ (NSValueTransformer *)templateList_tabbarJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSArray <NSDictionary *>*list){
                __block NSError *error;
                NSArray *items = [MTLJSONAdapter modelsOfClass:[TabBarTemplate class] fromJSONArray:list error:&error];
                return items;
            } reverseBlock:^id(NSArray <TabBarTemplate *>*list) {
                
                __block NSError *error;
                NSArray *items = [MTLJSONAdapter JSONArrayFromModels:list error:&error];
                return items;
            }];
}

+ (NSValueTransformer *)templateList_flexJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSArray <NSDictionary *>*list){
                __block NSError *error;
                NSArray *items = [MTLJSONAdapter modelsOfClass:[HomeFlexTemplate class] fromJSONArray:list error:&error];
                return items;
            } reverseBlock:^id(NSArray <HomeFlexTemplate *>*list) {
                
                __block NSError *error;
                NSArray *items = [MTLJSONAdapter JSONArrayFromModels:list error:&error];
                return items;
            }];
}

+ (NSValueTransformer *)templateList_quickJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSArray <NSDictionary *>*list){
                __block NSError *error;
                NSArray *items = [MTLJSONAdapter modelsOfClass:[QuickTemplate class] fromJSONArray:list error:&error];
                return items;
            } reverseBlock:^id(NSArray <QuickTemplate *>*list) {
                
                __block NSError *error;
                NSArray *items = [MTLJSONAdapter JSONArrayFromModels:list error:&error];
                return items;
            }];
}

+ (NSValueTransformer *)templateList_cusJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSArray <NSDictionary *>*list){
                __block NSError *error;
                NSArray *items = [MTLJSONAdapter modelsOfClass:[CusTemplate class] fromJSONArray:list error:&error];
                return items;
            } reverseBlock:^id(NSArray <CusTemplate *>*list) {
                
                __block NSError *error;
                NSArray *items = [MTLJSONAdapter JSONArrayFromModels:list error:&error];
                return items;
            }];
}

+ (NSValueTransformer *)templateList_menuJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSArray <NSDictionary *>*list){
                __block NSError *error;
                NSArray *items = [MTLJSONAdapter modelsOfClass:[MenuTemplate class] fromJSONArray:list error:&error];
                return items;
            } reverseBlock:^id(NSArray <MenuTemplate *>*list) {
                
                __block NSError *error;
                NSArray *items = [MTLJSONAdapter JSONArrayFromModels:list error:&error];
                return items;
            }];
}

@end
