//
//  TestKindQuery.h
//  Bet365
//
//  Created by adnin on 2019/8/3.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "MTLModel+Define.h"

NS_ASSUME_NONNULL_BEGIN

@interface TestKindQuery : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSString * imgUrl;

@property (nonatomic, copy) NSString * code;

@property (nonatomic, copy) NSString * name;

@property (nonatomic, copy) NSString * game;

@property (nonatomic, copy) NSString * firstKind;

@property (nonatomic, assign) NSInteger testOpen;

@property (nonatomic, assign) NSInteger isOpen;

@property (nonatomic, assign) NSInteger sort;

@property (nonatomic, assign) NSInteger enableDemo;

@property (nonatomic, copy) NSDate * maintenanceTimeStart;

@property (nonatomic, copy) NSDate * maintenanceTimeEnd;

@property (nonatomic, copy) NSDate * testMaintenanceTimeStart;

@property (nonatomic, copy) NSDate * testMaintenanceTimeEnd;

@property (nonatomic, copy) NSDate * addTime;

@property (nonatomic, copy) NSDate * updateTime;

@end

NS_ASSUME_NONNULL_END
