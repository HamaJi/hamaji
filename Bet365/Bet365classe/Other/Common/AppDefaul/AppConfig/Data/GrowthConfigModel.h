//
//  GrowthConfig.h
//  Bet365
//
//  Created by adnin on 2019/3/27.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "MTLModel+Define.h"

@interface GrowthLevelModel : NSObject

@property (nonatomic,strong)NSString *creditMoney;

@property (nonatomic,strong)NSNumber *growth;

@property (nonatomic,strong)NSNumber *level;

@property (nonatomic,strong)NSString *monthWage;

@property (nonatomic,strong)NSString *upgradeReward;

@property (nonatomic,strong)NSString *weekWage;

@end

@interface GrowthConfigModel : MTLModel<MTLJSONSerializing>

@property (nonatomic,strong)NSArray *completeUserInfoFields;

@property (nonatomic,strong)NSNumber *bindBankGrowth;

@property (nonatomic,strong)NSNumber *checkInMaxGrowth;

@property (nonatomic,strong)NSNumber *checkInOneDayGrowth;

@property (nonatomic,strong)NSNumber *completeUserInfoGrowth;

@property (nonatomic,strong)NSNumber *rechargeTransferGrowthRate;

@property (nonatomic,strong)NSNumber *isShowGrowthLevel;

@end



