//
//  TestKindQuery.m
//  Bet365
//
//  Created by adnin on 2019/8/3.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "TestKindQuery.h"

@implementation TestKindQuery

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{
             @"code":@"code",
             @"name":@"name",
             @"game":@"game",
             @"firstKind":@"firstKind",
             @"testOpen":@"testOpen",
             @"isOpen":@"isOpen",
             @"sort":@"sort",
             @"maintenanceTimeStart":@"maintenanceTimeStart",
             @"maintenanceTimeEnd":@"maintenanceTimeEnd",
             @"testMaintenanceTimeStart":@"testMaintenanceTimeStart",
             @"testMaintenanceTimeEnd":@"testMaintenanceTimeEnd",
             @"addTime":@"addTime",
             @"updateTime":@"updateTime",
             @"enableDemo":@"enableDemo",
             @"imgUrl":@"imgUrl",
             };
}


kJSONTransformerMethodImpTemplate(NSValueTransformer *, maintenanceTimeStart, @selector(timeJSONTransformer))

kJSONTransformerMethodImpTemplate(NSValueTransformer *, maintenanceTimeEnd, @selector(timeJSONTransformer))

kJSONTransformerMethodImpTemplate(NSValueTransformer *, testMaintenanceTimeStart, @selector(timeJSONTransformer))

kJSONTransformerMethodImpTemplate(NSValueTransformer *, testMaintenanceTimeEnd, @selector(timeJSONTransformer))

kJSONTransformerMethodImpTemplate(NSValueTransformer *, addTime, @selector(timeJSONTransformer))

kJSONTransformerMethodImpTemplate(NSValueTransformer *, updateTime, @selector(timeJSONTransformer))

@end
