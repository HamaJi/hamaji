//
//  SliderModel.m
//  Bet365
//
//  Created by super_小鸡 on 2019/9/22.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "SliderModel.h"

@implementation SliderModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"icon":@"icon",
             @"url":@"url"
             };
}

kJSONTransformerMethodImpTemplate(NSValueTransformer *, icon, @selector(iconTransformer))

@end
