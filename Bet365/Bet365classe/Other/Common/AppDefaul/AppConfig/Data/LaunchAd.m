//
//  LaunchAd.m
//  Bet365
//
//  Created by adnin on 2019/7/28.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "LaunchAd.h"

@implementation LaunchAd

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"href":@"href",
             @"image":@"image",
             @"showTime":@"showTime",
             @"type":@"type"
             };
}

@end
