//
//  App_config.h
//  sport
//
//  Created by luke on 2018/7/30.
//  Copyright © 2018年 luke. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommonConfig : AppDefaults
/**是否显示真人体育  */
@property (nonatomic,assign) BOOL isDP;
///**是否显示王者荣耀  */
//@property (nonatomic,copy) NSNumber * isWzry;

/**  */
@property (copy, nonatomic) NSString *appName;
/**  */
@property (copy, nonatomic) NSString *appLogo;
/**  */
@property (copy, nonatomic) NSString *forgetPasswordTip;
/**优惠活动  */
@property (copy, nonatomic) NSString *yhhdPath;
/**在线客服  */
@property (copy, nonatomic) NSString *zxkfPath;
/**红包图片  */
@property (copy, nonatomic) NSString *redPacketPath;
/**红包路径  */
@property (copy, nonatomic) NSString *isRedHref;
/**是否显示红包  */
@property (nonatomic,copy) NSNumber * isShowRedPacket;
/**是否显示浮动红包  */
@property (nonatomic,copy) NSNumber * isShowFloatRedPacket;

@property (nonatomic,assign) BOOL isShowChat;

@property (nonatomic,assign) BOOL isFlexDp;


@property (copy, nonatomic) NSString *chatRoomHref;

@end


