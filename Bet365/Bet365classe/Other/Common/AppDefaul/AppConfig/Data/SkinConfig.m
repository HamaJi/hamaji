//
//  SkinConfig.m
//  Bet365
//
//  Created by super_小鸡 on 2019/6/19.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "SkinConfig.h"
@implementation UIColor (Skin)

+(NSString *)skinDefaultName{
    return @"skin_color_default";
}

+(NSString *)skinPinkName{
    return @"skin_color_pink";
}

+(NSString *)skinBlackName{
    return @"skin_color_black";
}

+(NSString *)skinGreenName{
    return @"skin_color_green";
}

+(NSString *)skinBrownName{
    return @"skin_color_brown";
}

+(NSString *)skinBlueName{
    return @"skin_color_blue";
}

+(NSString *)skinWihteName{
    return @"skin_color_white";
}

+(NSString *)skinGoldName{
    return @"skin_color_gold";
}

+(NSString *)skinMintgreenName{
    return @"skin_color_mintgreen";
}

+ (NSString *)skinCoffeeName{
    return @"skin_color_coffee";
}

+(UIColor *)skinNaviBgColor{
    return BET_CONFIG.skin.navi_bg_color;
}

+(UIColor *)skinNaviTintColor{
    return BET_CONFIG.skin.navi_tint_color;
}

+(UIColor *)skinTabBgColor{
    return BET_CONFIG.skin.tab_bg_color;
}

+(UIColor *)skinTabNorColor{
    return BET_CONFIG.skin.tab_nor_color;
}

+(UIColor *)skinTabSelColor{
    return BET_CONFIG.skin.tab_sel_color;
}

+(UIColor *)skinViewScreenColor{
    return BET_CONFIG.skin.view_screen_color;
}

+(UIColor *)skinViewBgColor{
    return BET_CONFIG.skin.view_bg_color;
}

+(UIColor *)skinViewContentColor{
    return BET_CONFIG.skin.view_contentview_color;
}

+(UIColor *)skinTextTitileColor{
    return BET_CONFIG.skin.text_titile_color;
}

+(UIColor *)skinTextHeadColor{
    return BET_CONFIG.skin.text_head_color;
}

+(UIColor *)skinTextItemNorColor{
    return BET_CONFIG.skin.text_item_nor_color;
}

+(UIColor *)skinTextItemNorSubColor{
    return BET_CONFIG.skin.text_item_nor_sub_color;
}

+(UIColor *)skinTextItemSelColor{
    return BET_CONFIG.skin.text_item_sel_color;
}

+(UIColor *)skinTextItemSelSubColor{
    return BET_CONFIG.skin.text_item_sel_sub_color;
}

+(UIColor *)skinViewKitNorColor{
    return BET_CONFIG.skin.view_kit_nor_color;
}

+(UIColor *)skinViewKitSelColor{
    return BET_CONFIG.skin.view_kit_sel_color;
}

+(UIColor *)skinViewKitDisColor{
    return BET_CONFIG.skin.view_kit_dis_color;
}

+(UIColor *)skinViewCusHeadBgColor{
    return BET_CONFIG.skin.view_cus_headBg_color;
}


+(UIColor *)skinViewCusBgColor{
    return BET_CONFIG.skin.view_cus_bg_color;
}

+(UIColor *)skinViewQuickBgColor{
    return BET_CONFIG.skin.view_quick_bg_color;
}





@end

@implementation UIImage (Gradient)



+(UIImage *)skinGradientHorNaviImage{
    
    if ([BET_CONFIG.skin.skinName isEqualToString:[UIColor skinGoldName]]) {
        UIImage *img = [UIImage gradientImageWithSize:CGSizeMake(WIDTH, STATUSAND_NAVGATION_HEIGHT)
                                            andColors:@[[UIColor skinNaviBgColor],COLOR_WITH_HEX(0x795548)]
                                         GradientType:UIImageGradientType_HOZ];
        return img;
    }else if ([BET_CONFIG.skin.skinName isEqualToString:[UIColor skinCoffeeName]]) {
        UIImage *img = [UIImage gradientImageWithSize:CGSizeMake(WIDTH, STATUSAND_NAVGATION_HEIGHT)
                                            andColors:@[[UIColor skinNaviBgColor],[UIColor colorWithHexString:@"#D2BAA7"]]
                                         GradientType:UIImageGradientType_HOZ];
        return img;
    }else{
        UIImage *img = [UIImage imageWithColor:[UIColor skinNaviBgColor]
                                      withSize:CGSizeMake(WIDTH, TABBAR_HEIGHT)];
        return img;
    }
}

+(UIImage *)skinGradientVerNaviImage{
    
    if ([BET_CONFIG.skin.skinName isEqualToString:[UIColor skinGoldName]]) {
        UIImage *img = [UIImage gradientImageWithSize:CGSizeMake(WIDTH, STATUSAND_NAVGATION_HEIGHT)
                                            andColors:@[[UIColor skinNaviBgColor],COLOR_WITH_HEX(0x795548)]
                                         GradientType:UIImageGradientType_VER];
        return img;
    }else if ([BET_CONFIG.skin.skinName isEqualToString:[UIColor skinCoffeeName]]) {
        UIImage *img = [UIImage gradientImageWithSize:CGSizeMake(WIDTH, STATUSAND_NAVGATION_HEIGHT)
                                            andColors:@[[UIColor skinNaviBgColor],[UIColor colorWithHexString:@"#D2BAA7"]]
                                         GradientType:UIImageGradientType_VER];
        return img;
    }else{
        UIImage *img = [UIImage imageWithColor:[UIColor skinNaviBgColor]
                                      withSize:CGSizeMake(WIDTH, TABBAR_HEIGHT)];
        return img;
    }
}

+(UIImage *)skinGradientSelImage{
    if ([BET_CONFIG.skin.skinName isEqualToString:[UIColor skinGoldName]]) {
        UIImage *img = [UIImage gradientImageWithSize:CGSizeMake(WIDTH, TABBAR_HEIGHT)
                                            andColors:@[[UIColor skinViewKitSelColor],COLOR_WITH_HEX(0x795548)]
                                         GradientType:UIImageGradientType_HOZ];
        return img;
    }else if ([BET_CONFIG.skin.skinName isEqualToString:[UIColor skinCoffeeName]]) {
        UIImage *img = [UIImage gradientImageWithSize:CGSizeMake(WIDTH, STATUSAND_NAVGATION_HEIGHT)
                                            andColors:@[[UIColor skinNaviBgColor],[UIColor colorWithHexString:@"#D2BAA7"]]
                                         GradientType:UIImageGradientType_HOZ];
        return img;
    }else{
        UIImage *img = [UIImage imageWithColor:[UIColor skinViewKitSelColor]
                                      withSize:CGSizeMake(WIDTH, TABBAR_HEIGHT)];
        return img;
    }
}




@end

@implementation CAGradientLayer (Gradient)


+(CAGradientLayer *)gradientLayer:(CGRect)bounds FirColor:(UIColor *)firColor SecColor:(UIColor *)secColor{
    CAGradientLayer * gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = bounds;
    NSMutableArray *colors = @[].mutableCopy;
    if (firColor) {
        [colors safeAddObject:(__bridge id)firColor.CGColor];
    }
    if (secColor && ![secColor isEqual:firColor]) {
        [colors safeAddObject:(__bridge id)secColor.CGColor];
    }
    gradientLayer.colors = colors;
    gradientLayer.startPoint = CGPointMake(0, 0);
    gradientLayer.endPoint = CGPointMake(0, 1);
    gradientLayer.locations = @[@0,@1];
    return gradientLayer;
}

+(CAGradientLayer *)bet35GradientLayer:(CGRect)bounds {
    if ([BET_CONFIG.skin.skinName isEqualToString:[UIColor skinGoldName]]){
        return [self gradientLayer:bounds FirColor:[UIColor skinViewKitSelColor] SecColor:COLOR_WITH_HEX(0x795548)];
    }
    return [self gradientLayer:bounds FirColor:[UIColor skinViewKitSelColor] SecColor:nil];
}

@end

@implementation SkinConfig

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"navi_bg_color":@"navi_bg_color",
             @"navi_tint_color":@"navi_tint_color",
             @"tab_bg_color":@"tab_bg_color",
             @"tab_nor_color":@"tab_nor_color",
             @"tab_sel_color":@"tab_sel_color",
             @"view_screen_color":@"view_screen_color",
             @"view_bg_color":@"view_bg_color",
             @"view_contentview_color":@"view_contentview_color",
             
             @"text_titile_color":@"text_titile_color",
             @"text_head_color":@"text_head_color",
             @"text_item_nor_color":@"text_item_nor_color",
             @"text_item_nor_sub_color":@"text_item_nor_sub_color",
             @"text_item_sel_color":@"text_item_sel_color",
             @"text_item_sel_sub_color":@"text_item_sel_sub_color",
             
             @"view_kit_nor_color":@"view_kit_nor_color",
             @"view_kit_sel_color":@"view_kit_sel_color",
             @"view_kit_dis_color":@"view_kit_dis_color",
             
             @"view_cus_headBg_color":@"view_cus_headBg_color",
             @"view_cus_bg_color":@"view_cus_bg_color",
             @"view_quick_bg_color":@"view_quick_bg_color",
             
             };
}

kJSONTransformerMethodImpTemplate(NSValueTransformer *, navi_bg_color,                   @selector(colorTransFormer))
kJSONTransformerMethodImpTemplate(NSValueTransformer *, navi_tint_color,                   @selector(colorTransFormer))

kJSONTransformerMethodImpTemplate(NSValueTransformer *, tab_bg_color,                    @selector(colorTransFormer))
kJSONTransformerMethodImpTemplate(NSValueTransformer *, tab_nor_color,                  @selector(colorTransFormer))
kJSONTransformerMethodImpTemplate(NSValueTransformer *, tab_sel_color,                  @selector(colorTransFormer))
kJSONTransformerMethodImpTemplate(NSValueTransformer *, view_screen_color,               @selector(colorTransFormer))
kJSONTransformerMethodImpTemplate(NSValueTransformer *, view_bg_color,                   @selector(colorTransFormer))
kJSONTransformerMethodImpTemplate(NSValueTransformer *, view_contentview_color,          @selector(colorTransFormer))

kJSONTransformerMethodImpTemplate(NSValueTransformer *, text_titile_color,          @selector(colorTransFormer))
kJSONTransformerMethodImpTemplate(NSValueTransformer *, text_head_color,          @selector(colorTransFormer))
kJSONTransformerMethodImpTemplate(NSValueTransformer *, text_item_nor_color,          @selector(colorTransFormer))
kJSONTransformerMethodImpTemplate(NSValueTransformer *, text_item_nor_sub_color,          @selector(colorTransFormer))
kJSONTransformerMethodImpTemplate(NSValueTransformer *, text_item_sel_color,          @selector(colorTransFormer))
kJSONTransformerMethodImpTemplate(NSValueTransformer *, text_item_sel_sub_color,          @selector(colorTransFormer))

kJSONTransformerMethodImpTemplate(NSValueTransformer *, view_kit_nor_color,              @selector(colorTransFormer))
kJSONTransformerMethodImpTemplate(NSValueTransformer *, view_kit_sel_color,              @selector(colorTransFormer))
kJSONTransformerMethodImpTemplate(NSValueTransformer *, view_kit_dis_color,              @selector(colorTransFormer))

kJSONTransformerMethodImpTemplate(NSValueTransformer *, view_cus_headBg_color,        @selector(colorTransFormer))
kJSONTransformerMethodImpTemplate(NSValueTransformer *, view_cus_bg_color,            @selector(colorTransFormer))
kJSONTransformerMethodImpTemplate(NSValueTransformer *, view_quick_bg_color,          @selector(colorTransFormer))




#pragma mark - DefaultColor

-(NSString *)skinName{
    if (!_skinName) {
        _skinName = [UIColor skinDefaultName];
    }
    return _skinName;
}





-(UIColor *)navi_bg_color{
    if (!_navi_bg_color) {
        _navi_bg_color = JesseColor(196, 7, 26);
    }
    return _navi_bg_color;
}

-(UIColor *)navi_tint_color{
    if (!_navi_tint_color) {
        _navi_tint_color = COLOR_WITH_HEX(0xffffff);
    }
    return _navi_tint_color;
}

-(UIColor *)tab_bg_color{
    if (!_tab_bg_color) {
        _tab_bg_color = [UIColor whiteColor];
    }
    return _tab_bg_color;
}

-(UIColor *)tab_nor_color{
    if (!_tab_nor_color) {
        _tab_nor_color = [UIColor grayColor];
    }
    return _tab_nor_color;
}

-(UIColor *)tab_sel_color{
    if (!_tab_sel_color) {
        _tab_sel_color = JesseColor(196, 7, 26);
    }
    return _tab_sel_color;
}

-(UIColor *)view_screen_color{
    if (!_view_screen_color) {
        _view_screen_color = [UIColor groupTableViewBackgroundColor];
    }
    return _view_screen_color;
}

-(UIColor *)view_bg_color{
    if (!_view_bg_color) {
        _view_bg_color = [UIColor whiteColor];
    }
    return _view_bg_color;
}

-(UIColor *)view_contentview_color{
    if (!_view_contentview_color) {
        _view_contentview_color = COLOR_WITH_HEX(0xD60101);
    }
    return _view_contentview_color;
}

-(UIColor *)text_titile_color{
    if (!_text_titile_color) {
        _text_titile_color = [UIColor whiteColor];
    }
    return _text_titile_color;
}

-(UIColor *)text_head_color{
    if (!_text_head_color) {
        _text_head_color = [UIColor blackColor];
    }
    return _text_head_color;
}

-(UIColor *)text_item_nor_color{
    if (!_text_item_nor_color) {
        _text_item_nor_color = [UIColor blackColor];
    }
    return _text_item_nor_color;
}

-(UIColor *)text_item_nor_sub_color{
    if (!_text_item_nor_sub_color) {
        _text_item_nor_sub_color = [UIColor grayColor];
    }
    return _text_item_nor_sub_color;
}

-(UIColor *)text_item_sel_color{
    if (!_text_item_sel_color) {
        _text_item_sel_color = [UIColor redColor];
    }
    return _text_item_sel_color;
}

-(UIColor *)text_item_sel_sub_color{
    if (!_text_item_sel_sub_color) {
        _text_item_sel_sub_color = COLOR_WITH_HEX(0xFF5280);
    }
    return _text_item_sel_sub_color;
}

-(UIColor *)view_kit_nor_color{
    if (!_view_kit_nor_color) {
        _view_kit_nor_color = [UIColor whiteColor];
    }
    return _view_kit_nor_color;
}

-(UIColor *)view_kit_sel_color{
    if (!_view_kit_sel_color) {
        _view_kit_sel_color = JesseColor(196, 7, 26);
    }
    return _view_kit_sel_color;
}

-(UIColor *)view_kit_dis_color{
    if (!_view_kit_dis_color) {
        _view_kit_dis_color = [UIColor grayColor];
    }
    return _view_kit_dis_color;
}


-(UIColor *)view_quick_bg_color{
    if (!_view_quick_bg_color) {
        _view_quick_bg_color = [UIColor groupTableViewBackgroundColor];
    }
    return _view_quick_bg_color;
}

-(UIColor *)view_cus_headBg_color{
    if (!_view_cus_headBg_color) {
        _view_cus_headBg_color = [UIColor whiteColor];
    }
    return _view_cus_headBg_color;
}

-(UIColor *)view_cus_bg_color{
    if (!_view_cus_bg_color) {
        _view_cus_bg_color = [UIColor groupTableViewBackgroundColor];
    }
    return _view_cus_bg_color;
}





#pragma mark - Public
+(NSString *)getSkinName:(NSString *)mainTheme{
    if ([mainTheme isEqualToString:@"2"]) {
        return [UIColor skinPinkName];
    }else if ([mainTheme isEqualToString:@"3"]) {
        return [UIColor skinBlackName];
    }else if ([mainTheme isEqualToString:@"4"]) {
        return [UIColor skinGreenName];
    }else if ([mainTheme isEqualToString:@"5"]) {
        return [UIColor skinBlueName];
    }else if ([mainTheme isEqualToString:@"6"]) {
        return [UIColor skinBrownName];
    }else if ([mainTheme isEqualToString:@"7"]) {
        return [UIColor skinWihteName];
    }else if ([mainTheme isEqualToString:@"8"]) {
        return [UIColor skinGoldName];
    }else if ([mainTheme isEqualToString:@"9"]) {
        return [UIColor skinMintgreenName];
    }else if ([mainTheme isEqualToString:@"10"]) {
        return [UIColor skinCoffeeName];
    }else{
        return [UIColor skinDefaultName];
    }
}

+(NSString *)getMainTheme:(NSString *)skinName{
    if ([skinName isEqualToString:[UIColor skinPinkName]]) {
        return @"2";
    }else if ([skinName isEqualToString:[UIColor skinBlackName]]) {
        return @"3";
    }else if ([skinName isEqualToString:[UIColor skinGreenName]]) {
        return @"4";
    }else if ([skinName isEqualToString:[UIColor skinBlueName]]) {
        return @"5";
    }else if ([skinName isEqualToString:[UIColor skinBrownName]]) {
        return @"6";
    }else if ([skinName isEqualToString:[UIColor skinWihteName]]) {
        return @"7";
    }else if ([skinName isEqualToString:[UIColor skinGoldName]]) {
        return @"8";
    }else if ([skinName isEqualToString:[UIColor skinMintgreenName]]){
        return @"9";
    }else if ([skinName isEqualToString:[UIColor skinCoffeeName]]){
        return @"10";
    }else{
        return @"1";
    }
}



@end

