//
//  UserCenterTemplateModel.h
//  Bet365
//
//  Created by adnin on 2019/3/23.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "MTLModel+Define.h"

@interface CenterMenuItemTemplate : MTLModel<MTLJSONSerializing>

@property (nonatomic,strong)NSString *name;

@property (nonatomic,strong)NSString *appLink;

@property (nonatomic,strong)NSString *icon;

@property (nonatomic,assign)BOOL isBuge;

@end

@interface CenterMenuSectionTemplate : MTLModel<MTLJSONSerializing>

@property (nonatomic,strong)NSString *titile;

@property (nonatomic,strong)NSArray <CenterMenuItemTemplate *>*menuList;

@end

@interface UserCenterTemplateModel : MTLModel<MTLJSONSerializing>

@property (nonatomic,strong)NSArray <CenterMenuSectionTemplate *>*centerMenuList;

@property (nonatomic,strong)NSArray <CenterMenuSectionTemplate *>*centerDlMenuList;

@end
