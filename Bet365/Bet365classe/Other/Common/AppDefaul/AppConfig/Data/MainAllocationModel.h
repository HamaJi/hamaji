//
//  MainFlexModel.h
//  Bet365
//
//  Created by adnin on 2018/11/27.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "MTLModel+Define.h"
#import "SkinConfig.h"
//typedef NS_ENUM(NSInteger, TabBarTemplateType) {
//    /** none*/
//    TabBarTemplateType_NONE = 0,
//    /** 首页*/
//    TabBarTemplateType_HOME,
//    /** 个人*/
//    TabBarTemplateType_PERSON,
//    /** 购彩*/
//    TabBarTemplateType_gcdt,
//    /** 体育*/
//    TabBarTemplateType_sports,
//    /** 开奖*/
//    TabBarTemplateType_kj,
//    /** 走势*/
//    TabBarTemplateType_zs,
//    /** 棋牌*/
//    TabBarTemplateType_chess,
//    /** 长龙*/
//    TabBarTemplateType_tulongbang,
//    /** 充值*/
//    TabBarTemplateType_recharge,
//    /** 优惠*/
//    TabBarTemplateType_yhhd,
//    /** 聊天室*/
//    TabBarTemplateType_chat,
//    /** 电子*/
//    TabBarTemplateType_electronic,
//    /** 真人*/
//    TabBarTemplateType_live,
//};

typedef NS_ENUM(NSInteger,TemplateJumpType) {
    
    /** 无参*/
    TemplateJumpType_ROUTER_ONLYKEY = 0,
    /** 有参*/
    TemplateJumpType_ROUTER_PARAMERS,
};
@interface MenuAttrTemplate : MTLModel<MTLJSONSerializing>

@property (nonatomic,strong)NSString *gameKind; //"AGIN";

@property (nonatomic,strong)NSString *gameType; //"6";

@property (nonatomic,strong)NSString *liveCode; //"ag";

@end

@interface MenuTemplate : MTLModel<MTLJSONSerializing>

@property (nonatomic,strong)NSString *name;

@property (nonatomic,strong)NSString *appLink;

@property (nonatomic,assign)TemplateJumpType type;

@property (nonatomic,strong)MenuAttrTemplate *menuAttr;

@end

@interface TabBarTemplate : MTLModel<MTLJSONSerializing>

@property (nonatomic,strong)NSString *name;

@property (nonatomic,strong)NSString *type;

@property (nonatomic,strong)NSString *icon;

@property (nonatomic,strong)NSString *selIcon;

NSNumber *getTabarTemplateType(NSString *key);
                               
NSString *getTabarViewControllerClassName(NSString *key);

+(NSUInteger)tabBarIndexByRouterKey:(NSString *)routerKey;

@end

typedef NS_ENUM(NSInteger, HomeFlexTemplateType) {
    
    HomeFlexTemplateType_NONE = 0,
    /** 自定义模板*/
    HomeFlexTemplateType_CUS,
    /** 大盘体育模块*/
    HomeFlexTemplateType_INDEX_DP,
    /** 中奖排行榜*/
    HomeFlexTemplateType_INDEX_WINNER,
    /** 快速入口*/
    HomeFlexTemplateType_INDEX_QUICK,
    /** 快速入口2*/
    HomeFlexTemplateType_INDEX_QUICK2,
    /** 公告*/
    HomeFlexTemplateType_NOTICE,
    /** 彩票*/
    HomeFlexTemplateType_LOTTERY,
    /** 轮播图*/
    HomeFlexTemplateType_BANNER,
    /** 轮播图2*/
    HomeFlexTemplateType_SLIDER,
    /** 横幅*/
    HomeFlexTemplateType_HORCANVA,
    /** 游戏大厅二*/
    HomeFlexTemplateType_Lobby,
    /** 游戏大厅三*/
    HomeFlexTemplateType_Lobby3,
    /** 在线人数*/
    HomeFlexTemplateType_AppOnLineCount,
};

typedef NS_ENUM(NSInteger, HomeQuickTemplateDisplayType) {
    /** 未登录*/
    HomeQuickTemplateDisplayType_UNLOGIN = 0,
    /** 登录*/
    HomeQuickTemplateDisplayType_LOGIN,
    /** 默认*/
    HomeQuickTemplateDisplayType_DEFAULTS
};

@interface HomeFlexTemplate : MTLModel<MTLJSONSerializing>

@property (nonatomic,strong)NSString *name;

@property (nonatomic,assign)HomeFlexTemplateType type;

NSNumber *getFlexTemplateType(NSString *key);

@end

@interface QuickTemplate : MTLModel<MTLJSONSerializing>

@property (nonatomic,assign)HomeQuickTemplateDisplayType displayType;

@property (nonatomic,strong)NSString *name;

@property (nonatomic,strong)NSString *link;

@property (nonatomic,strong)NSString *imgUrl;


@end

@interface CusTemplateItem : MTLModel<MTLJSONSerializing>
@property (nonatomic,strong)NSString *name;
@property (nonatomic,strong)NSString *link;
@property (nonatomic,strong)NSString *appImg;
@property (nonatomic,assign)TemplateJumpType type;
@property (nonatomic,strong)MenuAttrTemplate *attr;
@end

@interface CusTemplate : MTLModel<MTLJSONSerializing>
@property (nonatomic,strong)NSString *headName;
@property (nonatomic,strong)NSString *headImage;
@property (nonatomic,strong)NSArray *items;
@end

@interface DpTemplate : MTLModel<MTLJSONSerializing>
@property (nonatomic,strong)NSString *name;
@property (nonatomic,strong)NSString *icon;
@property (nonatomic,strong)NSString *hot;
@property (nonatomic,strong)NSString *link;
@end

@interface MainAllocationModel : MTLModel<MTLJSONSerializing>

@property (nonatomic,strong)NSArray <TabBarTemplate *>*templateList_tabbar;

@property (nonatomic,strong)NSArray <HomeFlexTemplate *>*templateList_flex;

@property (nonatomic,strong)NSArray <QuickTemplate *>*templateList_quick;

@property (nonatomic,strong)NSArray <CusTemplate *>*templateList_cus;

@property (nonatomic,strong)NSArray <DpTemplate *>*templateList_dpGame;

@property (nonatomic,strong)NSString *allocationName;
    
@property (nonatomic,strong)NSString *skinName;

@end
