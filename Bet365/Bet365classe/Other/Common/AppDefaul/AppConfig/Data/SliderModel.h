//
//  SliderModel.h
//  Bet365
//
//  Created by super_小鸡 on 2019/9/22.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "MTLModel+Define.h"

NS_ASSUME_NONNULL_BEGIN

@interface SliderModel : MTLModel<MTLJSONSerializing>

@property (nonatomic,strong) NSString *icon;

@property (nonatomic,strong) NSString *url;
//
@property (nonatomic,assign)CGSize iconSize;

@end

NS_ASSUME_NONNULL_END
