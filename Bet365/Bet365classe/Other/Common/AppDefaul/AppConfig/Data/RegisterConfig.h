//
//  RegisterConfig.h
//  Bet365
//
//  Created by HHH on 2018/7/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "AppDefaults.h"

@interface RegisterConfig : AppDefaults
/**
 是否开通试玩
 */

@property (nonatomic,assign)BOOL trailUserValidCode;
/**
 
 */
@property (nonatomic,assign)NSInteger fullName;
/**
 
 */
@property (nonatomic,assign)NSInteger email;
/**
 
 */
@property (nonatomic,assign)NSInteger phone;
/**
 
 */
@property (nonatomic,assign)NSInteger intrCode;
/**
 
 */
@property (nonatomic,strong)NSNumber *intrMobileCode;
/**
 
 */
@property (nonatomic,assign)NSInteger qq;
/**
 
 */
@property (nonatomic,assign)NSInteger weixin;
/**
 
 */
@property (nonatomic,assign)NSInteger birthday;
/**
 
 */
@property (nonatomic,assign)BOOL onlyfullName;
/**
 
 */
@property (nonatomic,assign)BOOL ipCount;
/**
 
 */
@property (nonatomic,assign)BOOL sendMenoy;
/**
 
 */
@property (nonatomic,assign)BOOL dmlMenoy;
/**
 
 */
@property (nonatomic,assign)NSInteger vCode;
/**
 
 */
@property (nonatomic,assign)BOOL wechatBonus;
/**
 
 */
@property (nonatomic,assign)BOOL wechatBonusDml;

/**
 
 */
@property (nonatomic,assign)BOOL freeAccountBalance;
/**
 
 */
@property (nonatomic,assign)NSInteger fundPwd;
/**
 
 */
@property (nonatomic,assign)BOOL onlyfullPhone;
/**
 
 */
@property (nonatomic,assign)BOOL onlyBankCard;

@property (nonatomic,copy) NSString *userAccountMessage;
/**  */
@property (nonatomic,copy) NSString *wechatPlaceholder;

@property (nonatomic,copy) NSString *qqPlaceholder;

@property (nonatomic,copy) NSString *phonePlaceholder;

@property (nonatomic,copy) NSString *mailPlaceHolder;

@property (nonatomic,copy) NSString *intr;
@property (nonatomic,copy) NSString *userNameRegisterMessage;

/**
 
 */
@property (nonatomic,assign)NSInteger registerPhone;

/**
 是否开启复杂密码校验 4.20蛤蟆吉【8919】
 */
@property (nonatomic,assign)BOOL isComplexPassword;
@end
