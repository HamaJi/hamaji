//
//  CongfigClusterData.h
//  Bet365
//
//  Created by HHH on 2018/7/7.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "AppDefaults.h"

@interface CongfigClusterData : AppDefaults

///**
// 登录是否需要验证码
// */
//@property (nonatomic,copy)NSNumber *isLogCode;


/**
 <#Description#>
 */
@property (nonatomic,copy)NSString * visitors_rebate;

/**
 <#Description#>
 */
@property (nonatomic,copy)NSString * min_rebate;

/**
 <#Description#>
 */
@property (nonatomic,copy)NSString * lhc_temaB_rebate;

/**
 <#Description#>
 */
@property (nonatomic,copy)NSString * disable_ip_whitelist;

/**
 <#Description#>
 */
@property (nonatomic,strong)NSString * dl_can_create_dl;

/**
 <#Description#>
 */
@property (nonatomic,copy)NSString *hy_is_cancel;
/**
 <#Description#>
 */
@property (nonatomic,copy)NSString *vhy_login_tip;
/**
 <#Description#>
 */
@property (nonatomic,copy)NSString * is_show_bank_detail;

/**
 <#Description#>
 */
@property (nonatomic,copy)NSString * is_show_user_detail;

/**
 <#Description#>
 */
@property (nonatomic,copy)NSString * play_type_config;

/**
 <#Description#>
 */
@property (nonatomic,strong)NSString * site_rebate_model;

/**
 <#Description#>
 */
@property (nonatomic,strong)NSString * user_password_type;

/**
 <#Description#>
 */
@property (nonatomic,copy)NSString * vcode_style;

/**
 <#Description#>
 */
@property (nonatomic,copy)NSString * visitors_can_use_cp_view;

/**
 <#Description#>
 */
@property (nonatomic,copy)NSString *app_spread_domain;

@property (nonatomic,copy)NSString *is_user_bank_modifiable;

/**
 轮播图高度
 */
@property (nonatomic,assign)CGFloat lunboHeight;

///**
// 是否开启王者荣耀
// */
//@property (nonatomic,copy)NSNumber *WZRYStatus;

/**
 当前极六合彩广告链接
 */
@property (nonatomic,copy)NSString *adLinks;
/**
 当前六合彩广告跳对应ID
 */
@property (nonatomic,copy)NSString *game_id;
///**
// 当前广告数据存储
// */
//@property (nonatomic,strong)NSArray *adLinks_Storage;
/**
 当前个人官方返点滑条记录
 */
@property (nonatomic,assign)CGFloat rebet;

@property (nonatomic,copy)NSString *dl_modify_swtich;

@property (nonatomic,copy)NSString *dl_min_code_num;

@property (nonatomic,copy)NSString *user_transfer_stauts;

@property (nonatomic,copy)NSString *recharge_quick_money;

@end
