//
//  LimitModel.m
//  Bet365
//
//  Created by super_小鸡 on 2019/8/15.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "LimitModel.h"

@implementation LimitModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"userPasswordType":@"userPasswordType",
             @"limitIpForMultipleAccount":@"limitIpForMultipleAccount",
             @"overMaxAllowedAccountTips":@"overMaxAllowedAccountTips",
             @"errorPwdOverTimesMsg":@"errorPwdOverTimesMsg",
             @"accountStateErrorMsg":@"accountStateErrorMsg",
             @"errorPwdCount":@"errorPwdCount",
             @"vCode":@"vCode",
             @"errorPwdLimit":@"errorPwdLimit",
             @"ipLocationVerifyTurnOn":@"ipLocationVerifyTurnOn",
             @"ipLocationVerifyFields":@"ipLocationVerifyFields",
             @"ipLocationVerifyCount":@"ipLocationVerifyCount",
//             @"initPwdIfEmpty":@"initPwdIfEmpty",
             
             
//             @"initPwdVerifyCount":@"initPwdVerifyCount",
             @"maxAllowedAccountPerIp":@"maxAllowedAccountPerIp",
             @"ipLocationCode":@"ipLocationCode",
             @"onlyLoginPhone":@"onlyLoginPhone",
             @"loginPhone":@"loginPhone",
             };
}


@end
