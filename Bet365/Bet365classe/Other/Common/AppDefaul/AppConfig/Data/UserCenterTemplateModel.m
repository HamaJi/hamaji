//
//  UserCenterTemplateModel.m
//  Bet365
//
//  Created by adnin on 2019/3/23.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "UserCenterTemplateModel.h"
@implementation CenterMenuItemTemplate

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"name":@"name",
             @"appLink":@"url",
             @"icon":@"icon",
             @"isBuge":@"url"
             };
}

kJSONTransformerMethodImpTemplate(NSValueTransformer *, appLink, @selector(routerKeyTransformer))

kJSONTransformerMethodImpTemplate(NSValueTransformer *, icon, @selector(iconTransformer))

+ (NSValueTransformer *)isBugeJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString *url){
                NSMutableString *mUrl = url.mutableCopy;
                [mUrl deleteString:@"#/"];
                if ([mUrl isEqualToString:kRouterMessage]) {
                    return @(YES);
                }
                return @(NO);
            } reverseBlock:^id(NSNumber *isBuge) {
                return nil;
            }];
}

@end

@implementation CenterMenuSectionTemplate

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"titile":@"title",
             @"menuList":@"menu"};
}

+ (NSValueTransformer *)menuListJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSArray <NSDictionary *>*list){
                __block NSError *error;
                NSArray *items = [MTLJSONAdapter modelsOfClass:[CenterMenuItemTemplate class] fromJSONArray:list error:&error];
                return items;
            } reverseBlock:^id(NSArray <CenterMenuItemTemplate *>*list) {
                
                __block NSError *error;
                NSArray *items = [MTLJSONAdapter JSONArrayFromModels:list error:&error];
                return items;
            }];
}
@end

@implementation UserCenterTemplateModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"centerMenuList":@"centerMenu",
             @"centerDlMenuList":@"centerDlMenu"};
}

+ (NSValueTransformer *)centerMenuListJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSArray *arr){
                __block NSError *error;
                NSArray *list = [MTLJSONAdapter modelsOfClass:[CenterMenuSectionTemplate class] fromJSONArray:arr error:&error];
                return list;
            } reverseBlock:^id(NSArray <CenterMenuSectionTemplate *> *list) {
                
                __block NSError *error;
                NSArray *arr = [MTLJSONAdapter JSONArrayFromModels:list error:&error];
                return arr;
            }];
}

+ (NSValueTransformer *)centerDlMenuListJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSArray *arr){
                __block NSError *error;
                NSArray *list = [MTLJSONAdapter modelsOfClass:[CenterMenuSectionTemplate class] fromJSONArray:arr error:&error];
                return list;
            } reverseBlock:^id(NSArray <CenterMenuSectionTemplate *> *list) {
                
                __block NSError *error;
                NSArray *arr = [MTLJSONAdapter JSONArrayFromModels:list error:&error];
                return arr;
            }];
}
@end
