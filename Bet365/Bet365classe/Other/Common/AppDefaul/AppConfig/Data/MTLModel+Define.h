//
//  MTLModel+Define.h
//  Bet365
//
//  Created by adnin on 2019/3/17.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import <Mantle/Mantle.h>
#import <Foundation/Foundation.h>

@interface NSString (Scale)

@property (nonatomic,readonly)CGFloat width;

@property (nonatomic,readonly)CGFloat height;

@end

#define kJSONTransformerMethodImpTemplate(rCls, name ,sel)                     \
+ (rCls)name##JSONTransformer{                                                 \
    rCls temp = ((rCls (*)(id, SEL))[self methodForSelector:sel])(self, sel);  \
    return temp;                                                               \
}                                                                              \

@interface MTLModel (Define)

+(NSValueTransformer *)colorTransFormer;

+(NSValueTransformer *)iconTransformer;

+(NSValueTransformer *)routerKeyTransformer;

+(NSValueTransformer *)templateAttrTransformer;

+(NSValueTransformer *)templateJumpTypeTransformer;

+(NSValueTransformer *)timeJSONTransformer;

@end
