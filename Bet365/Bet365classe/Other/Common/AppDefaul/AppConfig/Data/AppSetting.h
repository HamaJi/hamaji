//
//  AppSetting.h
//  Bet365
//
//  Created by HHH on 2018/7/15.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "AppDefaults.h"
typedef enum{
    UnitYuanType = 0,
    UnitJiaoType = 1,
    UnitFenType = 2
}UnitType;//钱币单位类型

typedef enum{
    ChatFollowStatus_ALL = 0,
    ChatFollowStatus_ONLY = 1,
}ChatFollowStatus;
@interface AppSetting : AppDefaults


/**
 缓存令牌码
 */
@property (nonatomic,strong) NSString *defaultAppID;

@property (nonatomic,assign)ChatFollowStatus chatFollowStatus;


/**
 聊天推送是否隐藏
 */
@property (nonatomic,assign)BOOL hideChatMessageNotification;

/**
 聊天推送音效是否开启
 */
@property (nonatomic,assign)BOOL openChatMeessageNotifacationSound;

/**
 聊天开奖礼花动画
 */
@property (nonatomic,assign)BOOL closeChatWinFireAnimation;


/**
 聊天顶部彩票是否隐藏
 */
@property (nonatomic,assign)BOOL hideChatLotteryTopView;


/**
 聊天底部菜单是否隐藏
 */
@property (nonatomic,assign)BOOL hideChatBottomBar;

/**
 聊天延迟ping是否隐藏
 */
@property (nonatomic,assign)BOOL hideChatPingView;

/**
 是否开启音乐
 */
@property (nonatomic,assign)BOOL isOpenMusic;

/**
 是否登录过
 */
@property (nonatomic,assign)BOOL hasLogin;


/**
 系统地址
 */
@property (nonatomic,copy)NSString *serverUrl;

@property (nonatomic,copy)NSString *ETag;


///**
// APP是否开启过（首次进去启动页）
// */
//@property (nonatomic,assign)BOOL hasStep;

/**
 悬浮按钮X
 */
@property (nonatomic,assign)CGFloat dragSwipeOriginX;

/**
 悬浮按钮Y
 */
@property (nonatomic,assign)CGFloat dragSwipeOriginY;

/** 是否确认跳转Safari */
@property (nonatomic,assign) BOOL isOpenSafari;

/**
 聊天入口引导
 */
@property (nonatomic,assign) BOOL hasGuideChatEnter;

/**
 聊天引导
 */
@property (nonatomic,assign) BOOL hasGuideChatContent;

@property (nonatomic,assign)UnitType lotteryUnitType;

/** 首页悬浮红包X */
@property (nonatomic,assign) CGFloat floatRedX;
/** 首页悬浮红包Y */
@property (nonatomic,assign) CGFloat floatRedY;

@property (nonatomic,assign) NSInteger updateBuildVersion;

@property (nonatomic,assign) BOOL yeBaoBalanceHiden;

@property (nonatomic,strong) NSString *fv;
@end
