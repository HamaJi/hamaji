//
//  GrowthConfig.m
//  Bet365
//
//  Created by adnin on 2019/3/27.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "GrowthConfigModel.h"

@implementation GrowthLevelModel

@end

@implementation GrowthConfigModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"completeUserInfoFields":@"completeUserInfoFields",
             @"bindBankGrowth":@"bindBankGrowth",
             @"checkInMaxGrowth":@"checkInMaxGrowth",
             @"checkInOneDayGrowth":@"checkInOneDayGrowth",
             @"completeUserInfoGrowth":@"completeUserInfoGrowth",
             @"rechargeTransferGrowthRate":@"rechargeTransferGrowthRate",
             @"isShowGrowthLevel" : @"isShowGrowthLevel"
             };
}



+ (NSValueTransformer *)completeUserInfoFieldsJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString * modelHref){
                return [modelHref componentsSeparatedByString:@","];
            } reverseBlock:^id(NSArray * list) {
                return [list componentsJoinedByString:@","];
            }];
}

@end
