//
//  MTLModel+Define.m
//  Bet365
//
//  Created by adnin on 2019/3/17.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "MTLModel+Define.h"
#import "MainAllocationModel.h"
#import <Aspects/Aspects.h>

@interface NSString()

@property (nonatomic,strong) NSNumber *kHeight;

@property (nonatomic,strong) NSNumber *kWidth;


@end

@implementation NSString (Scale)


-(NSNumber *)kHeight{
    return objc_getAssociatedObject(self, _cmd);
}

-(NSNumber *)kWidth{
    return objc_getAssociatedObject(self, _cmd);
}

-(void)setKHeight:(NSNumber *)kHeight{
    objc_setAssociatedObject(self, @selector(kHeight), kHeight, OBJC_ASSOCIATION_COPY);
}

-(void)setKWidth:(NSNumber *)kWidth{
    objc_setAssociatedObject(self, @selector(kWidth), kWidth, OBJC_ASSOCIATION_COPY);
}

-(CGFloat)width{
    if (!self.kWidth) {
        NSString * patton = @"_(\\d+)_(\\d+).";
        
        NSArray *array =    nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:patton options:NSRegularExpressionCaseInsensitive error:nil];
        array = [regex matchesInString:self options:0 range:NSMakeRange(0, [self length])];
        
        __block NSString *str1 = nil;
        [array enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(NSTextCheckingResult *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            for(int i=0;i<obj.numberOfRanges;i++){
                str1 = [self substringWithRange:[obj rangeAtIndex:i]];
                if (i == 1) {
                    self.kWidth = [NSNumber numberWithFloat:[str1 floatValue]];
                }
            }
            *stop = YES;
        }];
        
        if (!self.kWidth) {
            self.kWidth = @(0);
        }
    }
    return [self.kWidth floatValue];
}



-(CGFloat)height{
    if (!self.kHeight) {
        NSString * patton = @"_(\\d+)_(\\d+).";
        
        NSArray *array =    nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:patton options:NSRegularExpressionCaseInsensitive error:nil];
        array = [regex matchesInString:self options:0 range:NSMakeRange(0, [self length])];
        
        __block NSString *str1 = nil;
        [array enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(NSTextCheckingResult *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            for(int i=0;i<obj.numberOfRanges;i++){
                str1 = [self substringWithRange:[obj rangeAtIndex:i]];
                if (i == 2) {
                    self.kHeight = [NSNumber numberWithFloat:[str1 floatValue]];
                }
            }
            *stop = YES;
        }];
        
        if (!self.kHeight) {
            self.kHeight = @(0);
        }
    }
    return [self.kHeight floatValue];
}


@end

@implementation MTLModel (Define)
+(NSValueTransformer *)colorTransFormer{
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString *colorStr){
                if (!colorStr.length) {
                    return nil;
                }
                UIColor *color = COLOR_WITH_HEXSTRING(colorStr);
                return color;
            } reverseBlock:^id(UIColor *color) {
                return [color hexString];
            }];
}

+ (NSValueTransformer *)iconTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString *imagePath){
                
                NSMutableString *urlStr = imagePath.mutableCopy;
                if ([urlStr hasPrefix:@"/views/image/images"]) {
                    [urlStr deleteString:@"/views/image"];
                }
                if ([urlStr hasSuffix:@".png"] ||
                    [urlStr hasSuffix:@".jpg"] ||
                    [urlStr hasSuffix:@".jpeg"] ||
                    [urlStr hasSuffix:@".gif"]) {
                    return [NSString stringWithFormat:@"%@%@",SerVer_Url,urlStr];
                }
                return urlStr;
            } reverseBlock:^id(NSArray <MTLModel *>*list) {
                
                __block NSError *error;
                NSArray *items = [MTLJSONAdapter JSONArrayFromModels:list error:&error];
                return items;
            }];
}



+ (NSValueTransformer *)routerKeyTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString * modelHref){
                NSMutableString *mUrl = modelHref.mutableCopy;
                [mUrl deleteString:@"#"];
                if ([mUrl hasPrefix:@"/"]) {
                    [mUrl deleteCharactersInRange:NSMakeRange(0, 1)];
                }
                return mUrl;
            } reverseBlock:^id(NSString * modelHref) {
                return modelHref;
            }];
}

+ (NSValueTransformer *)templateAttrTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSDictionary * data){
                NSError *error;
                NSArray *arr = [MTLJSONAdapter modelOfClass:[MenuAttrTemplate class] fromJSONDictionary:data error:&error];
                return arr;
            } reverseBlock:^id(NSString * modelHref) {
                return modelHref;
            }];
}

+ (NSValueTransformer *)templateJumpTypeTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString * modelHref){
                if ([modelHref isEqualToString:@"0"]) {
                    return @(1);
                }else if ([modelHref isEqualToString:@"1"]){
                    return @(0);
                }
                return @(0);
            } reverseBlock:^id(NSString * modelHref) {
                return modelHref;
            }];
}

+ (NSValueTransformer *)timeJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString *timeStr){
                NSDate *date = [NSDate dateWithString:timeStr Format:@"yyyy-MM-dd HH:mm:ss"];
                return date;
            } reverseBlock:^id(NSDate *date) {
                NSString *time = [date getTimeFormatStringWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                return time;
            }];
}

#pragma mark - Private
NSString *removeFirstColorString(NSString *colorStr){
    NSMutableString *mStr = colorStr.mutableCopy;
    [mStr deleteString:@"#"];
    if (mStr.length == 3) {
        for (int i = 0; i<3; i++) {
            NSInteger idx = i+i;
            NSRange range = NSMakeRange(idx, 1);
            NSString *str = [mStr substringWithRange:range];
            [mStr insertString:str atIndex:idx];
        }
    }
    return mStr;
}
@end
