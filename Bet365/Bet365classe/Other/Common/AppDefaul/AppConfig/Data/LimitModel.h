//
//  LimitModel.h
//  Bet365
//
//  Created by super_小鸡 on 2019/8/15.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <Mantle/Mantle.h>

NS_ASSUME_NONNULL_BEGIN

@interface LimitModel : MTLModel<MTLJSONSerializing>

@property (nonatomic,strong)NSString *userPasswordType;
@property (nonatomic,strong)NSString *overMaxAllowedAccountTips;
@property (nonatomic,strong)NSString *errorPwdOverTimesMsg;
@property (nonatomic,strong)NSString *accountStateErrorMsg;
@property (nonatomic,strong)NSNumber *limitIpForMultipleAccount;
@property (nonatomic,strong)NSNumber *errorPwdLimit;

@property (nonatomic,assign)NSInteger ipLocationVerifyTurnOn;
@property (nonatomic,strong)NSString *ipLocationVerifyFields;
@property (nonatomic,assign)NSInteger ipLocationVerifyCount;

@property (nonatomic,assign)NSInteger errorPwdCount;
@property (nonatomic,assign)NSInteger vCode;
//@property (nonatomic,assign)NSInteger initPwdIfEmpty;
//@property (nonatomic,strong)NSNumber *initPwdVerifyCount;
@property (nonatomic,strong)NSNumber *maxAllowedAccountPerIp;
@property (nonatomic,strong)NSNumber * ipLocationCode;
@property (nonatomic,strong)NSNumber *onlyLoginPhone;
@property (nonatomic,strong)NSNumber *loginPhone;


@end

NS_ASSUME_NONNULL_END
