//
//  LaunchAd.h
//  Bet365
//
//  Created by adnin on 2019/7/28.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <Mantle/Mantle.h>

NS_ASSUME_NONNULL_BEGIN

@interface LaunchAd : MTLModel<MTLJSONSerializing>

@property (nonatomic,strong) NSString *href;

@property (nonatomic,strong) NSString *image;

@property (nonatomic,assign) NSInteger showTime;

@property (nonatomic,assign) BOOL type;

@end

NS_ASSUME_NONNULL_END
