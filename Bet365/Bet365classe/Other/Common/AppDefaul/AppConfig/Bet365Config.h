//
//  Bet365Config.h
//  Bet365
//
//  Created by HHH on 2018/7/15.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppSetting.h"
#import "RegisterConfig.h"
#import "CommonConfig.h"
#import "Bet365NoticeModel.h"
#import "MainAllocationModel.h"
#import "LotteryFactory.h"
#import "UserCenterTemplateModel.h"
#import "Bet365LiveLimitModel.h"
#import "BetTotal365ViewController.h"
#import "GrowthConfigModel.h"
#import "SportsModuleShare.h"
#import "LukeLiveRecordItem.h"
#import "SkinConfig.h"
#import "LimitModel.h"
#import "TestKindQuery.h"
#import "LaunchAd.h"
#import "SliderModel.h"

#define BET_CONFIG [Bet365Config shareInstance]

@interface Bet365Config : NSObject

@property (nonatomic,strong)SkinConfig *skin;

@property (nonatomic,strong)AppSetting *userSettingData;

@property (nonatomic,strong) CongfigClusterData *config;

@property (nonatomic,strong) RegisterConfig *registerConfig;

@property (nonatomic,strong) CommonConfig *common_config;

@property (nonatomic,strong) Bet365NoticeModel *noticeModel;

@property (nonatomic,strong) MainAllocationModel*allocation;

@property (nonatomic,strong) NSMutableArray <MenuTemplate *>*menuList;

@property (nonatomic,strong) GrowthConfigModel *growthConfigModel;

@property (nonatomic,strong) NSArray <GrowthLevelModel *>*growthLevelList;

@property (nonatomic,strong) UserCenterTemplateModel *userCenterMenu;

@property (nonatomic,strong) LimitModel *limit;

@property (nonatomic,strong) LaunchAd *launchAd;

@property (nonatomic,strong) NSArray <SliderModel *>*sliderList;


@property (nonatomic,strong)RACSubject *updateSubject;


/**充值提示备注颜色**/
@property (nonatomic,copy)NSString *remark_color;

/**版本号**/
@property (nonatomic,readonly,assign)NSInteger buildVersion;
/**极光Key**/
@property (nonatomic,readonly,copy)NSString *jPushKey;

@property (nonatomic,readonly,copy)NSString *appId;

@property (nonatomic,readonly,copy)NSString *appName;

@property (nonatomic,readonly,copy)NSString *customURL;

/**当前客服敬语**/
@property (nonatomic,readonly,copy)NSString *customServiceRead;

/**当前令牌码**/
@property (nonatomic,readonly,copy)NSString *boardCode;
/**当前推广码是否写死状态**/
@property (nonatomic,readonly,copy)NSString *PromotionCodeState;
/**当前推广码唯一code**/
@property (nonatomic,readonly,copy)NSString *PromotionCode;
/**当前游戏选用type(官方/信用)**/
@property (nonatomic,assign)currenGameType gameType;
/**
 配置信息是否更新
 */
@property (nonatomic,assign) BOOL configHasUpdate;


/**
 下注成功长龙信息发生变化
 */
@property (nonatomic,assign)BOOL successlongDragon;
/**
 
 /**
 当前游戏模式类型
 */
@property (nonatomic,assign)LotteryKind kind_type;
/**
 /**
 当前是否为特殊游戏类型
 */
@property (nonatomic,assign)BOOL Special;
/**

 /**
 当前游戏模式类型
 */
@property (nonatomic,copy)NSString *lottery_id;
/**
 /**
 当前广告储存
 */
@property (nonatomic,strong)NSMutableArray *adver_config;

 /**
 充值提示
 */
@property (nonatomic,copy)NSString *rechargeAlert;
/**
 是否进入了体育界面
 **/
@property (nonatomic,assign)BOOL enterSport;
/**
 导航栏图标
 */
@property (nonatomic,strong)UIImage *naviTitileImg;
///**
// 真人是否试玩存储
// */
//@property (nonatomic,strong)NSArray<Bet365LiveLimitModel *> *liveLimitTestArr;

+(instancetype)shareInstance;
/**
 请求配置信息

 @param completed 成功回调
 */
-(void)getConfigCompleted:(void(^)(BOOL success))completed;


/**
 获取启动页广告信息

 */
-(void)getLaunchAdMent:(void(^)(LaunchAd *launchAd))completed;

/** 公告栏数据 **/
- (NSURLSessionTask *)getNoticeCompleted:(void(^)(BOOL success))completed;

-(void)getGrowthConfig:(void(^)(BOOL success,NetReceiveObjType type))complete;

@end





