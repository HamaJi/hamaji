//
//  TextData.h
//  Bet365
//
//  Created by HHH on 2018/8/8.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "AppDefaults.h"

@interface TextData : AppDefaults
@property (nonatomic, assign) float a;

@property (nonatomic, assign) int b;

@property (nonatomic, assign) double c;

@property (nonatomic, assign) NSInteger d;

@property (nonatomic, copy) NSString * e;

@property (nonatomic, assign) CGFloat  f;

#pragma mark - Other

@property (nonatomic, copy) UserBankData *g;

@property (nonatomic, copy) NSNumber * h;

@property (nonatomic, copy) NSArray *i;

@end
