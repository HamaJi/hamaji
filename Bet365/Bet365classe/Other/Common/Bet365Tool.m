//
//  Bet365Tool.m
//  Bet365
//
//  Created by HHH on 2018/7/2.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Bet365Tool.h"
#import <ifaddrs.h>
#import <arpa/inet.h>
#import <net/if.h>
#import "sys/utsname.h"
#import <AdSupport/AdSupport.h>
#import <UserNotifications/UserNotifications.h>
#define IOS_CELLULAR    @"pdp_ip0"
#define IOS_WIFI        @"en0"
#define IOS_VPN         @"utun0"
#define IP_ADDR_IPv4    @"ipv4"
#define IP_ADDR_IPv6    @"ipv6"
#import "LunarSolarConverter.h"
#import "RequestUrlHeader.h"

@implementation Bet365Tool
static Bet365Tool *betTool = nil;

+(instancetype)shareInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        betTool = [[Bet365Tool alloc] init];
    });
    return betTool;
}

-(instancetype)init{
    if (self = [super init]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [Bet365Tool getNetworkIPAddressCompleted:^(BOOL success) {
//                UILabel *label = [UILabel setAllocLabelWithText:[NSString stringWithFormat:@"%li-%@",BET_CONFIG.buildVersion,self.ipStr] FontOfSize:5 rgbColor:0x000000];
//                label.textColor = [UIColor skinNaviTintColor];
//                [JesseAppdelegate.window addSubview:label];
//                [label mas_makeConstraints:^(MASConstraintMaker *make) {
//                    make.top.equalTo(JesseAppdelegate.window).offset(3);
//                    make.right.equalTo(JesseAppdelegate.window).offset(-3);
//                }];
            }];
            [Bet365Tool setThirdLb];
        });
    
    }
    return self;
}

+ (void)setThirdLb
{
    [SVProgressHUD setSuccessImage:[UIImage originalWithImage:@"success"]];
    [SVProgressHUD setErrorImage:[UIImage originalWithImage:@"error"]];
    [SVProgressHUD setMinimumSize:CGSizeMake(150, 150)];
    [SVProgressHUD setImageViewSize:CGSizeMake(50, 50)];
    [SVProgressHUD setCornerRadius:6];
    [SVProgressHUD setMaximumDismissTimeInterval:1.5];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setBackgroundColor:[UIColor colorWithWhite:0.2 alpha:0.8]];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
    
    IQKeyboardManager *keyboardManager = [IQKeyboardManager sharedManager];
    keyboardManager.enable = YES;
    keyboardManager.shouldResignOnTouchOutside = YES;
    keyboardManager.keyboardDistanceFromTextField = 20.0f;
    keyboardManager.toolbarDoneBarButtonItemText = @"完成";
}


+(void) checkCurrentNotificationStatusCompleted:(void(^)(BOOL allowd))completed{
    if (@available(iOS 10 , *)){
        [[UNUserNotificationCenter currentNotificationCenter] getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
            
            if (settings.authorizationStatus == UNAuthorizationStatusDenied){
                // 没权限
                completed(NO);
            }else{
                completed(YES);
            }
            
        }];
    }else if (@available(iOS 8 , *)){
        UIUserNotificationSettings * setting = [[UIApplication sharedApplication] currentUserNotificationSettings];
        
        if (setting.types == UIUserNotificationTypeNone) {
            // 没权限
            completed(NO);
        }else{
            completed(YES);
        }
    }else{
        UIRemoteNotificationType type = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        if (type == UIUserNotificationTypeNone){
            // 没权限
            completed(NO);
        }else{
            completed(YES);
        }
    }
}

/**
 获取设备当前本地IP地址

 @param preferIPv4 是否为ipv4
 @return 192.168.0.1
 */
+ (NSString *)getIPAddress:(BOOL)preferIPv4
{
    NSArray *searchArray = preferIPv4 ?
    @[ IOS_VPN @"/" IP_ADDR_IPv4, IOS_VPN @"/" IP_ADDR_IPv6, IOS_WIFI @"/" IP_ADDR_IPv4, IOS_WIFI @"/" IP_ADDR_IPv6, IOS_CELLULAR @"/" IP_ADDR_IPv4, IOS_CELLULAR @"/" IP_ADDR_IPv6 ] :
    @[ IOS_VPN @"/" IP_ADDR_IPv6, IOS_VPN @"/" IP_ADDR_IPv4, IOS_WIFI @"/" IP_ADDR_IPv6, IOS_WIFI @"/" IP_ADDR_IPv4, IOS_CELLULAR @"/" IP_ADDR_IPv6, IOS_CELLULAR @"/" IP_ADDR_IPv4 ] ;
    
    NSDictionary *addresses = [self getIPAddresses];
    NSLog(@"addresses: %@", addresses);
    
    __block NSString *address;
    [searchArray enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL *stop) {
        address = addresses[key];
        //筛选出IP地址格式
        if([self isValidatIP:address]) *stop = YES;
    } ];
    return address ? address : @"0.0.0.0";
}

+ (BOOL)isValidatIP:(NSString *)ipAddress {
    if (ipAddress.length == 0) {
        return NO;
    }
    NSString *urlRegEx = @"^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
    
    NSError *error;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:urlRegEx options:0 error:&error];
    
    if (regex != nil) {
        NSTextCheckingResult *firstMatch=[regex firstMatchInString:ipAddress options:0 range:NSMakeRange(0, [ipAddress length])];
        
        if (firstMatch) {
            NSRange resultRange = [firstMatch rangeAtIndex:0];
            NSString *result=[ipAddress substringWithRange:resultRange];
            //输出结果
            NSLog(@"%@",result);
            return YES;
        }
    }
    return NO;
}

+ (NSDictionary *)getIPAddresses
{
    NSMutableDictionary *addresses = [NSMutableDictionary dictionaryWithCapacity:8];
    
    // retrieve the current interfaces - returns 0 on success
    struct ifaddrs *interfaces;
    if(!getifaddrs(&interfaces)) {
        // Loop through linked list of interfaces
        struct ifaddrs *interface;
        for(interface=interfaces; interface; interface=interface->ifa_next) {
            if(!(interface->ifa_flags & IFF_UP) /* || (interface->ifa_flags & IFF_LOOPBACK) */ ) {
                continue; // deeply nested code harder to read
            }
            const struct sockaddr_in *addr = (const struct sockaddr_in*)interface->ifa_addr;
            char addrBuf[ MAX(INET_ADDRSTRLEN, INET6_ADDRSTRLEN) ];
            if(addr && (addr->sin_family==AF_INET || addr->sin_family==AF_INET6)) {
                NSString *name = [NSString stringWithUTF8String:interface->ifa_name];
                NSString *type;
                if(addr->sin_family == AF_INET) {
                    if(inet_ntop(AF_INET, &addr->sin_addr, addrBuf, INET_ADDRSTRLEN)) {
                        type = IP_ADDR_IPv4;
                    }
                } else {
                    const struct sockaddr_in6 *addr6 = (const struct sockaddr_in6*)interface->ifa_addr;
                    if(inet_ntop(AF_INET6, &addr6->sin6_addr, addrBuf, INET6_ADDRSTRLEN)) {
                        type = IP_ADDR_IPv6;
                    }
                }
                if(type) {
                    NSString *key = [NSString stringWithFormat:@"%@/%@", name, type];
                    addresses[key] = [NSString stringWithUTF8String:addrBuf];
                }
            }
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    return [addresses count] ? addresses : nil;
}



/**
 获取外网ip

 @param completed 192.168.0.1
 */
+ (void)getNetworkIPAddressCompleted:(void(^)(BOOL success))completed{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        __block NSString *ipStr = nil;
        //搜狐api var returnCitySN = {\"cip\": \"113.61.35.69\", \"cid\": \"PH\", \"cname\": \"PHILIPPINES\"};
        [NET_DATA_MANAGER GET:[NSString stringWithFormat:@"%@%@",SerVer_Url,IP_INFO] RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_HTTP parameters:nil responseCache:nil success:^(id responseObject) {
            ipStr = [responseObject objectForKey:@"ip"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [Bet365Tool shareInstance].ipStr = ipStr ? ipStr : @"0.0.0.0";
                completed ? completed([Bet365Tool shareInstance].ipStr.length > 0) : nil;
            });
//            @""
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [Bet365Tool shareInstance].ipStr = @"0.0.0.0";
                completed ? completed(NO) : nil;
            });
        }];
        
//        //淘宝
//        NSURL *ipURL = [NSURL URLWithString:@"http://ip.taobao.com/service/getIpInfo2.php?ip=myip"];
//        NSData *data = [NSData dataWithContentsOfURL:ipURL];
//        if (!data) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [Bet365Tool shareInstance].ipStr = @"0.0.0.0";
//                completed ? completed(NO) : nil;
//            });
//            return;
//        }
//        NSDictionary *ipDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
//
//        if (ipDic && [ipDic[@"code"] integerValue] == 0) {
//            ipStr = ipDic[@"data"][@"ip"];
//        }
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [Bet365Tool shareInstance].ipStr = ipStr ? ipStr : @"0.0.0.0";
//            completed ? completed([Bet365Tool shareInstance].ipStr.length > 0) : nil;
//        });
    });
}


/**
 获取设备型号

 @return iPhone 7
 */
+ (NSString *)getDeviceName{
    // 需要
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSASCIIStringEncoding];
    
    if ([platform isEqualToString:@"iPhone1,1"]) return @"iPhone 2G";
    
    if ([platform isEqualToString:@"iPhone1,2"]) return @"iPhone 3G";
    
    if ([platform isEqualToString:@"iPhone2,1"]) return @"iPhone 3GS";
    
    if ([platform isEqualToString:@"iPhone3,1"]) return @"iPhone 4";
    
    if ([platform isEqualToString:@"iPhone3,2"]) return @"iPhone 4";
    
    if ([platform isEqualToString:@"iPhone3,3"]) return @"iPhone 4";
    
    if ([platform isEqualToString:@"iPhone4,1"]) return @"iPhone 4S";
    
    if ([platform isEqualToString:@"iPhone5,1"]) return @"iPhone 5";
    
    if ([platform isEqualToString:@"iPhone5,2"]) return @"iPhone 5";
    
    if ([platform isEqualToString:@"iPhone5,3"]) return @"iPhone 5c";
    
    if ([platform isEqualToString:@"iPhone5,4"]) return @"iPhone 5c";
    
    if ([platform isEqualToString:@"iPhone6,1"]) return @"iPhone 5s";
    
    if ([platform isEqualToString:@"iPhone6,2"]) return @"iPhone 5s";
    
    if ([platform isEqualToString:@"iPhone7,1"]) return @"iPhone 6 Plus";
    
    if ([platform isEqualToString:@"iPhone7,2"]) return @"iPhone 6";
    
    if ([platform isEqualToString:@"iPhone8,1"]) return @"iPhone 6s";
    
    if ([platform isEqualToString:@"iPhone8,2"]) return @"iPhone 6s Plus";
    
    if ([platform isEqualToString:@"iPhone8,4"]) return @"iPhone SE";
    
    if ([platform isEqualToString:@"iPhone9,1"]) return @"iPhone 7";//国行、日版、港行
    
    if ([platform isEqualToString:@"iPhone9,2"]) return @"iPhone 7 Plus";//港行、国行
    if ([platform isEqualToString:@"iPhone9,3"])    return @"iPhone 7";//美版、台版
    if ([platform isEqualToString:@"iPhone9,4"])    return @"iPhone 7 Plus";//美版、台版
    
    if ([platform isEqualToString:@"iPhone10,1"])   return @"iPhone 8";//国行(A1863)、日行(A1906)
    
    if ([platform isEqualToString:@"iPhone10,4"])   return @"iPhone 8";//美版(Global/A1905)
    
    if ([platform isEqualToString:@"iPhone10,2"])   return @"iPhone 8 Plus";//国行(A1864)、日行(A1898)
    
    if ([platform isEqualToString:@"iPhone10,5"])   return @"iPhone 8 Plus";//美版(Global/A1897)
    
    if ([platform isEqualToString:@"iPhone10,3"])   return @"iPhone X";//国行(A1865)、日行(A1902)
    
    if ([platform isEqualToString:@"iPhone10,6"])   return @"iPhone X";//美版(Global/A1901)
    
    
    if ([platform isEqualToString:@"iPod1,1"])   return @"iPod Touch 1G";
    
    if ([platform isEqualToString:@"iPod2,1"])   return @"iPod Touch 2G";
    
    if ([platform isEqualToString:@"iPod3,1"])   return @"iPod Touch 3G";
    
    if ([platform isEqualToString:@"iPod4,1"])   return @"iPod Touch 4G";
    
    if ([platform isEqualToString:@"iPod5,1"])   return @"iPod Touch 5G";
    
    if ([platform isEqualToString:@"iPad1,1"])   return @"iPad 1G";
    
    if ([platform isEqualToString:@"iPad2,1"])   return @"iPad 2";
    
    if ([platform isEqualToString:@"iPad2,2"])   return @"iPad 2";
    
    if ([platform isEqualToString:@"iPad2,3"])   return @"iPad 2";
    
    if ([platform isEqualToString:@"iPad2,4"])   return @"iPad 2";
    
    if ([platform isEqualToString:@"iPad2,5"])   return @"iPad Mini 1G";
    
    if ([platform isEqualToString:@"iPad2,6"])   return @"iPad Mini 1G";
    
    if ([platform isEqualToString:@"iPad2,7"])   return @"iPad Mini 1G";
    
    if ([platform isEqualToString:@"iPad3,1"])   return @"iPad 3";
    
    if ([platform isEqualToString:@"iPad3,2"])   return @"iPad 3";
    
    if ([platform isEqualToString:@"iPad3,3"])   return @"iPad 3";
    
    if ([platform isEqualToString:@"iPad3,4"])   return @"iPad 4";
    
    if ([platform isEqualToString:@"iPad3,5"])   return @"iPad 4";
    
    if ([platform isEqualToString:@"iPad3,6"])   return @"iPad 4";
    
    if ([platform isEqualToString:@"iPad4,1"])   return @"iPad Air";
    
    if ([platform isEqualToString:@"iPad4,2"])   return @"iPad Air";
    
    if ([platform isEqualToString:@"iPad4,3"])   return @"iPad Air";
    
    if ([platform isEqualToString:@"iPad4,4"])   return @"iPad Mini 2G";
    
    if ([platform isEqualToString:@"iPad4,5"])   return @"iPad Mini 2G";
    
    if ([platform isEqualToString:@"iPad4,6"])   return @"iPad Mini 2G";
    
    if ([platform isEqualToString:@"i386"])      return @"iPhone Simulator";
    
    if ([platform isEqualToString:@"x86_64"])    return @"iPhone Simulator";
    
    return platform;
}

+(NSString *)netUserAgent{
    
    
    NSString *systemVersion = [UIDevice currentDevice].systemVersion;
//    NSLog(@"当前系统版本号-->%@", systemVersion);
    
    NSString *systemName = [UIDevice currentDevice].systemName;
//    NSLog(@"当前系统名称-->%@", systemName);
    
//    NSString * bundleVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
//    NSLog(@"当前外部版本号-->%@", bundleVersion);
    
    NSString * buildVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
//    NSLog(@"当前构建版本号-->%@", buildVersion);
    NSString *uuid;
    if ([[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled]) {
        uuid = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
//        NSLog(@"广告位标识符idfa-->%@", uuid);
    }else{
        uuid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
//        NSLog(@"唯一识别码uuid-->%@", uuid);
    }
    
    

    NSString *deviceName = [self getDeviceName];
//    NSLog(@"设备型号-->%@", deviceName);
    
    NSString *str = [NSString stringWithFormat:@"(%@)(%@:%@)(%@)(%@)(%@)",
                     [NSString stringWithFormat:@"NATIVE_ios_v.%li",BET_CONFIG.buildVersion],
                     systemName,
                     systemVersion,
                     buildVersion,
                     uuid,
                     deviceName];
//    str =  [str stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    return str;
}
//初始化soalr
+ (Solar *)initSolar:(NSDate *)date{
    Solar *solar  = [[Solar alloc] init];
    solar.solarYear = date.year;
    solar.solarMonth = date.month;
    solar.solarDay = date.day;
    return solar;
}
//Initialize soalr with date string
+ (Solar *)initSolarWithDateString:(NSString *)dateString{
    NSString * format = @"yyyy-MM-dd HH:mm:ss";
    NSTimeZone * timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT+0800"];
    
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setTimeZone:timeZone];
    [inputFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"]];
    [inputFormatter setDateFormat:format];
    NSDate *date = [inputFormatter dateFromString:dateString];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setTimeZone: timeZone];
    NSDateComponents * components = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date];
    
    Solar *solar = [[Solar alloc] init];
    solar.solarYear = components.year;
    solar.solarMonth = (int)components.month;
    solar.solarDay = (int)components.day;
    
    return solar;
}
//获取生肖组合
+(NSString *)getZodNameForLHCByOpenTimeSolar:(Solar *)openTimeSolar Num:(NSInteger)num{
    NSArray *zodicArr = @[@"鼠",@"牛",@"虎",@"兔",@"龙",@"蛇",@"马",@"羊",@"猴",@"鸡",@"狗",@"猪"];
    Lunar *lunar = [LunarSolarConverter solarToLunar:openTimeSolar];
    int nongli = lunar.lunarYear;
    int refrence = 1924;
    int index = (nongli - refrence) % 12;
    
    NSArray *firZod = [zodicArr subarrayWithRange:NSMakeRange(0, index + 1)];
    NSArray *secZod = [zodicArr subarrayWithRange:NSMakeRange(index + 1, zodicArr.count - index - 1)];

    firZod = [[firZod reverseObjectEnumerator] allObjects];
    secZod = [[secZod reverseObjectEnumerator] allObjects];
    
    NSMutableArray *mZodicArr = @[].mutableCopy;
    [mZodicArr addObjectsFromArray:firZod];
    [mZodicArr addObjectsFromArray:secZod];
    
    NSString *zodName = [mZodicArr safeObjectAtIndex:((num - 1) % 12)];
    return zodName ? zodName : @"-";

}

+ (UIImage *)lhcBackColorNumber:(NSInteger)num
{
    if (num == 1 || num == 2 || num == 7 || num == 8 || num == 12 || num == 13 || num == 18 || num == 19 || num == 23 || num == 24 || num == 29 || num == 30 || num == 34 || num == 35 || num == 40 || num == 45 || num == 46) {
        return [UIImage imageNamed:@"lhc_red"];
    }else if (num == 3 || num == 4 || num == 9 || num == 10 || num == 14 || num == 15 || num == 20 || num == 25 || num == 26 || num == 31 || num == 36 || num == 37 || num == 41 || num == 42 || num == 47 || num == 48) {
        return [UIImage imageNamed:@"lhc_blue"];
    }else{
        return [UIImage imageNamed:@"lhc_green"];
    }
}

+(void)removeAllCacheOnCompletion:(void(^)())completion{
    [PPNetworkCache removeAllHttpCache];
    [AppDefaults removeAllDefaults];
    [[SDImageCache sharedImageCache] clearDiskOnCompletion:completion];
    [[SDImageCache sharedImageCache] clearMemory];

}

void aji_swizzleClassMethod(Class theClass, SEL originalSelector, SEL swizzledSelector){

    Method origMethod = class_getClassMethod(theClass, originalSelector);
    Method newMethod = class_getClassMethod(theClass, swizzledSelector);
    
    theClass = object_getClass((id)theClass);
    
    if(class_addMethod(theClass, originalSelector, method_getImplementation(newMethod), method_getTypeEncoding(newMethod)))
        class_replaceMethod(theClass, swizzledSelector, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
    else
        method_exchangeImplementations(origMethod, newMethod);
}

void aji_swizzleInstanceMethod(id instance, SEL originalSelector, SEL swizzledSelector){
    
    Class class = [instance class];

    Method originalMethod = class_getInstanceMethod(class, originalSelector);
    Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
    BOOL didAddMethod =
    class_addMethod(class,
                    originalSelector,
                    method_getImplementation(swizzledMethod),
                    method_getTypeEncoding(swizzledMethod));
    if (didAddMethod) {
        class_replaceMethod(class,
                            swizzledSelector,
                            method_getImplementation(originalMethod),
                            method_getTypeEncoding(originalMethod));
    } else {
        method_exchangeImplementations(originalMethod, swizzledMethod);
    }
}


NSString *plistPath (NSString *headerStr){
    return [[NSBundle mainBundle] pathForResource:headerStr ofType:@"plist"];
}
@end
