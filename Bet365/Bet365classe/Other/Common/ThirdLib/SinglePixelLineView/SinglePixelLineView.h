// SinglePixelLineView.h

#import <UIKit/UIKit.h>


IB_DESIGNABLE
@interface SinglePixelLineView : UIView

/** 画上边线, 默认NO */
@property(nonatomic) IBInspectable BOOL drawTopEdge;

/** 画左边线, 默认NO */
@property(nonatomic) IBInspectable BOOL drawLeftEdge;

/** 画下边线, 默认NO */
@property(nonatomic) IBInspectable BOOL drawBottomEdge;

/** 画右边线, 默认NO */
@property(nonatomic) IBInspectable BOOL drawRightEdge;

/** 分割线颜色, 默认黑色 */
@property(nonatomic) IBInspectable UIColor *separatorColor;

@end

