// SinglePixelLineView.m

#import "SinglePixelLineView.h"


@implementation SinglePixelLineView

#pragma mark - 初始化

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self singlePixelLineViewInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self singlePixelLineViewInit];
    }
    return self;
}

- (void)singlePixelLineViewInit
{
    self.backgroundColor = [UIColor clearColor];
    
    /* 默认值 */
    _separatorColor = [UIColor blackColor];
}

#pragma mark - 属性

- (void)setDrawTopEdge   :(BOOL)draw { _drawTopEdge    = draw; [self setNeedsDisplay]; }
- (void)setDrawLeftEdge  :(BOOL)draw { _drawLeftEdge   = draw; [self setNeedsDisplay]; }
- (void)setDrawBottomEdge:(BOOL)draw { _drawBottomEdge = draw; [self setNeedsDisplay]; }
- (void)setDrawRightEdge :(BOOL)draw { _drawRightEdge  = draw; [self setNeedsDisplay]; }

- (void)setSeparatorColor:(UIColor *)separatorColor
{
    _separatorColor = separatorColor;
    [self setNeedsDisplay];
}

#pragma mark - 绘画

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, _separatorColor.CGColor);
    
//#if TARGET_INTERFACE_BUILDER
    CGFloat lineWidth = 1;
//#else
//    CGFloat lineWidth = 1.0 / [UIScreen mainScreen].scale;
//#endif
    
    CGFloat W = self.frame.size.width ;
    CGFloat H = self.frame.size.height;

    if (_drawTopEdge   ) { CGContextFillRect(context, CGRectMake(0            , 0            , W        , lineWidth)); }
    if (_drawLeftEdge  ) { CGContextFillRect(context, CGRectMake(0            , 0            , lineWidth, H        )); }
    if (_drawBottomEdge) { CGContextFillRect(context, CGRectMake(0            , H - lineWidth, W        , lineWidth)); }
    if (_drawRightEdge ) { CGContextFillRect(context, CGRectMake(W - lineWidth, 0            , lineWidth, H        )); }
}

@end

