//
//  HCGDatePickerAppearance.m
//  HcgDatePicker-master
//
//  Created by 黄成钢 on 14/12/2016.
//  Copyright © 2016 chedaoshanqian. All rights reserved.
//

#import "HCGDatePickerAppearance.h"

#define AppScreenWidth [UIScreen mainScreen].bounds.size.width
#define AppScreenHeight [UIScreen mainScreen].bounds.size.height
#define DATE_PICKER_HEIGHT 216.0f
#define TOOLVIEW_HEIGHT 40.0f
#define BACK_HEIGHT TOOLVIEW_HEIGHT + DATE_PICKER_HEIGHT

typedef void(^dateBlock)(NSDate *);

@interface HCGDatePickerAppearance ()

@property (assign, nonatomic) BOOL isBirth;

@property (nonatomic, strong) UIView *backView;

@property (nonatomic, assign) DatePickerMode dataPickerMode;

@property (nonatomic, copy) dateBlock dateBlock;

@property (nonatomic, strong) HCGDatePicker *datePicker;
/** 最小时间 */
@property (nonatomic,assign) int min;

@end

@implementation HCGDatePickerAppearance


- (instancetype)initWithDatePickerMode:(DatePickerMode)dataPickerMode isBirth:(BOOL)isBirth completeBlock:(void (^)(NSDate *date))completeBlock {
    self = [super init];
    if (self) {
        _dataPickerMode = dataPickerMode;
        _isBirth = isBirth;
        [self setupUI];
        self.frame = CGRectMake(0, 0, AppScreenWidth, AppScreenHeight);
        _dateBlock = completeBlock;
    }
    return self;
}


- (instancetype)initWithDatePickerMode:(DatePickerMode)dataPickerMode completeBlock:(void (^)(NSDate *date))completeBlock {
    self = [super init];
    if (self) {
        _dataPickerMode = dataPickerMode;
        _isBirth = NO;
        [self setupUI];
        self.frame = CGRectMake(0, 0, AppScreenWidth, AppScreenHeight);
        _dateBlock = completeBlock;
    }
    return self;
}

- (instancetype)initWithDatePickerMode:(DatePickerMode)dataPickerMode MinDate:(int)min completeBlock:(void (^)(NSDate *))completeBlock
{
    if (self = [super init]) {
        _min = min;
        _dataPickerMode = dataPickerMode;
        [self setupUI];
        self.frame = CGRectMake(0, 0, AppScreenWidth, AppScreenHeight);
        _dateBlock = completeBlock;
    }
    return self;
}

- (void)setupUI {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hide)];
    [self addGestureRecognizer:tap];
    _backView = [[UIView alloc]initWithFrame:CGRectMake(0, AppScreenHeight, AppScreenWidth, BACK_HEIGHT)];
    _backView.backgroundColor = [UIColor whiteColor];
    
    NSDate *date = [NSDate date];
    NSTimeInterval oneDay = 24 * 60 * 60;
    NSDate *minDate = nil;
    if (_min != 0) {
        minDate = [date initWithTimeIntervalSinceNow:-_min * oneDay];
    }
    _datePicker = [[HCGDatePicker alloc]initWithDatePickerMode:_dataPickerMode MinDate:minDate MaxDate:date];
    _datePicker.frame = CGRectMake(0, TOOLVIEW_HEIGHT, AppScreenWidth, DATE_PICKER_HEIGHT);
    [_datePicker selectDate:date];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(AppScreenWidth - 80, 8, 80, 40)];
    [btn setTitle:@"确定" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithRed:15/255.0f green:136/255.0f blue:235/255.0f alpha:1] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(done) forControlEvents:UIControlEventTouchUpInside];
    [self.backView addSubview:_datePicker];
    [self.backView addSubview:btn];
    [self addSubview:self.backView];
}

- (void)didSelectDateWithDate:(NSDate *)date {
     [_datePicker selectDate:date];
}


- (void)done {
    [self hide];
    if (_dateBlock) {
        _dateBlock(_datePicker.date);
    }

}

- (void)show {
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    [UIView animateWithDuration:0.25 animations:^{
        _backView.frame = CGRectMake(0, AppScreenHeight - (BACK_HEIGHT), AppScreenWidth, BACK_HEIGHT);
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    }];
}

-(void)hide {
    [UIView animateWithDuration:0.2 animations:^{
        _backView.frame = CGRectMake(0, AppScreenHeight, AppScreenWidth, BACK_HEIGHT);
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];

}

@end
