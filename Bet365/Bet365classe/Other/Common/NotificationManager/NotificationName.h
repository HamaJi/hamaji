//
//  NotificationName.h
//  MACProject
//
//  Created by Deep river on 2017/4/19.
//  Copyright © 2017年 com.mackun. All rights reserved.
//

#ifndef NotificationName_h
#define NotificationName_h

#define NOTIF_HOME_TEXT         @"NOTIF_HOME_TEXT"//测试通知

#define NOTIF_USERINFO_REFRESH  @"inforReload"//用户数据刷新成功

#define NOTIF_USER_LOGOUT       @"NOTIF_USER_LOGOUT"//注销

#define LOTTERY_NOTIFA          @"LOTTERY_NOTIFA"

#endif /* NotificationName_h */

