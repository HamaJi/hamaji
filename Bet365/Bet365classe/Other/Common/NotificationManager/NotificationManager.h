//
//  NotificationManager.h
//  MACProject
//
//  Created by Deep river on 2017/4/19.
//  Copyright © 2017年 com.mackun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NotificationName.h"
#define NOTI_MANAGER [NotificationManager sharedInstance]



@interface NotificationManager : NSObject

+ (NotificationManager *)sharedInstance;
+ (void)destroyInstance;
/**
 接收通知
 
 @param observer 接收对象
 @param notificationName 通知名
 @param object 通知参数
 @param nextBlock 回调
 */
-(void)addObserver:(id)observer NotificationName:(NSString*)notificationName object:(id)object subscribeNext:(void (^)(id x))nextBlock;

/**
 发送通知
 
 @param notificationName 通知名
 @param object 通知参数
 */
- (void)postNotificationWithName:(NSString *)notificationName Object:(id)object;

@end
