//
//  NotificationManager.m
//  MACProject
//
//  Created by Deep river on 2017/4/19.
//  Copyright © 2017年 com.mackun. All rights reserved.
//

#import "NotificationManager.h"

static NotificationManager *instance = nil;

@implementation NotificationManager
{
   
}
+ (NotificationManager *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[super alloc] init];
    });
    return instance;
}

+ (void)destroyInstance
{
    instance = nil;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}


/**
 接收通知

 @param observer 接收对象
 @param notificationName 通知名
 @param object 通知参数
 @param nextBlock 回调
 */
-(void)addObserver:(id)observer NotificationName:(NSString*)notificationName object:(id)object subscribeNext:(void (^)(id x))nextBlock{
    NSArray *callList = [NSThread callStackSymbols];
    for (NSString *name in callList) {
        NSLog(@"%@",name);
    }
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:notificationName object:object] takeUntil:[observer rac_willDeallocSignal]] subscribeNext:^(NSNotification * _Nullable x) {
        nextBlock ? nextBlock(x.object) : nil ;
    }];
}
/**
 发送通知

 @param notificationName 通知名
 @param object 通知参数
 */
- (void)postNotificationWithName:(NSString *)notificationName Object:(id)object {
    [[NSNotificationCenter defaultCenter]  postNotificationName:notificationName object:object];
}


@end
