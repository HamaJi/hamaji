//
//  UIDevice+Rotating.m
//  Bet365
//
//  Created by jesse on 2017/10/2.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "UIDevice+Rotating.h"

@implementation UIDevice (Rotating)
+(void)setOrirntation:(UIInterfaceOrientation)orientation{
    SEL selector = NSSelectorFromString(@"setOrientation:");
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[self instanceMethodSignatureForSelector:selector]];
    [invocation setSelector:selector];
    [invocation setTarget:[self currentDevice]];
    int val = orientation;
    [invocation setArgument:&val atIndex:2];
    [invocation invoke];
}
@end
