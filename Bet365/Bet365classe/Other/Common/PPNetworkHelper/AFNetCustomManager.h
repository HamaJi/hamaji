//
//  AFNetCustomManager.h
//  Bet365
//
//  Created by luke on 2017/12/2.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface AFNetCustomManager : AFHTTPSessionManager

+ (instancetype)sharedClient;

@end
