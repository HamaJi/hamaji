//
//  AFNetCustomManager.m
//  Bet365
//
//  Created by luke on 2017/12/2.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "AFNetCustomManager.h"

@implementation AFNetCustomManager

+ (instancetype)sharedClient
{
    static AFNetCustomManager *_netManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _netManager = [[AFNetCustomManager alloc] initWithBaseURL:[NSURL URLWithString:SerVer_Url]];
    });
    return _netManager;
}

@end
