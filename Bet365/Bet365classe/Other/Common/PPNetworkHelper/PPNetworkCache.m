//
//  PPNetworkCache.m
//  NetWorkMannager
//
//  Created by Deep river on 16/8/12.
//  Copyright © 2016年 com.mackun. All rights reserved.
//

#import "PPNetworkCache.h"
#import <YYKit/YYKit.h>
//#import <YYKit/YYCache.h>
@implementation PPNetworkCache

static NSString *const NetworkResponseCache = @"NetworkResponseCache";

static YYCache *_dataCache;


+ (void)initialize{
    _dataCache = [YYCache cacheWithName:NetworkResponseCache];

}

+ (void)saveHttpCache:(id)httpCache forKey:(NSString *)key{
    if (!httpCache || [httpCache isEqual:[NSNull null]]) {
        return;
    }
    //异步缓存,不会阻塞主线程
    if (!httpCache || !key) {
        return;
    }
    [_dataCache setObject:httpCache forKey:key withBlock:^{
        
    }];
}

+ (id)getHttpCacheForKey:(NSString *)key{
    id obj = [_dataCache objectForKey:key];

    return netResponseElectrometer(obj);
}

+ (NSInteger)getAllHttpCacheSize{
    return [_dataCache.diskCache totalCost];
}

+(void)removeHttpCacheForKey:(NSString *)key{
    [_dataCache.diskCache removeObjectForKey:key];
}

+ (void)removeAllHttpCache{
    [_dataCache.diskCache removeAllObjects];
}

@end

