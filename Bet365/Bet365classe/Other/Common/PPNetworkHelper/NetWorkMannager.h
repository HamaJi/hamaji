//
//  NetWorkMannager.h
//  NetWorkMannager
//
//  Created by Deep river on 16/8/12.
//  Copyright © 2016年 com.mackun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PPNetworkCache.h"
#import "NSErrorHeader.h"

#define SerVer_Url ([Bet365Config shareInstance].userSettingData.serverUrl)

#define NET_DATA_MANAGER [NetWorkMannager shareManager]

typedef NS_ENUM(NSUInteger, PPNetworkStatus) {
    /** 未知网络*/
    PPNetworkStatusUnknown = 0,
    /** 无网络*/
    PPNetworkStatusNotReachable,
    /** 手机网络*/
    PPNetworkStatusReachableViaWWAN,
    /** WIFI网络*/
    PPNetworkStatusReachableViaWiFi
};

typedef NS_ENUM(NSUInteger, PPSerializerType) {
    /** HTTP*/
    PPSerializerType_HTTP,
    /** JSON*/
    PPSerializerType_JSON,
};

typedef NS_ENUM(NSUInteger, NetReceiveObjType) {
    /** fail**/
    NetReceiveObjType_FAIL,
    /** cache**/
    NetReceiveObjType_CACHE,
    /** response*/
    NetReceiveObjType_RESOPONSE,
};

/** 请求成功的Block */
typedef void(^HttpRequestSuccess)(id responseObject);

/** 请求失败的Block */
typedef void(^HttpRequestFailed)(NSError *error);

/** 缓存的Block */
typedef void(^HttpRequestCache)(id responseCache);

/** 上传或者下载的进度, Progress.completedUnitCount:当前大小 - Progress.totalUnitCount:总大小*/
typedef void (^HttpProgress)(NSProgress *progress);

/** 网络状态的Block*/
typedef void(^NetworkStatusBlock)(PPNetworkStatus status);



@interface NetWorkMannager : NSObject



+(instancetype)shareManager;

#pragma mark - 设置AFHTTPSessionManager相关属性

- (AFHTTPSessionManager *)createAFHTTPSessionManagerByRequestType:(PPSerializerType)requestType
                                                     ResponseType:(PPSerializerType)responseType;

-(void)setServerUrl:(nonnull NSString *)serverUrl;
/**
 *  通过Block回调实时获取网络状态
 */
- (void)checkNetworkStatusWithBlock:(NetworkStatusBlock)statusBlock;

- (BOOL)currentNetworkStatus;

@property(nonatomic,assign,readonly)PPNetworkStatus status;


/**
 GET

 @param URL 地址
 @param parameters 参数
 @param responseCache 缓存
 @param success 成功
 @param failure 失败
 @return Task任务
 */
- (__kindof NSURLSessionTask *)GET:(NSString *)URL
                       RequestType:(PPSerializerType)requestType
                      ResponseType:(PPSerializerType)responseType
               parameters:(NSDictionary *)parameters
            responseCache:(HttpRequestCache)responseCache
                  success:(HttpRequestSuccess)success
                  failure:(HttpRequestFailed)failure;

/**
 POST
 
 @param URL 地址
 @param parameters 参数
 @param responseCache 缓存
 @param success 成功
 @param failure 失败
 @return Task任务
 */
- (__kindof NSURLSessionTask *)POST:(NSString *)URL
               RequestType:(PPSerializerType)requestType
              ResponseType:(PPSerializerType)responseType
                parameters:(NSDictionary *)parameters
             responseCache:(HttpRequestCache)responseCache
                   success:(HttpRequestSuccess)success
                   failure:(HttpRequestFailed)failure;

/**
 *  上传图片文件
 *
 *  @param images     图片数组
 *  @param name       文件对应服务器上的字段
 *  @param fileName   文件名
 *  @param progress   上传进度信息
 *  @param success    请求成功的回调
 *  @param failure    请求失败的回调
 *
 *  @return 返回的对象可取消请求,调用cancel方法
 */

- (__kindof NSURLSessionTask *)upload:(NSString *)url
                      ImageData:(NSData *)imageData
                       Width:(NSInteger)w
                      height:(NSInteger)h
                    Paramers:(NSDictionary *)paraamers
                  folderName:(NSString*)fd
                    fileName:(NSString *)fileName
                    progress:(HttpProgress)progress
                     success:(HttpRequestSuccess)success
                     failure:(HttpRequestFailed)failure;

- (NSURLSessionTask *)uploadServerUrl:(NSString *)serverUrl
             APIurl:(NSString *)url
    ImageData:(NSData *)imageData
     Width:(NSInteger)w
    height:(NSInteger)h
  Paramers:(NSDictionary *)paraamers
folderName:(NSString*)fd
  fileName:(NSString *)fileName
  progress:(HttpProgress)progress
   success:(HttpRequestSuccess)success
                              failure:(HttpRequestFailed)failure;
/**
 *  下载文件
 *
 *  @param URL      请求地址
 *  @param fileDir  文件存储目录(默认存储目录为Download)
 *  @param progress 文件下载的进度信息
 *  @param success  下载成功的回调(回调参数filePath:文件的路径)
 *  @param failure  下载失败的回调
 *
 *  @return 返回NSURLSessionDownloadTask实例，可用于暂停继续，暂停调用suspend方法，开始下载调用resume方法
 */
- (__kindof NSURLSessionTask *)downloadWithURL:(NSString *)URL
                                       fileDir:(NSString *)fileDir
                                      progress:(HttpProgress)progress
                                       success:(void(^)(NSString *filePath))success
                                       failure:(HttpRequestFailed)failure;

-(void)addCookieToServerUrl;

-(void)addCookieToURL:(NSURL *)url key:(NSString *)key value:(NSString *)value;

id netResponseElectrometer();

@end




