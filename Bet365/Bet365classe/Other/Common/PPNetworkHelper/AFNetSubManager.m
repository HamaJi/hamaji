//
//  AFNetSubManager.m
//  Bet365
//
//  Created by jesse on 2017/12/23.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "AFNetSubManager.h"

@implementation AFNetSubManager
+ (instancetype)sharedClient
{
    static AFNetSubManager *Manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Manager = [[AFNetSubManager alloc] initWithBaseURL:[NSURL URLWithString:SerVer_Url]];
        
    });
    return Manager;
}
@end
