//
//  AFNetSubManager.h
//  Bet365
//
//  Created by jesse on 2017/12/23.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AFNetSubManager : AFHTTPSessionManager
+ (instancetype)sharedClient;
@end
