//
//  RequestUrlHeader.h
//  Bet365
//
//  Created by HHH on 2018/7/5.
//  Copyright © 2018年 jesse. All rights reserved.
//

#ifndef RequestUrlHeader_h
#define RequestUrlHeader_h

/** Main **/
#define MAIN_FLEX_URL           @"/views/index_flex.json"
#define GAMELOBBY3_URL           @"/views/appGamesLobby3.json"


/** Home **/
#define LunBoImageUrl           @"/views/wap_index_slider.html"//轮播MNS
#define TheWinNingList          @"/v/lottery/getNoticeWinMessage"//中奖榜所有数据
#define LOTTERY_SHAREBET        @"/api/bet/shareBet" //彩票聊天室分享
#define readHomeLogo            @"/views/game_center_logo.html"//应用导航logo
#define TheGameOpenTimeList     @"/v/lottery/allOpenInfo"//当前所有游戏json数据
#define APP_HENGFU_URL          @"/views/app_hengfu.json"//横幅
#define TheGameJsonList         @"/data/json/games.json"//获取当前游戏数据列表
#define TheGameCategoryJson     @"/data/json/cate.json"//获取游戏类别数据
#define TheAnnouncementUrl      @"/data/json/notice_list.json"//公告数据
#define TheSixOpenUrl           @"/data/json/lhc_sx.json"//六合彩配置
#define RetailtyUrl             @"/api/live/play" //真人接口获取返回
#define TestPlayGameUrl         @"/v/user/regTest"//获取试玩账户
#define REGISTER_TESTUSER_URL   @"/v/user/registerTrailUser"
#define ActivityhtmlUrl         @"/wap/yhhd/preferential.html"//优惠活动html
#define RETAILSTWITCHURL        @"/views/mobile_include_head.html"//当前真人体育控制
#define sport_ele_liveUrl       @"/views/sport_ele_live.html"
#define system_config           @"/data/json/web_system_config.json"//配置信息
#define adverLink_config        @"/views/new_alert.json"//广告配置
#define GameDataRequestUrl(r)   [NSString stringWithFormat:@"/data/json/%@.json",r]
#define HOME_REDPAGE_WINLIST_URL @"/api/activity/queryUnrealWelfareDetailList"
#define HOME_REDPAGE_INFO        @"/api/activity/getRedEnvelopeType"
#define HOME_OPEN_REDPAGE        @"/api/activity/getRedEnvelope"
#define HOME_REDPAGE_NOTICE      @"/views/app_RedPackageNotice.json"
#define HOME_REDPAGE_ENVELOPE    @"/api/activity/getRedEnvelopeInfoNew"
#define HOME_USERONLINE_USERL    @"/v/user/onlineCount"
#define BASE_MSG_URL             @"/views/baseMsg.json"  //红包规则
#define REDPAGE_DRAW_URL         @"/api/activity/drawRedEnvelope"  //拆红包
#define REDPAGE_WEEKEND_URL      @"/api/activity/getWeekendRedEnvelope"  //领红包
#define REDPAGE_WEEK_URL         @"/api/activity/queryWeekActivityQualification"  //周末红包
#define OPEN_TIME_URL            @"/data/json/official/config_open_time.json" 
#define APP_DP_URL               @"/views/app_dp.json"

#define TURNTABLE_PRIZE          @"/api/activity/getPrize"
#define TURNTABLE_WELFAREDATAIL  @"/api/activity/rotaryTableWelfareDetail"
#define TURNTABLE_DRAWROTARY     @"/api/activity/drawRotaryTable"


/** 个人中心 **/
#define USER_CENTER_MENUE_URL   @"/views/userCenter.json"
#define USER_CENTER_GROWTH_URL   @"/data/json/growth_config.json"
#define USER_CENTER_SIGIN_URL   @"/api/user/checkIn"
#define LogoutUrl               @"/v/user/logout" //退出
#define LoginUrl                @"/v/user/login"   //登录
#define LoginFvUrl                @"/v/user/fv_login"   //fv登录
#define LoginPhoneUrl                @"/v/user/phoneLogin"   //登录

#define RegisterUrl             @"/v/user/reg"//注册
#define LoginLimit              @"/data/json/limit/userLoginLimit.json"//验证码
#define GET_LIMIET_URL              @"/api/limit/get"
#define RegisterLimit           @"/v/user/getRegLimit"//验证码
#define VerifyIpLocationFields  @"/v/user/verifyIpLocationFields"//归属地变更验证
#define INIT_PWD_URL            @"/v/user/initPwd"//重置密码
#define RegisterDataLimit       @"/data/json/limit/registerLimit.json"//注册配置信息（试玩）
#define InfoUrl                 @"/api/user/info"   //用户信息
#define InfoStatus_URL          @"/api/user/status"//金额和登录状态
#define FundPwd                 @"/api/userFindPwd/save" //找回密码
#define USER_UPDATE_PASSWORD    @"/api/user/updatePassword" //修改登录密码
#define USER_UPDATE_FUNDPWD     @"/api/user/updateFundPwd" //修改取款密码

//#define USER_MODIFY_USERINFO    @"/api/user/modifyUserInfo" //完善个人信息
#define USER_MODIFY_USERINFO    @"/api/user/modifyUserInfoNew" //完善个人信息

#define USER_VERIFYTYPE_USERINFO_PHONE    @"/v/user/get_sms_code" //完善个人信息内的手机验证码

#define CHECKUNIQUE_URL         @"/v/user/checkUnique"  //校验唯一
#define VCode                   @"/v/vCode"//验证码
#define V_USRE_CODE             @"/v/vUserCode" //登录后验证码
#define CP_RECORDS_URL          @"/api/cp/records/todayList" //彩票投注记录
#define CP_CANCEL_URL           @"/api/bet/cancel"  //彩票撤单
#define CP_HISTORYDAY_URL       @"/api/cp/records/historyDayReport"  //彩票历史记录
#define CP_HISTORYLIST_URL      @"/api/cp/records/historyList"  //彩票历史投注记录详情
#define LIVE_ALLLIVEGAMES_URL   @"/api/live/allLiveGames"
#define GAME_RECORD_LIST_URL    @"/api/live/gameReportList"  //真人电子体育投注记录
#define GAME_PLAY_SETTING       @"/data/json/game_play_setting.json"
#define DeliverURL              @"/v/records/queryByAgent" //投注记录
#define USER_TRACE_URL          @"/api/trace/query"//追号记录
#define USER_TRACE_TRUE_URL     @"/api/trace/turn"  //追号详情
#define UserMessageURL          @"/api/pushMessage/queryAll"//消息记录
#define MESSAGE_READ_URlL       @"/api/pushMessage/modifyReadStatus" //读消息
#define MESSAGE_REMOVE_URlL     @"/api/pushMessage/remove"  //删除消息
#define UserNoticeURL           @"/api/notice/queryAll"  //游戏公告
#define USER_DRAW_URL           @"/api/userWithdraw/queryPage"
#define USER_RWS_TEAM_URL       @"/api/dl/queryTeamRws"  //个人总览
#define USER_RWS_PERSON_URL     @"/api/dl/queryPersonalRws"  //团队总览
#define USER_SUBUSERS_URL       @"/api/dl/querySubUsers"  //用户列表
#define USER_UPDATE_REBATE_URL  @"/api/dl/updateRebate"  //返点设定
#define USER_REBATE_RANGE_URL   @"/api/dl/getRebateRange"  //返点区间
#define USER_DLREBATE_RANGE_URL @"/api/dl/getDlSubRebateRange"  //返点区间
#define USER_ADD_REBATE_URL     @"/api/dl/add"  //注册管理
#define USER_VALID_DRAW_URL     @"/api/userWithdraw/validWithdraw/queryAll"
#define USER_BANK_DRAW_URL      @"/data/json/config.json" //银行卡信息
#define USER_BANK_MODIFY_URL    @"/api/user/addBankCard" //添加银行卡信息
#define USER_BANK_CARD_URL      @"/api/user/modifyBankCard" //编辑银行卡信息
#define USER_BANK_RELEASE_URL   @"/api/user/releaseBindBankCard" //解除银行卡信息
#define USER_UPDATE_SPREAD_URL  @"/api/spreadInfo/updateByAgentId"  //修改推广链接
#define USER_SPREAD_INFO_URL    @"/api/spreadInfo/queryPageByAgentId"  //推广详情
#define USER_REMOVE_SPREAD_URL  @"/api/spreadInfo/removeByAgentId"  //闪退推广链接
#define USER_SPREAD_NET_URL     @"/api/spreadInfo/createByAgentId" //网页链接
#define USER_SPREAD_TYPES_URL   @"/api/spreadInfo/spreadTypes"  //推广页面
#define USER_QRCODE_URL         @"/api/qrcode"   //二维码
#define USER_Live_BR_URL        @"/api/live/br"  //真人电子投注记录
#define USER_RECHARGE_URL       @"/api/recharge/personalRechargeOrder" //充值订单
#define USER_DISCOUNT_TYPE_URL  @"/api/recharge/queryDiscountTypeList" //充值优惠类型
#define USER_PAGE_BILL_URL      @"/api/reportMag/queryPageBill" //账户明细
#define USER_DRAW_MONEY_URL     @"/api/userWithdraw/queryOutMoneyIndex" //提款信息
#define USER_SAVE_MONEY_URL     @"/api/userWithdraw/save" //提款
#define TREND_NUMTREND          @"%@/anls-api/data/%@/numTrend/100.do" //走势
#define USER_TRANSFER           @"/api/live/transfer"  //额度转换
#define USER_BALANCE            @"/api/live/getBalance"  //查询余额
#define USER_PAYTYPES_URL       @"/api/recharge/getPayTypes"  //充值类型
#define USER_PAYACCOUNT_URL     @"/api/recharge/getPayAccounts"  //具体充值类型
#define USER_PAYCHANNEL_URL     @"/api/recharge/getTpPayChannels"  //具体充值类型
#define USER_PAYCONFIG_URL      @"/api/recharge/getRechargeConfig"  //
#define USER_TRANSFER_URL       @"/api/recharge/transfer"  // 充值
#define USER_ONLINEPAY_URL      @"/api/recharge/onlinePay"  // 在线充值
#define USER_CHECKOUTERROR_URL  @"/v/user/checkoutErrorCount"  // 验证码
#define USER_EDIT_LIMIT_URL     @"/data/json/limit/editUserInfoLimit.json"  // 完善资料
#define USER_LOFINVERIFY_LOGIN_URL    @"/v/user/loginVerify"//获取图片验证或者腾讯验证判断
#define USER_VERIFY_CHANGESMS_URL    @"/v/user/changeLoginPhoneVerify"//获取图片验证或者腾讯验证判断

#define USER_SMS_LOGIN_URL            @"/v/user/getLoginPhoneCode"
#define USER_SMS_UPDATE_URL            @"/v/user/getUpdateSMSCode"
#define USER_SMS_REGISTER_URL          @"/v/user/getRegisterSMSCode"

#define USER_CANCLEPHONE_URL            @"/api/user/releaseLoginPhone"
#define USER_MODIFYPHONE_URL            @"/api/user/updateLoginPhone"

#define USER_LOFINVERIFY_REGISTER_URL @"/v/user/regVerify"
#define USER_FORCEUPDATEPSW_URL @"/v/user/forceUpdatePassword"//强制修改密码
#define USER_EDIT_URL           @"/data/json/limit/editUserInfoLimit.json"//完善资料
#define USER_TRANSLIST_URL      @"/api/reportMag/getTransList"//账变记录类型

/** 彩票**/
#define Lottery_Official_ConfigUrl            @"/data/json/official/config"//官方彩票配置信息
#define LotteryDataUrl(lotteryID)   ([NSString stringWithFormat:@"/data/json/%@.json",lotteryID])//官方彩票详情
#define Lottery_OpenInfo_URL         @"/v/lottery/openInfo"    //单彩票开奖情况
#define Lottery_LuziInfo_URL         @"/v/lottery/luzhi"        //单彩票路子数据
#define Lottery_Lmcl_URL             @"/v/lottery/getLmcl"       //单彩票长路数据
#define GAME_CENTER_URL             @"/views/app_gameCenterNav.json"       //游戏大厅
#define GAME_LOBBY_URL             @"/views/appGamesLobby.json"       //游戏大厅二

/** 体育**/
#define SPORT_ON_OFF_URL         @"/api/sports/getSportOnOff"
#define SPORT_MATCH_URL          @"/api/sports/match"
#define SPORT_RECORD_URL         @"/api/sports/queryAllBet"  //投注记录
#define SPORT_RESULT_URL         @"/api/sports/loadResult"  //赛果
#define SPORT_BETCONFIG_URL      @"/api/sports/getBetConfig" //投注限制
#define SPORT_SINGLE_CONFIG_URL  @"/api/sports/getSingleUserBetConfig" //投注单个限制
#define SPORT_MAINTIME_URL       @"/api/sports/getMaintenanceTime" //是否维护
#define Lottery_GAME_Lmcl_URL    @"/v/lottery/game_lmcl" //自定义屠龙
#define Lottery_GAME_RANK_URL    @"/v/lottery/lmcl_rank" //自定义屠龙混排
#define SPORT_MESSAGE            @"/api/sports/message" //体育公告

/** 真人电子棋牌跳转URL**/
#define LIVE_ELE_QIPAI_URL       @"/api/live/play"
#define LIVE_TEST_URL            @"/api/live/freePlay"
#define JB_KY_URL                @"/api/live/qst"
#define WZRY_ESPORTS_URL         @"/api/eSports/queryMatchPage"
#define WZRY_ESPORTS_DATE_URL    @"/api/eSports/queryMatchDate"
#define WZRY_ESPORTS_MATCH_URL   @"/api/eSports/queryMatchCompetition"
#define WZRY_ESPORTS_RESULT_URL  @"/api/eSports/queryMatchResult"
#define WZRY_ESPORTS_ODDS_URL    @"/api/eSports/queryCompetitionOdds"
#define WZRY_ESPORTS_BET_URL     @"/api/eSports/bet"
#define WZRY_ESPORTS_RECORD_URL  @"/api/eSports/queryOrderPage"
#define ALL_QUERYGAME_LIST_URL      @"/api/live/queryAllFirstKind"

/** 聊天**/
#define CHAT_POSTINIT_URL                        @"/api/chat/post/api_u_init"
//#define CHAT_UPLOAD_IMG_URL                      @"/api/chat/uploadImage"
#define CHAT_UPLOAD_IMG_URL                      @"/api/chat/uploadTmpImage"
#define CHAT_UPLOAD_HEAD_URL                     @"/api/chat/uploadHead"
#define TheGameOpenTime                          @"/v/lottery/openInfo"
#define CHAT_REDDETAIL_URL                       @"/api/activity/getChatRedPacket"
#define CHAT_TOPLIST_URL                         @"/api/otth/queryLeadBoard"
#define CHAT_BANUSER_URL                         @"/api/chat/post/w_room_speak"
#define CHAT_BANIMGUSER_URL                      @"/api/chat/post/w_room_sendImg"
#define CHAT_DELETEMESSAGE_URL                   @"/api/chat/post/w_room_removeMessage"
#define CHAT_KITEUSER_URL                        @"/api/chat/post/w_room_kick"
#define CHAT_LOTTERY_TOPRESULTES_URL             @"/v/lottery/getTopResults"
#define CHAT_UPDATE_USERINFO_URL                 @"/api/chat/post/api_user_update"
#define CHAT_AVATOALIST_JSON                     @"/chat_user_avatar.json"
#define CHAT_USERINFO_URL                        @"/api/chat/post/api_user_get"
#define CHAT_HEELBET_URL                        @"/api/bet/heelBet"
#define Chat_MESSAGELIST_URL                    @"/api/chat/post/w_room_historyMessage"
#define CHAT_SLIDEMENU_URL                       @"/data/json/chat-menu.json"
#define CHAT_DELEGATE_URL                       @"/views/app_float.json"
#define CHAT_REMOVEALL_URL                      @"/api/chat/post/w_room_removeAllMessage"
#define WZRY_OPENSTATUS                          @"/api/eSports/getOpenStatus"//王者荣耀开关接口

/** 优惠活动**/
//#define ACTIVITY_RECOR_URL                        @"/api/activity/queryMyActivityRecord"
#define ACTIVITY_PRMT_URL                        @"/api/prmt-records"

#define GET_ACTIVITY_RECOR_URL                    @"/api/activity/getActivityRecord"
#define MY_ACTIVITY_URL                           @"/api/activity/queryMyActivityQualification"
#define MY_LOTTERY_URL                            @"/api/activity/queryMyActivityLottery"
#define MY_WELFARE_URL                            @"/api/activity/queryMyWelfareDetailList"

#define MAIN_FLEX_MANUE_URL                     @"/views/app_menu.json"

#define USER_RECHARGECARD_URL                   @"/views/web_more_rechange.json"
#define USER_PAYTYPELIST_URL                   @"/api/recharge/getPayTypes"
#define USER_RECHARGELIMIT_URL                    @"/api/recharge/getRechargeLimit"
#define LAUNCH_AD_URL                           @"/views/advertisement.json"
#define SLIDER_JSON_URL                       @"/views/app_index_slider.json"

/** 彩金 **/
#define USER_PRMT_LOBY                      @"/views/yhApplication.json"
#define USER_PRMT_APPLY                     @"/api/prmt-activities/apply"

#endif /* RequestUrlHeader_h */

/** 余额宝*/
#define USER_YEBAO_INFO_URL                      @"/api/yubao/info"
#define USER_YEBAO_TRANSFER_IN_URL                @"/api/yubao/transferIn"
#define USER_YEBAO_TRANSFER_OUT_URL                @"/api/yubao/transferOut"
#define USER_YEBAO_TRANSFER_HISTORY_URL                @"/api/yubao/interestHistory"

/**借呗*/

#define LOG_SEND_URL                          @"http://appadmin.yibofafa666.com/api/app/error/upload4Ios"
#define USER_LOAN_RECORD_URL                          @"/api/user/loan/list"
#define USER_LOAN_APPLY_MONEY_URL                          @"/api/user/loan/apply"
#define USER_LOAN_REPAY_MONEY_URL                          @"/api/user/loan/repay"
#define USER_GROWTH_LIST_URL                  @"/api/user/growth/list"

#define IMMEDIATE_URL                         @"/api/pushMessage/queryImmediateMessage"
#define UPDATE_IMMEDIATE_URL                  @"/api/pushMessage/updateMessageStatus"

#define LOG_SEND_URL                          @"https://app.appxxcj.com/api/app/error/upload4Ios"
//#define LOG_SEND_URL                          @"https://app.appxxcj.com/api/app/error/reportItf"

#define SET_USERTRANSFER                      @"/api/user/setUserTransfer"
#define LIVE_FREETRANSFER                      @"/api/live/freeTransfer"
#define LIVE_RECLAIM                           @"/api/live/reclaimLiveAmount"

#define IP_INFO                           @"/v/ipInfo"
