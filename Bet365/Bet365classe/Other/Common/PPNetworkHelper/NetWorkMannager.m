
//
//  NetWorkMannager.m
//  NetWorkMannager
//
//  Created by Deep river on 16/8/12.
//  Copyright © 2016年 com.mackun. All rights reserved.
//


#import "NetWorkMannager.h"
#import "AFNetworking.h"
#import "AFNetworkActivityIndicatorManager.h"
#import <CoreTelephony/CTCarrier.h>
#import "AFHTTPRequestSerializer+Bet365.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import "AFHTTPRequestSerializer+Bet365.h"
//#import "NSURLRequest+SSL.h"
#import "RequestUrlHeader.h"
#import "NetCloudSwitchTool.h"

#import "SafeguardViewController.h"
#import <TFHpple/TFHpple.h>

@interface NetWorkMannager (){

}

@property (nonatomic,strong)NSMutableArray <NetworkStatusBlock>*statusBlockList;

@end


@implementation NetWorkMannager


static NetWorkMannager *_instance = nil;

static NSString *_userAgent = nil;



+(instancetype)shareManager{
    return [[self alloc] init];
}

+(instancetype)allocWithZone:(struct _NSZone *)zone{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [super allocWithZone:zone];
    });
    return _instance;
}

- (instancetype)init{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [super init];
        [_instance startMonitoringNetwork];
    });
    return _instance;
}
#pragma mark - 开始监听网络
- (void)startMonitoringNetwork
{
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status)
        {
            case AFNetworkReachabilityStatusUnknown:
                
                _status = PPNetworkStatusUnknown;
                NSLog(@"未知网络");
                break;
            case AFNetworkReachabilityStatusNotReachable:
                _status = PPNetworkStatusNotReachable;
                NSLog(@"无网络");
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                _status = PPNetworkStatusReachableViaWWAN;
                NSLog(@"手机自带网络");
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                _status = PPNetworkStatusReachableViaWiFi;
                NSLog(@"WIFI");
                break;
        }
        [self.statusBlockList enumerateObjectsUsingBlock:^(NetworkStatusBlock  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj(_status);
        }];
    }];
    [manager startMonitoring];
    
}

- (void)checkNetworkStatusWithBlock:(NetworkStatusBlock)statusBlock{
    if (!statusBlock || [self.statusBlockList containsObject:statusBlock]) {
        return;
    }
    [self.statusBlockList addObject:statusBlock];
}

- (BOOL)currentNetworkStatus{
    return (_status > PPNetworkStatusNotReachable);
}

#pragma mark - GET请求自动缓存

- (NSURLSessionTask *)GET:(NSString *)URL
              RequestType:(PPSerializerType)requestType
             ResponseType:(PPSerializerType)responseType
               parameters:(NSDictionary *)parameters

            responseCache:(HttpRequestCache)responseCache
                  success:(HttpRequestSuccess)success
                  failure:(HttpRequestFailed)failure
{

    NSString *serverUrl;
    NSMutableDictionary *mParameters = [[NSMutableDictionary alloc] initWithDictionary:parameters];
    if(![URL hasPrefix:@"http"]){
        serverUrl = [NSString stringWithFormat:@"%@%@",SerVer_Url,URL];
        double _t = 1000 *[[NSDate date] timeIntervalSince1970];
        [mParameters setObject:[NSString stringWithFormat:@"%.f",_t] forKey:@"_t"];
    }else{
        serverUrl = URL;
    }
    NSString *urlHeader = [URL componentsSeparatedByString:@"?_t="].firstObject;
    id ppCache = netResponseElectrometer([PPNetworkCache getHttpCacheForKey:urlHeader]);
    responseCache ? responseCache(ppCache) : nil;

    if (!success || !failure) {
        return nil;
    }
    
//    [NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:[[NSURL URLWithString:serverUrl] host]];
    AFHTTPSessionManager *manager = [self createAFHTTPSessionManagerByRequestType:requestType ResponseType:responseType];

    return [manager GET:serverUrl parameters:mParameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

//        NSLog(@"%@\n%@",task.originalRequest.URL,responseObject);
        
//        if ([responseObject isKindOfClass:[NSData class]]) {
//            NSLog(@"%@\n%@",task.originalRequest.URL,[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil]);
//        }else{
//            NSLog(@"%@\n%@",task.originalRequest.URL,responseObject);
//        }
        
        [self reStrogeCookie:task];
        id retentObj = netResponseElectrometer(responseObject);
        if (responseCache && retentObj) {
            [PPNetworkCache saveHttpCache:retentObj forKey:urlHeader];
        }
        success ? success(retentObj) : nil;
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [[AjiRequestTaskMetricsTool sharedInstance] requestSendError:error];
        
        error.bet365RequestUrl = serverUrl;
        
        failure ? failure(error) : nil;
        if (task.state != NSURLSessionTaskStateCanceling) {
            if (error.tokenInvalid) {
                [USER_DATA_MANAGER cleanData];
                BET_CONFIG.userSettingData.fv = nil;
                [BET_CONFIG.userSettingData save];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [NAVI_MANAGER tokenInvalidPopRoot];
                });
            }else if (error.chatNoRoom){
                
            }else if(error.msg){
                if ([URL containsString:LoginUrl]){
                    return;
                }else if ([error.msg isContainChinese] && ![URL containsString:CHAT_POSTINIT_URL]) {
                    [SVProgressHUD showErrorWithStatus:error.msg];
                }else{
                    [self alertError:error];
                }
            }
        }
    }];
}




#pragma mark - POST请求自动缓存

- (NSURLSessionTask *)POST:(NSString *)URL
               RequestType:(PPSerializerType)requestType
              ResponseType:(PPSerializerType)responseType
                parameters:(NSDictionary *)parameters
             responseCache:(HttpRequestCache)responseCache
                   success:(HttpRequestSuccess)success
                   failure:(HttpRequestFailed)failure
{
    NSString *serverUrl;
    NSMutableDictionary *mParameters = [[NSMutableDictionary alloc] initWithDictionary:parameters];
    if(![URL hasPrefix:@"http"]){
        serverUrl = [NSString stringWithFormat:@"%@%@",SerVer_Url,URL];
        NSInteger _t = 1000 *[[NSDate date] timeIntervalSince1970];
        [mParameters setObject:@(_t) forKey:@"_t"];
    }else{
        serverUrl = URL;
    }
    NSString *urlHeader = [URL componentsSeparatedByString:@"?_t="].firstObject;
    id ppCache = netResponseElectrometer([PPNetworkCache getHttpCacheForKey:urlHeader]);
    responseCache ? responseCache(ppCache) : nil;
    
    if (!success || !failure) {
        return nil;
    }
    
//    [NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:[[NSURL URLWithString:serverUrl] host]];
    AFHTTPSessionManager *manager = [self createAFHTTPSessionManagerByRequestType:requestType ResponseType:responseType];
    
    return [manager POST:serverUrl parameters:mParameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self reStrogeCookie:task];
        id retentObj = netResponseElectrometer(responseObject);
        if (responseCache && retentObj) {
            [PPNetworkCache saveHttpCache:retentObj forKey:urlHeader];
        }
        
        success ? success(retentObj) : nil;
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [[AjiRequestTaskMetricsTool sharedInstance] requestSendError:error];
      
        error.bet365RequestUrl = serverUrl;
        failure ? failure(error) : nil;
        if (task.state != NSURLSessionTaskStateCanceling) {
            if (error.tokenInvalid) {
                [USER_DATA_MANAGER cleanData];
                BET_CONFIG.userSettingData.fv = nil;
                [BET_CONFIG.userSettingData save];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [NAVI_MANAGER tokenInvalidPopRoot];
                });
            }else if (error.chatNoRoom){
                
            }else if(error.msg){
                if ([URL containsString:LoginUrl] || [URL containsString:LoginPhoneUrl]){
                    return;
                }else if ([error.msg isContainChinese] && ![URL containsString:CHAT_POSTINIT_URL]) {
                    [SVProgressHUD showErrorWithStatus:error.msg];
                }else{
                    [self alertError:error];
                }
            }
        }
    }];
}

//NET_ERROR_URL
#pragma mark - 上传图片文件

- (NSURLSessionTask *)upload:(NSString *)url
                      ImageData:(NSData *)imageData
                       Width:(NSInteger)w
                      height:(NSInteger)h
                    Paramers:(NSDictionary *)paraamers
                  folderName:(NSString*)fd
                    fileName:(NSString *)fileName
                    progress:(HttpProgress)progress
                     success:(HttpRequestSuccess)success
                     failure:(HttpRequestFailed)failure
{
    
    NSMutableDictionary *paramers = [[NSMutableDictionary alloc] initWithDictionary:paraamers];
    [paramers setObject:@(w) forKey:@"imgWidth"];
    [paramers setObject:@(h) forKey:@"imgHeight"];
    
    AFHTTPSessionManager *manager = [self createAFHTTPSessionManagerByRequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON];
        return [manager POST:[NSString stringWithFormat:@"%@%@",SerVer_Url,url] parameters:paramers constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            NSString *type = [UIImage contentTypeWithImageData:imageData];
            [formData appendPartWithFileData:imageData
                                        name:fd
                                    fileName:[NSString stringWithFormat:@"%@.%@",fileName,type]
                                    mimeType:[NSString stringWithFormat:@"image/%@",type]];
            
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        //上传进度
        progress ? progress(uploadProgress) : nil;
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        success ? success(responseObject) : nil;
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [[AjiRequestTaskMetricsTool sharedInstance] requestSendError:error];
        error.bet365RequestUrl = url;
        failure ? failure(error) : nil;
        if (error.tokenInvalid) {
            BET_CONFIG.userSettingData.fv = nil;
            [BET_CONFIG.userSettingData save];
            [USER_DATA_MANAGER cleanData];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [NAVI_MANAGER tokenInvalidPopRoot];
            });
        }else if(error.msg){
            [SVProgressHUD showErrorWithStatus:error.msg];
        }
    }];
    
}


- (NSURLSessionTask *)uploadServerUrl:(NSString *)serverUrl
                               APIurl:(NSString *)url
                      ImageData:(NSData *)imageData
                       Width:(NSInteger)w
                      height:(NSInteger)h
                    Paramers:(NSDictionary *)paraamers
                  folderName:(NSString*)fd
                    fileName:(NSString *)fileName
                    progress:(HttpProgress)progress
                     success:(HttpRequestSuccess)success
                     failure:(HttpRequestFailed)failure
{
    
    NSMutableDictionary *paramers = [[NSMutableDictionary alloc] initWithDictionary:paraamers];
    [paramers setObject:@(w) forKey:@"imgWidth"];
    [paramers setObject:@(h) forKey:@"imgHeight"];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

        return [manager POST:[NSString stringWithFormat:@"%@%@",serverUrl,url] parameters:paramers constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            NSString *type = [UIImage contentTypeWithImageData:imageData];
            [formData appendPartWithFileData:imageData
                                        name:fd
                                    fileName:[NSString stringWithFormat:@"%@.%@",fileName,type]
                                    mimeType:[NSString stringWithFormat:@"image/%@",type]];
            
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        //上传进度
        progress ? progress(uploadProgress) : nil;
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        success ? success(responseObject) : nil;
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [[AjiRequestTaskMetricsTool sharedInstance] requestSendError:error];
        error.bet365RequestUrl = url;
        failure ? failure(error) : nil;
        if (error.tokenInvalid) {
            [USER_DATA_MANAGER cleanData];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [NAVI_MANAGER tokenInvalidPopRoot];
            });
        }else if(error.msg){
            [SVProgressHUD showErrorWithStatus:error.msg];
        }
    }];
    
}

#pragma mark - 下载文件
- (NSURLSessionTask *)downloadWithURL:(NSString *)URL
                              fileDir:(NSString *)fileDir
                             progress:(HttpProgress)progress
                              success:(void(^)(NSString *))success
                              failure:(HttpRequestFailed)failure
{
    AFHTTPSessionManager *manager = [self createAFHTTPSessionManagerByRequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URL]];
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
        //下载进度
        progress ? progress(downloadProgress) : nil;
        NSLog(@"下载进度:%.2f%%",100.0*downloadProgress.completedUnitCount/downloadProgress.totalUnitCount);
        
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        
        //拼接缓存目录
        NSString *downloadDir = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:fileDir ? fileDir : @"Download"];
        //打开文件管理器
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        //创建Download目录
        [fileManager createDirectoryAtPath:downloadDir withIntermediateDirectories:YES attributes:nil error:nil];
        
        //拼接文件路径
        NSString *filePath = [downloadDir stringByAppendingPathComponent:response.suggestedFilename];
        
        NSLog(@"downloadDir = %@",downloadDir);
        
        //返回文件位置的URL路径
        return [NSURL fileURLWithPath:filePath];
        
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        
        success ? success(filePath.absoluteString /** NSURL->NSString*/) : nil;
        if (failure && error) {
            [[AjiRequestTaskMetricsTool sharedInstance] requestSendError:error];
            error.bet365RequestUrl = URL;
            failure ? failure(error) : nil;
            if (error.tokenInvalid) {
                [USER_DATA_MANAGER cleanData];
                BET_CONFIG.userSettingData.fv = nil;
                [BET_CONFIG.userSettingData save];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [NAVI_MANAGER tokenInvalidPopRoot];
                });
            }else if(error.msg){
                if ([URL containsString:LoginUrl]){
                    return;
                }else if ([error.msg isContainChinese] && ![URL containsString:CHAT_POSTINIT_URL]) {
                    [SVProgressHUD showErrorWithStatus:error.msg];
                }else{
                    [self alertError:error];
                }
            }
        }
        
        
    }];
    
    //开始下载
    [downloadTask resume];
    
    return downloadTask;
    
}

-(void)setServerUrl:(nonnull NSString *)serverUrl{
    if (!serverUrl.length) {
        return;
    }
    if (![serverUrl hasPrefix:@"http://"] &&
        ![serverUrl hasPrefix:@"https://"]) {
        return;
    }
    if ([serverUrl isEqualToString:BET_CONFIG.userSettingData.serverUrl]) {
        return;
    }
    BET_CONFIG.userSettingData.serverUrl = serverUrl;
    [BET_CONFIG.userSettingData save];
    [self addCookieToURL:[NSURL URLWithString:serverUrl] key:@"Cookie" value:USER_DATA_MANAGER.userInfoData.cookie];
    if (!BET_CONFIG.configHasUpdate) {
        [BET_CONFIG getConfigCompleted:nil];
    }
    
}

-(void)addCookieToServerUrl{
    [self addCookieToURL:[NSURL URLWithString:BET_CONFIG.userSettingData.serverUrl] key:@"Cookie" value:USER_DATA_MANAGER.userInfoData.cookie];
}

-(void)addCookieToURL:(NSURL *)url key:(NSString *)key value:(NSString *)value{
    NSString *newValue = (value != nil) ? value : @"";
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSMutableArray *cookies = [NSMutableArray arrayWithArray:[cookieStorage cookiesForURL:url]];
    NSString *domian =  [url host];
    NSString *path =  [url path];
    NSMutableArray *tempCookies = [cookies copy];
    for (NSHTTPCookie *cookie in tempCookies) {
        if ([cookie.name isEqualToString:key]) {
            [cookies removeObject:cookie];
        }
    }
    NSMutableDictionary *properties = [NSMutableDictionary dictionary];
    [properties setObject:key forKey:NSHTTPCookieName];
    [properties setObject:newValue forKey:NSHTTPCookieValue];
    /**
     * Cookie跨域
     */
    if ([domian hasPrefix:@"."]) {
        [properties setObject:domian forKey:NSHTTPCookieDomain];
    }else{
        [properties setObject:[NSString stringWithFormat:@".%@",domian] forKey:NSHTTPCookieDomain];
    }

    [properties setObject:path forKey:NSHTTPCookiePath];
    NSHTTPCookie *cookieuser = [NSHTTPCookie cookieWithProperties:properties];
    [cookies addObject:cookieuser];
    [cookieStorage setCookies:cookies forURL:url mainDocumentURL:nil];
}
#pragma mark - Private


#pragma mark - 设置AFHTTPSessionManager相关属性

- (AFHTTPSessionManager *)createAFHTTPSessionManagerByRequestType:(PPSerializerType)requestType
                                                     ResponseType:(PPSerializerType)responseType
{
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
//    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
//    securityPolicy.allowInvalidCertificates = YES;
//    securityPolicy.validatesDomainName=NO;
//    manager.securityPolicy = securityPolicy;
    
    //设置请求的超时时间
    manager.requestSerializer.timeoutInterval = 10.0f;
    
    if (requestType == PPSerializerType_HTTP) {
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    }else if (requestType == PPSerializerType_JSON){
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    if (responseType == PPSerializerType_HTTP) {
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }else if (responseType == PPSerializerType_JSON){
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
    }
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",
                                                         @"text/plain",
                                                         @"text/json",
                                                         @"application/json",
                                                         @"text/javascript",
                                                         @"image/jpeg",
                                                         @"image/jpg",
                                                         nil];
    
    
//    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
//    [manager.requestSerializer setValue:[Bet365Tool netUserAgent] forHTTPHeaderField:@"User-Agent"];
//    [manager.requestSerializer setValue:@"application/json;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
//    if (USER_DATA_MANAGER.isLogin) {
//        [manager.requestSerializer setValue:[NSString stringWithFormat:@"token=%@",USER_DATA_MANAGER.userInfoData.token] forHTTPHeaderField:@"Cookie"];
//    }
    return manager;
}

-(void)reStrogeCookie:(NSURLSessionDataTask *)task{
    NSString *url = [task.currentRequest.URL absoluteString];
    if (![url containsString:LoginUrl] &&
        ![url containsString:RegisterUrl] &&
        ![url containsString:TestPlayGameUrl]) {
        return;
    }
    if ([task.response isKindOfClass:[NSHTTPURLResponse class]]) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        NSDictionary *allHeaders = response.allHeaderFields;
        NSString *cookie = [allHeaders objectForKey:@"Set-Cookie"];
        USER_DATA_MANAGER.userInfoData.cookie = cookie;
        [USER_DATA_MANAGER.userInfoData save];
    }
}

id netResponseElectrometer(id obj){
    if (!obj) {
        return nil;
    }else if ([obj isKindOfClass:[NSDictionary class]] ||
              [obj isKindOfClass:[NSArray class]] ||
              [obj isKindOfClass:[UIImage class]]){
        return obj;
    }else if ([obj isKindOfClass:[NSString class]]){
        return [NetWorkMannager filterDefence:obj];
    }else{
        id value = [NSJSONSerialization JSONObjectWithData:obj options:NSJSONReadingMutableContainers error:nil];
        if (!value) {
            value =  [[NSString alloc] initWithData:obj encoding:NSUTF8StringEncoding];
        }
        if (!value) {
            value = [[UIImage alloc] initWithData:obj];
        }
        return [NetWorkMannager filterDefence:value];
    }
}

BOOL safeguard(id obj){
    if ([obj isKindOfClass:[NSString class]]){
        BOOL isSafeguard = [obj containsString:@"维护"];
        return isSafeguard;
    }
    return NO;
}

+(id)filterDefence:(id)value{
    BOOL isSafeguard = safeguard(value);
    if (isSafeguard) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            UIViewController *controller = [NAVI_MANAGER getCurrentVC];
            if([controller isKindOfClass:[UIViewController class ]]){
                static dispatch_once_t onceToken;
                dispatch_once(&onceToken, ^{
                    SafeguardViewController *vc = [[SafeguardViewController alloc] initWithNibName:@"SafeguardViewController" bundle:nil];
                    vc.htmlString = value;;
                    [controller presentViewController:vc animated:YES completion:nil];
                });
            }
        });
    }
    if (isSafeguard) {
        return nil;
    }else{
        return value;
    }
}

- (void)alertError:(NSError *)error
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(kAlertError:) object:error];
    [self performSelector:@selector(kAlertError:) withObject:error afterDelay:3.0f];
}

-(void)kAlertError:(NSError *)error{
    if ([error.msg hasPrefix:@"<html>"]) {
        NSString *msg = nil;
        if ([error.msg containsString:@"403"]){
            msg = @"403";
            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"访问限制,请联系客服%@\n%@",[Bet365Tool shareInstance].ipStr,msg]];
        }
    }
}

#pragma GET/SET
-(NSMutableArray<NetworkStatusBlock> *)statusBlockList{
    if (!_statusBlockList) {
        _statusBlockList = [NSMutableArray array];
    }
    return _statusBlockList;
}
@end
