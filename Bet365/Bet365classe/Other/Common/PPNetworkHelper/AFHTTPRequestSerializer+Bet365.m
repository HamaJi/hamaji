//
//  AFHTTPRequestSerializer+Bet365.m
//  Bet365
//
//  Created by HHH on 2018/8/14.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "AFHTTPRequestSerializer+Bet365.h"
#import <Aspects/Aspects.h>
@implementation AFHTTPRequestSerializer (Bet365)
+(void)load{
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        aji_swizzleClassMethod([self class],
                           @selector(serializer),
                           @selector(serializer_swizzle));
    });
    
   
}
+(instancetype)serializer_swizzle{
    AFHTTPRequestSerializer *instance = [self serializer_swizzle];
    [instance setValue:[Bet365Tool netUserAgent] forHTTPHeaderField:@"User-Agent"];
    return instance;
}

@end
