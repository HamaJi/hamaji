//
//  SubmitLogModel.h
//  Bet365
//
//  Created by HHH on 2018/11/2.
//  Copyright © 2018年 jesse. All rights reserved.
//


#define LOG_CHAT_SOCKETCONNECT  @"LOG_CHAT_SOCKETCONNECT"
#define LOG_CHAT_JOIN           @"LOG_CHAT_JOIN"
#define LOG_CHAT_GETMORELIST    @"LOG_CHAT_GETMORELIST"
#define LOG_CHAT_INFO           @"LOG_CHAT_INFO"

#define LOG_LOGIN_ERROR         @"LOG_LOGIN_ERROR"
#define LOG_LOGIN_NONEERROR     @"LOG_LOGIN_NONEERROR"
#define LOG_REGISTER_ERROR      @"LOG_REGISTER_ERROR"

#define LOG_UPLOADIMAG_ERROR    @"LOG_UPLOADIMAG_ERROR"


@interface SubmitLogModel : MTLModel<MTLJSONSerializing>


/**
 app版本
 */
@property (nonatomic,assign) NSInteger build;

/**
 账号
 */
@property (nonatomic,strong) NSString *acc;

/**
 密钥
 */
@property (nonatomic,strong) NSString *token;

/**
 开始时间
 */
@property (nonatomic,strong) NSDate *starTime;

/**
 结束时间
 */
@property (nonatomic,strong) NSDate *endTime;

/**
 持续时间
 */
@property (nonatomic,assign) NSTimeInterval duration;

/**
 事件标签
 */
@property (nonatomic,strong) NSString *events;

/**
 自定义详情
 */
@property (nonatomic,strong) NSString *content;

@property (nonatomic,strong) NSData *data;

@property (nonatomic,strong) NSString *ip;
@end
