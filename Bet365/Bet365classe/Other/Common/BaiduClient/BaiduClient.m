//
//  BaiduClient.m
//  BaiduCloudSec
//
//  Created by HHH on 2018/8/2.
//  Copyright © 2018年 HHH. All rights reserved.
//

#import "BaiduClient.h"
#import "AESCrypt.h"
#import "NSString+AES_Base64.h"
#import "NSData+AES.h"
#import "CloudModel.h"

#define Baidu_ACCESS        @""
#define Baidu_SECRET        @""
#define Baidu_ENDPOINT_URL  @""

@interface BaiduClient()
/**
 BOS客户端
 */
@property (nonatomic,strong)BOSClient* client;

/**
 客户端密钥类
 */
@property (nonatomic,strong)BCECredentials* credentials;

/**
 客户端域名类
 */
@property (nonatomic,strong)BOSClientConfiguration* configuration;
@end

@implementation BaiduClient

static BaiduClient *instance = nil;

+(instancetype)shareInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[BaiduClient alloc] init];
    });
    return instance;
}

-(instancetype)init{
    if (self = [super init]) {
        
        [self getObjectsByBucketName:Baidu_INFO_BUCKET_PATCH Success:^(BOSListObjectsResponse *objectList) {
            
        } Fail:^(NSError *error) {

        }];
    }
    return self;
}

#pragma mark - Bucket

/**
 获取Bucket列表
 
 @param success 成功回调
 @param fail 失败回调
 */
-(void)getBucketList:(void(^)(NSArray<BOSBucketSummary*>* buckets))success
                Fail:(void(^)(NSError *error))fail{
    __block BOSListBucketResponse* response = nil;
    BCETask* task = [self.client listBuckets];
    task.then(^(BCEOutput* output) {
        
        if (output.response) {
            response = (BOSListBucketResponse*)output.response;
        }
        // 获取Bucket信息
        NSArray<BOSBucketSummary*>* buckets = response.buckets;
        for (BOSBucketSummary* bucket in buckets) {
            NSLog(@"bucket name: %@", bucket.name);
            NSLog(@"bucket location: %@", bucket.location);
            NSLog(@"bucket create date: %@", bucket.createDate);

        }
        
        if (output.error) {
            fail ? fail(output.error) : nil;
        }else{
            success ? success(buckets) : nil;
        }
    });
    [task waitUtilFinished];
}

/**
 获取Bucket是否存在，已经权限
 
 @param path Buckete路径
 @param success 成功回调
 @param fail 失败回调
 */
-(void)getBucketAuthorityByPatch:(NSString *)path
                         Success:(void(^)(BOSHeadBucketResponse *bucketResponse))success
                            Fail:(void(^)(NSError *error))fail{
    __block BOSHeadBucketResponse* response = nil;
    BCETask* task = [self.client headBucket:path];
    task.then(^(BCEOutput* output) {
        if (output.response) {
            response = (BOSHeadBucketResponse*)output.response;
            success ? success(response) : nil;
            NSLog(@"head bucket success!");
        }
        
        if (output.error) {
            NSLog(@"Bucket not exist or no permission!");
            fail ? fail(output.error) : nil;
        }else{
            
        }
    });
    [task waitUtilFinished];
}


#pragma mark - Object

/**
 获取文件列表描述

 @param bucketName 归档名称
 @param success BOSListObjectsResponse.coentent
 @param fail 失败回调
 */
-(void)getObjectsByBucketName:(NSString *)bucketName
                      Success:(void(^)(BOSListObjectsResponse* objectList))success
                         Fail:(void(^)(NSError *error))fail{
    BOSListObjectsRequest* listObjRequest = [[BOSListObjectsRequest alloc] init];
    listObjRequest.bucket = bucketName;
    
    __block BOSListObjectsResponse* listObjResponse = nil;
    BCETask* task = [self.client listObjects:listObjRequest];
    task.then(^(BCEOutput* output) {
        if (output.response) {
            listObjResponse = (BOSListObjectsResponse*)output.response;
            success ? success(listObjResponse) : nil;
        }
        
        if (output.error) {
            NSLog(@"list objects failure");
            fail ? fail(output.error) : nil;
        }
    });
    [task waitUtilFinished];
}


/**
 获取文件

 @param bucketName 归档名称
 @param objectName 文件名
 @param success BOSObjectData.data为实体
 @param fail 失败回调
 */
-(void)getObjectByBucketName:(NSString *)bucketName
                  ObjectName:(NSString *)objectName Success:(void(^)(BOSGetObjectResponse* object))success
                        Fail:(void(^)(NSError *error))fail{
    __block BOSGetObjectResponse* getObjResponse = nil;
    BOSGetObjectRequest* getObjRequest = [[BOSGetObjectRequest alloc] init];
    getObjRequest.bucket = bucketName;
    getObjRequest.key = objectName;
    BCETask* task = [self.client getObject:getObjRequest];
    task.then(^(BCEOutput* output) {
        if (output.response) {
            getObjResponse = (BOSGetObjectResponse*)output.response;
            
            NSLog(@"get object success!");
            success ? success(getObjResponse) : nil;
        }
        
        if (output.error) {
            NSLog(@"get object failure with %@", output.error);
            fail ? fail(output.error) : nil;
        }
        
        if (output.progress) {
            NSLog(@"the get object progress is %@", output.progress);
        }
    });
    [task waitUtilFinished];
    
//    NSData* data = getObjResponse.objectData.data;
}


/**
 发送LogModel

 @param model 日志
 @param bucketName 频道Path
 @param success 成功回调
 @param fail 失败回调
 */
-(void)sendObject:(SubmitLogModel *)model BucketName:(NSString *)bucketName Success:(void(^)())success Fail:(void(^)(NSError *error))fail{
    NSError *erro;
    NSDictionary *jsonDictionary = [MTLJSONAdapter JSONDictionaryFromModel:model error:&erro];
    NSData *data = [jsonDictionary mj_JSONData];
    if (!data) {
        fail ? fail(erro) : nil;
        return;
    }
    
    BOSObjectContent* content = [[BOSObjectContent alloc] init];
    content.objectData.data = data;
    
    BOSPutObjectRequest* request = [[BOSPutObjectRequest alloc] init];
    request.bucket = bucketName;
    
    if (model.acc.length) {
        request.key = [NSString stringWithFormat:@"%@/%li//%@/%@/%@",BET_CONFIG.userSettingData.defaultAppID,model.build,model.acc,model.events,[model.starTime stringWithFormat:@"yyyy-MM-dd-HH:mm:ss:SS"]];
    }else{
        request.key = [NSString stringWithFormat:@"%@/%li/unlogin/%@/%@",BET_CONFIG.userSettingData.defaultAppID,model.build,model.events,[model.starTime stringWithFormat:@"yyyy-MM-dd-HH:mm:ss:SS"]];
    }
    if (model.endTime) {
        request.key = [request.key stringByAppendingString:[NSString stringWithFormat:@"~%@",[model.endTime stringWithFormat:@"yyyy-MM-dd-HH:mm:ss:SS"]]];
    }
    
    request.objectContent = content;
    
    __block BOSPutObjectResponse* response = nil;
    BCETask* task = [self.client putObject:request];
    task.then(^(BCEOutput* output) {
        if (output.progress) {
            NSLog(@"put object progress is %@", output.progress);
        }
        
        if (output.response) {
            success ? success() : nil;
            response = (BOSPutObjectResponse*)output.response;
            NSLog(@"put object success!");
        }
        
        if (output.error) {
            fail ? fail(output.error) : nil;
            NSLog(@"put object failure");
        }
    });
    [task waitUtilFinished];
}



#pragma mark - Private


#pragma mark - GET/SET
-(BCECredentials *)credentials{
    if (!_credentials) {
        _credentials = [[BCECredentials alloc] init];
        _credentials.accessKey = Baidu_ACCESS;
        _credentials.secretKey = Baidu_SECRET;
    }
    return _credentials;
}

-(BOSClientConfiguration *)configuration{
    if (!_configuration) {
        _configuration = [[BOSClientConfiguration alloc] init];
        _configuration.endpoint = Baidu_ENDPOINT_URL;
        _configuration.region = [BCERegion region:BCERegionGZ];
        _configuration.credentials = self.credentials;
        
    }
    return _configuration;
}

-(BOSClient *)client{
    if (!_client) {
        _client = [[BOSClient alloc] initWithConfiguration:self.configuration];
    }
    return _client;
}
@end
