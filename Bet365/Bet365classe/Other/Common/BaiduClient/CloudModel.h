//
//  CloudModel.h
//  Bet365
//
//  Created by HHH on 2018/8/7.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface CloudModel : MTLModel <MTLJSONSerializing>

@property (nonatomic,copy)NSString * download;
@property (nonatomic,copy)NSString * updateDescription;
@property (nonatomic,assign)NSInteger  forcedUpdate;        //是否强制更新
@property (nonatomic,copy)NSArray  * domain;                //域名
//@property (nonatomic,copy)NSString * appID;
@property (nonatomic,copy)NSString *version;
@property (nonatomic,assign)NSInteger  build;
@property (nonatomic,assign)NSInteger  actBuild;
@end
