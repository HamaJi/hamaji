//
//  BaiduClient.h
//  BaiduCloudSec
//
//  Created by HHH on 2018/8/2.
//  Copyright © 2018年 HHH. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <BaiduBCEBasic/BaiduBCEBasic.h>
#import <BaiduBCESTS/BaiduBCESTS.h>
#import <BaiduBCEBOS/BaiduBCEBOS.h>

#import "SubmitLogModel.h"

#define Baidu_INFO_BUCKET_PATCH  @""

#define Baidu_LOG_BUCKET_PATCH  @""

#define BAI_CLOUD_MANAGER [BaiduClient shareInstance]

@interface BaiduClient : NSObject

+(instancetype)shareInstance;


#pragma mark - Bucket

/**
 获取Bucket列表
 
 @param success 成功回调
 @param fail 失败回调
 */
-(void)getBucketList:(void(^)(NSArray<BOSBucketSummary*>* buckets))success
                Fail:(void(^)(NSError *error))fail;

/**
 获取Bucket是否存在，已经权限
 
 @param path Buckete路径
 @param success 成功回调
 @param fail 失败回调
 */
-(void)getBucketAuthorityByPatch:(NSString *)path
                         Success:(void(^)(BOSHeadBucketResponse *bucketResponse))success
                            Fail:(void(^)(NSError *error))fail;

#pragma mark - Object

/**
 获取文件列表描述
 
 @param bucketName 归档名称
 @param success BOSListObjectsResponse.coentent
 @param fail 失败回调
 */
-(void)getObjectsByBucketName:(NSString *)bucketName
                      Success:(void(^)(BOSListObjectsResponse* objectList))success
                         Fail:(void(^)(NSError *error))fail;

/**
 获取文件
 
 @param bucketName 归档名称
 @param objectName 文件名
 @param success BOSObjectData.data为实体
 @param fail 失败回调
 */
-(void)getObjectByBucketName:(NSString *)bucketName
                  ObjectName:(NSString *)objectName Success:(void(^)(BOSGetObjectResponse* object))success
                        Fail:(void(^)(NSError *error))fail;

/**
 发送LogModel
 
 @param model 日志
 @param bucketName 频道Path
 @param success 成功回调
 @param fail 失败回调
 */
-(void)sendObject:(SubmitLogModel *)model BucketName:(NSString *)bucketName Success:(void(^)())success Fail:(void(^)(NSError *error))fail;
@end
