//
//  CloudModel.m
//  Bet365
//
//  Created by HHH on 2018/8/7.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "CloudModel.h"

@implementation CloudModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"download": @"download",
             @"updateDescription": @"updateDescription",
             @"forcedUpdate": @"forcedUpdate",
             @"domain": @"domain",
             @"version": @"version",
             @"build":@"build",
             @"actBuild":@"actBuild"
             };
}

+ (NSValueTransformer *)domainJSONTransformer{
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString *robin){
                return [robin componentsSeparatedByString:@","];
            } reverseBlock:^id(NSArray *d) {
                return [d componentsJoinedByString:@","];
            }];
}

@end

