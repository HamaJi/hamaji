//
//  SubmitLogModel.m
//  Bet365
//
//  Created by HHH on 2018/11/2.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "SubmitLogModel.h"

@implementation SubmitLogModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"build":@"build",
             @"acc": @"acc",
             @"token": @"token",
             @"starTime": @"starTime",
             @"endTime": @"endTime",
             @"duration" : @"duration",
             @"events": @"events",
             @"content": @"content",
             @"ip":@"ip",
             @"data":@"data"
             };
}

+ (NSValueTransformer *)starTimeJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString *time){
                return [NSDate dateWithString:time format:@"yyyy-MM-dd HH:mm:ss"];
            } reverseBlock:^id(NSDate *date) {
                return [date getTimeFormatStringWithFormat:@"yyyy-MM-dd HH:mm:ss"];
            }];
}

+ (NSValueTransformer *)endTimeJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString *time){
                return [NSDate dateWithString:time format:@"yyyy-MM-dd HH:mm:ss"];
            } reverseBlock:^id(NSDate *date) {
                return [date getTimeFormatStringWithFormat:@"yyyy-MM-dd HH:mm:ss"];
            }];
}


-(instancetype)init{
    if ([super init]) {
        self.build = BET_CONFIG.buildVersion;
        self.acc = USER_DATA_MANAGER.userInfoData.account;
        self.token = USER_DATA_MANAGER.userInfoData.token;
        self.starTime = [NSDate date];
        self.ip = [Bet365Tool shareInstance].ipStr;
        @weakify(self);
        [[RACSignal combineLatest:@[[RACObserve(self, starTime) distinctUntilChanged],[RACObserve(self, endTime) distinctUntilChanged]] reduce:^id _Nonnull(NSDate *start,NSDate *end){
            return @(start!=nil && end!=nil);
        }] subscribeNext:^(id  _Nullable x) {
            if (x) {
                @strongify(self);
                self.duration = [self.endTime timeIntervalSinceDate:self.starTime];
            }
        }];
    }
    return self;
}


@end
