//
//  Bet365Tool.h
//  Bet365
//
//  Created by HHH on 2018/7/2.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LunarSolarConverter.h"
@interface Bet365Tool : NSObject
@property (nonatomic,copy)NSString *ipStr;
+(instancetype)shareInstance;
+(void) checkCurrentNotificationStatusCompleted:(void(^)(BOOL allowd))completed;
/**
 获取设备当前本地IP地址
 
 @param preferIPv4 是否为ipv4
 @return 192.168.0.1
 */
+ (NSString *)getIPAddress:(BOOL)preferIPv4;

/**
 获取外网ip
 
 @param completed 192.168.0.1
 */
+ (void)getNetworkIPAddressCompleted:(void(^)(BOOL success))completed;

/**
 获取设备型号
 
 @return iPhone 7
 */
+ (NSString *)getDeviceName;



/**
 请求UserAgent

 @return (iOS/11.4)(D5336D34-5561-49B5-9DAC-F73A25C8A2B8)(5BE4D148-95D4-4A29-BD04-110F32D39801)(iPhone Simulator edge)
 */
+(NSString *)netUserAgent;
//返回solar
+(Solar *)initSolar:(NSDate *)date;
+(Solar *)initSolarWithDateString:(NSString *)dateString;
//获取生肖组合
+(NSString *)getZodNameForLHCByOpenTimeSolar:(Solar *)openTimeSolar Num:(NSInteger)num;

+ (UIImage *)lhcBackColorNumber:(NSInteger)num;

+(void)removeAllCacheOnCompletion:(void(^)())completion;

/**
 swizzle

 */

void aji_swizzleClassMethod();

void aji_swizzleInstanceMethod();

NSString *plistPath (NSString *headerStr);
@end
