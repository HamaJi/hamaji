//
//  VerifyManager.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/12/16.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "VerifyManager.h"
#import <TCWebCodesSDK/TCWebCodesBridge.h>
#import <GT3Captcha/GT3Captcha.h>
#import "Bet365VerifyImagCodeViewController.h"
@interface VerifyManager()
<GT3CaptchaManagerDelegate,
GT3CaptchaButtonDelegate>

@property (nonatomic,assign) VerifyAuthorType verifyType;

@property (nonatomic) NSString *t_captcha_app_id;

@property (nonatomic)NSDictionary *gt3Paramers;

@property (nonatomic,strong)GT3CaptchaManager *captchaManager;

@property (nonatomic,copy) void(^gt3ResultBlock)(NSDictionary *result);


@end

@implementation VerifyManager


-(void)setRequestJsonString:(NSString *)requestJsonString{
    NSMutableString *verifyTypeStr = [[NSMutableString alloc] initWithString:requestJsonString];
    
    if ([verifyTypeStr containsString:@"T_CAPTCHA"]) {
        [verifyTypeStr deleteString:@"T_CAPTCHA,"];
        self.t_captcha_app_id = verifyTypeStr;
        self.verifyType = VerifyAuthorType_TENCENT;
        if (self.captchaManager) {
            self.captchaManager = nil;
        }
    }else if ([verifyTypeStr containsString:@"GEETEST"]){
        [verifyTypeStr deleteString:@"GEETEST,"];
        self.gt3Paramers = [verifyTypeStr mj_JSONObject];
        self.verifyType = VerifyAuthorType_GT3;
    }else if([verifyTypeStr isEqualToString:@"IMAGE"]){
        self.verifyType = VerifyAuthorType_IMAGE;
        if (self.captchaManager) {
            self.captchaManager = nil;
        }
    }else{
        self.verifyType = VerifyAuthorType_UNVERIFY;
        if (self.captchaManager) {
            self.captchaManager = nil;
        }
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(verifyManager:updateVerifyAuthorType:)]) {
        [self.delegate verifyManager:self updateVerifyAuthorType:self.verifyType];
    }
}

-(void)launchVerify{
    
    if (self.verifyType == VerifyAuthorType_TENCENT) {
        @weakify(self);
        [self tcWebCodesVerify:^(NSDictionary * _Nonnull result) {
            @strongify(self);
            if (!self.delegate) {
                return;
            }
            if (result.allKeys.count) {
                if ([self.delegate respondsToSelector:@selector(verifyManager:verificationSuccessParamers:authorType:)]) {
                    [self.delegate verifyManager:self verificationSuccessParamers:result authorType:self.verifyType];
                }
            }else{
                if ([self.delegate respondsToSelector:@selector(verifyManager:verificationFaildAuthorType:)]) {
                    [self.delegate verifyManager:self verificationFaildAuthorType:self.verifyType];
                }
                
            }
        }];
    }else if (self.verifyType == VerifyAuthorType_GT3){
        @weakify(self);
        [self gt3CodesVerify:^(NSDictionary * _Nonnull result) {
            @strongify(self);
            if (!self.delegate) {
                return;
            }
            if (result.allKeys.count) {
                if ([self.delegate respondsToSelector:@selector(verifyManager:verificationSuccessParamers:authorType:)]) {
                    [self.delegate verifyManager:self verificationSuccessParamers:result authorType:self.verifyType];
                }
            }else{
                if ([self.delegate respondsToSelector:@selector(verifyManager:verificationFaildAuthorType:)]) {
                    [self.delegate verifyManager:self verificationFaildAuthorType:self.verifyType];
                }
                
            }
        }];
    }else if (self.verifyType == VerifyAuthorType_IMAGE){
        if (self.launchImageCode) {
            [self showVerifyImageCode];
        }else{
            if ( self.delegate && [self.delegate respondsToSelector:@selector(verifyManager:verificationSuccessParamers:authorType:)]) {
                [self.delegate verifyManager:self verificationSuccessParamers:@{} authorType:self.verifyType];
            }
        }
    }else{
        //无需验证方式则直接回抛成功
        if ( self.delegate && [self.delegate respondsToSelector:@selector(verifyManager:verificationSuccessParamers:authorType:)]) {
            [self.delegate verifyManager:self verificationSuccessParamers:@{} authorType:self.verifyType];
        }
    }
}
-(void)showVerifyImageCode{
    Bet365VerifyImagCodeViewController *vc = [[Bet365VerifyImagCodeViewController alloc] initWithNibName:@"Bet365VerifyImagCodeViewController" bundle:nil];
    vc.transitioningDelegate = vc;
    vc.modalPresentationStyle = UIModalPresentationCustom;
    [[NAVI_MANAGER getCurrentVC] presentPopViewController:vc completion:nil];
    vc.submitBlock = ^BOOL(NSString * _Nonnull code) {
        if (!code.length) {
            return NO;
        }
        if ( self.delegate && [self.delegate respondsToSelector:@selector(verifyManager:verificationSuccessParamers:authorType:)]) {
            [self.delegate verifyManager:self verificationSuccessParamers:@{@"vCode":code} authorType:self.verifyType];
        }
        return YES;
    };
    vc.resetBlock = ^{
        
    };
}
#pragma mark - TC
-(void)tcWebCodesVerify:(void(^)(NSDictionary *result))resultCompleted{
    if (!resultCompleted) {
        return;
    }
    [[TCWebCodesBridge sharedBridge] setCapOptions:@{@"sdkClose": @YES}];
    [[TCWebCodesBridge sharedBridge] loadTencentCaptcha:[UIApplication sharedApplication].keyWindow appid:self.t_captcha_app_id callback:^(NSDictionary *resultJSON) {
        if (0 == [resultJSON[@"ret"] intValue]) {
            NSMutableDictionary *paramers = @{}.mutableCopy;
            [paramers safeSetObject:resultJSON[@"ticket"] forKey:@"Ticket"];
            [paramers safeSetObject:resultJSON[@"randstr"] forKey:@"Randstr"];
            if(![paramers[@"Ticket"] length] ||
               ![paramers[@"Randstr"] length]){
                [SVProgressHUD showErrorWithStatus:@"ErrorMsg,图形验证码异常，请联系管理员"];
                resultCompleted ? resultCompleted(nil) : nil;
                return;
            }
            resultCompleted ? resultCompleted(paramers) : nil;
        }else{
            resultCompleted ? resultCompleted(nil) : nil;
        }
    }];
}


#pragma mark - GT3
-(void)gt3CodesVerify:(void(^)(NSDictionary *result))resultCompleted{
    if (!resultCompleted) {
        return;
    }
    self.gt3ResultBlock = resultCompleted;
    [self creatCaptchaManager];
    [self.captchaManager startGTCaptchaWithAnimated:YES];
}

-(void)creatCaptchaManager{
    if (!self.captchaManager) {
        GT3CaptchaManager *captchaManager = [GT3CaptchaManager sharedGTManagerWithAPI1:nil API2:nil timeout:5.0];
        [captchaManager configureGTest:self.gt3Paramers[@"gt"]
                             challenge:self.gt3Paramers[@"challenge"]
                               success:self.gt3Paramers[@"success"]
                              withAPI2:nil];
        captchaManager.maskColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6];
        //        [captchaManager startGTCaptchaWithAnimated:YES];
        captchaManager.delegate = self;
        [captchaManager useLanguage:GT3LANGTYPE_ZH_CN];
        self.captchaManager = captchaManager;
    }
}

#pragma mark - GT3CaptchaManagerDelegate
-(BOOL)shouldUseDefaultRegisterAPI:(GT3CaptchaManager *)manager{
    return NO;
}
-(BOOL)shouldUseDefaultSecondaryValidate:(GT3CaptchaManager *)manager{
    return NO;
}
- (void)gtCaptcha:(GT3CaptchaManager *)manager errorHandler:(GT3Error *)error {
    //处理验证中返回的错误
    if (error.code == -999) {
        // 请求被意外中断, 一般由用户进行取消操作导致, 可忽略错误
    }
    else if (error.code == -10) {
        // 预判断时被封禁, 不会再进行图形验证
    }
    else if (error.code == -20) {
        // 尝试过多
    }
    else {
        // 网络问题或解析失败
    }
    //    [TipsView showTipOnKeyWindow:error.error_code fontSize:12.0];
}

- (void)gtCaptchaUserDidCloseGTView:(GT3CaptchaManager *)manager {
    NSLog(@"User Did Close GTView.");
    
}

- (void)gtCaptcha:(GT3CaptchaManager *)manager didReceiveSecondaryCaptchaData:(NSData *)data response:(NSURLResponse *)response error:(GT3Error *)error decisionHandler:(void (^)(GT3SecondaryCaptchaPolicy))decisionHandler {
    if (!error) {
        //处理你的验证结果
        NSLog(@"\ndata: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        //成功请调用decisionHandler(GT3SecondaryCaptchaPolicyAllow)
        decisionHandler(GT3SecondaryCaptchaPolicyAllow);
        //失败请调用decisionHandler(GT3SecondaryCaptchaPolicyForbidden)
        //decisionHandler(GT3SecondaryCaptchaPolicyForbidden);
        
        
    }
    else {
        //二次验证发生错误
        decisionHandler(GT3SecondaryCaptchaPolicyForbidden);
        //        [TipsView showTipOnKeyWindow:error.error_code fontSize:12.0];
    }
}

- (void)gtCaptcha:(GT3CaptchaManager *)manager didReceiveCaptchaCode:(NSString *)code result:(NSDictionary *)result message:(NSString *)message {
    if ([message isEqualToString:@"Success"]) {
        NSMutableDictionary *paremers = [[NSMutableDictionary alloc] initWithDictionary:self.gt3Paramers];
        [paremers addEntriesFromDictionary:result];
        self.gt3Paramers = paremers;
        if(![self.gt3Paramers[@"geetest_challenge"] length] ||
           ![self.gt3Paramers[@"geetest_validate"] length] ||
           ![self.gt3Paramers[@"geetest_seccode"] length]){
            [SVProgressHUD showErrorWithStatus:@"ErrorMsg,图形验证码异常，请联系管理员"];
            self.gt3ResultBlock ? self.gt3ResultBlock(nil) : nil;
            return;
            
        }
        self.gt3ResultBlock ? self.gt3ResultBlock([self.gt3Paramers entriesForKeys:@[@"geetest_challenge",
                                                                                     @"geetest_seccode",
                                                                                     @"geetest_validate",]]) : nil;
    }else{
        self.gt3ResultBlock ? self.gt3ResultBlock(nil) : nil;
    }
}



/** 处理API1返回的数据并将验证初始化数据解析给管理器
 - (NSDictionary *)gtCaptcha:(GT3CaptchaManager *)manager didReceiveDataFromAPI1:(NSDictionary *)dictionary withError:(GT3Error *)error {
 
 }
 */

/** 修改API1的请求 */
- (void)gtCaptcha:(GT3CaptchaManager *)manager willSendRequestAPI1:(NSURLRequest *)originalRequest withReplacedHandler:(void (^)(NSURLRequest *))replacedHandler {
    NSMutableURLRequest *mRequest = [originalRequest mutableCopy];
    NSString *newURL = [NSString stringWithFormat:@"%@?t=%.0f", originalRequest.URL.absoluteString, [[[NSDate alloc] init]timeIntervalSince1970]];
    mRequest.URL = [NSURL URLWithString:newURL];
    
    replacedHandler(mRequest);
}
@end
