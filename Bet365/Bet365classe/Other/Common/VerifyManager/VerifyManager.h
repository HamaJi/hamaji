//
//  VerifyManager.h
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/12/16.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, VerifyAuthorType) {
    VerifyAuthorType_NONE = 0,
    VerifyAuthorType_UNVERIFY,
    VerifyAuthorType_IMAGE,
    VerifyAuthorType_TENCENT,
    VerifyAuthorType_GT3,
};

NS_ASSUME_NONNULL_BEGIN
@class VerifyManager;

@protocol VerifyManagerDelegate <NSObject>

-(void)verifyManager:(VerifyManager *)manager updateVerifyAuthorType:(VerifyAuthorType)type;

-(void)verifyManager:(VerifyManager *)manager verificationSuccessParamers:(NSDictionary *)paramers authorType:(VerifyAuthorType)type;

-(void)verifyManager:(VerifyManager *)manager verificationFaildAuthorType:(VerifyAuthorType)type;

@end

@interface VerifyManager : NSObject

@property (nonatomic,strong)NSString *requestJsonString;

@property (weak,nonatomic) id<VerifyManagerDelegate> delegate;

-(void)launchVerify;

//如果是平台图形验证码，调用launchVerify()是否直接唤起图形验证码弹窗，因为登录和注册还是用自己的图形验证码输入框
@property (nonatomic,assign) BOOL launchImageCode;

@end

NS_ASSUME_NONNULL_END
