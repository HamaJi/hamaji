//
//  ChoosePrivateChatCell.h
//  Bet365
//
//  Created by adnin on 2019/2/16.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChoosePrivateChatCell;

@protocol ChoosePrivateChatCellDelegate <NSObject>

-(void)tapPrivateSubmitChoosePrivateCell:(ChoosePrivateChatCell *)cell;

@end

@interface ChoosePrivateChatCell : UITableViewCell

@property (nonatomic,weak)ChatUserInfoModel *model;

@property (nonatomic,weak)ChatUserInfoEntity *entity;

@property (nonatomic) NSUInteger unread;

@property (nonatomic,weak)id<ChoosePrivateChatCellDelegate> delegate;
@end
