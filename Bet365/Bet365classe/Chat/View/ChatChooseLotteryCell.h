//
//  ChatChooseLotteryCell.h
//  Bet365
//
//  Created by HHH on 2018/10/19.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KFCPHomeGameJsonModel.h"
@interface ChatChooseLotteryCell : UICollectionViewCell
@property (nonatomic)KFCPHomeGameJsonModel *model;
@end
