//
//  ChatRedResultsCell.m
//  Bet365
//
//  Created by HHH on 2018/11/22.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatRedResultsCell.h"
@interface ChatRedResultsCell()
@property (strong, nonatomic) IBOutlet UIImageView *userIconView;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *moneyLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;


@end
@implementation ChatRedResultsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setDetailModel:(ChatRedPacketDetailModel *)detailModel{
    _detailModel = detailModel;
    [self.userIconView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SerVer_Url,detailModel.avator]]
                         placeholderImage:[UIImage imageNamed:@"chat_userAvator_placeHolder"]];
    self.userNameLabel.text = detailModel.nickName;
    self.moneyLabel.text = [NSString stringWithFormat:@"%.2f元",[detailModel.money floatValue]];
    self.timeLabel.text = detailModel.settlementCreatetime;
}
@end
