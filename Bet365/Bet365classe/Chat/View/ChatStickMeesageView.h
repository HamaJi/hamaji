//
//  ChatStickMeesageView.h
//  Bet365
//
//  Created by HHH on 2018/12/5.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ChatStickIgnorBlock)(Message *message);

@interface ChatStickMeesageView : UIView

@property (nonatomic,strong)Message *message;

@property (nonatomic,assign,readonly) BOOL isShow;

@property (nonatomic,copy) ChatStickIgnorBlock closeHandle;

@property (nonatomic,copy) ChatStickIgnorBlock jumpHandle;
@end
