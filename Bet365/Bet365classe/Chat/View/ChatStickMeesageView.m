//
//  ChatStickMeesageView.m
//  Bet365
//
//  Created by HHH on 2018/12/5.
//  Copyright © 2018年 jesse. All rights reserved.
//
#import "ChatBubbleView.h"
#import "ChatStickMeesageView.h"
#import <POP/POP.h>
#import "aji_horCircleView.h"



@interface ChatStickMeesageView ()
<aji_horCircleViewDelegate>
@property (strong, nonatomic) IBOutlet aji_horCircleView *horCircleView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@end

@implementation ChatStickMeesageView
-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.horCircleView.delegate = self;
        });
    }
    return self;
}
#pragma mark - aji_horCircleViewDelegate
-(void)HorCircleView:(aji_horCircleView *)view TapUpAtIndex:(NSUInteger)idx{
    (self.jumpHandle && self.message) ? self.jumpHandle(self.message) : nil;
}

-(void)HorCircleView:(aji_horCircleView *)view MoveCompletedWasIndex:(NSUInteger)idx{
    if (self.message) {
        ChatUserInfoEntity *entity = [ChatUtilitySubscribeData getUserInfoListEntityByUserId:[self.message.userId integerValue]].firstObject;
        self.nameLabel.text = entity.nickName;
    }
}
    
#pragma mark - GET/SET
-(void)setMessage:(Message *)message{
    _message = message;
    if (!_message) {
        [self.horCircleView stopAnimation];
        return;
    }
    if (_message.chatType == ChatMessageType_TEXT) {
        self.horCircleView.list = @[message.content.attributedText];
    }else if (_message.chatType == ChatMessageType_IMG){
        NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:@"[图片]"];
        [attStr setAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor],
                                NSFontAttributeName : [UIFont systemFontOfSize:15]} range:NSMakeRange(0,@"[图片]".length)];
        attStr ? (self.horCircleView.list = @[attStr]) : nil;
    }else if (_message.chatType == ChatMessageType_RED){
        NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:@"[红包]"];
        [attStr setAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor],
                                NSFontAttributeName : [UIFont systemFontOfSize:15]} range:NSMakeRange(0,@"[红包]".length)];
        attStr ? (self.horCircleView.list = @[attStr]) : nil;
    }
    [self layoutIfNeeded];
    [self.horCircleView startAnimation];
}
- (IBAction)tapCloseAction:(UIButton *)sender {
    (self.closeHandle && self.message) ? self.closeHandle(self.message) : nil;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
