//
//  ChatJoinRoomBubbleTools.m
//  Bet365
//
//  Created by adnin on 2019/4/17.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "ChatJoinRoomBubbleTools.h"
@interface ChatJoinRoomBubbleTools()<CAAnimationDelegate>

@property (nonatomic,weak)CALayer *superLayer;

@property (nonatomic,strong)NSMutableArray <CALayer *>*layerList;

@end

@implementation ChatJoinRoomBubbleTools

-(void)dealloc{
    [self removeAllBubbleString];
}

#pragma mark - Public
-(instancetype)initWithSupLayer:(CALayer *)supLayer{
    if (!supLayer) {
        return nil;
    }
    if (self = [super init]) {
        self.superLayer = supLayer;
    }
    return self;
}

-(void)addBubbleString:(NSString *)string{
    if (!self.superLayer) {
        return;
    }
    [self removeFirstLayer];
    CATextLayer *layer = [self getLayer:string];
    [self generateBubbleByLayer:layer];
    [self.superLayer addSublayer:layer];
}



-(void)removeAllBubbleString{
    [self.layerList enumerateObjectsUsingBlock:^(CALayer * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeAllAnimations];
        [obj removeFromSuperlayer];
    }];
    [self.layerList removeAllObjects];
}

#pragma mark - Private
-(void)removeFirstLayer{
    if (self.layerList.count < self.maxCount) {
        return;
    }
    CALayer *layer =  self.layerList.firstObject;
    [layer removeAllAnimations];
    [layer removeFromSuperlayer];
    [self.layerList removeObject:layer];
}
-(CATextLayer *)getLayer:(NSString *)layerString{
    CATextLayer * layer = [CATextLayer layer];
    layer.anchorPoint = CGPointMake(0.0, 0.0);
    layer.frame = CGRectMake(self.startPosition.x, self.startPosition.y, [layerString widthForFont:[UIFont fontWithName:@"HiraKakuProN-W3" size:self.fontSize]] + 8, self.fontSize + 5);
    layer.string = layerString;
    layer.font = (__bridge CFTypeRef _Nullable)(@"HiraKakuProN-W3");
    layer.fontSize = self.fontSize;
    layer.alignmentMode = kCAAlignmentCenter;
    layer.foregroundColor = self.fontColor.CGColor;
    layer.backgroundColor = self.shadowColor.CGColor;
    
    layer.cornerRadius = 5;
    return layer;
}

-(void)generateBubbleByLayer:(CALayer*)layer{

    CGPoint endPoint = CGPointMake(self.startPosition.x, self.startPosition.y - self.maxAnimationHeight);
    CGMutablePathRef curvedPath = CGPathCreateMutable();
    CGPathMoveToPoint(curvedPath, NULL, self.startPosition.x, self.startPosition.y);
    CGPathAddCurveToPoint(curvedPath, NULL, self.startPosition.x, self.startPosition.y, endPoint.x, endPoint.y, endPoint.x, endPoint.y);
    UIBezierPath *path = [UIBezierPath bezierPathWithCGPath:curvedPath];

    CAKeyframeAnimation *keyFrame = [CAKeyframeAnimation animation];
    keyFrame.keyPath = @"position";
    keyFrame.path = path.CGPath;
    keyFrame.duration = self.duration;
    keyFrame.calculationMode = kCAAnimationPaced;
    
    [layer addAnimation:keyFrame forKey:@"keyframe"];
    
    
    CABasicAnimation *scale = [CABasicAnimation animation];
    scale.keyPath = @"transform.scale";
    scale.toValue = @1;
    scale.fromValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 0.1)];
    scale.duration = 0.5;
    
    CABasicAnimation *alpha = [CABasicAnimation animation];
    alpha.keyPath = @"opacity";
    alpha.fromValue = @1.0;
    alpha.toValue = @0.1;
    alpha.duration = self.duration * 0.4;
    alpha.beginTime = self.duration - alpha.duration;
    
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.animations = @[keyFrame, scale, alpha];
    group.duration = self.duration;
    group.delegate = self;
    group.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    group.fillMode = kCAFillModeForwards;
    group.removedOnCompletion = NO;
    [layer addAnimation:group forKey:@"group"];
    
    [self.layerList addObject:layer];
}

-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    if (flag){
        CALayer *layer = [self.layerList firstObject];
        [layer removeAllAnimations];
        [layer removeFromSuperlayer];
        [self.layerList removeObject:layer];
    }
}

#pragma mark - GET/SET
-(NSMutableArray <CALayer *>*)layerList{
    if (!_layerList) {
        _layerList = @[].mutableCopy;
    }
    return _layerList;
}

-(NSUInteger)maxCount{
    if (!_maxCount) {
        _maxCount = 5;
    }
    return _maxCount;
}
@end
