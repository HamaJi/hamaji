//
//  ChatChooseLotteryCell.m
//  Bet365
//
//  Created by HHH on 2018/10/19.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatChooseLotteryCell.h"
@interface ChatChooseLotteryCell()
@property (strong, nonatomic) IBOutlet UIImageView *iconImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

@end
@implementation ChatChooseLotteryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setModel:(KFCPHomeGameJsonModel *)model{
    if (!model) {
        self.nameLabel.text = @"";
        [self.iconImageView setImage:nil];
    }else{
        self.nameLabel.text = model.name;
        [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:jesse(@"/images/gameLogo/%@.png"),model.GameId]] placeholderImage:nil];
    }
}

@end
