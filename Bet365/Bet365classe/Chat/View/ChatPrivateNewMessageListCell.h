//
//  ChatPrivateNewMessageListCell.h
//  Bet365
//
//  Created by adnin on 2019/7/29.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class ChatPrivateNewMessageListCell;

@protocol ChatPrivateNewMessageListCellDelegate <NSObject>

-(void)chatPrivateNewMessageListCell:(ChatPrivateNewMessageListCell *)cell removeUserInfo:(ChatUserInfoModel *)userInfo;

@end

@interface ChatPrivateNewMessageListCell : UITableViewCell

@property (nonatomic,weak) ChatUserInfoModel *infoModel;

@property (weak,nonatomic) id<ChatPrivateNewMessageListCellDelegate> delegate;

@property (nonatomic) BOOL isShowClose;

@end

NS_ASSUME_NONNULL_END
