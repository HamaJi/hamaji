//
//  ChatTopListCell.m
//  Bet365
//
//  Created by HHH on 2018/12/4.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatTopListCell.h"
#import <POP/POP.h>
@interface ChatTopListCell()
@property (strong, nonatomic) IBOutlet UIButton *rankBtn;

@property (strong, nonatomic) IBOutlet UIImageView *iconImageView;

@property (strong, nonatomic) IBOutlet UILabel *namelabel;

@property (strong, nonatomic) IBOutlet UILabel *profitLabel;

@property (strong, nonatomic) IBOutlet UIView *animationView;


@property (strong, nonatomic) IBOutlet UIButton *followBtn;

@end
@implementation ChatTopListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.followBtn setBackgroundImage:[UIImage imageWithColor:COLOR_WITH_HEX(0x377D22)] forState:UIControlStateSelected];
    [self.followBtn setBackgroundImage:[UIImage imageWithColor:COLOR_WITH_HEX(0xD92E2F)] forState:UIControlStateNormal];
    // Initialization code
}


-(void)setModel:(ChatTopListModel *)model{
    _model = model;
    self.namelabel.text = [NSString stringWithFormat:@"昵称:%@",self.model.nickName];
    self.profitLabel.text = [NSString stringWithFormat:@"盈利:%.0f",[self.model.winMoney floatValue]];
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SerVer_Url,self.model.avatar]] placeholderImage:[UIImage imageNamed:@"chat_userAvator_placeHolder"]];
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(1,1)];
    scaleAnimation.velocity = [NSValue valueWithCGPoint:CGPointMake(2,2)];
    scaleAnimation.springBounciness = 20.0f;
    [self.animationView pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}

-(void)setEntity:(ChatFollowEntity *)entity{
    _entity = entity;
    [self.followBtn setSelected:_entity != nil];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (IBAction)followAction:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatTopListCell:TapFollowAt:)]) {
        [self.followBtn setSelected:[self.delegate ChatTopListCell:self TapFollowAt:self.model]];
    }
}

-(void)setRankIdx:(NSInteger)rankIdx{
    if (rankIdx > 2) {
        [self.rankBtn setImage:nil forState:UIControlStateNormal];
        [self.rankBtn setTitle:[NSString stringWithFormat:@"%li",rankIdx + 1] forState:UIControlStateNormal];
    }else{
        [self.rankBtn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"chat_toplist_%li",rankIdx]] forState:UIControlStateNormal];
        [self.rankBtn setTitle:@"" forState:UIControlStateNormal];
    }
    [self.contentView layoutIfNeeded];
}
@end
