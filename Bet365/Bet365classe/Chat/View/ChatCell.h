//
//  ChatCell.h
//  Bet365
//
//  Created by HHH on 2018/9/23.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatCellDelegate.h"
#import "Message.h"
@interface ChatCell : UITableViewCell

@property (nonatomic,weak)Message *message;

@property (nonatomic,weak)id<ChatCellDelegate> delegate;

@property (nonatomic,weak)ChatUserInfoEntity *userInfoEntity;

-(void)updateData;
@end
