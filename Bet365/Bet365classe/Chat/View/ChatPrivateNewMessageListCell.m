//
//  ChatPrivateNewMessageListCell.m
//  Bet365
//
//  Created by adnin on 2019/7/29.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "ChatPrivateNewMessageListCell.h"

@interface ChatPrivateNewMessageListCell()

@property (weak, nonatomic) IBOutlet UIImageView *avatorImageView;

@property (weak, nonatomic) IBOutlet UIImageView *levelImg;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@end

@implementation ChatPrivateNewMessageListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (IBAction)removeAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(chatPrivateNewMessageListCell:removeUserInfo:)]) {
        [self.delegate chatPrivateNewMessageListCell:self removeUserInfo:self.infoModel];
    }
}

-(void)setIsShowClose:(BOOL)isShowClose{
    self.closeBtn.hidden = !isShowClose;
}

-(void)setInfoModel:(ChatUserInfoModel *)infoModel{
    _infoModel = infoModel;
    self.nameLabel.text = _infoModel.nickName;
    NSInteger level = 0;
    if ([_infoModel.level integerValue] > 6 && [_infoModel.level integerValue] <= 100) {
        level = 6;
    }else{
        level = [_infoModel.level integerValue];
    }
    NSString *imgPatch = [NSString stringWithFormat:@"chat_level_%li",level];
    [self.levelImg setImage:[UIImage imageNamed:imgPatch]];
    
    [self.avatorImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SerVer_Url,_infoModel.avatar]]];
}
@end
