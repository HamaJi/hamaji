//
//  ChatTitleView.h
//  Bet365
//
//  Created by adnin on 2019/2/13.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ChatTitleView;

@protocol ChatTitleViewDelegate <NSObject>

@required

-(void)tapUpInsideChatTitleView:(ChatTitleView *)titleView;

@end

@interface ChatTitleView : UIView

@property (nonatomic)NSString *title;

@property (nonatomic)NSInteger count;

@property (nonatomic,assign)BOOL isPrivateChat;

@property (nonatomic,weak) id<ChatTitleViewDelegate> delegate;
@end
