//
//  ChatChooseAvatorCell.m
//  Bet365
//
//  Created by HHH on 2018/10/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatChooseAvatorCell.h"
@interface ChatChooseAvatorCell()
@property (strong, nonatomic) IBOutlet UIImageView *AvatorImg;
@property (weak, nonatomic) IBOutlet UILabel *titileLabel;

@end
@implementation ChatChooseAvatorCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setUrl:(NSURL *)url{
    [self.AvatorImg sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"chat_userAvator_placeHolder"]];
}

-(void)setPath:(NSString *)path{
    [self.AvatorImg setImage:[UIImage imageNamed:path]];
}

-(void)setTitileString:(NSString *)titileString{
    self.titileLabel.text = titileString ? titileString : @"";
}

@end
