//
//  ChatLotteryTopView.m
//  Bet365
//
//  Created by HHH on 2018/9/18.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatLotteryTopView.h"

#define LHC_K3_WIDTHHEIGHT (IS_IPHONE_5s ? 14.0 : 16.0)
#define OTHER_WIDTHHEIGHT  (IS_IPHONE_5s ? 14.0 : 16.0)
#define kFontSize (IS_IPHONE_5s ? 9 : 11)
@interface NSArray (ChatLotteryTopView)


@end

@implementation NSArray (ChatLotteryTopView)

-(NSInteger)plusAllObject{
    __block NSInteger sum = 0;
    [self enumerateObjectsUsingBlock:^(NSString *  _Nonnull str, NSUInteger idx, BOOL * _Nonnull stop) {
        sum += [str integerValue];
    }];
    return sum;
}

@end

@interface ChatLotteryTopView()

@property (strong, nonatomic) IBOutlet UILabel *titileLabel;

@property (weak, nonatomic) IBOutlet UILabel *timeLeadingLabel;

@property (weak, nonatomic) IBOutlet UILabel *curLeadingLabel;

@property (strong, nonatomic) IBOutlet UILabel *perLabel;

@property (strong, nonatomic) IBOutlet UILabel *curLabel;

@property (strong, nonatomic) IBOutlet UILabel *timeLabel;

@property (nonatomic) NSString *differenceTimeStampString;

/**
 彩票开奖任务
 */
@property (nonatomic,strong)NSURLSessionTask *requestTask;

@property (nonatomic,assign)BOOL isUpdatingLotteryOpenInfo;

@property (nonatomic,assign)int retryTimesOfLotteryOpenInfoUpdate;

@end
@implementation ChatLotteryTopView
-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setUI];
            [self addObserver];
        });
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setUI];
            [self addObserver];
        });
    }
    return self;
}

-(void)updateLotteryOpenInfoModelIfNeeded{
    NSTimeInterval timeStamp = differenceTimeStamp(self.model);
    if (timeStamp <= 0 && !self.isUpdatingLotteryOpenInfo && [self.model.cur.turn isNoBlankString] && !((int)self.model.cur.status)) {
        [self updateLotteryOpenInfoModel];
    }
}

-(void)updateLotteryOpenInfoModel{
    if (self.lotteryId.length && !self.isUpdatingLotteryOpenInfo) {
        self.isUpdatingLotteryOpenInfo = YES;
//        [self.requestTask cancel];
        self.requestTask = [CHAT_UTIL getOpenInfoByLotteryID:self.lotteryId Completed:^(LotteryOpenInfoModel *model) {
            if (model) {
                if([model.cur.turn isNoBlankString] && [model.pre.turn isNoBlankString] && !((int)model.cur.status) && model.cur.turn.intValue - model.pre.turn.intValue != 1){
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        self.isUpdatingLotteryOpenInfo = NO;
                        self.retryTimesOfLotteryOpenInfoUpdate++; if(self.retryTimesOfLotteryOpenInfoUpdate > 10){
                             self.retryTimesOfLotteryOpenInfoUpdate = 0;
                            self.model = model;
                            if (self.delegate && [self.delegate respondsToSelector:@selector(replaceLotteryOpenInfoModel:)]) {
                                [self.delegate replaceLotteryOpenInfoModel:_model];
                            }
                             return;
                         }
                         [self updateLotteryOpenInfoModel];
                    });
                    
                    return;
                }
                self.retryTimesOfLotteryOpenInfoUpdate = 0;
                
                self.model = model;
                if (self.delegate && [self.delegate respondsToSelector:@selector(replaceLotteryOpenInfoModel:)]) {
                    [self.delegate replaceLotteryOpenInfoModel:_model];
                }
            }
            self.isUpdatingLotteryOpenInfo = NO;
        }];
    }
}


-(void)setUI{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.titileLabel.textColor = [UIColor skinTextItemNorColor];
        
        self.curLeadingLabel.textColor = [UIColor skinTextItemNorSubColor];
        self.timeLeadingLabel.textColor = [UIColor skinTextItemNorSubColor];
        self.perLabel.textColor = [UIColor skinTextItemNorSubColor];
        
        self.curLabel.textColor = [UIColor skinTextItemSelSubColor];
        self.timeLabel.textColor = [UIColor skinTextItemSelSubColor];

        self.backgroundColor = [UIColor skinViewBgColor];
        self.iconImageView.layer.cornerRadius = self.iconImageView.ycz_width / 2.0;
        self.layer.shadowOffset = CGSizeMake(1, 1);
        self.layer.shadowOpacity = 0.8;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
    });
}
-(void)addObserver{
    
    self.ballContentView.userInteractionEnabled = YES;
    @weakify(self);
    [self.ballContentView addTapGestureRecognizer:^(UITapGestureRecognizer *recognizer, NSString *gestureId) {
        @strongify(self);
        if (self.delegate && [self.delegate respondsToSelector:@selector(tapBallContentAtChatLotteryTopView:)]) {
            [self.delegate tapBallContentAtChatLotteryTopView:self];
        }
    }];
    
    self.iconImageView.userInteractionEnabled = YES;
    [self.iconImageView addTapGestureRecognizer:^(UITapGestureRecognizer *recognizer, NSString *gestureId) {
        @strongify(self);
        if (self.delegate && [self.delegate respondsToSelector:@selector(tapIconImageViewAtChatLotteryTopView:)]) {
            [self.delegate tapIconImageViewAtChatLotteryTopView:self];
        }
    }];
    
    [[[RACSignal interval:0.1 onScheduler:[RACScheduler mainThreadScheduler]] takeUntil:self.rac_willDeallocSignal ] subscribeNext:^(id x) {
        @strongify(self);
        if (self.isFirsResponse) {
            self.timeLabel.text = self.differenceTimeStampString;
            [self updateLotteryOpenInfoModelIfNeeded];
        }
    }];
    
    [[[RACSignal interval:3 onScheduler:[RACScheduler mainThreadScheduler]] takeUntil:self.rac_willDeallocSignal ] subscribeNext:^(id x) {
        
        @strongify(self);
        if (self.isFirsResponse) {
//            [self updateLotteryOpenInfoModel];
        }
        
    }];

}

-(void)setModel:(LotteryOpenInfoModel *)model{
    
    _model = model;
    if (_model == nil) {
        return;
    }
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:jesse(@"/images/gameLogo/%@.png"),model.lotteryID]]];
    self.perLabel.text = [NSString stringWithFormat:@"第%@期",model.pre.turnNum];
     self.curLabel.text = model.cur.turnNum;
    
    [LOTTERY_FACTORY.lotteryData enumerateObjectsUsingBlock:^(KFCPHomeGameJsonModel  *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.GameId isEqualToString:model.lotteryID]) {
            self.titileLabel.text = obj.name;
            [self setTypeWithType:obj.type openNum:model.pre.openNumList];
            *stop = YES;
        }else{
            self.titileLabel.text = model.lotteryID;
        }
    }];
    
    
}

- (void)setTypeWithType:(NSString *)type openNum:(NSArray *)openNumArr{
    [self.ballContentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    if (!openNumArr.count) {
        return;
    }
    UIView *contentView = [[UIView alloc] init];
    if ([type isEqualToString:@"lhc"]) {
        UIImageView *lastOpenImage = nil;
        for (int i = 0; i < openNumArr.count; i ++) {
            UILabel *plusLb = nil;
            if (i == openNumArr.count - 1) {
                plusLb = [[UILabel alloc] init];
                plusLb.text = @"+";
                plusLb.textColor = [UIColor redColor];
                [contentView addSubview:plusLb];
                [plusLb mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.equalTo(lastOpenImage);
                    make.leading.equalTo(lastOpenImage.mas_trailing).offset(5);
                }];
            }
            UIImageView *openImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:StringFormatWithStr(@"ball",openNumArr[i])]];
            [contentView addSubview:openImage];
            [openImage mas_makeConstraints:^(MASConstraintMaker *make) {
                if (plusLb) {
                    make.leading.equalTo(plusLb.mas_trailing).offset(5);
                }else if (lastOpenImage) {
                    make.leading.equalTo(lastOpenImage.mas_trailing).offset(2);
                }else{
                    make.leading.equalTo(contentView);
                }
                make.top.equalTo(contentView).offset(0);
                make.size.mas_equalTo(CGSizeMake(LHC_K3_WIDTHHEIGHT, LHC_K3_WIDTHHEIGHT));
            }];
            
            Solar *solar = [[Solar alloc] init];
            solar.solarYear = self.model.pre.openTime.year;
            solar.solarMonth = self.model.pre.openTime.month;
            solar.solarDay = self.model.pre.openTime.day;
            
            NSString *zodName = [Bet365Tool getZodNameForLHCByOpenTimeSolar:solar Num:[openNumArr[i] integerValue]];
            
            UILabel *ZodiacLb = [UILabel addLabelWithFont:(kFontSize) color:[UIColor redColor] title:zodName];
            
            [contentView addSubview:ZodiacLb];
            [ZodiacLb mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(openImage);
                make.top.equalTo(openImage.mas_bottom).offset(5);
                make.bottom.equalTo(contentView).offset(0);
            }];
            lastOpenImage = openImage;
        }
    }else if ([type isEqualToString:@"pcdd"]) {
        NSString *plusStr = [openNumArr componentsJoinedByString:@" + "];
        UILabel *pcddLb = [UILabel addLabelWithFont:kFontSize color:JesseColor(196, 7, 26) title:[NSString stringWithFormat:@"%@ = %ld",plusStr,[openNumArr plusAllObject]]];
        [contentView addSubview:pcddLb];
        [pcddLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(contentView);
            make.top.equalTo(contentView).offset(0);
            make.bottom.equalTo(contentView).offset(0);
        }];
    }else if ([type isEqualToString:@"k3"]) {
        UIImageView *lastOpenImage = nil;
        for (int i = 0; i < openNumArr.count; i ++) {
            UIImageView *openImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"k3_%@",openNumArr[i]]]];
            [contentView addSubview:openImage];
            [openImage mas_makeConstraints:^(MASConstraintMaker *make) {
                if (lastOpenImage) {
                    make.leading.equalTo(lastOpenImage.mas_trailing).offset(5);
                }else{
                    make.leading.equalTo(contentView);
                }
                make.top.equalTo(contentView).offset(0);
                make.size.mas_equalTo(CGSizeMake(LHC_K3_WIDTHHEIGHT, LHC_K3_WIDTHHEIGHT));
                make.bottom.equalTo(contentView).offset(0);
            }];
            lastOpenImage = openImage;
        }
        NSString *daxiao = [openNumArr plusAllObject] >= 11 ? @"大" : @"小";
        NSString *danshuang = [openNumArr plusAllObject] % 2 == 0 ? @"双" : @"单";
        UILabel *plusLb = [UILabel addLabelWithFont:kFontSize color:JesseColor(196, 7, 26) title:[NSString stringWithFormat:@"%li",[openNumArr plusAllObject]]];
        plusLb.textAlignment = NSTextAlignmentCenter;
        plusLb.cornerRadius = 4;
        plusLb.borderWidth = 1;
        plusLb.borderColor = JesseGrayColor(210);
        UILabel *daxiaoLb = [UILabel addLabelWithFont:kFontSize color:JesseColor(196, 7, 26) title:daxiao];
        daxiaoLb.textAlignment = NSTextAlignmentCenter;
        daxiaoLb.cornerRadius = 4;
        daxiaoLb.borderWidth = 1;
        daxiaoLb.borderColor = JesseGrayColor(210);
        UILabel *danshuangLb = [UILabel addLabelWithFont:kFontSize color:JesseColor(196, 7, 26) title:danshuang];
        danshuangLb.textAlignment = NSTextAlignmentCenter;
        danshuangLb.cornerRadius = 4;
        danshuangLb.borderWidth = 1;
        danshuangLb.borderColor = JesseGrayColor(210);
        [contentView addSubview:plusLb];
        [contentView addSubview:daxiaoLb];
        [contentView addSubview:danshuangLb];
        [plusLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(lastOpenImage);
            make.leading.equalTo(lastOpenImage.mas_trailing).offset(15);
            make.size.mas_equalTo(CGSizeMake(LHC_K3_WIDTHHEIGHT, LHC_K3_WIDTHHEIGHT));
        }];
        [daxiaoLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(lastOpenImage);
            make.leading.equalTo(plusLb.mas_trailing).offset(3);
            make.size.mas_equalTo(CGSizeMake(LHC_K3_WIDTHHEIGHT, LHC_K3_WIDTHHEIGHT));
        }];
        [danshuangLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(lastOpenImage);
            make.leading.equalTo(daxiaoLb.mas_trailing).offset(3);
            make.size.mas_equalTo(CGSizeMake(LHC_K3_WIDTHHEIGHT, LHC_K3_WIDTHHEIGHT));
        }];
    }else{
        UILabel *lastLb = nil;
        CGFloat maxW = self.bounds.size.width - self.iconImageView.ycz_x - self.iconImageView.ycz_width - 30;
        NSInteger rows = maxW / (LHC_K3_WIDTHHEIGHT + 2);
        for (int i = 0; i < openNumArr.count; i ++) {
            UILabel *lb = [UILabel addLabelWithFont:kFontSize color:[UIColor whiteColor] title:StringFormatWithString(openNumArr[i])];
            lb.textAlignment = NSTextAlignmentCenter;
            lb.backgroundColor = JesseColor(196, 7, 26);
            if ([type isEqualToString:@"pk10"]) {
                NSInteger idx = ([[openNumArr safeObjectAtIndex:i] integerValue]);
                lb.backgroundColor = tenReplaceColor(idx);
            }
            lb.cornerRadius = LHC_K3_WIDTHHEIGHT * 0.5;
            [contentView addSubview:lb];
            [lb mas_makeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(LHC_K3_WIDTHHEIGHT, LHC_K3_WIDTHHEIGHT));
                if (lastLb) {
                    if (i % rows == 0) {
                        make.top.equalTo(lastLb.mas_bottom).offset(2);
                        make.leading.equalTo(contentView);
                    }else{
                        make.top.equalTo(lastLb.mas_top);
                        make.leading.equalTo(lastLb.mas_trailing).offset(2);
                    }
                }else{
                    make.top.equalTo(contentView).offset(10);
                    make.leading.equalTo(contentView);
                }
            }];
            lastLb = lb;
        }
        NSString *daxiao = nil;
        NSString *danshuang = nil;
        NSString *dragon = nil;
        if ([type isEqualToString:@"ssc"]) {
            daxiao = [openNumArr plusAllObject] >= 23 ? @"大" : @"小";
            dragon = [openNumArr[0] integerValue] > [openNumArr[4] integerValue] ? @"龙" : [openNumArr[0] integerValue] == [openNumArr[4] integerValue] ? @"和局" : @"虎";
            danshuang = [openNumArr plusAllObject] % 2 == 0 ? @"双" : @"单";
        }else if ([type isEqualToString:@"pk10"]) {
            NSInteger num = [[openNumArr safeObjectAtIndex:0] integerValue] + [[openNumArr safeObjectAtIndex:1] integerValue];
            daxiao = num > 11 ? @"大" : @"小";
            dragon = nil;
            danshuang = num % 2 == 0 ? @"双" : @"单";
        }else if ([type isEqualToString:@"11x5"]) {
            daxiao = [openNumArr plusAllObject] > 30 ? @"大" : [openNumArr plusAllObject] == 30 ? @"和" : @"小";
            dragon = nil;
            danshuang = [openNumArr plusAllObject] % 2 == 0 ? @"双" : @"单";
        }else if ([type isEqualToString:@"gdklsf"]) {
            daxiao = [openNumArr plusAllObject] > 84 ? @"总大" : [openNumArr plusAllObject] == 84 ? @"和" : @"总小";
            danshuang = [openNumArr plusAllObject] % 2 == 0 ? @"总双" : @"总单  ";
            dragon = [openNumArr[0] integerValue] > [openNumArr[7] integerValue] ? @"龙" : [openNumArr[0] integerValue] == [openNumArr[7] integerValue] ? @"和局" : @"虎";
        }else if ([type isEqualToString:@"bjkl8"]) {
            daxiao = [openNumArr plusAllObject] > 810 ? @"大" : [openNumArr plusAllObject] == 810 ? @"和" : @"小";
            danshuang = [openNumArr plusAllObject] % 2 == 0 ? @"总双" : @"总单  ";
            dragon = nil;
        }else if ([type isEqualToString:@"fc3d"]) {
            daxiao = [openNumArr plusAllObject] >= 14 ? @"大" : @"小";
            danshuang = [openNumArr plusAllObject] % 2 == 0 ? @"双" : @"单";
            dragon = nil;
        }else{
            daxiao = nil;
            danshuang = nil;
            dragon = nil;
        }
        UILabel *plusLb = [UILabel addLabelWithFont:kFontSize color:JesseColor(196, 7, 26) title:nil];
        plusLb.borderColor = JesseGrayColor(210);
        if ([type isEqualToString:@"ssc"]) {
            plusLb.text = danshuang;
        }else if ([type isEqualToString:@"pk10"]) {
            plusLb.text = [NSString stringWithFormat:@"%li",[[openNumArr safeObjectAtIndex:0] integerValue] + [[openNumArr safeObjectAtIndex:1] integerValue]];
        }else{
            plusLb.text = [NSString stringWithFormat:@"%li",[openNumArr plusAllObject]];
        }
        CGFloat plusWidth = [[NSString stringWithFormat:@"%li",[openNumArr plusAllObject]] stringWidthWithFont:[UIFont systemFontOfSize:IS_IPHONE_5s ? 12 : 14]];
        plusLb.textAlignment = NSTextAlignmentCenter;
        plusLb.cornerRadius = 4;
        plusLb.borderWidth = 1;
        
        UILabel *daxiaoLb = [UILabel addLabelWithFont:kFontSize color:JesseColor(196, 7, 26) title:daxiao];
        if ([type isEqualToString:@"pk10"] || [type isEqualToString:@"gdklsf"] || [type isEqualToString:@"bjkl8"]) {
            daxiaoLb.text = danshuang;
        }else{
            daxiaoLb.text = daxiao;
        }
        daxiaoLb.textAlignment = NSTextAlignmentCenter;
        daxiaoLb.cornerRadius = 4;
        daxiaoLb.borderWidth = 1;
        daxiaoLb.borderColor = JesseGrayColor(210);
        UILabel *danshuangLb = [UILabel addLabelWithFont:kFontSize color:JesseColor(196, 7, 26) title:danshuang];
        if ([type isEqualToString:@"ssc"]) {
            danshuangLb.text = dragon;
        }else if ([type isEqualToString:@"pk10"] || [type isEqualToString:@"bjkl8"] || [type isEqualToString:@"gdklsf"]) {
            danshuangLb.text = daxiao;
        }else{
            danshuangLb.text = danshuang;
        }
        danshuangLb.textAlignment = NSTextAlignmentCenter;
        danshuangLb.cornerRadius = 4;
        danshuangLb.borderWidth = 1;
        danshuangLb.borderColor = JesseGrayColor(210);
        UILabel *dragonLb = [UILabel addLabelWithFont:kFontSize color:JesseColor(196, 7, 26) title:danshuang];
        dragonLb.text = dragon;
        dragonLb.textAlignment = NSTextAlignmentCenter;
        dragonLb.cornerRadius = 4;
        dragonLb.borderWidth = 1;
        dragonLb.borderColor = JesseGrayColor(210);
        [contentView addSubview:plusLb];
        [contentView addSubview:daxiaoLb];
        [contentView addSubview:danshuangLb];
        [contentView addSubview:dragonLb];
        [plusLb mas_makeConstraints:^(MASConstraintMaker *make) {
            if ([type isEqualToString:@"pk10"] || [type isEqualToString:@"gdklsf"] || [type isEqualToString:@"bjkl8"]) {
                make.top.equalTo(lastLb.mas_bottom).offset(3);
                make.leading.equalTo(contentView);
            }else{
                make.centerY.equalTo(lastLb);
                make.leading.equalTo(lastLb.mas_trailing).offset(2);
            }
            make.size.mas_equalTo(CGSizeMake(plusWidth > OTHER_WIDTHHEIGHT ? plusWidth : OTHER_WIDTHHEIGHT, [type isEqualToString:@"gxklsf"] ? 0 : OTHER_WIDTHHEIGHT));
            make.bottom.equalTo(contentView).offset(-4);
        }];
        [daxiaoLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(plusLb.mas_trailing).offset(2);
            make.centerY.equalTo(plusLb);
            
            make.size.mas_equalTo(CGSizeMake([daxiaoLb.text stringWidthWithFont:[UIFont systemFontOfSize:kFontSize]] + 5, [type isEqualToString:@"gxklsf"] ? 0 : OTHER_WIDTHHEIGHT));
        }];
        [danshuangLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(daxiaoLb.mas_trailing).offset(2);
            make.centerY.equalTo(plusLb);
            make.size.mas_equalTo(CGSizeMake([danshuangLb.text stringWidthWithFont:[UIFont systemFontOfSize:kFontSize]] + 5, [type isEqualToString:@"gxklsf"] ? 0 : OTHER_WIDTHHEIGHT));
        }];
        [dragonLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(danshuangLb.mas_trailing).offset(2);
            make.centerY.equalTo(plusLb);
            make.size.mas_equalTo(CGSizeMake([type isEqualToString:@"gdklsf"] ? [dragonLb.text stringWidthWithFont:[UIFont systemFontOfSize:kFontSize]] + 5 : 0, OTHER_WIDTHHEIGHT));
        }];
    }
    [self.ballContentView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.ballContentView);
        make.centerY.equalTo(self.ballContentView);
    }];
}

UIColor *tenReplaceColor(NSInteger idx){
    switch (idx) {
        case 1:
            return COLOR_WITH_HEX(0xfffc00);
        case 2:
            return COLOR_WITH_HEX(0x0054ff);
        case 3:
            return COLOR_WITH_HEX(0x001868);
        case 4:
            return COLOR_WITH_HEX(0xff5b00);
        case 5:
            return COLOR_WITH_HEX(0x00c0ff);
        case 6:
            return COLOR_WITH_HEX(0x5d06f4);
        case 7:
            return COLOR_WITH_HEX(0xb2b2b2);
        case 8:
            return COLOR_WITH_HEX(0xf70400);
        case 9:
            return COLOR_WITH_HEX(0xad0000);
        case 10:
            return COLOR_WITH_HEX(0x0ad500);
            
        default:
            return COLOR_WITH_HEX(0xb2b2b2);
            break;
    }
}

-(NSString *)differenceTimeStampString{
    NSTimeInterval timeStamp = differenceTimeStamp(self.model);
    if (timeStamp <= 0) {
        return (![self.model.cur.turn isNoBlankString] && ((int)self.model.cur.status)) ? @"已封盘" : @"开奖中";
    }
    NSInteger time = timeStamp;
    return [NSString stringWithFormat:@"%@:%@:%@",
            [NSString timeFormat:time / 3600],
            [NSString timeFormat:time % 3600/60],
            [NSString timeFormat:time % 60]];
}

NSTimeInterval differenceTimeStamp(LotteryOpenInfoModel *model){
    if (!model.pre.openTime) {
        return 0;
    }
    NSDate *date = [NSDate date];
    NSInteger differenceTime = [model.cur.closeTime timeIntervalSince1970] - [date timeIntervalSince1970] + model.serverTimeDiffence;
    return differenceTime;
}


@end
