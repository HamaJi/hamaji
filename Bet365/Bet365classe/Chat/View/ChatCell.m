//
//  ChatCell.m
//  Bet365
//
//  Created by HHH on 2018/9/23.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatCell.h"

@implementation ChatCell

- (void)awakeFromNib {
    [super awakeFromNib];
    @weakify(self);
    [[RACSignal combineLatest:@[RACObserve(self, message),RACObserve(self, userInfoEntity)] reduce:^id (Message *message,ChatUserInfoEntity *entity){
        if ([message.userId integerValue] == -1 && !entity) {
            return @(YES);
        }else if ([message.userId integerValue] == [entity.userId integerValue]) {
            return @(YES);
        }
        return @(NO);
    }] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        if ([x boolValue]){
            [self performSelector:@selector(updateData)];
        }
    }];
}
- (BOOL)canBecomeFirstResponder{
    return YES;
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    if (action == @selector(copyMessageText:) ||
        action == @selector(saveMessageImage:) ||
        action == @selector(saveMessagePopUserDatail:) ||
        action == @selector(revokeMessage:) ||
        action == @selector(followMessage:) ||
        action == @selector(jumpPrivateChat:)){
        return YES;
    }
    return NO;
}

-(void)jumpPrivateChat:(UIMenuController *)menu{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatMessageCellPrivateChat:)]) {
        [self.delegate ChatMessageCellPrivateChat:(ChatMessageCell *)self];
    }
}

- (void)copyMessageText:(UIMenuController *)menu{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatMessageCellCopyText:)]) {
        [self.delegate ChatMessageCellCopyText:(ChatMessageCell *)self];
    }
}

- (void)saveMessageImage:(UIMenuController *)menu{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatMessageCellSaveImg:)]) {
        [self.delegate ChatMessageCellSaveImg:(ChatMessageCell *)self];
    }
}

- (void)saveMessagePopUserDatail:(UIMenuController *)menu{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatMessageCellPopUserDetail:)]) {
        [self.delegate ChatMessageCellPopUserDetail:(ChatMessageCell *)self];
    }
}

- (void)revokeMessage:(UIMenuController *)menu{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatMessageCellRevokeMessage:)]) {
        [self.delegate ChatMessageCellRevokeMessage:(ChatMessageCell *)self];
    }
}

- (void)followMessage:(UIMenuController *)menu{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatMessageCellFollowMessage:)]) {
        [self.delegate ChatMessageCellFollowMessage:(ChatMessageCell *)self];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)setMessage:(Message *)message{
    _message = message;
    if (!_message.hasRead) {
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(ChatCellMessageHasRead:)]) {
            [self.delegate ChatCellMessageHasRead:self];
        }
    }
}

-(void)updateData{
    
}
@end
