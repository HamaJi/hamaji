//
//  ChatMetricsView.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2020/1/16.
//  Copyright © 2020 jesse. All rights reserved.
//

#import "ChatMetricsView.h"
#import "UIView+PanPosition.h"
#import "NetTaskStatistics.h"
#import "TimerManager.h"
@interface ChatMetricsView()
<UIViewPanPositionSerializing,
TimerManagerDelegate,
NetTaskStatisticsDelegate>

@property (nonatomic,strong) UIImageView *bgImageView;

@property (nonatomic,strong) UILabel *titileLabel;

@property (nonatomic,strong)NetTaskStatistics *statisTics;

@end

@implementation ChatMetricsView

static NSString *const ChatMetricsViewTimeCycleKey = @"ChatMetricsViewTimeCycleKey";

-(instancetype)init{
    if (self = [super init]) {
        [self setUI];
        [self addObserver];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self setUI];
        [self addObserver];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setUI];
        [self addObserver];
    }
    return self;
}

#pragma mark - Private
-(void)setUI{
    self.safePanAreaInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    self.beAllowedPanPostion = NO;
    self.backgroundColor = [UIColor clearColor];
    self.clipsToBounds = YES;
    self.layer.cornerRadius = 25.0 / 2.0;
    
    [self addSubview:self.bgImageView];
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self addSubview:self.titileLabel];
    [self.titileLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(20);
        make.right.equalTo(self).offset(-10);
        make.top.equalTo(self).offset(5);
        make.bottom.equalTo(self).offset(-5);
        make.height.mas_equalTo(15);
    }];
    
}

-(void)addObserver{
    [TIMER_MANAGER addDelegate:self];
    [TIMER_MANAGER addCycleTimerWithKey:ChatMetricsViewTimeCycleKey andReduceScope:10];
}
-(void)dealloc{
    [TIMER_MANAGER removeDelegate:self];
}
#pragma mark - UIViewPanPositionSerializing
-(void)panAtPosition:(CGPoint)position{
    
}

#pragma mark - NetTaskStatisticsDelegate
-(void)url:(NSString *)url didFinishCollectingMetrics:(NetMetricsModel *)metrics{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!metrics || metrics.duration < 1.0) {
            NSString *string = @"请求超时";
            NSMutableAttributedString *mString = [[NSMutableAttributedString alloc] initWithString:string];
            [mString addFont:[UIFont systemFontOfSize:12] substring:string];
            [mString addColor:[UIColor redColor] substring:string];
            self.titileLabel.attributedText = mString;
        }else{
            NSString *duration = [NSString stringWithFormat:@"%.0f",metrics.duration];
            NSString *string = [NSString stringWithFormat:@"ping:%@ms",duration];
            NSMutableAttributedString *mString = [[NSMutableAttributedString alloc] initWithString:string];
            [mString addFont:[UIFont systemFontOfSize:12] substring:string];
            [mString addColor:[UIColor whiteColor] substring:string];
            [mString addColor:COLOR_WITH_HEX(textHexColor(metrics.duration)) substring:duration];
            self.titileLabel.attributedText = mString;
        }
        [self layoutIfNeeded];
    });
    
    
}

#pragma mark - TimerManagerDelega
-(void)cycleTimerWithKey:(NSString *)key RestCount:(NSInteger)count{
    if ([key isEqualToString:ChatMetricsViewTimeCycleKey]) {
        if (self.url.length) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self.statisTics runTaskByRequestUrlString:self.url];
            });
        }
    }
}

#pragma mark - GET/SET
-(UIImageView *)bgImageView{
    if (!_bgImageView) {
        _bgImageView = [[UIImageView alloc] init];
        _bgImageView.backgroundColor = COLOR_WITH_HEX_ALP(0x8dd3fb, 0.5);
    }
    return _bgImageView;
}

-(UILabel *)titileLabel{
    if (!_titileLabel) {
        _titileLabel = [UILabel setAllocLabelWithText:@"" FontOfSize:14 rgbColor:0xffffff];
    }
    return _titileLabel;
}

-(void)setUrl:(NSString *)url{
    if ([_url isEqualToString:url]) {
        return;
    }
    _url = url;
}


-(NetTaskStatistics *)statisTics{
    if (!_statisTics) {
        _statisTics = [[NetTaskStatistics alloc] init];
        _statisTics.delegate = self;
    }
    return _statisTics;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
