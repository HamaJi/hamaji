//
//  ChatChooseRoomCell.h
//  Bet365
//
//  Created by HHH on 2018/9/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
/** 选中 */
typedef void(^ChatChooseRoomBlock)(ChatRoomInfoModel *model);
@interface ChatChooseRoomCell : UICollectionViewCell
@property (nonatomic,weak)ChatRoomInfoModel *roomInfoModel;
@property (nonatomic,assign) NSInteger roomId;
@property (nonatomic,copy)ChatChooseRoomBlock handle;
@end
