//
//  ChatSuspendView.m
//  Bet365
//
//  Created by HHH on 2018/11/13.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatSuspendView.h"

@interface ChatSuspendView()
@property (nonatomic,strong)UILabel *titileLabel;
@property (nonatomic,strong)UIImageView *imageView;
@end
@implementation ChatSuspendView

-(instancetype)init{
    if (self = [super init]) {
        self.userInteractionEnabled = YES;
        @weakify(self);
        [[RACSignal combineLatest:@[[RACObserve(self, titile) distinctUntilChanged],
                                    [RACObserve(self, imgUrlStr) distinctUntilChanged]] reduce:^id _Nonnull(NSString *titile,NSString *imgUrlStr){
                                        if (titile && imgUrlStr) {
                                            return @(YES);
                                        }
                                        return @(NO);
                                    }] subscribeNext:^(id  _Nullable x) {
                                        if ([x boolValue]) {
                                            @strongify(self);
                                             [self.imgUrlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                                            [self.imageView sd_setImageWithURL:[NSURL URLWithString:[self.imgUrlStr stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
                                            [self.imageView sd_setImageWithURL:[NSURL URLWithString:self.imgUrlStr] placeholderImage:[UIImage imageNamed:@""]];
                                            self.titileLabel.text = self.titile;
                                        }
                                    }];
    }
    return self;
}

-(UILabel *)titileLabel{
    if (!_titileLabel) {
        _titileLabel = [UILabel setAllocLabelWithText:@"" FontOfSize:13 rgbColor:0x000000];
        [self addSubview:_titileLabel];
        [_titileLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.bottom.right.equalTo(self);
        }];
        _titileLabel.textAlignment = NSTextAlignmentCenter;
        [_titileLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
        [_titileLabel setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
    }
    return _titileLabel;
}

-(UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        _imageView.contentMode = UIViewContentModeScaleToFill;
        [self addSubview:_imageView];
        [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.equalTo(self);
            make.bottom.equalTo(self).offset(-15);
        }];
        [_imageView setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
        [_imageView setContentHuggingPriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
    }
    return _imageView;
}
@end
