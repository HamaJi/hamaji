//
//  ChatSystemCell.m
//  Bet365
//
//  Created by HHH on 2018/9/23.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatSystemCell.h"
@interface ChatSystemCell()

@property (strong, nonatomic) IBOutlet UILabel *yuanLabel;
@property (strong, nonatomic) IBOutlet UIImageView *iconImg;
@property (strong, nonatomic) IBOutlet UILabel *redMoneyLabel;
@property (strong, nonatomic) IBOutlet UILabel *whiteLabel;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *redConstanHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *whiteContstanLeading;

@end
@implementation ChatSystemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)updateData{
    
    if (self.message.chatType == ChatMessageType_REDCOLLOAR) {
        self.redConstanHeight.constant = 15;
        self.whiteContstanLeading.constant = 5;
        if ([CHAT_UTIL.suspendModel.isShowHbMoney boolValue]) {
            self.redMoneyLabel.text = [NSString stringWithFormat:@"%.2f",[self.message.content.redMoney floatValue]];
            self.yuanLabel.text = @"元红包";
        }else{
            self.redMoneyLabel.text = @"";
            self.yuanLabel.text = @"";
        }
        self.whiteLabel.text = [NSString stringWithFormat:@"%@领取了红包",self.userInfoEntity.nickName];
    }else {
        self.redConstanHeight.constant = 0;
        self.whiteContstanLeading.constant = 0;
        self.redMoneyLabel.text = @"";
        self.yuanLabel.text = @"";
//        if (self.message.chatType == ChatMessageType_JOIN){
//            self.whiteLabel.text = [NSString stringWithFormat:@"%@  加入了群聊",self.userInfoEntity.nickName];
//        }else
        if (self.message.chatType == ChatMessageType_KICK){
            self.whiteLabel.text = [NSString stringWithFormat:@"%@  被管理员移出群聊",self.userInfoEntity.nickName];
        }else if (self.message.chatType == ChatMessageType_REMOVE){
            self.whiteLabel.text = @"您已删除了一条消息";
        }else if (self.message.chatType == ChatMessageType_SYSTEM){
            self.whiteLabel.text = self.message.content.text;
        }else if (self.message.chatType == ChatMessageType_UNSPEACK){
            self.whiteLabel.text = @"您已被管理与禁止发言";
        }else if (self.message.chatType == ChatMessageType_UNIMAGE){
            self.whiteLabel.text = @"您已被管理与禁止发送图片";
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setNeedsUpdateConstraints];
    });
}

@end
