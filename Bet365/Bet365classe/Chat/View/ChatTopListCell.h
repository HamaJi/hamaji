//
//  ChatTopListCell.h
//  Bet365
//
//  Created by HHH on 2018/12/4.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ChatTopListCell;
@protocol ChatTopListCellDelegate <NSObject>

@required

-(BOOL)ChatTopListCell:(ChatTopListCell *)cell TapFollowAt:(ChatTopListModel *)model;

@end

@interface ChatTopListCell : UITableViewCell

@property (nonatomic,weak) ChatTopListModel *model;

@property (nonatomic,weak)ChatFollowEntity *entity;

@property (nonatomic) NSInteger rankIdx;

@property (nonatomic,weak)id<ChatTopListCellDelegate> delegate;

@end
