//
//  ChatBubbleView.m
//  Bet365
//
//  Created by HHH on 2018/9/15.
//  Copyright © 2018年 jesse. All rights reserved.
//
///** 文本*/
//ChatMessageType_TEXT,
///** 图片*/
//ChatMessageType_IMG,
///** 语音*/
//ChatMessageType_VOICE,
///** 视频*/
//ChatMessageType_VIDEO,
///** 彩票*/
//ChatMessageType_LOTTERY,
///** 红包*/
//ChatMessageType_RED,
#import "ChatBubbleView.h"
#import "EmojiItem.h"
#import <YYKit/UIImage+YYAdd.h>
#import <YYKit/YYKit.h>

#define AnglerRadius 5
#define ArcRadius 7.5
#define LineHiehght 0.0

@interface ChatBubbleView()

#pragma mark - ChatMessageType_TEXT 文本
@property (nonatomic,strong)UILabel *textLabel;

#pragma mark - ChatMessageType_IMG 图片
@property (nonatomic,strong)YLImageView *imageView;

#pragma mark - ChatMessageType_RED 红包
@property (nonatomic,strong)ChatBubbleRedView *redView;

#pragma mark - ChatMessageType_LOTTERY 彩票
@property (nonatomic,strong)ChatBubbleLotteryView *lotteryView;

@end

@implementation ChatBubbleView


-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self addObserver];
        
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        
        [self addObserver];
    }
    return self;
}
#pragma mark - Events
-(void)lotterySubMitAction{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatBubbleViewDidTapLotteryButton:)]) {
        [self.delegate ChatBubbleViewDidTapLotteryButton:self.lotteryView];
    }
}
#pragma mark - Private
-(void)addObserver{
    dispatch_async(dispatch_get_main_queue(), ^{
        @weakify(self);
        [[RACObserve(self, message) map:^id _Nullable(id  _Nullable value) {
            Message *message = value;
            if (message.chatType == ChatMessageType_RED) {
                return COLOR_WITH_HEX(0xDD4236);
            }else if (message.chatType == ChatMessageType_LOTTERY && !message.content.heelNickName.length){
                return COLOR_WITH_HEX(0xFDF3F1);
            }else if (message.content.isTopping){
                return COLOR_WITH_HEX(0xF8D97E);
            }else if (message.chatFrom == ChatMessageFrom_ME){
                return COLOR_WITH_HEX(0xEC8F50);
            }else if (message.chatType == ChatMessageType_NONE ||
                      (message.chatFrom == ChatMessageFrom_SYS && [message.identity integerValue] != kGroupWelcomMsgIdentity)){
                return nil;
            }
            return COLOR_WITH_HEX(0xFDF3F1);
        }] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            if (x) {
                self.backgroundColor = x;
                [self setNeedsUpdateConstraints];
                [self setNeedsDisplay];
            }
        }];
        
        self.textLabel.userInteractionEnabled = YES;
        
        [self.textLabel addTapGestureRecognizer:^(UITapGestureRecognizer *recognizer, NSString *gestureId) {
            @strongify(self);
            if (self.delegate && [self.delegate respondsToSelector:@selector(ChatBubbleViewDidTapText:)]) {
                [self.delegate ChatBubbleViewDidTapText:self];
            }
        }];
        
        [self.textLabel addLongPressGestureRecognizer:^(UILongPressGestureRecognizer *recognizer, NSString *gestureId) {
            @strongify(self);
            if (recognizer.state == UIGestureRecognizerStateBegan) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(ChatBubbleViewDidLongTapText:)]) {
                    [self.delegate ChatBubbleViewDidLongTapText:self];
                }
            }
        }];
        
        self.imageView.userInteractionEnabled = YES;
        [self.imageView addTapGestureRecognizer:^(UITapGestureRecognizer *recognizer, NSString *gestureId) {
            @strongify(self);
            if (self.delegate && [self.delegate respondsToSelector:@selector(ChatBubbleViewDidTapImage:)]) {
                [self.delegate ChatBubbleViewDidTapImage:self];
            }
        }];
        
        [self.imageView addLongPressGestureRecognizer:^(UILongPressGestureRecognizer *recognizer, NSString *gestureId) {
            @strongify(self);
            if (recognizer.state == UIGestureRecognizerStateBegan) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(ChatBubbleViewDidTapLongImage:)]) {
                    [self.delegate ChatBubbleViewDidTapLongImage:self];
                }
            }
        }];
        
        self.redView.userInteractionEnabled = YES;
        [self.redView addTapGestureRecognizer:^(UITapGestureRecognizer *recognizer, NSString *gestureId) {
            @strongify(self);
            if (self.delegate && [self.delegate respondsToSelector:@selector(ChatBubbleViewDidTapRed:)]) {
                [self.delegate ChatBubbleViewDidTapRed:self];
            }
        }];
        
        [self.redView addLongPressGestureRecognizer:^(UILongPressGestureRecognizer *recognizer, NSString *gestureId) {
            @strongify(self);
            if (recognizer.state == UIGestureRecognizerStateBegan) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(ChatBubbleViewDidLongTapRed:)]) {
                    [self.delegate ChatBubbleViewDidLongTapRed:self];
                }
            }
        }];
        
        self.lotteryView.userInteractionEnabled = YES;
        [self.lotteryView addTapGestureRecognizer:^(UITapGestureRecognizer *recognizer, NSString *gestureId) {
            @strongify(self);
            if (self.delegate && [self.delegate respondsToSelector:@selector(ChatBubbleViewDidTapLottery:)]) {
                [self.delegate ChatBubbleViewDidTapLottery:self];
            }
        }];
        
        [self.lotteryView addLongPressGestureRecognizer:^(UILongPressGestureRecognizer *recognizer, NSString *gestureId) {
            @strongify(self);
            if (recognizer.state == UIGestureRecognizerStateBegan) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(ChatBubbleViewDidTapLongLottery:)]) {
                    [self.delegate ChatBubbleViewDidTapLongLottery:self];
                }
            }
        }];
        
        [self.lotteryView.subMitBtn addTarget:self action:@selector(lotterySubMitAction) forControlEvents:UIControlEventTouchUpInside];
        
    });
}

-(void)removeAllViewWithout:(UIView *)view{
    [[self subviews] enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (![obj isEqual:view]) {
            [obj removeFromSuperview];
        }
    }];
    if (!view.superview) {
        [self addSubview:view];
    }
}

-(CGSize)sizeThatFits:(CGSize)size{
    return [super sizeThatFits:size];
}

-(void)updateConstraints{
    [super updateConstraints];
    if (self.forceChatFrom == ChatMessageFrom_SYS) {
        [self updateConstraintsByChatFrom:self.message.chatFrom];
    }else{
        [self updateConstraintsByChatFrom:self.forceChatFrom];
    }
}

-(void)updateConstraintsByChatFrom:(ChatMessageFrom)chatFrom{
    CGFloat maxWidth = WIDTH - 2*40 - 10*3 - ArcRadius;
    if (self.message.chatType == ChatMessageType_TEXT){
        [self removeAllViewWithout:self.textLabel];
        self.textLabel.preferredMaxLayoutWidth = maxWidth;
        [self.textLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset(10);
            make.bottom.equalTo(self).offset(-10);
            make.width.mas_lessThanOrEqualTo(maxWidth);
            
            if (chatFrom == ChatMessageFrom_ME){
                make.right.equalTo(self).offset(-10 - ArcRadius);
                make.left.equalTo(self).offset(10);
            }else{
                make.left.equalTo(self).offset(10 + ArcRadius);
                make.right.equalTo(self).offset(-10);
            }
        }];
    }else if (self.message.chatType == ChatMessageType_RED) {
        [self removeAllViewWithout:self.redView];
        [self.redView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(self);
            if (chatFrom == ChatMessageFrom_ME){
                make.right.equalTo(self).offset(- ArcRadius);
                make.left.equalTo(self);
            }else{
                make.left.equalTo(self).offset(ArcRadius);
                make.right.equalTo(self);
            }
            make.width.mas_greaterThanOrEqualTo(maxWidth - 50);
            make.width.mas_lessThanOrEqualTo(maxWidth);
            make.height.mas_equalTo(60);
        }];
    }else if (self.message.chatType == ChatMessageType_IMG){
        [self removeAllViewWithout:self.imageView];
        CGSize size = CGSizeMake([self.message.content.thumbnail_width floatValue], [self.message.content.thumbnail_height floatValue]);
        if (size.width < maxWidth) {
            maxWidth = size.width;
        }
        CGFloat multiplie = size.height != 0 ? size.height / (size.width *1.0) : 35;
        [self.imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(self);
            if (chatFrom == ChatMessageFrom_ME) {
                make.left.equalTo(self);
                make.right.equalTo(self).offset(-ArcRadius);
            }else if (chatFrom == ChatMessageFrom_OTHER){
                make.left.equalTo(self).offset(ArcRadius);
                make.right.equalTo(self);
            }else{
                make.left.right.equalTo(self);
            }
            make.width.mas_equalTo(maxWidth);
            make.height.mas_equalTo(self.imageView.mas_width).multipliedBy(multiplie);
        }];
    }else if (self.message.chatType == ChatMessageType_LOTTERY){
        if (self.message.content.heelNickName.length) {
            [self removeAllViewWithout:self.textLabel];
            self.textLabel.preferredMaxLayoutWidth = maxWidth;
            [self.textLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self).offset(10);
                make.bottom.equalTo(self).offset(-10);
                make.width.mas_lessThanOrEqualTo(maxWidth);
                
                if (chatFrom == ChatMessageFrom_ME){
                    make.right.equalTo(self).offset(-10 - ArcRadius);
                    make.left.equalTo(self).offset(10);
                }else{
                    make.left.equalTo(self).offset(10 + ArcRadius);
                    make.right.equalTo(self).offset(-10);
                }
            }];
        }else{
            [self removeAllViewWithout:self.lotteryView];
            [self.lotteryView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.bottom.equalTo(self);
                make.width.mas_lessThanOrEqualTo(maxWidth);
                if (chatFrom == ChatMessageFrom_ME){
                    make.right.equalTo(self).offset(- ArcRadius);
                    make.left.equalTo(self);
                }else{
                    make.left.equalTo(self).offset(ArcRadius);
                    make.right.equalTo(self);
                }
                
            }];
        }
    }
}

- (void)drawRect:(CGRect)rect ChatFrome:(ChatMessageFrom)chatFrom{
    CGFloat width = rect.size.width;
    CGFloat height = rect.size.height;
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    
    [shapeLayer setFillColor:[self.backgroundColor CGColor]];
    CGMutablePathRef path = CGPathCreateMutable();
    
    if (chatFrom == ChatMessageFrom_ME) {
        
        CGPathMoveToPoint(path, nil, width - ArcRadius, height - ArcRadius);
        
        CGPathAddArcToPoint(path,
                            nil,
                            width - ArcRadius,
                            height,
                            width - ArcRadius - AnglerRadius,
                            height,
                            AnglerRadius);
        
        CGPathAddArcToPoint(path,
                            nil,
                            0,
                            height,
                            0,
                            height - AnglerRadius,
                            AnglerRadius);
        
        CGPathAddArcToPoint(path,
                            nil,
                            0,
                            0,
                            AnglerRadius,
                            0,
                            AnglerRadius);
        
        
        
        if (self.message.chatType != ChatMessageType_IMG &&
            self.message.chatType != ChatMessageType_VIDEO &&
            self.message.chatType != ChatMessageType_RED) {
            CGPathAddLineToPoint(path, nil, width, 0);
            
            CGPathAddLineToPoint(path, nil, width - ArcRadius, ArcRadius);
            
            CGPathAddLineToPoint(path, nil, width - ArcRadius, height - AnglerRadius);
            
        }else{
            
            CGPathAddArcToPoint(path,
                                nil,
                                width - ArcRadius,
                                0,
                                width - ArcRadius,
                                AnglerRadius,
                                AnglerRadius);
        }
    }else{
        
        
        CGPathMoveToPoint(path, nil, ArcRadius + AnglerRadius, 0);
        
        CGPathAddArcToPoint(path,
                            nil,
                            width,
                            0,
                            width,
                            AnglerRadius,
                            AnglerRadius);
        
        CGPathAddArcToPoint(path,
                            nil,
                            width,
                            height,
                            width - AnglerRadius,
                            height,
                            AnglerRadius);
        
        CGPathAddArcToPoint(path,
                            nil,
                            ArcRadius,
                            height,
                            ArcRadius,
                            height - AnglerRadius,
                            AnglerRadius );
        
        if (self.message.chatType != ChatMessageType_IMG &&
            self.message.chatType != ChatMessageType_VIDEO &&
            self.message.chatType != ChatMessageType_RED) {
            CGPathAddLineToPoint(path, nil, ArcRadius, ArcRadius);
            
            CGPathAddLineToPoint(path, nil, 0, 0);
            
            CGPathAddLineToPoint(path, nil, ArcRadius + AnglerRadius, 0);
        }else{
            CGPathAddArcToPoint(path,
                                nil,
                                ArcRadius,
                                0,
                                ArcRadius + AnglerRadius,
                                0,
                                AnglerRadius);
        }
    }
    
    CGPathCloseSubpath(path);
    [shapeLayer setPath:path];
    CFRelease(path);
    self.layer.mask = shapeLayer;
}

- (void)drawRect:(CGRect)rect{
    if (self.forceChatFrom == ChatMessageFrom_SYS) {
        [self drawRect:rect ChatFrome:self.message.chatFrom];
    }else{
        [self drawRect:rect ChatFrome:self.forceChatFrom];
    }
    
    
    [super drawRect:rect];
}
#pragma mark - GET/SET
-(UILabel *)textLabel{
    if (!_textLabel) {
        _textLabel = [UILabel setAllocLabelWithText:@"" FontOfSize:13 rgbColor:0x000000];
        _textLabel.numberOfLines = 0;
        _textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    }
    return _textLabel;
}

-(YLImageView *)imageView{
    if (!_imageView) {
        _imageView = [[YLImageView alloc] init];
        
    }
    return _imageView;
}

-(ChatBubbleRedView *)redView{
    if (!_redView) {
        
        _redView = [[[NSBundle mainBundle] loadNibNamed:@"ChatBubbleRedView" owner:self options:nil] firstObject];

    }
    return _redView;
}

-(ChatBubbleLotteryView *)lotteryView{
    if (!_lotteryView) {
        _lotteryView = [[[NSBundle mainBundle] loadNibNamed:@"ChatBubbleLotteryView" owner:self options:nil] firstObject];

    }
    return _lotteryView;
}

-(void)setMessage:(Message *)message{
    _message = message;
    self.textLabel.attributedText = message.content.attributedText;
    if (message.chatType == ChatMessageType_LOTTERY && message.content.heelNickName) {
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"@%@ 跟投金额 %.2f元",message.content.heelNickName,[message.content.totalMoney floatValue]]];
        self.textLabel.attributedText = text;
    }
    [self.imageView setImage:nil];
    NSString *imgUrl = [NSString stringWithFormat:@"%@%@",CHAT_UTIL.clientInfo.imageHost,message.content.thumbnail];
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:imgUrl]
                      placeholderImage:nil];
    self.lotteryView.message = message;
    self.redView.message = message;
    [self setNeedsUpdateConstraints];
    [self setNeedsDisplay];
}

@end
