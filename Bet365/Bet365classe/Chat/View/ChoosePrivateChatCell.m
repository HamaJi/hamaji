//
//  ChoosePrivateChatCell.m
//  Bet365
//
//  Created by adnin on 2019/2/16.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChoosePrivateChatCell.h"
@interface ChoosePrivateChatCell()

@property (strong, nonatomic) IBOutlet UIImageView *avatorView;

@property (strong, nonatomic) IBOutlet UIImageView *levelView;

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

@property (strong, nonatomic) IBOutlet UIButton *subMitBtn;
@property (weak, nonatomic) IBOutlet UILabel *bugeLabel;

@end
@implementation ChoosePrivateChatCell

- (void)awakeFromNib {
    [super awakeFromNib];
    @weakify(self);
    [[RACSignal combineLatest:@[[RACObserve(self, model) distinctUntilChanged],
                                [RACObserve(self, entity) distinctUntilChanged]]
                       reduce:^id (ChatUserInfoModel *model,ChatUserInfoEntity *entity){
        return @(model != nil  && entity != nil);
    }] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        if ([x boolValue]) {
            [self setUI];
        }
    }];
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)subAciton:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(tapPrivateSubmitChoosePrivateCell:)]) {
        [self.delegate tapPrivateSubmitChoosePrivateCell:self];
    }
}

-(void)setUI{
    self.nameLabel.text = self.entity.nickName;
    NSString *imgPatch = [NSString stringWithFormat:@"chat_level_%li",[self.entity.level integerValue]];
    [self.levelView setImage:[UIImage imageNamed:imgPatch]];
    [self.avatorView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SerVer_Url,self.entity.avatar]] placeholderImage:[UIImage imageNamed:@"chat_userAvator_placeHolder"]];
    
    [self.subMitBtn setHidden:!(([self.model.privateChat integerValue] || [CHAT_UTIL.clientInfo.userInfo.privateChat integerValue]) && self.model.userId != CHAT_UTIL.clientInfo.userInfo.userId && self.model.status)];
}

-(void)setUnread:(NSUInteger)unread{
    self.bugeLabel.hidden = (unread == 0);
    self.bugeLabel.text = [NSString stringWithFormat:@"%li",unread];
}
@end
