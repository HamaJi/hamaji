//
//  ChatChooseRoomCell.m
//  Bet365
//
//  Created by HHH on 2018/9/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatChooseRoomCell.h"
@interface ChatChooseRoomCell()
@property (strong, nonatomic) IBOutlet UIImageView *iconImageView;
@property (strong, nonatomic) IBOutlet UILabel *titileLabel;
@property (strong, nonatomic) IBOutlet UIButton *enterBtn;
@property (strong, nonatomic) IBOutlet UIView *conteView;

@end
@implementation ChatChooseRoomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.enterBtn setTitle:@"进入房间" forState:UIControlStateNormal];
    [self.enterBtn setTitle:@"当前房间" forState:UIControlStateSelected];
    [self.enterBtn setTitle:@"房间未开启" forState:UIControlStateDisabled];
    
    [self.enterBtn setBackgroundImage:[UIImage imageWithColor:COLOR_WITH_HEX(0x4EA49A)] forState:UIControlStateNormal];
    [self.enterBtn setBackgroundImage:[UIImage imageWithColor:COLOR_WITH_HEX(0xD92E2F)] forState:UIControlStateSelected];
    [self.enterBtn setBackgroundImage:[UIImage imageWithColor:COLOR_WITH_HEX(0xd2d2d2)] forState:UIControlStateDisabled];
    

    @weakify(self);
    [[RACSignal combineLatest:@[[RACObserve(self, roomId) distinctUntilChanged],
                                [RACObserve(self, roomInfoModel) distinctUntilChanged]] reduce:^id _Nonnull(ChatRoomInfoModel *roomInfoModel, NSNumber *roomId){
        return @(roomInfoModel && roomId);
    }] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.enterBtn setSelected:([self.roomInfoModel.identity integerValue] == self.roomId)];
            [self.enterBtn setEnabled:self.roomInfoModel.isOpen];
            self.titileLabel.text = self.roomInfoModel.name;
            [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/data/image/room_icon/%li.jpg",SerVer_Url,[self.roomInfoModel.identity integerValue]]] placeholderImage:[UIImage imageNamed:@"chat_userAvator_placeHolder"]];
            self.iconImageView.layer.cornerRadius = self.iconImageView.ycz_width / 2.0;
        });
    }];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.conteView.layer.shadowOffset = CGSizeMake(1, 1);
        self.conteView.layer.shadowOpacity = 0.8;
        self.conteView.layer.shadowColor = [UIColor blackColor].CGColor;
    });

    // Initialization code
}
- (IBAction)enterAction:(UIButton *)sender {
    self.handle ? self.handle(self.roomInfoModel) : nil;
}

@end
