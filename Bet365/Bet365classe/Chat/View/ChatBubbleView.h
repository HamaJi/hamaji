//
//  ChatBubbleView.h
//  Bet365
//
//  Created by HHH on 2018/9/15.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Message.h"
#import <YLGIFImage/YLImageView.h>
#import "ChatBubbleRedView.h"
#import "ChatBubbleLotteryView.h"
@class ChatBubbleView;

@protocol ChatBubbleViewDelegate <NSObject>

@optional

-(void)ChatBubbleViewDidTapImage:(ChatBubbleView *)view;

-(void)ChatBubbleViewDidTapLongImage:(ChatBubbleView *)view;

-(void)ChatBubbleViewDidTapLottery:(ChatBubbleView *)view;

-(void)ChatBubbleViewDidTapLotteryButton:(ChatBubbleView *)view;

-(void)ChatBubbleViewDidTapLongLottery:(ChatBubbleView *)view;

-(void)ChatBubbleViewDidTapRed:(ChatBubbleView *)view;

-(void)ChatBubbleViewDidLongTapRed:(ChatBubbleView *)view;

-(void)ChatBubbleViewDidTapText:(ChatBubbleView *)view;

-(void)ChatBubbleViewDidLongTapText:(ChatBubbleView *)view;

@end

@interface ChatBubbleView : UIView

@property (nonatomic,weak)Message *message;

/**
 强制修改气泡指向类型
 */
@property (nonatomic,assign)ChatMessageFrom forceChatFrom;

@property (nonatomic,weak)id<ChatBubbleViewDelegate> delegate;



/**
 ChatMessageType_TEXT 文本
 */
@property (nonatomic,strong,readonly)UILabel *textLabel;


/**
 ChatMessageType_IMG 图片
 */
@property (nonatomic,strong,readonly)YLImageView *imageView;


/**
 ChatMessageType_RED 红包
 */
@property (nonatomic,strong,readonly)ChatBubbleRedView *redView;


/**
 ChatMessageType_LOTTERY 彩票
 */
@property (nonatomic,strong,readonly)ChatBubbleLotteryView *lotteryView;

@end
