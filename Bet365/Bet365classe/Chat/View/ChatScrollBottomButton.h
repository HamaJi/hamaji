//
//  ChatScrollBottomButton.h
//  Bet365
//
//  Created by HHH on 2018/9/26.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatScrollBottomButton : UIButton

@property (nonatomic,assign)NSUInteger unreadCount;
@end
