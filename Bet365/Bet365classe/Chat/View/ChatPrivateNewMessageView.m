//
//  ChatPrivateNewMessageView.m
//  Bet365
//
//  Created by adnin on 2019/2/17.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatPrivateNewMessageView.h"
#import "ChatPrivateNewMessageCell.h"

@interface ChatPrivateNewMessageView()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic,strong) UIButton *moreButton;

@property (nonatomic,strong) UICollectionView *collectionView;

@property (nonatomic,strong) NSMutableDictionary *entityData;

@property (nonatomic,assign)CGFloat barHeight;

@property (nonatomic,assign)BOOL isEditing;

@end

@implementation ChatPrivateNewMessageView

-(instancetype)init{
    if (self = [super init]) {
        [self addObserver];
        
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addObserver];
        
    }
    return self;
}

-(void)addObserver{
    @weakify(self);
    [[RACObserve(self, barHeight) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.collectionView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo([x floatValue]);
            }];
            [self layoutIfNeeded];
            if (self.delegate && [self.delegate respondsToSelector:@selector(ChatPrivateNewMessageView:UpdateHeight:)]) {
                [self.delegate ChatPrivateNewMessageView:self UpdateHeight:[x floatValue]];
            }
        });
    }];
    
    [self.moreButton addTarget:self action:@selector(moreAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.collectionView addLongPressGestureRecognizer:^(UILongPressGestureRecognizer *recognizer, NSString *gestureId) {
        @strongify(self);
        if (recognizer.state == UIGestureRecognizerStateBegan) {
            if (!self.isEditing) {
                [self becomeFirstResponder];
            }else{
                [self resignFirstResponder];
            }
        }
    }];
    
    [[RACObserve(self, isEditing) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        [self.collectionView reloadData];
    }];
    
}

- (BOOL)canBecomeFirstResponder{
    return YES;
}

-(BOOL)becomeFirstResponder{
    self.isEditing = YES;
    return [super becomeFirstResponder];
}

-(BOOL)resignFirstResponder{
    self.isEditing = NO;
    return [super resignFirstResponder];
}

-(void)moreAction{
    if (self.delegate && [self.delegate respondsToSelector:@selector(tapMoreChatPrivateNewMessageView:)]) {
        [self.delegate tapMoreChatPrivateNewMessageView:self];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.userList.count;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 1;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);

}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0.0;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    
    return 0.0;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(50,65);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ChatPrivateNewMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatPrivateNewMessageCell" forIndexPath:indexPath];
    cell.model = [self.userList safeObjectAtIndex:indexPath.section];
    cell.entity = [self.entityData objectForKey:[NSString stringWithFormat:@"%li",cell.model.userId]];
    cell.unreadCount = [CHAT_UTIL getUnreadMessageCountByUserId:cell.model.userId];
    /** 私聊列表添加客服入口,禅道12868 **/
    cell.isEditing = ([cell.model.customer integerValue] != 1) ? self.isEditing : NO;
    return cell;
}
//-(BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath{
//    return YES;
//}
//-(void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
//
//}
#pragma mark - UICollectionViewDelegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    ChatPrivateNewMessageCell *cell = (ChatPrivateNewMessageCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if (self.delegate) {
        /** 私聊列表添加客服入口,禅道12868 **/
        if (self.isEditing && ([cell.model.customer integerValue] != 1)) {
            if ([self.delegate respondsToSelector:@selector(ChatPrivateNewMessageView:deleteUser:)]) {
                [self.delegate ChatPrivateNewMessageView:self deleteUser:cell.model];
            }
        }else{
            if ([self.delegate respondsToSelector:@selector(ChatPrivateNewMessageView:didSelectUser:)]) {
                [self.delegate ChatPrivateNewMessageView:self didSelectUser:cell.model];
            }
        }
    }
}

#pragma mark - GET/SET
-(void)setUserList:(NSMutableArray<ChatUserInfoModel *> *)userList{
    _userList = userList.mutableCopy;
    [self.entityData removeAllObjects];
    [userList enumerateObjectsUsingBlock:^(ChatUserInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        ChatUserInfoEntity *entity = [ChatUtilitySubscribeData getUserInfoListEntityByUserId:obj.userId].firstObject;
        if (entity) {
            [self.entityData setObject:entity forKey:[NSString stringWithFormat:@"%li",obj.userId]];
        }
    }];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!_userList.count) {
            [self resignFirstResponder];
        }
        self.barHeight = _userList.count ? 65 : 0;
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
    
    
}


-(NSMutableDictionary *)entityData{
    if (!_entityData) {
        _entityData = @{}.mutableCopy;
    }
    return _entityData;
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.pagingEnabled = YES;
        [self addSubview:_collectionView];
        [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.moreButton.mas_right);
            make.top.right.bottom.equalTo(self);
            make.height.mas_equalTo(0);
        }];
        
        [_collectionView setBackgroundColor:[UIColor clearColor]];
        [_collectionView setShowsHorizontalScrollIndicator:NO];
        [_collectionView registerNib:[UINib nibWithNibName:@"ChatPrivateNewMessageCell" bundle:nil] forCellWithReuseIdentifier:@"ChatPrivateNewMessageCell"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
    }
    return _collectionView;
}

-(UIButton *)moreButton{
    if (!_moreButton) {
        _moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_moreButton setImage:[UIImage imageNamed:@"chat_newprivatemessage_more"] forState:UIControlStateNormal];
        [self addSubview:_moreButton];
        [_moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.equalTo(self).offset(5);
            make.bottom.equalTo(self).offset(-5);
            make.width.equalTo(_moreButton.mas_height).multipliedBy(25.0 / 48.0);
        }];
    }
    return _moreButton;
}
@end
