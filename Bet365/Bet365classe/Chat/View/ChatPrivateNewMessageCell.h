//
//  ChatPrivateNewMessageCell.h
//  Bet365
//
//  Created by adnin on 2019/2/18.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatPrivateNewMessageCell : UICollectionViewCell

@property (nonatomic,strong)ChatUserInfoEntity *entity;

@property (nonatomic,strong)ChatUserInfoModel *model;

@property (nonatomic,assign) NSInteger unreadCount;

@property (nonatomic,assign)BOOL isEditing;

@end
