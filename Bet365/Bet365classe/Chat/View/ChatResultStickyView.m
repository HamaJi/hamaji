//
//  ChatResultStickyView.m
//  Bet365
//
//  Created by HHH on 2018/11/22.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatResultStickyView.h"


@interface ChatResultStickyView()
@property (strong, nonatomic) IBOutlet UIImageView *userIconView;

@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *contentLabel;
@property (strong, nonatomic) IBOutlet UILabel *moneyLabel;
@property (strong, nonatomic) IBOutlet UILabel *yuanLabel;
@property (strong, nonatomic) IBOutlet UILabel *toastLabel;


@end

@implementation ChatResultStickyView
-(void)setModel:(ChatRedPacketModel *)model{
    
    [self.userIconView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SerVer_Url,model.activityChatRedpacket.createrAvator]]
                         placeholderImage:[UIImage imageNamed:@"chat_userAvator_placeHolder"]];
    self.userNameLabel.text = model.activityChatRedpacket.createrName;
    if (model.activityChatRedpacket.redpacketName.length) {
        self.contentLabel.text = model.activityChatRedpacket.redpacketName;
    }
    
    __block ChatRedPacketDetailModel *detailModel = nil;
    [model.details enumerateObjectsUsingBlock:^(ChatRedPacketDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.userId integerValue] == CHAT_UTIL.clientInfo.userInfo.userId) {
            detailModel = obj;
            *stop = YES;
        }
    }];
    
    self.moneyLabel.text = [NSString stringWithFormat:@"%.2f",[detailModel.money floatValue]];
    
    __block BOOL isContain = NO;
    [model.details enumerateObjectsUsingBlock:^(ChatRedPacketDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([[NSString stringWithFormat:@"%li",CHAT_UTIL.clientInfo.userInfo.userId] isEqualToString:obj.userId]) {
            isContain = YES;
            *stop = YES;
        }
    }];
    
    if (!isContain) {
        self.yuanLabel.text = @"";
        self.toastLabel.text = @"";
        if (model.activityChatRedpacket.count == model.activityChatRedpacket.consumeCount) {
            self.moneyLabel.text = @"手慢了，红包派完了";
        }
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
