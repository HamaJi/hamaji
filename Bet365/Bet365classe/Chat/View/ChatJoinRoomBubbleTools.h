//
//  ChatJoinRoomBubbleTools.h
//  Bet365
//
//  Created by adnin on 2019/4/17.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@interface ChatJoinRoomBubbleTools : NSObject
/**
 起点
 */
@property (nonatomic,assign)CGPoint startPosition;

/**
 字体大小
 */
@property (nonatomic,assign)CGFloat fontSize;

/**
 字体颜色
 */
@property (nonatomic,strong)UIColor *fontColor;

/**
 背景阴影颜色
 */
@property (nonatomic,strong)UIColor *shadowColor;

/**
 最大动画移动高度
 */
@property (nonatomic,assign)CGFloat maxAnimationHeight;

/**
 动画持续时间
 */
@property (nonatomic,assign)CGFloat duration;

@property (nonatomic,assign)NSUInteger maxCount;


-(instancetype)initWithSupLayer:(CALayer *)supLayer;

/**
 添加动画气泡字符

 @param string 字符
 */
-(void)addBubbleString:(NSString *)string;

-(void)removeAllBubbleString;

@end

NS_ASSUME_NONNULL_END
