//
//  ChatTitleView.m
//  Bet365
//
//  Created by adnin on 2019/2/13.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatTitleView.h"
@interface ChatTitleView()

@property (strong, nonatomic) UILabel *titleLabel;

@property (strong, nonatomic) UIButton *onlineCountBtn;

@end

@implementation ChatTitleView

-(instancetype)init{
    if (self = [super init]) {
        [self setUI];
        dispatch_async(dispatch_get_main_queue(), ^{
            @weakify(self);
            [self addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
                @strongify(self);
                if (self.isPrivateChat && self.delegate && [self.delegate respondsToSelector:@selector(tapUpInsideChatTitleView:)]) {
                    [self.delegate tapUpInsideChatTitleView:self];
                }
            }];
        });
    }
    return self;
}

-(void)setUI{
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.top.equalTo(self);
    }];
    
    [self addSubview:self.onlineCountBtn];
    [self.onlineCountBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(5);
        make.height.mas_equalTo(0);
    }];
}
-(void)setIsPrivateChat:(BOOL)isPrivateChat{
    
    if (isPrivateChat == _isPrivateChat) {
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        _isPrivateChat = isPrivateChat;
        if (isPrivateChat) {
            [self.onlineCountBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.bottom.right.equalTo(self);
                make.top.equalTo(self.titleLabel.mas_bottom).offset(2);
                make.height.mas_equalTo(20);
            }];
        }else{
            [self.onlineCountBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.bottom.right.equalTo(self);
                make.top.equalTo(self.titleLabel.mas_bottom).offset(2);
                make.height.mas_equalTo(0);
            }];
        }
        
        [self layoutIfNeeded];
    });
    
}

-(void)setTitle:(NSString *)title{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.titleLabel.text = title ? title : @"";
    });
}

-(void)setCount:(NSInteger)count{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.onlineCountBtn setTitle:[NSString stringWithFormat:@"%li人在线",count] forState:UIControlStateNormal];
    });
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

#pragma mark - GET/SET
-(UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [UILabel setAllocLabelWithText:@"" FontOfSize:18 rgbColor:0xffffff];
        [_titleLabel setFont:[UIFont boldSystemFontOfSize:16]];
    }
    return _titleLabel;
}

-(UIButton *)onlineCountBtn{
    if (!_onlineCountBtn) {
        _onlineCountBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_onlineCountBtn setImage:[UIImage imageNamed:@"chat_choosenike"] forState:UIControlStateNormal];
        [_onlineCountBtn setTitle:@"0人在线" forState:UIControlStateNormal];
        _onlineCountBtn.titleLabel.font = [UIFont systemFontOfSize:10];
        [_onlineCountBtn setImagePosition:LXMImagePositionLeft spacing:5];
        _onlineCountBtn.userInteractionEnabled = NO;
        [_onlineCountBtn setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
        [_onlineCountBtn setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
        _onlineCountBtn.clipsToBounds = YES;
    }
    return _onlineCountBtn;
}
@end
