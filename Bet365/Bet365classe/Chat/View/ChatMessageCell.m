//
//  ChatMessageCell.m
//  Bet365
//
//  Created by HHH on 2018/9/15.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatMessageCell.h"
#import <SDWebImage/UIButton+WebCache.h>
@interface ChatMessageCell()<ChatBubbleViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *levelBtn;
@property (weak, nonatomic) IBOutlet UIButton *avatorBtn;
@property (strong, nonatomic) IBOutlet ChatBubbleView *bubbleView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;
@property (strong, nonatomic) IBOutlet UIButton *failBtn;
@property (nonatomic,assign)ChatMessageType messegeType;

@end
@implementation ChatMessageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.bubbleView.delegate = self;
    [self.avatorBtn addTarget:self action:@selector(tapAvatorAction:) forControlEvents:UIControlEventTouchUpInside];
    // Initialization code
}
-(void)updateConstraints{
    if (self.message.chatFrom == ChatMessageFrom_ME) {
        [self.avatorBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-10);
            make.top.equalTo(self.contentView).offset(10);
            make.height.mas_equalTo(40);
            make.height.equalTo(self.avatorBtn.mas_width).multipliedBy(1.0);
        }];
        
        [self.levelBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.avatorBtn.mas_left).offset(-5);
            make.top.equalTo(self.avatorBtn);
            make.height.mas_equalTo(15);
            if ([self.userInfoEntity.level integerValue] != 0) {
                
            }else{
                make.width.mas_equalTo(0);
            }
        }];
        
        [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.levelBtn.mas_left).offset(-5);
            make.centerY.equalTo(self.levelBtn);
        }];
        
        [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.nameLabel.mas_left).offset(-5);
            make.centerY.equalTo(self.nameLabel);
        }];

        [self.bubbleView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.avatorBtn.mas_left);
            make.top.equalTo(self.levelBtn.mas_bottom).offset(5);
            make.bottom.equalTo(self.contentView).offset(-10);
            make.height.mas_greaterThanOrEqualTo(35);
        }];
        
        [self.loadingView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.bubbleView.mas_left).offset(-10);
            make.centerY.equalTo(self.bubbleView.mas_centerY);
        }];
        
        [self.failBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.bubbleView.mas_left).offset(-10);
            make.centerY.equalTo(self.bubbleView.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(40, 40));
        }];
        
        
        
        
    }else{
        
        [self.avatorBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(10);
            make.top.equalTo(self.contentView).offset(10);
            make.height.mas_equalTo(40);
            make.height.equalTo(self.avatorBtn.mas_width).multipliedBy(1.0);
        }];
        
        [self.levelBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.avatorBtn.mas_right).offset(5);
            make.top.equalTo(self.avatorBtn);
            make.height.mas_equalTo(15);
            if ([self.userInfoEntity.level integerValue] != 0) {
                
            }else{
                make.width.mas_equalTo(0);
            }
        }];
        
        [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.levelBtn.mas_right).offset(5);
            make.centerY.equalTo(self.levelBtn);
        }];
        
        [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.nameLabel.mas_right).offset(5);
            make.centerY.equalTo(self.nameLabel);
        }];
        
        [self.bubbleView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.avatorBtn.mas_right);
            make.top.equalTo(self.levelBtn.mas_bottom).offset(5);
            make.bottom.equalTo(self.contentView).offset(-10);
            make.height.mas_greaterThanOrEqualTo(35);
        }];
    }
    [super updateConstraints];
}

#pragma mark - ChatBubbleViewDelegate
-(void)ChatBubbleViewDidTapText:(ChatBubbleView *)view{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatMessageCellDidTapText:)]) {
        [self.delegate ChatMessageCellDidTapText:self];
    }
}

-(void)ChatBubbleViewDidLongTapText:(ChatBubbleView *)view{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatMessageCellDidLongTapText:)]) {
        [self.delegate ChatMessageCellDidLongTapText:self];
    }
}

-(void)ChatBubbleViewDidTapImage:(ChatBubbleView *)view{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatMessageCellDidTapImage:)]) {
        [self.delegate ChatMessageCellDidTapImage:self];
    }
}
-(void)ChatBubbleViewDidTapLongImage:(ChatBubbleView *)view{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatMessageCellDidTapLongImage:)]) {
        [self.delegate ChatMessageCellDidTapLongImage:self];
    }
}

-(void)ChatBubbleViewDidTapLottery:(ChatBubbleView *)view{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatMessageCellDidTapLottery:)]) {
        [self.delegate ChatMessageCellDidTapLottery:self];
    }
}

-(void)ChatBubbleViewDidTapLotteryButton:(ChatBubbleView *)view{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatMessageCellDidTapLotteryButton:)]) {
        [self.delegate ChatMessageCellDidTapLotteryButton:self];
    }
}

-(void)ChatBubbleViewDidTapLongLottery:(ChatBubbleView *)view{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatMessageCellDidTapLongLottery:)]) {
        [self.delegate ChatMessageCellDidTapLongLottery:self];
    }
}
-(void)ChatBubbleViewDidTapRed:(ChatBubbleView *)view{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatMessageCellDidTapRed:)]) {
        [self.delegate ChatMessageCellDidTapRed:self];
    }
}
-(void)ChatBubbleViewDidLongTapRed:(ChatBubbleView *)view{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatMessageCellDidTapLongRed:)]) {
        [self.delegate ChatMessageCellDidTapLongRed:self];
    }
}

#pragma mark - Events
-(void)tapAvatorAction:(UIButton *)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatMessageCellDidTapAvator:)]) {
        [self.delegate ChatMessageCellDidTapAvator:self];
    }
}

-(void)tapFailAction:(UIButton *)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatMessageCellTapFail:)]) {
        [self.delegate ChatMessageCellTapFail:self];
    }
}

#pragma mark - GET/SET

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)updateData{
    
    if (self.message.sendStatus == ChatMessageSendStatus_SENDING) {
        self.loadingView.hidden = NO;
        [self.loadingView startAnimating];
    }else{
        self.loadingView.hidden = YES;
    }
    [self.failBtn addTarget:self action:@selector(tapFailAction:) forControlEvents:UIControlEventTouchUpInside];
    self.failBtn.hidden = self.message.sendStatus != ChatMessageSendStatus_FAIL;
    
    self.bubbleView.message = self.message;
    
    if (self.userInfoEntity) {
        self.nameLabel.text = self.userInfoEntity.nickName;
        NSInteger level = 0;
        if ([self.userInfoEntity.level integerValue] > 6 && [self.userInfoEntity.level integerValue] <= 100) {
            level = 6;
        }else{
            level = [self.userInfoEntity.level integerValue];
        }
        NSString *imgPatch = [NSString stringWithFormat:@"chat_level_%li",level];
        [self.levelBtn setImage:[UIImage imageNamed:imgPatch] forState:UIControlStateNormal];
        [self.avatorBtn sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SerVer_Url,self.userInfoEntity.avatar]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"chat_userAvator_placeHolder"]];
    }else if([self.message.userId integerValue] == -1){
        self.nameLabel.text = @"游客";
        [self.levelBtn setImage:nil forState:UIControlStateNormal];
        [self.avatorBtn setImage:[UIImage imageNamed:@"chat_userAvator_placeHolder"] forState:UIControlStateNormal];
    }else if ([self.message.userId integerValue] == 0){
        self.nameLabel.text = @"系统消息";
        [self.levelBtn setImage:nil forState:UIControlStateNormal];
        [self.avatorBtn setImage:[UIImage imageNamed:@"chat_userAvator_placeHolder"] forState:UIControlStateNormal];
    }else{
        self.nameLabel.text = @"--";
        [self.levelBtn setImage:nil forState:UIControlStateNormal];
        [self.avatorBtn setImage:[UIImage imageNamed:@"chat_userAvator_placeHolder"] forState:UIControlStateNormal];
    }
    
    self.timeLabel.text = [self.message.curTime getTimeFormatStringWithFormat:@"HH:mm:ss"];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setNeedsUpdateConstraints];
    });
}

-(UILabel *)kTextLabel{
    return self.bubbleView.textLabel;
}

-(YLImageView *)kImageView{
    return self.bubbleView.imageView;
}

-(ChatBubbleRedView *)kRedView{
    return self.bubbleView.redView;
}

-(ChatBubbleLotteryView *)kLotteryView{
    return self.bubbleView.lotteryView;
}
@end
