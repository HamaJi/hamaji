//
//  ChatMessageCell.h
//  Bet365
//
//  Created by HHH on 2018/9/15.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatCell.h"
#import "ChatBubbleView.h"
@interface ChatMessageCell : ChatCell

@property (strong, nonatomic,readonly) ChatBubbleView *bubbleView;


/**
 ChatMessageType_TEXT 文本
 */
@property (readonly,nonatomic)UILabel *kTextLabel;


/**
 ChatMessageType_IMG 图片
 */
@property (readonly,nonatomic)YLImageView *kImageView;


/**
 ChatMessageType_RED 红包
 */
@property (readonly,nonatomic)ChatBubbleRedView *kRedView;


/**
 ChatMessageType_LOTTERY 彩票
 */
@property (readonly,nonatomic)ChatBubbleLotteryView *kLotteryView;
@end
