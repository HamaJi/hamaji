//
//  ChatRedResultsCell.h
//  Bet365
//
//  Created by HHH on 2018/11/22.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatRedResultsCell : UITableViewCell

@property (nonatomic,strong) ChatRedPacketDetailModel *detailModel;

@end
