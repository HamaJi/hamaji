//
//  ChatLotteryTopResultCell.m
//  Bet365
//
//  Created by HHH on 2018/10/3.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatLotteryTopResultCell.h"
#define LotteryTopBallWidth 17.0
#define LotteryTopBallSpacing 3.0
@interface ChatLotteryTopResultCell()
@property (strong, nonatomic) IBOutlet UILabel *titileLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UIView *ballContentView;
@property (nonatomic)CGFloat maxWidth;

@end
@implementation ChatLotteryTopResultCell


- (void)awakeFromNib {
    [super awakeFromNib];
    
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setBallUI{
    self.titileLabel.text = [NSString stringWithFormat:@"第%@期",self.model.turnNum];
    self.timeLabel.text = self.model.openTime;
    [self.ballContentView.subviews.firstObject removeFromSuperview];
    UIView *contentView = [[UIView alloc] init];

    LotteryType type = checkNowGameType([NSString stringWithFormat:@"%li",self.model.gameId]);
    __block CGFloat leading = 0;
    __block CGFloat row = 0;
    __block NSUInteger count;
    if (type == LotteryType_CQSSC) {
        /** 蓝图**/
        count = self.model.openNum.count;
        __block NSUInteger megre = 0;
        __block NSUInteger horMacCount = self.maxWidth / (LotteryTopBallWidth + LotteryTopBallSpacing);
        __block NSUInteger maxRow = count / horMacCount + ((count % horMacCount) > 0 ? 1 : 0);
        [self.model.openNum enumerateObjectsUsingBlock:^(NSString *  _Nonnull text, NSUInteger idx, BOOL * _Nonnull stop) {
            if (idx / horMacCount !=0 && idx % horMacCount == 0 ) {
                row += 1;
                leading = 0;
            }
            UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"FC%@",text]]];
            [contentView addSubview:img];
            [img mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(contentView).offset(leading);
                make.top.equalTo(contentView).offset(row * (LotteryTopBallWidth + LotteryTopBallSpacing));
                make.size.mas_equalTo(CGSizeMake(LotteryTopBallWidth, LotteryTopBallWidth));
            }];
            megre += [text integerValue];
            leading += (LotteryTopBallWidth + LotteryTopBallSpacing);
            if (idx + 1 == count) {
                leading = 0 ;
                row += 1;
                NSInteger cha = [[self.model.openNum safeObjectAtIndex:0] integerValue] - [[self.model.openNum safeObjectAtIndex:4] integerValue];
                NSString *animalString;
                if (cha < 0) {
                    animalString = @"虎";
                }else if (cha == 0){
                    animalString = @"龙";
                }else{
                    animalString = @"和局";
                }
                NSString *bottomStr = [NSString stringWithFormat:@"%@  %@  %@",
                                       (megre % 2 == 0 ? @"双" : @"单"),
                                       (megre >= 23 ? @"大" : @"小"),
                                       animalString];
                UILabel *bottomLabel = [UILabel setAllocLabelWithText:bottomStr FontOfSize:12 rgbColor:0x000000];
                [contentView addSubview:bottomLabel];
                [bottomLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(contentView).offset(leading);
                    make.top.equalTo(contentView).offset(row * (LotteryTopBallWidth + LotteryTopBallSpacing));
                    make.bottom.equalTo(contentView);
                }];
            }
            
        }];
    }else if (type == LotteryType_FC3D ||
              type == LotteryType_GXKL ||
              type == LotteryType_GD11TO5 ||
              type == LotteryType_GDHL10) {
        /** 蓝图**/
        count = self.model.openNum.count;
        __block NSUInteger horMacCount = self.maxWidth / (LotteryTopBallWidth + LotteryTopBallSpacing);
        __block NSUInteger maxRow = count / horMacCount + ((count % horMacCount) > 0 ? 1 : 0);
        [self.model.openNum enumerateObjectsUsingBlock:^(NSString *  _Nonnull text, NSUInteger idx, BOOL * _Nonnull stop) {
            if (idx / horMacCount !=0 && idx % horMacCount == 0 ) {
                row += 1;
                leading = 0;
            }
            UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"FC%@",text]]];
            [contentView addSubview:img];
            [img mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(contentView).offset(leading);
                make.top.equalTo(contentView).offset(row * (LotteryTopBallWidth + LotteryTopBallSpacing));
                make.size.mas_equalTo(CGSizeMake(LotteryTopBallWidth, LotteryTopBallWidth));
                if (row + 1== maxRow) {
                    make.bottom.equalTo(contentView);
                }
            }];
            leading += (LotteryTopBallWidth + LotteryTopBallSpacing);
            
            
        }];
    }else if (type == LotteryType_BJPK10){
        /** 灰绿蓝红图**/
        count = self.model.openNum.count;
        __block NSUInteger horMacCount = self.maxWidth / (LotteryTopBallWidth + LotteryTopBallSpacing);
        __block NSUInteger maxRow = count / horMacCount + ((count % horMacCount) > 0 ? 1 : 0);
        [self.model.openNum enumerateObjectsUsingBlock:^(NSString *  _Nonnull text, NSUInteger idx, BOOL * _Nonnull stop) {
            if (idx / horMacCount !=0 && idx % horMacCount == 0 ) {
                row += 1;
                leading = 0;
            }
            
            UILabel *label = [self creatLabelByText:text];
            label.backgroundColor = [self getLotteryIconStringBgColorBySorIdx:[text integerValue] IndexPath:nil];
            [contentView addSubview:label];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(contentView).offset(leading);
                make.top.equalTo(contentView).offset(row * (LotteryTopBallWidth + LotteryTopBallSpacing));
                make.size.mas_equalTo(CGSizeMake(LotteryTopBallWidth, LotteryTopBallWidth));
            }];
            leading += (LotteryTopBallWidth + LotteryTopBallSpacing);
            if (idx + 1 == count) {
                leading = 0 ;
                row += 1;
                NSInteger megre = [self.model.openNum.firstObject integerValue] + [[self.model.openNum safeObjectAtIndex:1] integerValue];
                NSString *bottomStr = [NSString stringWithFormat:@"冠亚和  %li  %@  %@",
                                       megre,
                                       (megre % 2 == 0 ? @"双" : @"单"),
                                       (megre >= 11 ? @"大" : @"小")];
                UILabel *bottomLabel = [UILabel setAllocLabelWithText:bottomStr FontOfSize:12 rgbColor:0x000000];
                [contentView addSubview:bottomLabel];
                [bottomLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(contentView).offset(leading);
                    make.top.equalTo(contentView).offset(row * (LotteryTopBallWidth + LotteryTopBallSpacing));
                    make.bottom.equalTo(contentView);
                }];
            }
        }];
    }else if (type == LotteryType_PCDD){
        /** 灰绿蓝红+合图**/
        count = self.model.openNum.count + 2;
        __block NSUInteger horMacCount = self.maxWidth / (LotteryTopBallWidth + LotteryTopBallSpacing);
        __block NSUInteger maxRow = count / horMacCount + ((count % horMacCount) > 0 ? 1 : 0);
        __block NSUInteger merge = 0;
        
        for (int idx = 0; idx<count; idx++) {
            NSString *text = [self.model.openNum safeObjectAtIndex:idx];
            if (idx / horMacCount !=0 && idx % horMacCount == 0 ) {
                row += 1;
                leading = 0;
            }
            UILabel *label;
            if (idx == self.model.openNum.count) {
                label = [UILabel setAllocLabelWithText:@"+" FontOfSize:18 rgbColor:0x000000];
                label.textColor = [UIColor darkGrayColor];
            }else if(idx - 1 == self.model.openNum.count){
                
                label = [self creatLabelByText:[NSString stringWithFormat:@"%li",merge]];
                label.backgroundColor = [self getLotteryIconStringBgColorBySorIdx:[text integerValue] IndexPath:nil];
            }else {
                merge += [text integerValue];
                label = [self creatLabelByText:text];
                label.backgroundColor = [self getLotteryIconStringBgColorBySorIdx:[text integerValue] IndexPath:nil];
            }
            [contentView addSubview:label];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(contentView).offset(leading);
                make.top.equalTo(contentView).offset(row * (LotteryTopBallWidth + LotteryTopBallSpacing));
                make.size.mas_equalTo(CGSizeMake(LotteryTopBallWidth, LotteryTopBallWidth));
                if (row + 1== maxRow) {
                    make.bottom.equalTo(contentView);
                }
            }];
            leading += (LotteryTopBallWidth + LotteryTopBallSpacing);
        }
    }else if (type == LotteryType_BJKL8){
        /**  红心图**/
        count = self.model.openNum.count;
        __block NSUInteger horMacCount = self.maxWidth / (LotteryTopBallWidth + LotteryTopBallSpacing);
        __block NSUInteger maxRow = count / horMacCount + ((count % horMacCount) > 0 ? 1 : 0);
        [self.model.openNum enumerateObjectsUsingBlock:^(NSString *  _Nonnull text, NSUInteger idx, BOOL * _Nonnull stop) {
            if (idx / horMacCount !=0 && idx % horMacCount == 0 ) {
                row += 1;
                leading = 0;
            }
            UILabel *label = [self creatLabelByText:text];
            [contentView addSubview:label];
            label.backgroundColor = COLOR_WITH_HEX(0xD92E2F);
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(contentView).offset(leading);
                make.top.equalTo(contentView).offset(row * (LotteryTopBallWidth + LotteryTopBallSpacing));
                make.size.mas_equalTo(CGSizeMake(LotteryTopBallWidth, LotteryTopBallWidth));
                if (row + 1== maxRow) {
                    make.bottom.equalTo(contentView);
                }
            }];
            leading += (LotteryTopBallWidth + LotteryTopBallSpacing);
        }];
    }else if (type == LotteryType_KSK3){
        /**  骰子图**/
        count = self.model.openNum.count;
        __block NSUInteger horMacCount = self.maxWidth / (LotteryTopBallWidth + LotteryTopBallSpacing);
        __block NSUInteger maxRow = count / horMacCount + ((count % horMacCount) > 0 ? 1 : 0);
        __block NSUInteger megre = 0;
        
        [self.model.openNum enumerateObjectsUsingBlock:^(NSString *  _Nonnull text, NSUInteger idx, BOOL * _Nonnull stop) {
            if (idx / horMacCount !=0 && idx % horMacCount == 0 ) {
                row += 1;
                leading = 0;
            }
            UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"k3_%@",text]]];
            [contentView addSubview:img];
            [img mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(contentView).offset(leading);
                make.top.equalTo(contentView).offset(row * (LotteryTopBallWidth + LotteryTopBallSpacing));
                make.size.mas_equalTo(CGSizeMake(LotteryTopBallWidth, LotteryTopBallWidth));
                if (row + 1== maxRow) {
                    make.bottom.equalTo(contentView);
                }
            }];
            leading += (LotteryTopBallWidth + LotteryTopBallSpacing);
            if (idx + 1 == count) {
                megre += [text integerValue];
                NSString *bottomStr = [NSString stringWithFormat:@"%li  %@  %@",
                                       megre,
                                       (megre >= 11 ? @"大" : @"小"),
                                       (megre % 2 == 0 ? @"双" : @"单")];
                UILabel *bottomLabel = [UILabel setAllocLabelWithText:bottomStr FontOfSize:12 rgbColor:0x000000];
                [contentView addSubview:bottomLabel];
                [bottomLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(contentView).offset(leading);
                    make.top.equalTo(contentView).offset(row * (LotteryTopBallWidth + LotteryTopBallSpacing));
                    make.bottom.equalTo(contentView);
                }];
            }
        }];
        
    }else if (type == LotteryType_SIX){
        /**  傻逼生肖图**/
        count = self.model.openNum.count;
        for (int i = 0; i<count; i++) {
            if (i + 1 == count) {
                UILabel * spacingLabel = [UILabel setAllocLabelWithText:@"+" FontOfSize:18 rgbColor:0x000000];
                [contentView addSubview:spacingLabel];
                [spacingLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.equalTo(contentView);
                    make.width.mas_equalTo(LotteryTopBallWidth);
                    make.left.equalTo(contentView).offset(leading);
                }];
                leading += (LotteryTopBallWidth + LotteryTopBallSpacing);
            }
            
            UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"ball%@",self.model.openNum[i]]]];
            [contentView addSubview:img];
            [img mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(contentView);
                make.left.equalTo(contentView).offset(leading);
                make.size.mas_equalTo(CGSizeMake(LotteryTopBallWidth, LotteryTopBallWidth));
            }];
            
            NSInteger zodiacSignIdx = 11 - [self.model.openNum[i] integerValue] % 12;
            UILabel *label = [UILabel setAllocLabelWithText:getChatTopResultZodiacSigns()[zodiacSignIdx] FontOfSize:11 rgbColor:0x000000];
            [contentView addSubview:label];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(img.mas_bottom);
                make.bottom.equalTo(contentView);
                make.centerX.equalTo(img);
                make.height.mas_equalTo(LotteryTopBallWidth);
                make.bottom.equalTo(contentView);
            }];
            
            leading += (LotteryTopBallSpacing + LotteryTopBallWidth);
            
            
        }
    }
    
    [self.ballContentView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.ballContentView);
    }];
    
}

-(UIColor *)getLotteryIconStringBgColorBySorIdx:(NSUInteger)sorIdx IndexPath:(NSIndexPath *)indexPath{
    NSInteger idx = sorIdx % 3;
    switch (idx) {
        case 0:
            return COLOR_WITH_HEX(0x74B566);
        case 1:
            return COLOR_WITH_HEX(0x3D75D6);
        case 2:
            return COLOR_WITH_HEX(0xCA5C54);
        default:
            return [UIColor lightGrayColor];
    }
    
}

-(UILabel *)creatLabelByText:(NSString *)text{
    UILabel *label = [UILabel setAllocLabelWithText:@"" FontOfSize:11 rgbColor:0xffffff];
    label.clipsToBounds = YES;
    label.layer.cornerRadius = 8.5;
    label.textAlignment = NSTextAlignmentCenter;
    label.text= text;
    return label;
}
NSArray *getChatTopResultZodiacSigns(){
    return @[@"鼠",@"牛",@"虎",@"兔",@"龙",@"蛇",@"马",@"羊",@"猴",@"鸡",@"狗",@"猪"];
}

#pragma mark - GET/SET
-(CGFloat)maxWidth{
    return WIDTH - 30.0;
}
-(void)setModel:(ChatLotteryTopResultModel *)model{
    _model = model;
    [self setBallUI];
}
@end
