//
//  ChatRightCell.m
//  Bet365
//
//  Created by HHH on 2018/10/9.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatRightCell.h"

@interface ChatRightCell()

@property (strong, nonatomic) IBOutlet UIImageView *iconImg;
@property (strong, nonatomic) IBOutlet UILabel *titileLabel;

@end
@implementation ChatRightCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setItem:(ChatRightItem *)item{
    _item = item;
    self.titileLabel.text = item.titile;
    [self.iconImg sd_setImageWithURL:[NSURL URLWithString:_item.imgPath] placeholderImage:[UIImage imageNamed:_item.imgPath]];
}

@end
