//
//  ChatHeaderView.m
//  Bet365
//
//  Created by HHH on 2018/10/9.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatHeaderView.h"
@interface ChatHeaderView()

@property (weak, nonatomic)IBOutlet UIImageView *avatorIconImg;

@property (weak, nonatomic)IBOutlet UILabel *nickNameLabel;

@end

@implementation ChatHeaderView

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            @weakify(self);
            [[RACSignal combineLatest:@[[RACObserve(self, nickName) distinctUntilChanged],[RACObserve(self, avatorStr) distinctUntilChanged]]] subscribeNext:^(RACTuple * _Nullable x) {
                
                @strongify(self);
                [self.avatorIconImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SerVer_Url,CHAT_UTIL.clientInfo.userInfo.avatar]] placeholderImage:[UIImage imageNamed:@"chat_userAvator_placeHolder"]];
                if (!CHAT_UTIL.clientInfo.userInfo.nickName && [USER_DATA_MANAGER.userInfoData.type isEqualToString:@"TEST"]) {
                    self.nickNameLabel.text = @"试玩用户";
                }else{
                    self.nickNameLabel.text = CHAT_UTIL.clientInfo.userInfo.nickName;
                }
            }];
            self.avatorIconImg.userInteractionEnabled = YES;
            self.nickNameLabel.userInteractionEnabled = YES;
            [self.avatorIconImg addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
                @strongify(self)
                self.handle ? self.handle() : nil;
            }];
            [self.nickNameLabel addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
                @strongify(self)
                self.handle ? self.handle() : nil;
            }];
        });
        
        
    }
    return self;
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

