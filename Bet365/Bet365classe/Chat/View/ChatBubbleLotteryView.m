//
//  ChatBubbleLotteryView.m
//  Bet365
//
//  Created by HHH on 2018/9/16.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatBubbleLotteryView.h"

@interface ChatBubbleLotteryView()

@property (strong, nonatomic) IBOutlet UILabel *kindLabel;
@property (strong, nonatomic) IBOutlet UILabel *numberLabel;
@property (strong, nonatomic) IBOutlet UILabel *playRuleLabel;
@property (strong, nonatomic) IBOutlet UILabel *moneyLabel;
@property (strong, nonatomic) IBOutlet UILabel *contentLabel;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *buttonConstraintHeight;


@end

@implementation ChatBubbleLotteryView
-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        
    }
    return self;
}

-(void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    [self shadowForColor:[UIColor blackColor] shadowWidth:3 shadowType:UIViewShadowTypeAll];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self shadowForColor:[UIColor blackColor] shadowWidth:3 shadowType:UIViewShadowTypeAll];
    });
}

-(void)setMessage:(Message *)message{
    _message = message;
    self.kindLabel.text = _message.content.gameName;
    self.playRuleLabel.text = _message.content.playName;
    self.moneyLabel.text = [NSString stringWithFormat:@"%.2f元",[_message.content.totalMoney floatValue]];
    self.contentLabel.text = _message.content.betContent;
    self.numberLabel.text = _message.content.turnNum;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
