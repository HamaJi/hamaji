//
//  ChatMessagePopViewCell.m
//  Bet365
//
//  Created by HHH on 2018/10/2.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatMessagePopViewCell.h"
@interface ChatMessagePopViewCell()
@property (strong, nonatomic) IBOutlet UILabel *titileLabel;

@end
@implementation ChatMessagePopViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setTitile:(NSString *)titile{
    self.titileLabel.text = titile;
}
@end
