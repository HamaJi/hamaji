//
//  ChatScrollBottomButton.m
//  Bet365
//
//  Created by HHH on 2018/9/26.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatScrollBottomButton.h"
#import <POP/POP.h>

@interface ChatScrollBottomButton()
@property (nonatomic,strong)UILabel *bugeLabel;
@end
@implementation ChatScrollBottomButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        
        
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        @weakify(self);
        [[RACObserve(self, unreadCount) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            NSInteger count = [x integerValue];
            self.bugeLabel.hidden = (count < 1);
            self.bugeLabel.text = [NSString  stringWithFormat:@"%li",count];
        }];
    }
    return self;
}

-(instancetype)init{
    if (self = [super init]) {
        @weakify(self);
        [[RACObserve(self, unreadCount) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            NSInteger count = [x integerValue];
            self.bugeLabel.hidden = (count < 1);
            self.bugeLabel.text = [NSString  stringWithFormat:@"%li",count];
        }];
        
    }
    return self;
}
-(UILabel *)bugeLabel{
    if (!_bugeLabel) {
        _bugeLabel = [UILabel setAllocLabelWithText:@"0" FontOfSize:9 rgbColor:0xffffff];
        _bugeLabel.textAlignment = NSTextAlignmentCenter;
        _bugeLabel.backgroundColor = COLOR_WITH_HEX(0xD96156);
        [self addSubview:_bugeLabel];
        [_bugeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.mas_top).offset(10);
            make.left.equalTo(self.mas_right).offset(-10);
            make.size.mas_equalTo(CGSizeMake(_bugeLabel.font.lineHeight * 1.5, _bugeLabel.font.lineHeight * 1.5));
        }];
        _bugeLabel.clipsToBounds = YES;
        _bugeLabel.layer.cornerRadius = (_bugeLabel.font.lineHeight * 1.5) / 2.0;
        
        
    }
    return _bugeLabel;
}

-(void)setUnreadCount:(NSUInteger)unreadCount{
    dispatch_async_on_main_queue(^{
        self.bugeLabel.hidden = (unreadCount < 1);
        self.bugeLabel.text = [NSString  stringWithFormat:@"%li",unreadCount];
    });
}



@end
