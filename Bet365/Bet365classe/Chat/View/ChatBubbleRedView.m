//
//  ChatBubbleRedView.m
//  Bet365
//
//  Created by HHH on 2018/9/16.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatBubbleRedView.h"
@interface ChatBubbleRedView()
@property (weak, nonatomic) IBOutlet UILabel *titileLabel;
@property (strong, nonatomic) IBOutlet UIImageView *markView;

@end
@implementation ChatBubbleRedView

#pragma mark - GET/SET
-(void)setMessage:(Message *)message{
    _message = message;
    self.titileLabel.text = message.content.redName;
    if (message.hasRedPack) {
        self.markView.hidden = NO;
    }else{
        self.markView.hidden = YES;
        
    }
}
-(void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    [self shadowForColor:[UIColor blackColor] shadowWidth:3 shadowType:UIViewShadowTypeAll];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self shadowForColor:[UIColor blackColor] shadowWidth:3 shadowType:UIViewShadowTypeAll];
        //        self.layer.shadowOffset = CGSizeMake(0, 0);
        //        self.layer.shadowOpacity = 1;
        //        self.layer.shadowColor = [UIColor blackColor].CGColor;
        //        self.layer.shadowRadius = 5.f;
    });
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
