//
//  InputToolBarCell.m
//  Bet365
//
//  Created by HHH on 2018/9/17.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "InputToolBarCell.h"
@interface InputToolBarCell()
@property (strong, nonatomic) IBOutlet UIButton *button;

@end
@implementation InputToolBarCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
}

-(void)setItem:(InputToolBarCustomItem *)item{
    _item = item;
    [self.button setImage:_item.img forState:UIControlStateNormal];
    [self.button setTitle:_item.titile forState:UIControlStateNormal];
    [self.button setImagePosition:LXMImagePositionTop spacing:5];
}
@end
