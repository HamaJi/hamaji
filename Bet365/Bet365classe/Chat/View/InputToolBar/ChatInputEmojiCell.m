//
//  ChatInputEmojiCell.m
//  Bet365
//
//  Created by HHH on 2018/9/20.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatInputEmojiCell.h"
@interface ChatInputEmojiCell()
@property (strong, nonatomic) IBOutlet UIImageView *iconImg;

@end
@implementation ChatInputEmojiCell

- (void)awakeFromNib {
    [super awakeFromNib];
    RACSignal *itemSignal = [RACObserve(self, item) distinctUntilChanged];
    RACSignal *isDeleteSignal = [RACObserve(self, isDelete) distinctUntilChanged];
    @weakify(self);
    [[RACSignal combineLatest:@[itemSignal,isDeleteSignal] reduce:^id _Nonnull(EmojiItem *item,NSNumber *isDelete){
        if ([isDelete boolValue]) {
            return [UIImage imageNamed:@"chat_deleteEmoji_icon"];
        }else if (item){
            UIImage *ima = [UIImage imageNamed:item.imgName];
            if (!ima) {
                NSLog(@"items.imaG = %@",item.imgName);
            }
            return ima;
        }else{
            return nil;
        }
    }] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        
        [self.iconImg setImage:x];
    }];
    // Initialization code
}



@end
