//
//  ChatInputEmojiCell.h
//  Bet365
//
//  Created by HHH on 2018/9/20.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EmojiItem.h"
@interface ChatInputEmojiCell : UICollectionViewCell
@property (nonatomic,strong)EmojiItem *item;
@property (nonatomic,assign)BOOL isDelete;
@end
