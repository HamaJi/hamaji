//
//  ChatInputToolBar.h
//  Bet365
//
//  Created by HHH on 2018/9/17.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SendRedItem.h"
@class ChatInputToolBar;

@protocol ChatInputToolBarDelegate <NSObject>

@required

-(BOOL)ChatInputToolBar:(ChatInputToolBar *)toolBar SendText:(NSString *)str;

-(BOOL)tapMoreActionChatInputToolBar:(ChatInputToolBar *)toolBar;

-(void)ChatInputToolBarBecomeFirstResponder:(ChatInputToolBar *)toolBar;

-(void)ChatInputToolBarResignFirstResponder:(ChatInputToolBar *)toolBar;

@end

@interface ChatInputToolBar : UIView

@property (nonatomic,weak)id<ChatInputToolBarDelegate> delegate;

/**
 自定义bar高度
 */
@property (nonatomic,assign)CGFloat barHeight;

/**
 能否编辑
 */
@property (nonatomic,assign)BOOL editale;

/**
 能否发送图片
 */
@property (nonatomic,assign)BOOL isSendImg;


/**
 textView拼接内容
 */
@property (nonatomic)NSString *textAppendingStr;

-(void)becomeFirstResponder;

-(void)resignFirstResponder;
@end
