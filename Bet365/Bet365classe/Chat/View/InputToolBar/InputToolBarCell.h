//
//  InputToolBarCell.h
//  Bet365
//
//  Created by HHH on 2018/9/17.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InputToolBarCustomItem.h"
@interface InputToolBarCell : UICollectionViewCell
@property (nonatomic,strong)InputToolBarCustomItem *item;
@end
