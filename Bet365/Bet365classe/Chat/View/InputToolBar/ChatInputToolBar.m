//
//  ChatInputToolBar.m
//  Bet365
//
//  Created by HHH on 2018/9/17.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatInputToolBar.h"
#import "AjiTextView.h"
#import "InputToolBarCell.h"
#import "InputToolBarCustomItem.h"
#import <UICollectionViewLeftAlignedLayout/UICollectionViewLeftAlignedLayout.h>
#import "ChatInputEmojiCell.h"
#import "ChatUtility.h"
#import <Aspects/Aspects.h>

#import <AssetsLibrary/ALAsset.h>
#import <AssetsLibrary/ALAssetsLibrary.h>
#import <AssetsLibrary/ALAssetsGroup.h>
#import <AssetsLibrary/ALAssetRepresentation.h>
#import "NSString+RemoveEmoji.h"
#import "ChatViewController.h"
#define InputEmojiCellLeading  15.0
#define InputEmojiCellWidth  (WIDTH - 2*InputEmojiCellLeading) / 8.0
#define MULITTHREEBYTEUTF16TOUNICODE(x,y) (((((x ^ 0xD800) << 2) | ((y ^ 0xDC00) >> 8)) << 8) | ((y ^ 0xDC00) & 0xFF)) + 0x10000

typedef NS_ENUM(NSUInteger, ChatInputToolBarType) {
    /** 无*/
    ChatInputToolBarType_NONE,
    /** 文本输入*/
    ChatInputToolBarType_TEXT,
    /** 表情*/
    ChatInputToolBarType_EMOJI,
    /** 拓展*/
    ChatInputToolBarType_CUSTOM,
};

@interface ChatInputToolBar()
<UICollectionViewDelegate,
UICollectionViewDataSource,
UITextViewDelegate>

@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet AjiTextView *textView;
@property (strong, nonatomic) IBOutlet UIButton *emojiBtn;
@property (strong, nonatomic) IBOutlet UIButton *mortBtn;
@property (strong, nonatomic) IBOutlet UIView *barView;
@property (strong, nonatomic) IBOutlet UIView *emojiContentView;
@property (strong, nonatomic) IBOutlet UIView *customContentView;
@property (strong, nonatomic) IBOutlet UIButton *sendBtn;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *textViewTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *textViewBottom;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *textViEWContentHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *emojiBottomViewContentHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *emojiPageViewContentHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *emojiBottomViewContentTop;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *customContentHeight;

@property (nonatomic,assign)ChatInputToolBarType type;
@property (nonatomic,assign)ChatInputToolBarType beforeType;

@property (nonatomic,strong)NSMutableArray <NSArray <EmojiItem *>*>*emojiList;
@property (nonatomic,strong)NSMutableArray <EmojiItem *>*selectedEmojiList;
@property (nonatomic,strong)NSMutableArray <NSString *>*textList;
@end

@implementation ChatInputToolBar

-(void)dealloc{
    self.collectionView.dataSource = nil;
    self.collectionView.delegate = nil;
    self.textView.delegate = nil;
}


-(void)becomeFirstResponder{
    if (self.beforeType == ChatInputToolBarType_NONE) {
        self.beforeType = ChatInputToolBarType_TEXT;
    }
    self.type = self.beforeType;
}

-(void)resignFirstResponder{
    self.type = ChatInputToolBarType_NONE;
}


-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        self.editale = YES;
        self.isSendImg = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            self.collectionView.delegate = self;
            self.collectionView.dataSource = self;
            self.textView.delegate = self;
            
            [self.sendBtn setBackgroundImage:[UIImage imageWithColor:COLOR_WITH_HEX(0x307BF6)] forState:UIControlStateNormal];
            [self.sendBtn setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateDisabled];
            
            [self.sendBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.sendBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateDisabled];
            
            [self.sendBtn shadowForColor:[UIColor blackColor] shadowWidth:3 shadowType:UIViewShadowTypeLeft];
            
            self.pageControl.numberOfPages = self.emojiList.count;
            self.pageControl.currentPage = 0;
            
            @weakify(self);
            /** 是否禁言**/
            [[RACObserve(self, editale) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
                @strongify(self);
                BOOL editable = [x boolValue];
                self.textView.editable = editable;
                [self.sendBtn setEnabled:editable];
                [self.emojiBtn setEnabled:editable];
                [self.mortBtn setEnabled:editable];
                if (!editable) {
                    self.textView.text = @"";
                    [self.textView resignFirstResponder];
                    self.textView.placeholder = CHAT_UTIL.suspendModel.app_gag_title.length ? CHAT_UTIL.suspendModel.app_gag_title : @"当前禁止发言";
                }else{
                    self.textView.placeholder = @"";
                }
            }];
            
            /** Bar高度**/
            [[RACObserve(self, barHeight) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
                @strongify(self);
                self.textViEWContentHeight.constant = [x floatValue] + self.textViewTop.constant + self.textViewBottom.constant;
                [self layoutIfNeeded];
                [self.superview layoutIfNeeded];
            }];
            
            /** 键盘升起**/
            [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(NSNotification * _Nullable x) {
                
                @strongify(self);
                
                if ([[NAVI_MANAGER getCurrentVC] isKindOfClass:[ChatViewController class]]) {
                    NSDictionary *userInfo = x.userInfo;
                    double duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
                    
                    CGRect keyboardF = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
                    [UIView beginAnimations:nil context:NULL];
                    [UIView setAnimationBeginsFromCurrentState:YES];
                    [UIView setAnimationDuration:duration];
                    [UIView setAnimationCurve:7];
                    //                [self mas_updateConstraints:^(MASConstraintMaker *make) {
                    //                    make.bottom.offset(- keyboardF.size.height);
                    //                }];
//                    CGFloat tabBarHeigh = BET_CONFIG.userSettingData.hideChatBottomBar ? 50.0 : 100.0;
                    self.constraints.lastObject.constant = keyboardF.size.height - 50.0;
                    [self.superview layoutIfNeeded];
                    [UIView commitAnimations];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(duration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        if (self.delegate && [self.delegate respondsToSelector:@selector(ChatInputToolBarBecomeFirstResponder:)]) {
                            [self.delegate ChatInputToolBarBecomeFirstResponder:self];
                        }
                    });
                }
                
                
            }];
            
            /** 键盘收起**/
            [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(NSNotification * _Nullable x) {
                @strongify(self);
                
                if ([[NAVI_MANAGER getCurrentVC] isKindOfClass:[ChatViewController class]]) {
                    NSDictionary *userInfo = x.userInfo;
                    double duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
                    [UIView beginAnimations:nil context:NULL];
                    [UIView setAnimationBeginsFromCurrentState:YES];
                    [UIView setAnimationDuration:duration];
                    [UIView setAnimationCurve:7];
                    //                [self mas_updateConstraints:^(MASConstraintMaker *make) {
                    //                    make.bottom.offset(0);
                    //                }];
                    self.constraints.lastObject.constant = 0;
                    [self.superview layoutIfNeeded];
                    [UIView commitAnimations];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(duration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        if (self.delegate && [self.delegate respondsToSelector:@selector(ChatInputToolBarResignFirstResponder:)]) {
                            [self.delegate ChatInputToolBarResignFirstResponder:self];
                        }
                    });
                }
                
            }];
            
            /** bar类型**/
            [[RACObserve(self, type) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
                @strongify(self);
                [self.collectionView reloadData];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:0.2 animations:^{
                        switch ([x integerValue]) {
                            case ChatInputToolBarType_TEXT:
                                [self.emojiBtn setSelected:NO];
                                [self.mortBtn setSelected:NO];
                                self.emojiPageViewContentHeight.constant = 0;
                                self.emojiBottomViewContentTop.constant = 0;
                                self.emojiBottomViewContentHeight.constant = 0;
                                self.customContentHeight.constant = 0;
                                self.emojiContentView.alpha = 0;
                                self.customContentView.alpha = 0;
                                break;
                            case ChatInputToolBarType_EMOJI:
                            {
                                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatInputToolBarBecomeFirstResponder:)]) {
                                        [self.delegate ChatInputToolBarBecomeFirstResponder:self];
                                    }
                                });
                                self.pageControl.currentPage = 0;
                                [self.emojiBtn setSelected:YES];
                                [self.mortBtn setSelected:NO];
                                self.emojiPageViewContentHeight.constant = 39;
                                self.emojiBottomViewContentTop.constant = InputEmojiCellWidth * 3 + 10 + 39;
                                self.emojiBottomViewContentHeight.constant = 40;
                                self.customContentHeight.constant = 0;
                                self.emojiContentView.alpha = 1;
                                self.customContentView.alpha = 0;
                            }
                                
                                break;
                            case ChatInputToolBarType_CUSTOM:
                            {
                                
//                                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                                    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatInputToolBarBecomeFirstResponder:)]) {
//                                        [self.delegate ChatInputToolBarBecomeFirstResponder:self];
//                                    }
//                                    [self.sendBtn shadowForColor:[UIColor blackColor] shadowWidth:3 shadowType:UIViewShadowTypeLeft];
//                                });
//                                [self.emojiBtn setSelected:NO];
//                                [self.mortBtn setSelected:YES];
//                                self.emojiPageViewContentHeight.constant = 0;
//                                self.emojiBottomViewContentTop.constant = 0;
//                                self.emojiBottomViewContentHeight.constant = 0;
//                                self.customContentHeight.constant = WIDTH / 4.0;
//                                self.emojiContentView.alpha = 0;
//                                self.customContentView.alpha = 1;
                                [self.textView resignFirstResponder];
                                [self.emojiBtn setSelected:NO];
                                [self.mortBtn setSelected:NO];
                                self.emojiPageViewContentHeight.constant = 0;
                                self.emojiBottomViewContentTop.constant = 0;
                                self.emojiBottomViewContentHeight.constant = 0;
                                self.customContentHeight.constant = 0;
                                self.emojiContentView.alpha = 0;
                                self.customContentView.alpha = 0;
                            }
                                break;
                            default:
                            {
                                [self.textView resignFirstResponder];
                                [self.emojiBtn setSelected:NO];
                                [self.mortBtn setSelected:NO];
                                self.emojiPageViewContentHeight.constant = 0;
                                self.emojiBottomViewContentTop.constant = 0;
                                self.emojiBottomViewContentHeight.constant = 0;
                                self.customContentHeight.constant = 0;
                                self.emojiContentView.alpha = 0;
                                self.customContentView.alpha = 0;
                            }
                                break;
                        }
                        [self layoutIfNeeded];
                        [self.superview layoutIfNeeded];
                    }];
                });
            }];
            
            /** 输入内容 **/
            [self.textView.rac_textSignal subscribeNext:^(NSString * _Nullable x) {
                @strongify(self);
//                self.textView.text = [x removedEmojiString];
                [self uploadTextViewHeightBy:x];
            }];
            
            /** 表情发送按钮 **/

            [[RACSignal merge:@[self.textView.rac_textSignal,RACObserve(self.textView,text)]] subscribeNext:^(id  _Nullable x) {
                @strongify(self);
                NSString *value = x;
                [self.sendBtn setEnabled:value.length > 0];
            }];


            
            
        });
    }
    return self;
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.type == ChatInputToolBarType_EMOJI ? self.emojiList.count : 0;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.emojiList[section].count + 1;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
    
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0.0;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    
    return 0.0;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(InputEmojiCellWidth,InputEmojiCellWidth);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ChatInputEmojiCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatInputEmojiCell" forIndexPath:indexPath];
    EmojiItem *item = [self.emojiList[indexPath.section] safeObjectAtIndex:indexPath.item];
    if ([item isKindOfClass:[NSString class]]) {
        cell.item = nil;
        cell.isDelete = NO;
    }else{
        cell.item = item;
        cell.isDelete = (item == nil);
    }
    
    return cell;
}

#pragma mark - UICollectionViewDelegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    ChatInputEmojiCell *cell = (ChatInputEmojiCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if (cell.isDelete) {
        [self deleteTextInput];
    }else if (cell.item){
        NSAttributedString *imageText = [[NSAttributedString alloc] initWithString:cell.item.identifier];
        NSRange range = self.textView.selectedRange;
        NSMutableAttributedString *strM = [[NSMutableAttributedString alloc] initWithAttributedString:self.textView.attributedText];
        [strM replaceCharactersInRange:self.textView.selectedRange withAttributedString:imageText];
        [strM addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(self.textView.selectedRange.location, cell.item.identifier.length)];
        self.textView.attributedText = strM;
        [self.textView.delegate textViewDidChange:self.textView];
        [self uploadTextViewHeightBy:strM];
        self.textView.selectedRange = NSMakeRange(range.location + cell.item.identifier.length,0);
        if (self.textView.selectedRange.location == self.textView.attributedText.length) {
            [self.textView scrollRangeToVisible:NSMakeRange(self.textView.attributedText.length, 1)];
        }
    }
}



#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == self.collectionView) {
        NSInteger idx = scrollView.contentOffset.x / (WIDTH - 2*InputEmojiCellLeading);
        self.pageControl.currentPage = idx;
    }
}

#pragma mark - UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]) {
        [self sendAction:nil];
        return NO;
    }else if(text.length == 0){
        if ([[(UITextInputMode*)[[UITextInputMode activeInputModes] firstObject] primaryLanguage] isEqualToString:@"zh-Hans"]) {
            UITextRange *selectedRange = [textView markedTextRange];
            UITextPosition *position = [textView positionFromPosition:selectedRange.start offset:0];
            if (position){
                return YES;
            }
        }
        [self deleteTextInput];
        return NO;
        
    }else if ([text isEqualToString:@"@"]){
        /** 判断@符与末尾字符是否为空格 **/
        if (self.textView.text.length &&
            [[self.textView.text substringWithRange:NSMakeRange(self.textView.text.length - 1, 1)] isEqualToString:@" "]) {
            return YES;
        }else{
            self.textView.text = [self.textView.text stringByAppendingString:@" "];
        }
        
    }
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.type = ChatInputToolBarType_TEXT;
    });
    
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    
}
#pragma mark - Private
-(void)deleteTextInput{

    
    NSRange range = self.textView.selectedRange;
    NSString *souceText = [self.textView.text substringToIndex:range.location];
    if (range.location == NSNotFound) {
        range.location = self.textView.text.length;
    }
    if (range.length > 0) {
        [self.textView deleteBackward];
        return;
    }else{
        
        NSString * pattern = @"\\[[a-zA-Z0-9\\u4e00-\\u9fa5]+\\]";
        NSError *error = nil;
        NSRegularExpression * re = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
        
        if (!re) {
            NSLog(@"%@", [error localizedDescription]);
        }
        NSArray *resultArray = [re matchesInString:souceText options:0 range:NSMakeRange(0, souceText.length)];
        NSTextCheckingResult *checkingResult = resultArray.lastObject;
        
        for (NSString *faceName in [EmojiItem getKeyList]) {
            if ([souceText hasSuffix:@"]"]) {
                if ([[souceText substringWithRange:checkingResult.range] isEqualToString:faceName]) {
                    NSMutableString *mStr = [self.textView.text mutableCopy];
                    [mStr deleteCharactersInRange:NSMakeRange(range.location - checkingResult.range.length, checkingResult.range.length)];
                    self.textView.text = mStr;
                    self.textView.selectedRange = NSMakeRange(checkingResult.range.location, 0);
                    [self uploadTextViewHeightBy:mStr];
                    return;
                }
            }
        }
        /** 检索" @用户名 "(包含空格)**/
        pattern = @"(?<=\\ @).*?(?=\\ )";
        re = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
        resultArray = [re matchesInString:souceText options:0 range:NSMakeRange(0, souceText.length)];
        checkingResult = resultArray.lastObject;

        if (checkingResult && checkingResult.range.location + checkingResult.range.length + 1 == souceText.length) {
            NSMutableString *mStr = [self.textView.text mutableCopy];
            [mStr deleteCharactersInRange:NSMakeRange(range.location - checkingResult.range.length - 3, checkingResult.range.length + 3)];
            self.textView.text = mStr;
            self.textView.selectedRange = NSMakeRange(checkingResult.range.location - 2, 0);
            [self uploadTextViewHeightBy:mStr];
            return;
        }
        
        [self.textView deleteBackward];
        [self uploadTextViewHeightBy:self.textView.text];
    }
}

-(void)uploadTextViewHeightBy:(id)str{
    
    CGFloat height = [self.textView sizeThatFits:CGSizeMake(CGRectGetWidth(self.textView.bounds), MAXFLOAT)].height;
    
    if (height <= self.textView.font.lineHeight) {
        self.textViEWContentHeight.constant = self.textView.font.lineHeight + 17;
    }else if (height >= self.textView.font.lineHeight * 6){
        self.textViEWContentHeight.constant = self.textView.font.lineHeight * 6;
    }else{
        self.textViEWContentHeight.constant = height;
    }
    [self layoutIfNeeded];
    [self.superview layoutIfNeeded];
}
#pragma mark - Action
- (IBAction)MoreAction:(UIButton *)sender {
//    if (self.type == ChatInputToolBarType_CUSTOM) {
//        self.type = ChatInputToolBarType_TEXT;
//    }else{
//        self.type = ChatInputToolBarType_CUSTOM;
//        [self.textView resignFirstResponder];
//    }
    [self.textView resignFirstResponder];
     self.type = ChatInputToolBarType_NONE;
    if (self.delegate && [self.delegate respondsToSelector:@selector(tapMoreActionChatInputToolBar:)]) {
        [self.delegate tapMoreActionChatInputToolBar:self];
    }
    
}


- (IBAction)EmojiAction:(UIButton *)sender {
    if (self.type == ChatInputToolBarType_EMOJI) {
        self.type = ChatInputToolBarType_TEXT;
    }else{
        self.type = ChatInputToolBarType_EMOJI;
        [self.textView resignFirstResponder];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.collectionView reloadData];
        });
    }
}

-(IBAction)sendAction:(UIButton *)sender{
    if(![self.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length) {
        return;
    }
    if (self.textView.text.length) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(ChatInputToolBar:SendText:)]) {
            
//            if ([self.delegate ChatInputToolBar:self SendText:[self.textView.text removedEmojiString]]) {
//                self.textView.text = @"";
//            }
            NSString *str = [self.textView.text removedEmojiString];
            if (!str.length && self.textView.text.length) {
                self.textView.text = @"";
//                [SVProgressHUD showErrorWithStatus:@"表情编码异常"];
                return;
            }
            if ([self.delegate ChatInputToolBar:self SendText:str]) {
                self.textView.text = @"";
            }
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            self.type = ChatInputToolBarType_NONE;
//            [self.textView resignFirstResponder];
            [self uploadTextViewHeightBy:self.textView.text];
        });
    }
}

-(IBAction)sendMoreAction:(UIButton *)sender{
    switch (sender.tag) {
        case ChatMessageType_IMG:
            [self showImagePicker];
            break;
        case ChatMessageType_RED:
            [SVProgressHUD showImage:[UIImage imageNamed:@"chat_input_red"] status:@"我们将会在近期推出红包功能，敬请期待"];
            break;
        default:
            break;
    }
}

-(void)showImagePicker{
    if (!self.isSendImg) {
        [SVProgressHUD showErrorWithStatus:@"当前房间禁止发送图片"];
        return;
    }
    
}
#pragma mark - GET/SET
-(NSMutableArray<NSArray<EmojiItem *> *> *)emojiList{
    if (!_emojiList) {
        _emojiList = [NSMutableArray array];
        NSMutableArray *list = [EmojiItem creatItems];
        NSUInteger page = list.count / 23 + ((list.count % 23) > 0 ? 1:0);
        __block NSUInteger index = 0;
        for (int i = 0; i<page; i++) {
            NSInteger lenght = (list.count - 1 - index) < 23 ? (list.count - 1 - index) :  23;
            if (lenght < 23) {
                for (int i = 0 ; i<23-lenght; i++) {
                    [list addObject:@""];
                }
                [_emojiList addObject:[list subarrayWithRange:NSMakeRange(index, 23)]];
            }else{
                [_emojiList addObject:[list subarrayWithRange:NSMakeRange(index, 23)]];
            }
            
            index = 0;
            [_emojiList enumerateObjectsUsingBlock:^(NSArray<EmojiItem *> * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                index += obj.count;
            }];
            
        }
    }
    return _emojiList;
}




-(NSMutableArray<EmojiItem *> *)selectedEmojiList{
    if (!_selectedEmojiList) {
        _selectedEmojiList = [NSMutableArray array];
    }
    return _selectedEmojiList;
}
-(UICollectionView *)collectionView{
    if (!_collectionView) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.pagingEnabled = YES;
        [self.emojiContentView addSubview:_collectionView];
        [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.emojiContentView).offset(15);
            make.right.equalTo(self.emojiContentView).offset(-15);
            make.bottom.equalTo(self.emojiContentView.subviews.firstObject.mas_top);
            make.top.equalTo(self.emojiContentView);
        }];
        
        [_collectionView setBackgroundColor:[UIColor clearColor]];
        [_collectionView setShowsHorizontalScrollIndicator:NO];
        [_collectionView registerNib:[UINib nibWithNibName:@"ChatInputEmojiCell" bundle:nil] forCellWithReuseIdentifier:@"ChatInputEmojiCell"];
    }
    return _collectionView;
}

-(void)setTextAppendingStr:(NSString *)textAppendingStr{
    if (textAppendingStr.length && self.editale) {
        self.textView.text = [self.textView.text stringByAppendingString:textAppendingStr];
        [self uploadTextViewHeightBy:self.textView.text];
    }
}

-(void)setType:(ChatInputToolBarType)type{
    self.beforeType = _type;
    _type = type;
}

@end
