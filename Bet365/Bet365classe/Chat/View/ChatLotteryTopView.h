//
//  ChatLotteryTopView.h
//  Bet365
//
//  Created by HHH on 2018/9/18.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LotteryOpenInfoModel.h"
#define LotteryTopBallWidth 17.0
#define LotteryTopBallSpacing 3.0
@class ChatLotteryTopView;

@protocol ChatLotteryTopViewDelegate <NSObject>

@required

-(void)tapIconImageViewAtChatLotteryTopView:(ChatLotteryTopView *)view;

-(void)tapBallContentAtChatLotteryTopView:(ChatLotteryTopView *)view;

-(void)replaceLotteryOpenInfoModel:(LotteryOpenInfoModel *)model;

@end
@interface ChatLotteryTopView : UIView

@property (strong,nonatomic)NSString *lotteryId;

@property (weak,nonatomic)id<ChatLotteryTopViewDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIImageView *iconImageView;

@property (strong, nonatomic) IBOutlet UIView *ballContentView;

@property (assign,nonatomic) BOOL isFirsResponse;

-(void)updateLotteryOpenInfoModel;

@property (strong,nonatomic)LotteryOpenInfoModel *model;

@end
