//
//  ChatNoticeView.h
//  Bet365
//
//  Created by HHH on 2018/12/16.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatNoticeMessageView : UIView

@property (nonatomic,strong)Message *message;

@property (nonatomic,assign,readonly) BOOL isShow;

+(instancetype)shareInstance;

-(void)show;

-(void)hide;

@end
