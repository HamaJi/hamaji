//
//  ChatCellDelegate.h
//  Bet365
//
//  Created by HHH on 2018/9/27.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ChatCell;
@class ChatMessageCell;

@protocol ChatCellDelegate <NSObject>

@required

-(void)ChatCellMessageHasRead:(ChatCell *)cell;

@optional

-(void)ChatMessageCellTapFail:(ChatMessageCell *)cell;

-(void)ChatMessageCellDidTapAvator:(ChatMessageCell *)cell;

-(void)ChatMessageCellDidTapImage:(ChatMessageCell *)cell;

-(void)ChatMessageCellDidTapLongImage:(ChatMessageCell *)cell;

-(void)ChatMessageCellDidTapLottery:(ChatMessageCell *)cell;

-(void)ChatMessageCellDidTapLotteryButton:(ChatMessageCell *)cell;

-(void)ChatMessageCellDidTapLongLottery:(ChatMessageCell *)cell;

-(void)ChatMessageCellDidTapRed:(ChatMessageCell *)cell;

-(void)ChatMessageCellDidTapLongRed:(ChatMessageCell *)cell;

-(void)ChatMessageCellDidTapText:(ChatMessageCell *)cell;

-(void)ChatMessageCellDidLongTapText:(ChatMessageCell *)cell;

-(void)ChatMessageCellCopyText:(ChatMessageCell *)cell;

-(void)ChatMessageCellSaveImg:(ChatMessageCell *)cell;

-(void)ChatMessageCellPopUserDetail:(ChatMessageCell *)cell;

-(void)ChatMessageCellRevokeMessage:(ChatMessageCell *)cell;

-(void)ChatMessageCellFollowMessage:(ChatMessageCell *)cell;

-(void)ChatMessageCellPrivateChat:(ChatMessageCell *)cell;

@end
