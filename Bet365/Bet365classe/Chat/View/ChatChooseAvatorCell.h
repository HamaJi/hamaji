//
//  ChatChooseAvatorCell.h
//  Bet365
//
//  Created by HHH on 2018/10/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatChooseAvatorCell : UICollectionViewCell
@property (nonatomic)NSURL *url;
@property (nonatomic)NSString *path;
@property (nonatomic)NSString *titileString;
@end
