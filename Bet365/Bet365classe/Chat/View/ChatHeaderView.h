//
//  ChatHeaderView.h
//  Bet365
//
//  Created by HHH on 2018/10/9.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
/** 选中 */
typedef void(^ChatHeaderViewTapBlock)();
@interface ChatHeaderView : UIView

@property (nonatomic,strong)NSString *nickName;

@property (nonatomic,strong)NSString *avatorStr;

@property (nonatomic,copy)ChatHeaderViewTapBlock handle;

@end
