//
//  ChatNoticeView.m
//  Bet365
//
//  Created by HHH on 2018/12/16.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatNoticeMessageView.h"
#import "ChatBubbleView.h"
#import <POP/POP.h>
#define CHAT_STICK_SHOWPOP @"chat_stick_showpop"
#define CHAT_STICK_HIDEPOP @"chat_stick_hidepop"

@interface ChatNoticeMessageView()

@property (strong, nonatomic) IBOutlet UIImageView *avatorImg;
@property (strong, nonatomic) IBOutlet UIImageView *levelImg;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet ChatBubbleView *bubbleView;

@end

@implementation ChatNoticeMessageView

static ChatNoticeMessageView *instance = nil;

+(instancetype)shareInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{

        instance = [[[NSBundle mainBundle] loadNibNamed:@"ChatNoticeMessageView" owner:self options:nil] firstObject];
    });
    return instance;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self addObserver];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addObserver];
        
    }
    return self;
}

#pragma mark - Private
-(void)addObserver{
    //    self.layer.anchorPoint = CGPointMake(0.5, 0.0);
}

#pragma mark - Public
-(void)show{
    if (self.isShow) {
        return;
    }
    UIView *superView = [[[UIApplication sharedApplication] delegate] window];
    if (![self isDescendantOfView:superView]) {
        [superView addSubview:self];
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(superView);
            make.bottom.equalTo(superView.mas_top);
            make.height.mas_lessThanOrEqualTo(200);
        }];
    }
    
    _isShow = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        POPSpringAnimation *showSpring = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionY];
        showSpring.fromValue = @(self.bounds.origin.y);
        showSpring.toValue = @(self.bounds.size.height / 2.0 + STATUSAND_NAVGATION_HEIGHT);
        showSpring.springBounciness = 15.0;
        showSpring.removedOnCompletion = false;
        [showSpring setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
            [self mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.right.equalTo(superView);
                make.top.equalTo(superView.mas_top).offset(STATUSAND_NAVGATION_HEIGHT);
                make.height.mas_lessThanOrEqualTo(200);
            }];
        }];
        [self pop_addAnimation:showSpring forKey:CHAT_STICK_SHOWPOP];
    });
    
    
}
-(void)hide{
    if (!self.isShow) {
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        POPBasicAnimation *hideBasic = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerPositionY];
        hideBasic.toValue = @(-self.bounds.size.height);
        
        [hideBasic setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
            _isShow = NO;
            if (self.superview) {
                [self mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.equalTo(self.superview);
                    make.bottom.equalTo(self.superview.mas_top);
                    make.height.mas_lessThanOrEqualTo(200);
                }];
            }
        }];
        [self pop_addAnimation:hideBasic forKey:CHAT_STICK_HIDEPOP];
    });
}
#pragma mark - GET/SET
-(void)setMessage:(Message *)message{
    _message = message;
    ChatUserInfoEntity *entity = [ChatUtilitySubscribeData getUserInfoListEntityByUserId:[message.userId integerValue]].firstObject;
    self.bubbleView.message = message;
    self.nameLabel.text = entity.nickName;
    NSString *imgPatch = [NSString stringWithFormat:@"chat_level_%li",[entity.level integerValue]];
    [self.levelImg setImage:[UIImage imageNamed:imgPatch]];
    self.timeLabel.text = [message.curTime getTimeFormatStringWithFormat:@"HH:mm:ss"];
    [self.avatorImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SerVer_Url,entity.avatar]] placeholderImage:[UIImage imageNamed:@"chat_userAvator_placeHolder"]];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setNeedsUpdateConstraints];
    });
}

-(ChatBubbleView *)bubbleView{
    if (_bubbleView.forceChatFrom == ChatMessageFrom_SYS) {
        _bubbleView.forceChatFrom = ChatMessageFrom_OTHER;
    }
    return _bubbleView;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
