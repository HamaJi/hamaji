//
//  ChatPrivateNewMessageCell.m
//  Bet365
//
//  Created by adnin on 2019/2/18.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatPrivateNewMessageCell.h"
#import <pop/POP.h>
@interface ChatPrivateNewMessageCell()

@property (strong, nonatomic) IBOutlet UIImageView *avatorImageView;
@property (strong, nonatomic) IBOutlet UILabel *nickNameLabel;

@property (strong, nonatomic) IBOutlet UILabel *countLabel;
@property (strong, nonatomic) IBOutlet UIImageView *deleteImageView;

@property (strong, nonatomic)POPBasicAnimation *rotationAnimation;

@property (strong, nonatomic)POPBasicAnimation *scaleAnimation;

@end

@implementation ChatPrivateNewMessageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [[RACSignal combineLatest:@[RACObserve(self, model),RACObserve(self, entity)] reduce:^id (ChatUserInfoModel *model,ChatUserInfoEntity *entity){
        return @(model != nil && entity != nil);
    }] subscribeNext:^(id  _Nullable x) {
        if ([x boolValue]) {
            [self setUI];
        }
    }];
    
    // Initialization code
}

-(void)setUI{
    [self.avatorImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SerVer_Url,self.entity.avatar]] placeholderImage:[UIImage imageNamed:@"chat_userAvator_placeHolder"]];
    self.nickNameLabel.text = self.entity.nickName;
}

-(void)setUnreadCount:(NSInteger)unreadCount{
    _unreadCount = unreadCount;
    self.countLabel.text = [NSString stringWithFormat:@"%li",unreadCount];
    self.countLabel.alpha = (unreadCount && !self.isEditing) ? 1 : 0;
}

-(void)setIsEditing:(BOOL)isEditing{
    if (_isEditing == isEditing) {
        return;
    }
    _isEditing = isEditing;
    if (_isEditing) {
        [self.avatorImageView.layer pop_addAnimation:self.rotationAnimation forKey:@"rotationAnimation"];
//        [self.avatorImageView.layer pop_addAnimation:self.scaleAnimation forKey:@"scaleAnimation"];
        self.countLabel.alpha = 0;
        self.deleteImageView.alpha = 1;
    }else{
        [self.avatorImageView.layer pop_removeAllAnimations];
        self.countLabel.alpha = self.unreadCount ? 1 : 0;
        self.deleteImageView.alpha = 0;
    }
}

-(POPBasicAnimation *)rotationAnimation{
    if (!_rotationAnimation) {
        _rotationAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerRotation];
        _rotationAnimation.toValue = @(-0.07);
        _rotationAnimation.fromValue = @(0.07);
        _rotationAnimation.duration = 0.07;
        _rotationAnimation.repeatCount = HUGE_VALF;
        _rotationAnimation.removedOnCompletion = NO;
        _rotationAnimation.autoreverses = YES;
        [_rotationAnimation setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
            
        }];
    }
    return _rotationAnimation;
}

-(POPBasicAnimation *)scaleAnimation{
    if (!_scaleAnimation) {
        _scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
        _scaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(0.95, 0.95)];
        _scaleAnimation.fromValue = [NSValue valueWithCGSize:CGSizeMake(1.05, 1.05)];
        _scaleAnimation.duration = 0.07;
        _scaleAnimation.repeatCount = HUGE_VALF;
        _scaleAnimation.removedOnCompletion = NO;
        _scaleAnimation.autoreverses = YES;
        [_scaleAnimation setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
            
        }];
    }
    return _scaleAnimation;
}

@end

