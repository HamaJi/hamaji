//
//  ChatPrivateNewMessageView.h
//  Bet365
//
//  Created by adnin on 2019/2/17.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ChatPrivateNewMessageView;

@protocol ChatPrivateNewMessageViewDelegate <NSObject>

-(void)ChatPrivateNewMessageView:(ChatPrivateNewMessageView *)view didSelectUser:(ChatUserInfoModel *)userModel;

-(void)ChatPrivateNewMessageView:(ChatPrivateNewMessageView *)view deleteUser:(ChatUserInfoModel *)userModel;

-(void)tapMoreChatPrivateNewMessageView:(ChatPrivateNewMessageView *)view;

-(void)ChatPrivateNewMessageView:(ChatPrivateNewMessageView *)view UpdateHeight:(CGFloat )height;

@end

@interface ChatPrivateNewMessageView : UIControl

@property (nonatomic,strong)NSMutableArray <ChatUserInfoModel *>*userList;

@property (nonatomic,weak) id<ChatPrivateNewMessageViewDelegate> delegate;


@end
