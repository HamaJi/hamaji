//
//  ChatViewController.h
//  Bet365
//
//  Created by adnin on 2019/2/12.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark - ViewController
#import "ChatRightViewController.h"

#pragma mark - Command
#import "ChatAction.h"
#import "RequestUrlHeader.h"
#import "PresentingBottomAnimator.h"
#import "DismissingBottomAnimator.h"
#import "PresentingAlphaAnimator.h"
#import "DismissingAlphaAnimator.h"
#import <PhotoBrowser/PhotoBrowser.h>

#pragma mark - View
#import "ChatInputToolBar.h"
#import "ChatScrollBottomButton.h"

#pragma mark - Model
#import "ChatRightItem.h"


@interface ChatViewController : Bet365ViewController
<
UIViewControllerTransitioningDelegate,
PBViewControllerDataSource,
PBViewControllerDelegate
>
{
@protected
    
    __strong UITableView *_tableView;
    
    __strong ChatAction *_action;
    
    __strong ChatInputToolBar *_inputToolBar;
    
    __strong ChatScrollBottomButton *_bottomBtn;
}

@property (nonatomic,getter=getTableView,setter=setTableView:) UITableView *tableView;

@property (nonatomic,getter=getAction,setter=setAction:) ChatAction *action;

@property (nonatomic,getter=getInputToolBar,setter=setInputToolBar:) ChatInputToolBar *inputToolBar;

@property (nonatomic,getter=getBottomBtn,setter=setBottomBtn:) ChatScrollBottomButton *bottomBtn;

@property (nonatomic,strong)NSArray <ChatRightItem *>*rightSlideItems;

@property (nonatomic,assign)NSInteger roomID;


+(instancetype)shareInstance;

+(void)showChatViewController;

-(void)clean;

#pragma mark - UI
-(void)setupLeftBackButton;

-(void)setupRightMenuButton;

#pragma mark - Events
-(void)leftDrawerButtonPress:(id)sender;

-(void)rightDrawerButtonPress:(id)sender;

-(void)userDefaultAction;

-(void)rechargeAction;

-(void)drawAction;

-(void)popBuyLotteryAction;

-(void)popUserCenterAction;

@end
