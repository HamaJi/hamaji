//
//  ChatChooseAvatorViewController.h
//  Bet365
//
//  Created by HHH on 2018/10/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ChatChooseAvatorBlock)(NSString *imgStr);

@interface ChatChooseAvatorViewController : Bet365ViewController

@property (nonatomic,copy)ChatChooseAvatorBlock handle;
@end
