//
//  ChatJumpLotteryPopViewController.m
//  Bet365
//
//  Created by HHH on 2018/10/19.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatJumpLotteryPopViewController.h"
#import "ChatChooseLotteryCell.h"
#import "HomeLotteryHeadView.h"

#define kHorMaxCount  4
@interface ChatJumpLotteryPopViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (strong, nonatomic) UICollectionView *collectionView;

@property (strong, nonatomic) UIPageControl *pageControl;

@property (strong, nonatomic) UIView *contentView;

@property (strong, nonatomic) NSMutableArray <KFCPHomeGameJsonModel *> *lotteryList;

@property (strong, nonatomic) NSMutableArray <KFCPHomeGameJsonModel *> *tempList;

@end

@implementation ChatJumpLotteryPopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pageControl.currentPage = 0;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    NSInteger maxCount = self.lotteryList.count;
    return maxCount / (kHorMaxCount * 2) + (maxCount % (kHorMaxCount * 2) > 0 ? 1 : 0);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return kHorMaxCount * 2;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
    
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0.0;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    
    return 0.0;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.collectionView.bounds.size.width / kHorMaxCount, self.collectionView.bounds.size.width / kHorMaxCount);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger idx = chatLotteryIndex(indexPath);
    ChatChooseLotteryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatChooseLotteryCell" forIndexPath:indexPath];
    cell.model = [self.lotteryList safeObjectAtIndex:idx];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger idx = chatLotteryIndex(indexPath);
    KFCPHomeGameJsonModel *model = [self.lotteryList safeObjectAtIndex:idx];
    if (model) {
        [self dismissViewControllerAnimated:YES completion:^{
            self.handle ? self.handle(model.GameId) : nil;
        }];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == self.collectionView) {
        NSInteger idx = scrollView.contentOffset.x / self.collectionView.bounds.size.width;
        self.pageControl.currentPage = idx;
        NSLog(@"pageIdx = %li",idx);
    }
}
#pragma mark - Events
-(void)closeAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(UICollectionView *)collectionView{
    if (!_collectionView) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.pagingEnabled = YES;
        [self.contentView addSubview:_collectionView];
        [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.equalTo(self.contentView);
            make.bottom.equalTo(self.pageControl.mas_top);
            make.height.mas_equalTo(WIDTH / 2.0 + 5);
        }];
        
        [_collectionView setBackgroundColor:[UIColor clearColor]];
        [_collectionView setShowsHorizontalScrollIndicator:NO];
        [_collectionView registerNib:[UINib nibWithNibName:@"ChatChooseLotteryCell" bundle:nil] forCellWithReuseIdentifier:@"ChatChooseLotteryCell"];
    }
    return _collectionView;
}
-(UIView *)contentView{
    if (!_contentView) {
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.view addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        [btn addTarget:self action:@selector(closeAction) forControlEvents:UIControlEventTouchUpInside];
        
        _contentView = [[UIView alloc] init];
        _contentView.backgroundColor = COLOR_WITH_HEX_ALP(0x000000, 0.8);
        [self.view addSubview:_contentView];
        [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.bottom.right.equalTo(self.view);
        }];
        
    }
    return _contentView;
}

-(UIPageControl *)pageControl{
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] init];
        
         NSInteger maxCount = [((NSArray *)[LOTTERY_FACTORY.lotteryClassifyList safeObjectAtIndex:HomePageCreditType]) count];
        _pageControl.numberOfPages = maxCount / (kHorMaxCount * 2) + (maxCount %(kHorMaxCount * 2) >0 ? 1 : 0);
        
        [self.contentView addSubview:_pageControl];
        [_pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.centerX.equalTo(self.contentView);
        }];
    }
    return _pageControl;
}

-(void)setChannelID:(NSInteger)channelID{
    __block ChatRoomInfoModel *roomInfo = nil;
    [CHAT_UTIL.clientInfo.roomInfoList enumerateObjectsUsingBlock:^(ChatRoomInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.identity integerValue] == channelID) {
            roomInfo = obj;
            *stop = YES;
        }
    }];
    if (roomInfo.planGameList.count) {
        [roomInfo.planGameList enumerateObjectsUsingBlock:^(NSString * _Nonnull lotteryId, NSUInteger lotteryIdx, BOOL * _Nonnull lotteryStop) {
            [LOTTERY_FACTORY.lotteryData enumerateObjectsUsingBlock:^(KFCPHomeGameJsonModel * _Nonnull model, NSUInteger modelIdx, BOOL * _Nonnull modelStop) {
                if ([model.GameId isEqualToString:lotteryId])
                    [self.lotteryList addObject:model];
            }];
        }];
        // 修改為H5網頁聊天室下方購彩，清單排序方式
        // 將LotteryList分成上下兩段
        // viewcontroller第一排為上半段，第二排為下半段
        __block int pagesSecondRowFirstIndex = 4;
        NSInteger count = self.lotteryList.count;
        [self.tempList addObjectsFromArray:self.lotteryList];
        [self.tempList removeObjectsInRange:NSMakeRange(0,(count / 2 + count % 2))];
        [self.lotteryList removeObjectsInRange:NSMakeRange((count / 2 + count % 2), (count - (count / 2 + count % 2)))];
        [self.tempList enumerateObjectsUsingBlock:^(KFCPHomeGameJsonModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (pagesSecondRowFirstIndex < count) {
                [self.lotteryList insertObject:obj atIndex:((idx % 4) + pagesSecondRowFirstIndex)];
                if ((idx + 1) % 4 == 0)
                    pagesSecondRowFirstIndex += 8;
            } else {
                [self.lotteryList addObject:obj];
            }
        }];
    } else
        [self.lotteryList addObjectsFromArray:LOTTERY_FACTORY.lotteryData];
    [self.collectionView reloadData];
}

-(NSMutableArray<KFCPHomeGameJsonModel *> *)lotteryList{
    if (!_lotteryList) {
        _lotteryList = @[].mutableCopy;
    }
    return _lotteryList;
}
-(NSMutableArray <KFCPHomeGameJsonModel *> *)tempList{
    if (!_tempList) {
        _tempList = @[].mutableCopy;
    }
    return _tempList;
}

NSInteger chatLotteryIndex(NSIndexPath *indexPath){
    NSInteger idx = indexPath.section * kHorMaxCount * 2;
    if (indexPath.item % 2) {
        idx += kHorMaxCount + indexPath.item / 2;
    }else{
        idx += indexPath.item / 2;
    }
    return idx;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
