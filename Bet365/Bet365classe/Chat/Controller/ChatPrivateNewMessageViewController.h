//
//  ChatPrivateNewMessageViewController.h
//  Bet365
//
//  Created by adnin on 2019/7/29.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class ChatPrivateNewMessageViewController;

@protocol ChatPrivateNewMessageListDelegate <NSObject>

-(void)chatPrivateNewMessageListRemoveUserInfo:(ChatUserInfoModel *)userInfo;

-(void)chatPrivateNewMessageListController:(ChatPrivateNewMessageViewController *)vc didSelectUser:(ChatUserInfoModel *)userModel;

@end

@interface ChatPrivateNewMessageViewController : Bet365ViewController

@property (nonatomic,weak)NSArray <ChatUserInfoModel *>*userList;

@property (nonatomic,weak) id<ChatPrivateNewMessageListDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
