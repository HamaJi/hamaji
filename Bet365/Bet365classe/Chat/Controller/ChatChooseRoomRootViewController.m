//
//  ChatChooseRoomRootViewController.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/11.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "ChatChooseRoomRootViewController.h"
#import "TrapezoidalView.h"
#import "ChatGroupViewController.h"

@interface ChooseRoomRootCell : UICollectionViewCell

@property (nonatomic,strong) UIImageView *iconImageView;

@property (nonatomic,strong) UILabel *titileLabel;

@property (nonatomic,strong) TrapezoidalView *trapezoidalView;

@property (nonatomic,strong) UILabel *tagLabel;

@end

@implementation ChooseRoomRootCell



-(instancetype)init{
    if (self = [super init]) {
        self.contentView.backgroundColor = [UIColor skinViewBgColor];
        self.contentView.clipsToBounds = YES;
        self.contentView.cornerRadius = 10;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        self.contentView.backgroundColor = [UIColor skinViewBgColor];
        self.contentView.clipsToBounds = YES;
        self.contentView.cornerRadius = 10;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.contentView.backgroundColor = [UIColor skinViewBgColor];
        self.contentView.clipsToBounds = YES;
        self.contentView.cornerRadius = 10;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

-(UIImageView *)iconImageView{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_iconImageView];
        [_iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.equalTo(self.contentView);
        }];
    }
    return _iconImageView;
}

-(UILabel *)titileLabel{
    if (!_titileLabel) {
        _titileLabel = [UILabel setAllocLabelWithText:@"" FontOfSize:12 rgbColor:0x000000];
        _titileLabel.textColor = [UIColor skinTextItemNorColor];
        _titileLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_titileLabel];
        [_titileLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.iconImageView.mas_bottom);
            make.left.bottom.right.equalTo(self.contentView);
            make.height.mas_equalTo(20);
        }];
    }
    return _titileLabel;
}

-(UILabel *)tagLabel{
    if (!_tagLabel) {
        _tagLabel = [UILabel setAllocLabelWithText:@"" FontOfSize:10 rgbColor:0x000000];
        _tagLabel.font = [UIFont boldSystemFontOfSize:10];
        _tagLabel.textColor = [UIColor skinViewKitNorColor];
        [self.trapezoidalView addSubview:_tagLabel];
        [_tagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.trapezoidalView);
            make.left.equalTo(self.trapezoidalView).offset(10);
        }];
    }
    return _tagLabel;
}

-(TrapezoidalView *)trapezoidalView{
    if (!_trapezoidalView) {
        _trapezoidalView = [[TrapezoidalView alloc] init];
        _trapezoidalView.backgroundColor = [UIColor skinViewKitSelColor];
        _trapezoidalView.bottomRightOffset = TrapezoidalOffsetMake(10, 0);
        _trapezoidalView.alpha = 0;
        [self.contentView addSubview:_trapezoidalView];
        [_trapezoidalView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.equalTo(self.contentView);
            make.height.mas_equalTo(15);
            make.width.mas_equalTo(50);
        }];
    }
    return _trapezoidalView;
}

@end

@interface ChatChooseRoomRootViewController ()
<UICollectionViewDataSource,
UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;

@property (weak, nonatomic) IBOutlet UILabel *titileLabel;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) NSArray <ChatRoomInfoModel *>*roomInfoList;

@end

@implementation ChatChooseRoomRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.naviRightItem = Bet365NaviRightItemMenu | Bet365NaviRightItemBalance;
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    self.iconImageView.image = [[UIImage imageNamed:@"chat_chooseroom_root_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.iconImageView.tintColor = [UIColor skinViewKitSelColor];
    self.titileLabel.textColor = [UIColor skinViewKitSelColor];
    self.roomInfoList = CHAT_UTIL.clientInfo.roomInfoList;
    [self.collectionView registerClass:ChooseRoomRootCell.class forCellWithReuseIdentifier:@"ChooseRoomRootCell"];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.roomInfoList.count;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSUInteger count = 3;
    CGFloat width = (self.collectionView.bounds.size.width - (count + 1) * 10) / (count * 1.0);
    return CGSizeMake(width,width + 30);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ChooseRoomRootCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChooseRoomRootCell" forIndexPath:indexPath];
    
    ChatRoomInfoModel *model = [self.roomInfoList objectAtIndex:indexPath.item];
    [cell.iconImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/data/image/room_icon/%li.jpg",SerVer_Url,[model.identity integerValue]]] placeholderImage:[UIImage imageNamed:@"chat_userAvator_placeHolder"]];
    cell.titileLabel.text = model.name;
    cell.trapezoidalView.alpha = [ChatGroupViewController shareInstance].roomID == [model.identity integerValue] ? 1 : 0;
    cell.tagLabel.text = [ChatGroupViewController shareInstance].roomID == [model.identity integerValue] ? @"在线" : @"";
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    ChatRoomInfoModel *model = [self.roomInfoList objectAtIndex:indexPath.item];
    [ChatGroupViewController shareInstance].roomID = [model.identity integerValue];
    [UIViewController routerJumpToUrl:kRouterChatScrollBottom];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
