//
//  ChatImageEmojiPickViewController.h
//  Bet365
//
//  Created by adnin on 2020/4/25.
//  Copyright © 2020 jesse. All rights reserved.
//

#import "Bet365ViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^ChatChooseNetEmojiBlock)(NetEmojiModel *emoji);

typedef void(^ChatTapImagePickHandle)();

@interface ChatImageEmojiPickViewController : Bet365ViewController

@property (nonatomic,copy) ChatChooseNetEmojiBlock emojiBlock;

@property (nonatomic,copy) ChatTapImagePickHandle imageBlock;

@end

NS_ASSUME_NONNULL_END
