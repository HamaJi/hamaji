//
//  ChatJumpLotteryPopViewController.h
//  Bet365
//
//  Created by HHH on 2018/10/19.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ChatJumpLotteryBlock)(NSString *lotteryID);

@interface ChatJumpLotteryPopViewController : Bet365ViewController
@property (nonatomic,copy)ChatJumpLotteryBlock handle;
@property (nonatomic,assign)NSInteger channelID;
@end
