//
//  ChatViewController.m
//  Bet365
//
//  Created by adnin on 2019/2/12.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatViewController.h"
#import "ChatViewController+Extension.h"
#import "Bet365GameReportViewController.h"
#import "LukeRecordViewController.h"
#import "Bet365GameReportViewController.h"
#pragma mark - ViewController
#import "ChatGroupViewController.h"
#import "LukeDrawViewController.h"
#import "SportsModuleShare.h"
#import "KFCPActivityController.h"
#import "LukeLiveViewController.h"
#import "Bet365QPViewController.h"
#import "ElecWZRYAllViewController.h"
#import "TuLongClassFicationViewController.h"
#import "Bet365FootBallViewController.h"
#import "CustomWebViewController.h"
#import "ChatSeetingViewController.h"
#import "ChatTopListViewController.h"
#import "ChatJumpLotteryPopViewController.h"
#import "SecRechargeViewController.h"
#import "Bet365TuLongController.h"
#import "ChatChooseRoomRootViewController.h"
#pragma mark - Command
#import <SDWebImage/UIButton+WebCache.h>
#import "ChatViewController+Extension.h"
#import "UIViewControllerSerializing.h"
#import "AllLiveData.h"

@interface ChatViewController ()<UIViewControllerSerializing>

@end

static ChatViewController *instance = nil;

RouterKey *const kRouterChatScrollBottom = @"chatJumpping";

RouterKey *const kRouterChatRoom = @"chatRoom";

RouterKey *const kRouterChatIndex = @"chat/index.html";

RouterKey *const kRouterChatRoot = @"chatRoot";

@implementation ChatViewController

+(void)load{
    
    [self registerJumpRouterKey:kRouterChatRoot toHandle:^(NSDictionary *parameters) {
        ChatChooseRoomRootViewController *vc = [[ChatChooseRoomRootViewController alloc] initWithNibName:@"ChatChooseRoomRootViewController" bundle:nil];
        vc.hidesBottomBarWhenPushed = YES;
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
    }];

    [self registerJumpRouterKey:kRouterChatScrollBottom toHandle:^(NSDictionary *parameters) {
        [self showChatViewController];
    }];
    
    [self registerJumpRouterKey:kRouterChatIndex toHandle:^(NSDictionary *parameters) {
        NSString *room = parameters[@"room"];
        if ([room integerValue] > 0) {
            [ChatGroupViewController shareInstance].roomID = [room integerValue];
            [self showChatViewController];
        }else if (![[NSString stringWithFormat:@"/%@",kRouterChatIndex] isEqualToString:BET_CONFIG.common_config.chatRoomHref]) {
            ChatChooseRoomRootViewController *vc = [[ChatChooseRoomRootViewController alloc] initWithNibName:@"ChatChooseRoomRootViewController" bundle:nil];
            vc.hidesBottomBarWhenPushed = YES;
            [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        }else{
            [self showChatViewController];
        }
    }];
    
    [self registerJumpRouterKey:kRouterChatRoom toHandle:^(NSDictionary *parameters) {
        if (![[NSString stringWithFormat:@"#/%@",kRouterChatScrollBottom] isEqualToString:BET_CONFIG.common_config.chatRoomHref]) {
            ChatChooseRoomRootViewController *vc = [[ChatChooseRoomRootViewController alloc] initWithNibName:@"ChatChooseRoomRootViewController" bundle:nil];
            vc.hidesBottomBarWhenPushed = YES;
            [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        }else{
            [self showChatViewController];
        }
        
    }];
}

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    if (!BET_CONFIG.common_config.isShowChat){
        [SVProgressHUD showErrorWithStatus:@"聊天室未开启"];
        return NO;
    }
    RouterKey *router = parameters[@"routerKey"];
    switch (CHAT_UTIL.infoStatus) {
        case ChatUtilityClineInfoStatus_UNREQUEST:
        {
            [SVProgressHUD showWithStatus:@"数据加载中"];
            [CHAT_UTIL getChatClienInfo:^(ChatInfoModel *info) {
                [SVProgressHUD dismiss];
                if (info) {
                    [UIViewController routerJumpToUrl:router];
                }
            }];
        }
            return NO;
        case ChatUtilityClineInfoStatus_REQUESTING:
        {
            [SVProgressHUD showInfoWithStatus:@"数据加载中"];
        }
            return NO;
        case ChatUtilityClineInfoStatus_FAIL:
        {
            if (CHAT_UTIL.error.chatFailCode) {
                [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@聊天室维护中",CHAT_UTIL.error.msg]];
            }else{
                [SVProgressHUD showErrorWithStatus:CHAT_UTIL.error.msg];
            }
        }
            return NO;
        default:
            return YES;
    }
}

+(instancetype)shareInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

+(void)showChatViewController{
    UIViewController *currentViewController = [NAVI_MANAGER getCurrentVC];
    if ([currentViewController isEqual:[ChatGroupViewController shareInstance]]) {
        return;
    }
    NSInteger index = [currentViewController.navigationController indexAtViewController:[ChatGroupViewController shareInstance]];
    if (index != NSNotFound) {
        [currentViewController.navigationController popToViewController:[ChatGroupViewController shareInstance] animated:YES];
        [[ChatGroupViewController shareInstance] setHidesBottomBarWhenPushed:YES];
    }else{
        [currentViewController.navigationController pushViewController:[ChatGroupViewController shareInstance] animated:YES];
        
        [[ChatGroupViewController shareInstance] setHidesBottomBarWhenPushed:YES];
    }
}

-(instancetype)init{
    if (self = [super init]) {
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(_tableView && _action){
        [self.action scrollBottomAnimated:YES];
    }
       
}



- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
    [IQKeyboardManager sharedManager].enable = YES;
    
}

@dynamic tableView;

@dynamic action;

@dynamic inputToolBar;

@dynamic bottomBtn;

- (void)viewDidLoad {

    [super viewDidLoad];
    self.naviRightItem = Bet365NaviRightItemNone;
    
    [self setupRightMenuButton];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    @weakify(self);
    [self cw_registerShowIntractiveWithEdgeGesture:YES transitionDirectionAutoBlock:^(CWDrawerTransitionDirection direction) {
        @strongify(self);
        if (direction == CWDrawerTransitionFromRight) {
            [self scaleYAnimationFromRight];
        }
    }];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
-(void)clean{
    if (_action) {
        _action = nil;
    }
    if (_tableView) {
        [_tableView removeFromSuperview];
        _tableView = nil;
    }
    if (_inputToolBar) {
        [_inputToolBar removeFromSuperview];
        _inputToolBar = nil;
    }
    if (_bottomBtn) {
        [_bottomBtn removeFromSuperview];
        _bottomBtn = nil;
    }
}
#pragma mark - UI
-(void)setupLeftBackButton{
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setTitle:@"返回" forState:UIControlStateNormal];
    UIImage *img = [UIImage imageNamed:@ "left_lage" ];
    img = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [leftBtn setImage:img forState:UIControlStateNormal];
    [leftBtn setTintColor:[UIColor skinNaviTintColor]];
    [leftBtn setTitleColor:[UIColor skinNaviTintColor] forState:UIControlStateNormal];
    [leftBtn setImagePosition:LXMImagePositionLeft spacing:5];

    [leftBtn addTarget:self action:@selector(leftDrawerButtonPress:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftButton;
}

-(void)setupRightMenuButton{
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn.frame= CGRectMake(0, 0, 30, 30);
    [btn setImage:[UIImage imageNamed:@"chat_jumpMore"] forState:UIControlStateNormal];
    btn.clipsToBounds = YES;
    btn.layer.cornerRadius = 15;
    [btn addTarget:self action:@selector(rightDrawerButtonPress:) forControlEvents:UIControlEventTouchUpInside];
    [btn setImagePosition:LXMImagePositionLeft spacing:3];
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    if (USER_DATA_MANAGER.isLogin) {
        self.navigationItem.rightBarButtonItems = @[rightBarItem,[[UIBarButtonItem alloc]initWithCustomView:self.naviHeaderView]];
    }else{
        
        UIButton *visiter = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        visiter.frame= CGRectMake(0, 0, 80, 20);
        [visiter setTitle:@"登录/注册" forState:UIControlStateNormal];
        [visiter addTarget:self action:@selector(visitorLogin:) forControlEvents:UIControlEventTouchUpInside];
//        [visiter setBackgroundImage:[UIImage imageWithColor:COLOR_WITH_HEX(0xCD3733)]  forState:UIControlStateNormal];
//        visiter.clipsToBounds = YES;
//        visiter.layer.cornerRadius = 10;
        [visiter setImagePosition:LXMImagePositionLeft spacing:3];
        UIBarButtonItem *visiteritem = [[UIBarButtonItem alloc] initWithCustomView:visiter];
        
        self.navigationItem.rightBarButtonItems = @[rightBarItem,visiteritem];
    }
    
}

#pragma mark - UIViewControllerTransitioningDelegate
- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source{
    if ([self respondsToSelector:@selector(presenteAnimation:)]) {
        return [self presenteAnimation:presented];
    }
    return nil;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    if ([self respondsToSelector:@selector(dismissAnimation:)]) {
        return [self dismissAnimation:dismissed];
    }
    return nil;
}


#pragma mark - Events
-(void)leftDrawerButtonPress:(id)sender{
    [self.action resignAllFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)rightDrawerButtonPress:(id)sender{
    [self scaleYAnimationFromRight];
}

-(void)visitorLogin:(id)sender{
    [USER_DATA_MANAGER shouldPush2Login];
}

- (void)scaleYAnimationFromRight {
    [self.action resignAllFirstResponder];
    ChatRightViewController *vc = [[ChatRightViewController alloc] init];
    vc.rithgItems = self.rightSlideItems;
    @weakify(self);
    vc.handle = ^(ChatRightItem *model) {
        @strongify(self);
        if (model) {
            [self jumpAction:model.tag];
        }else{
            [self userDefaultAction];
        }
    };
//    CWLateralSlideConfiguration *conf = [CWLateralSlideConfiguration configurationWithDistance:0 maskAlpha:0.4 scaleY:0.8 direction:CWDrawerTransitionFromRight backImage:[UIImage imageNamed:@"chat_right_bg"]];
    CWLateralSlideConfiguration *conf = [CWLateralSlideConfiguration defaultConfiguration];
        conf.direction = CWDrawerTransitionFromRight; // 从右边滑出
        conf.finishPercent = 0.1f;
        conf.showAnimDuration = 1.0;
    //    conf.HiddenAnimDuration = 1.0;
    //    conf.maskAlpha = 0.1;
        conf.backImage = [UIImage imageNamed:@"chat_right_bg"];
        conf.scaleY = 0.8;
        
        [self cw_showDrawerViewController:vc animationType:CWDrawerAnimationTypeDefault configuration:conf];

}

-(void)jumpAction:(NSInteger)idx{
    [self.action resignAllFirstResponder];
    ChatSlideMenuModel *model = [CHAT_UTIL.slideMenulist safeObjectAtIndex:idx];
    if ([model.type isEqualToString:@"online-customer"]) {
//        [UIViewController routerJumpToUrl:kRouterZXKF];
//        /** 蛤蟆吉TODO*/
//        /** 在线客服**/
        CustomWebViewController *web = [[CustomWebViewController alloc] init];
        [web loadUrl:BET_CONFIG.common_config.zxkfPath Operation:CustomWebRequestOperation_WEB];
        [self.navigationController pushViewController:web animated:YES];
    }else if ([model.type isEqualToString:@"gcdt"]) {
        /** root彩票**/        
        [UIViewController routerJumpToUrl:kRouterGCDT];
    }else if ([model.type isEqualToString:@"recharge"]){
        
        /** 充值**/
        SecRechargeViewController *vc = [[SecRechargeViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }else if ([model.type isEqualToString:@"withdraw"]){
        /** 提现**/
        LukeDrawViewController *vc = [[LukeDrawViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }else if ([model.type isEqualToString:@"qp"]){
        /** 棋牌**/
        LukeLiveViewController *VC = [[LukeLiveViewController alloc] init];
        VC.gameType = GameCenterType_chess;
        [self.navigationController pushViewController:VC animated:YES];
        VC.hidesBottomBarWhenPushed = YES;
    }else if ([model.type isEqualToString:@"tulongbang"]){
        /** 长龙**/
        Bet365TuLongController *tulong = [[Bet365TuLongController alloc]init];
        [self.navigationController pushViewController:tulong animated:YES];
        tulong.hidesBottomBarWhenPushed = YES;
    }else if ([model.type isEqualToString:@"sport"]){
        /** 体育**/
        LukeLiveViewController *VC = [[LukeLiveViewController alloc] init];
        VC.gameType = GameCenterType_sports;
        [self.navigationController pushViewController:VC animated:YES];
        VC.hidesBottomBarWhenPushed = YES;
    }else if ([model.type isEqualToString:@"mine"]){
        /** root首页**/
        [self popUserCenterAction];
    }else if ([model.type isEqualToString:@"ele"]){
        /** 电子游戏**/
        LukeLiveViewController *VC = [[LukeLiveViewController alloc] init];
        VC.gameType = GameCenterType_ele;
        [self.navigationController pushViewController:VC animated:YES];
        VC.hidesBottomBarWhenPushed = YES;
    }else if ([model.type isEqualToString:@"chess_ky"]){
        /** 开元**/
        Bet365QPViewController *VC = [[Bet365QPViewController alloc] init];
        VC.type = @"ky";
        [self.navigationController pushViewController:VC animated:YES];
    }else if ([model.type isEqualToString:@"chess_jb"]){
        /** 金宝**/
        Bet365QPViewController *VC = [[Bet365QPViewController alloc] init];
        VC.type = @"jb";
        [self.navigationController pushViewController:VC animated:YES];
        VC.hidesBottomBarWhenPushed = YES;
    }else if ([model.type isEqualToString:@"wzry"]){
        /** 王者荣耀**/
        ElecWZRYAllViewController *vc = [[ElecWZRYAllViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }else if ([model.type containsString:@"game/"]){
        /** 彩票**/
        [self pushViewControllerWithGameId:[NSString stringWithFormat:@"%ld",[[model.type componentsSeparatedByString:@"/"].lastObject integerValue]]];
    }else if ([model.type isEqualToString:@"ag"]){
        /** ag视讯**/
        [self pushKFCPActivityControllerWithType:@"ag" gameId:@"1"];
    }else if ([model.type isEqualToString:@"lmg"]){
        /** lmg视讯**/
        [self pushKFCPActivityControllerWithType:@"lmg" gameId:@""];
    }else if ([model.type isEqualToString:@"bbin"]){
        /** bbin视讯**/
        [self pushKFCPActivityControllerWithType:@"bbin" gameId:@""];
    }else if ([model.type isEqualToString:@"mg"]){
        /** mg视讯**/
        [self pushKFCPActivityControllerWithType:@"mg" gameId:@""];
    }else if ([model.type isEqualToString:@"OG"]){
        /** OG视讯**/
        [self pushKFCPActivityControllerWithType:@"OG" gameId:@""];
    }else if ([model.type isEqualToString:@"DG"]){
        /** DG视讯**/
        [self pushKFCPActivityControllerWithType:@"DG" gameId:@""];
    }else if ([model.type isEqualToString:@"ds"]){
        /** ds视讯**/
        [self pushKFCPActivityControllerWithType:@"ds" gameId:@""];
    }else if ([model.type isEqualToString:@"yhhd"]){
        /** 优惠活动**/
        CustomWebViewController *vc = [[CustomWebViewController alloc] initWithNibName:@"CustomWebViewController" bundle:nil];
        [vc loadUrl:[NSString stringWithFormat:@"%@%@",SerVer_Url,ActivityhtmlUrl] Operation:CustomWebRequestOperation_WEB];
        [self.navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }else if ([model.type isEqualToString:@"live"]){
        /** 真人**/
        LukeLiveViewController *VC = [[LukeLiveViewController alloc] init];
        VC.gameType = GameCenterType_live;
        [self.navigationController pushViewController:VC animated:YES];
        VC.hidesBottomBarWhenPushed = YES;
    }else if ([model.type isEqualToString:@"sbsport"]){
        /** 沙巴**/
        [self pushKFCPActivityControllerWithType:@"sb" gameId:@""];
    }else if ([model.type isEqualToString:@"hgsport"]){
        /** 皇冠**/
        Bet365FootBallViewController *footBallVC = [[Bet365FootBallViewController alloc] init];
        footBallVC.sportType = @"ft";
        [self.navigationController pushViewController:footBallVC animated:YES];
        footBallVC.hidesBottomBarWhenPushed = YES;
    }else if ([model.type isEqualToString:@"bbsport"]){
        /** bb**/
        [self pushKFCPActivityControllerWithType:@"bb" gameId:@""];
    }else if ([model.type isEqualToString:@"m8sport"]){
        /** m8**/
        [self pushKFCPActivityControllerWithType:@"m8" gameId:@""];
    }else if([model.type isEqualToString:@"lucky"] ||
             [model.type isEqualToString:@"avia"]){
        /** lucky棋牌,泛亚原生列表**/
        Bet365QPViewController *vc = [[Bet365QPViewController alloc] init];
        vc.type = model.type;
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([model.type isEqualToString:@"cp-bet-list"]){
        /*彩票注单*/
        LukeRecordViewController *vc = [[LukeRecordViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }else if ([model.type hasPrefix:@"live-report-"]){
        if (!USER_DATA_MANAGER.isLogin) {
            [SVProgressHUD showErrorWithStatus:@"游客无法查看注单，请登录"];
            return;
        }
        /*其他注单*/
        if ([model.type hasSuffix:@"vr"]) {
            Bet365GameReportViewController *reportVC = [[Bet365GameReportViewController alloc] init];
            reportVC.type = GameCenterType_vr;
            [self.navigationController pushViewController:reportVC animated:YES];
            reportVC.hidesBottomBarWhenPushed = YES;
        }else if([model.type hasSuffix:@"live"]){
            Bet365GameReportViewController *reportVC = [[Bet365GameReportViewController alloc] init];
            reportVC.type = GameCenterType_live;
            [self.navigationController pushViewController:reportVC animated:YES];
            reportVC.hidesBottomBarWhenPushed = YES;
        }else if([model.type hasSuffix:@"sport"]){
            Bet365GameReportViewController *reportVC = [[Bet365GameReportViewController alloc] init];
            reportVC.type = GameCenterType_sports;
            [self.navigationController pushViewController:reportVC animated:YES];
            reportVC.hidesBottomBarWhenPushed = YES;
        }else if([model.type hasSuffix:@"electronic"]){
            Bet365GameReportViewController *reportVC = [[Bet365GameReportViewController alloc] init];
            reportVC.type = GameCenterType_ele;
            [self.navigationController pushViewController:reportVC animated:YES];
            reportVC.hidesBottomBarWhenPushed = YES;
        }else if([model.type hasSuffix:@"chess"]){
            Bet365GameReportViewController *reportVC = [[Bet365GameReportViewController alloc] init];
            reportVC.type = GameCenterType_chess;
            [self.navigationController pushViewController:reportVC animated:YES];
            reportVC.hidesBottomBarWhenPushed = YES;
        }
    }
}

- (void)pushViewControllerWithGameId:(NSString *)gameId{
    [self.action resignAllFirstResponder];
    BetTotal365ViewController *totalC = [[BetTotal365ViewController alloc]init];
    totalC.view.backgroundColor = [UIColor whiteColor];
    totalC.isTuLongEnter = NO;
    [totalC replacekindWayType];
    void (^addCreditChild)() = ^(){
        [totalC addChildController];
        [totalC fristIsOfficalBy:0];
    };
    void (^addOfficalChild)() = ^(){
        [totalC addofficalSonController];
        [totalC fristIsOfficalBy:1];
    };
    if (((NSArray *)LOTTERY_FACTORY.lotteryClassifyList[0]).count==0) {
        BET_CONFIG.kind_type = LotteryKind_CREDIT;
        addCreditChild();
    }else{
        LotteryKind kind_type = check_Status(gameId);
        BET_CONFIG.kind_type = kind_type;
        if (kind_type==LotteryKind_EXIST) {
            ([BET_CONFIG.config.play_type_config integerValue]==1)?addCreditChild():addOfficalChild();
        }else{
            (kind_type==LotteryKind_CREDIT)?addCreditChild():addOfficalChild();
        }
    }
    [totalC pushKindTypeByParameterNumber:[NSString stringWithFormat:@"%@",gameId]];
    [self.navigationController pushViewController:totalC animated:YES];
    totalC.hidesBottomBarWhenPushed = YES;
//    [UIViewController enterLotteryByLotteryId:gameId];
}

- (void)pushKFCPActivityControllerWithType:(NSString *)type gameId:(NSString *)gameId{
    [self.action resignAllFirstResponder];
    if ([USER_DATA_MANAGER shouldPush2Login]) {
        if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"TEST"]) {
            [SVProgressHUD showErrorWithStatus:@"试玩帐号不能进入游戏，请注册成为会员"];
        }else if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"VHY"]) {
            [SVProgressHUD showErrorWithStatus:@"推广帐号不能进入游戏"];
        }else{
            if ([ALL_DATA.liveDatas filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"liveCode == %@",type]].count) {
                AllLiveGamesModel *liveModel = [[ALL_DATA.liveDatas filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"liveCode == %@",type]] safeObjectAtIndex:0];
                CustomWebViewController *vc = [[CustomWebViewController alloc] initWithNibName:@"CustomWebViewController" bundle:nil];
                [vc loadCode:liveModel.liveCode Type:liveModel.gameType Kind:liveModel.kind Operation:CustomWebRequestOperation_WK];
                [self.navigationController pushViewController:vc animated:YES];
                vc.hidesBottomBarWhenPushed = YES;
            }else if ([ALL_DATA.sportsDatas filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"liveCode == %@",type]].count) {
                AllLiveGamesModel *liveModel = [[ALL_DATA.sportsDatas filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"liveCode == %@",type]] safeObjectAtIndex:0];
                CustomWebViewController *vc = [[CustomWebViewController alloc] initWithNibName:@"CustomWebViewController" bundle:nil];
                [vc loadCode:liveModel.liveCode Type:liveModel.gameType Kind:liveModel.kind Operation:CustomWebRequestOperation_WK];
                [self.navigationController pushViewController:vc animated:YES];
                vc.hidesBottomBarWhenPushed = YES;
            }
        }
    }
}
-(void)userDefaultAction{
    [self.action resignAllFirstResponder];
    ChatSeetingViewController *vc = [[ChatSeetingViewController alloc] initWithNibName:@"ChatSeetingViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
    vc.hidesBottomBarWhenPushed = YES;
}

-(void)jumpToLottery:(NSString *)lotteryID{
    [self.action resignAllFirstResponder];
    if ([USER_DATA_MANAGER shouldPush2Login]) {
        [UIViewController enterLotteryByLotteryId:lotteryID];
    }
}


-(void)popBuyLotteryAction{
    ChatJumpLotteryPopViewController *vc = [[ChatJumpLotteryPopViewController alloc] init];
    vc.transitioningDelegate = self;
    vc.modalPresentationStyle = UIModalPresentationCustom;
    @weakify(self);
    vc.handle = ^(NSString *lotteryID) {
        @strongify(self);
        
        [self jumpToLottery:lotteryID];
    };
    vc.channelID = self.roomID;
    [self presentViewController:vc animated:YES completion:nil];
    
}

- (void)rechargeAction
{
    [UIViewController routerJumpToUrl:kRouterRecharge];
}

- (void)drawAction
{
    [UIViewController routerJumpToUrl:kRouterDraw];
}

-(void)popUserCenterAction{
    [self.navigationController popToRootViewControllerAnimated:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIViewController routerJumpToUrl:kRouterCenter];
    });
}
#pragma mark - GET/SET
- (UITableView *)getTableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] init];
        _tableView.backgroundColor = [UIColor skinViewScreenColor];
        _tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    }
    return _tableView;
}


-(void)setTableView:(UITableView *)tableView{
    _tableView = tableView;
}

-(ChatInputToolBar *)getInputToolBar{
    if (!_inputToolBar) {
        _inputToolBar = [[[NSBundle mainBundle] loadNibNamed:@"ChatInputToolBar" owner:self options:nil] firstObject];
        _inputToolBar.barHeight = 50;
    }
    return _inputToolBar;
}

-(void)setInputToolBar:(ChatInputToolBar *)inputToolBar{
    _inputToolBar = inputToolBar;
}

-(ChatScrollBottomButton *)getBottomBtn{
    if (!_bottomBtn) {
        _bottomBtn = [ChatScrollBottomButton buttonWithType:UIButtonTypeCustom];
        [_bottomBtn setBackgroundImage:[UIImage imageNamed:@"chat_scrolldown"] forState:UIControlStateNormal];
        _bottomBtn.hidden = YES;
    }
    return _bottomBtn;
}


-(void)setBottomBtn:(ChatScrollBottomButton *)bottomBtn{
    _bottomBtn = bottomBtn;

}

-(NSArray<ChatRightItem *> *)rightSlideItems{
    if (!_rightSlideItems) {
        NSMutableArray *itmes = @[].mutableCopy;
        [CHAT_UTIL.slideMenulist enumerateObjectsUsingBlock:^(ChatSlideMenuModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ChatRightItem *item = [ChatRightItem creatItemTitile:obj.name
                                                         ImgPath:nil Sel:@selector(jumpAction:)];
            if (obj.icon.length) {
                if ([obj.icon isValidUrl]) {
                    item.imgPath = obj.icon;
                }else{
                    item.imgPath = [[NSString stringWithFormat:@"%@%@",SerVer_Url,obj.icon] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                }
            }else{
                if ([obj.type containsString:@"game/"]) {
                    item.imgPath = [NSString stringWithFormat:jesse(@"/images/gameLogo/%@.png"),[obj.type componentsSeparatedByString:@"/"].lastObject];
                }else if ([obj.type isEqualToString:@"online-customer"]){
                    item.imgPath = @"1-2";
                }else if([obj.type hasPrefix:@"live-report"] ||
                         [obj.type isEqualToString:@"cp-bet-list"]){
                    /** 投注记录**/
                    item.imgPath = obj.type;
                }else{
                    item.imgPath = [NSString stringWithFormat:@"chat_tab_%@",obj.type];
                }
            }
            item.tag = idx;
            [itmes addObject:item];
        }];
        _rightSlideItems = itmes;
    }
    return _rightSlideItems;
}


@end
