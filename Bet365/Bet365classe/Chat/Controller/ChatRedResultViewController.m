//
//  ChatRedResultViewController.m
//  Bet365
//
//  Created by HHH on 2018/11/22.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatRedResultViewController.h"
#import "ChatRedResultsCell.h"
#import "ChatResultStickyView.h"
#import <VGParallaxHeader/UIScrollView+VGParallaxHeader.h>

@interface ChatRedResultViewController ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) UIView *headContentView;
@property (strong, nonatomic) ChatResultStickyView *stickyView;
@end

@implementation ChatRedResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self instanNaviBar];
    [self.tableView setParallaxHeaderView:self.headContentView mode:VGParallaxHeaderModeTopFill height:230];
    self.tableView.parallaxHeader.stickyViewPosition = VGParallaxHeaderStickyViewPositionBottom;
    [self.tableView.parallaxHeader setStickyView:self.stickyView
                                       withHeight:160];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)instanNaviBar{
    self.naviRightItem = Bet365NaviRightItemBalance;
    
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setTitle:@"返回" forState:UIControlStateNormal];
    UIImage *img = [UIImage imageNamed:@ "left_lage" ];
    img = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [leftBtn setImage:img forState:UIControlStateNormal];
    [leftBtn setTintColor:[UIColor skinNaviTintColor]];
    [leftBtn setTitleColor:[UIColor skinNaviTintColor] forState:UIControlStateNormal];
    [leftBtn setImagePosition:LXMImagePositionLeft spacing:5];
    [leftBtn addTarget:self action:@selector(closeAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftButton;
    
//    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:@"红包记录" style:UIBarButtonItemStyleDone target:self action:@selector(mineRedAction)];
//    [rightButton setTitleTextAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15],
//                                          NSForegroundColorAttributeName : COLOR_WITH_HEX(0xEDA050)}
//                               forState:UIControlStateNormal];
//    self.navigationItem.rightBarButtonItem = rightButton;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.tableView shouldPositionParallaxHeader];
}

#pragma mark - Events
-(void)closeAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)mineRedAction{
    
}
#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (CHAT_UTIL.suspendModel.isShowHbHistory && ![CHAT_UTIL.suspendModel.isShowHbHistory boolValue]) {
        return 0;
    }
    return self.model.details.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    UITableViewHeaderFooterView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"UITableViewHeaderFooterView"];
//    if (!view) {
//        view = [[UITableViewHeaderFooterView alloc] initWithReuseIdentifier:@"UITableViewHeaderFooterView"];
//        [tableView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:@"UITableViewHeaderFooterView"];
//        UILabel *label = [UILabel setAllocLabelWithText:@"" FontOfSize:14 rgbColor:0xd2d2d2];
//        [view.contentView addSubview:label];
//        [label mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerY.equalTo(view.contentView);
//            make.left.equalTo(view.contentView).offset(7);
//        }];
//        view.contentView.backgroundColor = [UIColor whiteColor];
//    }
//
//    UILabel *titileLabel = view.contentView.subviews.lastObject;
//    if (self.model.activityChatRedpacket.count > self.model.activityChatRedpacket.consumeCount) {
////        titileLabel.text = [NSString stringWithFormat:@"领取%li/%li个，共%.2f/%.2f元",
////                            self.model.activityChatRedpacket.consumeCount,
////                            self.model.activityChatRedpacket.count,
////                            [self.model.activityChatRedpacket.consumeMoney floatValue],
////                            [self.model.activityChatRedpacket.totalMoney floatValue]];
//        titileLabel.text = [NSString stringWithFormat:@"已被领取%li个",
//                            self.model.activityChatRedpacket.consumeCount];
//
//    }else{
////        titileLabel.text = [NSString stringWithFormat:@"%li个红包，共%.2f元",self.model.activityChatRedpacket.count,[self.model.activityChatRedpacket.totalMoney floatValue]];
//        titileLabel.text = [NSString stringWithFormat:@"已被领取%li个",
//                            self.model.activityChatRedpacket.consumeCount];
//    }
//    return view;
//}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ChatRedResultsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatRedResultsCell"];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"ChatRedResultsCell" owner:self options:nil].firstObject;
    }
    cell.detailModel = [self.model.details safeObjectAtIndex:indexPath.row];
    return cell;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark  GET/SET
-(ChatResultStickyView *)stickyView{
    if (!_stickyView) {
        _stickyView = [ChatResultStickyView instantiateFromNib];
        
    }
    return _stickyView;
}
-(UIView *)headContentView{
    if (!_headContentView) {
        _headContentView = [[UIView alloc] init];
        [_headContentView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
        UIImageView *imgViwe = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@ "chat_red_bg" ]];
        [_headContentView addSubview:imgViwe];
        [imgViwe mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.equalTo(_headContentView);
            make.bottom.equalTo(_headContentView).offset(-160);
        }];

        
        
//        [_headContentView addSubview:leftBtn];
//        [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(_headContentView).offset(10);
//            if (is_IphoneX) {
//                make.top.equalTo(_headContentView).offset(44);
//            }else{
//                make.top.equalTo(_headContentView).offset(20);
//            }
//        }];
        
        
        
        
    }
    return _headContentView;
}
-(void)setModel:(ChatRedPacketModel *)model{
    _model = model;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.stickyView.model = model;
        [self.tableView reloadData];
    });
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
