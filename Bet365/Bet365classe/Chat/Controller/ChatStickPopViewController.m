//
//  ChatStickPopViewController.m
//  Bet365
//
//  Created by super_小鸡 on 2018/12/18.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatStickPopViewController.h"

@interface ChatStickPopViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titileLabel;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIView *titileView;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@end

@implementation ChatStickPopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImage *img = [UIImage imageNamed:@ "chat_close_icon" ];
    img = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.closeBtn setImage:img forState:UIControlStateNormal];
    [self.closeBtn setTintColor:[UIColor skinNaviTintColor]];
    UIImageView *bg = [[UIImageView alloc] initWithImage:[UIImage skinGradientHorNaviImage]];
    [self.titileView insertSubview:bg atIndex:0];
    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(bg.superview);
    }];
    self.titileLabel.textColor = [UIColor skinTextTitileColor];
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)closeAction:(UIButton *)sender {
     [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)setMessage:(Message *)message{
    _message = message;
    ChatUserInfoEntity *entity = [ChatUtilitySubscribeData getUserInfoListEntityByUserId:[message.userId integerValue]].firstObject;
    self.titileLabel.text = [NSString stringWithFormat:@"来自【%@】的消息",entity.nickName];
    self.textView.attributedText = _message.content.attributedText;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
