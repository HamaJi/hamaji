//
//  ChatPrivateNewMessageViewController.m
//  Bet365
//
//  Created by adnin on 2019/7/29.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "ChatPrivateNewMessageViewController.h"
#import "ChatPrivateNewMessageListCell.h"

@interface ChatPrivateNewMessageViewController ()
<UITableViewDataSource,
UITableViewDelegate,
ChatPrivateNewMessageListCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UILabel *titileLabel;

@property (nonatomic, assign) BOOL isShowClose;

@end

@implementation ChatPrivateNewMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.isShowClose = YES;
    self.tableView.isShowEmpty = YES;
    self.tableView.titleForEmpty = @"暂无私聊用户";
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - Events
- (IBAction)closeAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDelegate

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.userList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ChatPrivateNewMessageListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatPrivateNewMessageListCell"];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"ChatPrivateNewMessageListCell" owner:self options:nil].firstObject;
    }
    cell.infoModel = [self.userList safeObjectAtIndex:indexPath.row];
    
    cell.isShowClose = [cell.infoModel.customer integerValue] != 1 ? self.isShowClose : NO;
    cell.delegate = self;
    return cell;
}

#pragma UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self dismissViewControllerAnimated:YES completion:^{
        ChatUserInfoModel *model = [self.userList safeObjectAtIndex:indexPath.row];
        if (self.delegate && [self.delegate respondsToSelector:@selector(chatPrivateNewMessageListController:didSelectUser:)]) {
            [self.delegate chatPrivateNewMessageListController:self didSelectUser:model];
        }
    }];
}

#pragma mark - ChatPrivateNewMessageListCellDelegate
-(void)chatPrivateNewMessageListCell:(ChatPrivateNewMessageListCell *)cell removeUserInfo:(ChatUserInfoModel *)userInfo{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(chatPrivateNewMessageListRemoveUserInfo:)]) {
        [self.delegate chatPrivateNewMessageListRemoveUserInfo:userInfo];
    }
}

#pragma mark - GET/SET
-(void)setUserList:(NSArray<ChatUserInfoModel *> *)userList{
    _userList = userList;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
