//
//  ChatTopListViewController.h
//  Bet365
//
//  Created by HHH on 2018/12/4.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ChatTopListBlock)();

@interface ChatTopListViewController : Bet365ViewController

@property (nonatomic,strong)NSArray <ChatTopListModel *>*list;

@property (nonatomic,copy)ChatTopListBlock handle;

@property (nonatomic,weak)ChatUtilityGroupSubscribeData *data;

@end
