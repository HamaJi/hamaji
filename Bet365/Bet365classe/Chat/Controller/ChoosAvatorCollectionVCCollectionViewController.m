//
//  ChoosAvatorCollectionVCCollectionViewController.m
//  Bet365
//
//  Created by HHH on 2018/10/7.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChoosAvatorCollectionVCCollectionViewController.h"
#import "ChatChooseAvatorCell.h"
@interface ChoosAvatorCollectionVCCollectionViewController ()
@property (nonatomic,strong)NSArray *list;

@end

@implementation ChoosAvatorCollectionVCCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

-(instancetype)initWithList:(NSArray <NSString *>*)list PageIdx:(NSUInteger)idx{
    if(self = [super initWithNibName:@"ChoosAvatorCollectionVCCollectionViewController" bundle:nil]){
        self.list = list;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.backgroundColor = [UIColor skinViewScreenColor];
    [self.collectionView registerNib:[UINib nibWithNibName:@"ChatChooseAvatorCell" bundle:nil] forCellWithReuseIdentifier:@"ChatChooseAvatorCell"];
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Do any additional setup after loading the view.
}
#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.list.count;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0.0;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    
    return 0.0;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(WIDTH / 5.0,WIDTH / 5.0);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ChatChooseAvatorCell  *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatChooseAvatorCell" forIndexPath:indexPath];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"ChatChooseAvatorCell" owner:self options:nil].firstObject;
    }
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SerVer_Url,[self.list objectAtIndex:indexPath.item]]];
    cell.url = url;
    return cell;
}

#pragma mark - UICollectionViewDelegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    self.handle ? self.handle([self.list objectAtIndex:indexPath.item]) : nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
