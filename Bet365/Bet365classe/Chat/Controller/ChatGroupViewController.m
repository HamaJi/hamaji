//
//  ChatViewController.m
//  Bet365
//
//  Created by HHH on 2018/9/16.
//  Copyright © 2018年 jesse. All rights reserved.
//
#import "ChatGroupViewController.h"

#pragma mark - ViewController
#import "ChatStickPopViewController.h"
#import "GuideViewController.h"
#import "KFCPActivityController.h"
#import "ChatPrivateViewController.h"
#import "ChoosePrivateChatViewController.h"

#pragma mark - View
#import "aji_horCircleView.h"
#import "ChatSuspendView.h"
#import "ChatTitleView.h"
#import "ChatPrivateNewMessageView.h"
#import "ChatMetricsView.h"

#pragma mark - Command
#import "ChatGroupAction.h"
#import "ChatViewController+Extension.h"


#define CHAT_IGNOR_DATA @"CHAT_IGNOR_DATA"
@interface ChatGroupViewController ()
<
CAAnimationDelegate,
ChatActionDelegate,
ChatTitleViewDelegate
>

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *width;

@property (strong, nonatomic) iCarousel *carousel;

@property (strong, nonatomic) UIButton *moreRoomBtn;

@property (strong, nonatomic) IBOutlet UIView *winContentView;

@property (strong, nonatomic) IBOutlet UILabel *winLabel;

@property (strong, nonatomic) IBOutlet UIView *tabBarView;

@property (strong, nonatomic) UIView *horCircleContentView;

@property (strong, nonatomic) aji_horCircleView *horCircleView;

@property (strong, nonatomic) UIButton *topListButton;

@property (strong, nonatomic) UIButton *followSortButton;

@property (strong, nonatomic) UIButton *removeAllButton;

@property (strong, nonatomic) ChatTitleView *naviTitleView;

@property (strong, nonatomic) ChatPrivateNewMessageView *unreadMessageListView;

@property (strong, nonatomic) ChatStickMeesageView *stickMessageView;

@property (strong, nonatomic) ChoosePrivateChatViewController *chooseUserListViewController;

@property (weak, nonatomic) IBOutlet UIView *vipEnterContentView;

@property (weak, nonatomic) IBOutlet UIImageView *vipEneterBgImageView;

@property (weak, nonatomic) IBOutlet UIImageView *vipEenterHorseImageView;

@property (weak, nonatomic) IBOutlet UILabel *vipEnterTitileLabel;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tabBarContstraintHeight;

@property (nonatomic,strong) ChatMetricsView *metricsView;

@property (strong, nonatomic) ChatSuspendView *suspendView;

@property (strong, nonatomic) CAEmitterLayer *streamerLayer;

@property (nonatomic, strong) NSMutableArray <NSString *>*cellKeyList;

@property (nonatomic, strong) NSMutableArray <CAEmitterCell *>*CAEmitterCellArr;

@property (strong, nonatomic) CABasicAnimation *winEnterBasicAni;

@property (strong, nonatomic) CABasicAnimation *winOutBasicAni;

@property (strong, nonatomic) CABasicAnimation *vipEnterBasicAni;

@property (strong, nonatomic) CABasicAnimation *vipOutBasicAni;

@property (strong, nonatomic) NSMutableArray <Message *>*messageList;

@property (strong, nonatomic) NSMutableArray <Message *>*vipEnterMessageList;

@property (nonatomic,strong)ChatJoinRoomBubbleTools *joinRoomBubbleTools;

@property (nonatomic)BOOL winPlaying;

@property (nonatomic)BOOL vipEnterPlaying;

/**
 忽略置顶消息集
 */
@property (nonatomic,strong)NSMutableArray <Message *>*ignorStickMessageList;

@end

@implementation ChatGroupViewController

@synthesize roomID = _roomID;

+(void)load{

    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.titleView = self.naviTitleView;
    [self.navigationController.navigationBar setHidden:NO];
    if (_horCircleView) {
        if (BET_CONFIG.noticeModel.chat_notice.count) {
            [self.horCircleView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(30);
            }];
            NSMutableArray *list = @[].mutableCopy;
            
            [BET_CONFIG.noticeModel.chat_notice enumerateObjectsUsingBlock:^(NoticeModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.noticeContent) {
                    NSAttributedString *attStr = [obj.noticeContent htmlStr];
                    attStr ? [list addObject:attStr] : nil;
                }
            }];
            self.horCircleView.list = list;
            [self.horCircleView startAnimation];
        }
    }
    if (!self.roomID){
        self.roomID = CHAT_UTIL.clientInfo.defaultRoomId;
    }else if (CHAT_UTIL.connectStatus < ChatUtilityConnectStatus_CONNECTING){
        [CHAT_UTIL webSocketResetConnect];
    }
    [IQKeyboardManager sharedManager].enable = NO;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!self.winPlaying && self.messageList.count) {
            [self.winContentView.layer addAnimation:self.winEnterBasicAni forKey:@"WIN_ENTER"];
            ChatUserInfoEntity *entity = [ChatUtilitySubscribeData getUserInfoListEntityByUserId:[self.messageList.firstObject.userId integerValue]].firstObject;
            self.winLabel.text = [NSString stringWithFormat:@"恭喜【%@】中奖%.2f元",entity.nickName,[self.messageList.firstObject.content.winMoney floatValue]];
        }
        if (!self.vipEnterPlaying && self.vipEnterMessageList.count) {
            [self startVipEnterAnimateByMessage:self.vipEnterMessageList.firstObject];
        }
    });
}

    
    -(void)viewDidAppear:(BOOL)animated{
        [super viewDidAppear:animated];
    }



- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
    [IQKeyboardManager sharedManager].enable = YES;
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.action resignAllFirstResponder];
        [self.action scrollBottomAnimated:YES];
    });
    
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    [super setupLeftBackButton];
    self.winContentView.layer.anchorPoint = CGPointMake(0.0, 0.0);
    
    self.vipEnterContentView.layer.anchorPoint = CGPointMake(0.0, 0.0);
    
    [self addObserver];
    [[self.tabBarView subviews] enumerateObjectsUsingBlock:^(__kindof UIButton * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj setImagePosition:LXMImagePositionTop spacing:3];
    }];
    // Do any additional setup after loading the view from its nib.
}

-(void)dealloc{
    
}


-(void)addObserver{
    
    @weakify(self)
    [[[RACObserve(self, messageList) merge:self.messageList.rac_sequence.signal] map:^id(id value) {
        @strongify(self)
        return @(self.messageList.count > 0 && !self.winPlaying);
    }] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        if ([x boolValue]) {
            [self.winContentView.layer addAnimation:self.winEnterBasicAni forKey:@"WIN_ENTER"];
            ChatUserInfoEntity *entity = [ChatUtilitySubscribeData getUserInfoListEntityByUserId:[self.messageList.firstObject.userId integerValue]].firstObject;
            self.winLabel.text = [NSString stringWithFormat:@"恭喜【%@】在%@中奖%.2f元",entity.nickName,self.messageList.firstObject.content.gameName,[self.messageList.firstObject.content.winMoney floatValue]];
        }
    }];
    
    [[[RACObserve(self, vipEnterMessageList) merge:self.vipEnterMessageList.rac_sequence.signal] map:^id(id value) {
        @strongify(self)
        return @(self.vipEnterMessageList.count > 0 && !self.vipEnterPlaying);
    }] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        if ([x boolValue]) {
            [self startVipEnterAnimateByMessage:self.vipEnterMessageList.firstObject];
        }
    }];
    
    [[RACObserve(BET_CONFIG.userSettingData, hideChatPingView) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        self.metricsView.hidden = [x boolValue];
    }];
    
    [[RACObserve(CHAT_UTIL.clientInfo, chatUrl) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        self.metricsView.url = CHAT_UTIL.clientInfo.chatUrl;
    }];
    
    [[RACObserve(BET_CONFIG.userSettingData, closeChatWinFireAnimation) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        BOOL isClose = [x boolValue];
        if (isClose) {
            [self explodeFireWork];
        }
    }];
    
    [RACObserve(CHAT_UTIL, suspendModel) subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        ChatSuspendDelegateModel *model = x;
        if (model.app_float_name.length ||
            model.app_float_link.length ||
            model.app_float_image.length) {
            if (_suspendView) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.suspendView.center = [self updateCenter:CGPointMake(WIDTH, HEIGHT / 2.0)];
                    self.suspendView.titile = CHAT_UTIL.suspendModel.app_float_name;
                    self.suspendView.imgUrlStr = [NSString stringWithFormat:@"%@/views/image/%@",SerVer_Url,CHAT_UTIL.suspendModel.app_float_image];
                });
            }
            
            
        }else if (_suspendView) {
            [_suspendView removeFromSuperview];
            _suspendView = nil;
        }
    }];
    
    
    [[RACObserve(CHAT_UTIL, clientInfo) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        if (x == nil || [x isEqual:[NSNull null]]) {
            self.roomID = 0;
        }
    }];
    
    [[RACObserve(CHAT_UTIL, connectStatus) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        dispatch_async(dispatch_get_main_queue(), ^{
            ChatUtilityConnectStatus status = [x integerValue];
            if (status == ChatUtilityConnectStatus_DISCONNECTE) {
                self.naviTitleView.title = @"未连接";
            }else if (status == ChatUtilityConnectStatus_UNCONNECTE) {
                self.naviTitleView.title = @"无法连接";
            }else if (status == ChatUtilityConnectStatus_CONNECTING) {
                self.naviTitleView.title = @"连接中...";
            }else if (status == ChatUtilityConnectStatus_CONNECTED_JOINNING) {
                self.naviTitleView.title = @"加入房间...";
            }else if (status == ChatUtilityConnectStatus_CONNECTED_JOINED ||
                      status == ChatUtilityConnectStatus_CONNECTED_WAITJOIN) {
                [CHAT_UTIL.clientInfo.roomInfoList enumerateObjectsUsingBlock:^(ChatRoomInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if ([obj.identity integerValue] == self.roomID) {
                        self.naviTitleView.title = obj.name;
                        *stop = YES;
                    }
                }];
            }else{
                self.naviTitleView.title = @"";
            }
        });
    }];
    
    [[RACObserve(USER_DATA_MANAGER, isLogin) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        [super setupRightMenuButton];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private
-(void)startVipEnterAnimateByMessage:(Message *)message{
    ChatUserInfoEntity *entity = [ChatUtilitySubscribeData getUserInfoListEntityByUserId:[message.userId integerValue]].firstObject;
    
    NSString *bgPath = [NSString stringWithFormat:@"chat_vipenter_bg_%@",vipEnterSuffixByLevel(entity.level)];
    NSString *heardPath = [NSString stringWithFormat:@"chat_vipenter_%@",vipEnterSuffixByLevel(entity.level)];
    self.vipEneterBgImageView.image = [UIImage sd_imageWithGIFData:[NSData dataWithContentsOfFile: [[NSBundle mainBundle] pathForResource:bgPath ofType:@"gif"]]];
    self.vipEenterHorseImageView.image = [UIImage sd_imageWithGIFData:[NSData dataWithContentsOfFile: [[NSBundle mainBundle] pathForResource:heardPath ofType:@"gif"]]];
    
    NSString *animals = vipEnterNameByLevel(entity.level);
    NSString *nickName = entity.nickName.length ? entity.nickName : @"****";
    NSString *str = [NSString stringWithFormat:@"%@  骑着%@驾临聊天室",nickName,animals];
    NSMutableAttributedString *mStr = [[NSAttributedString alloc] initWithString:str].mutableCopy;
    [mStr addColor:[UIColor whiteColor] substring:str];
    [mStr addFont:[UIFont systemFontOfSize:9] substring:str];
    [mStr addColor:[UIColor orangeColor] substring:entity.nickName];
    self.vipEnterTitileLabel.attributedText = mStr;
    
    [self.vipEnterContentView.layer addAnimation:self.vipEnterBasicAni forKey:@"VIP_ENTER"];
}
- (void)startFireworksAnimate {
    if (!BET_CONFIG.userSettingData.closeChatWinFireAnimation) {
        self.streamerLayer.emitterCells  = self.CAEmitterCellArr;
        self.streamerLayer.beginTime = CACurrentMediaTime();
        for (CAEmitterCell *cell in self.streamerLayer.emitterCells) {
            cell.birthRate = 168;
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self explodeFireWork];
        });
    }
    
}
- (void)explodeFireWork {
    
    for (NSString * key in self.cellKeyList) {
        NSString * keyPathStr = [NSString stringWithFormat:@"emitterCells.%@.birthRate",key];
        [self.streamerLayer setValue:@0 forKeyPath:keyPathStr];
    }
    self.cellKeyList = nil;
    self.CAEmitterCellArr = nil;
}
-(void)clean{
    [super clean];
    [self cleanWinAnimation];
    [self cleanVipEnterAnimation];
    if (_horCircleView) {
        [_horCircleView removeFromSuperview];
        _horCircleView = nil;
    }
    if (_horCircleContentView) {
        [_horCircleContentView removeFromSuperview];
        _horCircleContentView = nil;
    }
    if (_carousel) {
        [_carousel removeFromSuperview];
        _carousel = nil;
    }
    
    if (_topListButton) {
        [_topListButton removeFromSuperview];
        _topListButton = nil;
    }
    if (_followSortButton) {
        [_followSortButton removeFromSuperview];
        _followSortButton = nil;
    }
    if (_removeAllButton) {
        [_removeAllButton removeFromSuperview];
        _removeAllButton = nil;
    }
    if (_moreRoomBtn) {
        [_moreRoomBtn removeFromSuperview];
        _moreRoomBtn = nil;
    }
    if (_suspendView) {
        [_suspendView removeFromSuperview];
        _suspendView = nil;
    }
    if(_stickMessageView){
        [_stickMessageView removeFromSuperview];
        _stickMessageView = nil;
    }
//    if (_ignorStickMessageList) {
//        [_ignorStickMessageList removeAllObjects];
//    }
    
    if (_unreadMessageListView) {
        [_unreadMessageListView removeFromSuperview];
        _unreadMessageListView = nil;
    }
    if (_joinRoomBubbleTools) {
        [_joinRoomBubbleTools removeAllBubbleString];
        _joinRoomBubbleTools = nil;
    }
}

-(void)cleanWinAnimation{
    [self explodeFireWork];
    if (_messageList.count) {
        [_messageList removeAllObjects];
    }
    [self.winContentView.layer removeAllAnimations];
    self.winEnterBasicAni = nil;
    self.winOutBasicAni = nil;
}

-(void)cleanVipEnterAnimation{

    if (_vipEnterMessageList) {
        [_vipEnterMessageList removeAllObjects];
    }
    [self.vipEnterContentView.layer removeAllAnimations];
    self.vipOutBasicAni = nil;
    self.vipEnterBasicAni = nil;
}

- (CAEmitterCell *)emitterCell:(NSInteger)idx {
    CAEmitterCell * smoke = [CAEmitterCell emitterCell];
    smoke.birthRate = 0;
    smoke.lifetime = 7;
    smoke.lifetimeRange = 2;
    smoke.scale = 0.35;
    smoke.alphaRange = 1;


    NSString *key = [NSString stringWithFormat:@"chat_icon_%li",idx];
    [self.cellKeyList addObject:key];
    
    CGImageRef image2 = [UIImage imageNamed:key].CGImage;
    smoke.contents= (__bridge id _Nullable)(image2);
    smoke.name = key;
    int x = random() % 200;
    smoke.yAcceleration = 300 + x;
    smoke.velocity = 300 + x;
    smoke.velocityRange = 300;
    
    smoke.emissionLongitude = 3 * M_PI / 2 ;
    smoke.emissionRange = M_PI_2 / 2;
    smoke.spin = M_PI * 2;
    smoke.spinRange = M_PI * 2;
    return smoke;
}

#pragma mark - Extension
-(NSObject<UIViewControllerAnimatedTransitioning> *)presenteAnimation:(UIViewController *)presented{
    if ([presented isKindOfClass:[GuideViewController class]]) {
        return [PresentingAlphaAnimator new];
    }else{
        return [PresentingBottomAnimator new];
    }
}

-(NSObject<UIViewControllerAnimatedTransitioning> *)dismissAnimation:(UIViewController *)dismissed{
    if ([dismissed isKindOfClass:[GuideViewController class]]) {
        return [DismissingAlphaAnimator new];
    }else{
        return [DismissingBottomAnimator new];
    }
}

#pragma mark - ChatActionDelegate
-(void)ChatAction:(ChatGroupAction *)action ChangeRoomByID:(NSInteger)roomID{
    self.roomID = roomID;
}

-(void)ChatAction:(ChatGroupAction *)action ReceiveWinMessage:(Message *)message{
    if (!BET_CONFIG.userSettingData.closeChatWinFireAnimation) {
        [[self mutableArrayValueForKey:@"messageList"] addObject:message];
    }
    
}

-(void)ChatAction:(ChatGroupAction *)action ReceiveVipEnterMessage:(Message *)message{
    [[self mutableArrayValueForKey:@"vipEnterMessageList"] addObject:message];
}

-(void)ChatAction:(ChatGroupAction *)action UpdataOpenList:(NSArray<LotteryOpenInfoModel *> *)openList{
    if (openList.count) {
        [self.carousel mas_updateConstraints:^(MASConstraintMaker *make) {
            if (IS_IPHONE_5s) {
                make.height.mas_equalTo(104);
            }else{
                make.height.mas_equalTo(114);
            }
        }];
    }else{
        [self.carousel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0);
        }];
    }
    [self.view layoutIfNeeded];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showChatGuide];
    });
}

-(void)ChatAction:(ChatGroupAction *)action JoinRoomFail:(NSInteger)roomId{
    [self cleanWinAnimation];
    [self cleanVipEnterAnimation];
    self.tableView.isShowEmpty = YES;
}

-(void)ChatAction:(ChatGroupAction *)action JoinRoomSuccese:(ChatUtilityGroupSubscribeData *)data{
    
    if ([data.ruleModel.isDelMessage boolValue] && _removeAllButton == nil) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
        [self.removeAllButton addTarget:_action action:@selector(removeAllAction:) forControlEvents:UIControlEventTouchUpInside];
#pragma clang diagnostic pop
    }else if (![data.ruleModel.isDelMessage boolValue] && _removeAllButton != nil){
        [_removeAllButton removeFromSuperview];
        _removeAllButton = nil;
    }

}

-(void)HasBeKickedChatAction:(ChatGroupAction *)action{
    [self.navigationController popToRootViewControllerAnimated:YES];
    if (CHAT_UTIL.clientInfo.roomInfoList.count) {
        self.roomID = [CHAT_UTIL.clientInfo.roomInfoList.firstObject.identity integerValue];
    }else{
        self.roomID = 0;
    }
}

-(void)ChatAction:(ChatGroupAction *)action SetHideTabBar:(BOOL)isHide{
    if (isHide) {
        self.tabBarContstraintHeight.constant = 0;
    }else{
        self.tabBarContstraintHeight.constant = 50;
    }
    self.tabBarView.hidden = isHide;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view layoutIfNeeded];
    });
}

-(void)ChatAction:(ChatGroupAction *)action StickMessage:(Message *)message{

    /** 消息已赋值**/
    if ([_stickMessageView.message isEqual:message]) {
        return;
    }
    
    /** 忽略集包含此消息**/
    if ([self.ignorStickMessageList containsObject:message]) {
        return;
    }else{
        __block BOOL isContain = NO;
        [self.ignorStickMessageList enumerateObjectsUsingBlock:^(Message * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.identity integerValue] == [message.identity integerValue]) {
                isContain = YES;
                *stop =YES;
            }
        }];
        if (isContain) {
            return;
        }
    }
    
    self.stickMessageView.message = message;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.stickMessageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.top.equalTo(self.tableView);
            if (!self.stickMessageView.message) {
                make.height.mas_equalTo(0);
            }else{
                make.height.mas_equalTo(50);
            }
        }];
    });
}

-(void)ChatAction:(ChatGroupAction *)action UpdateUserOnlineCount:(NSInteger)userOnlineCount{

    dispatch_async(dispatch_get_main_queue(), ^{
        if (CHAT_UTIL.clientInfo.userInfo.userId == -1) {
            self.naviTitleView.isPrivateChat = NO;
        }else{
//            [((ChatUtilityGroupSubscribeData *)self.action.data).userList enumerateObjectsUsingBlock:^(ChatUserInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                if (obj.userId == CHAT_UTIL.clientInfo.userInfo.userId) {
//                    self.naviTitleView.isPrivateChat = [obj.privateChat boolValue];
//                    *stop = YES;
//                }
//            }];
            
            
            self.naviTitleView.isPrivateChat = [((ChatUtilityGroupSubscribeData *)self.action.data).ruleModel.isShowOnlineUser integerValue];
        }
        self.naviTitleView.count = userOnlineCount;
        if ([[NAVI_MANAGER getCurrentVC] isEqual:_chooseUserListViewController]) {
            self.chooseUserListViewController.userList = ((ChatUtilityGroupSubscribeData *) self.action.data).userList;
        }
    });
}

#pragma mark - CAAnimationDelegate
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    
    if (flag) {
        if (anim == [self.winContentView.layer animationForKey:@"WIN_ENTER"]){
            [self startFireworksAnimate];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.winContentView.layer removeAnimationForKey:@"WIN_ENTER"];
                [self.winContentView.layer addAnimation:self.winOutBasicAni forKey:@"WIN_OUT"];
                self.winEnterBasicAni = nil;
                
            });
        }else if(anim == [self.winContentView.layer animationForKey:@"WIN_OUT"]){
            [self.winContentView.layer removeAnimationForKey:@"WIN_OUT"];
            self.winOutBasicAni = nil;
            if (self.messageList.count) {
                [[self mutableArrayValueForKey:@"messageList"] removeObjectAtIndex:0];
            }
        }else if (anim == [self.vipEnterContentView.layer animationForKey:@"VIP_ENTER"]){
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.vipEnterContentView.layer removeAnimationForKey:@"VIP_ENTER"];
                [self.vipEnterContentView.layer addAnimation:self.vipOutBasicAni forKey:@"VIP_OUT"];
                self.vipEnterBasicAni = nil;
                
            });
        }else if (anim == [self.vipEnterContentView.layer animationForKey:@"VIP_OUT"]){
            [self.vipEnterContentView.layer removeAnimationForKey:@"VIP_OUT"];
            self.vipOutBasicAni = nil;
            if (self.vipEnterMessageList.count) {
                [[self mutableArrayValueForKey:@"vipEnterMessageList"] removeObjectAtIndex:0];
            }
        }
    }else{
        
        [self.winContentView.layer removeAllAnimations];
        [self explodeFireWork];
        _winEnterBasicAni = nil;
        _winOutBasicAni = nil;
        
        [self.vipEnterContentView.layer removeAllAnimations];
        _vipOutBasicAni = nil;
        _vipEnterBasicAni = nil;
    }
    
}

#pragma mark - PBViewControllerDelegate
- (void)viewController:(PBViewController *)viewController didSingleTapedPageAtIndex:(NSInteger)index presentedImage:(UIImage *)presentedImage {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - ChatNaviTitleViewDelegate
-(void)tapUpInsideChatTitleView:(ChatTitleView *)titleView{
    
    
    self.chooseUserListViewController.transitioningDelegate = self;
    self.chooseUserListViewController.modalPresentationStyle = UIModalPresentationCustom;
    self.chooseUserListViewController.userList = ((ChatUtilityGroupSubscribeData *) self.action.data).userList;
    @weakify(self);
    self.chooseUserListViewController.handle = ^(ChatUserInfoModel *model) {
        @strongify(self);
        ChatSendRequestGroupModel *requestModel = [ChatSendRequestGroupModel creatSendModel:kChatSendRequestUserInfoPrivate Date:[NSDate date]];
        requestModel.userId = model.userId;
        requestModel.roomId = self.roomID;
        [CHAT_UTIL webSocketSendMessageByChatSendModel:requestModel Subscribe:self.action.data];
    };
    dispatch_async(dispatch_get_main_queue(), ^{
        [CHAT_UTIL.clientInfo.roomInfoList enumerateObjectsUsingBlock:^(ChatRoomInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.identity integerValue] == _roomID) {
                self.chooseUserListViewController.titile = obj.name;
                *stop = YES;
            }
        }];
    });
    [self presentViewController:self.chooseUserListViewController animated:YES completion:nil];
}


#pragma mark - GUIDE
-(void)showChatGuide{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (BET_CONFIG.userSettingData.hasGuideChatContent) {
            return;
        }
        BET_CONFIG.userSettingData.hasGuideChatContent = YES;
        [BET_CONFIG.userSettingData save];
        GuideViewController *vc = [[GuideViewController alloc] initWithNibName:@"GuideViewController" bundle:nil];
        GuideIndex idx;
        if (self.carousel.numberOfItems) {
            idx = GuideIndex_CHAT_OPENRESULTS;
        }else{
            idx = GuideIndex_CHAT_NAVIMORE;
        }
        vc.handle = ^UIView *(GuideIndex idx) {
            if (idx == GuideIndex_CHAT_OPENRESULTS) {
                return ((ChatLotteryTopView *)[self.carousel itemViewAtIndex:self.carousel.currentItemIndex]).ballContentView;
            }else if (idx == GuideIndex_CHAT_LOTTERY) {
                return ((ChatLotteryTopView *)[self.carousel itemViewAtIndex:self.carousel.currentItemIndex]).iconImageView;
            }else if (idx == GuideIndex_CHAT_NAVIMORE){
                return self.navigationItem.rightBarButtonItems.firstObject.customView;
            }
            return nil;
        };
        vc.index = idx;
        vc.transitioningDelegate = self;
        vc.modalPresentationStyle = UIModalPresentationCustom;
        [[NAVI_MANAGER getCurrentVC] presentViewController:vc animated:YES completion:nil];
    });
}
#pragma mark - Events
- (IBAction)tabTapAction:(UIButton *)sender {

    NSInteger index = sender.tag;
    switch (index) {
        case 0:
        {
            [super rechargeAction];
        }
            break;
        case 1:
        {
            [super drawAction];
        }
            break;
        case 2:
        {
            
            
            [super popBuyLotteryAction];
            
        }
            break;
        case 3:
        {
            [super popUserCenterAction];
        }
            break;
        case 4:
        {


            [self rightDrawerButtonPress:nil];
        }
            break;
            
        default:
            break;
    }
}

- (void)handlePanGesture:(UIPanGestureRecognizer *)recognizer{
    //移动状态
    UIGestureRecognizerState recState =  recognizer.state;
    
    switch (recState) {
        case UIGestureRecognizerStateBegan:
            
            break;
        case UIGestureRecognizerStateChanged:
        {
            CGPoint translation = [recognizer translationInView:self.view];
            recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x, recognizer.view.center.y + translation.y);
            NSLog(@"%@",NSStringFromCGPoint(recognizer.view.center));
            
        }
            break;
        case UIGestureRecognizerStateEnded:
        {
            
            [UIView animateWithDuration:0.5 animations:^{
                recognizer.view.center = [self updateCenter:recognizer.view.center];
            }];
        }
            break;
            
        default:
            break;
    }
    
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
}

-(void)jumpWebViewController{
    if ([CHAT_UTIL.suspendModel.app_float_link hasPrefix:@"mqq"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:CHAT_UTIL.suspendModel.app_float_link]];
    }else if ([CHAT_UTIL.suspendModel.app_float_link hasPrefix:@"weixin"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:CHAT_UTIL.suspendModel.app_float_link]];
    }else{
        [UIViewController routerJumpToUrl:CHAT_UTIL.suspendModel.app_float_link];
    }
}


-(CGPoint)updateCenter:(CGPoint)center{
    CGFloat spacing = self.suspendView.frame.size.width / 2.0;
    CGPoint recognizePoint = CGPointMake(center.x, center.y - STATUSAND_NAVGATION_HEIGHT);
    CGPoint stopPoint;
    if (recognizePoint.x < self.view.frame.size.width / 2.0) {
        
        if (recognizePoint.y <= self.view.frame.size.height/2.0) {
            //左上
            CGFloat y = recognizePoint.y - spacing > 0 ? recognizePoint.y + STATUSAND_NAVGATION_HEIGHT : STATUSAND_NAVGATION_HEIGHT + spacing;
            stopPoint = CGPointMake(self.suspendView.frame.size.width/2.0,y);
        }else{
            //左下
            CGFloat y = recognizePoint.y + STATUSAND_NAVGATION_HEIGHT + spacing >= self.view.frame.size.height ? self.view.frame.size.height - spacing : recognizePoint.y + STATUSAND_NAVGATION_HEIGHT;
            stopPoint = CGPointMake(self.suspendView.frame.size.width/2.0,y);
        }
    }else{
        if (recognizePoint.y <= self.view.frame.size.height/2.0) {
            //右上
            CGFloat y = recognizePoint.y - spacing > 0 ? recognizePoint.y + STATUSAND_NAVGATION_HEIGHT : STATUSAND_NAVGATION_HEIGHT + spacing;
            stopPoint = CGPointMake(self.view.frame.size.width - self.suspendView.frame.size.width/2.0,y);
        }else{
            //右下
            CGFloat y = recognizePoint.y + STATUSAND_NAVGATION_HEIGHT + spacing >= self.view.frame.size.height ? self.view.frame.size.height - spacing : recognizePoint.y + STATUSAND_NAVGATION_HEIGHT;
            stopPoint = CGPointMake(self.view.frame.size.width - self.suspendView.frame.size.width/2.0,y);
        }
    }
    
    //超出屏幕下边缘
    
    if (stopPoint.y + self.suspendView.frame.size.height >= self.view.frame.size.height) {
        
        stopPoint = CGPointMake(stopPoint.x,
                                self.view.frame.size.height - self.suspendView.frame.size.height/2.0 - spacing);
    }
    if (stopPoint.x - self.suspendView.frame.size.width/2.0 <= 0) {
        stopPoint = CGPointMake(self.suspendView.frame.size.width/2.0, stopPoint.y);
    }
    if (stopPoint.x + self.suspendView.frame.size.width/2.0 >= self.view.frame.size.width) {
        stopPoint = CGPointMake(self.view.frame.size.width - self.suspendView.frame.size.width/2.0, stopPoint.y);
    }
    if (stopPoint.y - self.suspendView.frame.size.height/2.0 <= 0) {
        stopPoint = CGPointMake(stopPoint.x,
                                self.suspendView.frame.size.height/2.0 + STATUSAND_NAVGATION_HEIGHT + spacing);
    }
    BET_CONFIG.userSettingData.dragSwipeOriginX = stopPoint.x;
    BET_CONFIG.userSettingData.dragSwipeOriginY = stopPoint.y;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [BET_CONFIG.userSettingData save];
    });
    return stopPoint;
}

-(void)hideHorCircleAction:(UIButton *)sender{
    [self.horCircleView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(0.0);
    }];
    [self.view layoutIfNeeded];
}

#pragma mark - PBViewControllerDataSource
- (NSInteger)numberOfPagesInViewController:(nonnull PBViewController *)viewController{
    return 1;
}
- (void)viewController:(PBViewController *)viewController presentImageView:(UIImageView *)imageView forPageAtIndex:(NSInteger)index progressHandler:(void (^)(NSInteger, NSInteger))progressHandler {
    [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",CHAT_UTIL.clientInfo.imageHost,self.stickMessageView.message.content.thumbnail]] placeholderImage:[UIImage imageWithSmallGIFData: self.stickMessageView.message.content.pImageData scale:1.0]];
    
}

- (UIView *)thumbViewForPageAtIndex:(NSInteger)index {
    return self.stickMessageView;
}

#pragma mark - GET/SET
-(ChatAction *)getAction{
    if (!_action) {
        ChatGroupAction *action = [ChatGroupAction creatActionByChannelId:self.roomID];
        dispatch_async(dispatch_get_main_queue(), ^{
            action.carousel = self.carousel;
            action.tableView = self.tableView;
            NSMutableArray *list = @[].mutableCopy;
            [BET_CONFIG.noticeModel.chat_notice enumerateObjectsUsingBlock:^(NoticeModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.noticeContent) {
                    NSMutableAttributedString *attStr = [obj.noticeContent htmlStr];
                    attStr ? [list addObject:attStr] : nil;
                }
            }];
            action.unreadMessageListView = self.unreadMessageListView;
            self.horCircleView.list = list;
            [self.horCircleView startAnimation];
            action.inputToolBar = self.inputToolBar;
            action.bottomBtn = self.bottomBtn;
            dispatch_async(dispatch_get_main_queue(), ^{
                action.joinRoomBubbleTools = self.joinRoomBubbleTools;
            });
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
            __block ChatRoomInfoModel * roomInfo = nil;
            [CHAT_UTIL.clientInfo.roomInfoList enumerateObjectsUsingBlock:^(ChatRoomInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj.identity integerValue] == self.roomID) {
                    roomInfo = obj;
                    *stop = YES;
                }
            }];
            [self.moreRoomBtn addTarget:action action:@selector(moreRoomAction:) forControlEvents:UIControlEventTouchUpInside];
            if ([roomInfo.isOpenLeaderboard integerValue]) {
                [self.topListButton addTarget:action action:@selector(topListAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            [self.followSortButton addTarget:action action:@selector(followSortAction:) forControlEvents:UIControlEventTouchUpInside];
            [self.moreRoomBtn addTarget:action action:@selector(moreRoomAction:) forControlEvents:UIControlEventTouchUpInside];
            __block ChatRoomInfoModel *model = nil;
            [CHAT_UTIL.clientInfo.roomInfoList enumerateObjectsUsingBlock:^(ChatRoomInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj.identity integerValue] == self.roomID) {
                    model = obj;
                    *stop = YES;
                }
            }];
            if (model.isOpenLeaderboard) {
                [self.topListButton addTarget:action action:@selector(topListAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            [self.followSortButton addTarget:action action:@selector(followSortAction:) forControlEvents:UIControlEventTouchUpInside];
            //        [self.removeAllButton addTarget:action action:@selector(removeAllAction:) forControlEvents:UIControlEventTouchUpInside];
#pragma clang diagnostic pop
            
            if (CHAT_UTIL.suspendModel.app_float_name.length || CHAT_UTIL.suspendModel.app_float_link.length || CHAT_UTIL.suspendModel.app_float_image.length) {
                @weakify(self);
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.suspendView.center = [self updateCenter:CGPointMake(WIDTH, HEIGHT / 2.0)];
                    self.suspendView.titile = CHAT_UTIL.suspendModel.app_float_name;
                    self.suspendView.imgUrlStr = [NSString stringWithFormat:@"%@/views/image/%@",SerVer_Url,CHAT_UTIL.suspendModel.app_float_image];
                });
                [self.suspendView addPanGestureRecognizer:^(UIPanGestureRecognizer *recognizer, NSString *gestureId) {
                    @strongify(self);
                    [self handlePanGesture:recognizer];
                }];
            }
        });
        _action = action;
    }
    return _action;
}


-(ChatTitleView *)naviTitleView{
    if (!_naviTitleView) {
        _naviTitleView = [[ChatTitleView alloc] init];
        _naviTitleView.ycz_width = 120;
        _naviTitleView.ycz_height = 40.0;
        _naviTitleView.delegate = self;
    }
    return _naviTitleView;
}
-(UITableView *)getTableView{
    if (!_tableView) {
        _tableView = [super getTableView];
        [self.view insertSubview:_tableView belowSubview:self.winContentView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.top.equalTo(self.carousel.mas_bottom).offset(0);
        }];
        _tableView.titleForEmpty = @"您暂无权限加入该房间";
        _tableView.descriptionForEmpty = @"请联系上级代理或客服，加入聊天室";
        _tableView.imageNameForEmpty = @"chat_unjoint";
        
    }
    return _tableView;
}

-(ChatInputToolBar *)getInputToolBar{
    if (!_inputToolBar) {
        _inputToolBar = [super getInputToolBar];
        [self.view insertSubview:_inputToolBar belowSubview:self.winContentView];
        [_inputToolBar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.top.equalTo(self.horCircleContentView.mas_bottom);
            make.bottom.equalTo(self.tabBarView.mas_top);
        }];
        
    }
    return _inputToolBar;
}

-(ChatScrollBottomButton *)getBottomBtn{
    if (!_bottomBtn) {
        _bottomBtn = [super getBottomBtn];
        [self.view insertSubview:_bottomBtn belowSubview:self.winContentView];
        [_bottomBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.view).offset(-15);
            make.bottom.equalTo(self.unreadMessageListView.mas_top).offset(-10);
            make.size.mas_equalTo(CGSizeMake(30, 30));
        }];
    }
    return _bottomBtn;
}

-(ChatPrivateNewMessageView *)unreadMessageListView{
    if (!_unreadMessageListView) {
        
        _unreadMessageListView = [[ChatPrivateNewMessageView alloc] initWithFrame:CGRectZero];
        _unreadMessageListView.backgroundColor = COLOR_WITH_HEX_ALP(0xffffff, 0.3);
        [self.view insertSubview:_unreadMessageListView belowSubview:self.winContentView];
        [_unreadMessageListView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.tableView.mas_bottom);
            make.left.right.equalTo(self.view);
        }];
    }
    return _unreadMessageListView;
}

-(UIView *)horCircleContentView{
    if (!_horCircleContentView) {
        _horCircleContentView = [[UIView alloc] init];
        _horCircleContentView.backgroundColor = [UIColor whiteColor];
        [self.view insertSubview:_horCircleContentView belowSubview:self.winContentView];
        [_horCircleContentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.top.equalTo(self.tableView.mas_bottom);
        }];
        
        UIImageView *line = [[UIImageView alloc] init];
        line.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [_horCircleContentView addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.equalTo(_horCircleContentView);
            make.height.mas_equalTo(0.5);
        }];
        
        UIButton *hideBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [hideBtn setImage:[UIImage imageNamed:@"ckeckin_close_ico"] forState:UIControlStateNormal];
        [_horCircleContentView addSubview:hideBtn];
        [hideBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.top.bottom.equalTo(_horCircleContentView);
            make.width.equalTo(hideBtn.mas_height).multipliedBy(1.0);
        }];
        [hideBtn addTarget:self action:@selector(hideHorCircleAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _horCircleContentView;
}

-(aji_horCircleView *)horCircleView{
    if (!_horCircleView) {
        _horCircleView = [[aji_horCircleView alloc] init];
        [self.horCircleContentView addSubview:_horCircleView];
        [_horCircleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.bottom.equalTo(self.horCircleContentView);
            make.top.equalTo(self.horCircleContentView).offset(0.5);
            make.right.equalTo(self.horCircleContentView).offset(-30.0);
            if (BET_CONFIG.noticeModel.chat_notice.count) {
                make.height.mas_equalTo(30);
            }else{
                make.height.mas_equalTo(0);
            }
        }];
    }
    return _horCircleView;
}

-(iCarousel *)carousel{
    if (!_carousel) {
        _carousel = [[iCarousel alloc] init];
        _carousel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _carousel.type = iCarouselTypeLinear;
        _carousel.stopAtItemBoundary = NO;
        _carousel.scrollToItemBoundary = YES;
        _carousel.decelerationRate = 0.75;
        _carousel.backgroundColor = [UIColor skinViewScreenColor];
        [self.view insertSubview:_carousel belowSubview:self.winContentView];
        [_carousel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view).offset(0);
            make.left.right.equalTo(self.view);
            make.height.mas_equalTo(0);
            
        }];
    }
    return _carousel;
}

-(UIButton *)moreRoomBtn{
    if (!_moreRoomBtn) {
        _moreRoomBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_moreRoomBtn setBackgroundColor:[UIColor whiteColor]];
        [_moreRoomBtn setTitle:@"更多聊天室" forState:UIControlStateNormal];
        [_moreRoomBtn.titleLabel setFont:[UIFont systemFontOfSize:13]];
        [_moreRoomBtn setTitleColor:COLOR_WITH_HEX(0xD92E2F) forState:UIControlStateNormal];
        [_moreRoomBtn setImage:[[UIImage imageNamed:@"chat_moreRoom_down"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        [_moreRoomBtn.imageView setTintColor:COLOR_WITH_HEX(0xD92E2F)];
        [_moreRoomBtn setTintColor:COLOR_WITH_HEX(0xD92E2F)];
        
        [self.view insertSubview:_moreRoomBtn belowSubview:self.inputToolBar];
        [_moreRoomBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.view).offset(-10);
            make.top.equalTo(self.carousel.mas_bottom).offset(15);
            make.size.mas_equalTo(CGSizeMake(100, 20));
        }];
        
        _moreRoomBtn.clipsToBounds = YES;
        _moreRoomBtn.layer.cornerRadius = 10;
        _moreRoomBtn.layer.borderWidth = 1;
        _moreRoomBtn.layer.borderColor = COLOR_WITH_HEX(0xD92E2F).CGColor;
        [_moreRoomBtn setImagePosition:LXMImagePositionLeft spacing:5];
    }
    return _moreRoomBtn;
}


-(UIButton *)topListButton{
    if (!_topListButton) {
        _topListButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_topListButton setImage:[UIImage imageNamed:@"chat_toplist"] forState:UIControlStateNormal];
        [self.view insertSubview:_topListButton belowSubview:self.inputToolBar];

        [_topListButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.moreRoomBtn.mas_bottom).offset(15);
            make.centerX.equalTo(self.moreRoomBtn);
        }];
    }
    return _topListButton;
}

-(UIButton *)followSortButton{
    if (!_followSortButton) {
        _followSortButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_followSortButton setImage:[UIImage imageNamed:@"chat_sort_followonly"] forState:UIControlStateNormal];
        [_followSortButton setImage:[UIImage imageNamed:@"chat_sort_all"] forState:UIControlStateSelected];
        __block ChatRoomInfoModel *model = nil;
        [CHAT_UTIL.clientInfo.roomInfoList enumerateObjectsUsingBlock:^(ChatRoomInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.identity integerValue] == self.roomID) {
                model = obj;
                *stop = YES;
            }
        }];
        [self.view insertSubview:_followSortButton belowSubview:self.winContentView];
        CGFloat topOffset = ((CHAT_UTIL.suspendModel.isShowAttention && ![CHAT_UTIL.suspendModel.isShowAttention boolValue]) ? 0.0f : 15.0f);
        [_followSortButton mas_makeConstraints:^(MASConstraintMaker *make) {
            if(CHAT_UTIL.suspendModel.isShowAttention && ![CHAT_UTIL.suspendModel.isShowAttention boolValue]){
                make.width.height.mas_equalTo(0);
            }
            
            if ([model.isOpenLeaderboard integerValue]) {
                make.top.equalTo(self.topListButton.mas_bottom).offset(topOffset);
            }else{
                make.top.equalTo(self.moreRoomBtn.mas_bottom).offset(topOffset);
            }
            make.centerX.equalTo(self.moreRoomBtn);
        }];
        if(CHAT_UTIL.suspendModel.isShowAttention && ![CHAT_UTIL.suspendModel.isShowAttention boolValue]){
            BET_CONFIG.userSettingData.chatFollowStatus = ChatFollowStatus_ALL;
            [BET_CONFIG.userSettingData save];
        }
        [_followSortButton setSelected:BET_CONFIG.userSettingData.chatFollowStatus];
    }
    return _followSortButton;
}

-(UIButton *)removeAllButton{
    if (!_removeAllButton) {
        _removeAllButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_removeAllButton setImage:[UIImage imageNamed:@"chat_removeall"] forState:UIControlStateNormal];
        [self.view insertSubview:_removeAllButton belowSubview:self.inputToolBar];
        [_removeAllButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.followSortButton.mas_bottom).offset(15);
            make.centerX.equalTo(self.followSortButton);
        }];
    }
    return _removeAllButton;
}

-(ChatSuspendView *)suspendView{
    if (!_suspendView) {
        _suspendView = [[ChatSuspendView alloc] init];
        _suspendView.size = CGSizeMake(60, 80);
        [self.view insertSubview:_suspendView aboveSubview:self.winContentView];
        _suspendView.titile = CHAT_UTIL.suspendModel.app_float_name;
        _suspendView.imgUrlStr = [NSString stringWithFormat:@"%@/views/image/%@",SerVer_Url,CHAT_UTIL.suspendModel.app_float_image];
        [self.view insertSubview:_suspendView aboveSubview:self.winContentView];
        @weakify(self);
        [_suspendView addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
            @strongify(self);
            [self jumpWebViewController];
        }];
        [_suspendView addPanGestureRecognizer:^(UIPanGestureRecognizer *recognizer, NSString *gestureId) {
            @strongify(self);
            [self handlePanGesture:recognizer];
        }];
    }
    return _suspendView;
}

-(ChatStickMeesageView *)stickMessageView{
    if (!_stickMessageView) {
        _stickMessageView = [[[NSBundle mainBundle] loadNibNamed:@"ChatStickMeesageView" owner:self options:nil] firstObject];
        [self.view insertSubview:_stickMessageView aboveSubview:self.winContentView];
        [_stickMessageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.top.equalTo(self.tableView);
            make.height.mas_equalTo(0);
        }];
        @weakify(self);
        _stickMessageView.closeHandle = ^(Message *message) {
            @strongify(self);
            [self.ignorStickMessageList addObject:message];
            self.stickMessageView.message = nil;
            [self.stickMessageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.right.equalTo(self.view);
                make.top.equalTo(self.tableView);
                make.height.mas_equalTo(0);
            }];
        };
        _stickMessageView.jumpHandle = ^(Message *message) {
            @strongify(self);
            if (message.chatType == ChatMessageType_TEXT) {
                ChatStickPopViewController *vc = [[ChatStickPopViewController alloc] initWithNibName:@"ChatStickPopViewController" bundle:nil];
                vc.transitioningDelegate = self;
                vc.modalPresentationStyle = UIModalPresentationCustom;
                [[NAVI_MANAGER getCurrentVC] presentViewController:vc animated:YES completion:nil];
                vc.message = message;
            }else if (message.chatType == ChatMessageType_IMG){
                PBViewController *pbViewController = [PBViewController new];
                pbViewController.imageViewClass = UIImageView.class;
                pbViewController.pb_dataSource = self;
                pbViewController.pb_delegate = self;
                pbViewController.pb_startPage = 0;
                [self presentViewController:pbViewController animated:YES completion:nil];
            }
            
        };
    }
    return _stickMessageView;
}

-(ChatMetricsView *)metricsView{
    if (!_metricsView) {
        _metricsView = [[ChatMetricsView alloc] init];
        [self.view insertSubview:_metricsView aboveSubview:self.winContentView];
        [_metricsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view).offset(5);
            make.left.equalTo(self.view).offset(-12.5);
        }];
    }
    return _metricsView;
}
-(void)setRoomID:(NSInteger)roomID{
    
    _roomID = roomID;
    if (_action) {
        _action.delegate = nil;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [CHAT_UTIL.clientInfo.roomInfoList enumerateObjectsUsingBlock:^(ChatRoomInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.identity integerValue] == _roomID) {
                self.naviTitleView.title = obj.name;
                *stop = YES;
            }
        }];
        [self clean];
        if (_roomID != 0) {
            self.action.delegate = self;
        }
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (CHAT_UTIL.connectStatus == ChatUtilityConnectStatus_DISCONNECTE) {
            self.naviTitleView.title = @"未连接";
        }else if (CHAT_UTIL.connectStatus == ChatUtilityConnectStatus_UNCONNECTE) {
            self.naviTitleView.title = @"无法连接";
        }else if (CHAT_UTIL.connectStatus == ChatUtilityConnectStatus_CONNECTING) {
            self.naviTitleView.title = @"连接中...";
        }else if (CHAT_UTIL.connectStatus == ChatUtilityConnectStatus_CONNECTED_JOINNING) {
            self.naviTitleView.title = @"加入房间...";
        }else if (CHAT_UTIL.connectStatus == ChatUtilityConnectStatus_CONNECTED_JOINED ||
                  CHAT_UTIL.connectStatus == ChatUtilityConnectStatus_CONNECTED_WAITJOIN ||
                  CHAT_UTIL.connectStatus == ChatUtilityConnectStatus_CONNECTED_JOINFAIL) {
            [CHAT_UTIL.clientInfo.roomInfoList enumerateObjectsUsingBlock:^(ChatRoomInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj.identity integerValue] == _roomID) {
                    self.naviTitleView.title = obj.name;
                    *stop = YES;
                }
            }];
        }else{
            self.naviTitleView.title = @"";
        }
    });
}


-(CABasicAnimation *)winEnterBasicAni{
    if (!_winEnterBasicAni) {
        _winEnterBasicAni = [CABasicAnimation animation];
        _winEnterBasicAni.keyPath = @"position";
        
        _winEnterBasicAni.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
        _winEnterBasicAni.fromValue = [NSValue valueWithCGPoint:CGPointMake(WIDTH, self.winContentView.ycz_y + TABBAR_HEIGHT)];
        _winEnterBasicAni.toValue = [NSValue valueWithCGPoint:CGPointMake((WIDTH - self.winContentView.ycz_width) / 2.0, self.winContentView.ycz_y + TABBAR_HEIGHT)];
        _winEnterBasicAni.duration = 1;
        _winEnterBasicAni.fillMode = kCAFillModeForwards;
        _winEnterBasicAni.removedOnCompletion = NO;
        _winEnterBasicAni.delegate = self;
    }
    return _winEnterBasicAni;
}
-(CABasicAnimation *)winOutBasicAni{
    if (!_winOutBasicAni) {
        _winOutBasicAni = [CABasicAnimation animation];
        _winOutBasicAni.keyPath = @"position";
        _winOutBasicAni.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        _winOutBasicAni.fromValue = [NSValue valueWithCGPoint:CGPointMake((WIDTH - self.winContentView.ycz_width) / 2.0, self.winContentView.ycz_y + TABBAR_HEIGHT)];
        _winOutBasicAni.toValue = [NSValue valueWithCGPoint:CGPointMake(- self.winContentView.ycz_width, self.winContentView.ycz_y + TABBAR_HEIGHT)];
        _winOutBasicAni.duration = 1;
        _winOutBasicAni.fillMode = kCAFillModeForwards;
        _winOutBasicAni.removedOnCompletion = NO;
        _winOutBasicAni.delegate = self;
        
    }
    return _winOutBasicAni;
}

-(CABasicAnimation *)vipEnterBasicAni{
    if (!_vipEnterBasicAni) {
        _vipEnterBasicAni = [CABasicAnimation animation];
        _vipEnterBasicAni.keyPath = @"position";
        _vipEnterBasicAni.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
        _vipEnterBasicAni.fromValue = [NSValue valueWithCGPoint:CGPointMake(WIDTH, self.vipEnterContentView.ycz_y + TABBAR_HEIGHT)];
        _vipEnterBasicAni.toValue = [NSValue valueWithCGPoint:CGPointMake((WIDTH - self.vipEnterContentView.ycz_width) / 2.0, self.vipEnterContentView.ycz_y + TABBAR_HEIGHT)];
        _vipEnterBasicAni.duration = 1;
        _vipEnterBasicAni.fillMode = kCAFillModeForwards;
        _vipEnterBasicAni.removedOnCompletion = NO;
        _vipEnterBasicAni.delegate = self;
    }
    return _vipEnterBasicAni;
}

-(CABasicAnimation *)vipOutBasicAni{
    if (!_vipOutBasicAni) {
        _vipOutBasicAni = [CABasicAnimation animation];
        _vipOutBasicAni.keyPath = @"position";
        _vipOutBasicAni.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        _vipOutBasicAni.fromValue = [NSValue valueWithCGPoint:CGPointMake((WIDTH - self.vipEnterContentView.ycz_width) / 2.0, self.vipEnterContentView.ycz_y + TABBAR_HEIGHT)];
        _vipOutBasicAni.toValue = [NSValue valueWithCGPoint:CGPointMake(- self.vipEnterContentView.ycz_width, self.vipEnterContentView.ycz_y + TABBAR_HEIGHT)];
        _vipOutBasicAni.duration = 1;
        _vipOutBasicAni.fillMode = kCAFillModeForwards;
        _vipOutBasicAni.removedOnCompletion = NO;
        _vipOutBasicAni.delegate = self;
        
    }
    return _vipOutBasicAni;
}

-(NSMutableArray<Message *> *)messageList{
    if (!_messageList) {
        _messageList = [NSMutableArray array];
    }
    return _messageList;
}

-(NSMutableArray<Message *> *)vipEnterMessageList{
    if (!_vipEnterMessageList) {
        _vipEnterMessageList = [NSMutableArray array];
    }
    return _vipEnterMessageList;
}

-(BOOL)winPlaying{
    return (_winEnterBasicAni || _winOutBasicAni);
}

-(BOOL)vipEnterPlaying{
    return (_vipEnterBasicAni || _vipOutBasicAni);
}

- (CAEmitterLayer *)streamerLayer{
    if (!_streamerLayer) {
        _streamerLayer = [CAEmitterLayer layer];
        _streamerLayer.emitterPosition = CGPointMake(WIDTH / 2.0,
                                                     self.winContentView.ycz_y + TABBAR_HEIGHT + self.winContentView.ycz_height / 2.0);
        _streamerLayer.emitterSize   = CGSizeMake(WIDTH / 2.0, 10);
        _streamerLayer.masksToBounds = NO;
        _streamerLayer.emitterMode = kCAEmitterLayerVolume;
        _streamerLayer.renderMode = kCAEmitterLayerUnordered;
        _streamerLayer.emitterShape = kCAEmitterLayerRectangle;
        _streamerLayer.preservesDepth = NO;
        [self.view.layer addSublayer:_streamerLayer];
    }
    return _streamerLayer;
}

-(NSMutableArray <NSString *>*)cellKeyList{
    if (!_cellKeyList) {
        _cellKeyList = [NSMutableArray array];
    }
    return _cellKeyList;
}

-(NSMutableArray<CAEmitterCell *> *)CAEmitterCellArr{
    if (!_CAEmitterCellArr) {
        _CAEmitterCellArr = [NSMutableArray array];
        for (int i = 0; i < 24; i++){
            CAEmitterCell * cell = [self emitterCell:i];
            [_CAEmitterCellArr addObject:cell];
        }
    }
    return _CAEmitterCellArr;
}



-(NSMutableArray<Message *> *)ignorStickMessageList{
    if (!_ignorStickMessageList) {
//        NSDictionary *data = [PPNetworkCache getHttpCacheForKey:CHAT_IGNOR_DATA];
//        NSArray *list = [data objectForKey:[NSString stringWithFormat:@"%@+%li",USER_DATA_MANAGER.userInfoData.account,self.roomID]];
        _ignorStickMessageList = @[].mutableCopy;
    }
    return _ignorStickMessageList;
}

-(ChoosePrivateChatViewController *)chooseUserListViewController{
    if (!_chooseUserListViewController) {
        _chooseUserListViewController = [[ChoosePrivateChatViewController alloc] initWithNibName:@"ChoosePrivateChatViewController" bundle:nil];
    }
    return _chooseUserListViewController;
}

-(ChatJoinRoomBubbleTools *)joinRoomBubbleTools{
    if (!_joinRoomBubbleTools) {
        _joinRoomBubbleTools = [[ChatJoinRoomBubbleTools alloc] initWithSupLayer:self.view.layer];
        _joinRoomBubbleTools.startPosition = self.view.center;
        _joinRoomBubbleTools.fontSize = 15.0;
        _joinRoomBubbleTools.startPosition = CGPointMake(5, CGRectGetMaxY(self.tableView.frame));
        _joinRoomBubbleTools.fontColor = [UIColor whiteColor];
        _joinRoomBubbleTools.shadowColor = COLOR_WITH_HEX(0xd2d2d2);
        _joinRoomBubbleTools.maxAnimationHeight = 85;
        _joinRoomBubbleTools.duration = 3.0;

    }
    return _joinRoomBubbleTools;
}

NSString *vipEnterSuffixByLevel(NSNumber *level){
    NSInteger kIdx = [level integerValue];
    if (kIdx == 1 ||
        kIdx == 2 ||
        kIdx == 3) {
        return @"tiger";
    }else if (kIdx == 4){
        return @"horse";
    }else if (kIdx == 5){
        return @"redDragon";
    }else if (kIdx == 6 ||
              kIdx == 101 ||
              kIdx == 102 ||
              kIdx == 103){
        return @"goldDragon";
    }else{
        return @"tiger";
    }
}

NSString *vipEnterNameByLevel(NSNumber *level){
    NSInteger kIdx = [level integerValue];
    if (kIdx == 1 ||
        kIdx == 2 ||
        kIdx == 3) {
        return @"雄狮";
    }else if (kIdx == 4){
        return @"白马";
    }else if (kIdx == 5){
        return @"烈焰巨龙";
    }else if (kIdx == 6 ||
              kIdx == 101 ||
              kIdx == 102 ||
              kIdx == 103){
        return @"金色巨龙";
    }else{
        return @"雄狮";
    }
}
@end
