//
//  ChoosePrivateChatViewController.m
//  Bet365
//
//  Created by adnin on 2019/2/15.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChoosePrivateChatViewController.h"
#import "ChoosePrivateChatCell.h"

@interface ChoosePrivateChatViewController ()<UITableViewDelegate ,UITableViewDataSource,ChoosePrivateChatCellDelegate>

@property (strong, nonatomic) IBOutlet UILabel *titileLabel;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *titileView;
#pragma mark - Data
@property (strong, nonatomic) NSMutableArray <ChatUserInfoEntity *>*entityList;

@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@end

@implementation ChoosePrivateChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImage *img = [UIImage imageNamed:@ "chat_close_icon" ];
    img = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.closeBtn setImage:img forState:UIControlStateNormal];
    [self.closeBtn setTintColor:[UIColor skinNaviTintColor]];
    UIImageView *bg = [[UIImageView alloc] initWithImage:[UIImage skinGradientHorNaviImage]];
    [self.titileView insertSubview:bg atIndex:0];
    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(bg.superview);
    }];
    self.titileLabel.textColor = [UIColor skinTextTitileColor];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - ChoosePrivateChatCellDelegate
-(void)tapPrivateSubmitChoosePrivateCell:(ChoosePrivateChatCell *)cell{
    [self dismissViewControllerAnimated:YES completion:^{
        self.handle ? self.handle(cell.model) : nil;
    }];
    
    
}
#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.userList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ChoosePrivateChatCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChoosePrivateChatCell"];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"ChoosePrivateChatCell" owner:self options:nil].firstObject;
    }
    cell.model = [self.userList safeObjectAtIndex:indexPath.row];
    cell.entity = [self.userList safeObjectAtIndex:indexPath.row];
    cell.delegate = self;
    cell.unread = [CHAT_UTIL getUnreadMessageCountByUserId:cell.model.userId];
    return cell;
}

- (IBAction)closeAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(NSMutableArray<ChatUserInfoEntity *> *)entityList{
    if (!_entityList) {
        _entityList = @[].mutableCopy;
    }
    return _entityList;
}

-(void)setUserList:(NSArray<ChatUserInfoModel *> *)userList{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId = %li",CHAT_UTIL.clientInfo.userInfo.userId];
    ChatUserInfoModel *mine = [[userList filteredArrayUsingPredicate:predicate] safeObjectAtIndex:0];
    
    predicate = [NSPredicate predicateWithFormat:@"(status = 1) AND (userId != %li)",CHAT_UTIL.clientInfo.userInfo.userId];
    NSMutableArray *mUserList = [userList filteredArrayUsingPredicate:predicate].mutableCopy;
    if (mine) {
        mUserList.count ? [mUserList insertObject:mine atIndex:0] : [mUserList addObject:mine];
    }
    
    [mUserList enumerateObjectsUsingBlock:^(ChatUserInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        ChatUserInfoEntity *entity = [ChatUtilityGroupSubscribeData getUserInfoListEntityByUserId:obj.userId].firstObject;
        if (!entity) {
            entity = [ChatUserInfoEntity new];
        }
        [self.entityList addObject:entity];
    }];
    
    _userList = mUserList;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

-(void)setTitile:(NSString *)titile{
    self.titileLabel.text = titile;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

