//
//  ChatLotteryPopViewController.h
//  Bet365
//
//  Created by HHH on 2018/9/27.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Message.h"
typedef void(^ChatLotteryPopBlock)(NSNumber* lotteryID,NSInteger scale);

typedef void(^ChatLotteryPopMoreBlock)(Message *message);

@interface ChatLotteryPopViewController : Bet365ViewController

@property (nonatomic,strong)LotteryOpenInfoModel *openInfoModel;

@property (nonatomic,strong)Message *message;

@property (nonatomic,copy)ChatLotteryPopBlock handle;

@property (nonatomic,copy)ChatLotteryPopMoreBlock moreHandle;

@end
