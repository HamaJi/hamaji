//
//  ChatRightViewController.m
//  Bet365
//
//  Created by HHH on 2018/10/9.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatRightViewController.h"
#import "ChatRightCell.h"
#import "ChatHeaderView.h"
#import "LukeDrawViewController.h"
#import <VGParallaxHeader/UIScrollView+VGParallaxHeader.h>
@interface ChatRightViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) ChatHeaderView *headerView;

@end

@implementation ChatRightViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self.tableView setParallaxHeaderView:self.headerView mode:VGParallaxHeaderModeCenter height:120];
    @weakify(self);
    [[RACSignal combineLatest:@[[RACObserve(CHAT_UTIL.clientInfo.userInfo,nickName) distinctUntilChanged],[RACObserve(CHAT_UTIL.clientInfo.userInfo,avatar) distinctUntilChanged]]] subscribeNext:^(RACTuple * _Nullable x) {
        @strongify(self);
        self.headerView.nickName = CHAT_UTIL.clientInfo.userInfo.nickName;
        self.headerView.avatorStr = CHAT_UTIL.clientInfo.userInfo.avatar;
        [self.tableView reloadData];
    }];

    

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITabelViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.rithgItems.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ChatRightCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatRightCell"];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"ChatRightCell" owner:self options:nil].firstObject;
    }
    ChatRightItem *data = [self.rithgItems objectAtIndex:indexPath.row];
    cell.item = data;
    return cell;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.handle ? self.handle(self.rithgItems[indexPath.row]) : nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - GET/SET
-(ChatHeaderView *)headerView{
    if (!_headerView) {
        
        _headerView = [[[NSBundle mainBundle] loadNibNamed:@"ChatHeaderView" owner:self options:nil] firstObject];
        _headerView.ycz_width = self.tableView.bounds.size.width;
        @weakify(self)
        _headerView.handle = ^{
            @strongify(self)
            self.handle ? self.handle(nil) : nil;
            [self dismissViewControllerAnimated:YES completion:nil];
        };
    }
    return _headerView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
