//
//  ChatLotteryPopViewController.m
//  Bet365
//
//  Created by HHH on 2018/9/27.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatLotteryPopViewController.h"

@interface ChatLotteryPopViewController ()
{
    NSInteger _kScal;
}
@property (weak, nonatomic) IBOutlet UIView *titileView;
@property (strong, nonatomic) IBOutlet UILabel *titileLabel;
@property (strong, nonatomic) IBOutlet UILabel *lotteryNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *playNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *turnumbLabel;
@property (strong, nonatomic) IBOutlet UILabel *contentLabel;
@property (strong, nonatomic) IBOutlet UILabel *currentTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *moneyLabel;
@property (strong, nonatomic) IBOutlet UITextField *followScaleTf;
@property (strong, nonatomic) IBOutlet UILabel *followMoney;
@property (strong, nonatomic) IBOutlet UIButton *subMitBtn;
@property (strong, nonatomic) IBOutlet UIButton *moreBtn;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@property (nonatomic)NSInteger surplusTime;
@end

@implementation ChatLotteryPopViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
}
-(void)dealloc{
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImageView *bg = [[UIImageView alloc] initWithImage:[UIImage skinGradientHorNaviImage]];
    [self.titileView insertSubview:bg atIndex:0];
    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(bg.superview);
    }];
    
    self.titileLabel.textColor = [UIColor skinTextTitileColor];
    self.titileLabel.text = @"注单详情";
    UIImage *img = [UIImage imageNamed:@ "chat_close_icon" ];
    img = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.closeBtn setImage:img forState:UIControlStateNormal];
    [self.closeBtn setTintColor:[UIColor skinNaviTintColor]];
    
    [self.subMitBtn setBackgroundImage:[UIImage imageWithColor:COLOR_WITH_HEX(0xd2d2d2)] forState:UIControlStateDisabled];
    [self.subMitBtn setBackgroundImage:[UIImage imageWithColor:COLOR_WITH_HEX(0xD92E2F)] forState:UIControlStateNormal];
    
    [self.moreBtn setBackgroundImage:[UIImage imageWithColor:COLOR_WITH_HEX(0xd2d2d2)] forState:UIControlStateDisabled];
    [self.moreBtn setBackgroundImage:[UIImage imageWithColor:COLOR_WITH_HEX(0xD92E2F)] forState:UIControlStateNormal];
    @weakify(self);
    [[[RACSignal interval:0.1 onScheduler:[RACScheduler mainThreadScheduler]] takeUntil:self.rac_willDeallocSignal ] subscribeNext:^(id x) {
        @strongify(self);
        self.currentTimeLabel.text = [NSString stringWithFormat:@"%@:%@:%@",
                                [NSString timeFormat:self.surplusTime/3600],
                                [NSString timeFormat:self.surplusTime%3600/60],
                                [NSString timeFormat:self.surplusTime%60]];
        [self.subMitBtn setEnabled:self.surplusTime > 0];
    }];
    
    [[RACObserve(self, message) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        Message *message = x;
        self.lotteryNameLabel.text = message.content.gameName;
        self.playNameLabel.text = message.content.playName;
        self.turnumbLabel.text = message.content.turnNum;
        self.contentLabel.text = message.content.betContent;
//        self.moneyLabel.text = [NSString stringWithFormat:@"%.2f",[message.content.totalMoney floatValue]];
        [self.subMitBtn setEnabled:self.surplusTime > 0];
        self.moneyLabel.text = [NSString stringWithFormat:@"%.2f元/倍,总计%.2f元",[message.content.oneMoney doubleValue],[message.content.totalMoney floatValue]];
        self.followMoney.text = @"0元";
    }];
    [self.followScaleTf.rac_textSignal subscribeNext:^(NSString * _Nullable x) {
        @strongify(self);
        
        if ([x deptNumInputShouldNumber:x]) {
            _kScal = [x integerValue];
        }else{
            _kScal = 0;
        }
        self.followMoney.text = [NSString stringWithFormat:@"%.2f元",[self.message.content.oneMoney doubleValue] *_kScal];
        
    }];
    
    // Do any additional setup after loading the view from its nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

-(NSInteger)surplusTime{

    NSInteger time = [self.message.content.betEndTime integerValue] / 1000 - [[NSDate date] timeIntervalSince1970] + self.openInfoModel.serverTimeDiffence;
    return (time >= 0) ?  time : 0;
}

- (IBAction)subMitAction:(id)sender {
    if (!_kScal) {
        [SVProgressHUD showErrorWithStatus:@"请填写正确的跟投倍数"];
        return;
    }
    [self dismissViewControllerAnimated:YES completion:^{
        self.handle? self.handle(self.message.content.gameId,_kScal) : nil;
    }];
}

- (IBAction)moreAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        self.moreHandle? self.moreHandle(self.message) : nil;
    }];
}

- (IBAction)closeAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
