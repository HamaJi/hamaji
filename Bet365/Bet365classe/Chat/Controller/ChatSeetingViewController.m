//
//  ChatSeetingViewController.m
//  Bet365
//
//  Created by HHH on 2018/10/2.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatSeetingViewController.h"
#import "ChatChooseAvatorViewController.h"

@interface ChatSeetingViewController ()

@property (strong, nonatomic) IBOutlet UIImageView *avatorIconImg;
@property (strong, nonatomic) IBOutlet UIButton *nickNameBtn;
@property (strong, nonatomic) IBOutlet UISwitch *swichWinBtn;
@property (strong, nonatomic) IBOutlet UISwitch *swichLotteryBtn;
@property (strong, nonatomic) IBOutlet UISwitch *swichBottomBarBtn;
@property (weak, nonatomic) IBOutlet UISwitch *pingSwitch;

@property (strong, nonatomic) IBOutlet UISwitch *chatMessgeNotifcationSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *chatMessageNotifcationSoundSwitch;
@property (nonatomic,assign) BOOL notifacationAllow;

@end

@implementation ChatSeetingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.naviRightItem = Bet365NaviRightItemMenu;
    
    [self.swichWinBtn setOn:!BET_CONFIG.userSettingData.closeChatWinFireAnimation animated:YES];
    [self.swichLotteryBtn setOn:!BET_CONFIG.userSettingData.hideChatLotteryTopView animated:YES];
    [self.swichBottomBarBtn setOn:!BET_CONFIG.userSettingData.hideChatBottomBar animated:YES];
    [self.chatMessageNotifcationSoundSwitch setOn: BET_CONFIG.userSettingData.openChatMeessageNotifacationSound animated:YES];
    [self.pingSwitch setOn:!BET_CONFIG.userSettingData.hideChatPingView animated:YES];
    @weakify(self);
    [Bet365Tool checkCurrentNotificationStatusCompleted:^(BOOL allowd) {
        @strongify(self);
        self.notifacationAllow = allowd;
    }];
    
    [[RACObserve(self, notifacationAllow) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        dispatch_async(dispatch_get_main_queue(), ^{
            BOOL allowd = [x boolValue];
            [self.chatMessgeNotifcationSwitch setOn:(!BET_CONFIG.userSettingData.hideChatMessageNotification &&
                                                     allowd)
                                           animated:YES];
        });
    }];
    
    [[RACObserve(CHAT_UTIL.clientInfo.userInfo,nickName) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        [self.nickNameBtn setTitle:x forState:UIControlStateNormal];
    }];
    
    [[RACObserve(CHAT_UTIL.clientInfo.userInfo,avatar) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        [self.avatorIconImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SerVer_Url,x]] placeholderImage:[UIImage imageNamed:@"chat_userAvator_placeHolder"]];
    }];
    
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)swichAction:(UISwitch *)sender {
    switch (sender.tag) {
        case 0:
            BET_CONFIG.userSettingData.closeChatWinFireAnimation = !sender.on;
            break;
        case 1:
            BET_CONFIG.userSettingData.hideChatLotteryTopView = !sender.on;
            break;
        case 2:
            BET_CONFIG.userSettingData.hideChatBottomBar = !sender.on;
            break;
        case 3:
        {
            if (!self.notifacationAllow) {
                [self showAlrtToSetting];
                [self.chatMessgeNotifcationSwitch setOn:NO];
            }else{
                BET_CONFIG.userSettingData.hideChatMessageNotification = !sender.on;
            }
        }
            break;
        case 4:
        {
            BET_CONFIG.userSettingData.openChatMeessageNotifacationSound = sender.on;
        }
            break;
        case 5:
        {
            BET_CONFIG.userSettingData.hideChatPingView = !sender.on;
        }
            break;
        default:
            break;
    }
    [BET_CONFIG.userSettingData save];
}

- (IBAction)nickNameAction:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"修改昵称" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = CHAT_UTIL.clientInfo.userInfo.nickName;
    }];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"提交" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if (alert.textFields.lastObject.text.length == 0){
            [SVProgressHUD showErrorWithStatus:@"您提交的昵称无改动"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self nickNameAction:nil];
            });
            return;
        }
        [CHAT_UTIL updateUserAvator:nil NickName:alert.textFields.lastObject.text Completed:^(BOOL success) {
            if (success) {
                CHAT_UTIL.clientInfo.userInfo.nickName = alert.textFields.lastObject.text;
            }
        }];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:action1];
    [alert addAction:action2];
    [self presentViewController:alert animated:YES completion:nil];

}

- (IBAction)avatorAction:(id)sender {
    ChatChooseAvatorViewController *vc = [[ChatChooseAvatorViewController alloc] initWithNibName:@"ChatChooseAvatorViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
    vc.hidesBottomBarWhenPushed = YES;
    vc.handle = ^(NSString *imgStr) {
        [CHAT_UTIL updateUserAvator:imgStr NickName:nil Completed:^(BOOL success) {
            if (success) {
                CHAT_UTIL.clientInfo.userInfo.avatar = imgStr;
            }
        }];
    };
}

-(void) showAlrtToSetting
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"您还没有允许推送权限" message:@"去设置一下吧" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction * setAction = [UIAlertAction actionWithTitle:@"去设置" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSURL * url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            }
        });
    }];
    
    [alert addAction:cancelAction];
    [alert addAction:setAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
