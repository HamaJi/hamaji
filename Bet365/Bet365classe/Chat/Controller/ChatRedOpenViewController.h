//
//  ChatRedOpenViewController.h
//  Bet365
//
//  Created by HHH on 2018/11/22.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSUInteger, ChatRedTapStatus) {
    ChatRedTapStatus_NONE,
    /** 打开*/
    ChatRedTapStatus_OPEN,
    /** 红包详情*/
    ChatRedTapStatus_RESULTS,

    
};
typedef void(^ChatOpenViewBlock)(ChatRedTapStatus status);

@interface ChatRedOpenViewController : Bet365ViewController

@property (nonatomic,strong)ChatRedPacketModel *model;

@property (nonatomic,copy)ChatOpenViewBlock handle;

@end
