//
//  ChatChooseAvatorViewController.m
//  Bet365
//
//  Created by HHH on 2018/10/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatChooseAvatorViewController.h"
#import "ChatChooseAvatorCell.h"
#import <UICollectionViewLeftAlignedLayout/UICollectionViewLeftAlignedLayout.h>
#import <MXSegmentedPager/MXSegmentedPager.h>
#import "ChoosAvatorCollectionVCCollectionViewController.h"
@interface ChatChooseAvatorViewController ()<MXSegmentedPagerDelegate,MXSegmentedPagerDataSource>


@property (assign, nonatomic) NSUInteger segmentIdx;

@property (strong, nonatomic) NSDictionary *data;

@property (nonatomic, strong) MXSegmentedPager *segmentedPager;

@property (nonatomic, strong) NSDictionary *viewAndTitleDitc;

@end

@implementation ChatChooseAvatorViewController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.naviRightItem = Bet365NaviRightItemMenu;
    
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    @weakify(self);
    [CHAT_UTIL getAvatorListCompleted:^(NSDictionary *data) {
        @strongify(self);
        self.data = data;
        self.viewAndTitleDitc = nil;
        self.segmentedPager.delegate = self;
        self.segmentedPager.dataSource = self;
        [self.segmentedPager reloadData];
    }];
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Events



#pragma - mark MXSegmentedPagerDelegate
- (NSInteger)numberOfPagesInSegmentedPager:(MXSegmentedPager *)segmentedPager{
    return self.data.allKeys.count;
}

- (NSString *)segmentedPager:(MXSegmentedPager *)segmentedPager titleForSectionAtIndex:(NSInteger)index{
    return self.data.allKeys[index];
}
- (UIView *)segmentedPager:(MXSegmentedPager *)segmentedPager viewForPageAtIndex:(NSInteger)index{
    return [self.viewAndTitleDitc[self.data.allKeys[index]] collectionView];
}

-(void)segmentedPager:(MXSegmentedPager *)segmentedPager didSelectViewWithIndex:(NSInteger)index{
    
}
#pragma mark - GET/SET
-(NSDictionary *)viewAndTitleDitc{
    if (!_viewAndTitleDitc) {
        @weakify(self);
        NSMutableArray *vcList = [NSMutableArray array];
        [[self.data allKeys] enumerateObjectsUsingBlock:^(NSString *  _Nonnull key, NSUInteger idx, BOOL * _Nonnull stop) {
            ChoosAvatorCollectionVCCollectionViewController *vc = [[ChoosAvatorCollectionVCCollectionViewController alloc] initWithList:self.data[key] PageIdx:idx];
            
            vc.handle = ^(NSString *imgStr) {
                @strongify(self);
                self.handle ? self.handle(imgStr) : nil;
                [self.navigationController popViewControllerAnimated:YES];
            };
            [vcList addObject:vc];
        }];
        _viewAndTitleDitc = [NSDictionary dictionaryWithObjects:vcList forKeys:self.data.allKeys];
    }
    return _viewAndTitleDitc;
}

-(MXSegmentedPager *)segmentedPager{
    if (_segmentedPager == nil) {
        _segmentedPager = [[MXSegmentedPager alloc] init];
        [self.view addSubview:_segmentedPager];
        _segmentedPager.segmentedControl.segmentEdgeInset = UIEdgeInsetsMake(0, 10, 0, 10);
        _segmentedPager.segmentedControl.selectionIndicatorColor = [UIColor skinViewKitNorColor];
        _segmentedPager.segmentedControl.selectionIndicatorHeight = 2;
        _segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe;
        _segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        _segmentedPager.segmentedControl.verticalDividerEnabled = NO;
        _segmentedPager.segmentedControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleFixed;
        
        /** 非选状态 **/
        _segmentedPager.segmentedControl.titleTextAttributes =
        @{NSForegroundColorAttributeName : [UIColor skinViewKitNorColor],
          NSFontAttributeName : [UIFont systemFontOfSize:13]};
        /** 选中状态 **/
        _segmentedPager.segmentedControl.selectedTitleTextAttributes =
        @{NSForegroundColorAttributeName : [UIColor skinViewKitNorColor],
          NSFontAttributeName : [UIFont boldSystemFontOfSize:15]};
        
        _segmentedPager.segmentedControlEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        _segmentedPager.segmentedControl.backgroundColor = [UIColor skinViewContentColor];
        
//        _segmentedPager.segmentedControl.verticalDividerEnabled = YES;
//        _segmentedPager.segmentedControl.verticalDividerColor = [UIColor redColor];
//        _segmentedPager.segmentedControl.verticalDividerWidth = 1;
        [self.view addSubview:_segmentedPager];
        [_segmentedPager mas_makeConstraints:^(MASConstraintMaker *make) {
            if (@available(iOS 11.0, *)) {
                make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
                make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop).offset(0);
                make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
                make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            }else{
                make.left.right.top.bottom.equalTo(self.view);
            }
        }];
    }
    return _segmentedPager;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
