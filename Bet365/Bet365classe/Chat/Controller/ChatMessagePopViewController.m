//
//  ChatMessagePopViewController.m
//  Bet365
//
//  Created by HHH on 2018/10/2.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatMessagePopViewController.h"
#import "ChatMessagePopViewCell.h"



@interface ChatMessagePopViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *titileView;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet UILabel *titileLabel;

@property (strong, nonatomic) NSMutableArray *list;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableviewConstraintHeight;

@property (strong, nonatomic) ChatFollowEntity *followEntity;

@property (nonatomic,strong)RACSubject *followUpdateSubject;

@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@end

@implementation ChatMessagePopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImageView *bg = [[UIImageView alloc] initWithImage:[UIImage skinGradientHorNaviImage]];
    [self.titileView insertSubview:bg atIndex:0];
    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(bg.superview);
    }];
    
    self.titileLabel.textColor = [UIColor skinTextTitileColor];
    UIImage *img = [UIImage imageNamed:@ "chat_close_icon" ];
    img = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.closeBtn setImage:img forState:UIControlStateNormal];
    [self.closeBtn setTintColor:[UIColor skinNaviTintColor]];
    @weakify(self);
    
    RACSignal *messageSignal = [RACObserve(self, message) distinctUntilChanged];
    RACSignal *ruleSignal = [RACObserve(self, ruleModel) distinctUntilChanged];
    
    [[RACSignal combineLatest:@[messageSignal,
                                ruleSignal,
                                [RACObserve(self, authorHandle) distinctUntilChanged]] reduce:^id _Nonnull(Message *message,ChatRuleModel *ruleModel,ChatMessagePopViewBlock authorHandle){
                                    return @(message != nil && ruleModel != nil && authorHandle != nil );
                                }] subscribeNext:^(id  _Nullable x) {
                                    @strongify(self);
                                    if ([x boolValue]) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [self resetAuthorItems];
                                            ChatUserInfoEntity *entity = [ChatUtilitySubscribeData getUserInfoListEntityByUserId:[self.message.userId integerValue]].firstObject;
                                            self.titileLabel.text = [NSString stringWithFormat:@"【%@】的消息操作",entity.nickName];
                                            [self.tableView reloadData];
                                            self.tableviewConstraintHeight.constant = self.tableView.rowHeight * self.list.count;
                                        });
                                    }
                                }];
    
    [[RACSignal combineLatest:@[messageSignal,[RACObserve(self, sendHandle) distinctUntilChanged]] reduce:^id _Nonnull(Message *message,ChatMessagePopViewBlock sendHandle){
        return @(message != nil && sendHandle != nil);
    }] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        if ([x boolValue]) {
            [self resetSendItems];
            ChatUserInfoEntity *entity = [ChatUtilitySubscribeData getUserInfoListEntityByUserId:[self.message.userId integerValue]].firstObject;

            self.titileLabel.text = [NSString stringWithFormat:@"【%@】的消息操作",entity.nickName];
            [self.tableView reloadData];
            self.tableviewConstraintHeight.constant = self.tableView.rowHeight * self.list.count;
        }
    }];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.list.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ChatMessagePopViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatMessagePopViewCell"];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"ChatMessagePopViewCell" owner:self options:nil].firstObject;
    }
    ChatMessagePopViewTapStatus status = [self.list[indexPath.row] integerValue];
    if (status == ChatMessagePopViewTapStatus_FOLLOW) {
        cell.titile = (self.followEntity != nil) ? @"取消关注" : @"关注";
    }else{
        cell.titile = titileKey(status);
    }
    return cell;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self dismissViewControllerAnimated:YES completion:^{
        ChatMessagePopViewBlock handle;
        if (self.authorHandle) {
            handle = self.authorHandle;
        }else if (self.sendHandle){
            handle = self.sendHandle;
        }else{
            return;
        }
        if ([self.list[indexPath.row] integerValue] == ChatMessagePopViewTapStatus_FOLLOW) {
            NSManagedObjectContext *context = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_defaultContext]];
            [self.data setFollowEntityByFollowId:[self.message.userId integerValue] inContext:context];
            self.followEntity = [self.data getFollowEntityByFollowId:[self.message.userId integerValue] inContext:[NSManagedObjectContext MR_defaultContext]];
        }
        handle([self.list[indexPath.row] integerValue]);
    }];
}

#pragma mark - Private
-(void)resetAuthorItems{
    self.list = nil;

    BOOL isPrivateChat = NO;

    NSArray *privateList = [((ChatUtilityGroupSubscribeData *)self.data).userList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(privateChat == 1) AND (userId == %li OR userId == %li)",[self.message.userId integerValue],CHAT_UTIL.clientInfo.userInfo.userId]];
    isPrivateChat = privateList.count > 0;

    if (isPrivateChat && CHAT_UTIL.clientInfo.userInfo.userId != -1) {
        [self.list addObject:@(ChatMessagePopViewTapStatus_PRIVATECHAT)];
    }
    
    if ([self.ruleModel.isViewUserInfo boolValue] && self.message.chatFrom != ChatMessageFrom_ME) {
        [self.list addObject:@(ChatMessagePopViewTapStatus_USER)];
    }
    if ([self.ruleModel.isGag boolValue] && self.message.chatFrom != ChatMessageFrom_ME) {
        [self.list addObject:@(ChatMessagePopViewTapStatus_UNMESSAGE)];
    }
    if ([self.ruleModel.isBanImg boolValue] && self.message.chatFrom != ChatMessageFrom_ME) {
        [self.list addObject:@(ChatMessagePopViewTapStatus_UNIMG)];
    }
    
    if (self.message.chatFrom == ChatMessageFrom_ME) {
        [self.list addObject:@(ChatMessagePopViewTapStatus_REVOKE)];
    }else if ([self.ruleModel.isDelMessage boolValue]) {
        [self.list addObject:@(ChatMessagePopViewTapStatus_DELETE)];
    }
    
    if (self.message.chatType != ChatMessageType_LOTTERY) {
        if (self.message.chatFrom == ChatMessageFrom_OTHER) {
            [self.list addObject:@(ChatMessagePopViewTapStatus_FOLLOW)];
        }
    }
    
    if ([self.ruleModel.isKick boolValue] && self.message.chatFrom != ChatMessageFrom_ME) {
        [self.list addObject:@(ChatMessagePopViewTapStatus_KIKE)];
    }
    if (self.message.chatType == ChatMessageType_TEXT) {
        [self.list addObject:@(ChatMessagePopViewTapStatus_COPYTEXT)];
    }
    if (self.message.chatType == ChatMessageType_IMG) {
        [self.list addObject:@(ChatMessagePopViewTapStatus_SAVEIMG)];
    }
}

-(void)resetSendItems{
    self.list = nil;
    [self.list addObject:@(ChatMessagePopViewTapStatus_DELETE)];
    [self.list addObject:@(ChatMessagePopViewTapStatus_SENDAGAINT)];
    if (self.message.chatType == ChatMessageType_TEXT) {
        [self.list addObject:@(ChatMessagePopViewTapStatus_COPYTEXT)];
    }
}
#pragma mark - Events
- (IBAction)closeAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSMutableArray *)list{
    if (!_list) {
        
        _list = [NSMutableArray array];
    }
    return _list;
}

-(void)setMessage:(Message *)message{
    _message = message;
    NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
    self.followEntity = [self.data getFollowEntityByFollowId:[message.userId integerValue] inContext:context];
    [self.followUpdateSubject sendNext:@( self.followEntity!=nil )];
}

-(RACSubject *)followUpdateSubject{
    if (!_followUpdateSubject) {
        _followUpdateSubject = [[RACSubject alloc] init];
    }
    return _followUpdateSubject;
}
NSString *titileKey(ChatMessagePopViewTapStatus status){
    
    return @[@"发送消息",
             @"用户信息",
             @"禁止发言",
             @"禁止发图",
             @"踢出用户",
             @"删除消息",
             @"复制消息",
             @"保存图片",
             @"彩票详情",
             @"重新发送",
             @"撤回消息",
             @"关注"][status];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
