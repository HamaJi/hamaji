//
//  ChatViewController+Extension.h
//  Bet365
//
//  Created by adnin on 2019/2/12.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatViewController.h"

@interface ChatViewController ()

-(NSObject<UIViewControllerAnimatedTransitioning> *)presenteAnimation:(UIViewController *)presented;

-(NSObject<UIViewControllerAnimatedTransitioning> *)dismissAnimation:(UIViewController *)dismissed;
@end
