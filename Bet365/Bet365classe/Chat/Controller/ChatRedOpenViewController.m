//
//  ChatRedOpenViewController.m
//  Bet365
//
//  Created by HHH on 2018/11/22.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatRedOpenViewController.h"

@interface ChatRedOpenViewController ()

@property (strong, nonatomic) IBOutlet UIButton *closeBtn;

@property (strong, nonatomic) IBOutlet UILabel *yuanLabel;
@property (strong, nonatomic) IBOutlet UIImageView *userIconView;
@property (strong, nonatomic) IBOutlet UIButton *openBtn;
@property (strong, nonatomic) IBOutlet UIButton *resultBtn;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *toasLabel;
@property (strong, nonatomic) IBOutlet UILabel *contentLabel;

@property (strong, nonatomic) NSArray <UIImage *>*coinImgList;

@property (strong, nonatomic) IBOutlet UIImageView *flipCoinViwe;

@end

@implementation ChatRedOpenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Events
- (IBAction)resultAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        self.handle ? self.handle(ChatRedTapStatus_RESULTS) : nil;
    }];
    
}

- (IBAction)closeAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)openAction:(id)sender {
    self.flipCoinViwe.hidden = NO;
    self.openBtn.alpha = 0;
    self.openBtn.userInteractionEnabled = NO;
    self.flipCoinViwe.animationImages = self.coinImgList;
    self.flipCoinViwe.animationDuration = 0.5;
    [self.flipCoinViwe startAnimating];
    [self dismissViewControllerAnimated:YES completion:^{
        self.handle ? self.handle(ChatRedTapStatus_OPEN) : nil;
    }];
    
}


#pragma mark - GET/SET
-(void)setModel:(ChatRedPacketModel *)model{
    _model = model;
    [self.userIconView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SerVer_Url,model.activityChatRedpacket.createrAvator]]
                         placeholderImage:[UIImage imageNamed:@"chat_userAvator_placeHolder"]];
    self.userNameLabel.text = model.activityChatRedpacket.createrName;
    if (model.activityChatRedpacket.redpacketName.length) {
        self.contentLabel.text = model.activityChatRedpacket.redpacketName;
    }
    
    __block BOOL isContain = NO;
    [model.details enumerateObjectsUsingBlock:^(ChatRedPacketDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.userId integerValue] == CHAT_UTIL.clientInfo.userInfo.userId) {
            isContain = YES;
            *stop = YES;
        }
    }];
    
    if (model.activityChatRedpacket.count == 1 && isContain) {
        self.toasLabel.text = @"给您发了一个红包";
        self.contentLabel.text = [NSString stringWithFormat:@"%.2f",[model.activityChatRedpacket.totalMoney floatValue]];
        self.yuanLabel.text = @"元";
        self.openBtn.hidden = YES;
        self.resultBtn.hidden = NO;
    }else if (model.activityChatRedpacket.count == model.activityChatRedpacket.consumeCount &&
              !isContain){
        self.toasLabel.text = @"";
        self.contentLabel.text = @"手慢了，红包派完了";
        self.openBtn.hidden = YES;
        self.resultBtn.hidden = NO;
    }
    
}

-(NSArray <UIImage *>*)coinImgList{
    if (!_coinImgList) {
        NSMutableArray *arr = @[].mutableCopy;
        for (int i = 0; i<15; i++) {
            UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"chat_flipCoin_%i",i]];
            img ? [arr addObject:img] : nil;
        }
        _coinImgList = arr;
    }
    return _coinImgList;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
