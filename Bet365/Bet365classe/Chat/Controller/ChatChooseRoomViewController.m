//
//  ChatChooseRoomViewController.m
//  Bet365
//
//  Created by HHH on 2018/9/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatChooseRoomViewController.h"

@interface ChatChooseRoomViewController ()
<UICollectionViewDelegate,
UICollectionViewDataSource>

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonnull) ChatInfoModel *chatInfoModel;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *collectionConstraintTop;
@end

@implementation ChatChooseRoomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.collectionView registerNib:[UINib nibWithNibName:@"ChatChooseRoomCell" bundle:nil] forCellWithReuseIdentifier:@"ChatChooseRoomCell"];
    RAC(self,chatInfoModel) = RACObserve(CHAT_UTIL, clientInfo);
    @weakify(self)
    [[RACObserve(self, chatInfoModel) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        [self.chatInfoModel.roomInfoList enumerateObjectsUsingBlock:^(ChatRoomInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *key = [NSString stringWithFormat:@"%@/data/image/room_icon/%li.jpg",SerVer_Url,[obj.identity integerValue]];
            [[SDImageCache sharedImageCache] removeImageForKey:key fromDisk:YES withCompletion:nil];
        }];
        [self.collectionView reloadData];
    }];
    self.collectionConstraintTop.constant = STATUSAND_NAVGATION_HEIGHT;
    dispatch_async(dispatch_get_main_queue(), ^{
        __block NSIndexPath *indexPath = nil;
        [self.chatInfoModel.roomInfoList enumerateObjectsUsingBlock:^(ChatRoomInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.identity integerValue] == self.roomID) {
                indexPath = [NSIndexPath indexPathForItem:idx inSection:0];
            }
        }];
        if (indexPath) {
            [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
    });
    // Do any additional setup after loading the view from its nib.
}
-(void)dealloc{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)closeAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.chatInfoModel.roomInfoList.count;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5, 5, 5, 5);
    
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    
    return 5;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(85,110);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ChatChooseRoomCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatChooseRoomCell" forIndexPath:indexPath];
    cell.roomInfoModel = [self.chatInfoModel.roomInfoList safeObjectAtIndex:indexPath.item];
    cell.roomId = self.roomID;
    @weakify(self);
    cell.handle = ^(ChatRoomInfoModel *model) {
        @strongify(self);
        [self enterRoom:model];
    };
    return cell;
}

#pragma mark - UICollectionViewDelegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    ChatChooseRoomCell *cell = (ChatChooseRoomCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [self enterRoom:cell.roomInfoModel];
}

-(void)enterRoom:(ChatRoomInfoModel *)model{
    if ([model.identity integerValue] == self.roomID) {
        [SVProgressHUD showErrorWithStatus:@"您已在当前房间"];
        
        return;
    }else if (!model.isOpen){
        [SVProgressHUD showErrorWithStatus:@"该房间未开放"];
        return;
    }
    [self dismissViewControllerAnimated:YES completion:^{
        self.handle ? self.handle(model) : nil;
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
