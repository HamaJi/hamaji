//
//  ChatImageEmojiPickViewController.m
//  Bet365
//
//  Created by adnin on 2020/4/25.
//  Copyright © 2020 jesse. All rights reserved.
//

#import "ChatImageEmojiPickViewController.h"

#import "ChatChooseAvatorCell.h"
#import "ChatNetEmojiCacheEntity+CoreDataProperties.h"
typedef NS_ENUM(NSUInteger, ChatEmojiPickViewStatus) {
    /** 工具栏样式*/
    ChatEmojiPickViewStatus_TOOLBAR,
    /** 视图窗口样式*/
    ChatEmojiPickViewStatus_VIEWCONTROLLER,
};


@interface ChatImageEmojiPickViewController ()<
UICollectionViewDelegate,
UICollectionViewDataSource,
UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titileViewLayoutConstraintHeight;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintContentHeight;

@property (nonatomic,strong) NSMutableArray <NetEmojiModel *>*list;

@property (nonatomic,assign) ChatEmojiPickViewStatus status;

@property (nonatomic,assign) NSUInteger page;



@end

@implementation ChatImageEmojiPickViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [self.collectionView registerNib:[UINib nibWithNibName:@"ChatChooseAvatorCell" bundle:nil] forCellWithReuseIdentifier:@"ChatChooseAvatorCell"];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [[ChatNetEmojiCacheEntity MR_findAll] enumerateObjectsUsingBlock:^(__kindof ChatNetEmojiCacheEntity * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSError *error;
            NetEmojiModel *model = [MTLManagedObjectAdapter modelOfClass:NetEmojiModel.class fromManagedObject:obj error:&error];
            [self.list safeAddObject:model];
        }];
        
        
        [self.list sortUsingComparator:^NSComparisonResult(NetEmojiModel*  _Nonnull obj1, NetEmojiModel*  _Nonnull obj2) {
            if ([obj1.time timeIntervalSince1970] > [obj2.time timeIntervalSince1970]) {
                return NSOrderedAscending;
            }
            return NSOrderedDescending;
        }];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.collectionView reloadData];
        });
    });

    
    // Do any additional setup after loading the view from its nib.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)closeAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.status == ChatEmojiPickViewStatus_TOOLBAR) {
        return self.list.count + 1;
    }
    return self.list.count;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0.0;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    
    return 0.0;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(WIDTH / 4.0,WIDTH / 4.0);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ChatChooseAvatorCell  *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatChooseAvatorCell" forIndexPath:indexPath];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"ChatChooseAvatorCell" owner:self options:nil].firstObject;
    }
    if (self.status == ChatEmojiPickViewStatus_TOOLBAR ) {
        if (indexPath.item == 0) {
            cell.path = @"chat_input_bum";
            cell.titileString = @"相册/相机";
        }else{
            NetEmojiModel *model = [self.list safeObjectAtIndex:indexPath.item - 1];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",CHAT_UTIL.clientInfo.imageHost,model.filePath]];
            cell.url = url;
            cell.titileString = @"";
        }
    }else{
        NetEmojiModel *model = [self.list safeObjectAtIndex:indexPath.item];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",CHAT_UTIL.clientInfo.imageHost,model.filePath]];
        cell.url = url;
        cell.titileString = @"";
    }
    return cell;
}

#pragma mark - UICollectionViewDelegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NetEmojiModel *model;
    if (self.status == ChatEmojiPickViewStatus_TOOLBAR) {
        if (indexPath.item == 0) {
            [self dismissViewControllerAnimated:YES completion:^{
                self.imageBlock ? self.imageBlock() : nil;
            }];
            return;
        }
        model = [self.list safeObjectAtIndex:indexPath.item - 1];
    }else if (self.status == ChatEmojiPickViewStatus_VIEWCONTROLLER){
        model = [self.list safeObjectAtIndex:indexPath.item];
    }
    NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
    NSString *predicateStr = [NSString stringWithFormat:@"(filePath = '%@')",model.filePath];
    ChatNetEmojiCacheEntity *entity = [ChatNetEmojiCacheEntity MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:predicateStr] inContext:context];
    NSError *error;
    if (!entity) {
        
        entity = [MTLManagedObjectAdapter managedObjectFromModel:model insertingIntoContext:context error:&error];
    }
    entity.time = [NSDate date];
    [context MR_saveToPersistentStoreAndWait];
    [self dismissViewControllerAnimated:YES completion:^{
        self.emojiBlock ? self.emojiBlock(model) : nil;
    }];
    
//    self.handle ? self.handle([self.list objectAtIndex:indexPath.item]) : nil;
}



#pragma mark - UISearchBarDelegate
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    if (self.status == ChatEmojiPickViewStatus_TOOLBAR) {
        self.status = ChatEmojiPickViewStatus_VIEWCONTROLLER;
        return NO;
    }
    return YES;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self resetNetEmojiList];
    });
}

#pragma mark - NetRequest
-(void)resetNetEmojiList{
    self.page = 1;
    [self getEmojiList];
    
}
-(void)getEmojiList{
    @weakify(self);
    [CHAT_UTIL getImageKey:^(NSString *key) {
        @strongify(self);
        [CHAT_UTIL getEmojiListWithKey:key KeyWord:self.searchBar.text Page:self.page + 1 Rows:40 completed:^(NSArray<NetEmojiModel *> *list) {
            if (self.page == 1) {
                [self.list removeAllObjects];
            }
            if (list.count) {
                self.page += 1;
            }
            
            [self.list addObjectsFromArray:list];
            [self.collectionView reloadData];
            [self.collectionView.mj_header endRefreshing];
            [self.collectionView.mj_footer endRefreshing];
        }];
    }];
    
}

-(void)setStatus:(ChatEmojiPickViewStatus)status{
    if (_status == status) {
        return;
    }
    _status = status;
    if(_status == ChatEmojiPickViewStatus_VIEWCONTROLLER){
        [self.collectionView reloadData];
        self.searchBar.placeholder = @"搜索更多表情";
        self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self resetNetEmojiList];
        }];
        self.collectionView.mj_footer = [MJRefreshAutoStateFooter footerWithRefreshingBlock:^{
            [self getEmojiList];
        }];
        [self.collectionView.mj_header beginRefreshing];
        [UIView animateWithDuration:0.2 animations:^{
            self.titileViewLayoutConstraintHeight.constant = 38.0;
            self.layoutConstraintContentHeight.constant = HEIGHT;
            [self.view layoutIfNeeded];
        }];
    }
}

-(NSMutableArray<NetEmojiModel *> *)list{
    if (!_list) {
        _list = @[].mutableCopy;
    }
    return _list;
}


@end
