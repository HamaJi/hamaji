//
//  ChatUserInfoPopViewController.m
//  Bet365
//
//  Created by HHH on 2018/10/12.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatUserInfoPopViewController.h"

@interface ChatUserInfoPopViewController ()

@property (strong, nonatomic) IBOutlet UILabel *titileLabel;
@property (strong, nonatomic) IBOutlet UILabel *accountLabel;
@property (strong, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *typeLabel;
@property (strong, nonatomic) IBOutlet UIImageView *levelImg;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UIView *titileView;
@end

@implementation ChatUserInfoPopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImage *img = [UIImage imageNamed:@ "chat_close_icon" ];
    img = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.closeBtn setImage:img forState:UIControlStateNormal];
    [self.closeBtn setTintColor:[UIColor skinNaviTintColor]];
    UIImageView *bg = [[UIImageView alloc] initWithImage:[UIImage skinGradientHorNaviImage]];
    [self.titileView insertSubview:bg atIndex:0];
    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(bg.superview);
    }];
    self.titileLabel.textColor = [UIColor skinTextTitileColor];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setModel:(ChatUserInfoModel *)model{
    _model = model;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.titileLabel.text = [NSString stringWithFormat:@"【%@】信息",(_model.nickName ? _model.nickName : @"")];
        self.accountLabel.text = _model.account;
        self.nickNameLabel.text = (_model.nickName ? _model.nickName : @"");
        if ([_model.userType isEqualToString:@"HY"]) {
            self.typeLabel.text = @"正式会员";
        }else if ([_model.userType isEqualToString:@"VHY"]){
            self.typeLabel.text = @"推广会员";
        }else if ([_model.userType isEqualToString:@"TEST"]){
            self.typeLabel.text = @"试玩会员";
        }else{
            self.typeLabel.text = @"";
        }
        NSString *imgPatch = [NSString stringWithFormat:@"chat_level_%li",[_model.level integerValue]];
        [self.levelImg setImage:[UIImage imageNamed:imgPatch]];
    });
}
- (IBAction)closeAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
