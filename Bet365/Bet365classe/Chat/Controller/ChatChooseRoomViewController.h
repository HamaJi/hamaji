//
//  ChatChooseRoomViewController.h
//  Bet365
//
//  Created by HHH on 2018/9/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatChooseRoomCell.h"
/** 选中 */

@interface ChatChooseRoomViewController : Bet365ViewController
@property (nonatomic,assign)NSInteger roomID;
@property (nonatomic,copy)ChatChooseRoomBlock handle;
@end
