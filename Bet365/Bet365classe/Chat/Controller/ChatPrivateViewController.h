//
//  ChatPrivateViewController.h
//  Bet365
//
//  Created by adnin on 2019/2/10.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatViewController.h"
@interface ChatPrivateViewController : ChatViewController

@property (nonatomic,strong)ChatUserInfoModel *userModel;

@end
