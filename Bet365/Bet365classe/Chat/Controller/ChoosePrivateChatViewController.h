//
//  ChoosePrivateChatViewController.h
//  Bet365
//
//  Created by adnin on 2019/2/15.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ChatPrivateChooseBlock)(ChatUserInfoModel *model);

@interface ChoosePrivateChatViewController : Bet365ViewController

@property (nonatomic,strong)NSArray <ChatUserInfoModel *>*userList;

@property (nonatomic)NSString *titile;

@property (nonatomic,copy)ChatPrivateChooseBlock handle;

@end
