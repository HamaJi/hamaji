//
//  ChatTopListViewController.m
//  Bet365
//
//  Created by HHH on 2018/12/4.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatTopListViewController.h"
#import "ChatTopListCell.h"
@interface ChatTopListViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
ChatTopListCellDelegate
>
{
    BOOL _kHasFix;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *titileLabel;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contstraintHeight;
@property (weak, nonatomic) IBOutlet UIView *titileView;

@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@end

@implementation ChatTopListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImageView *bg = [[UIImageView alloc] initWithImage:[UIImage skinGradientHorNaviImage]];
    [self.titileView insertSubview:bg atIndex:0];
    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(bg.superview);
    }];
    self.titileLabel.textColor = [UIColor skinTextTitileColor];
    UIImage *img = [UIImage imageNamed:@ "chat_close_icon" ];
    img = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.closeBtn setImage:img forState:UIControlStateNormal];
    [self.closeBtn setTintColor:[UIColor skinNaviTintColor]];
    self.contstraintHeight.constant = HEIGHT - 250;
    self.tableView.isShowEmpty = YES;
    self.tableView.titleForEmpty = @"暂无排行榜数据";
    [self.tableView registerNib:[UINib nibWithNibName:@"ChatTopListCell" bundle:nil] forCellReuseIdentifier:@"ChatTopListCell"];
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)closeAction:(UIButton *)sender {
    (self.handle && _kHasFix) ? self.handle() : nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITableViewDelegate

#pragma mark -  UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.list.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ChatTopListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatTopListCell"];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"cell" owner:self options:nil].firstObject;
    }
    ChatTopListModel *model = [self.list safeObjectAtIndex:indexPath.row];
    NSManagedObjectContext *contex = [NSManagedObjectContext MR_defaultContext];
    ChatFollowEntity *entity = [self.data getFollowEntityByFollowId:model.userId inContext:contex];
    cell.model = model;
    cell.entity = entity;
    cell.rankIdx = indexPath.row;
    cell.delegate = self;
    
    return cell;
}

-(BOOL)ChatTopListCell:(ChatTopListCell *)cell TapFollowAt:(ChatTopListModel *)model{
    NSManagedObjectContext *content = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_defaultContext]];;
    BOOL isFollow = [self.data setFollowEntityByFollowId:model.userId inContext:content];
    _kHasFix = YES;
    return isFollow;
}




-(void)setList:(NSArray<ChatTopListModel *> *)list{
    _list = list;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
