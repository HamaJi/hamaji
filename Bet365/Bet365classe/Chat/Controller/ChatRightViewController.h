//
//  ChatRightViewController.h
//  Bet365
//
//  Created by HHH on 2018/10/9.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatRightItem.h"

typedef void(^ChatRightPushBlock)(ChatRightItem *model);

@interface ChatRightViewController : Bet365ViewController

@property (nonatomic,strong)NSArray <ChatRightItem *>*rithgItems;

@property (nonatomic,copy)ChatRightPushBlock handle;

@end
