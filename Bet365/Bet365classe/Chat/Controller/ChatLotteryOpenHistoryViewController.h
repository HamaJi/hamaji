//
//  ChatLotteryOpenHistoryViewController.h
//  Bet365
//
//  Created by HHH on 2018/10/1.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatLotteryOpenHistoryViewController : Bet365ViewController

@property (strong, nonatomic) NSArray <ChatLotteryTopResultModel *> *list;

@end
