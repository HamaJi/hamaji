//
//  ChatLotteryOpenHistoryViewController.m
//  Bet365
//
//  Created by HHH on 2018/10/1.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatLotteryOpenHistoryViewController.h"
#import "KFCPpreizeK3TableViewCell.h"
#import "KFCPPCDDTableViewCell.h"
#import "KFCPOpenPrizeSixLotteryTableViewCell.h"
#import "KFCPHistoryHomeTableViewCell.h"
#import "ChatLotteryTopResultCell.h"
#import "Bet365LotteryResultCell.h"

@interface ChatLotteryOpenHistoryViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *titileView;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet UILabel *titileLabel;

@property (weak, nonatomic) IBOutlet UIButton *closeBtn;


@end

static NSString *const DesregisterHistoryCell = @"DesOpenPrizeHistoryCell";
static NSString *const DesregisterK3History = @"DesKSHistory";
static NSString *const DesregisterHistoryPCDDCell = @"DesHistoryPCDDCell";
static NSString *const DesregisterSixLotteryCell = @"DeshistorySixLotteryCell";

@implementation ChatLotteryOpenHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImageView *bg = [[UIImageView alloc] initWithImage:[UIImage skinGradientHorNaviImage]];
    [self.titileView insertSubview:bg atIndex:0];
    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(bg.superview);
    }];
    self.titileLabel.textColor = [UIColor skinTextTitileColor];
    
    UIImage *img = [UIImage imageNamed:@ "chat_close_icon" ];
    img = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.closeBtn setImage:img forState:UIControlStateNormal];
    [self.closeBtn setTintColor:[UIColor skinNaviTintColor]];
    
    self.titileLabel.text = @"开奖记录";
    [self.tableView registerNib:[UINib nibWithNibName:@"Bet365LotteryResultCell" bundle:nil] forCellReuseIdentifier:@"Bet365LotteryResultCell"];
    self.tableView.isShowEmpty = NO;
    self.tableView.titleForEmpty = @"无记录";
    self.tableView.descriptionForEmpty = @"该彩票暂无开奖记录";
    _tableView.estimatedRowHeight = 60;
    
    _tableView.rowHeight = UITableViewAutomaticDimension;
    
    
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.list.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Bet365LotteryResultCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Bet365LotteryResultCell"];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"Bet365LotteryResultCell" owner:self options:nil].firstObject;
    }
    ChatLotteryTopResultModel *model = self.list[indexPath.row];
    __block KFCPHomeGameJsonModel *homeModel;
    [[LOTTERY_FACTORY.lotteryClassifyList safeObjectAtIndex:1] enumerateObjectsUsingBlock:^(KFCPHomeGameJsonModel  *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.GameId integerValue] ==model.gameId) {
            homeModel = obj;
            *stop = YES;
        }
    }];
    [cell setHomeGameJsonModel:homeModel ChatLotteryTopResultModel:model];
    return cell;
}
#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - Events
- (IBAction)closeAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
