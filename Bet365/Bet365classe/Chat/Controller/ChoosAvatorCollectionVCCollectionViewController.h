//
//  ChoosAvatorCollectionVCCollectionViewController.h
//  Bet365
//
//  Created by HHH on 2018/10/7.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ChatChooseAvatorBlock)(NSString *imgStr);
@interface ChoosAvatorCollectionVCCollectionViewController : UICollectionViewController
-(instancetype)initWithList:(NSArray <NSString *>*)list PageIdx:(NSUInteger)idx;
@property (nonatomic,copy)ChatChooseAvatorBlock handle;
@end
