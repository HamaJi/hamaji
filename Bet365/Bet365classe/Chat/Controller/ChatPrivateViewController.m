//
//  ChatPrivateViewController.m
//  Bet365
//
//  Created by adnin on 2019/2/10.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatPrivateViewController.h"
#import "ChatRightViewController.h"

#pragma mark - Command
#import "ChatPrivateAction.h"

#pragma mark - View
#import "ChatInputToolBar.h"

@interface ChatPrivateViewController ()
<ChatActionDelegate>

@end

@implementation ChatPrivateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupRightMenuButton];
    
    // Do any additional setup after loading the view from its nib.
}

//-(void)viewDidAppear:(BOOL)animated{
//    [super viewDidAppear:animated];
//    [CHAT_UTIL updateAllUnreadMessageCount:self.action.data.unreadCount FromePrivateSubscribeData:(ChatUtilityPrivateSubscribeData *)self.action.data];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - ChatActionDelegate
-(void)ChatAction:(ChatPrivateAction *)action UserIsOnline:(BOOL)isOnline{
    self.title = [NSString stringWithFormat:@"%@[%@]",self.userModel.nickName,(isOnline ? @"在线":@"离线")];
}

#pragma mark - Events


#pragma mark - GET/SET
-(ChatAction *)getAction{
    if (!_action) {
        
        ChatPrivateAction *action = [ChatPrivateAction creatActionByChannelId:self.userModel.userId];
        action.userModel = self.userModel;
        dispatch_async(dispatch_get_main_queue(), ^{
            action.tableView = self.tableView;
            action.inputToolBar = self.inputToolBar;
            action.bottomBtn = self.bottomBtn;
        });
        _action = action;
    }
    return _action;
}

-(UITableView *)getTableView{
    if (!_tableView) {
        _tableView = [super getTableView];
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.equalTo(self.view);
        }];
        
        _tableView.titleForEmpty = @"无法搭建聊天关系";
        _tableView.descriptionForEmpty = @"请联系管理员";
        
        _tableView.imageNameForEmpty = @"chat_unjoint";
    }
    return _tableView;
}

-(ChatInputToolBar *)getInputToolBar{
    if (!_inputToolBar) {
        _inputToolBar = [super getInputToolBar];
        [self.view addSubview:_inputToolBar];
        [_inputToolBar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            make.top.equalTo(self.tableView.mas_bottom);
        }];
        _inputToolBar.barHeight = 50;
    }
    return _inputToolBar;
}

-(ChatScrollBottomButton *)getBottomBtn{
    if (!_bottomBtn) {
        _bottomBtn = [super getBottomBtn];
        [self.view addSubview:_bottomBtn];
        [_bottomBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.view).offset(-15);
            make.bottom.equalTo(self.inputToolBar.mas_top).offset(-10);
            make.size.mas_equalTo(CGSizeMake(30, 30));
        }];
    }
    return _bottomBtn;
}

-(void)setUserModel:(ChatUserInfoModel *)userModel{
    if ([userModel isEqual:_userModel]) {
        return;
    }
    if (userModel.userId == _userModel.userId) {
        return;
    }
    _userModel = userModel;
    if (!_userModel) {
        return;
    }
    self.title = [NSString stringWithFormat:@"%@[%@]",_userModel.nickName,(_userModel.status ? @"在线":@"离线")];
    self.action.delegate = self;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
