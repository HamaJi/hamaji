//
//  ChatMessagePopViewController.h
//  Bet365
//
//  Created by HHH on 2018/10/2.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSUInteger, ChatMessagePopViewTapStatus) {
    /** 发送消息*/
    ChatMessagePopViewTapStatus_PRIVATECHAT,
    /** 用户信息*/
    ChatMessagePopViewTapStatus_USER,
    /** 禁言*/
    ChatMessagePopViewTapStatus_UNMESSAGE,
    /** 禁图*/
    ChatMessagePopViewTapStatus_UNIMG,
    /** 踢人*/
    ChatMessagePopViewTapStatus_KIKE,
    /** 删除*/
    ChatMessagePopViewTapStatus_DELETE,
    /** 拷贝文本*/
    ChatMessagePopViewTapStatus_COPYTEXT,
    /** 保存图片*/
    ChatMessagePopViewTapStatus_SAVEIMG,
    /** 彩票*/
    ChatMessagePopViewTapStatus_LOTTERY,
    /** 重发*/
    ChatMessagePopViewTapStatus_SENDAGAINT,
    /** 撤回*/
    ChatMessagePopViewTapStatus_REVOKE,
    /** 关注*/
    ChatMessagePopViewTapStatus_FOLLOW,
    
};

/** 选中 */
typedef void(^ChatMessagePopViewBlock)(ChatMessagePopViewTapStatus status);
/** 关注**/
typedef void(^ChatMessagePopViewFollowBlock)(BOOL isFollow);

@interface ChatMessagePopViewController : Bet365ViewController
@property (nonatomic,weak) Message *message;
@property (nonatomic,weak) ChatRuleModel *ruleModel;
@property (nonatomic,copy) ChatMessagePopViewBlock authorHandle;
@property (nonatomic,copy) ChatMessagePopViewBlock sendHandle;
@property (nonatomic,copy) ChatMessagePopViewFollowBlock followHandle;

@property (nonatomic,weak) ChatUtilityGroupSubscribeData *data;
@end
