//
//  NSMutableArray+Chat.h
//  Bet365
//
//  Created by HamaJi on 2018/11/1.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Chat)


/**
 新增消息

 @param message 消息
 @return recurCount
 */
-(NSInteger)addMessage:(Message *)message;


/**
 新增消息集合

 @param otherArray 消息集合
 @return recurCount == 0 无重复内容，添加成功；recurCount > 0 有重复内容，添加成功；recurCount < 0 添加失败；
 */
-(NSInteger)addMessagesFromArray:(NSArray <Message *>*)otherArray;


/**
 展示消息的过滤

 @param totoalMessageList 所有消息
 @param roomId 房间id
 @param context 句柄
 @return <#return value description#>
 */
+(NSMutableArray *)filterMessageListBy:(NSArray <Message *>*)totoalMessageList RoomId:(NSInteger)roomId inContext:(NSManagedObjectContext *)context;

/**
 替换

 @param message 替换实体
 @param sendModel 参照
 @return 位置
 */
-(NSRange)replaceMessage:(Message *)message BySendModel:(ChatSendModel *)sendModel;


/**
 删除消息

 @param message 消息
 @return 元素原先位置
 */
-(NSRange)removeMessage:(Message *)message;


/**
 删除消息

 @param identity 消息id
 @return 元素原先位置
 */
-(NSRange)removeMessageByIdentity:(NSUInteger)identity;


/**
 删除发送占位消息

 @param sendModel  发送实体
 @return 是否删除
 */
-(BOOL)removeSendMessageByModel:(ChatSendModel *)sendModel;


/**
 删除发送占位消息

 @param chatSendId 发送id
 @return 是否删除
 */
-(BOOL)removeSendMessageById:(NSString *)chatSendId;


-(void)removeUnFollowMessageByRoomId:(NSInteger)roomID;
@end


