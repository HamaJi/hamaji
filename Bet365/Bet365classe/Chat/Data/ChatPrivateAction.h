//
//  ChatPrivateAction.h
//  Bet365
//
//  Created by HHH on 2019/2/1.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatAction.h"

@interface ChatPrivateAction : ChatAction

@property (nonatomic,strong)ChatUserInfoModel *userModel;

@end
