//
//  NSMutableArray+Chat.m
//  Bet365
//
//  Created by HamaJi on 2018/11/1.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NSMutableArray+Chat.h"

@implementation NSMutableArray (Chat)
-(NSInteger)addMessage:(Message *)message{
    return [self addMessagesFromArray:@[message]];
}
-(NSInteger)addMessagesFromArray:(NSArray <Message *>*)otherArray{

    __block NSMutableArray *mList = [[NSMutableArray alloc] initWithArray:otherArray];
    __block NSMutableArray *removeList = @[].mutableCopy;
    __block NSInteger recurCount = 0;
    
    [self enumerateObjectsUsingBlock:^(Message * _Nonnull dataMessage, NSUInteger dataIdx, BOOL * _Nonnull dataStop) {
        [otherArray enumerateObjectsUsingBlock:^(Message * _Nonnull message, NSUInteger idx, BOOL * _Nonnull stop) {
            if (message.chatType == ChatMessageType_KICK ||
                (message.chatType == ChatMessageType_REMOVE && message.chatFrom != ChatMessageFrom_ME) ||
                message.chatType == ChatMessageType_UNSPEACK ||
                message.chatType == ChatMessageType_UNIMAGE ||
                message.chatType == ChatMessageType_WIN ||
                message.chatType == ChatMessageType_RULE ||
                (message.chatType == ChatMessageType_JOIN && message.chatFrom == ChatMessageFrom_ME) ||
                message.chatType == ChatMessageType_REDCOLLOAR_ERROR) {
                [mList removeObject:message];
            }
            if ([message.identity integerValue] &&
                [dataMessage.identity integerValue] &&
                [dataMessage.identity integerValue] == [message.identity integerValue]) {
                recurCount += 1;
                [removeList addObject:dataMessage];
            }else if(message.content.sendId.length &&
                     dataMessage.content.sendId.length &&
                     [message.content.sendId isEqualToString:dataMessage.content.sendId] &&
                     message.chatType == dataMessage.chatType){
                [removeList addObject:dataMessage];
            }
//            else if (BET_CONFIG.userSettingData.chatFollowStatus == ChatFollowStatus_ONLY && )
        }];
    }];
    
    
    
    if (!mList.count) {
        return 0 - otherArray.count;
    }
    
    [removeList enumerateObjectsUsingBlock:^(Message *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [self removeObject:obj];
    }];
    [self addObjectsFromArray:mList];
    
    [self sortUsingComparator:^NSComparisonResult(Message  *_Nonnull message1, Message * _Nonnull message2) {
        
        NSInteger numerb1 = [message1.curTime timeIntervalSince1970];
        NSInteger number2 = [message2.curTime timeIntervalSince1970];

        if ([message1.identity integerValue] && [message2.identity integerValue]) {
            numerb1 = [message1.identity integerValue];
            number2 = [message2.identity integerValue];
        }
        return numerb1 > number2; // 升序
    }];
    
    if (mList.count == 1) {
        [self enumerateObjectsUsingBlock:^(Message  *_Nonnull message, NSUInteger idx, BOOL * _Nonnull stop) {
            Message *add = mList.firstObject;
            if ([message.content.sendId isEqualToString:add.content.sendId]) {
                
            }
        }];
    }
    
    
    return recurCount;
}

+(NSMutableArray *)filterMessageListBy:(NSArray <Message *>*)totoalMessageList RoomId:(NSInteger)roomId inContext:(NSManagedObjectContext *)context{
    NSMutableArray *filterMessageList = @[].mutableCopy;
    NSArray *entityList = [CHAT_UTIL getFollowEntityListByRoomId:roomId inContext:context];
    
    if (BET_CONFIG.userSettingData.chatFollowStatus == ChatFollowStatus_ONLY) {
        [totoalMessageList enumerateObjectsUsingBlock:^(Message * _Nonnull message, NSUInteger messageIdx, BOOL * _Nonnull messageStop) {
            if (message.chatFrom == ChatMessageFrom_ME || message.chatFrom == ChatMessageFrom_SYS) {
                /** 自己或系统的消息**/
                [filterMessageList addObject:message];
            }else if ([message.level integerValue] >= 100){
                /** 会员等级>=100的消息**/
                [filterMessageList addObject:message];
            }else if(message.content.isTopping){
                /** 置顶消息 **/
                [filterMessageList addObject:message];
            }else{
                /** 他人消息,切在关注列表**/
                [entityList enumerateObjectsUsingBlock:^(ChatFollowEntity * _Nonnull entity, NSUInteger entityIdx, BOOL * _Nonnull entityStop) {
                    if (entity.followUserID == [message.userId integerValue] &&
                        message.chatFrom == ChatMessageFrom_OTHER) {
                        [filterMessageList addObject:message];
                        *entityStop = YES;
                    }
                    
                }];
            }
        }];
    }else{
        [filterMessageList addObjectsFromArray:totoalMessageList];
    }
    return filterMessageList;
}


-(NSRange)replaceMessage:(Message *)message BySendModel:(ChatSendModel *)sendModel{
    __block NSRange range;
    
    [self enumerateObjectsUsingBlock:^(Message * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.content.sendId isEqualToString:[sendModel chatSendId]] &&
            obj.chatType != ChatMessageType_SYSTEM) {
            message.content.pImageData = obj.content.pImageData;
            [self replaceObjectAtIndex:idx withObject:message];
            range = NSMakeRange(idx, 1);
            *stop = YES;
        }
    }];
    return range;
}

-(NSRange)removeMessage:(Message *)message{
    __block NSRange range = NSMakeRange(0, 0);
    if ([self containsObject:message]) {
        range = NSMakeRange([self indexOfObject:message], 1);
        [self removeObject:message];
    }else if (message.content.value && [message.content.value integerValue]){
        range =  [self removeMessageByIdentity:[message.content.value integerValue]];
    }else if (message.identity) {
        [self enumerateObjectsUsingBlock:^(Message *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.identity integerValue] == [message.identity integerValue]) {
                [self removeObjectAtIndex:idx];
                range = NSMakeRange(idx, 1);
                *stop = YES;
            }
        }];
    }else if(message.content.sendId.length){
        [self enumerateObjectsUsingBlock:^(Message *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.content.sendId isEqualToString:message.content.sendId]) {
                [self removeObjectAtIndex:idx];
                range = NSMakeRange(idx, 1);
                *stop = YES;
            }
        }];
    }
    return range;
}

-(NSRange)removeMessageByIdentity:(NSUInteger)identity{
    __block NSRange range = NSMakeRange(0, 0);
    if (identity) {
        [self enumerateObjectsUsingBlock:^(Message *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.identity integerValue] == identity) {
                [self removeObjectAtIndex:idx];
                range = NSMakeRange(idx, 1);
                *stop = YES;
            }
        }];
    }
    return range;
}

-(BOOL)removeSendMessageByModel:(ChatSendModel *)sendModel{
    __block BOOL success = NO;
    [self enumerateObjectsUsingBlock:^(ChatSendModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.chatSendId isEqualToString:sendModel.chatSendId]) {
            [self removeObjectAtIndex:idx];
            success = YES;
            *stop = YES;
        }
    }];
    return success;
}

-(BOOL)removeSendMessageById:(NSString *)chatSendId{
    __block BOOL success = NO;
    [self enumerateObjectsUsingBlock:^(ChatSendModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.chatSendId isEqualToString:chatSendId]) {
            [self removeObjectAtIndex:idx];
            success = YES;
            *stop = YES;
        }
    }];
    return success;
}

-(void)removeUnFollowMessageByRoomId:(NSInteger)roomID{
    NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
    NSArray <ChatFollowEntity *>*list = [CHAT_UTIL getFollowEntityListByRoomId:roomID inContext:context];
    NSMutableArray <Message *>*messages = [[NSMutableArray alloc] initWithArray:self];
    [messages enumerateObjectsUsingBlock:^(Message * _Nonnull message, NSUInteger idx, BOOL * _Nonnull stop) {
        [list enumerateObjectsUsingBlock:^(ChatFollowEntity * _Nonnull entity, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([message.userId integerValue] == entity.userID) {
                [self removeMessage:message];
                *stop = YES;
            }
        }];
    }];
}

@end
