//
//  InputToolBarCustomItem.m
//  Bet365
//
//  Created by HHH on 2018/9/17.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "InputToolBarCustomItem.h"

@implementation InputToolBarCustomItem
+(instancetype)creatItemTitile:(NSString *)titile ImgName:(NSString *)imgName Sel:(SEL)sel{
    InputToolBarCustomItem *item = [[InputToolBarCustomItem alloc] init];
    item.titile = titile;
    item.img = [UIImage imageNamed:imgName];
    item.selector = sel;
    return item;
}
@end
