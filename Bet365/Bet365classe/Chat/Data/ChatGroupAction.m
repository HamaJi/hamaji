//
//  ChatGroupAction.m
//  Bet365
//
//  Created by HHH on 2018/9/16.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatGroupAction.h"
#import "ChatMessageCell.h"
#import "ChatTopListViewController.h"
#import "ChatChooseRoomViewController.h"
#import "ChatLotteryOpenHistoryViewController.h"
#import "ChatAction+Extension.h"
#import "ChatPrivateViewController.h"
#import "ChatPrivateNewMessageViewController.h"
#import "ChatRoomLotteryPinEntity+CoreDataProperties.h"
@interface ChatGroupAction()
<
iCarouselDataSource,
iCarouselDelegate,
ChatLotteryTopViewDelegate,
ChatPrivateNewMessageViewDelegate,
ChatPrivateNewMessageListDelegate
>

@property (nonatomic,strong) ChatPrivateNewMessageViewController *priavetNewMsgVc;

@end

@implementation ChatGroupAction

-(instancetype)init{
    if (self = [super init]) {
        /** 置顶消息是否不在列表展示**/
        dispatch_async(dispatch_get_main_queue(), ^{
            ChatUtilityGroupSubscribeData *data = (ChatUtilityGroupSubscribeData *)self.data;

            
            
            @weakify(self);
            self.inputToolBar.editale = YES;
            self.inputToolBar.isSendImg = YES;
            
            /** 请求开奖信息 **/
            [CHAT_UTIL getAllOpenInfoAtRoom:self.channelID Completed:^(NSMutableArray<LotteryOpenInfoModel *> *modelList) {
                @strongify(self)
                if (self.delegate && [self.delegate respondsToSelector:@selector(ChatAction:UpdataOpenList:)]) {
                    if (BET_CONFIG.userSettingData.hideChatLotteryTopView) {
                        [self.delegate ChatAction:self UpdataOpenList:nil];
                    }else{
                        [self.delegate ChatAction:self UpdataOpenList:modelList];
                    }
                    
                }
                if (!self.openLotteryList && self.filterMessageList.count) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self chatReloadTableView];
                        [self scrollBottomAnimated:YES];
                    });
                }
                self.openLotteryList = modelList;
                
                [self.carousel reloadData];
                NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
                NSString *predicateStr = [NSString stringWithFormat:@"(roomid = %li)",self.channelID];
                ChatRoomLotteryPinEntity *entity = [ChatRoomLotteryPinEntity MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:predicateStr] inContext:context];
                if (entity) {
                    [self.openLotteryList enumerateObjectsUsingBlock:^(LotteryOpenInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        if ([obj.lotteryID isEqualToString:entity.lotteryid]) {
                            *stop = YES;
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self.carousel scrollToItemAtIndex:idx animated:NO];
                            });
                            
                        }
                    }];
                }
            }];
            
            
            [[RACObserve(BET_CONFIG.userSettingData, hideChatBottomBar) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
                @strongify(self);
                if (self.delegate && [self.delegate respondsToSelector:@selector(ChatAction:SetHideTabBar:)]) {
                    [self.delegate ChatAction:self SetHideTabBar:[x boolValue]];
                }
            }];
            
            [[RACObserve(BET_CONFIG.userSettingData, hideChatLotteryTopView) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
                @strongify(self);
                BOOL isHide = [x boolValue];
                if ([self.delegate respondsToSelector:@selector(ChatAction:UpdataOpenList:)]) {
                    [self.delegate ChatAction:self UpdataOpenList:isHide ? nil : self.openLotteryList];
                }
                [self.carousel reloadData];
            }];
            
            [[RACObserve(self, filterMessageList) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
                @strongify(self);
                /** 更新置顶消息，取最后一条**/
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"content.isTopping == %i", YES];
                self.stickMessage = [[self.filterMessageList filteredArrayUsingPredicate:predicate] lastObject];
            }];
            
            [[RACObserve(self, stickMessage) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
                @strongify(self);
                if (self.delegate && [self.delegate respondsToSelector:@selector(ChatAction:StickMessage:)]) {
                    [self.delegate ChatAction:self StickMessage:x];
                }
            }];
        });
    }
    return self;
}

- (void)ChatUtilityJoinGroupScribeData:(ChatUtilityGroupSubscribeData *)data{
    [SVProgressHUD dismiss];
    if (data != self.data) {
        return;
    }
    if (data.roomInfoModel && data.roomInfoModel && CHAT_UTIL.connectStatus != ChatUtilityConnectStatus_CONNECTED_JOINFAIL){
        self.tableView.isShowEmpty = NO;
        if (self.delegate && [self.delegate respondsToSelector:@selector(ChatAction:JoinRoomSuccese:)]) {
            [self.delegate ChatAction:self JoinRoomSuccese:(ChatUtilityGroupSubscribeData *)self.data];
        }
    }
    self.tableView.isShowEmpty = NO;
    self.inputToolBar.editale = ([data.ruleModel.isSpeak boolValue]);
    self.inputToolBar.isSendImg = ([data.ruleModel.isSendImg boolValue]);
    [self chatReloadTableView];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self scrollBottomAnimated:NO];
    });
    
}

- (void)ChatUtilityJoinFaildGroupScribeData:(ChatUtilityGroupSubscribeData *)data FaildMsg:(NSString *)faildMsg{
    if (data != self.data) {
        return;
    }
    self.tableView.mj_header = nil;
    self.inputToolBar.editale = NO;
    self.filterMessageList = nil;
    self.tableView.isShowEmpty = YES;
    self.tableView.titleForEmpty = faildMsg;
    [self cancleAllRequestTask];
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(ChatAction:JoinRoomFail:)]) {
            [self.delegate ChatAction:self JoinRoomFail:self.channelID];
        }
        if ([self.delegate respondsToSelector:@selector(ChatAction:UpdataOpenList:)]) {
            [self.delegate ChatAction:self UpdataOpenList:nil];
        }
    }
}

- (void)ChatUtilityUpdateAllUnreadMessage:(NSArray <ChatUserInfoModel *>*)list{
    NSMutableDictionary *dic = @{}.mutableCopy;
    [list enumerateObjectsUsingBlock:^(ChatUserInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [dic safeSetObject:obj forKey:[NSString stringWithFormat:@"%li",obj.userId]];
    }];
    
    ChatUtilityGroupSubscribeData *data = (ChatUtilityGroupSubscribeData *)self.data;
    /** 私聊列表添加客服入口,禅道12868 **/
    
    NSMutableArray *customerList = [[data.userList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(customer == %li) AND (userId != %li)",1,CHAT_UTIL.clientInfo.userInfo.userId]] mutableCopy];
    [customerList enumerateObjectsUsingBlock:^(ChatUserInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [dic safeSetObject:obj forKey:[NSString stringWithFormat:@"%li",obj.userId]];
    }];
    
    NSArray *sortList = [[dic allValues] sortedArrayUsingComparator:^NSComparisonResult(ChatUserInfoModel*  _Nonnull obj1, ChatUserInfoModel*  _Nonnull obj2) {
        if ([obj1.lastNewDate timeIntervalSince1970] > [obj2.lastNewDate timeIntervalSince1970]) {
            return NSOrderedAscending;
        }
        return NSOrderedDescending;
    }];
    self.unreadMessageListView.userList = sortList;
    self.priavetNewMsgVc.userList = sortList;
}

- (void)ChatUtilityScribeData:(ChatUtilityGroupSubscribeData *)data JumpPrivateChatByUserInfo:(ChatUserInfoModel *)userInfo{
    if ([data isEqual:self.data]) {
        ChatPrivateViewController *privateVc = [[ChatPrivateViewController alloc] initWithNibName:@"ChatPrivateViewController" bundle:nil];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:privateVc animated:YES];
        privateVc.roomID = data.identifier;
        privateVc.userModel = userInfo;
        privateVc.hidesBottomBarWhenPushed = YES;
    }
}

-(void)ChatUtilityScribeData:(ChatUtilityGroupSubscribeData *)data UpdateUserInfo:(ChatUserInfoModel *)userInfo{
    if ([data isEqual:self.data]) {
        [data updateUserInfoModel:userInfo];
        [self chatReloadTableView];
    }
}

- (void)ChatUilityScribeData:(ChatUtilitySubscribeData *)data JoinRoomMessage:(Message *)message{
    if ([data isEqual:self.data]) {
        if ([message.userId integerValue] == CHAT_UTIL.clientInfo.userInfo.userId) {
            return;
        }
        ChatUserInfoEntity *entity = [[ChatUtilitySubscribeData getUserInfoListEntityByUserId:[message.userId integerValue]] safeObjectAtIndex:0];
        if (entity) {
            [self.joinRoomBubbleTools addBubbleString:[NSString stringWithFormat:@"%@  加入了房间",entity.nickName]];
        }
    }
}

-(void)updateChatUtilitySubscribeData:(ChatUtilitySubscribeData *)data UpdateUserOnlineCount:(NSInteger)userOnlineCount{
    if ( self.delegate && [self.delegate respondsToSelector:@selector(ChatAction:UpdateUserOnlineCount:)]) {
        [self.delegate ChatAction:(ChatGroupAction *)self UpdateUserOnlineCount:userOnlineCount];
    }
}

-(void)dealloc{
    self.carousel.delegate = nil;
    self.carousel.dataSource = nil;
    
}
-(void)scrolleHadScroll{
    [super scrolleHadScroll];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"content.isTopping == %i", YES];
    Message *toppingMessage = [[self.filterMessageList filteredArrayUsingPredicate:predicate] lastObject];
    if (toppingMessage) {
        __block BOOL isContain = NO;
        [[self.tableView visibleCells] enumerateObjectsUsingBlock:^(ChatCell * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.message isEqual:toppingMessage]) {
                isContain = YES;
                *stop = YES;
            }
        }];
        self.stickMessage = isContain ? nil : toppingMessage;
    }else{
        self.stickMessage = nil;
    }
}



#pragma mark - Privatedou
-(void)jumpToLotteryViewController:(NSString *)lotteryID{
    
    [self resignAllFirstResponder];
    if ([USER_DATA_MANAGER shouldPush2Login]) {
        [UIViewController enterLotteryByLotteryId:lotteryID];
    }
}

#pragma mark - Overralate

-(ChatUtilitySubscribeData *)subSrcibeData{
    return [CHAT_UTIL webSocketSubsribeGroup:self.channelID];
}

-(void)openRedPage:(NSInteger )redPacketId{
    if (!USER_DATA_MANAGER.userInfoData.token) {
        return;
    }
    if (!redPacketId) {
        [SVProgressHUD showErrorWithStatus:@"红包数据异常"];
        return;
    }
    ChatSendMessageGroupModel *model = [ChatSendMessageGroupModel creatSendModel:kChatSendCollarRedPage Date:[NSDate date]];
    model.roomId = self.channelID;
    model.content = [NSString stringWithFormat:@"%li",redPacketId];
    [CHAT_UTIL webSocketSendMessageByChatSendModel:model Subscribe:self.data];
}

-(void)sendTextMessage:(NSString *)text{
    
    if (!USER_DATA_MANAGER.userInfoData.token) {
        return;
    }
    ChatSendMessageGroupModel *model = [ChatSendMessageGroupModel creatSendModel:kChatSendMessageTextGroup Date:[NSDate date]];
    model.roomId = self.channelID;
    
//    NSString *jsonText = [@{@"type":@"text",
//                            @"content":text,
//                            @"messageId":model.chatSendId
//                            } mj_JSONString];
//    model.content = jsonText;
    model.content = text;
    [CHAT_UTIL webSocketSendMessageByChatSendModel:model Subscribe:self.data];
}



-(void)getHistoryMessageListByBehindMessage:(Message *)message{
    
    ChatSendRequestGroupModel *model = [ChatSendRequestGroupModel creatSendModel:kChatSendRequestHistoryGroup Date:[NSDate date]];
    model.roomId = self.channelID;
    model.messageId = [message.identity integerValue];
    [CHAT_UTIL webSocketSendMessageByChatSendModel:model Subscribe:self.data];
}

-(void)tapRemoveMessage:(Message *)message{
    [CHAT_UTIL webSocketRemoveMessage:message Data:self.data];
}

-(void)reloadTableViewByFileterMessageList{
    /** 更新置顶消息，取最后一条**/
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"content.isTopping == %i", YES];
    self.stickMessage = [[self.filterMessageList filteredArrayUsingPredicate:predicate] lastObject];
}
-(void)ChatMessageCellPrivateChat:(ChatMessageCell *)cell{
    ChatSendRequestGroupModel *requestModel = [ChatSendRequestGroupModel creatSendModel:kChatSendRequestUserInfoPrivate Date:[NSDate date]];
    requestModel.userId = [cell.message.userId integerValue];
    requestModel.roomId = self.channelID;
    [CHAT_UTIL webSocketSendMessageByChatSendModel:requestModel Subscribe:self.data];
}
#pragma mark - iCarouselDataSource
- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    if (BET_CONFIG.userSettingData.hideChatLotteryTopView) {
        return 0;
    }
    return self.openLotteryList.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view{
    
    if (!view) {
        view = [[[NSBundle mainBundle] loadNibNamed:@"ChatLotteryTopView" owner:self options:nil] firstObject];
        view.ycz_width = WIDTH - 30;
        if (IS_IPHONE_5s) {
            view.ycz_height = 94;
        }else{
            view.ycz_height = 104;
        }
        view.ycz_y = 5;
        ((ChatLotteryTopView *)view).delegate = self;
    }
    
    LotteryOpenInfoModel *openModel = self.openLotteryList[index];
    ((ChatLotteryTopView *)view).lotteryId = openModel.lotteryID;
    LotteryOpenInfoModel * model = [self.openLotteryList safeObjectAtIndex:index];
    ((ChatLotteryTopView *)view).model = model;
    
    return view;
}

- (CATransform3D)carousel:(iCarousel *)carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform{
    
    transform = CATransform3DRotate(transform, M_PI / 8.0f, 0.0f, 1.0f, 0.0f);
    return CATransform3DTranslate(transform, 0.0f, 0.0f, offset * carousel.itemWidth);
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value{
    switch (option){
        case iCarouselOptionWrap:{
            return NO;
        }
        case iCarouselOptionSpacing:{
            return value * 1.05;
        }
        case iCarouselOptionFadeMax:{
            if (carousel.type == iCarouselTypeCustom){
                
                return 0.0f;
            }
            return value;
        }
        default:{
            return value;
        }
    }
}

-(void)carouselDidEndScrollingAnimation:(iCarousel *)carousel{
    ChatLotteryTopView *view = (ChatLotteryTopView *)carousel.currentItemView;
    
    NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
    NSString *predicateStr = [NSString stringWithFormat:@"(roomid = %li)",self.channelID];
    ChatRoomLotteryPinEntity *entity = [ChatRoomLotteryPinEntity MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:predicateStr] inContext:context];
    if (!entity) {
        entity = [ChatRoomLotteryPinEntity MR_createEntityInContext:context];
    }
    entity.lotteryid = view.lotteryId;
    entity.roomid = @(self.channelID);
    [context MR_saveToPersistentStoreAndWait];
    
}

-(void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel{
    [carousel.visibleItemViews enumerateObjectsUsingBlock:^(ChatLotteryTopView*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.isFirsResponse = NO;
    }];
    NSUInteger index = carousel.currentItemIndex;
    ChatLotteryTopView *view = (ChatLotteryTopView *)carousel.currentItemView;
    view.isFirsResponse = YES;
    [view updateLotteryOpenInfoModel];
    
}

-(void)carouselWillBeginScrollingAnimation:(iCarousel *)carousel{
    [carousel.visibleItemViews enumerateObjectsUsingBlock:^(ChatLotteryTopView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
    }];
}


#pragma mark - ChatLotteryTopViewDelegate
-(void)tapIconImageViewAtChatLotteryTopView:(ChatLotteryTopView *)view{
    [self jumpToLotteryViewController:view.lotteryId];
}

-(void)tapBallContentAtChatLotteryTopView:(ChatLotteryTopView *)view{
    [SVProgressHUD showWithStatus:@"正在请求"];
    [CHAT_UTIL getLotteryOpenHistoryByLotteryID:[view.lotteryId integerValue] PageSize:100 Completed:^(NSArray<ChatLotteryTopResultModel *> *list) {
        if (!list.count) {
            [SVProgressHUD showErrorWithStatus:@"请求失败"];
        }else{
            [SVProgressHUD dismiss];
            ChatLotteryOpenHistoryViewController *vc = [[ChatLotteryOpenHistoryViewController alloc] initWithNibName:@"ChatLotteryOpenHistoryViewController" bundle:nil];
            vc.list = list;
            vc.transitioningDelegate = self;
            vc.modalPresentationStyle = UIModalPresentationCustom;
            [((UIViewController *)self.delegate) presentViewController:vc
                                                              animated:YES
                                                            completion:nil];
        }
    }];
}

-(void)replaceLotteryOpenInfoModel:(LotteryOpenInfoModel *)model{
    dispatch_async(dispatch_get_main_queue(), ^{
        __block NSInteger index = -1;
        [self.openLotteryList enumerateObjectsUsingBlock:^(LotteryOpenInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.lotteryID isEqualToString:model.lotteryID]) {
                index = idx;
                *stop = YES;
            }
        }];
        if (index) {
            [self.openLotteryList replaceObjectAtIndex:index withObject:model];
        }
    });
}

#pragma mark - ChatPrivateNewMessageViewDelegate
-(void)ChatPrivateNewMessageView:(ChatPrivateNewMessageView *)view didSelectUser:(ChatUserInfoModel *)userModel{
    ChatSendRequestGroupModel *requestModel = [ChatSendRequestGroupModel creatSendModel:kChatSendRequestUserInfoPrivate Date:[NSDate date]];
    requestModel.userId = userModel.userId;
    requestModel.roomId = self.channelID;
    [CHAT_UTIL webSocketSendMessageByChatSendModel:requestModel Subscribe:self.data];
}

-(void)ChatPrivateNewMessageView:(ChatPrivateNewMessageView *)view deleteUser:(ChatUserInfoModel *)userModel{
    [CHAT_UTIL removeUnreadMessagePrivateUser:userModel];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        ChatUtilityPrivateSubscribeData *data = [CHAT_UTIL getPrivateSubsribeDataByUserId:userModel.userId];
        if (data) {
            [data removeAllUnredaMessage];
        }
    });
}

-(void)tapMoreChatPrivateNewMessageView:(ChatPrivateNewMessageView *)view{
    self.priavetNewMsgVc = [[ChatPrivateNewMessageViewController alloc] initWithNibName:@"ChatPrivateNewMessageViewController" bundle:nil];
    self.priavetNewMsgVc.delegate = self;
    self.priavetNewMsgVc.transitioningDelegate = self;
    self.priavetNewMsgVc.modalPresentationStyle = UIModalPresentationCustom;
    [((UIViewController *)self.delegate) presentViewController:self.priavetNewMsgVc
                                                      animated:YES
                                                    completion:nil];
    self.priavetNewMsgVc.userList = self.unreadMessageListView.userList;
}

-(void)ChatPrivateNewMessageView:(ChatPrivateNewMessageView *)view UpdateHeight:(CGFloat)height{
    [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, height, 0)];
}

#pragma mark - ChatPrivateNewMessageListDelegate
-(void)chatPrivateNewMessageListRemoveUserInfo:(ChatUserInfoModel *)userInfo{
    [CHAT_UTIL removeUnreadMessagePrivateUser:userInfo];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        ChatUtilityPrivateSubscribeData *data = [CHAT_UTIL getPrivateSubsribeDataByUserId:userInfo.userId];
        if (data) {
            [data removeAllUnredaMessage];
        }
    });
}

-(void)chatPrivateNewMessageListController:(ChatPrivateNewMessageViewController *)vc didSelectUser:(ChatUserInfoModel *)userModel{
    ChatSendRequestGroupModel *requestModel = [ChatSendRequestGroupModel creatSendModel:kChatSendRequestUserInfoPrivate Date:[NSDate date]];
    requestModel.userId = userModel.userId;
    requestModel.roomId = self.channelID;
    [CHAT_UTIL webSocketSendMessageByChatSendModel:requestModel Subscribe:self.data];
    
    
}

#pragma mark - Events
-(void)topListAction:(id)sender{
    [CHAT_UTIL getTopListByRoomId:self.channelID Completed:^(NSArray<ChatTopListModel *> *list) {
        ChatTopListViewController *vc = [[ChatTopListViewController alloc] initWithNibName:@"ChatTopListViewController" bundle:nil];
        vc.transitioningDelegate = self;
        vc.modalPresentationStyle = UIModalPresentationCustom;
        [((UIViewController *)self.delegate) presentViewController:vc
                                                          animated:YES
                                                        completion:nil];
        vc.data = (ChatUtilityGroupSubscribeData *)self.data;
        vc.list = list;
        @weakify(self);
        vc.handle = ^{
            @strongify(self);
            [self.data manualUpdateFilter];
        };
    }];
}

-(void)followSortAction:(id)sender{
//    [self.joinRoomBubbleTools addBubbleString:@"哈哈哈哈哈哈"];
    UIButton *button = (UIButton *)sender;
    BET_CONFIG.userSettingData.chatFollowStatus =! BET_CONFIG.userSettingData.chatFollowStatus;
    [button setSelected:BET_CONFIG.userSettingData.chatFollowStatus];
    [BET_CONFIG.userSettingData save];
    [self.data manualUpdateFilter];
}

-(void)removeAllAction:(id)sender{
    [Bet365AlertSheet showChooseAlert:@"友情提示"
                              Message:@"是否清空消息"
                                Items:@[@"取消",@"清除"]
                              Handler:^(NSInteger index) {
                                  if (index == 1) {
                                      [CHAT_UTIL removeAllMessageListByRoomId:self.channelID Message:nil];
                                  }
                              }];
}

-(void)moreRoomAction:(id)sender{
    
    [self resignAllFirstResponder];
    ChatChooseRoomViewController *vc = [[ChatChooseRoomViewController alloc] initWithNibName:@"ChatChooseRoomViewController" bundle:nil];
    vc.roomID = self.channelID;
    @weakify(self);
    vc.handle = ^(ChatRoomInfoModel *model) {
        @strongify(self);
        if (self.delegate && [self.delegate respondsToSelector:@selector(ChatAction:ChangeRoomByID:)]) {
            [self.delegate ChatAction:self ChangeRoomByID:[model.identity integerValue]];
        }
    };
    vc.transitioningDelegate = self;
    vc.modalPresentationStyle = UIModalPresentationCustom;
    [((UIViewController *)self.delegate) presentViewController:vc
                                                      animated:YES
                                                    completion:nil];
}

#pragma mark - GET/SET
-(void)setCarousel:(iCarousel *)carousel{
    _carousel = carousel;
    _carousel.delegate = self;
    _carousel.dataSource = self;
    
}

-(void)setUnreadMessageListView:(ChatPrivateNewMessageView *)unreadMessageListView{
    _unreadMessageListView = unreadMessageListView;
    _unreadMessageListView.delegate = self;
}

-(NSMutableArray<LotteryOpenInfoModel *> *)openLotteryList{
    if (!_openLotteryList) {
        _openLotteryList = @[].mutableCopy;
    }
    return _openLotteryList;
}



-(void)setJoinRoomBubbleTools:(ChatJoinRoomBubbleTools *)joinRoomBubbleTools{
    _joinRoomBubbleTools = joinRoomBubbleTools;
}


@end
