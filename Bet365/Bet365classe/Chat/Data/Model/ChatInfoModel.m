//
//  ChatInfoModel.m
//  Bet365
//
//  Created by HHH on 2018/9/22.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatInfoModel.h"
@implementation ChatUserInfoModel
- (void)setNilValueForKey:(NSString *)key{
    if ([key isEqualToString:@"status"]) {
        self.status = 0;
    }else{
        [super setNilValueForKey:key];
    }
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey{

    return @{@"avatar" : @"avatar",
             @"account" : @"account",
             @"nickName" : @"nickName",
             @"userType" : @"userType",
             @"fk" : @"fk",
             @"level" : @"level",
             @"userId" : @"userId",
             @"privateChat" : @"privateChat",
             @"chatUserId":@"chatUserId",
             @"status" : @"status",
             @"privateChatId":@"chatId",
             @"unreadCount":@"unreadCount",
             @"customer":@"customer"
             };
}

+ (NSDictionary *)managedObjectKeysByPropertyKey {
    
    return @{@"avatar" : @"avatar",
             @"account" : @"account",
             @"nickName" : @"nickName",
             @"userType" : @"userType",
             @"fk" : @"fk",
             @"level" : @"level",
             @"userId" : @"userId",
             @"customer" : @"customer"
             };
}

+ (NSString *)managedObjectEntityName {
    return @"ChatUserInfoEntity";
}

+ (NSSet *)propertyKeysForManagedObjectUniquing{
    return [NSSet setWithObject:@"userId"];
}

@end

@implementation ChatRuleModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"maxLength":@"maxLength",
             @"roomId":@"roomId",
             @"userId":@"userId",
             @"isKick":@"isKick",
             @"isGag":@"isGag",
             @"isSendImg":@"isSendImg",
             @"isBanImg":@"isBanImg",
             @"isDelMessage":@"isDelMessage",
             @"isSpeak":@"isSpeak",
             @"isSendPacket":@"isSendPacket",
             @"isViewUserInfo":@"isViewUserInfo",
             @"isPrivateChat":@"isPrivateChat",
             @"isManager":@"isManager",
             @"isShowOnlineUser":@"isShowOnlineUser"};

}

@end

@implementation ChatRoomInfoModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"welcomeMsg":@"welcomeMsg",
             @"identity": @"id",
             @"name": @"name",
             @"isOpen": @"isOpen",
             @"sendPlan": @"sendPlan",
             @"planGameList": @"planGame",
             @"speakBeginTime": @"speakBeginTime",
             @"speakEndTime": @"speakEndTime",
             @"remark": @"remark",
             @"isOpenLeaderboard":@"isOpenLeaderboard",
             @"isShowOnlineUser":@"isShowOnlineUser"
             };

}

+ (NSValueTransformer *)planGameListJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString *jsonStr){
                if (!jsonStr.length) {
                    return nil;
                }
                return [jsonStr componentsSeparatedByString:@","];
            } reverseBlock:^id(NSArray *list) {
                return [list componentsJoinedByString:@","];
            }];
}

@end

@implementation ChatInfoModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"userInfo": @"userInfo",
             @"roomInfoList": @"roomInfoList",
             @"defaultRoomId": @"defaultRoomId",
             @"chatUrl": @"chatUrl",
             @"nodes": @"nodes",
             @"imageHost":@"imageHost",
             @"uploadHost":@"uploadHost"
    };
    
}

+ (NSValueTransformer *)userInfoListJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSDictionary *dic){
                NSError *error;
                ChatUserInfoModel *model = [MTLJSONAdapter modelOfClass:[ChatUserInfoModel class] fromJSONDictionary:dic error:&error];
                return model;
            } reverseBlock:^id(ChatUserInfoModel *d) {
                return d;
            }];
}

//+ (NSValueTransformer *)roleInfoListJSONTransformer{
//    
//    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
//            ^id(NSDictionary *dic){
//                NSError *error;
//                ChatRuleModel *model = [MTLJSONAdapter modelOfClass:[ChatRuleModel class] fromJSONDictionary:dic error:&error];
//                return model;
//            } reverseBlock:^id(ChatRuleModel *d) {
//                return d;
//            }];
//}

+ (NSValueTransformer *)roomInfoListJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSArray *list){
                NSMutableArray *arr = [NSMutableArray array];
                __block NSError *error;
                [list enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull dic, NSUInteger idx, BOOL * _Nonnull stop) {
                    ChatRoomInfoModel *model = [MTLJSONAdapter modelOfClass:[ChatRoomInfoModel class] fromJSONDictionary:dic error:&error];
                    if (!error && model != nil) {
                        [arr addObject:model];
                    }
                }];
                return arr;
            } reverseBlock:^id(NSMutableArray *d) {
                return d;
            }];
}
@end
