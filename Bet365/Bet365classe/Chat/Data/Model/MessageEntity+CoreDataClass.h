//
//  MessageEntity+CoreDataClass.h
//  Bet365
//
//  Created by HHH on 2018/11/7.
//  Copyright © 2018年 jesse. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MessageContentEntity;

NS_ASSUME_NONNULL_BEGIN

@interface MessageEntity : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "MessageEntity+CoreDataProperties.h"
