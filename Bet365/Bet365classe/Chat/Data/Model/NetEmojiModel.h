//
//  NetEmojiModel.h
//  Bet365
//
//  Created by adnin on 2020/4/25.
//  Copyright © 2020 jesse. All rights reserved.
//

#import "MTLModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface NetEmojiModel : MTLModel<MTLJSONSerializing,MTLManagedObjectSerializing>

@property (nonatomic,assign) NSInteger useCount;

@property (nonatomic,strong) NSString * filePath;
@property (nonatomic,strong) NSString * identifier;
@property (nonatomic,strong) NSString * name;

@property (nonatomic,strong) NSNumber * width;
@property (nonatomic,strong) NSNumber * height;

@property (nonatomic,strong) NSDate *time;

@end

NS_ASSUME_NONNULL_END
