//
//  MessageEntity+CoreDataProperties.h
//  Bet365
//
//  Created by HHH on 2018/11/7.
//  Copyright © 2018年 jesse. All rights reserved.
//
//

#import "MessageEntity+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MessageEntity (CoreDataProperties)

+ (NSFetchRequest<MessageEntity *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *avatar;
@property (nullable, nonatomic, copy) NSNumber *chatFrom;
@property (nullable, nonatomic, copy) NSNumber *chatType;
@property (nullable, nonatomic, copy) NSString *curTime;
@property (nullable, nonatomic, copy) NSString *fk;
@property (nullable, nonatomic, copy) NSNumber *hasRead;
@property (nullable, nonatomic, copy) NSNumber *identity;
@property (nullable, nonatomic, copy) NSNumber *level;
@property (nullable, nonatomic, copy) NSString *nickName;
@property (nullable, nonatomic, copy) NSString *roleId;
@property (nullable, nonatomic, copy) NSNumber *sendStatus;
@property (nullable, nonatomic, copy) NSNumber *userId;
@property (nullable, nonatomic, retain) MessageContentEntity *content;

@end

NS_ASSUME_NONNULL_END
