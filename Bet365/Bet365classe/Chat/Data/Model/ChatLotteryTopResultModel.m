//
//  ChatLotteryTopResultModel.m
//  Bet365
//
//  Created by HHH on 2018/10/3.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatLotteryTopResultModel.h"

@implementation ChatLotteryTopResultModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"identity": @"id",
             @"gameId": @"gameId",
             @"status": @"status",
             @"collectStatus": @"collectStatus",
             
             @"turnNum": @"turnNum",
             @"turn": @"turn",
             @"openTime": @"openTime",
             @"remark": @"remark",
             @"statDate": @"statDate",
             @"addTime": @"addTime",
             @"updateTime": @"updateTime",
             @"beginDate": @"beginDate",
             @"endDate": @"endDate",
             
             @"result": @"result",
             @"openNum": @"openNum",
             };
}

+ (NSValueTransformer *)resultJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString *str){
                return [NSDictionary dictionaryWithJsonString:str];
            } reverseBlock:^id(NSDictionary *dic) {
                return [dic mj_JSONString];
            }];
}

+ (NSValueTransformer *)openNumJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString *str){
                return [str componentsSeparatedByString:@","];
            } reverseBlock:^id(NSArray *arr) {
                return [arr componentsJoinedByString:@","];
            }];
}
@end

