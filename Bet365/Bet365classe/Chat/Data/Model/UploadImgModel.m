//
//  UploadImgModel.m
//  Bet365
//
//  Created by HHH on 2018/9/27.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "UploadImgModel.h"

@implementation UploadImgModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"img": @"img",
             @"sm": @"sm",
             @"img_width": @"img_width",
             @"img_height": @"img_height",
             @"sm_width": @"sm_width",
             @"sm_height": @"sm_height"};
}
@end

