//
//  EmojiItem.m
//  Bet365
//
//  Created by HHH on 2018/9/20.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "EmojiItem.h"

@implementation EmojiItem
+(NSMutableArray <EmojiItem *>*)creatItems{
    NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ChatEmoji"ofType:@"plist"]];
    __block NSMutableArray <EmojiItem *>*list = [NSMutableArray array];
    [[dic allKeys] enumerateObjectsUsingBlock:^(NSString *  _Nonnull key, NSUInteger idx, BOOL * _Nonnull stop) {
        EmojiItem *item = [[EmojiItem alloc] init];
        item.identifier = key;
        item.imgName = [dic[key] lowercaseString];
        [list addObject:item];
    }];
    return list;
}

+(NSArray <NSString *>*)getKeyList{
    NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ChatEmoji"ofType:@"plist"]];
    return [dic allKeys];
}

+(EmojiItem *)getItemWithKey:(NSString *)key{
    NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ChatEmoji"ofType:@"plist"]];
    EmojiItem *item = [[EmojiItem alloc] init];
    item.identifier = key;
    item.imgName = [dic[key] lowercaseString];
    return item;
}
@end
