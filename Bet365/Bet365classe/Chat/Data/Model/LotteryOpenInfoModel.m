//
//  LotteryOpenInfoModel.m
//  Bet365
//
//  Created by HHH on 2018/8/31.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "LotteryOpenInfoModel.h"

@implementation LotteryOpenInfoSubModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"turnNum": @"turnNum",
             @"turn": @"turn",
             @"status": @"status",
             @"closeTime": @"closeTime",
             @"openNumList":@"openNum",
             @"openTime":@"openTime"
             };
}

+ (NSValueTransformer *)openTimeJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString *timeStr){
                NSDate *date = [NSDate dateWithString:timeStr Format:@"yyyy-MM-dd HH:mm:ss"];
                return date;
            } reverseBlock:^id(NSArray *d) {
                return d;
            }];
}

+ (NSValueTransformer *)closeTimeJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSNumber *closeTime){
                NSTimeInterval time;
                if ([closeTime integerValue] > 999999999999) {
                    time = [closeTime integerValue] / 1000;
                }else{
                    time = [closeTime integerValue];
                }
                NSDate *date = [NSDate dateWithTimeIntervalSince1970:time];
                return date;
            } reverseBlock:^id(NSDate *date) {
                return date;
            }];
}
+ (NSValueTransformer *)openNumListJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString *openNum){
                return openNum ? [openNum componentsSeparatedByString:@","] : nil;
            } reverseBlock:^id(NSArray *d) {
                return d;
            }];
}

@end

@implementation LotteryOpenInfoModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"cur": @"cur",
             @"pre": @"pre",
             @"lotteryID" : @"lotteryID",
             @"serverTimeDiffence":@"serverTime"
             
             };
}

+ (NSValueTransformer *)serverTimeDiffenceJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSNumber *serverTime){
                
                NSTimeInterval time;
                if ([serverTime integerValue] > 999999999999) {
                    time = [serverTime integerValue] / 1000;
                }else{
                    time = [serverTime integerValue];
                }
                NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
                NSTimeInterval diffenceTime = now - time;
                return @(diffenceTime);
            } reverseBlock:^id(NSNumber *date) {
                return date;
            }];
}

+ (NSValueTransformer *)curJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:LotteryOpenInfoSubModel.class];
}

+ (NSValueTransformer *)preJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:LotteryOpenInfoSubModel.class];
}
@end
