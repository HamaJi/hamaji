//
//  ChatSlideMenuModel.h
//  Bet365
//
//  Created by HHH on 2018/10/17.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "MTLModel.h"

@interface ChatSlideMenuModel : MTLModel<MTLJSONSerializing>
@property (nonatomic,strong)NSString * name;
@property (nonatomic,strong)NSString * type;
@property (nonatomic,strong)NSString * url;
@property (nonatomic,strong)NSString * icon;
@end
