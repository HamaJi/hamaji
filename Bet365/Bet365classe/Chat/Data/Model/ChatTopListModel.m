//
//  ChatTopListModel.m
//  Bet365
//
//  Created by HHH on 2018/12/4.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatTopListModel.h"

@implementation ChatTopListModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"account": @"account",
             @"avatar": @"avatar",
             @"userId": @"userId",
             @"winMoney": @"winMoney",
             @"nickName" :@"nickName"
             };
}

@end
