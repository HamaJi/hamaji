//
//  ChatSuspendDelegateModel.h
//  Bet365
//
//  Created by HHH on 2018/11/13.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "MTLModel.h"

@interface ChatSuspendDelegateModel : MTLModel<MTLJSONSerializing>
@property (nonatomic,strong)NSString *app_float_image;
@property (nonatomic,strong)NSString *app_float_name;
@property (nonatomic,strong)NSString *app_float_link;
@property (nonatomic,strong)NSString *app_gag_title;

@property (nonatomic,strong)NSNumber *isShowHbHistory;
@property (nonatomic,strong)NSNumber *isShowHbMoney;

@property (nonatomic,strong)NSNumber *isShowAttention;

@end

