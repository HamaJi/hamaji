//
//  ChatLotteryTopResultModel.h
//  Bet365
//
//  Created by HHH on 2018/10/3.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "MTLModel.h"

@interface ChatLotteryTopResultModel : MTLModel<MTLJSONSerializing>

@property (nonatomic,assign)NSInteger identity; // id
@property (nonatomic,assign)NSInteger gameId;
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,copy)NSNumber * collectStatus;

@property (nonatomic,copy)NSString * turnNum;
@property (nonatomic,copy)NSString * turn;
@property (nonatomic,copy)NSString * openTime;
@property (nonatomic,copy)NSString * remark;
@property (nonatomic,copy)NSString * statDate;
@property (nonatomic,copy)NSString * addTime;
@property (nonatomic,copy)NSString * updateTime;
@property (nonatomic,copy)NSString * beginDate;
@property (nonatomic,copy)NSString * endDate;

@property (nonatomic,strong)NSDictionary * result;
@property (nonatomic,strong)NSArray <NSString *>*openNum;

@end
