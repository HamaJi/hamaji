//
//  MessageContentEntity+CoreDataProperties.m
//  Bet365
//
//  Created by HHH on 2018/11/7.
//  Copyright © 2018年 jesse. All rights reserved.
//
//

#import "MessageContentEntity+CoreDataProperties.h"

@implementation MessageContentEntity (CoreDataProperties)

+ (NSFetchRequest<MessageContentEntity *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"MessageContentEntity"];
}

@dynamic betContent;
@dynamic betEndTime;
@dynamic betStatTime;
@dynamic gameId;
@dynamic gameName;
@dynamic img;
@dynamic img_height;
@dynamic img_width;
@dynamic isBanImg;
@dynamic isDelMessage;
@dynamic isGag;
@dynamic isKick;
@dynamic orderNo;
@dynamic pImageData;
@dynamic playCode;
@dynamic playName;
@dynamic roomId;
@dynamic sendId;
@dynamic text;
@dynamic thumbnail;
@dynamic thumbnail_height;
@dynamic thumbnail_width;
@dynamic totalMoney;
@dynamic turnNum;
@dynamic userId;
@dynamic value;
@dynamic winMoney;
@dynamic isViewUserInfo;
@dynamic isSendImg;
@dynamic isSendPacket;
@dynamic isSpeak;

@end
