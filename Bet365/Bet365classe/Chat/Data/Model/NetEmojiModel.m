//
//  NetEmojiModel.m
//  Bet365
//
//  Created by adnin on 2020/4/25.
//  Copyright © 2020 jesse. All rights reserved.
//

#import "NetEmojiModel.h"

@implementation NetEmojiModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"filePath": @"filePath",
             @"name": @"name",
             @"useCount": @"useCount",
             @"identifier": @"id",
             @"width":@"width",
             @"height":@"height",
             @"time":@"time"
    };
    
}
+ (NSDictionary *)managedObjectKeysByPropertyKey {
    
    return @{@"filePath": @"filePath",
             @"name": @"name",
             @"useCount": @"useCount",
             @"identifier": @"identifier",
             @"width":@"width",
             @"height":@"height",
             @"time":@"time"
    };
}

+ (NSString *)managedObjectEntityName {
    return @"ChatNetEmojiCacheEntity";
}

@end
