//
//  ChatTopListModel.h
//  Bet365
//
//  Created by HHH on 2018/12/4.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "MTLModel.h"

@interface ChatTopListModel : MTLModel<MTLJSONSerializing>

@property (nonatomic,assign) NSInteger userId;

@property (nonatomic,copy) NSNumber *winMoney;

@property (nonatomic,copy) NSString *avatar;

@property (nonatomic,copy) NSString *account;

@property (nonatomic,copy) NSString *nickName;

@end
