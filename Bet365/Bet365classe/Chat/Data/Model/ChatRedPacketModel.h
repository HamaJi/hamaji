//
//  ChatRedPacketModel.h
//  Bet365
//
//  Created by HHH on 2018/11/23.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "MTLModel.h"

@interface ChatRedPacketDetailModel : MTLModel<MTLJSONSerializing>
@property (nonatomic,copy) NSNumber * identity;
@property (nonatomic,copy) NSNumber* activityId;
@property (nonatomic,copy) NSNumber* ruleId;

@property (nonatomic,copy) NSNumber *money;

@property (nonatomic,copy) NSString *createDatetime;
@property (nonatomic,copy) NSString *status;
@property (nonatomic,copy) NSString *prize;
@property (nonatomic,copy) NSString *userId;
@property (nonatomic,copy) NSString *account;
@property (nonatomic,copy) NSString *settlementCreatetime;
@property (nonatomic,copy) NSString *settlementType;
@property (nonatomic,copy) NSString *activityRecordName;
@property (nonatomic,copy) NSString *nickName;
@property (nonatomic,copy) NSString *avator;

@end
@interface ChatRedPacketactivityModel : MTLModel<MTLJSONSerializing>
@property (nonatomic,assign) NSInteger identity;
@property (nonatomic,assign) NSInteger count;
@property (nonatomic,assign) NSInteger consumeCount;
@property (nonatomic,assign) NSInteger roomId;
@property (nonatomic,strong) NSNumber * activityId;

@property (nonatomic,copy) NSNumber *totalMoney;
@property (nonatomic,copy) NSNumber *consumeMoney;

@property (nonatomic,copy) NSString *redpacketName;
@property (nonatomic,copy) NSString *createDatetime;
@property (nonatomic,copy) NSString *deadTime;
@property (nonatomic,copy) NSString *creater;

@property (nonatomic,copy) NSString *createrName;
@property (nonatomic,assign) NSInteger createrId;
@property (nonatomic,copy) NSString *createrAvator;


@end
@interface ChatRedPacketModel : MTLModel<MTLJSONSerializing>

@property (nonatomic,strong) NSString *reuslt;

@property (nonatomic,strong) NSArray <ChatRedPacketDetailModel *> *details;

@property (nonatomic,strong) ChatRedPacketactivityModel *activityChatRedpacket;

@end

