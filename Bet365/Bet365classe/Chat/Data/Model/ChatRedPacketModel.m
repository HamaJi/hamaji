//
//  ChatRedPacketModel.m
//  Bet365
//
//  Created by HHH on 2018/11/23.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatRedPacketModel.h"

@implementation ChatRedPacketDetailModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"identity": @"id",
             @"activityId": @"activityId",
             @"ruleId": @"ruleId",
             
             @"money":@"money",
             
             @"createDatetime":@"createDatetime",
             @"status":@"status",
             @"prize":@"prize",
             @"userId":@"userId",
             @"settlementCreatetime":@"settlementCreatetime",
             @"settlementType":@"settlementType",
             @"activityRecordName":@"activityRecordName",
             @"nickName":@"remark",
             @"avator":@"remark",
             };
}
+ (NSValueTransformer *)nickNameJSONTransformer{
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString *remark){
                return [[remark componentsSeparatedByString:@","] safeObjectAtIndex:0];
            } reverseBlock:^id(NSString *nickName) {
                return nickName;
            }];
}

+ (NSValueTransformer *)avatorJSONTransformer{
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString *remark){
                return [[remark componentsSeparatedByString:@","] safeObjectAtIndex:1];
            } reverseBlock:^id(NSString *avator) {
                return avator;
            }];
}
@end

@implementation ChatRedPacketactivityModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"identity": @"id",
             @"count": @"count",
             @"consumeCount": @"consumeCount",
             @"consumeCount":@"consumeCount",
             @"roomId":@"roomId",
             @"activityId":@"activityId",
             
             @"totalMoney":@"totalMoney",
             @"consumeMoney":@"consumeMoney",
             
             @"redpacketName":@"redpacketName",
             @"createDatetime":@"createDatetime",
             @"deadTime":@"deadTime",
             @"creater":@"creater",
             };
}

@end

@implementation ChatRedPacketModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"reuslt": @"reuslt",
             @"details": @"details",
             @"activityChatRedpacket": @"activityChatRedpacket"
             };
}

+ (NSValueTransformer *)detailsJSONTransformer{
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSArray <NSDictionary *>*list){
                NSError *error;
                NSArray *arr = [MTLJSONAdapter modelsOfClass:[ChatRedPacketDetailModel class] fromJSONArray:list error:&error];
                return arr;
            } reverseBlock:^id(NSArray <ChatRedPacketDetailModel *>*list) {
                NSError *error;
                NSArray *arr = [MTLJSONAdapter JSONArrayFromModels:list error:&error];
                return arr;
            }];
}

+ (NSValueTransformer *)activityChatRedpacketJSONTransformer{
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSDictionary *dic){
                NSError *error;
                ChatRedPacketactivityModel *model = [MTLJSONAdapter modelOfClass:[ChatRedPacketactivityModel class] fromJSONDictionary:dic error:&error];
                return model;
            } reverseBlock:^id(ChatRedPacketactivityModel *model) {
                NSError *error;
                NSDictionary *dic = [MTLJSONAdapter JSONDictionaryFromModel:model error:&error];
                return dic;
            }];
}

@end
