//
//  ChatSendModel.h
//  Bet365
//
//  Created by HamaJi on 2018/10/21.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Message.h"

@interface ChatSendModel : NSObject

@property (nonatomic,assign)NSInteger roomID;

@property (nonatomic,assign)NSInteger userID;

@property (nonatomic,strong)NSDate *date;

@property (nonatomic,assign)ChatMessageType messageType;

@property (nonatomic,strong)id content;

@property (nonatomic,assign)ChatMessageSendStatus sendStatus;

+(instancetype)creatSendModel:(NSInteger)roomID MessageType:(ChatMessageType)type Date:(NSDate *)date UserID:(NSInteger)userID;

+(instancetype)stringValueToSendModelHead:(NSString *)string;

-(NSString *)chatSendId;

@end
