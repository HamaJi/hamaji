//
//  ChatSuspendDelegateModel.m
//  Bet365
//
//  Created by HHH on 2018/11/13.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatSuspendDelegateModel.h"

@implementation ChatSuspendDelegateModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"app_float_image": @"app_float_image",
             @"app_float_name": @"app_float_name",
             @"app_float_link": @"app_float_link",
             @"app_gag_title": @"app_gag_title",
             @"isShowHbHistory":@"isShowHbHistory",
             @"isShowHbMoney":@"isShowHbMoney",
             @"isShowAttention":@"isShowAttention"
             };
}
@end
