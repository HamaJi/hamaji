//
//  ChatUtilitySubscribeData.h
//  Bet365
//
//  Created by HamaJi on 2018/10/5.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChatSendModel.h"
@class ChatUtilitySubscribeData;

@protocol ChatUtilitySubscribeDataDelegate <NSObject>

@required

-(void)updateChatUtilitySubscribeData:(ChatUtilitySubscribeData *)data UnMessageCount:(NSUInteger)count;

-(void)updateChatUtilitySubscribeData:(ChatUtilitySubscribeData *)data TimeOutMeessage:(ChatSendModel *)model;

@end

@interface ChatUtilitySubscribeData : NSObject

@property (nonatomic,assign)NSInteger roomID; // id

@property (nonatomic,strong)NSMutableArray <Message *>*messageList;

@property (nonatomic,strong)NSMutableArray <Message *>*filterMessageList;

@property (nonatomic,strong)NSMutableArray <ChatSendModel *>*sendingList;

@property (strong,nonatomic)ChatRuleModel *ruleModel;

@property (strong,nonatomic)ChatRoomInfoModel *roomInfoModel;

@property (strong,nonatomic)STOMPSubscription *josinSubscription;

@property (strong,nonatomic)STOMPSubscription *singleSubscription;

//@property (strong,nonatomic)STOMPSubscription *systemSubscription;

@property (weak,nonatomic)id<ChatUtilitySubscribeDataDelegate> delegate;
    
@property(assign,readonly,nonatomic)NSInteger unreadCount;

-(void)addUnreadMessage:(Message *)message;

-(void)removeUnreadMeesage:(Message *)message;

-(void)removeAllUnredaMessage;

-(void)resumeSendTime;

-(void)pauseSendTime;

-(void)starSendTime;

-(void)stopSendTime;
@end
