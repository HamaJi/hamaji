//
//  ChatUserInfoEntity+CoreDataClass.h
//  Bet365
//
//  Created by adnin on 2019/2/13.
//  Copyright © 2019年 jesse. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatUserInfoEntity : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "ChatUserInfoEntity+CoreDataProperties.h"
