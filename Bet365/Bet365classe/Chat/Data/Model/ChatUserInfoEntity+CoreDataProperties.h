//
//  ChatUserInfoEntity+CoreDataProperties.h
//  Bet365
//
//  Created by adnin on 2019/2/13.
//  Copyright © 2019年 jesse. All rights reserved.
//
//

#import "ChatUserInfoEntity+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ChatUserInfoEntity (CoreDataProperties)

+ (NSFetchRequest<ChatUserInfoEntity *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *avatar;
@property (nullable, nonatomic, copy) NSString *account;
@property (nullable, nonatomic, copy) NSString *nickName;
@property (nullable, nonatomic, copy) NSString *userType;
@property (nullable, nonatomic, copy) NSString *fk;
@property (nullable, nonatomic, copy) NSNumber *level;
@property (nullable, nonatomic, copy) NSNumber *userId;
@property (nullable, nonatomic, copy) NSNumber *customer;

@end

NS_ASSUME_NONNULL_END
