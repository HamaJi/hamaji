//
//  Message.m
//  Bet365
//
//  Created by HHH on 2018/9/23.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Message.h"
#import "EmojiItem.h"
@implementation MessageContent

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"sendId":@"messageId",
             @"text": @"text",
             @"attributedText": @"text",
             @"value": @"value",
             @"userId":@"userId",
             
             @"winMoney":@"winMoney",
             @"betContent": @"betContent",
             @"gameId": @"gameId",
             @"gameName": @"gameName",
             @"playCode": @"playCode",
             @"playName" : @"playName",
             @"turnNum": @"turnNum",
             @"totalMoney": @"totalMoney",
             @"betEndTime": @"betEndTime",
             @"betStatTime": @"betStatTime",
             @"orderNo": @"orderNo",
             @"oneMoney":@"oneMoney",
             @"heelNickName" :@"heelNickName",
             
             @"img": @"img",
             @"img_width" : @"img_width",
             @"img_height": @"img_height",
             @"thumbnail": @"thumbnail",
             @"thumbnail_width" : @"thumbnail_width",
             @"thumbnail_height": @"thumbnail_height",
             
             @"roomId":@"roomId",
             
             @"isKick":@"isKick",
             @"isGag":@"isGag",
             @"isDelMessage":@"isDelMessage",
             @"isBanImg":@"isBanImg",
             @"isViewUserInfo":@"isViewUserInfo",
             @"isSendImg":@"isSendImg",
             @"isSendPacket":@"isSendPacket",
             @"isSpeak":@"isSpeak",
             
             @"redPacketId":@"id",
             @"redCollarPacketId":@"redPacketId",
             @"redName":@"name",
             @"redMoney":@"money",
             
             @"msg":@"message",
             @"isTopping":@"topping",
             
             
             @"chatUserId":@"chatUserId",
             @"nickName":@"nickName",
             @"avatar":@"avatar",
             @"level":@"level",
             @"privateChat":@"privateChat",
             @"status":@"status",
             @"showVipEnter":@"showVipEnter"
             };
}


- (void)setNilValueForKey:(NSString *)key{
    if ([key isEqualToString:@"roomId"]) {
        self.roomId = 0;
    }else{
        [super setNilValueForKey:key];
    }
}

+ (NSValueTransformer *)attributedTextJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString *text){
                id obj = [self replaceEmojiByText:text];
                return obj;
            } reverseBlock:^id(NSString *str ) {
                return @"";
            }];
}

+ (NSValueTransformer *)valueJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(id value){
                if ([value isKindOfClass:[NSDictionary class]]) {
                    
                }else if ([value isKindOfClass:[NSString class]]){
                    
                }
                return value;
            } reverseBlock:^id(id value) {
                return value;
            }];
}
+(NSMutableAttributedString *)replaceEmojiByText:(NSString *)aText{
    
    NSString *brText = [aText stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
    
    NSString *style = @"<style> body { font-family: Avenir; font-size: 15px; color: black; } p:last-of-type { margin: 0; }</style>";
    
    NSData *data = [[NSString stringWithFormat:@"%@%@",style,brText] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary *dict = @{@"WebMainResource": @{@"WebResourceData": data, @"WebResourceFrameName": @"", @"WebResourceMIMEType": @"text/html", @"WebResourceTextEncodingName": @"UTF-8", @"WebResourceURL": @"about:blank"}};
    data = [NSPropertyListSerialization dataWithPropertyList:dict format:NSPropertyListXMLFormat_v1_0 options:0 error:nil];
    
    NSDictionary *options = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                              NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)};
    NSMutableAttributedString *mAtext = [[NSAttributedString alloc]initWithData:data
                                                                        options:options
                                                             documentAttributes:nil
                                                                          error:nil].mutableCopy;
    NSString *text = mAtext.string;
    //    [mAtext addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(0, mAtext.length)];
    
    //    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    //    [paragraphStyle setLineSpacing:3.0];
    //    [mAtext addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [mAtext length])];
    
    
    NSString * pattern = @"\\[[a-zA-Z0-9\\u4e00-\\u9fa5\\ ]+\\]";
    NSError *error = nil;
    
    NSRegularExpression * re = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSArray <NSTextCheckingResult *>*checkingResultList = [re matchesInString:text options:0 range:NSMakeRange(0, text.length)];
    
    NSMutableArray <NSTextCheckingResult *>*mResultList = @[].mutableCopy;
    
    [checkingResultList enumerateObjectsUsingBlock:^(NSTextCheckingResult * _Nonnull obj, NSUInteger cIdx, BOOL * _Nonnull mStop) {
        [[EmojiItem getKeyList] enumerateObjectsUsingBlock:^(NSString * _Nonnull key, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *holderStr = [text substringWithRange:obj.range];
            if ([key isEqualToString:holderStr]) {
                [mResultList addObject:obj];
                *stop = YES;
            }
        }];
        
    }];
    [mResultList enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(NSTextCheckingResult * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *holderStr = [text substringWithRange:obj.range];
        EmojiItem *emojiItem = [EmojiItem getItemWithKey:holderStr];
        NSTextAttachment *textAttachment = [[NSTextAttachment alloc] initWithData:nil ofType:nil] ;
        textAttachment.image = [UIImage imageNamed:emojiItem.imgName];
        textAttachment.bounds = CGRectMake(0, -5, 25, 25);
        NSAttributedString *imageText = [NSAttributedString attributedStringWithAttachment:textAttachment];
        [mAtext replaceCharactersInRange:obj.range withAttributedString:imageText];
    }];
    return mAtext;
}

@end
@implementation Message

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"identity": @"id",
             @"userId": @"userId",
             @"fk": @"fk",
             @"chatType": @"chatType",
             @"curTime": @"curTime",
             @"content": @"content",
             @"chatFrom":@"userId",
             @"privateChatId":@"chatId"
             };
}

+ (NSValueTransformer *)chatFromJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSNumber *userID){
                if ([userID integerValue] == CHAT_UTIL.clientInfo.userInfo.userId){
                    return @(ChatMessageFrom_ME);
                }else if(![userID integerValue]){
                    return @(ChatMessageFrom_SYS);
                }else{
                    return @(ChatMessageFrom_OTHER);
                }
            } reverseBlock:^id(NSNumber * frome) {
                return nil;
            }];
}



+ (NSValueTransformer *)contentJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSDictionary *dic){
                NSError *error;
                
                MessageContent *model = [MTLJSONAdapter modelOfClass:[MessageContent class] fromJSONDictionary:dic error:&error];
                if (error) {
                    
                }
                return model;
            } reverseBlock:^id(MessageContent *d) {
                return d;
            }];
}



+ (NSValueTransformer *)chatTypeJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString *str){
                return @(messageType(str));
            } reverseBlock:^id(NSNumber *type ) {
                return chatTypeKey([type integerValue]);
            }];
}

+ (NSValueTransformer *)curTimeJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString *timeStr){
                NSDate *date = [NSDate dateWithString:timeStr Format:@"yyyy-MM-dd HH:mm:ss"];
                return date;
            } reverseBlock:^id(NSDate *date) {
                NSString *time = [date getTimeFormatStringWithFormat:@"yyyy-MM-dd HH:mm:ss"];
                return time;
            }];
}


ChatMessageType messageType(NSString *chatTypeKey){
    ChatMessageType type = [[[Message messageTypeHash] objectForKey:chatTypeKey] integerValue];
    
    if (type == ChatMessageType_NONE) {
        type = ChatMessageType_TEXT;
    }
    return type;
}

NSString *chatTypeKey(ChatMessageType messageType){
    __block NSString * chatTypeKey = nil;
    [[Message messageTypeHash] enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        if ([obj integerValue] == messageType) {
            chatTypeKey = key;
            *stop = YES;
        }
    }];
    return chatTypeKey;
}

+(NSDictionary *)messageTypeHash{
    return @{
             @"ImageMessage"            :@(ChatMessageType_IMG),
             @"Base64ImageMessage"      :@(ChatMessageType_IMG),
             @"JoinMessage"             :@(ChatMessageType_JOIN),
             @"LeaveMessage"            :@(ChatMessageType_LEAVE),
             @"KickMessage"             :@(ChatMessageType_KICK),
             @"RemoveMessage"           :@(ChatMessageType_REMOVE),
             @"RemoveAllMessage"        :@(ChatMessageType_REMOVEALL),
             @"TextMessage"             :@(ChatMessageType_TEXT),
             @"SystemMessage"           :@(ChatMessageType_SYSTEM),
             @"PushCPBetMessage"        :@(ChatMessageType_LOTTERY),
             @"PushCPWinMessage"        :@(ChatMessageType_WIN),
             @"SendImgMessage"          :@(ChatMessageType_UNIMAGE),
             @"SpeakMessage"            :@(ChatMessageType_UNSPEACK),
             @"RoomRoleMessage"         :@(ChatMessageType_RULE),
             @"RedPacketMessage"        :@(ChatMessageType_RED),
             @"RedPacketCollarMessage"  :@(ChatMessageType_REDCOLLOAR),
             @"RedPacketCollarError"    :@(ChatMessageType_REDCOLLOAR_ERROR),
             @"UserOnlineMessage"       :@(ChatMessageType_ONLINE),
             @"UserOfflineMessage"      :@(ChatMessageType_OFFLINE)
             };
}

@end

