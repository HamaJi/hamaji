//
//  ChatSlideMenuModel.m
//  Bet365
//
//  Created by HHH on 2018/10/17.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatSlideMenuModel.h"

@implementation ChatSlideMenuModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"name": @"name",
             @"type": @"type",
             @"url": @"url",
             @"icon": @"icon",};
}

@end
