//
//  EmojiItem.h
//  Bet365
//
//  Created by HHH on 2018/9/20.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EmojiItem : NSObject

@property (nonatomic,strong,nonnull)NSString *identifier;

@property (nonatomic,strong,nonnull)NSString *imgName;

+(NSMutableArray <EmojiItem *>*)creatItems;

+(NSArray <NSString *>*)getKeyList;

+(EmojiItem *)getItemWithKey:(NSString *)key;
@end
