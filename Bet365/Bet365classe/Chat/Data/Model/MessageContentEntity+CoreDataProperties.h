//
//  MessageContentEntity+CoreDataProperties.h
//  Bet365
//
//  Created by HHH on 2018/11/7.
//  Copyright © 2018年 jesse. All rights reserved.
//
//

#import "MessageContentEntity+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MessageContentEntity (CoreDataProperties)

+ (NSFetchRequest<MessageContentEntity *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *betContent;
@property (nullable, nonatomic, copy) NSNumber *betEndTime;
@property (nullable, nonatomic, copy) NSNumber *betStatTime;
@property (nullable, nonatomic, copy) NSNumber *gameId;
@property (nullable, nonatomic, copy) NSString *gameName;
@property (nullable, nonatomic, copy) NSString *img;
@property (nullable, nonatomic, copy) NSNumber *img_height;
@property (nullable, nonatomic, copy) NSNumber *img_width;
@property (nullable, nonatomic, copy) NSNumber *isBanImg;
@property (nullable, nonatomic, copy) NSNumber *isDelMessage;
@property (nullable, nonatomic, copy) NSNumber *isGag;
@property (nullable, nonatomic, copy) NSNumber *isKick;
@property (nullable, nonatomic, copy) NSString *orderNo;
@property (nullable, nonatomic, retain) NSData *pImageData;
@property (nullable, nonatomic, copy) NSString *playCode;
@property (nullable, nonatomic, copy) NSString *playName;
@property (nullable, nonatomic, copy) NSNumber *roomId;
@property (nullable, nonatomic, copy) NSString *sendId;
@property (nullable, nonatomic, copy) NSString *text;
@property (nullable, nonatomic, copy) NSString *thumbnail;
@property (nullable, nonatomic, copy) NSNumber *thumbnail_height;
@property (nullable, nonatomic, copy) NSNumber *thumbnail_width;
@property (nullable, nonatomic, copy) NSDecimalNumber *totalMoney;
@property (nullable, nonatomic, copy) NSString *turnNum;
@property (nullable, nonatomic, copy) NSNumber *userId;
@property (nullable, nonatomic, copy) NSString *value;
@property (nullable, nonatomic, copy) NSDecimalNumber *winMoney;
@property (nullable, nonatomic, copy) NSNumber *isViewUserInfo;
@property (nullable, nonatomic, copy) NSNumber *isSendImg;
@property (nullable, nonatomic, copy) NSNumber *isSendPacket;
@property (nullable, nonatomic, copy) NSNumber *isSpeak;

@end

NS_ASSUME_NONNULL_END
