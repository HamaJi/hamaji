//
//  UploadImgModel.h
//  Bet365
//
//  Created by HHH on 2018/9/27.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "MTLModel.h"

@interface UploadImgModel : MTLModel<MTLJSONSerializing>

@property (nonatomic,strong)NSString * img;
@property (nonatomic,strong)NSString * sm;
@property (nonatomic,assign)float img_width;
@property (nonatomic,assign)float img_height;
@property (nonatomic,assign)float sm_width;
@property (nonatomic,assign)float sm_height;
@end

 
