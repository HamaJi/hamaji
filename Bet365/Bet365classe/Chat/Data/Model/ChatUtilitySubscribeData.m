//
//  ChatUtilitySubscribeData.m
//  Bet365
//
//  Created by HamaJi on 2018/10/5.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatUtilitySubscribeData.h"
typedef NS_ENUM(NSInteger, ChatSendCycleStatus) {
    /** 进行中*/
    ChatSendCycleStatus_ING = 0,
    /** 暂停*/
    ChatSendCycleStatus_PAUSE,
    /** 停止*/
    ChatSendCycleStatus_STOP,
};
@interface ChatUtilitySubscribeData()

@property (nonatomic,assign)ChatSendCycleStatus sendCycleStatus;

@property (nonatomic,strong)NSMutableArray <Message *>*unreadMessageList;

@end
@implementation ChatUtilitySubscribeData

-(instancetype)init{
    if (self = [super init]) {
        /** 未读数 **/
        @weakify(self);
        [[RACObserve(self, unreadMessageList) merge:self.unreadMessageList.rac_sequence.signal] subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            _unreadCount = [x count];
            if (self.delegate && [self.delegate respondsToSelector:@selector(updateChatUtilitySubscribeData:UnMessageCount:)]) {
                [self.delegate updateChatUtilitySubscribeData:self UnMessageCount:self.unreadCount];
            }
        }];
        
        [[[RACSignal interval:1 onScheduler:[RACScheduler schedulerWithPriority:RACSchedulerPriorityBackground name:@"CHAT_SENDCYLE_QUEUE"]] takeUntil:self.rac_willDeallocSignal ] subscribeNext:^(id x) {
            @strongify(self);
            if (self.sendCycleStatus == ChatSendCycleStatus_ING) {
                NSDate *date = [NSDate date];
                [self.sendingList enumerateObjectsUsingBlock:^(ChatSendModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if ([date timeIntervalSinceDate:obj.date] > 120) {
                        if (obj.sendStatus == ChatMessageSendStatus_SENDING) {
                            obj.sendStatus = ChatMessageSendStatus_FAIL;
                            GT_OC_OUT_SET(@"CHAT", YES, @"消息发送超时 sendId = %@", [obj chatSendId]);
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (self.delegate && [self.delegate respondsToSelector:@selector(updateChatUtilitySubscribeData:TimeOutMeessage:)]) {
                                    [self.delegate updateChatUtilitySubscribeData:self TimeOutMeessage:obj];
                                }
                            });
                        }
                    }
                }];
            }
        }];
    }
    return self;
}


-(NSMutableArray<Message *> *)messageList{
    if (!_messageList) {
        _messageList = @[].mutableCopy;
    }
    return _messageList;
    
}

-(NSMutableArray<Message *> *)filterMessageList{
    if (!_filterMessageList) {
        _filterMessageList = @[].mutableCopy;
    }
    return _filterMessageList;
}

-(NSMutableArray<Message *> *)unreadMessageList{
    if (!_unreadMessageList) {
        _unreadMessageList = @[].mutableCopy;
    }
    return _unreadMessageList;
}

-(NSMutableArray<ChatSendModel *> *)sendingList{
    if (!_sendingList) {
        _sendingList = @[].mutableCopy;
    }
    return _sendingList;
}

-(void)addUnreadMessage:(Message *)message{
    NSLog(@"message.chatFrom = %li , message.chatType = %li",message.chatFrom,message.chatType);
    if (message.chatFrom != ChatMessageFrom_ME &&
        (message.chatType == ChatMessageType_TEXT ||
         message.chatType == ChatMessageType_IMG ||
         message.chatType == ChatMessageType_LOTTERY ||
         message.chatType == ChatMessageType_RED ||
         message.chatType == ChatMessageType_JOIN)) {
            NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
            NSArray *entityList = [CHAT_UTIL getFollowEntityListByRoomId:self.roomID inContext:context];
            if (BET_CONFIG.userSettingData.chatFollowStatus == ChatFollowStatus_ONLY) {
                if (entityList.count) {
                    [entityList enumerateObjectsUsingBlock:^(ChatFollowEntity * _Nonnull entity, NSUInteger entityIdx, BOOL * _Nonnull entityStop) {
                        if (entity.followUserID == [message.userId integerValue]) {
                                [[self mutableArrayValueForKey:@"unreadMessageList"] addObject:message];
                            }
                    }];
                }
            }else{
                [[self mutableArrayValueForKey:@"unreadMessageList"] addObject:message];
            }
        }
}

-(void)removeUnreadMeesage:(Message *)message{
    [self.unreadMessageList enumerateObjectsUsingBlock:^(Message * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.identity integerValue] == [message.identity integerValue]) {
            [[self mutableArrayValueForKey:@"unreadMessageList"] removeObjectAtIndex:idx];
            *stop = YES;
        }else if ([obj.identity integerValue] == [message.content.value integerValue]){
            [[self mutableArrayValueForKey:@"unreadMessageList"] removeObjectAtIndex:idx];
            *stop = YES;
        }
    }];
}

-(void)removeAllUnredaMessage{
    [[self mutableArrayValueForKey:@"unreadMessageList"] removeAllObjects];
}

-(void)resumeSendTime{
    if (self.sendCycleStatus != ChatSendCycleStatus_PAUSE) {
        return;
    }
    self.sendCycleStatus = ChatSendCycleStatus_ING;
    
}

-(void)pauseSendTime{
    if (self.sendCycleStatus != ChatSendCycleStatus_ING) {
        return;
    }
    self.sendCycleStatus = ChatSendCycleStatus_PAUSE;
}

-(void)starSendTime{
    if (self.sendCycleStatus == ChatSendCycleStatus_ING) {
        return;
    }
    self.sendCycleStatus = ChatSendCycleStatus_ING;
}

-(void)stopSendTime{
    if (self.sendCycleStatus != ChatSendCycleStatus_ING) {
        return;
    }
    self.sendCycleStatus = ChatSendCycleStatus_STOP;
}
@end
