//
//  ChatUserInfoEntity+CoreDataProperties.m
//  Bet365
//
//  Created by adnin on 2019/2/13.
//  Copyright © 2019年 jesse. All rights reserved.
//
//

#import "ChatUserInfoEntity+CoreDataProperties.h"

@implementation ChatUserInfoEntity (CoreDataProperties)

+ (NSFetchRequest<ChatUserInfoEntity *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"ChatUserInfoEntity"];
}

@dynamic avatar;
@dynamic account;
@dynamic nickName;
@dynamic userType;
@dynamic fk;
@dynamic level;
@dynamic userId;
@dynamic customer;

@end
