//
//  Message.h
//  Bet365
//
//  Created by HHH on 2018/9/23.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "MTLModel.h"
typedef NS_ENUM(NSInteger, ChatMessageFrom) {
    ChatMessageFrom_SYS = 0,
    /** 自己*/
    ChatMessageFrom_ME,
    /** 对方*/
    ChatMessageFrom_OTHER,
};
typedef NS_ENUM(NSInteger, ChatMessageType) {
    
#pragma mark - Single
    ChatMessageType_NONE = 0,
    /** 文本*/
    ChatMessageType_TEXT,
    /** 图片*/
    ChatMessageType_IMG,
    /** 彩票*/
    ChatMessageType_LOTTERY,
    /** 红包*/
    ChatMessageType_RED,
    /** 红包领取*/
    ChatMessageType_REDCOLLOAR,
    /** 红包领取失败*/
    ChatMessageType_REDCOLLOAR_ERROR,
    
#pragma mark - System
    /** 加入聊天*/
    ChatMessageType_JOIN,
    /** 离开聊天*/
    ChatMessageType_LEAVE,
    /** 移除聊天*/
    ChatMessageType_KICK,
    /** 删除聊天*/
    ChatMessageType_REMOVE,
    /** 删除聊天*/
    ChatMessageType_REMOVE_PRIVATE,
    /** 删除全部聊天*/
    ChatMessageType_REMOVEALL,
    
    /** 系统*/
    ChatMessageType_SYSTEM,
    /** 禁言*/
    ChatMessageType_UNSPEACK,
    /** 禁图*/
    ChatMessageType_UNIMAGE,
    /** 中奖*/
    ChatMessageType_WIN,
    /** 权限*/
    ChatMessageType_RULE,
    
    /** 已读回执*/
    ChatMessageType_READ_PRIVATE,
    /** 获取私聊用户信息*/
    ChatMessageType_USERINFO_PRIVATE,
    
#pragma mark - TODO
    /** 语音*/
    ChatMessageType_VOICE,
    /** 视频*/
    ChatMessageType_VIDEO,
    
#pragma mark - Private
    /** 在线*/
    ChatMessageType_ONLINE,
    /** 离线聊天*/
    ChatMessageType_OFFLINE,
    
};

typedef NS_ENUM(NSInteger, ChatMessageSendStatus) {
    /** 成功*/
    ChatMessageSendStatus_SUCCESS = 0,
    /** 失败*/
    ChatMessageSendStatus_FAIL,
    /** 发送中*/
    ChatMessageSendStatus_SENDING,
};

@interface MessageContent : MTLModel<MTLJSONSerializing>

@property (nonatomic,copy)NSString *sendId;
@property (nonatomic,strong)NSString * text;
@property (nonatomic,strong)NSAttributedString * attributedText; //
@property (nonatomic,assign)NSInteger userId;
@property (nonatomic,strong)id value;

/** 彩票集**/
@property (nonatomic,strong)NSNumber* winMoney;
@property (nonatomic,strong)NSString * betContent;
@property (nonatomic,strong)NSNumber *gameId;
@property (nonatomic,strong)NSString * gameName;
@property (nonatomic,strong)NSString * playCode;
@property (nonatomic,strong)NSString * playName;
@property (nonatomic,strong)NSString * turnNum;
@property (nonatomic,strong)NSNumber *totalMoney;
@property (nonatomic,strong)NSNumber* betEndTime;
@property (nonatomic,strong)NSNumber* betStatTime;
@property (nonatomic,strong)NSString * orderNo;
@property (nonatomic,strong)NSNumber * oneMoney;

@property (nonatomic,strong)NSString * heelNickName;

/** 图片集**/
@property (nonatomic,strong)NSData *pImageData;
@property (nonatomic,strong)NSString* img;
@property (nonatomic,strong)NSNumber* img_width;
@property (nonatomic,strong)NSNumber* img_height;
@property (nonatomic,strong)NSString* thumbnail;
@property (nonatomic,strong)NSNumber* thumbnail_width;
@property (nonatomic,strong)NSNumber* thumbnail_height;

/** 系统集**/
@property (nonatomic,assign)NSInteger roomId;

/** 权限集**/
@property (nonatomic,assign) BOOL isKick; // 是否能踢人 0否、1是
@property (nonatomic,assign) BOOL isGag; // 是否能禁言 0否、1是
@property (nonatomic,assign) BOOL isDelMessage;// 是否能删除 0否、1是
@property (nonatomic,assign) BOOL isBanImg; // 是否能禁止发图片 0否、1是
@property (nonatomic,assign) BOOL isViewUserInfo; // 是否可以查看会员信息 0否、1是
@property (nonatomic,assign) BOOL isSendImg; // 自己是否可以发送图片
@property (nonatomic,assign) BOOL isSendPacket; // 自己是否可以发红包
@property (nonatomic,assign) BOOL isSpeak; // 自己是否可以说话
@property (nonatomic,assign) BOOL isTopping;

@property (nonatomic,strong) NSString *redPacketId;
@property (nonatomic,strong) NSString *redCollarPacketId;
@property (nonatomic,strong) NSString *redName;
@property (nonatomic,strong) NSNumber *redMoney;

@property (nonatomic,strong) NSString *msg;

/** 加入房间消息**/
@property (nonatomic,strong) NSString *nickName;
@property (nonatomic,strong) NSString *avatar;
@property (nonatomic,strong) NSNumber *chatUserId;
@property (nonatomic,strong) NSNumber *level;
@property (nonatomic,strong) NSNumber *privateChat;
@property (nonatomic,strong) NSNumber *status;

/*是否VIP进入房间*/
@property (nonatomic,strong) NSNumber *showVipEnter;

+(NSMutableAttributedString *)replaceEmojiByText:(NSString *)text;

@end

@interface Message : MTLModel<MTLJSONSerializing>

@property (nonatomic,copy)NSNumber* identity; // id
@property (nonatomic,copy)NSNumber *userId;

@property (nonatomic,strong)NSNumber* privateChatId;
//@property (nonatomic,copy)NSNumber * level;

@property (nonatomic,strong)NSString * fk;
//@property (nonatomic,strong)NSString * avatar;

@property (nonatomic,assign)ChatMessageFrom chatFrom;
@property (nonatomic,assign)ChatMessageType chatType;
//@property (nonatomic,strong)NSString * nickName;
@property (nonatomic,strong)NSDate * curTime;
@property (nonatomic,strong)NSString * roleId;
@property (nonatomic,strong)MessageContent * content;
@property (nonatomic,assign)ChatMessageSendStatus sendStatus;

@property (nonatomic,assign)BOOL hasRead;
@property (nonatomic,assign)BOOL hasRedPack;

@property (nonatomic,assign)CGFloat kHeight;
@end
NSString *ajiEncodeString();
