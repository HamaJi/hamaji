//
//  ChatSendModel.m
//  Bet365
//
//  Created by HamaJi on 2018/10/21.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatSendModel.h"

@implementation ChatSendModel
+(instancetype)creatSendModel:(NSInteger)roomID MessageType:(ChatMessageType)type Date:(NSDate *)date UserID:(NSInteger)userID{
    ChatSendModel *model = [ChatSendModel new];
    model.roomID = roomID;
    model.userID = userID;
    model.messageType = type;
    model.date = date;
    model.sendStatus = ChatMessageSendStatus_SENDING;
    return model;
}

-(NSString *)chatSendId{
    NSString *messageID = [NSString stringWithFormat:@"roomID:%li,userID:%li,messageType:%li,stamp:%f",self.roomID,self.userID,self.messageType,[self.date timeIntervalSince1970]];
    NSString*content = [messageID stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    return content;
}

+(instancetype)stringValueToSendModelHead:(NSString *)string{
    NSArray <NSString *>*list = [[string aci_decryptWithAESBykey:@"ChatSend"] componentsSeparatedByString:@","];
    ChatSendModel *model = [ChatSendModel new];
    model.roomID = [[list[0] substringFromIndex:[@"roomID:" length]] integerValue];
    model.userID = [[list[1] substringFromIndex:[@"userID:" length]] integerValue];
    model.messageType = [[list[2] substringFromIndex:[@"messageType:" length]] integerValue];
    model.date = [NSDate dateWithTimeIntervalSince1970:[[list[3] substringFromIndex:[@"stamp:" length]] integerValue]];;
    return model;
}


@end
