//
//  MessageEntity+CoreDataProperties.m
//  Bet365
//
//  Created by HHH on 2018/11/7.
//  Copyright © 2018年 jesse. All rights reserved.
//
//

#import "MessageEntity+CoreDataProperties.h"

@implementation MessageEntity (CoreDataProperties)

+ (NSFetchRequest<MessageEntity *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"MessageEntity"];
}

@dynamic avatar;
@dynamic chatFrom;
@dynamic chatType;
@dynamic curTime;
@dynamic fk;
@dynamic hasRead;
@dynamic identity;
@dynamic level;
@dynamic nickName;
@dynamic roleId;
@dynamic sendStatus;
@dynamic userId;
@dynamic content;


@end
