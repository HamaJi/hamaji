//
//  ChatInfoModel.h
//  Bet365
//
//  Created by HHH on 2018/9/22.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "MTLModel.h"
#import <MTLManagedObjectAdapter/MTLManagedObjectAdapter.h>

@interface ChatUserInfoModel : MTLModel<MTLJSONSerializing,MTLManagedObjectSerializing>

@property (nonatomic,copy)NSString * avatar;        //"/images/chat/wzry/195.jpg"
@property (nonatomic,copy)NSString * account;       //"cc001"
@property (nonatomic,copy)NSString * nickName;      //小虎啊"
@property (nonatomic,copy)NSString * userType;      //"HY"
@property (nonatomic,copy)NSString *fk;            //"9742A6610BD1655938D1F7E2C3A3909F"
@property (nonatomic,copy)NSNumber * level;         //102
@property (nonatomic,assign)NSInteger userId;       //108807346
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,strong)NSNumber* privateChat;
@property (nonatomic,strong)NSNumber* privateChatId;
@property (nonatomic,strong)NSNumber* chatUserId;

@property (nonatomic,strong)NSNumber *customer;

/** 加入房间request 实体privateChat列表内 **/
@property (nonatomic,assign)NSInteger unreadCount;
/** 最新消息时间 **/
@property (nonatomic,strong) NSDate *lastNewDate;
@end




@interface ChatRuleModel : MTLModel<MTLJSONSerializing>

@property (nonatomic,copy)NSNumber * maxLength;
@property (nonatomic,copy)NSNumber *roomId;
@property (nonatomic,copy)NSNumber *userId;

@property (nonatomic,copy)NSNumber *isKick;
@property (nonatomic,copy)NSNumber *isGag;
@property (nonatomic,copy)NSNumber *isSendImg;
@property (nonatomic,copy)NSNumber *isBanImg;
@property (nonatomic,copy)NSNumber *isDelMessage;
@property (nonatomic,copy)NSNumber *isSpeak;
@property (nonatomic,copy)NSNumber *isSendPacket;
@property (nonatomic,copy)NSNumber *isViewUserInfo;
@property (nonatomic,copy)NSNumber *isPrivateChat;
@property (nonatomic,copy)NSNumber *isManager;
@property (nonatomic,copy)NSNumber *isShowOnlineUser;
@end


@interface ChatRoomInfoModel : MTLModel<MTLJSONSerializing>
@property (nonatomic,copy)NSString *welcomeMsg;
@property (nonatomic,copy)NSString * name;
@property (nonatomic,copy)NSArray <NSString *>* planGameList;
@property (nonatomic,copy)NSString * speakBeginTime;
@property (nonatomic,copy)NSString * speakEndTime;
@property (nonatomic,copy)NSString * remark;
@property (nonatomic,copy)NSNumber* identity; // id
@property (nonatomic,copy)NSNumber * sendPlan;
@property (nonatomic,assign)BOOL isOpen;
@property (nonatomic,copy)NSNumber *isOpenLeaderboard;
@property (nonatomic,copy)NSNumber *isShowOnlineUser;
//@property (nonatomic,copy)NSString * platCode;
//@property (nonatomic,copy)NSNumber * roomType;

//@property (nonatomic,copy)NSNumber * rechMoney;
//@property (nonatomic,copy)NSNumber * betMoney;

//@property (nonatomic,strong)NSNumber *isSpeak;
//@property (nonatomic,strong)NSNumber *isChat;
//@property (nonatomic,strong)NSNumber *isSendImg;
//@property (nonatomic,copy)NSNumber *isSendPacket;
//@property (nonatomic,assign)BOOL  isTest;


////** 新增参数**/
//@property (nonatomic,copy)NSString * account;
//@property (nonatomic,copy)NSString * nickName;
//@property (nonatomic,copy)NSString * unSpeakTime;
//@property (nonatomic,assign)NSInteger fixed;
//@property (nonatomic,assign)NSInteger roleId;
//@property (nonatomic,assign)NSInteger roomId;
//@property (nonatomic,assign)NSInteger status;
//@property (nonatomic,assign)NSInteger userId;
@end

@interface ChatInfoModel : MTLModel<MTLJSONSerializing>

@property (nonatomic,strong)ChatUserInfoModel *userInfo;
@property (nonatomic,strong)NSMutableArray <ChatRoomInfoModel *>*roomInfoList;

@property (nonatomic,assign)NSInteger defaultRoomId;
@property (nonatomic,copy)NSString * chatUrl;

@property (nonatomic,copy)NSString *ybcsid;

@property (nonatomic,strong)NSArray<NSString *> * nodes;

@property (nonatomic,copy)NSString *imageHost;

@property (nonatomic,copy)NSString *uploadHost;
@end


