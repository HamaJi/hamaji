//
//  LotteryOpenInfoModel.h
//  Bet365
//
//  Created by HHH on 2018/8/31.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface LotteryOpenInfoSubModel : MTLModel <MTLJSONSerializing>

@property (nonatomic,copy)NSString <Optional>*turnNum;                  //开奖期YMD-C
@property (nonatomic,copy)NSString <Optional>*turn;                     //开奖期C
@property (nonatomic,assign)NSInteger status;                           //状态
@property (nonatomic,strong)NSArray <Optional>*openNumList;             //开奖信息
@property (nonatomic,strong)NSDate *openTime;                           //per
@property (nonatomic,strong)NSDate *closeTime;                          //cur

@end

@interface LotteryOpenInfoModel : MTLModel <MTLJSONSerializing>
@property (nonatomic, strong) NSString *lotteryID;
@property (nonatomic, strong) LotteryOpenInfoSubModel <Optional>*cur;   //当前
@property (nonatomic, strong) LotteryOpenInfoSubModel <Optional>*pre;   //上一期
@property (nonatomic, assign) NSTimeInterval serverTimeDiffence;

@end
