//
//  ChatAction.h
//  Bet365
//
//  Created by HHH on 2018/9/16.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - Service
#import "ChatUtility.h"
#import "HomePageFactory.h"
#import "LotteryFactory.h"

#pragma mark - Command
#import "EmojiItem.h"
#import "Message.h"
#import <AssetsLibrary/AssetsLibrary.h>

#pragma mark - View
#import "ChatStickMeesageView.h"
#import "ChatScrollBottomButton.h"
#import "ChatLotteryTopView.h"
#import "ChatInputToolBar.h"
#import <iCarousel/iCarousel.h>
#import "ChatCell.h"
#import "ChatMessageCell.h"
#import "ChatPrivateNewMessageView.h"

#pragma mark - ViewController
#import "ChatRedOpenViewController.h"
#import "ChatRedResultViewController.h"
#import <PhotoBrowser/PhotoBrowser.h>
#import "ChatLotteryPopViewController.h"
#import "ChatLotteryOpenHistoryViewController.h"
#import "ChatMessagePopViewController.h"
#import "ChatUserInfoPopViewController.h"

typedef NS_ENUM(NSUInteger, ChatTableViewReloadType) {
    /** 全刷*/
    ChatTableViewReloadType_RELOADALL,
    /** 新增*/
    ChatTableViewReloadType_INSERT,
};

@class ChatGroupAction;

@class ChatPrivateAction;

@protocol ChatActionDelegate <NSObject>

@required

@optional


#pragma mark - Delegate By Group
-(void)ChatAction:(ChatGroupAction *)action ChangeRoomByID:(NSInteger)roomID;

//-(void)ChatAction:(ChatAction *)action UpDataRoomInfo:(ChatRoomInfoModel *)model;

-(void)ChatAction:(ChatGroupAction *)action ReceiveWinMessage:(Message *)message;

-(void)ChatAction:(ChatGroupAction *)action ReceiveVipEnterMessage:(Message *)message;

-(void)ChatAction:(ChatGroupAction *)action UpdataOpenList:(NSArray<LotteryOpenInfoModel *> *)openList;

-(void)ChatAction:(ChatGroupAction *)action SetHideTabBar:(BOOL)isHide;

-(void)ChatAction:(ChatGroupAction *)action JoinRoomFail:(NSInteger)roomId;

-(void)ChatAction:(ChatGroupAction *)action JoinRoomSuccese:(ChatUtilityGroupSubscribeData *)data;

-(void)ChatAction:(ChatGroupAction *)action StickMessage:(Message *)message;

-(void)HasBeKickedChatAction:(ChatGroupAction *)action;

-(void)ChatAction:(ChatGroupAction *)action UpdateUserOnlineCount:(NSInteger)userOnlineCount;

#pragma mark - Delegate By Private
-(void)ChatAction:(ChatPrivateAction *)action UserIsOnline:(BOOL)isOnline;

@end

@interface ChatAction : NSObject
<UITableViewDataSource,
UITableViewDelegate,
PBViewControllerDataSource,
PBViewControllerDelegate,
ChatInputToolBarDelegate,
ChatCellDelegate,
ChatUtilityDataDelegate,
UIViewControllerTransitioningDelegate,
ChatUtilitySubscribeDataDelegate>

@property (weak, nonatomic) ChatScrollBottomButton *bottomBtn;

@property(weak,nonatomic)UITableView *tableView;

@property(weak,nonatomic)ChatInputToolBar *inputToolBar;

@property(assign,nonatomic)NSInteger channelID;

@property (nonatomic,weak)id <ChatActionDelegate> delegate;


#pragma mark - Private
/**
 数据源
 */
@property (nonatomic,strong)ChatUtilitySubscribeData *data;

@property (nonatomic,strong)NSMutableArray *filterMessageList;

/**
 UIImageBrowser图片集
 */
@property (nonatomic,strong)NSMutableArray <NSIndexPath*>*imgIndexPathList;

/**
 红包弹窗
 */
@property (nonatomic,strong)ChatRedOpenViewController *redOpenViewController;

/**
 红包结果弹窗
 */
@property (nonatomic,strong)ChatRedResultViewController *redResultViewController;

+(instancetype)creatActionByChannelId:(NSInteger)channelId;

-(void)scrollBottomAnimated:(BOOL)animated;

-(void)scrolleHadScroll;

-(void)cancleAllRequestTask;

-(void)resignAllFirstResponder;

-(void)chatReloadTableView;
@end
