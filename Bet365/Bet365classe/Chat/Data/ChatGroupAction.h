//
//  ChatGroupAction.h
//  Bet365
//
//  Created by HHH on 2018/9/16.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChatAction.h"
#import "ChatJoinRoomBubbleTools.h"


@interface ChatGroupAction : ChatAction

@property (weak, nonatomic) iCarousel *carousel;


/**
 置顶消息
 */
@property (nonatomic,strong)Message *stickMessage;

@property (weak, nonatomic) ChatPrivateNewMessageView *unreadMessageListView;

//@property (nonatomic,strong)NSMutableArray <Message *>*messageList;
/**
 彩票开奖集
 */
@property (nonatomic,strong)NSMutableArray <LotteryOpenInfoModel *>*openLotteryList;

//@property (nonatomic,strong)ChatRuleModel *ruleModel;
//
//@property (nonatomic,strong)ChatRoomInfoModel *roomInfoModel;

@property (nonatomic,weak)ChatJoinRoomBubbleTools *joinRoomBubbleTools;

@end
