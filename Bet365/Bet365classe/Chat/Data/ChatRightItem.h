//
//  ChatRightItem.h
//  Bet365
//
//  Created by HHH on 2018/10/9.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatRightItem : NSObject
@property (nonatomic,strong)NSString *titile;
@property (nonatomic,strong)NSString *imgPath;
@property (nonatomic,assign)SEL selector;
@property (nonatomic,assign)NSInteger tag;
+(instancetype)creatItemTitile:(NSString *)titile ImgPath:(NSString *)imgPath Sel:(SEL)sel;
@end
