//
//  ChatSendMessageGroupModel.m
//  Bet365
//
//  Created by adnin on 2019/2/6.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatSendMessageGroupModel.h"

@implementation ChatSendMessageGroupModel

ChatSendDestination *const kChatSendMessageTextGroup = @"/app/send";

ChatSendDestination *const kChatSendRequestHistoryGroup = @"/app/getHistoryMessage";

+(instancetype)creatDefaultSendModel:(NSDate *)date{
    ChatSendMessageGroupModel *model = [super creatDefaultSendModel:date];
    model.destination = kChatSendMessageTextGroup;
    return model;
}

-(NSString *)getChatSendId{
    NSMutableDictionary *data = @{}.mutableCopy;
    [data safeSetObject:@(self.roomId)                          forKey:@"roomID"];
    [data safeSetObject:@(CHAT_UTIL.clientInfo.userInfo.userId) forKey:@"userID"];
    [data safeSetObject:self.destination                        forKey:@"destination"];
    [data safeSetObject:@([self.date timeIntervalSince1970])    forKey:@"stamp"];
    NSLog(@"sendMessgeModel:%@",data);
    NSString*content = [[data mj_JSONString] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    return content;
}

-(NSDictionary *)getHeader{
    
    return @{@"Content-Type":@"application/x-www-form-urlencoded; charset=utf-8",
             @"roomId":@(self.roomId)};
}


@end
