//
//  ChatSendMessageModel.h
//  Bet365
//
//  Created by adnin on 2019/2/6.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatSendModel.h"

@interface ChatSendMessageModel : ChatSendModel

@property (nonatomic,strong)id content;

@property (nonatomic,assign)ChatMessageSendStatus sendStatus;

@property (nonatomic,assign)ChatMessageType messageType;

+(instancetype)creatDefaultSendModel:(NSDate *)date;

@end
