//
//  ChatSendMessageGroupModel.h
//  Bet365
//
//  Created by adnin on 2019/2/6.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatSendMessageModel.h"

@interface ChatSendMessageGroupModel : ChatSendMessageModel

@property (nonatomic,assign)NSInteger roomId;

@end
