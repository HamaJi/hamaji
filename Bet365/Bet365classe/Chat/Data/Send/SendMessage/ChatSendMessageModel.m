//
//  ChatSendMessageModel.m
//  Bet365
//
//  Created by adnin on 2019/2/6.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatSendMessageModel.h"
@implementation ChatSendMessageModel

ChatSendDestination *const kChatSendCollarRedPage = @"/app/collarRedpack";

//@synthesize description = _description;
//
//-(void)setDestination:(ChatSendDestination *)destination{
//    _description = destination;
//    self.messageType = ChatSendMessageType(destination);
//}

+(instancetype)creatDefaultSendModel:(NSDate *)date{
    id model = [[[self class] alloc] init];
    ((ChatSendMessageModel *)model).destination = kChatSendMessageTextPrivate;
    ((ChatSendMessageModel *)model).date = date;
    return model;
}
-(ChatMessageType)messageType{
    if (_messageType == ChatMessageType_NONE) {
        _messageType = ChatSendMessageType(self.destination);
    }
    return _messageType;
}

ChatMessageType ChatSendMessageType(ChatSendDestination *destination){
    ChatMessageType type = [[@{kChatSendMessageTextGroup:@(ChatMessageType_TEXT),
                               kChatSendMessageTextPrivate:@(ChatMessageType_TEXT),
                               kChatSendCollarRedPage:@(ChatMessageType_REDCOLLOAR)
    } objectForKey:destination] integerValue];
    return type;
}

-(void)setContent:(id)content{
    if (self.messageType == ChatMessageType_TEXT) {
        NSMutableDictionary *dic = @{}.mutableCopy;
        [dic safeSetObject:@"text" forKey:@"type"];
        [dic safeSetObject:content forKey:@"content"];
        [dic safeSetObject:[self getChatSendId] forKey:@"messageId"];
        _content = [dic mj_JSONString];
    }else if (self.messageType == ChatMessageType_IMG){
        NSMutableDictionary *dic = @{}.mutableCopy;
        [dic safeSetObject:@"image" forKey:@"type"];
        [dic safeSetObject:content forKey:@"imgInfo"];
        [dic safeSetObject:[self getChatSendId] forKey:@"messageId"];
        _content = [dic mj_JSONString];
    }else{
        _content = content;
    }
}
@end
