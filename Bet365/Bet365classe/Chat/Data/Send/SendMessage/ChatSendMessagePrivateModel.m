//
//  ChatSendMessagePrivateModel.m
//  Bet365
//
//  Created by adnin on 2019/2/6.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatSendMessagePrivateModel.h"

@implementation ChatSendMessagePrivateModel

ChatSendDestination *const kChatSendMessageTextPrivate = @"/app/sendToUser";

+(instancetype)creatDefaultSendModel:(NSDate *)date{
    ChatSendMessagePrivateModel *model = [super creatDefaultSendModel:date];
    model.destination = kChatSendMessageTextPrivate;
    return model;
}

-(NSString *)getChatSendId{
    NSMutableDictionary *data = @{}.mutableCopy;
    [data safeSetObject:@(self.userId)                          forKey:@"receiveId"];
    [data safeSetObject:@(CHAT_UTIL.clientInfo.userInfo.userId) forKey:@"sendId"];
    [data safeSetObject:self.destination                        forKey:@"destination"];
    [data safeSetObject:@([self.date timeIntervalSince1970])    forKey:@"stamp"];
    NSString*content = [[data mj_JSONString] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    return content;
}

-(NSDictionary *)getHeader{
    return  @{@"Content-Type":@"application/x-www-form-urlencoded; charset=utf-8",
              @"userId":@(self.userId)};
}


@end
