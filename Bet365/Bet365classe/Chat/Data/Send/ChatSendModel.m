//
//  ChatSendModel.m
//  Bet365
//
//  Created by HHH on 2018/10/21.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatSendModel.h"

@implementation ChatSendModel

+(instancetype)creatSendModel:(ChatSendDestination *)destination Date:(NSDate *)date{
    ChatSendModel *model = [[[self class] alloc] init];
    model.destination = destination;
    model.date = date;
    return model;
}

-(NSString *)getChatSendId{
    return nil;
}

-(NSString *)getHeader{
    return nil;
}

- (id) valueForUndefinedKey:(NSString *)key{
    NSLog(@"Undefined Key: %@",key);
    return nil;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    NSLog(@"Undefined Key: %@",key);
}

@end
