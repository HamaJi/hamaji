//
//  ChatSendRequestModel.h
//  Bet365
//
//  Created by adnin on 2019/2/6.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatSendModel.h"

@interface ChatSendRequestModel : ChatSendModel

@property (nonatomic,assign)NSInteger messageId;

@property (nonatomic,assign)NSInteger userId;

@end
