//
//  ChatSendRequestGroupModel.m
//  Bet365
//
//  Created by adnin on 2019/2/6.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatSendRequestGroupModel.h"

ChatSendDestination *const kChatSendRequestJoinGroup = @"/app/joinRoom";

ChatSendDestination *const kChatSendRequestUserInfoGroup = @"/app/getUserInfo";

@implementation ChatSendRequestGroupModel

-(NSString *)getChatSendId{
    NSMutableDictionary *data = @{}.mutableCopy;
    [data safeSetObject:@(self.roomId)                          forKey:@"roomID"];
    [data safeSetObject:@(CHAT_UTIL.clientInfo.userInfo.userId) forKey:@"userID"];
    [data safeSetObject:self.destination                        forKey:@"destination"];
    [data safeSetObject:@([self.date timeIntervalSince1970])    forKey:@"stamp"];
    NSString*content = [[data mj_JSONString] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    return content;
}

-(NSDictionary *)getHeader{
    
    NSMutableDictionary *mHeader = @{@"Content-Type":@"application/x-www-form-urlencoded; charset=utf-8",
                              @"roomId":@(self.roomId),
                              @"requestId":self.chatSendId
                              }.mutableCopy;
    if ([self.destination isEqualToString:kChatSendRequestHistoryGroup]) {
        [mHeader removeObjectForKey:@"roomId"];
    }
    if (self.userId) {
        [mHeader setObject:@(self.userId) forKey:@"userId"];
    }
    if (self.messageId) {
        [mHeader setObject:@(self.messageId) forKey:@"messageId"];
    }
    if (self.joinRoomPsw.length) {
        [mHeader setObject:self.joinRoomPsw forKey:@"password"];
    }
    
    return mHeader;
}
@end
