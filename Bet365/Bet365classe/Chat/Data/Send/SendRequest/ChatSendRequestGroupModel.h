//
//  ChatSendRequestGroupModel.h
//  Bet365
//
//  Created by adnin on 2019/2/6.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatSendRequestModel.h"

@interface ChatSendRequestGroupModel : ChatSendRequestModel

@property (nonatomic,assign)NSInteger roomId;

@property (nonatomic,assign) NSString *joinRoomPsw;

@end
