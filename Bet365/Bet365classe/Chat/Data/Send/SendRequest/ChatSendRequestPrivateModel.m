//
//  ChatSendRequestPrivateModel.m
//  Bet365
//
//  Created by adnin on 2019/2/6.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatSendRequestPrivateModel.h"


@implementation ChatSendRequestPrivateModel

ChatSendDestination *const kChatSendRequestUserInfoPrivate = @"/app/getChatInfo";

ChatSendDestination *const kChatSendRequestReadPrivate = @"/app/addReadRecord";

ChatSendDestination *const kChatSendRequestHistoryPrivate = @"/app/getUserHistoryMessage";

ChatSendDestination *const kChatSendRequestRevokePrivate = @"/app/revokeUserMessage";



-(NSString *)getChatSendId{
    NSMutableDictionary *data = @{}.mutableCopy;
    [data safeSetObject:@(self.userId)                          forKey:@"receiveId"];
    [data safeSetObject:@(CHAT_UTIL.clientInfo.userInfo.userId) forKey:@"sendId"];
    [data safeSetObject:self.destination                        forKey:@"destination"];
    [data safeSetObject:@([self.date timeIntervalSince1970])    forKey:@"stamp"];
    
    NSString*content = [[data mj_JSONString] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    return content;
}

-(NSDictionary *)getHeader{
    NSMutableDictionary *mHeader = @{@"Content-Type":@"application/x-www-form-urlencoded; charset=utf-8",
                                     @"requestId":self.chatSendId,
                                     }.mutableCopy;
    if (self.userId) {
        [mHeader setObject:@(self.userId) forKey:@"userId"];
    }
    [mHeader setObject:@(self.messageId) forKey:@"messageId"];
    return mHeader;
}
@end
