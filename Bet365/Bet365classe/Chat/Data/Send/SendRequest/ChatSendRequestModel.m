//
//  ChatSendRequestModel.m
//  Bet365
//
//  Created by adnin on 2019/2/6.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatSendRequestModel.h"

@implementation ChatSendRequestModel

ChatSendDestination *const kChatSendRequestImageKey = @"/app/getImageKey";

-(NSDictionary *)getHeader{
    
    NSMutableDictionary *mHeader = @{@"Content-Type":@"application/x-www-form-urlencoded; charset=utf-8",
                              
                              @"requestId":self.chatSendId
                              }.mutableCopy;
    
    return mHeader;
}

-(NSString *)getChatSendId{
    
    NSMutableDictionary *data = @{}.mutableCopy;
    [data safeSetObject:self.destination                        forKey:@"destination"];
    [data safeSetObject:@([self.date timeIntervalSince1970])    forKey:@"stamp"];
    NSString*content = [[data mj_JSONString] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    return content;
}
@end
