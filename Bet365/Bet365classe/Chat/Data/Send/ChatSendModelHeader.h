//
//  ChatSendModelHeader.h
//  Bet365
//
//  Created by adnin on 2019/2/6.
//  Copyright © 2019年 jesse. All rights reserved.
//

#ifndef ChatSendModelHeader_h
#define ChatSendModelHeader_h

#import "ChatSendModel.h"
#import "ChatSendRequestPrivateModel.h"
#import "ChatSendRequestGroupModel.h"
#import "ChatSendMessagePrivateModel.h"
#import "ChatSendMessageGroupModel.h"

#endif /* ChatSendModelHeader_h */
