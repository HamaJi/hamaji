//
//  ChatSendModel.h
//  Bet365
//
//  Created by HHH on 2018/10/21.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIViewController.h>
#import "Message.h"
#if UIKIT_STRING_ENUMS
typedef NSString ChatSendDestination NS_EXTENSIBLE_STRING_ENUM;
#else
typedef NSString ChatSendDestination;
#endif

FOUNDATION_EXTERN ChatSendDestination *const kChatSendCollarRedPage;

FOUNDATION_EXTERN ChatSendDestination *const kChatSendRequestJoinGroup;

FOUNDATION_EXTERN ChatSendDestination *const kChatSendMessageTextGroup;

FOUNDATION_EXTERN ChatSendDestination *const kChatSendMessageTextPrivate;

FOUNDATION_EXTERN ChatSendDestination *const kChatSendRequestHistoryGroup;

FOUNDATION_EXTERN ChatSendDestination *const kChatSendRequestHistoryPrivate;

FOUNDATION_EXTERN ChatSendDestination *const kChatSendRequestRevokePrivate;

FOUNDATION_EXTERN ChatSendDestination *const kChatSendRequestReadPrivate;

FOUNDATION_EXTERN ChatSendDestination *const kChatSendRequestUserInfoGroup;

FOUNDATION_EXTERN ChatSendDestination *const kChatSendRequestUserInfoPrivate;

FOUNDATION_EXTERN ChatSendDestination *const kChatSendRequestImageKey;

@interface ChatSendModel : NSObject

@property (nonatomic,strong) ChatSendDestination *destination;

@property (nonatomic,strong)NSDate *date;

@property (nonatomic,getter=getChatSendId) NSString *chatSendId;

@property (nonatomic,getter=getHeader) NSDictionary *header;

+(instancetype)creatSendModel:(ChatSendDestination *)destination Date:(NSDate *)date;

@end
