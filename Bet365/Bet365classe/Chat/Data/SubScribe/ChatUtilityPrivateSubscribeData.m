//
//  ChatUtilityPrivateSubscribeData.m
//  Bet365
//
//  Created by HHH on 2019/2/2.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatUtilityPrivateSubscribeData.h"

@implementation ChatUtilityPrivateSubscribeData

-(void)dealloc{
    
}

-(void)updateUserLineStatuByMessage:(Message *)message{
    BOOL isOnline = NO;
    if (message.chatType == ChatMessageType_ONLINE) {
        isOnline = YES;
    }else if (message.chatType == ChatMessageType_OFFLINE){
        isOnline = NO;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(updateChatUtilitySubscribeData:isOnline:)]) {
        [self.delegate updateChatUtilitySubscribeData:self isOnline:isOnline];
    }
}

-(BOOL)removeUnreadMeesage:(Message *)message{
    
    BOOL isContain = [super removeUnreadMeesage:message];
    ChatSendRequestPrivateModel *model = [ChatSendRequestPrivateModel creatSendModel:kChatSendRequestReadPrivate Date:[NSDate date]];
    model.messageId = [message.identity integerValue];
    model.userId = [message.userId integerValue];
    [CHAT_UTIL webSocketSendMessageByChatSendModel:model Subscribe:self];
    return isContain;
}


@end
