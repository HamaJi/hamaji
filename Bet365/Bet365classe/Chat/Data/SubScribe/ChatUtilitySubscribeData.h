//
//  ChatUtilitySubscribeData.h
//  Bet365
//
//  Created by HHH on 2018/10/5.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChatSendModel.h"
#import <MTLManagedObjectAdapter/MTLManagedObjectAdapter.h>

#pragma mark - Entity
#import "ChatUserInfoEntity+CoreDataClass.h"
#import "RedPackEntity+CoreDataClass.h"
#import "ChatFollowEntity+CoreDataClass.h"

#define kGroupWelcomMsgIdentity (-9527)

@class ChatUtilitySubscribeData;

@protocol ChatUtilitySubscribeDataDelegate <NSObject>

@required

-(void)updateChatUtilitySubscribeData:(ChatUtilitySubscribeData *)data FilterMessageList:(NSArray <Message *>*)messageList;

-(void)updateChatUtilitySubscribeData:(ChatUtilitySubscribeData *)data UnMessageCount:(NSUInteger)count;

-(void)updateChatUtilitySubscribeData:(ChatUtilitySubscribeData *)data TimeOutMeessage:(ChatSendModel *)model;

@optional

#pragma mark - Private
-(void)updateChatUtilitySubscribeData:(ChatUtilitySubscribeData *)data isOnline:(BOOL)isOnline;

#pragma mark - Group
-(void)updateChatUtilitySubscribeData:(ChatUtilitySubscribeData *)data UpdateUserOnlineCount:(NSInteger)userOnlineCount;

@end

@interface ChatUtilitySubscribeData : NSObject

/**
 群聊为房间id，私聊为用户id
 */
@property (assign,nonatomic)NSInteger identifier;

@property (nonatomic,getter = getMessageList) NSArray <Message *>*messageList;

@property (nonatomic,getter = getSendList) NSArray <ChatSendModel *>*sendingList;

@property (strong,nonatomic)STOMPSubscription *defaultSubscription;

@property (weak,nonatomic)id<ChatUtilitySubscribeDataDelegate> delegate;
    
@property(assign,readonly,nonatomic)NSInteger unreadCount;


#pragma mark - Message
-(void)manualUpdateFilter;

-(void)addMessage:(Message *)message;

-(void)addMessagesFromArray:(NSArray <Message *>*)otherArray;

-(void)replaceMessage:(Message *)message BySendModel:(ChatSendModel *)sendModel;

-(void)removeMessage:(Message *)message;

-(void)removeMessages:(NSArray <Message *>*)messages;

-(void)removeAllMessage;

-(void)removeMessageByPredicate:(NSPredicate *)predicate;

#pragma mark - ChatSendModel
-(void)addSendModel:(ChatSendModel *)sendModel;

-(void)removeSendModel:(ChatSendModel *)sendModel;

-(void)removeSendModels:(NSArray <ChatSendModel *>*)models;

-(ChatSendModel *)getSendingListObjectBy:(NSString *)sendId;

-(void)removeAllSendModel;

-(void)removeSendModelByPredicate:(NSPredicate *)predicate;

-(BOOL)messageListContain:(Message *)message;

-(Message *)getLastMessage;
#pragma mark - Unread

-(void)addUnreadMessageOnlyCount:(NSUInteger)count;

-(void)addUnreadMessage:(Message *)message;

-(BOOL)removeUnreadMeesage:(Message *)message;

-(void)removeAllUnredaMessage;

#pragma mark - Cycle
-(void)resumeSendTime;

-(void)pauseSendTime;

-(void)starSendTime;

-(void)stopSendTime;

#pragma mark - Follow
-(ChatFollowEntity * )getFollowEntityByFollowId:(NSInteger)followId inContext:(NSManagedObjectContext *)context;

-(NSArray <ChatFollowEntity *>*)getFollowEntityListInContext:(NSManagedObjectContext *)context;

-(BOOL)setFollowEntityByFollowId:(NSInteger)followId inContext:(NSManagedObjectContext *)context;

#pragma mark - UserInfo
+(NSArray <ChatUserInfoEntity *>*)getUserInfoListEntityByUserId:(NSInteger)userId;
@end
