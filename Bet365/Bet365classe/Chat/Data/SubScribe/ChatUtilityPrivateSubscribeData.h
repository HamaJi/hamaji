//
//  ChatUtilityPrivateSubscribeData.h
//  Bet365
//
//  Created by HHH on 2019/2/2.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatUtilitySubscribeData.h"

@interface ChatUtilityPrivateSubscribeData : ChatUtilitySubscribeData

@property (nonatomic,strong)ChatUserInfoModel *userModel;

-(void)updateUserLineStatuByMessage:(Message *)message;

@end
