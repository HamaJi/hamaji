//
//  ChatUtilityGroupSubscribeData.h
//  Bet365
//
//  Created by HHH on 2019/2/2.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatUtilitySubscribeData.h"
#import "ChatSendModel.h"

@interface ChatUtilityGroupSubscribeData : ChatUtilitySubscribeData

@property (assign,nonatomic) BOOL hasSubmitPsw;

@property (strong,nonatomic)ChatRuleModel *ruleModel;

@property (nonatomic,getter=getUserList,setter=setUserList:)NSArray <ChatUserInfoModel *>*userList;

@property (strong,nonatomic)ChatRoomInfoModel *roomInfoModel;

@property (nonatomic,assign)NSInteger onlineCount;

-(ChatUserInfoModel *)getUserInfoModelByUserId:(NSInteger)userId;

-(void)updateUserInfoModel:(ChatUserInfoModel *)model;

-(void)updateUserInfoModels:(NSArray <ChatUserInfoModel *>*)models;

@end
