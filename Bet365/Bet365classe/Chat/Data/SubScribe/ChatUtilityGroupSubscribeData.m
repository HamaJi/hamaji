//
//  ChatUtilityGroupSubscribeData.m
//  Bet365
//
//  Created by HHH on 2019/2/2.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatUtilityGroupSubscribeData.h"


@interface ChatUtilityGroupSubscribeData()

@property (nonatomic,strong) NSMutableDictionary *kFilterUserMap;

@end

@implementation ChatUtilityGroupSubscribeData

-(void)dealloc{
    self.kFilterUserMap = nil;
}

-(instancetype)init{
    if (self = [super init]) {

    }
    return self;
}

-(ChatUserInfoModel *)getUserInfoModelByUserId:(NSInteger)userId{
    ChatUserInfoModel *model = [self.kFilterUserMap objectForKey:[NSString stringWithFormat:@"%li",userId]];
    if (!model) {
        ChatUserInfoEntity *entity = [ChatUtilityGroupSubscribeData getUserInfoListEntityByUserId:userId].firstObject;
        if (entity) {
            model = [MTLManagedObjectAdapter modelOfClass:[ChatUserInfoModel class] fromManagedObject:entity error:nil];
        }
    }
    return model;
}

-(void)updateUserInfoModel:(ChatUserInfoModel *)model{
    
    if (!model || model.userId == -1) {
        return;
    }
    [self.kFilterUserMap safeSetObject:model forKey:[NSString stringWithFormat:@"%li",model.userId]];
    if (self.delegate && [self.delegate respondsToSelector:@selector(updateChatUtilitySubscribeData:UpdateUserOnlineCount:)]) {
        [self.delegate updateChatUtilitySubscribeData:self UpdateUserOnlineCount:self.onlineCount];
    }
    [self addUserInfoEntitys:@[model]];
}

-(void)updateUserInfoModels:(NSArray <ChatUserInfoModel *>*)models{
    if (!models.count) {
        return;
    }

    
    NSMutableDictionary *dic = @{}.mutableCopy;
    [models enumerateObjectsUsingBlock:^(ChatUserInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.userId != -1) {
            [dic safeSetObject:obj forKey:[NSString stringWithFormat:@"%li",obj.userId]];
        }
    }];
    [self.kFilterUserMap addEntriesFromDictionary:dic];
    
    if (![self getUserInfoModelByUserId:0]) {
        ChatUserInfoModel *symUserInfo = [ChatUserInfoModel new];
        symUserInfo.avatar = @"/images/chat/girl/011.jpg";
        symUserInfo.nickName = @"系统消息";
        symUserInfo.userId = 0;
        [self addUserInfoEntity:symUserInfo];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(updateChatUtilitySubscribeData:UpdateUserOnlineCount:)]) {
        [self.delegate updateChatUtilitySubscribeData:self UpdateUserOnlineCount:self.onlineCount];
    }
    [self addUserInfoEntitys:dic.allValues];
    if (![self getUserInfoModelByUserId:0]) {
        ChatUserInfoModel *symUserInfo = [ChatUserInfoModel new];
        symUserInfo.avatar = @"/images/chat/girl/011.jpg";
        symUserInfo.nickName = @"系统消息";
        symUserInfo.userId = 0;
        [self addUserInfoEntity:symUserInfo];
    }
}

-(void)addUserInfoEntity:(ChatUserInfoModel *)userInfo{
    
    if (userInfo.userId == 0) {
        
    }
    NSManagedObjectContext *context = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_defaultContext]];;
    NSError *error;
    ChatUserInfoEntity *entity = [MTLManagedObjectAdapter managedObjectFromModel:userInfo insertingIntoContext:context error:&error];
    if (!error && entity) {
        [context MR_saveToPersistentStoreAndWait];
    }
    
}

-(void)addUserInfoEntitys:(NSArray <ChatUserInfoModel *>*)userInfos{
    [userInfos enumerateObjectsUsingBlock:^(ChatUserInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [self addUserInfoEntity:obj];
    }];
}

-(NSInteger)onlineCount{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = 1"];
    NSArray *userList = self.kFilterUserMap.allValues.mutableCopy;
    NSArray *list = [userList filteredArrayUsingPredicate:predicate];
    _onlineCount = list.count;
    return _onlineCount;
}

-(NSArray<ChatUserInfoModel *> *)getUserList{
    return self.kFilterUserMap.allValues;
}

-(void)setUserList:(NSArray<ChatUserInfoModel *> *)userList{
    [self updateUserInfoModels:userList];
}

-(NSMutableDictionary *)kFilterUserMap{
    if (!_kFilterUserMap) {
        _kFilterUserMap = [[NSMutableDictionary alloc] init];
        [[ChatUserInfoEntity MR_findAll] enumerateObjectsUsingBlock:^(__kindof NSManagedObject * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ChatUserInfoModel *model = [MTLManagedObjectAdapter modelOfClass:[ChatUserInfoModel class] fromManagedObject:obj error:nil];
            [_kFilterUserMap safeSetObject:model forKey:[NSString stringWithFormat:@"%li",model.userId]];
        }];
        
    }
    return _kFilterUserMap;
}
@end
