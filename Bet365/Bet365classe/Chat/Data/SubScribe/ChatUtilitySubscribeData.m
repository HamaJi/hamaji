//
//  ChatUtilitySubscribeData.m
//  Bet365
//
//  Created by HHH on 2018/10/5.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatUtilitySubscribeData.h"
#import<pthread.h>
typedef NS_ENUM(NSInteger, ChatSendCycleStatus) {
    /** 进行中*/
    ChatSendCycleStatus_ING = 0,
    /** 暂停*/
    ChatSendCycleStatus_PAUSE,
    /** 停止*/
    ChatSendCycleStatus_STOP,
};


@interface ChatUtilitySubscribeData()
{
    pthread_mutex_t _messageListMutex;
    
    pthread_mutex_t _unreadMessageListMutex;
}

@property(nonatomic,strong)NSMutableArray <Message *>*kMessageList;

@property (nonatomic,strong)NSMutableArray <ChatSendModel *>*kSendingList;

@property (nonatomic,assign)ChatSendCycleStatus sendCycleStatus;

@property (nonatomic,strong)NSMutableArray <Message *>*unreadMessageList;

@property (nonatomic,strong)RACSubject *updateFilterSubject;


@end

@implementation ChatUtilitySubscribeData


-(instancetype)init{
    if (self = [super init]) {
        
        pthread_mutexattr_t msgAttr;
        pthread_mutexattr_init(&msgAttr);
        pthread_mutexattr_settype(&msgAttr, PTHREAD_MUTEX_DEFAULT);
        pthread_mutex_init(&_messageListMutex, &msgAttr);
        pthread_mutexattr_destroy(&msgAttr);
        
        pthread_mutexattr_t umsgAttr;
        pthread_mutexattr_init(&umsgAttr);
        pthread_mutexattr_settype(&umsgAttr, PTHREAD_MUTEX_DEFAULT);
        pthread_mutex_init(&_unreadMessageListMutex, &umsgAttr);
        pthread_mutexattr_destroy(&umsgAttr);
        
        
        
        /** 未读数 **/
        [self unreadMessageList];
        [self addObserver:self forKeyPath:@"unreadMessageList" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:nil];
        
        @weakify(self);
        [self.updateFilterSubject subscribeNext:^(id  _Nullable x) {
            @strongify(self);
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
                [self.kMessageList sortUsingComparator:^NSComparisonResult(Message  *_Nonnull message1, Message * _Nonnull message2) {

                    NSInteger numerb1 = [message1.curTime timeIntervalSince1970];
                    NSInteger number2 = [message2.curTime timeIntervalSince1970];

                    if (message1.identity && message2.identity) {
                        numerb1 = [message1.identity integerValue];
                        number2 = [message2.identity integerValue];
                    }
                    return numerb1 > number2; // 升序
                }];
                NSMutableArray *filterMessageList = [self filterMessageListBy:self.kMessageList inContext:context];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (self.delegate && [self.delegate respondsToSelector:@selector(updateChatUtilitySubscribeData:FilterMessageList:)]) {
                        [self.delegate updateChatUtilitySubscribeData:self FilterMessageList:filterMessageList];
                    }
                });
            });
        }];
        
        //蛤蟆吉 TODO
//        [self addObserver:self forKeyPath:@"kMessageList" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:NULL];
        
        [[[RACSignal interval:1 onScheduler:[RACScheduler schedulerWithPriority:RACSchedulerPriorityBackground name:@"CHAT_SENDCYLE_QUEUE"]] takeUntil:self.rac_willDeallocSignal ] subscribeNext:^(id x) {
            @strongify(self);
            if (self.sendCycleStatus == ChatSendCycleStatus_ING) {
                NSDate *date = [NSDate date];
                [self.kSendingList enumerateObjectsUsingBlock:^(ChatSendModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {

                    if ([obj isKindOfClass:[ChatSendMessageModel class]] && [date timeIntervalSinceDate:obj.date] > 120) {

                        if (((ChatSendMessageModel *)obj).sendStatus == ChatMessageSendStatus_SENDING) {
                            ((ChatSendMessageModel *)obj).sendStatus = ChatMessageSendStatus_FAIL;
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (self.delegate && [self.delegate respondsToSelector:@selector(updateChatUtilitySubscribeData:TimeOutMeessage:)]) {
                                    [self.delegate updateChatUtilitySubscribeData:self TimeOutMeessage:obj];
                                }
                            });
                        }
                    }
                }];
            }
        }];
    }
    return self;
}
-(void)dealloc{
    pthread_mutex_destroy(&_messageListMutex);
    pthread_mutex_destroy(&_unreadMessageListMutex);
    [self removeObserver:self forKeyPath:@"unreadMessageList"];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"unreadMessageList"]){
        _unreadCount = self.unreadMessageList.count;
        if (self.delegate && [self.delegate respondsToSelector:@selector(updateChatUtilitySubscribeData:UnMessageCount:)]) {
            [self.delegate updateChatUtilitySubscribeData:self UnMessageCount:self.unreadCount];
        }
    }
}


#pragma mark - Message

-(void)manualUpdateFilter{
    [self.updateFilterSubject sendNext:@"Update"];
}
-(void)addMessage:(Message *)message{
    if (message && message.chatType != ChatMessageType_NONE) {
        [self addMessagesFromArray:@[message]] ;
    }
}

-(void)addMessagesFromArray:(NSArray <Message *>*)otherArray{
    if (!otherArray.count) {
        return;
    }
    
    __block NSMutableArray *mList = [[NSMutableArray alloc] initWithArray:otherArray];
    __block NSMutableArray *removeList = @[].mutableCopy;
    
    [self.kMessageList enumerateObjectsUsingBlock:^(Message * _Nonnull dataMessage, NSUInteger dataIdx, BOOL * _Nonnull dataStop) {
        [otherArray enumerateObjectsUsingBlock:^(Message * _Nonnull message, NSUInteger idx, BOOL * _Nonnull stop) {
            if (message.chatType == ChatMessageType_KICK ||
                (message.chatType == ChatMessageType_REMOVE && message.chatFrom != ChatMessageFrom_ME) ||
                message.chatType == ChatMessageType_UNSPEACK ||
                message.chatType == ChatMessageType_UNIMAGE ||
                message.chatType == ChatMessageType_WIN ||
                message.chatType == ChatMessageType_RULE ||
                message.chatType == ChatMessageType_REDCOLLOAR_ERROR ||
                (message.chatType == ChatMessageType_REDCOLLOAR && CHAT_UTIL.suspendModel.isShowHbHistory && ![CHAT_UTIL.suspendModel.isShowHbHistory boolValue]) ||
                message.chatType == ChatMessageType_LEAVE ||
                ([message.identity integerValue] == [dataMessage.identity integerValue] &&
                 [message.identity integerValue] == kGroupWelcomMsgIdentity) ||
                (message.chatType == ChatMessageType_JOIN && [message.content.showVipEnter boolValue])) {
                [mList removeObject:message];
            }
            if ([message.identity integerValue] != 0 &&
                [dataMessage.identity integerValue] != 0 &&
                [message.identity integerValue] == [dataMessage.identity integerValue]) {
                [removeList safeAddObject:dataMessage];
            }else if(message.content.sendId.length &&
                     dataMessage.content.sendId.length &&
                     [message.content.sendId isEqualToString:dataMessage.content.sendId] &&
                     message.chatType == dataMessage.chatType){
                [removeList addObject:dataMessage];
            }
            //            else if (BET_CONFIG.userSettingData.chatFollowStatus == ChatFollowStatus_ONLY && )
        }];
    }];
    
    [removeList enumerateObjectsUsingBlock:^(Message *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [[self mutableArrayValueForKey:@"kMessageList"] removeObject:obj];
    }];
    [[self mutableArrayValueForKey:@"kMessageList"] addObjectsFromArray:mList];
    if (mList.count || removeList.count) {
        [self.updateFilterSubject sendNext:@(0)];
    }
    
//    kGroupWelcomMsgIdentity
}

-(void)replaceMessage:(Message *)message BySendModel:(ChatSendModel *)sendModel{
    
    __block BOOL success = NO;
    [self.kMessageList enumerateObjectsUsingBlock:^(Message * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.content.sendId isEqualToString:sendModel.chatSendId] &&
            obj.chatType != ChatMessageType_SYSTEM) {
            message.content.pImageData = obj.content.pImageData;
            [[self mutableArrayValueForKey:@"kMessageList"] replaceObjectAtIndex:idx withObject:message];
            success = YES;
            *stop = YES;
        }
    }];
    if (success) {
        [self.updateFilterSubject sendNext:@(0)];
    }
}

-(void)removeMessage:(Message *)message{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        pthread_mutex_lock(&_messageListMutex);
        if ([self.kMessageList containsObject:message]) {
            [[self mutableArrayValueForKey:@"kMessageList"] removeObject:message];
            [self.updateFilterSubject sendNext:@(0)];
        }else if(message.content.value){
            [self removeMessageByPredicate:[NSPredicate predicateWithFormat:@"(identity = %li)",[message.content.value integerValue]]];
        }else{
            [self removeMessageByPredicate:[NSPredicate predicateWithFormat:@"(identity = %li) OR ((content.sendId = %@) AND (identity = 0))",[message.identity integerValue],message.content.sendId]];
        }
        pthread_mutex_unlock(&_messageListMutex);
    });
    
}

-(void)removeMessages:(NSArray <Message *>*)messages{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [messages enumerateObjectsUsingBlock:^(Message * _Nonnull message, NSUInteger idx, BOOL * _Nonnull stop) {
            pthread_mutex_lock(&_messageListMutex);
            if ([self.kMessageList containsObject:message]) {
                [[self mutableArrayValueForKey:@"kMessageList"] removeObject:message];
                [self.updateFilterSubject sendNext:@(0)];
            }else if(message.content.value){
                [self removeMessageByPredicate:[NSPredicate predicateWithFormat:@"(identity = %li)",[message.content.value integerValue]]];
            }else{
                [self removeMessageByPredicate:[NSPredicate predicateWithFormat:@"(identity = %li) OR ((content.sendId = %@) AND (identity = 0))",[message.identity integerValue],message.content.sendId]];
            }
            pthread_mutex_unlock(&_messageListMutex);
        }];
    });
    
    
}

-(void)removeAllMessage{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        pthread_mutex_lock(&_messageListMutex);
        [[self mutableArrayValueForKey:@"kMessageList"] removeAllObjects];
        [self.updateFilterSubject sendNext:@(0)];
        pthread_mutex_unlock(&_messageListMutex);
    });
    
}

-(void)removeMessageByPredicate:(NSPredicate *)predicate{
    NSMutableArray *removeList = [self.kMessageList filteredArrayUsingPredicate:predicate].mutableCopy;
    if (removeList.count) {
        [[self mutableArrayValueForKey:@"kMessageList"] removeObjectsInArray:removeList];
        [self.updateFilterSubject sendNext:@(0)];
    }
}

-(BOOL)messageListContain:(Message *)message{
    if ([self.kMessageList containsObject:message]){
        return YES;
    }else{
        ;
        NSArray *containList = [self.kMessageList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(identity = %li) OR (content.sendId = %@)",[message.identity integerValue],message.content.sendId]];
        return containList.count > 0 ? YES : NO;
    }
    return NO;
}

-(Message *)getLastMessage{
    return [self.messageList lastObject];
}
#pragma mark - ChatSendModel
-(void)addSendModel:(ChatSendModel *)sendModel{
    if ([self.kSendingList containsObject:sendModel]) {
        return;
    }
    
    if ([sendModel.destination isEqualToString:kChatSendCollarRedPage]) {
        return;
    }
    
    NSArray *containsList = [self.kSendingList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"chatSendId = %li",sendModel.chatSendId]];
    if (containsList.count) {
        return;
    }
    [[self mutableArrayValueForKey:@"kSendingList"] addObject:sendModel];
}

-(ChatSendModel *)getSendingListObjectBy:(NSString *)sendId{
    __block ChatSendModel *model = nil;
    [self.kSendingList enumerateObjectsUsingBlock:^(ChatSendModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.chatSendId isEqualToString:sendId]) {
            model = obj;
            *stop = YES;
        }
    }];
//    ChatSendModel *model = [[self.kSendingList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"chatSendId = %li",sendId]] safeObjectAtIndex:0];
    return model;
}

-(void)removeSendModel:(ChatSendModel *)sendModel{
    if (!sendModel) {
        return;
    }
    if ([self.kSendingList containsObject:sendModel]) {
        [[self mutableArrayValueForKey:@"kSendingList"] removeObject:sendModel];
    }else{
        [self removeSendModelByPredicate:[NSPredicate predicateWithFormat:@"chatSendId = %li",sendModel.chatSendId]];
    }
}

-(void)removeSendModels:(NSArray <ChatSendModel *>*)models{
    [models enumerateObjectsUsingBlock:^(ChatSendModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [self removeSendModel:obj];
    }];
}

-(void)removeAllSendModel{
    [self.kSendingList removeAllObjects];
}

-(void)removeSendModelByPredicate:(NSPredicate *)predicate{
    
    NSArray *removeList = [self.kSendingList filteredArrayUsingPredicate:predicate];
    if (removeList.count) {
        [[self mutableArrayValueForKey:@"kSendingList"] removeObjectsInArray:removeList];
    }
}

-(void)addUnreadMessageOnlyCount:(NSUInteger)count{
    if (!_unreadCount) {
        _unreadCount += count;
    }
}
-(void)addUnreadMessage:(Message *)message{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        pthread_mutex_lock(&_unreadMessageListMutex);
        if (message.chatFrom != ChatMessageFrom_ME &&
            (message.chatType == ChatMessageType_TEXT ||
             message.chatType == ChatMessageType_IMG ||
             message.chatType == ChatMessageType_LOTTERY ||
             //         message.chatType == ChatMessageType_JOIN ||
             message.chatType == ChatMessageType_RED)) {
                if ([self isKindOfClass:[ChatUtilityGroupSubscribeData class]]) {
                    NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
                    NSArray *entityList = [self getFollowEntityListInContext:context];
                    if (BET_CONFIG.userSettingData.chatFollowStatus == ChatFollowStatus_ONLY) {
                        if (entityList.count) {
                            [entityList enumerateObjectsUsingBlock:^(ChatFollowEntity * _Nonnull entity, NSUInteger entityIdx, BOOL * _Nonnull entityStop) {
                                if (entity.followUserID == [message.userId integerValue]) {
                                    [[self mutableArrayValueForKey:@"unreadMessageList"] addObject:message];
                                    if ([self isKindOfClass:[ChatUtilityPrivateSubscribeData class]]) {
                                        [CHAT_UTIL updateAllUnreadMessageCount:self.unreadCount FromePrivateSubscribeData:(ChatUtilityPrivateSubscribeData *)self];
                                    }
                                }
                            }];
                        }
                    }else{
                        [[self mutableArrayValueForKey:@"unreadMessageList"] addObject:message];
                    }
                }else if([self isKindOfClass:[ChatUtilityPrivateSubscribeData class]]){
                    [[self mutableArrayValueForKey:@"unreadMessageList"] addObject:message];
                    if ([self isKindOfClass:[ChatUtilityPrivateSubscribeData class]]) {
                        [CHAT_UTIL updateAllUnreadMessageCount:self.unreadCount FromePrivateSubscribeData:(ChatUtilityPrivateSubscribeData *)self];
                    }
                }
                
            }
        pthread_mutex_unlock(&_unreadMessageListMutex);
    });
    NSLog(@"message.chatFrom = %li , message.chatType = %li",message.chatFrom,message.chatType);
    
}

-(BOOL)removeUnreadMeesage:(Message *)message{
    pthread_mutex_lock(&_unreadMessageListMutex);
    BOOL isSurplus = NO;
    if (_unreadCount > self.unreadMessageList.count) {
        _unreadCount = self.unreadMessageList.count;
        isSurplus = YES;
    }
    
    __block BOOL isContain = NO;
    [self.unreadMessageList enumerateObjectsUsingBlock:^(Message * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.identity integerValue] == [message.identity integerValue]) {
            [[self mutableArrayValueForKey:@"unreadMessageList"] removeObjectAtIndex:idx];
            isContain = YES;
            *stop = YES;
        }else if ([obj.identity integerValue] == [message.content.value integerValue]){
            [[self mutableArrayValueForKey:@"unreadMessageList"] removeObjectAtIndex:idx];
            isContain = YES;
            *stop = YES;
        }
    }];
    
    
    
    if ((isContain || isSurplus) && [self isKindOfClass:[ChatUtilityPrivateSubscribeData class]]) {
        [CHAT_UTIL updateAllUnreadMessageCount:self.unreadCount FromePrivateSubscribeData:(ChatUtilityPrivateSubscribeData *)self];
    }
    pthread_mutex_unlock(&_unreadMessageListMutex);
    return isContain;
}

-(void)removeAllUnredaMessage{
    
    pthread_mutex_lock(&_unreadMessageListMutex);
    if (_unreadCount > self.unreadMessageList.count) {
        _unreadCount = self.unreadMessageList.count;
    }
    [[self mutableArrayValueForKey:@"unreadMessageList"] removeAllObjects];
    if ([self isKindOfClass:[ChatUtilityPrivateSubscribeData class]]) {
        [CHAT_UTIL updateAllUnreadMessageCount:self.unreadCount FromePrivateSubscribeData:(ChatUtilityPrivateSubscribeData *)self];
    }
    pthread_mutex_unlock(&_unreadMessageListMutex);

}

-(void)resumeSendTime{
    if (self.sendCycleStatus != ChatSendCycleStatus_PAUSE) {
        return;
    }
    self.sendCycleStatus = ChatSendCycleStatus_ING;
}

-(void)pauseSendTime{
    if (self.sendCycleStatus != ChatSendCycleStatus_ING) {
        return;
    }
    self.sendCycleStatus = ChatSendCycleStatus_PAUSE;
}

-(void)starSendTime{
    if (self.sendCycleStatus == ChatSendCycleStatus_ING) {
        return;
    }
    self.sendCycleStatus = ChatSendCycleStatus_ING;
}

-(void)stopSendTime{
    if (self.sendCycleStatus != ChatSendCycleStatus_ING) {
        return;
    }
    self.sendCycleStatus = ChatSendCycleStatus_STOP;
}

-(NSMutableArray *)filterMessageListBy:(NSMutableArray <Message *>*)totoalMessageList inContext:(NSManagedObjectContext *)context{
    NSMutableArray *filterMessageList = @[].mutableCopy;
    NSArray *followEntityList = [self getFollowEntityListInContext:context];
    if (BET_CONFIG.userSettingData.chatFollowStatus == ChatFollowStatus_ONLY) {
        [totoalMessageList enumerateObjectsUsingBlock:^(Message * _Nonnull message, NSUInteger messageIdx, BOOL * _Nonnull messageStop) {
            ChatUserInfoEntity *infoEntity= [ChatUtilitySubscribeData getUserInfoListEntityByUserId:[message.userId integerValue]].firstObject;
            if (message.chatFrom == ChatMessageFrom_ME || message.chatFrom == ChatMessageFrom_SYS) {
                /** 自己或系统的消息**/
                [filterMessageList safeAddObject:message];
            }
            /*
             * 禅道11706
             */
//            else if ([infoEntity.level integerValue] >= 100){
//                /** 会员等级>=100的消息**/
//                [filterMessageList safeAddObject:message];
//            }
            else if(message.content.isTopping){
                /** 置顶消息 **/
                [filterMessageList safeAddObject:message];
            }else{
                /** 他人消息,切在关注列表**/
                [followEntityList enumerateObjectsUsingBlock:^(ChatFollowEntity * _Nonnull entity, NSUInteger entityIdx, BOOL * _Nonnull entityStop) {
                    if (entity.followUserID == [message.userId integerValue] &&
                        message.chatFrom == ChatMessageFrom_OTHER) {
                        [filterMessageList safeAddObject:message];
                        *entityStop = YES;
                    }
                    
                }];
            }
        }];
    }else{
        [filterMessageList safeAddObjectsFromArray:totoalMessageList];
    }
    [filterMessageList sortUsingComparator:^NSComparisonResult(Message  *_Nonnull message1, Message * _Nonnull message2) {
        
        NSInteger numerb1 = [message1.curTime timeIntervalSince1970];
        NSInteger number2 = [message2.curTime timeIntervalSince1970];

        
//        if (message1.identity && message2.identity) {
//            numerb1 = [message1.identity integerValue];
//            number2 = [message2.identity integerValue];
//
//            if (numerb1 == -9527) {
//                return numerb1 < number2;
//            }
//
//            if (number2 == -9527) {
//                return numerb1 > number2;
//            }
//        }
        
        return numerb1 > number2; // 升序
    }];
    
    return filterMessageList;
}

+(NSArray <ChatUserInfoEntity *>*)getUserInfoListEntityByUserId:(NSInteger)userId{
    NSString *predicateStr = [NSString stringWithFormat:@"userId = %li",userId];
    NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
    NSArray *entityList = [ChatUserInfoEntity MR_findAllWithPredicate:[NSPredicate predicateWithFormat:predicateStr] inContext:context];
    return entityList;
}

#pragma mark - Follow
-(ChatFollowEntity * )getFollowEntityByFollowId:(NSInteger)followId inContext:(NSManagedObjectContext *)context{
    NSString *predicateStr = [NSString stringWithFormat:@"(userID = %li) AND (followUserID = %li) AND (roomID = %li)",CHAT_UTIL.clientInfo.userInfo.userId,followId,self.identifier];
    ChatFollowEntity *entity = [ChatFollowEntity MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:predicateStr] inContext:context];
    return entity;
}

-(NSArray <ChatFollowEntity *>*)getFollowEntityListInContext:(NSManagedObjectContext *)context {
    NSString *predicateStr = [NSString stringWithFormat:@"(userID = %li) AND (roomID = %li)",CHAT_UTIL.clientInfo.userInfo.userId,self.identifier];
    NSArray *list = [ChatFollowEntity MR_findAllWithPredicate:[NSPredicate predicateWithFormat:predicateStr] inContext:context];
    return list;
}

-(BOOL)setFollowEntityByFollowId:(NSInteger)followId inContext:(NSManagedObjectContext *)context{
    
    BOOL willFollow = NO;
    
    ChatFollowEntity *entity = [self getFollowEntityByFollowId:followId inContext:context];
    if (entity) {
        [entity MR_deleteEntityInContext:context];
        [context MR_saveToPersistentStoreAndWait];
        willFollow = NO;
    }else{
        entity = [ChatFollowEntity MR_createEntityInContext:context];
        @weakify(self);
        [context MR_saveWithBlockAndWait:^(NSManagedObjectContext * _Nonnull localContext) {
            @strongify(self);
            ChatFollowEntity *localentity = [entity MR_inContext:localContext];
            localentity.userID = CHAT_UTIL.clientInfo.userInfo.userId;
            localentity.followUserID = followId;
            localentity.roomID = self.identifier;
        }];
        willFollow = YES;
    }
    return willFollow;
}

#pragma mark - GET/SET
-(NSMutableArray<Message *> *)kMessageList{
    if (!_kMessageList) {
        _kMessageList = @[].mutableCopy;
    }
    return _kMessageList;
}

-(NSArray *)getMessageList{
    return self.kMessageList;
}

-(NSMutableArray<ChatSendModel *> *)kSendingList{
    if (!_kSendingList) {
        _kSendingList = @[].mutableCopy;
    }
    return _kSendingList;
}

-(NSArray<ChatSendModel *> *)getSendList{
    return self.kSendingList;
}

-(NSMutableArray<Message *> *)unreadMessageList{
    if (!_unreadMessageList) {
        _unreadMessageList = @[].mutableCopy;
    }
    return _unreadMessageList;
}

-(RACSubject *)updateFilterSubject{
    if(!_updateFilterSubject){
        _updateFilterSubject = [RACSubject subject];
    }
    return _updateFilterSubject;
}

@end
