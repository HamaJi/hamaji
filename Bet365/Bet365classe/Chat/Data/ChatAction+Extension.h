//
//  ChatAction+Extension.h
//  Bet365
//
//  Created by HHH on 2018/9/24.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatAction.h"
#import "UploadImgModel.h"
@interface ChatAction ()


/**
 发送领取红包

 @param redPacketId 红包id
 */
-(void)openRedPage:(NSInteger )redPacketId;

/**
 发送文本消息

 @param text @"Hello World"
 */
-(void)sendTextMessage:(NSString *)text;



/**
 点击删除消息

 @param message 消息实体
 */
-(void)tapRemoveMessage:(Message *)message;

-(void)getHistoryMessageListByBehindMessage:(Message *)message;

-(void)reloadTableViewByFileterMessageList;



-(ChatUtilitySubscribeData *)subSrcibeData;

//-(void)topListAction:(id)sender;
//
//-(void)followSortAction:(id)sender;
@end
