//
//  InputToolBarCustomItem.h
//  Bet365
//
//  Created by HHH on 2018/9/17.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InputToolBarCustomItem : NSObject

@property (nonatomic,strong)NSString *titile;

@property (nonatomic,strong)UIImage *img;

@property (nonatomic,assign)SEL selector;

+(instancetype)creatItemTitile:(NSString *)titile ImgName:(NSString *)imgName Sel:(SEL)sel;
@end
