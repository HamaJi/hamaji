//
//  ChatAction.m
//  Bet365
//
//  Created by HHH on 2018/9/16.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatAction.h"
#import "NSString+Emoji.h"
#import "ChatAction+Extension.h"
#import "ChatImageEmojiPickViewController.h"
#import "DismissingBottomAnimator.h"
#import "PresentingBottomAnimator.h"
#import <TZImagePickerController/TZImagePickerController.h>
#import <TZImagePickerController/TZPhotoPickerController.h>
#import "ChatGroupAction.h"
#import "ChatPrivateAction.h"
#define MULITTHREEBYTEUTF16TOUNICODE(x,y) (((((x ^ 0xD800) << 2) | ((y ^ 0xDC00) >> 8)) << 8) | ((y ^ 0xDC00) & 0xFF)) + 0x10000

@interface ChatAction()
{
    Message *_behindMessage;
}

@end

@implementation ChatAction

+(instancetype)creatActionByChannelId:(NSInteger)channelId{
    ChatAction *action = [[[self class] alloc] init];
    action.channelID = channelId;
    return action;
}

-(void)dealloc{
    self.data = nil;
    self.delegate = nil;
    [CHAT_UTIL removeChatUtilityDelegate:self];
    self.tableView.delegate = nil;
    self.inputToolBar.delegate = nil;
    
}

-(instancetype)init{
    if (self = [super init]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.data = [self subSrcibeData];
            self.data.delegate = self;
            [CHAT_UTIL addChatUtilityDelegate:self];
            @weakify(self);
            [[RACObserve(self, filterMessageList) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
                @strongify(self);
                if ([self respondsToSelector:@selector(reloadTableViewByFileterMessageList)]) {
                    [self reloadTableViewByFileterMessageList];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self chatReloadTableView];
                    });
                }
            }];
        });
    }
    return self;
}

#pragma mark - Public
-(void)scrollBottomAnimated:(BOOL)animated{
    if (![[NAVI_MANAGER getCurrentVC] isEqual:self.delegate]) {
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self pri_tableViewReloadData];
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(pri_scrollBottomAnimated:) object:@(animated)];
        [self performSelector:@selector(pri_scrollBottomAnimated:) withObject:@(animated) afterDelay:0.2];
    });

}

-(void)scrolleHadScroll{
    /** 下拉按钮是否展示**/
    ChatCell *cell = [self.tableView visibleCells].lastObject;
    if ([self.filterMessageList indexOfObject:cell.message] != NSNotFound) {
        NSUInteger idx = [self.filterMessageList indexOfObject:cell.message];
        if (self.filterMessageList.count == 0) {
            self.bottomBtn.hidden = YES;
        }else{
            self.bottomBtn.hidden = !(self.filterMessageList.count - idx > 2);
        }
    }
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    if (menuController.isMenuVisible) {
        [menuController setMenuVisible:NO animated:YES];
    }
}

-(void)cancleAllRequestTask{
    
}

-(void)resignAllFirstResponder{
    UIViewController *delegeteVc =(UIViewController *)self.delegate;
    [delegeteVc.view endEditing:YES];
    [self.inputToolBar resignFirstResponder];
    
}
-(void)chatReloadTableView{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(pri_tableViewReloadData) afterDelay:0.2f];
}

#pragma mark - Private
-(void)pri_scrollBottomAnimated:(BOOL)animated{
    NSInteger row = [self.tableView numberOfRowsInSection:0] - 1;
    if (row >= 0) {
        self.tableView.hidden = YES;
        [self.tableView scrollToRow:row inSection:0 atScrollPosition:UITableViewScrollPositionBottom animated:animated];
        self.tableView.hidden = NO;
    }
}

-(void)pri_tableViewReloadData{
    self.tableView.hidden = YES;
    [self.tableView reloadData];
    self.tableView.hidden = NO;
}

-(void)pri_subscribeData{
    self.data = [self subSrcibeData];
    self.data.delegate = self;
}
#pragma mark - Overralate


#pragma mark - ChatUtilitySubscribeDataDelegate
-(void)updateChatUtilitySubscribeData:(ChatUtilitySubscribeData *)data UnMessageCount:(NSUInteger)count{
    self.bottomBtn.unreadCount = count;
}

-(void)updateChatUtilitySubscribeData:(ChatUtilitySubscribeData *)data TimeOutMeessage:(ChatSendModel *)model{
    __block Message *replaceMessage = nil;
    [self.data.messageList enumerateObjectsUsingBlock:^(Message * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([model.chatSendId isEqualToString:obj.content.sendId]) {
            obj.sendStatus = ChatMessageSendStatus_FAIL;
            replaceMessage = obj;
            *stop = YES;
        }
    }];
    [self ChatUilityScribeData:self.data ReplaceMessage:replaceMessage];
}

-(void)updateChatUtilitySubscribeData:(ChatUtilitySubscribeData *)data FilterMessageList:(NSArray <Message *>*)messageList{
    if ([self.filterMessageList isEqualToArray:messageList]) {
        return;
    }
    self.filterMessageList = messageList.mutableCopy;
    [self chatReloadTableView];
}

#pragma mark - UITableViewDataSource
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    Message *messge = [self.filterMessageList safeObjectAtIndex:0];
    if (messge.kHeight) {
        return messge.kHeight;
    }
    return 60;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    Message *messge = [self.filterMessageList safeObjectAtIndex:0];
    messge.kHeight = cell.frame.size.height;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.filterMessageList.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Message *message = [self.filterMessageList safeObjectAtIndex:indexPath.row];
    NSString *identity = cellIdentity(message);
    ChatCell *cell = [tableView dequeueReusableCellWithIdentifier:identity];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:identity owner:self options:nil].firstObject;
    }
    cell.delegate = self;
    ChatUserInfoEntity *entity = [[ChatUtilitySubscribeData getUserInfoListEntityByUserId:[message.userId integerValue]] safeObjectAtIndex:0];
    cell.userInfoEntity = entity;
    cell.message = message;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell layoutIfNeeded];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    if (menuController.isMenuVisible) {
        [menuController setMenuVisible:NO animated:YES];
    }
    [self resignAllFirstResponder];
}

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if ([scrollView isEqual:self.tableView]) {
        [self scrolleHadScroll];
    }
    
}

#pragma mark - ChatUtilityDataDelegate
- (void)ChatUilityConnectedSuccess{
    [SVProgressHUD dismiss];
    if ([self respondsToSelector:@selector(subSrcibeData)]) {
        self.data = [self subSrcibeData];
        self.data.delegate = self;

//        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(pri_subscribeData) object:nil];
//        [self performSelector:@selector(pri_subscribeData) withObject:nil afterDelay:1.0];
    }
}



- (void)ChatUilityScribeData:(ChatUtilitySubscribeData *)data InsertMessage:(Message*)message{
    if (data != self.data) {
        return;
    }
    if (message.chatFrom == ChatMessageFrom_ME &&
        [[NSString stringWithFormat:@"%li",self.redOpenViewController.model.activityChatRedpacket.identity] isEqualToString:message.content.redCollarPacketId]) {
        [CHAT_UTIL getRedDetailByMessage:message Completed:^(ChatRedPacketModel *model) {
            
            if (message.chatFrom == ChatMessageFrom_ME &&
                [[NSString stringWithFormat:@"%li",self.redOpenViewController.model.activityChatRedpacket.identity] isEqualToString:message.content.redCollarPacketId]) {
                model.activityChatRedpacket.createrName = self.redOpenViewController.model.activityChatRedpacket.createrName;
                model.activityChatRedpacket.createrId = self.redOpenViewController.model.activityChatRedpacket.createrId;
                model.activityChatRedpacket.createrAvator = self.redOpenViewController.model.activityChatRedpacket.createrAvator;
                [self showRedResults:model];
            }
        }];
    }
    [self chatReloadTableView];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (message.chatFrom == ChatMessageFrom_ME ||
            self.bottomBtn.isHidden) {
            [self scrollBottomAnimated:YES];
        }
    });

}

- (void)ChatUilityScribeData:(ChatUtilitySubscribeData *)data RemoveMessage:(Message*)message{
    
    if (data != self.data) {
        return;
    }
    [self pri_tableViewReloadData];
}

- (void)ChatUilityScribeData:(ChatUtilitySubscribeData *)data ReplaceMessage:(Message *)message{
    
    if (data != self.data) {
        return;
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (message.chatFrom == ChatMessageFrom_ME ||
            self.bottomBtn.isHidden) {
            [self scrollBottomAnimated:YES];
        }
    });
}

- (void)ChatUilityScribeData:(ChatUtilitySubscribeData *)data BackgroundMessage:(Message *)message{
    if (data != self.data) {
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        if (message.chatType == ChatMessageType_RULE) {
            /** 权限消息**/
            self.inputToolBar.editale = message.content.isSpeak;
            self.inputToolBar.isSendImg = message.content.isSendImg;
        }else if (message.chatType == ChatMessageType_WIN){
            /** 中奖消息**/
            if (self.delegate && [self.delegate respondsToSelector:@selector(ChatAction:ReceiveWinMessage:)]) {
                [self.delegate ChatAction:self ReceiveWinMessage:message];
            }
        }else if (message.chatType == ChatMessageType_JOIN && [message.content.showVipEnter boolValue]){
            /** VIP加入房间消息**/
            if (self.delegate && [self.delegate respondsToSelector:@selector(ChatAction:ReceiveVipEnterMessage:)]) {
                [self.delegate ChatAction:self ReceiveVipEnterMessage:message];
            }
        }else if (message.chatType == ChatMessageType_KICK){
            /** 踢人消息**/
            self.inputToolBar.editale = NO;
            self.inputToolBar.isSendImg = NO;
            self.tableView.isShowEmpty = YES;
            [self.tableView reloadEmptyDataSet];
            [self cancleAllRequestTask];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [Bet365AlertSheet showChooseAlert:@"你已被踢出聊天室" Message:@"请联系上级代理或客服，加入聊天室" Items:@[@"知道了"] Handler:nil];
            });
            if (self.delegate ) {
                if ([self.delegate respondsToSelector:@selector(ChatAction:UpdataOpenList:)]) {
                    [self.delegate ChatAction:self UpdataOpenList:nil];
                }
                if ([self.delegate respondsToSelector:@selector(HasBeKickedChatAction:)]) {
                    [self.delegate HasBeKickedChatAction:self];
                }
            }
            return;
        }else if (message.chatType == ChatMessageType_UNSPEACK) {
            self.inputToolBar.editale = NO;
        }else if (message.chatType == ChatMessageType_UNIMAGE) {
            self.inputToolBar.isSendImg = NO;
        }else if (message.chatType == ChatMessageType_REMOVE){
                
        }else if (message.chatType == ChatMessageType_REDCOLLOAR_ERROR) {
            [SVProgressHUD showErrorWithStatus:message.content.msg];
        }
        [self chatReloadTableView];
        if ([message.userId integerValue] == CHAT_UTIL.clientInfo.userInfo.userId ||
                message.chatType == ChatMessageType_SYSTEM) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    if (self.filterMessageList.count) {
                        [self scrollBottomAnimated:YES];
                    }
                });
            }
    });
    
}

- (void)ChatUilityScribeData:(ChatUtilitySubscribeData *)data getMessageHistory:(NSArray <Message *>*)list{
    if (data != self.data) {
        return;
    }
    [self.tableView.mj_header endRefreshing];
    if (!list.count || !_behindMessage) {
        return;
    }
    //蛤蟆吉5.13.0闪退修复
//    NSUInteger idx = [self.filterMessageList indexOfObject:_behindMessage];
//    if (idx != NSNotFound) {
//        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
//    }
}

-(void)ChatUilityUpdateConsnectStatus:(ChatUtilityConnectStatus)status{
    
}



#pragma mark - ChatInputToolBarDelegate
-(BOOL)tapMoreActionChatInputToolBar:(ChatInputToolBar *)toolBar{
    ChatImageEmojiPickViewController *vc = [[ChatImageEmojiPickViewController alloc] initWithNibName:@"ChatImageEmojiPickViewController" bundle:nil];
    vc.transitioningDelegate = self;
    vc.modalPresentationStyle = UIModalPresentationCustom;
    [((UIViewController *)self.delegate) presentViewController:vc
                                                      animated:YES
                                                    completion:nil];
    @weakify(self);
    vc.imageBlock = ^() {
        @strongify(self);
        [self showImagePicker];
    };
    vc.emojiBlock = ^(NetEmojiModel *emoji) {
        @strongify(self);
        NSMutableDictionary *dic = @{}.mutableCopy;
        [dic safeSetObject:emoji.filePath forKey:@"img"];
        [dic safeSetObject:emoji.filePath forKey:@"sm"];
        [dic safeSetObject:emoji.width forKey:@"img_width"];
        [dic safeSetObject:emoji.height forKey:@"img_height"];
        [dic safeSetObject:emoji.width forKey:@"sm_width"];
        [dic safeSetObject:emoji.height forKey:@"sm_height"];
        [self sendImageMessage:dic];
    };
    return YES;
}

-(BOOL)ChatInputToolBar:(ChatInputToolBar *)toolBar SendText:(NSString *)str{
    if ([self respondsToSelector:@selector(sendTextMessage:)]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self sendTextMessage:str];
        });
    }
    return YES;
}



-(void)ChatInputToolBarBecomeFirstResponder:(ChatInputToolBar *)toolBar{
    
    [self scrollBottomAnimated:NO];
    
}

-(void)ChatInputToolBarResignFirstResponder:(ChatInputToolBar *)toolBar{
//    [self scrollBottomAnimated:NO];
}

#pragma mark - ChatCellDelegate
-(void)ChatMessageCellTapFail:(ChatMessageCell *)cell{
    Message *message = cell.message;
    
    ChatMessagePopViewController *vc = [[ChatMessagePopViewController alloc] initWithNibName:@"ChatMessagePopViewController" bundle:nil];
    vc.transitioningDelegate = self;
    vc.modalPresentationStyle = UIModalPresentationCustom;
    [((UIViewController *)self.delegate) presentViewController:vc
                                                      animated:YES
                                                    completion:nil];
    vc.message = message;
    @weakify(self);
    vc.sendHandle = ^(ChatMessagePopViewTapStatus status) {
        @strongify(self);
        
        if (status == ChatMessagePopViewTapStatus_SENDAGAINT || status == ChatMessagePopViewTapStatus_DELETE) {
            if ([self respondsToSelector:@selector(tapRemoveMessage:)]) {
                [self tapRemoveMessage:message];
            }
            if (status == ChatMessagePopViewTapStatus_SENDAGAINT) {
                if (message.chatType == ChatMessageType_IMG) {
                    [self uploadImageData:message.content.pImageData];
                }else if (message.chatType == ChatMessageType_TEXT){
                    [self ChatInputToolBar:nil SendText:message.content.text];
                }
            }
        }else if (status == ChatMessagePopViewTapStatus_COPYTEXT) {
            UIPasteboard *board = [UIPasteboard generalPasteboard];
            board.string = message.content.text;
        }
    };
}

-(void)ChatCellMessageHasRead:(ChatCell *)cell{
    if (!cell.message.userId) {
        return;
    }
    Message *message = cell.message;
    [self.data removeUnreadMeesage:message];
    cell.message.hasRead = YES;
}

-(void)ChatMessageCellDidTapAvator:(ChatMessageCell *)cell{
    [self resignAllFirstResponder];
    if (!cell.message.userId) {
        return;
    }
    ChatUserInfoEntity *entity = [[ChatUtilitySubscribeData getUserInfoListEntityByUserId:[cell.message.userId integerValue]] safeObjectAtIndex:0];
    if (CHAT_UTIL.clientInfo.userInfo.userId != [cell.message.userId integerValue]) {
        self.inputToolBar.textAppendingStr = [NSString stringWithFormat:@" @%@ ",entity.nickName];
    }else{
        
    }
}

-(void)ChatMessageCellDidTapImage:(ChatMessageCell *)cell{
    
    [self resignAllFirstResponder];
    Message *message = cell.message;
    if (!message.userId) {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.imgIndexPathList removeAllObjects];
        __block NSInteger imgIdx = 0;
        
        NSIndexPath *currentIdxPath = [self.tableView indexPathForCell:cell];
        [self.filterMessageList enumerateObjectsUsingBlock:^(Message * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.chatType == ChatMessageType_IMG) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:0];
                [self.imgIndexPathList addObject:indexPath];
                if (currentIdxPath.row == indexPath.row) {
                    imgIdx = self.imgIndexPathList.count - 1;
                }
            }
        }];
        PBViewController *pbViewController = [PBViewController new];
        pbViewController.imageViewClass = UIImageView.class;
        pbViewController.pb_dataSource = self;
        pbViewController.pb_delegate = self;
        pbViewController.pb_startPage = imgIdx;
        [((UIViewController *)self.delegate) presentViewController:pbViewController animated:YES completion:nil];
    });
}

-(void)ChatMessageCellDidTapLongImage:(ChatMessageCell *)cell{
    [self resignAllFirstResponder];
    Message *message = cell.message;
    if (!message.userId) {
        return;
    }
    
    if (![self showMessageAuthorityPopView:message]) {
        UIMenuController *menuController = [UIMenuController sharedMenuController];
        if (menuController.isMenuVisible) {
            [menuController setMenuVisible:NO animated:YES];
        }else{
            if ([message.identity integerValue] < 0) {
                return;
            }
            //        [self.tableView scrollToRowAtIndexPath:[self.tableView indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            [cell becomeFirstResponder];
            NSMutableArray *items = @[].mutableCopy;
            CGFloat maxWidth = 0.0;
            
            UIMenuItem *item01 = [[UIMenuItem alloc]initWithTitle:@"保存" action:@selector(saveMessageImage:)];
            [items addObject:item01];
            
            if ([self.data isKindOfClass:[ChatUtilityGroupSubscribeData class]]) {
                BOOL isPrivateChat = NO;
                NSArray *privateList = [((ChatUtilityGroupSubscribeData *)self.data).userList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(privateChat == 1) AND (userId == %li OR userId == %li)",[message.userId integerValue],CHAT_UTIL.clientInfo.userInfo.userId]];
                isPrivateChat = privateList.count > 0;
                if (isPrivateChat && CHAT_UTIL.clientInfo.userInfo.userId != -1) {
                    UIMenuItem *item02 = [[UIMenuItem alloc]initWithTitle:@"发送消息" action:@selector(jumpPrivateChat:)];
                    [items addObject:item02];
                    maxWidth+= 70;
                }
            }
            
            if ([self.data isKindOfClass:[ChatUtilityGroupSubscribeData class]] && [((ChatUtilityGroupSubscribeData *)self.data).ruleModel.isViewUserInfo boolValue]) {
                UIMenuItem *item02 = [[UIMenuItem alloc]initWithTitle:@"成员信息" action:@selector(saveMessagePopUserDatail:)];
                [items addObject:item02];
                maxWidth+= 70;
            }
            
            if (message.chatFrom == ChatMessageFrom_ME) {
                UIMenuItem *item03 = [[UIMenuItem alloc]initWithTitle:@"撤回" action:@selector(revokeMessage:)];
                [items addObject:item03];
                maxWidth += 35;
            }else if(message.chatFrom == ChatMessageFrom_OTHER){
                if ([self.data isKindOfClass:[ChatUtilityGroupSubscribeData class]]) {
                    NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
                    ChatFollowEntity *entity = [self.data getFollowEntityByFollowId:[message.userId integerValue] inContext:context];
                    UIMenuItem *item04 = [[UIMenuItem alloc]initWithTitle:entity ? @"取消关注" : @"关注" action:@selector(followMessage:)];
                    [items addObject:item04];
                    maxWidth += (entity ? 70 : 35);
                }
            }
            
            
            
            

            menuController.menuItems = items;
            CGRect showRect = CGRectMake(cell.bubbleView.ycz_x + cell.bubbleView.ycz_width / 2.0,
                                         cell.ycz_y + cell.ycz_height/2,
                                         maxWidth,
                                         60);
            [menuController setTargetRect:showRect inView:cell.superview];
            [menuController setMenuVisible:YES animated:YES];
        }
    }
}

-(void)ChatMessageCellDidTapLottery:(ChatMessageCell *)cell{
    [self resignAllFirstResponder];
    if (!cell.message.userId) {
        return;
    }
    Message *message = cell.message;
    [CHAT_UTIL getOpenInfoByLotteryID:[message.content.gameId stringValue] Completed:^(LotteryOpenInfoModel *model) {
        dispatch_async(dispatch_get_main_queue(), ^{
            ChatLotteryPopViewController *vc = [[ChatLotteryPopViewController alloc] initWithNibName:@"ChatLotteryPopViewController" bundle:nil];
            vc.message = message;
            vc.handle = ^(NSNumber *lotteryID,NSInteger scale) {
                [CHAT_UTIL submitChatbeelLotteryWithUserID:[message.userId integerValue]
                                                   orderNo:message.content.orderNo
                                                  PlayCode:message.content.playCode
                                                TotalMoney:[NSNumber numberWithDouble:scale * [message.content.oneMoney doubleValue]]
                                                    RoomID:self.channelID
                                                 Completed:^(BOOL success) {
                                                     if (success) {
                                                         [SVProgressHUD showSuccessWithStatus:@"跟投成功"];
                                                     }else{
                                                         [SVProgressHUD showSuccessWithStatus:@"跟投失败"];
                                                     }
                                                 }];
            };
            @weakify(self);
            vc.moreHandle = ^(Message *message) {
                @strongify(self);
                [self showMessageAuthorityPopView:message];
            };
            vc.transitioningDelegate = self;
            vc.modalPresentationStyle = UIModalPresentationCustom;
            [((UIViewController *)self.delegate) presentViewController:vc
                                                              animated:YES
                                                            completion:nil];
        });
    }];
    
    
    
}

-(void)ChatMessageCellDidTapLotteryButton:(ChatMessageCell *)cell{
    [self ChatMessageCellDidTapLottery:cell];
//    NSInteger time = cell.message.content.betEndTime / 1000 - [[NSDate date] timeIntervalSince1970];
//    if (time <= 0) {
//        [SVProgressHUD showSuccessWithStatus:@"好可惜，又错过的大奖"];
//        return;
//    }
//    [CHAT_UTIL submitChatbeelLotteryWithUserID:[cell.message.userId integerValue]
//                                       orderNo:cell.message.content.orderNo
//                                      PlayCode:cell.message.content.playCode
//                                    TotalMoney:cell.message.content.totalMoney
//                                     Completed:^(BOOL success) {
//                                         if (success) {
//                                             [SVProgressHUD showSuccessWithStatus:@"跟投成功"];
//                                         }else{
//                                             [SVProgressHUD showSuccessWithStatus:@"跟投失败"];
//                                         }
//                                     }];
}

-(void)ChatMessageCellDidTapLongLottery:(ChatMessageCell *)cell{
    [self resignAllFirstResponder];
    if (!cell.message.userId) {
        return;
    }
    Message *message = cell.message;
    [self showMessageAuthorityPopView:message];
}

-(void)ChatMessageCellDidTapRed:(ChatMessageCell *)cell{
    [self resignAllFirstResponder];
    if (!cell.message.userId) {
        return;
    }
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    if (menuController.isMenuVisible) {
        [menuController setMenuVisible:NO animated:YES];
    }
    Message *message = cell.message;
    [self showRedOpen:message];
    
    
}

-(void)ChatMessageCellDidTapLongRed:(ChatMessageCell *)cell{
    [self resignAllFirstResponder];
    if (!cell.message.userId) {
        return;
    }
    Message *message = cell.message;
    [self showMessageAuthorityPopView:message];
}


-(void)ChatMessageCellDidTapText:(ChatMessageCell *)cell{
    [self resignAllFirstResponder];
    if (cell.message.chatType == ChatMessageType_LOTTERY) {
        [self ChatMessageCellDidTapLottery:cell];
    }else{
        if (!cell.message.userId) {
            return;
        }
        UIMenuController *menuController = [UIMenuController sharedMenuController];
        if (menuController.isMenuVisible) {
            [menuController setMenuVisible:NO animated:YES];
        }
    }
    
}

-(void)ChatMessageCellDidLongTapText:(ChatMessageCell *)cell{
    [self resignAllFirstResponder];
    if (cell.message.chatType == ChatMessageType_LOTTERY) {
        [self ChatMessageCellDidTapLottery:cell];
    }else{
        if (!cell.message.userId) {
            return;
        }
        Message *message = cell.message;
        if (![self showMessageAuthorityPopView:message]) {
            UIMenuController *menuController = [UIMenuController sharedMenuController];
            if (menuController.isMenuVisible) {
                [menuController setMenuVisible:NO animated:YES];
            }else {
                if ([message.identity integerValue] < 0) {
                    return;
                }
                
                //        [self.tableView scrollToRowAtIndexPath:[self.tableView indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionTop animated:YES];
                [cell becomeFirstResponder];
                NSMutableArray *items = @[].mutableCopy;
                CGFloat maxWidth = 0.0;
                UIMenuItem *item01 = [[UIMenuItem alloc]initWithTitle:@"复制" action:@selector(copyMessageText:)];
                [items addObject:item01];
                maxWidth += 35;
                
                if ([self.data isKindOfClass:[ChatUtilityGroupSubscribeData class]]) {
                    __block BOOL isPrivateChat = NO;
                    
                    NSArray *privateList = [((ChatUtilityGroupSubscribeData *)self.data).userList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(privateChat == 1) AND (userId == %li OR userId == %li)",[message.userId integerValue],CHAT_UTIL.clientInfo.userInfo.userId]];
                    isPrivateChat = privateList.count > 0;
                    
                    if (isPrivateChat && CHAT_UTIL.clientInfo.userInfo.userId != -1) {
                        UIMenuItem *item02 = [[UIMenuItem alloc]initWithTitle:@"发送消息" action:@selector(jumpPrivateChat:)];
                        [items addObject:item02];
                        maxWidth+= 70;
                    }
                }
                
                if ([self.data isKindOfClass:[ChatUtilityGroupSubscribeData class]] && [((ChatUtilityGroupSubscribeData *)self.data).ruleModel.isViewUserInfo boolValue]) {
                    UIMenuItem *item02 = [[UIMenuItem alloc]initWithTitle:@"成员信息" action:@selector(saveMessagePopUserDatail:)];
                    [items addObject:item02];
                    maxWidth+= 70;
                }
                
                
                
                if (message.chatFrom == ChatMessageFrom_ME) {
                    UIMenuItem *item03 = [[UIMenuItem alloc]initWithTitle:@"撤回" action:@selector(revokeMessage:)];
                    [items addObject:item03];
                    maxWidth += 35;
                }else if(message.chatFrom == ChatMessageFrom_OTHER){
                    if ([self.data isKindOfClass:[ChatUtilityGroupSubscribeData class]]) {
                        NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
                        ChatFollowEntity *entity = [self.data getFollowEntityByFollowId:[cell.message.userId integerValue] inContext:context];
                        UIMenuItem *item04 = [[UIMenuItem alloc]initWithTitle:entity ? @"取消关注" : @"关注" action:@selector(followMessage:)];
                        [items addObject:item04];
                        maxWidth += (entity ? 70 : 35);
                    }
                    
                }
                
                menuController.menuItems = items;
                CGRect showRect = CGRectMake(cell.bubbleView.ycz_x + cell.bubbleView.ycz_width / 2.0,
                                             cell.ycz_y + cell.ycz_height/2,
                                             maxWidth,
                                             60);
                [menuController setTargetRect:showRect inView:cell.superview];
                [menuController setMenuVisible:YES animated:YES];
            }
        }
    }
    
    
}
-(void)ChatMessageCellCopyText:(ChatMessageCell *)cell{
    if (!cell.message.userId) {
        return;
    }
    UIPasteboard *board = [UIPasteboard generalPasteboard];
    board.string = cell.message.content.text;
}

-(void)ChatMessageCellSaveImg:(ChatMessageCell *)cell{
    
    [self saveImgAtBumble:cell.bubbleView.imageView.image];
}

-(void)ChatMessageCellPopUserDetail:(ChatMessageCell *)cell{
    if (!cell.message.userId) {
        return;
    }
    [CHAT_UTIL getUserInfoWithUserID:[cell.message.userId integerValue] RoomID:self.channelID Completed:^(ChatUserInfoModel *model) {
        ChatUserInfoPopViewController *userPopVC = [[ChatUserInfoPopViewController alloc] initWithNibName:@"ChatUserInfoPopViewController" bundle:nil];
        userPopVC.model = model;
        userPopVC.transitioningDelegate = self;
        userPopVC.modalPresentationStyle = UIModalPresentationCustom;
        [((UIViewController *)self.delegate) presentViewController:userPopVC
                                                          animated:YES
                                                        completion:nil];
    }];
}

-(void)ChatMessageCellRevokeMessage:(ChatMessageCell *)cell{
    if ([self respondsToSelector:@selector(tapRemoveMessage:)]) {
        Message *message = cell.message;
        [self tapRemoveMessage:message];
    }
    
}
-(void)ChatMessageCellFollowMessage:(ChatMessageCell *)cell{
    NSManagedObjectContext *context = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_defaultContext]];;
    [self.data setFollowEntityByFollowId:[cell.message.userId integerValue] inContext:context];
}
#pragma mark - PBViewControllerDataSource
- (NSInteger)numberOfPagesInViewController:(nonnull PBViewController *)viewController{
    return self.imgIndexPathList.count;
}
- (void)viewController:(PBViewController *)viewController presentImageView:(UIImageView *)imageView forPageAtIndex:(NSInteger)index progressHandler:(void (^)(NSInteger, NSInteger))progressHandler {
    NSIndexPath *indexPath = self.imgIndexPathList[index];
    Message *message = [self.filterMessageList safeObjectAtIndex:indexPath.row];
//    ChatMessageCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    });
    [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",CHAT_UTIL.clientInfo.chatUrl,message.content.thumbnail]] placeholderImage:[UIImage imageWithSmallGIFData:message.content.pImageData scale:1.0]];
    
}

- (UIView *)thumbViewForPageAtIndex:(NSInteger)index {
    NSIndexPath *indexPath = self.imgIndexPathList[index];
    ChatMessageCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    return cell.bubbleView.imageView;
}

#pragma mark - PBViewControllerDelegate

- (void)viewController:(PBViewController *)viewController didSingleTapedPageAtIndex:(NSInteger)index presentedImage:(UIImage *)presentedImage {
    [((UIViewController *)self.delegate) dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIViewControllerTransitioningDelegate
- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source{

    if ([presented isKindOfClass:[ChatImageEmojiPickViewController class]]) {
        return [PresentingBottomAnimator new];
    }else{
        return [PresentingTopAnimator new];
    }
    
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    if ([dismissed isKindOfClass:[ChatImageEmojiPickViewController class]]) {
        return [DismissingBottomAnimator new];
    }else{
        return [DismissingTopAnimator new];
    }
}

#pragma mark - Action
#pragma mark - Private
/**
 发送图片消息

 @param imageData 图片转NSData
 */
-(void)uploadImageData:(NSData *)imageData{
    
    [CHAT_UTIL uploadImageData:imageData Completed:^(NSDictionary *imageInfo) {
        [self sendImageMessage:imageInfo];
    }];
}

-(void)sendImageMessage:(NSDictionary *)imageInfo{
    if (!USER_DATA_MANAGER.userInfoData.token) {
        return;
    }
    if (!imageInfo.allKeys.count) {
        return;
    }
    ChatSendMessageModel *model = nil;
    if ([self isKindOfClass:ChatGroupAction.class]) {
        model = [ChatSendMessageGroupModel creatDefaultSendModel:[NSDate date]];
        ((ChatSendMessageGroupModel *)model).roomId = self.channelID;
    }else if ([self isKindOfClass:ChatPrivateAction.class]){
        model = [ChatSendMessagePrivateModel creatDefaultSendModel:[NSDate date]];
        ((ChatSendMessagePrivateModel *)model).userId = self.channelID;
    }
    model.messageType = ChatMessageType_IMG;
    if (imageInfo) {
        model.content = imageInfo;
        [CHAT_UTIL webSocketSendMessageByChatSendModel:model Subscribe:self.data];
    }
}



-(void)showImagePicker{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 columnNumber:4 delegate:nil pushPhotoPickerVc:YES];

        imagePickerVc.isSelectOriginalPhoto = NO;
        
        //    if (self.maxCountTF.text.integerValue > 1) {
        //        // 1.设置目前已经选中的图片数组
        //        imagePickerVc.selectedAssets = _selectedAssets; // 目前已经选中的图片数组
        //    }
        imagePickerVc.allowTakePicture = YES; // 在内部显示拍照按钮
        
        // imagePickerVc.photoWidth = 1000;
        
        // 2. Set the appearance
        // 2. 在这里设置imagePickerVc的外观
        // imagePickerVc.navigationBar.barTintColor = [UIColor greenColor];
        // imagePickerVc.oKButtonTitleColorDisabled = [UIColor lightGrayColor];
        // imagePickerVc.oKButtonTitleColorNormal = [UIColor greenColor];

        
        // 3. Set allow picking video & photo & originalPhoto or not
        // 3. 设置是否可以选择视频/图片/原图
        imagePickerVc.allowPickingVideo = NO;
        imagePickerVc.allowPickingImage = YES;
        imagePickerVc.allowPickingOriginalPhoto = YES;
        imagePickerVc.allowPickingGif = YES;
        imagePickerVc.allowPickingMultipleVideo = NO; // 是否可以多选视频
        
        // 4. 照片排列按修改时间升序
        imagePickerVc.sortAscendingByModificationDate = YES;
        
    //     imagePickerVc.minImagesCount = 3;
    //     imagePickerVc.alwaysEnableDoneBtn = YES;
    //
    //     imagePickerVc.minPhotoWidthSelectable = 3000;
    //     imagePickerVc.minPhotoHeightSelectable = 2000;
    //
    //    / 5. Single selection mode, valid when maxImagesCount = 1
    //    / 5. 单选模式,maxImagesCount为1时才生效
        imagePickerVc.showSelectBtn = NO;
        imagePickerVc.allowCrop = NO;
        imagePickerVc.needCircleCrop = NO;
        [((UIViewController *)self.delegate).navigationController presentViewController:imagePickerVc animated:YES completion:^{

        }];
        
        @weakify(self);
        
        [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
            @strongify(self);
            
            if (photos.count) {
                NSData *imageData = UIImageJPEGRepresentation(photos.firstObject,0.1);
                [self uploadImageData:imageData];
                
            }
        }];
        
        [imagePickerVc setDidFinishPickingGifImageHandle:^(UIImage *animatedImage, id sourceAssets) {
            @strongify(self);
            if (animatedImage) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    NSArray *resourceList = [PHAssetResource assetResourcesForAsset:sourceAssets];
                    [resourceList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        PHAssetResource *resource = obj;
                        PHAssetResourceRequestOptions *option = [[PHAssetResourceRequestOptions alloc]init];
                        option.networkAccessAllowed = YES;
                        if ([resource.uniformTypeIdentifier isEqualToString:@"com.compuserve.gif"]) {
                            NSLog(@"source == gif");
                            // 首先,需要获取沙盒路径
                            NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
                            // 拼接图片名为resource.originalFilename的路径
                            NSString *imageFilePath = [path stringByAppendingPathComponent:resource.originalFilename];
                            __block NSData *data = [[NSData alloc]init];
                            [[PHAssetResourceManager defaultManager] writeDataForAssetResource:resource toFile:[NSURL fileURLWithPath:imageFilePath]  options:option completionHandler:^(NSError * _Nullable error) {
                                if (error) {
                                    NSLog(@"error:%@",error);
                                    if(error.code == -1){
                                        //文件已存在
                                        data = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:imageFilePath]];
                                    }
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [self uploadImageData:data];

                                    });
                                } else {
                                    data = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:imageFilePath]];
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [self uploadImageData:data];
                                    });
                                }
                            }];
                        }
                    }];
                });
            }
        }];
}
-(void)bottomButtonAction:(UIButton *)sender{
    [self scrollBottomAnimated:YES];
}

-(void)getMoreMessageList{
    [self.data.messageList enumerateObjectsUsingBlock:^(Message * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.identity integerValue] > 0) {
            _behindMessage = obj;
            *stop = YES;
        }
    }];
    if (_behindMessage) {
        if ([self respondsToSelector:@selector(getHistoryMessageListByBehindMessage:)]) {
            [self getHistoryMessageListByBehindMessage:_behindMessage];
        }
        
    }else{
        [self.tableView.mj_header endRefreshing];
    }
}

-(void)saveImgAtBumble:(UIImage *)img{
    if (!img) {
        return;
    }
    __block ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
    [lib writeImageToSavedPhotosAlbum:img.CGImage metadata:nil completionBlock:^(NSURL *assetURL, NSError *error) {
        if (error) {
            [SVProgressHUD showErrorWithStatus:@"保存失败"];
        }else{
            [SVProgressHUD showSuccessWithStatus:@"保存成功"];
        }
        lib = nil;
    }];
}

-(BOOL)showMessageAuthorityPopView:(Message *)message{
    if ([self.data isKindOfClass:[ChatUtilityPrivateSubscribeData class]]) {
        return NO;
    }
    if ([message.identity integerValue] < 0) {
        return YES;
    }
    ChatUtilityGroupSubscribeData *data = (ChatUtilityGroupSubscribeData *)self.data;
    if ([data.ruleModel.isGag integerValue] ||
        [data.ruleModel.isBanImg integerValue] ||
        [data.ruleModel.isKick integerValue] ||
        [data.ruleModel.isDelMessage integerValue] ||
        [data.ruleModel.isViewUserInfo integerValue]) {
        ChatMessagePopViewController *vc = [[ChatMessagePopViewController alloc] initWithNibName:@"ChatMessagePopViewController" bundle:nil];
        vc.transitioningDelegate = self;
        vc.modalPresentationStyle = UIModalPresentationCustom;
        [((UIViewController *)self.delegate) presentViewController:vc
                                                                               animated:YES
                                                                             completion:nil];
        vc.data = data;
        vc.message = message;
        vc.ruleModel = data.ruleModel;
        @weakify(self);
        vc.authorHandle = ^(ChatMessagePopViewTapStatus status) {
            @strongify(self);
            if (status == ChatMessagePopViewTapStatus_USER) {
                [CHAT_UTIL getUserInfoWithUserID:[message.userId integerValue] RoomID:self.channelID Completed:^(ChatUserInfoModel *model) {
                    ChatUserInfoPopViewController *userPopVC = [[ChatUserInfoPopViewController alloc] initWithNibName:@"ChatUserInfoPopViewController" bundle:nil];
                    userPopVC.model = model;
                    userPopVC.transitioningDelegate = self;
                    userPopVC.modalPresentationStyle = UIModalPresentationCustom;
                    [((UIViewController *)self.delegate) presentViewController:userPopVC
                                                                      animated:YES
                                                                    completion:nil];
                }];
            }else if (status == ChatMessagePopViewTapStatus_UNMESSAGE) {
                [Bet365AlertSheet showChooseAlert:@"是否禁止该聊天室成员发送任何消息?" Message:nil Items:@[@"[禁言]",@"取消"] Handler:^(NSInteger index) {
                    if (index == 0) {
                        [CHAT_UTIL chatUntilBanUserByUserID:[message.userId integerValue] RoomID:data.identifier FK:message.fk MessageID:[message.identity integerValue] Completed:^(BOOL success) {
                            
                        }];
                    }
                }];
            }else if (status == ChatMessagePopViewTapStatus_UNIMG) {
                [Bet365AlertSheet showChooseAlert:@"是否禁止该聊天室成员发送图片?" Message:nil Items:@[@"[禁图]",@"取消"] Handler:^(NSInteger index) {
                    if (index == 0) {
                        [CHAT_UTIL chatUntilBanImgByUserID:[message.userId integerValue] RoomID:data.identifier FK:message.fk MessageID:[message.identity integerValue] Completed:^(BOOL success) {
                            
                        }];
                    }
                }];
                
            }else if (status == ChatMessagePopViewTapStatus_KIKE) {
                
                [Bet365AlertSheet showChooseAlert:@"是否移除该聊天室成员?" Message:nil Items:@[@"[移除]",@"取消"] Handler:^(NSInteger index) {
                    if (index == 0) {
                        [CHAT_UTIL chatUntilKikeUserByUserID:[message.userId integerValue] RoomID:data.identifier FK:message.fk MessageID:[message.identity integerValue] Completed:^(BOOL success) {
                            
                        }];
                    }
                }];
                
            }else if (status == ChatMessagePopViewTapStatus_DELETE) {
                [Bet365AlertSheet showChooseAlert:@"是否删除该消息?" Message:@"删除的消息不会出现在平台聊天室" Items:@[@"[删除消息]",@"取消"] Handler:^(NSInteger index) {
                    if (index == 0) {
                        if ([self respondsToSelector:@selector(tapRemoveMessage:)]) {
                            [self tapRemoveMessage:message];
                        }
                        
                    }
                }];
                
            }else if (status == ChatMessagePopViewTapStatus_REVOKE){
                if ([self respondsToSelector:@selector(tapRemoveMessage:)]) {
                    [self tapRemoveMessage:message];
                }
            }else if (status == ChatMessagePopViewTapStatus_COPYTEXT){
                UIPasteboard *board = [UIPasteboard generalPasteboard];
                board.string = message.content.text;
            }else if (status == ChatMessagePopViewTapStatus_SAVEIMG){
                NSString *urlStr = [NSString stringWithFormat:@"%@%@",CHAT_UTIL.clientInfo.chatUrl,message.content.thumbnail];
                UIImage *img = [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:urlStr];
                if (!img) {
                    
                    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:urlStr] options:0 progress:nil
                                                                        completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                                                                            if (finished&&!error) {
                                                                                [self saveImgAtBumble:image];
                                                                            }else{
                                                                                
                                                                            }
                                                                        }];
                }else{
                    [self saveImgAtBumble:img];
                }
            }else if (status == ChatMessagePopViewTapStatus_FOLLOW){
                [data manualUpdateFilter];
                
//                [self.tableView beginUpdates];
                [self chatReloadTableView];
//                [self.tableView endUpdates];
            }else if (status == ChatMessagePopViewTapStatus_PRIVATECHAT){
                ChatSendRequestGroupModel *requestModel = [ChatSendRequestGroupModel creatSendModel:kChatSendRequestUserInfoPrivate Date:[NSDate date]];
                requestModel.userId = [message.userId integerValue];
                requestModel.roomId = self.channelID;
                [CHAT_UTIL webSocketSendMessageByChatSendModel:requestModel Subscribe:self.data];
            }
        };
        return YES;
    }
    return NO;
}

-(void)showRedOpen:(Message *)message{
    [CHAT_UTIL getRedDetailByMessage:message Completed:^(ChatRedPacketModel *model) {
        
        __block BOOL isContain = NO;
        [model.details enumerateObjectsUsingBlock:^(ChatRedPacketDetailModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.userId integerValue] == CHAT_UTIL.clientInfo.userInfo.userId) {
                isContain = YES;
                *stop = YES;
            }
        }];
        if (isContain &&
            model.activityChatRedpacket.count >= model.activityChatRedpacket.consumeCount &&
            model.activityChatRedpacket.count != 1) {
            /** 草tmd，红包要是还没领完自己又领过，或者红包被领完自己又tmd领过了，红包个数还不是1，狗日的，都给劳资跳转到红包领取记录去*/
            [self showRedResults:model];
        }else{
            ChatRedOpenViewController *vc = [[ChatRedOpenViewController alloc] initWithNibName:@"ChatRedOpenViewController" bundle:nil];
            
            
            vc.transitioningDelegate = self;
            vc.modalPresentationStyle = UIModalPresentationCustom;
            [((UIViewController *)self.delegate) presentViewController:vc
                                                              animated:YES
                                                            completion:nil];
            @weakify(self);
            vc.handle = ^(ChatRedTapStatus status) {
                @strongify(self);
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (status == ChatRedTapStatus_OPEN) {
                        if ([self respondsToSelector:@selector(openRedPage:)]) {
                            [self openRedPage:model.activityChatRedpacket.identity];
                        }
                    }else if (status == ChatRedTapStatus_RESULTS){
                        [CHAT_UTIL getRedDetailByMessage:message Completed:^(ChatRedPacketModel *secModel) {
                            [self showRedResults:secModel];
                        }];
                    }else{
                        self.redOpenViewController = nil;
                    }
                });
            };
            vc.model = model;
            self.redOpenViewController = vc;
        }
    }];
    
}

-(void)showRedResults:(ChatRedPacketModel *)model{
    if (!model) {
        return;
    }
    ChatRedResultViewController *vc = [[ChatRedResultViewController alloc] initWithNibName:@"ChatRedResultViewController" bundle:nil];
    Bet365NavViewController *navi = [[Bet365NavViewController alloc] initWithRootViewController:vc];
    [((UIViewController *)self.delegate) presentViewController:navi
                                                      animated:YES
                                                    completion:nil];
    vc.model = model;
    [CHAT_UTIL.clientInfo.roomInfoList enumerateObjectsUsingBlock:^(ChatRoomInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.identity integerValue] == self.channelID) {
            vc.title = obj.name;
            *stop = YES;
        }
    }];
    self.redResultViewController = vc;
}



#pragma mark - GET/SET
-(NSMutableArray *)filterMessageList{
    if (!_filterMessageList) {
        _filterMessageList = @[].mutableCopy;
    }
    return _filterMessageList;
}

-(void)setTableView:(UITableView *)tableView{
    _tableView = tableView;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.estimatedSectionHeaderHeight = 0;
    _tableView.estimatedSectionFooterHeight = 0;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    [_tableView registerNib:[UINib nibWithNibName:@"ChatMessageCell" bundle:nil] forCellReuseIdentifier:@"ChatMessageCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"ChatSystemCell" bundle:nil] forCellReuseIdentifier:@"ChatSystemCell"];
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(getMoreMessageList)];
    [header setTitle:@"下拉获取更多聊天记录" forState:MJRefreshStateIdle];
    [header setTitle:@"松开获取更多聊天记录" forState:MJRefreshStatePulling];
    [header setTitle:@"加载中..." forState:MJRefreshStateRefreshing];
    _tableView.mj_header = header;
    
}



-(void)setInputToolBar:(ChatInputToolBar *)inputToolBar{
    _inputToolBar = inputToolBar;
    _inputToolBar.delegate = self;
}

-(void)setBottomBtn:(ChatScrollBottomButton *)bottomBtn{
    _bottomBtn = bottomBtn;
    [_bottomBtn addTarget:self action:@selector(bottomButtonAction:) forControlEvents:UIControlEventTouchUpInside];
}

-(NSMutableArray<NSIndexPath *> *)imgIndexPathList{
    if (!_imgIndexPathList) {
        _imgIndexPathList = [NSMutableArray array];
    }
    return _imgIndexPathList;
}

//Class getChatActionClass(ChatType type){
//    Class class = NSClassFromString(getChatActionNameList()[type]);
//    return class;
//}
//
//NSArray <NSString *>* getChatActionNameList(){
//    return @[@"ChatGroupAction",
//             @"ChatGroupAction"];
    
//}
NSString *cellIdentity(Message *message){
    if (
//        message.chatType == ChatMessageType_JOIN ||
        message.chatType == ChatMessageType_LEAVE ||
        message.chatType == ChatMessageType_KICK ||
        message.chatType == ChatMessageType_REMOVE ||
        message.chatType == ChatMessageType_SYSTEM ||
        message.chatType == ChatMessageType_REDCOLLOAR) {
        return @"ChatSystemCell";
    }else{
        return @"ChatMessageCell";
    }
}

@end
