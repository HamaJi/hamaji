//
//  ChatPrivateAction.m
//  Bet365
//
//  Created by HHH on 2019/2/1.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ChatPrivateAction.h"
#import "ChatAction+Extension.h"

@implementation ChatPrivateAction
#pragma mark - ChatUtilyDelegate
-(void)ChatUtilityJoinPrivateScribeData:(ChatUtilityPrivateSubscribeData *)data{
    if ([data isEqual:self.data]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self chatReloadTableView];
            [self scrollBottomAnimated:NO];
            
        });
    }
}

-(void)updateChatUtilitySubscribeData:(ChatUtilitySubscribeData *)data isOnline:(BOOL)isOnline{
    if (self.delegate && [self.delegate respondsToSelector:@selector(ChatAction:UserIsOnline:)]) {
        [self.delegate ChatAction:self UserIsOnline:isOnline];
    }
}
#pragma mark - Overralate
-(ChatUtilitySubscribeData *)subSrcibeData{
    ChatUtilityPrivateSubscribeData *data = [CHAT_UTIL webSocketSubsribeUserOnlineStatus:self.channelID];
    data.userModel = self.userModel;
    return data;
}

-(void)openRedPage:(NSInteger )redPacketId{
    if (!USER_DATA_MANAGER.userInfoData.token) {
        return;
    }
    if (!redPacketId) {
        [SVProgressHUD showErrorWithStatus:@"红包数据异常"];
        return;
    }
    ChatSendMessagePrivateModel *model = [ChatSendMessagePrivateModel creatSendModel:kChatSendCollarRedPage Date:[NSDate date]];
    model.userId = self.channelID;
    model.content = [NSString stringWithFormat:@"%li",redPacketId];
    [CHAT_UTIL webSocketSendMessageByChatSendModel:model Subscribe:self.data];
}

-(void)sendTextMessage:(NSString *)text{
    
    if (!USER_DATA_MANAGER.userInfoData.token) {
        return;
    }
    ChatSendMessagePrivateModel *model = [ChatSendMessagePrivateModel creatSendModel:kChatSendMessageTextPrivate Date:[NSDate date]];
    model.userId = self.channelID;
//    NSString *jsonText = [@{@"type":@"text",
//                            @"content":text,
//                            @"messageId":model.chatSendId
//                            } mj_JSONString];
//    model.content = jsonText;
    model.content = text;
    [CHAT_UTIL webSocketSendMessageByChatSendModel:model Subscribe:self.data];
}



-(void)getHistoryMessageListByBehindMessage:(Message *)message{
    
    ChatSendRequestPrivateModel *model = [ChatSendRequestPrivateModel creatSendModel:kChatSendRequestHistoryPrivate Date:[NSDate date]];
    model.userId = self.channelID;
    model.messageId = [message.identity integerValue];
    [CHAT_UTIL webSocketSendMessageByChatSendModel:model Subscribe:self.data];
}

-(void)tapRemoveMessage:(Message *)message{
    [CHAT_UTIL webSocketRemoveMessage:message Data:self.data];
}

-(void)reloadTableViewByFileterMessageList{
    
}
#pragma mark - GET/SET
@end
