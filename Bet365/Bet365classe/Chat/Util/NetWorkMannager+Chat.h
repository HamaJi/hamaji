//
//  NetWorkMannager+Chat.h
//  Bet365
//
//  Created by HHH on 2018/9/22.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NetWorkMannager.h"

@interface NetWorkMannager (Chat)
-(__kindof NSURLSessionTask *)requestChatPostInitponseCache:(HttpRequestCache)responseCache
                                           success:(HttpRequestSuccess)success
                                           failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)uploadChatImageData:(NSData *)imageData
                               ServerUrl:(NSString *)url
                                ImageKey:(NSString *)key
                                progress:(HttpProgress)progress
                                 success:(HttpRequestSuccess)success
                                 failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)uploadChatImageData:(NSData *)imageData progress:(HttpProgress)progress success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

-(__kindof NSURLSessionTask *)requestChatGetAvatorListRponseCache:(HttpRequestCache)responseCache
                                                 success:(HttpRequestSuccess)success
                                                 failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestChatBanUserID:(NSInteger)userID
                               RoomID:(NSInteger)roomID
                                   FK:(NSString *)fk
                            MessageId:(NSInteger)messageId
                              Success:(HttpRequestSuccess)success
                              Failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestChatKikeUserID:(NSInteger)userID
                                RoomID:(NSInteger)roomID
                                    FK:(NSString *)fk
                             MessageId:(NSInteger)messageId
                               Success:(HttpRequestSuccess)success
                               Failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestChatBanImgUserID:(NSInteger)userID
                                  RoomID:(NSInteger)roomID
                                      FK:(NSString *)fk
                               MessageId:(NSInteger)messageId
                                 Success:(HttpRequestSuccess)success
                                 Failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestChatDeleteMessageUserID:(NSInteger)userID
                                         RoomID:(NSInteger)roomID
                                             FK:(NSString *)fk
                                      MessageId:(NSInteger)messageId
                                        Success:(HttpRequestSuccess)success
                                        Failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestLotteryOpenTopResultsByLotteryID:(NSInteger)lotteryID
                                               PageSize:(NSInteger)pageSize
                                          ResponseCache:(HttpRequestCache)responseCache
                                                success:(HttpRequestSuccess)success
                                                failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestUpdateUserAvator:(NSString *)avatar
                                    NickName:(NSString *)nickName
                               ResponseCache:(HttpRequestCache)responseCache
                                     success:(HttpRequestSuccess)success
                                     failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestChatUserInfoWithUserID:(NSInteger)userID
                                            RoomID:(NSInteger)roomID
                                           Success:(HttpRequestSuccess)success
                                           Failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestChatbeelLotteryWithorderNo:(NSString *)orderNo
                                              PlayCode:(NSString *)playCode
                                            TotalMoney:(NSNumber *)totalMoney
                                                UserId:(NSInteger)userId
                                                RoomID:(NSInteger)roomId
                                               Success:(HttpRequestSuccess)success
                                               Failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestChatMessageListWithIdentity:(NSInteger)identity
                                                 RoomId:(NSInteger)roomId
                                               PageSize:(NSInteger)size
                                                Success:(HttpRequestSuccess)success
                                                Failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestRemoveAllMessageWithIdentity:(NSInteger)identity
                                                  RoomId:(NSInteger)roomId
                                                 Success:(HttpRequestSuccess)success
                                                 Failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestChatSlideMenusSuccess:(HttpRequestSuccess)success
                                          Failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestChatDelegateResponseCache:(HttpRequestCache)responseCache
                                              Success:(HttpRequestSuccess)success
                                              Failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestTheGameOpenTimeByLotteryID:(NSString *)lotterytID
                                         ResponseCache:(HttpRequestCache)responseCache
                                               success:(HttpRequestSuccess)success
                                               failure:(HttpRequestFailed)failure;
-(NSURLSessionTask *)requestGetRedDetailByRedId:(NSString *)redId
                                  ResponseCache:(HttpRequestCache)responseCache
                                        success:(HttpRequestSuccess)success
                                        failure:(HttpRequestFailed)failure;
-(NSURLSessionTask *)requestTopListByRoomId:(NSInteger)roomId
                              ResponseCache:(HttpRequestCache)responseCache
                                    success:(HttpRequestSuccess)success
                                    failure:(HttpRequestFailed)failure;

- (NSURLSessionTask *)requestSendChatErrorData:(NSDictionary *)errorData
                                       success:(HttpRequestSuccess)success
                                       failure:(HttpRequestFailed)failure;

- (NSURLSessionTask *)requestGetEmojiListWithUrl:(NSString *)url
              Key:(NSString *)key
          KeyWord:(NSString *)keyword
             Page:(NSUInteger)page
             Rows:(NSUInteger)rows
success:(HttpRequestSuccess)success
                                         failure:(HttpRequestFailed)failure;
@end
