//
//  NetWorkMannager+Chat.m
//  Bet365
//
//  Created by HHH on 2018/9/22.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NetWorkMannager+Chat.h"
#import "RequestUrlHeader.h"
@implementation NetWorkMannager (Chat)

-(NSURLSessionTask *)requestChatPostInitponseCache:(HttpRequestCache)responseCache
                                               success:(HttpRequestSuccess)success
                                               failure:(HttpRequestFailed)failure{
    return [self POST:CHAT_POSTINIT_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:@{@"version":@(2)} responseCache:responseCache success:success failure:failure];
}

-(NSURLSessionTask *)uploadChatImageData:(NSData *)imageData RoomID:(NSInteger)roomId SendId:(NSString *)sendId toUserId:(NSInteger)toUserId progress:(HttpProgress)progress success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{
    UIImage *image = [UIImage imageWithSmallGIFData:imageData scale:1.0];
    
    NSMutableDictionary *paramers = @{@"messageId":sendId}.mutableCopy;
    if (roomId) {
        [paramers safeSetObject:@(roomId) forKey:@"roomId"];
    }
    if (toUserId) {
        [paramers safeSetObject:@(toUserId) forKey:@"toUserId"];
    }
    return [self upload:CHAT_UPLOAD_IMG_URL
              ImageData:imageData
                  Width:image.size.width
                 height:image.size.height
               Paramers:paramers
             folderName:@"file"
               fileName:[NSString stringWithFormat:@"chat_ios_img_%f",[[NSDate date] timeIntervalSince1970]]
               progress:progress
                success:success
                failure:failure];
}

-(NSURLSessionTask *)uploadChatImageData:(NSData *)imageData
                               ServerUrl:(NSString *)url
                                ImageKey:(NSString *)key
                                progress:(HttpProgress)progress
                                 success:(HttpRequestSuccess)success
                                 failure:(HttpRequestFailed)failure{
    UIImage *image = [UIImage imageWithSmallGIFData:imageData scale:1.0];
    NSMutableDictionary *paramers = @{}.mutableCopy;
    [paramers safeSetObject:key forKey:@"key"];
    return [self uploadServerUrl:url
                          APIurl:CHAT_UPLOAD_IMG_URL
                       ImageData:imageData
                           Width:image.size.width
                          height:image.size.height
                        Paramers:paramers
                      folderName:@"file"
                        fileName:[NSString stringWithFormat:@"chat_ios_img_%f",[[NSDate date] timeIntervalSince1970]]
                        progress:progress
                         success:success
                         failure:failure];
}

-(NSURLSessionTask *)requestChatGetAvatorListRponseCache:(HttpRequestCache)responseCache
                                                 success:(HttpRequestSuccess)success
                                                 failure:(HttpRequestFailed)failure{
    return [self GET:CHAT_AVATOALIST_JSON RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:nil responseCache:responseCache success:success failure:failure];
}

-(NSURLSessionTask *)requestChatBanUserID:(NSInteger)userID
                               RoomID:(NSInteger)roomID
                                   FK:(NSString *)fk
                            MessageId:(NSInteger)messageId
                              Success:(HttpRequestSuccess)success
                              Failure:(HttpRequestFailed)failure{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters safeSetObject:@(roomID) forKey:@"roomId"];
    [parameters safeSetObject:@(messageId) forKey:@"messageId"];
    [parameters safeSetObject:fk forKey:@"fk"];
    [parameters safeSetObject:@(userID) forKey:@"userId"];
    return [self POST:CHAT_BANUSER_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}

-(NSURLSessionTask *)requestChatKikeUserID:(NSInteger)userID
                                RoomID:(NSInteger)roomID
                                    FK:(NSString *)fk
                             MessageId:(NSInteger)messageId
                               Success:(HttpRequestSuccess)success
                               Failure:(HttpRequestFailed)failure{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters safeSetObject:@(roomID) forKey:@"roomId"];
    [parameters safeSetObject:@(messageId) forKey:@"messageId"];
    [parameters safeSetObject:fk forKey:@"fk"];
    [parameters safeSetObject:@(userID) forKey:@"userId"];
    return [self POST:CHAT_KITEUSER_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}

-(NSURLSessionTask *)requestChatBanImgUserID:(NSInteger)userID
                               RoomID:(NSInteger)roomID
                                   FK:(NSString *)fk
                            MessageId:(NSInteger)messageId
                              Success:(HttpRequestSuccess)success
                              Failure:(HttpRequestFailed)failure{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters safeSetObject:@(roomID) forKey:@"roomId"];
    [parameters safeSetObject:@(messageId) forKey:@"messageId"];
    [parameters safeSetObject:fk forKey:@"fk"];
    [parameters safeSetObject:@(userID) forKey:@"userId"];
    return [self POST:CHAT_BANIMGUSER_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}

-(NSURLSessionTask *)requestChatDeleteMessageUserID:(NSInteger)userID
                                  RoomID:(NSInteger)roomID
                                      FK:(NSString *)fk
                               MessageId:(NSInteger)messageId
                                 Success:(HttpRequestSuccess)success
                                 Failure:(HttpRequestFailed)failure{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters safeSetObject:@(roomID) forKey:@"roomId"];
    [parameters safeSetObject:@(messageId) forKey:@"messageId"];
    [parameters safeSetObject:@(userID) forKey:@"userId"];
    return [self POST:CHAT_DELETEMESSAGE_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}

-(NSURLSessionTask *)requestLotteryOpenTopResultsByLotteryID:(NSInteger)lotteryID
                                               PageSize:(NSInteger)pageSize
                                          ResponseCache:(HttpRequestCache)responseCache
                                                success:(HttpRequestSuccess)success
                                                failure:(HttpRequestFailed)failure{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters safeSetObject:@(lotteryID) forKey:@"gameId"];
    [parameters safeSetObject:@(pageSize) forKey:@"topNums"];
    return [self GET:CHAT_LOTTERY_TOPRESULTES_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:responseCache success:success failure:failure];
}

-(NSURLSessionTask *)requestUpdateUserAvator:(NSString *)avatar
                                    NickName:(NSString *)nickName
                                               ResponseCache:(HttpRequestCache)responseCache
                                                     success:(HttpRequestSuccess)success
                                                     failure:(HttpRequestFailed)failure{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (avatar) {
        [parameters safeSetObject:avatar forKey:@"avatar"];
    }
    if (nickName) {
        [parameters safeSetObject:nickName forKey:@"nickName"];
    }
    return [self POST:CHAT_UPDATE_USERINFO_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:responseCache success:success failure:failure];
}

-(NSURLSessionTask *)requestChatUserInfoWithUserID:(NSInteger)userID
                                   RoomID:(NSInteger)roomID
                                  Success:(HttpRequestSuccess)success
                                  Failure:(HttpRequestFailed)failure{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters safeSetObject:@(userID) forKey:@"userId"];
    [parameters safeSetObject:@(roomID) forKey:@"roomId"];
    return [self POST:CHAT_USERINFO_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}

-(NSURLSessionTask *)requestChatbeelLotteryWithorderNo:(NSString *)orderNo
                                           PlayCode:(NSString *)playCode
                                         TotalMoney:(NSNumber *)totalMoney
                                             UserId:(NSInteger)userId
                                                RoomID:(NSInteger)roomId
                                           Success:(HttpRequestSuccess)success
                                           Failure:(HttpRequestFailed)failure{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (orderNo) {
        [parameters safeSetObject:orderNo forKey:@"orderNo"];
    }
    if (playCode) {
        [parameters safeSetObject:playCode forKey:@"playCode"];
    }
    if (totalMoney) {
        [parameters safeSetObject:totalMoney forKey:@"totalMoney"];
    }
    [parameters safeSetObject:@(roomId) forKey:@"roomId"];
    [parameters safeSetObject:@(userId) forKey:@"userId"];
    return [self POST:CHAT_HEELBET_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

-(NSURLSessionTask *)requestChatMessageListWithIdentity:(NSInteger)identity
                                              RoomId:(NSInteger)roomId
                                            PageSize:(NSInteger)size
                                               Success:(HttpRequestSuccess)success
                                               Failure:(HttpRequestFailed)failure{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters safeSetObject:@(identity) forKey:@"messageId"];
    [parameters safeSetObject:@(roomId) forKey:@"roomId"];
    [parameters safeSetObject:@(size) forKey:@"size"];
    return [self POST:Chat_MESSAGELIST_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

-(NSURLSessionTask *)requestRemoveAllMessageWithIdentity:(NSInteger)identity
                                                 RoomId:(NSInteger)roomId
                                                Success:(HttpRequestSuccess)success
                                                Failure:(HttpRequestFailed)failure{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters safeSetObject:@(identity) forKey:@"messageId"];
    [parameters safeSetObject:@(roomId) forKey:@"roomId"];
    return [self POST:CHAT_REMOVEALL_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

-(NSURLSessionTask *)requestChatSlideMenusSuccess:(HttpRequestSuccess)success
                                           Failure:(HttpRequestFailed)failure{
    return [self GET:CHAT_SLIDEMENU_URL
         RequestType:PPSerializerType_HTTP
        ResponseType:PPSerializerType_HTTP
          parameters:nil
       responseCache:nil
             success:success
             failure:failure];
}

-(NSURLSessionTask *)requestChatDelegateResponseCache:(HttpRequestCache)responseCache
                                              Success:(HttpRequestSuccess)success
                                          Failure:(HttpRequestFailed)failure{
    return [self GET:CHAT_DELEGATE_URL
         RequestType:PPSerializerType_HTTP
        ResponseType:PPSerializerType_HTTP
          parameters:nil
       responseCache:responseCache
             success:success
             failure:failure];
}



-(NSURLSessionTask *)requestTheGameOpenTimeByLotteryID:(NSString *)lotterytID
                                         ResponseCache:(HttpRequestCache)responseCache
                                                     success:(HttpRequestSuccess)success
                                                     failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *paramers = @{}.mutableCopy;
    if (lotterytID) {
        [paramers safeSetObject:lotterytID forKey:@"gameId"];
    }
    return [self GET:TheGameOpenTime
         RequestType:PPSerializerType_HTTP
        ResponseType:PPSerializerType_JSON
          parameters:paramers
       responseCache:responseCache
             success:success failure:failure];
}

-(NSURLSessionTask *)requestGetRedDetailByRedId:(NSString *)redId
                                         ResponseCache:(HttpRequestCache)responseCache
                                               success:(HttpRequestSuccess)success
                                               failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *paramers = @{}.mutableCopy;
    if (redId.length) {
        [paramers safeSetObject:redId forKey:@"hongbaoid"];
    }
    return [self GET:CHAT_REDDETAIL_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:paramers responseCache:responseCache success:success failure:failure];
}

-(NSURLSessionTask *)requestTopListByRoomId:(NSInteger)roomId
                                  ResponseCache:(HttpRequestCache)responseCache
                                        success:(HttpRequestSuccess)success
                                        failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *paramers = @{}.mutableCopy;
    if (roomId) {
        [paramers safeSetObject:@(roomId) forKey:@"id"];
    }
    return [self GET:CHAT_TOPLIST_URL
         RequestType:PPSerializerType_HTTP
        ResponseType:PPSerializerType_JSON
          parameters:paramers
       responseCache:responseCache
             success:success
             failure:failure];
}

- (NSURLSessionTask *)requestSendChatErrorData:(NSDictionary *)errorData
                               success:(HttpRequestSuccess)success
                               failure:(HttpRequestFailed)failure{

    NSString *crashJsonStr = [errorData jk_JSONString];
    if (!crashJsonStr) {
        return nil;
    }
    AFHTTPSessionManager *manager = [self createAFHTTPSessionManagerByRequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON];
    return [manager POST:LOG_SEND_URL
              parameters:@{@"code":[NSString stringWithFormat:@"%@_CHAT_LOG",BET_CONFIG.userSettingData.defaultAppID],
                           @"log":crashJsonStr,
                           @"version":[NSString stringWithFormat:@"%li",BET_CONFIG.buildVersion]}
                progress:nil
                 success:^(NSURLSessionDataTask *_Nonnull task, id _Nonnull responseObject) {
                     
                 }
                 failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
                     
                 }];
}

- (NSURLSessionTask *)requestGetEmojiListWithUrl:(NSString *)url
                                             Key:(NSString *)key
                                         KeyWord:(NSString *)keyword
                                            Page:(NSUInteger)page
                                            Rows:(NSUInteger)rows
                               success:(HttpRequestSuccess)success
                               failure:(HttpRequestFailed)failure{
    
    AFHTTPSessionManager *manager = [self createAFHTTPSessionManagerByRequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON];
    
    NSMutableDictionary *paramers = @{}.mutableCopy;
    if (keyword.length) {
        [paramers safeSetObject:keyword forKey:@"keyword"];
    }
    [paramers safeSetObject:key forKey:@"key"];
    [paramers safeSetObject:@(page) forKey:@"page"];
    [paramers safeSetObject:@(rows) forKey:@"rows"];
    return [manager GET:url
              parameters:paramers
                progress:nil
                 success:^(NSURLSessionDataTask *_Nonnull task, id _Nonnull responseObject) {
        success ? success(responseObject) : nil;
                 }
                 failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
        failure ? failure(error) : nil;
                 }];
}

@end
