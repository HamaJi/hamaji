//
//  ChatUtility.h
//  Bet365
//
//  Created by HHH on 2018/9/16.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChatInfoModel.h"
#import "Message.h"
#import "UploadImgModel.h"
#import "ChatSlideMenuModel.h"
#import "LotteryOpenInfoModel.h"
#import "WebsocketStompKit.h"
#import <MulticastDelegate/GCDMulticastDelegate.h> 
#import "ChatLotteryTopResultModel.h"
#import "ChatUtilityGroupSubscribeData.h"
#import "ChatUtilityPrivateSubscribeData.h"
#import "ChatSendModelHeader.h"
#import "ChatSuspendDelegateModel.h"
#import "ChatRedPacketModel.h"
#import "ChatTopListModel.h"
#import "NetEmojiModel.h"

#define CHAT_UTIL [ChatUtility shareInstance]
#define kRedPackKey(r,u,e)   ([NSString stringWithFormat:@"roomid:%li,userid:%li,redId:%@",r,u,e])

#define CHAT_NOTIFCATION_KEY            @"ChatNotifcationKey"
#define CHAT_NOTIFCATION_IDENTIFIER     @"ChatNotifcationIdentifier"

typedef NS_ENUM(NSUInteger, ChatUtilityConnectStatus) {
    /** 未连接*/
    ChatUtilityConnectStatus_DISCONNECTE,
    /** 无法连接（网络问题）*/
    ChatUtilityConnectStatus_UNCONNECTE,
    /** 连接中*/
    ChatUtilityConnectStatus_CONNECTING,
    /** 已连接,未加入*/
    ChatUtilityConnectStatus_CONNECTED_WAITJOIN,
    /** 已连接,正在加入房间*/
    ChatUtilityConnectStatus_CONNECTED_JOINNING,
    /** 已连接，成功加入*/
    ChatUtilityConnectStatus_CONNECTED_JOINED,
    /** 无权限加入房间*/
    ChatUtilityConnectStatus_CONNECTED_JOINFAIL
};

typedef NS_ENUM(NSUInteger, ChatUtilityClineInfoStatus) {
    /** 未请求*/
    ChatUtilityClineInfoStatus_UNREQUEST,
    /** 请求中*/
    ChatUtilityClineInfoStatus_REQUESTING,
    /** 成功*/
    ChatUtilityClineInfoStatus_SUCCESS,
    /** 失败*/
    ChatUtilityClineInfoStatus_FAIL,
};

@protocol ChatUtilityDataDelegate <NSObject>

@required

- (void)ChatUilityConnectedSuccess;

- (void)ChatUilityUpdateConnectStatus:(ChatUtilityConnectStatus)status;

- (void)ChatUilityScribeData:(ChatUtilityGroupSubscribeData *)data JoinRoomMessage:(Message*)message;

- (void)ChatUilityScribeData:(ChatUtilitySubscribeData *)data InsertMessage:(Message*)message;

- (void)ChatUilityScribeData:(ChatUtilitySubscribeData *)data RemoveMessage:(Message*)message;

- (void)ChatUilityScribeData:(ChatUtilitySubscribeData *)data ReplaceMessage:(Message *)message;

- (void)ChatUilityScribeData:(ChatUtilitySubscribeData *)data BackgroundMessage:(Message *)message;

- (void)ChatUilityScribeData:(ChatUtilitySubscribeData *)data getMessageHistory:(NSArray <Message *>*)list;

@optional

- (void)ChatUtilityJoinGroupScribeData:(ChatUtilityGroupSubscribeData *)data;

- (void)ChatUtilityJoinFaildGroupScribeData:(ChatUtilityGroupSubscribeData *)data FaildMsg:(NSString *)faildMsg;

- (void)ChatUtilityJoinPrivateScribeData:(ChatUtilityPrivateSubscribeData *)data;

- (void)ChatUtilityScribeData:(ChatUtilityGroupSubscribeData *)data UpdateUserInfo:(ChatUserInfoModel *)userInfo;

- (void)ChatUtilityScribeData:(ChatUtilityGroupSubscribeData *)data JumpPrivateChatByUserInfo:(ChatUserInfoModel *)userInfo;

- (void)ChatUtilityUpdateAllUnreadMessage:(NSArray <ChatUserInfoModel *>*)list;

@end

@interface ChatUtility : NSObject

+(instancetype)shareInstance;
@property (nonatomic,assign)ChatUtilityClineInfoStatus infoStatus;

@property (nonatomic,strong)NSString *joinErrorStr;

@property (nonatomic,strong)ChatInfoModel *clientInfo;

@property (nonatomic,strong)NSError *error;

@property (nonatomic,strong)NSMutableArray <ChatSlideMenuModel *>*slideMenulist;

@property (nonatomic,strong)ChatSuspendDelegateModel *suspendModel;

@property (nonatomic,assign)ChatUtilityConnectStatus connectStatus;


#pragma mark - WebSocket

-(void)webSocketClose;

-(void)webSocketResetConnect;

-(void)webSocketConnectCompleted:(void(^)(BOOL successe))completed;

-(void)webSocketReconnectCompleted:(void(^)(BOOL successe))completed;

-(ChatUtilityGroupSubscribeData *)webSocketSubsribeGroup:(NSInteger)roomId;

-(ChatUtilityPrivateSubscribeData *)webSocketSubsribeUserOnlineStatus:(NSInteger)userId;

-(void)webSocketSendMessageByChatSendModel:(ChatSendModel *)model Subscribe:(ChatUtilitySubscribeData *)data;

-(void)webSocketRemoveMessage:(Message *)message Data:(ChatUtilitySubscribeData *)data;

-(void)updateAllUnreadMessageCount:(NSInteger)count FromePrivateSubscribeData:(ChatUtilityPrivateSubscribeData *)data;

-(NSInteger)getUnreadMessageCountByUserId:(NSInteger)uerId;

-(ChatUtilityPrivateSubscribeData *)getPrivateSubsribeDataByUserId:(NSInteger)uerId;

-(void)removeUnreadMessagePrivateUser:(ChatUserInfoModel *)userModel;

-(void)clean;

-(void)cleanNotiUnreadCount;

-(void)getImageKey:(void(^)(NSString *key))handle;

#pragma mark - MulticastDelegateMethod

-(void)addChatUtilityDelegate:(id)delegate;

-(void)removeChatUtilityDelegate:(id)delegate;

#pragma mark - NetRequest
-(void)getChatClienInfo:(void(^)(ChatInfoModel *info))success;

-(void)getSuspendDelegateJson:(void(^)(ChatSuspendDelegateModel *model))success;

-(void)getChatClienInfoSlideMenus:(void(^)(NSArray <ChatSlideMenuModel *>*list))success;

-(__kindof NSURLSessionTask *)uploadImageData:(NSData *)imageData
                                    Completed:(void(^)(NSDictionary *imageInfo))completed;

-(__kindof NSURLSessionTask *)updateUserAvator:(NSString *)avatar
                                      NickName:(NSString *)nickName
                                     Completed:(void(^)(BOOL success))completed;

-(__kindof NSURLSessionTask *)getAvatorListCompleted:(void(^)(NSDictionary *data))completed;

-(__kindof NSURLSessionTask *)getAllOpenInfoAtRoom:(NSInteger)roomID Completed:(void(^)(NSMutableArray <LotteryOpenInfoModel *>* modelList))completed;

-(__kindof NSURLSessionTask *)getOpenInfoByLotteryID:(NSString *)lotteryID Completed:(void(^)(LotteryOpenInfoModel * model))completed;

-(__kindof NSURLSessionTask *)chatUntilBanUserByUserID:(NSInteger)userID RoomID:(NSInteger)roomID FK:(NSString *)fk MessageID:(NSInteger)messageID Completed:(void(^)(BOOL success))completed;

-(__kindof NSURLSessionTask *)chatUntilBanImgByUserID:(NSInteger)userID RoomID:(NSInteger)roomID FK:(NSString *)fk MessageID:(NSInteger)messageID Completed:(void(^)(BOOL success))completed;

-(__kindof NSURLSessionTask *)chatUntilKikeUserByUserID:(NSInteger)userID RoomID:(NSInteger)roomID FK:(NSString *)fk MessageID:(NSInteger)messageID Completed:(void(^)(BOOL success))completed;

-(__kindof NSURLSessionTask *)getUserInfoWithUserID:(NSInteger)userID RoomID:(NSInteger)roomID Completed:(void(^)(ChatUserInfoModel *model))completed;

-(__kindof NSURLSessionTask *)submitChatbeelLotteryWithUserID:(NSInteger)userID
                                                      orderNo:(NSString *)orderNo
                                                     PlayCode:(NSString *)playCode
                                                   TotalMoney:(NSNumber *)totalMoney
                                                       RoomID:(NSInteger )roomId
                                                    Completed:(void(^)(BOOL success))completed;
-(__kindof NSURLSessionTask *)getRedDetailByMessage:(Message *)message
                                          Completed:(void(^)(ChatRedPacketModel *model))completed;

-(__kindof NSURLSessionTask *)getTopListByRoomId:(NSInteger)roomId
                                       Completed:(void(^)(NSArray <ChatTopListModel *>*list))completed;

-(__kindof NSURLSessionTask *)removeAllMessageListByRoomId:(NSInteger)roomId Message:(Message *)message;

-(__kindof NSURLSessionTask *)getLotteryOpenHistoryByLotteryID:(NSInteger)lotteryID
                                                      PageSize:(NSInteger)pageSize
                                                     Completed:(void(^)(NSArray <ChatLotteryTopResultModel *>*list))completed;

-(__kindof NSURLSessionTask *)getEmojiListWithKey:(NSString *)key KeyWord:(NSString *)keyword Page:(NSUInteger)page Rows:(NSUInteger)rows completed:(void(^)(NSArray <NetEmojiModel *> *list))completed;
@end
