
//
//  ChatUtility.m
//  Bet365
//
//  Created by HHH on 2018/9/16.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "ChatUtility.h"
#import "NetWorkMannager+lottery.h"
#import "NetWorkMannager+Chat.h"
#import "NetWorkMannager+Home.h"
#import "ChatGroupViewController.h"
#import "PresentingAlphaAnimator.h"
#import "DismissingAlphaAnimator.h"
#import <EBBannerView/EBBannerView.h>
#import <UserNotifications/UserNotifications.h>
#import <AudioToolbox/AudioToolbox.h>

#define kInfoKey ([NSString stringWithFormat:@"CHAT_INFO_KEY;%@,",USER_DATA_MANAGER.userInfoData.account],USER_DATA_MANAGER.userInfoData.token)

#define kGroupSubscribeMaxCount 1

#define kPrivateSubscribeMaxCount 999999

#define kImageKeyOverdueCycleName @"kImageKeyOverdueCycleName"


#define kImageKeyOverdueMaxTime 60*45

@interface ChatUtility()
<STOMPClientDelegate,
TimerManagerDelegate>

@property (nonatomic,strong)STOMPClient *client;

@property (nonatomic,assign)NSTimeInterval reConnectTime;

@property (nonatomic,strong)GCDMulticastDelegate<ChatUtilityDataDelegate> *multicastDelegate;

@property (strong,nonatomic)STOMPSubscription *systemSubscription;

@property (strong,nonatomic)STOMPSubscription *requestSubscription;

@property (strong,nonatomic)STOMPSubscription *privateSubscription;

@property (strong,nonatomic)NSMutableArray <ChatUtilityGroupSubscribeData *>*groupSubscribeList;

@property (strong,nonatomic)NSMutableArray <ChatUtilityPrivateSubscribeData *>*privatesubScribeList;

@property (strong,nonatomic)NSMutableArray <ChatSendRequestModel *>*utilRequestSendingList;

@property (strong,nonatomic)NSMutableArray <STOMPClient *>*disConectClientList;

@property (assign,nonatomic)NSInteger notiUnreadCount;

@property (nonatomic,strong)NSMutableArray <ChatUserInfoModel *>*unreadPrivateMessageUserList;

@property (nonatomic,strong)NSMutableArray *messageListFromNoneUser;

@property (nonatomic,strong)AFHTTPSessionManager *manager;


@property (nonatomic,strong) NSString *imageKey;

@property (nonatomic,copy) void(^imageKeyPickHandle)(NSString *imageKey);
@end

@implementation ChatUtility

+(instancetype)shareInstance{
    static ChatUtility *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[ChatUtility alloc] init];
    });
    return instance;
}

-(instancetype)init{
    if (self = [super init]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            @weakify(self);
            /** 网络状态 **/
            [NET_DATA_MANAGER checkNetworkStatusWithBlock:^(PPNetworkStatus status) {
                @strongify(self)
                if (self.clientInfo) {
                    if ([NET_DATA_MANAGER currentNetworkStatus]) {
                        [self webSocketResetConnect];
                    }else{
                        self.connectStatus = ChatUtilityConnectStatus_DISCONNECTE;
                        self.reConnectTime = 64;
                    }
                }
            }];

            
            [[RACObserve(self, connectStatus) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
                @strongify(self);
//                switch ([x integerValue]) {
//                    case ChatUtilityConnectStatus_DISCONNECTE:
//                        GT_OC_OUT_SET(@"CHAT", YES, @"Socket%@",@"未连接");
//                        break;
//                    case ChatUtilityConnectStatus_UNCONNECTE:
//                        GT_OC_OUT_SET(@"CHAT", YES, @"Socket%@",@"无法连接");
//                        break;
//                    case ChatUtilityConnectStatus_CONNECTING:
//                        GT_OC_OUT_SET(@"CHAT", YES, @"Socket%@",@"连接中");
//                        break;
//                    case ChatUtilityConnectStatus_CONNECTED_WAITJOIN:
//                        GT_OC_OUT_SET(@"CHAT", YES, @"Socket%@",@"已连接,未加入房间");
//                        break;
//                    case ChatUtilityConnectStatus_CONNECTED_JOINNING:
//                        GT_OC_OUT_SET(@"CHAT", YES, @"Socket%@",@"已连接,加入房间ing");
//                        break;
//                    case ChatUtilityConnectStatus_CONNECTED_JOINED:
//                        GT_OC_OUT_SET(@"CHAT", YES, @"Socket%@",@"已连接,已加入房间");
//                        break;
//                    default:
//                        break;
//                }
                
                if (self.multicastDelegate) {
                    [self.multicastDelegate ChatUilityUpdateConnectStatus:[x integerValue]];
                }
            }];
            
            [NOTI_MANAGER addObserver:self NotificationName:EBBannerViewDidClickNotification object:nil subscribeNext:^(id x) {
                /**iOS8+自定义通知弹窗点击响应*/
                [UIViewController routerJumpToUrl:kRouterChatScrollBottom];
//                [[UIViewController] routerJumpToUrl:kRouterChatScrollBottom];
            }];
            
            [[RACObserve(USER_DATA_MANAGER, isLogin) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
                @strongify(self);
                self.clientInfo = nil;
                self.infoStatus = ChatUtilityClineInfoStatus_UNREQUEST;
                [self clean];
            }];
            
            /** Socket 断连队列**/
            [[RACObserve(self, disConectClientList) merge:self.disConectClientList.rac_sequence.signal] subscribeNext:^(id  _Nullable x) {
                @strongify(self);
                [self webSocketDisConnectAll];
            }];
            
            /* 私聊未读消息 */
            [[RACObserve(self, unreadPrivateMessageUserList) merge:self.unreadPrivateMessageUserList.rac_sequence.signal] subscribeNext:^(id  _Nullable x) {
                @strongify(self);
                [self sortPrivateUserInfoList];
            }];
            
            [TIMER_MANAGER addDelegate:self];
            [TIMER_MANAGER addCycleTimerWithKey:kImageKeyOverdueCycleName andReduceScope:kImageKeyOverdueMaxTime];
        });
    }
    return self;
}

-(void)showNetError{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        if ([NET_DATA_MANAGER currentNetworkStatus] && _client && self.connectStatus < ChatUtilityConnectStatus_CONNECTING) {
            [Bet365AlertSheet showChooseAlert:@"检测到您的网络异常"
                                      Message:@"确保您的网络连接畅通，可执行【网络检测】以排查网路情况"
                                        Items:@[@"网络检测",@"取消"]
                                      Handler:^(NSInteger index) {
                                          if (index == 0) {
                                              [UIViewController routerJumpToUrl:kRouterMetrics];
                                          }
                                      }];
        }
    });
}


-(void)reSendLoadingMessageBySubscribeData:(ChatUtilitySubscribeData *)data{
    if (data.sendingList.count) {
        [data stopSendTime];
        NSMutableArray *removeMessageList = @[].mutableCopy;
        NSMutableArray *removeSendList = @[].mutableCopy;
        if ([data isKindOfClass:[ChatUtilityGroupSubscribeData class]]) {
            
            /** 禁言过滤**/
            if (![((ChatUtilityGroupSubscribeData *)data).ruleModel.isSpeak boolValue]) {
                
                [data.messageList enumerateObjectsUsingBlock:^(Message * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (obj.sendStatus != ChatMessageSendStatus_SUCCESS &&
                        ![removeMessageList containsObject:obj]) {
                        [removeMessageList addObject:obj];
                    }
                }];
                [data.sendingList enumerateObjectsUsingBlock:^(ChatSendModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if ([obj isKindOfClass:[ChatSendMessageModel class]]) {
                        if (((ChatSendMessageModel *)obj).sendStatus != ChatMessageSendStatus_SUCCESS &&
                            ![removeSendList containsObject:obj]) {
                            [removeSendList addObject:obj];
                        }
                    }
                }];
            }
            
            /** 禁图过滤**/
            if (![((ChatUtilityGroupSubscribeData *)data).ruleModel.isSendImg boolValue]){
                [data.messageList enumerateObjectsUsingBlock:^(Message * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (obj.chatType == ChatMessageType_IMG &&
                        obj.sendStatus == ChatMessageSendStatus_SENDING &&
                        ![removeMessageList containsObject:obj]) {
                        [removeMessageList addObject:obj];
                    }
                }];
                
                [((ChatUtilityGroupSubscribeData *)data).sendingList enumerateObjectsUsingBlock:^(ChatSendModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if ([obj isKindOfClass:[ChatSendMessageModel class]]) {
                        
//                        [((ChatSendMessageModel *)obj).destination isEqualToString:kChatSendMessageTextGroup] || [((ChatSendMessageModel *)obj).destination isEqualToString:kChatSendMessageTextPrivate]
//
//
                        if ( ((ChatSendMessageModel *)obj).messageType == ChatMessageType_IMG &&
                            ((ChatSendMessageModel *)obj).sendStatus == ChatMessageSendStatus_SENDING &&
                            ![removeSendList containsObject:obj]) {
                            [removeSendList addObject:obj];
                        }
                    }
                }];
            }
        }else if ([data isKindOfClass:[ChatUtilityPrivateSubscribeData class]]){
            /** 禁言过滤**/
            if (![((ChatUtilityPrivateSubscribeData *)data).userModel.privateChat integerValue]) {
                [data.messageList enumerateObjectsUsingBlock:^(Message * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (obj.sendStatus != ChatMessageSendStatus_SUCCESS &&
                        ![removeMessageList containsObject:obj]) {
                        [removeMessageList addObject:obj];
                    }
                }];
                [data.sendingList enumerateObjectsUsingBlock:^(ChatSendModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if ([obj isKindOfClass:[ChatSendMessageModel class]]) {
                        if (((ChatSendMessageModel *)obj).sendStatus != ChatMessageSendStatus_SUCCESS &&
                            ![removeSendList containsObject:obj]) {
                            [removeSendList addObject:obj];
                        }
                    }
                }];
            }
        }
        [data removeMessages:removeMessageList];
        [data removeSendModels:removeSendList];
        
        /** 未发送完成（仅loading状态）的消息，重新提交**/
        NSMutableArray *mList = @[].mutableCopy;
        [data.sendingList enumerateObjectsUsingBlock:^(ChatSendModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isKindOfClass:[ChatSendMessageModel class]]) {
                if (((ChatSendMessageModel *)obj).sendStatus == ChatMessageSendStatus_SENDING){
                    [mList addObject:obj];
                }
            }
            
        }];
        if (mList.count) {
            dispatch_queue_t queue = dispatch_queue_create("net.sendMessagelist.testQueue", DISPATCH_QUEUE_SERIAL);
            dispatch_sync(queue, ^{
                [mList enumerateObjectsUsingBlock:^(ChatSendMessageModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (obj.sendStatus == ChatMessageSendStatus_SENDING) {
                        [self webSocketSendMessageByChatSendModel:obj Subscribe:data];
                        [NSThread sleepForTimeInterval:2.1];
                    }
                }];
                [data starSendTime];
            });
        }else{
            [data starSendTime];
        }
    }
}


#pragma mark - WebSocket
-(void)webSocketClose{
    
    self.connectStatus = ChatUtilityConnectStatus_DISCONNECTE;
    
    if (![NET_DATA_MANAGER currentNetworkStatus]) {
        return;
    }
    
    [self.groupSubscribeList enumerateObjectsUsingBlock:^(ChatUtilitySubscribeData * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.defaultSubscription) {
            [obj.defaultSubscription unsubscribe];
            obj.defaultSubscription = nil;
        }
    }];
    
    [self.privatesubScribeList enumerateObjectsUsingBlock:^(ChatUtilitySubscribeData * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.defaultSubscription) {
            [obj.defaultSubscription unsubscribe];
            obj.defaultSubscription = nil;
        }
    }];
    
    if (self.requestSubscription) {
        [self.requestSubscription unsubscribe];
        self.requestSubscription = nil;
    }
    
    if (self.privateSubscription) {
        [self.privateSubscription unsubscribe];
        self.privateSubscription = nil;
    }
    
    if (self.systemSubscription) {
        [self.systemSubscription unsubscribe];
        self.systemSubscription = nil;
    }

    if (![self.disConectClientList containsObject:self.client]) {
        [[self mutableArrayValueForKey:@"disConectClientList"] addObject:self.client];
    }
    
    self.client = nil;

}


-(void)clean{
    [self webSocketClose];
    self.clientInfo = nil;
    self.groupSubscribeList = nil;
    self.privatesubScribeList = nil;
    [self cleanNotiUnreadCount];
}

-(void)cleanNotiUnreadCount{
    self.notiUnreadCount = 0;
}

-(void)webSocketResetConnect{
    self.reConnectTime = 0;
    [self webSocketReconnectCompleted:nil];
}

-(void)webSocketConnectCompleted:(void(^)(BOOL successe))completed{

    
    NSString *token = [USER_DATA_MANAGER.userInfoData.token stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding].length ? [USER_DATA_MANAGER.userInfoData.token stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] : @"";
    NSString *ybcsid = CHAT_UTIL.clientInfo.ybcsid.length ? CHAT_UTIL.clientInfo.ybcsid : @"";
    
    if (!CHAT_UTIL.clientInfo.chatUrl.length) {
        self.connectStatus = ChatUtilityConnectStatus_UNCONNECTE;
        completed ? completed(NO) : nil;
        return;
    }
    if (!NET_DATA_MANAGER.currentNetworkStatus) {
        self.reConnectTime = 64;
        self.connectStatus = ChatUtilityConnectStatus_UNCONNECTE;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self showNetError];
        });
        completed ? completed(NO) : nil;
        return;
    }
    
    self.connectStatus = ChatUtilityConnectStatus_CONNECTING;
    SubmitLogModel *log = [[SubmitLogModel alloc] init];
    log.events = LOG_CHAT_SOCKETCONNECT;
    
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSArray * chatDomains = self.clientInfo.nodes;
        if(chatDomains && chatDomains.count > 0){
            __block BOOL _isChatUrlSet = NO;
            __block int _failureTimes = 0;
            dispatch_semaphore_t sema = dispatch_semaphore_create(0);
            
            for(NSString * chatDomain in chatDomains){
                [self.manager GET:[NSString stringWithFormat:@"%@%@", chatDomain, @"/image/empty.png"] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    if (!_isChatUrlSet) {
                        _isChatUrlSet = YES;
                        self.clientInfo.chatUrl = chatDomain;
                        dispatch_semaphore_signal(sema);
                    }
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    _failureTimes++;
                    if(_failureTimes == chatDomains.count){
                        dispatch_semaphore_signal(sema);
                    }
                }];
            }
            
            dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        }
    });
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        @weakify(self);
        [self.client connectWithHeaders:@{
                                          @"token":token,
                                          @"ybcsid":ybcsid,
                                          @"client":[NSString stringWithFormat:@"iOS_%li",BET_CONFIG.buildVersion]
                                          }
                      completionHandler:^(STOMPFrame *connectedFrame, NSError *error) {
                                              @strongify(self);
                                              log.endTime = [NSDate date];
                                              BOOL success = (error == nil);
                                              if (success) {
                                                  self.connectStatus = ChatUtilityConnectStatus_CONNECTED_WAITJOIN;

                                                  [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(websocketConnectedSuccess) object:nil];
                                                  [self performSelector:@selector(websocketConnectedSuccess) withObject:nil afterDelay:1.0];
                                                  
                                                  log.content = @"SUCCESS";
    //                                              GT_OC_OUT_SET(@"CHAT", YES, @"socket连接成功 duration = %.2f", log.duration);
                                              }else{
                                                  self.connectStatus = ChatUtilityConnectStatus_DISCONNECTE;
                                                  [self webSocketReconnectCompleted:completed];
                                                  log.content = @"FAIL";
    //                                              GT_OC_OUT_SET(@"CHAT", YES, @"socket连接失败 duration = %.2f", log.duration);
                                              }
                                              if (log.duration >= 10) {
                                                  [BAI_CLOUD_MANAGER sendObject:log BucketName:Baidu_LOG_BUCKET_PATCH Success:^{
                                                      
                                                  } Fail:^(NSError *error) {
                                                      
                                                  }];
                                              }
                                              
                                              completed ? completed(success) : nil;
                                              success ? (self.reConnectTime = 0 ): NO;
                                          }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(30.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (self.connectStatus == ChatUtilityConnectStatus_CONNECTING) {
                log.content = @"TIMEOUT";
                log.endTime = [NSDate date];
                if (log.duration >= 10) {
                    [BAI_CLOUD_MANAGER sendObject:log BucketName:Baidu_LOG_BUCKET_PATCH Success:^{
                        
                    } Fail:^(NSError *error) {
                        
                    }];
                }
    //            GT_OC_OUT_SET(@"CHAT", YES, @"socket连接失败超时 duration = %.2f", log.duration);
                [self webSocketReconnectCompleted:nil];
            }
        });
    });
}

-(void)webSocketReconnectCompleted:(void(^)(BOOL successe))completed{
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(pri_webSocketReconnectCompleted:) object:completed];
    [self performSelector:@selector(pri_webSocketReconnectCompleted:) withObject:completed afterDelay:2.0];
}

-(void)pri_webSocketReconnectCompleted:(void(^)(BOOL successe))completed{
    [self webSocketClose];
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.reConnectTime >= 64) {
            self.connectStatus = ChatUtilityConnectStatus_UNCONNECTE;
            return;
        }
        NSLog(@"Clinet >>> be ReConnecting !!! ");
        [self webSocketConnectCompleted:completed];
        
        if (self.reConnectTime == 0) {
            self.reConnectTime = 2;
        }else{
            self.reConnectTime *= 2;
        }
    });
}

- (void)websocketConnectedSuccess{

    [self bindingSubScribe];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.multicastDelegate) {
            [self.multicastDelegate ChatUilityConnectedSuccess];
        }
    });
}

-(void)bindingSubScribe{
    
    if (self.connectStatus < ChatUtilityConnectStatus_CONNECTING) {
        [CHAT_UTIL webSocketResetConnect];
        return;
    }else if (self.connectStatus < ChatUtilityConnectStatus_CONNECTED_WAITJOIN){
        return;
    }
    [self webSocketSubsribeRequest];
    [self webSocketSubscribeSystem];
    [self webSocketSubscribePrivate];
    self.connectStatus = ChatUtilityConnectStatus_CONNECTED_JOINNING;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self getImageKey:^(NSString *key) {
            
        }];
    });
}

-(ChatUtilityGroupSubscribeData *)webSocketSubsribeGroup:(NSInteger)roomId{
    __block ChatUtilityGroupSubscribeData *groupData;
    
    if (self.connectStatus < ChatUtilityConnectStatus_CONNECTED_WAITJOIN) {
        return nil;
    }
    
    [self.groupSubscribeList enumerateObjectsUsingBlock:^(ChatUtilityGroupSubscribeData * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.identifier == roomId) {
            groupData = obj;
        }
    }];
    
    if (!groupData) {
        groupData = [[ChatUtilityGroupSubscribeData alloc] init];
        groupData.identifier = roomId;
        
        [self.groupSubscribeList addObject:groupData];
        
        if (self.groupSubscribeList.count > kGroupSubscribeMaxCount) {
            ChatUtilityGroupSubscribeData *oldData = [self.groupSubscribeList firstObject];
            if (oldData.defaultSubscription) {
                [oldData.defaultSubscription unsubscribe];
                oldData.defaultSubscription = nil;
            }
            [self.groupSubscribeList removeObject:oldData];
        }
    }
    
    ChatSendRequestGroupModel *joinSend = [ChatSendRequestGroupModel creatSendModel:kChatSendRequestJoinGroup Date:[NSDate date]];
    joinSend.roomId = groupData.identifier;
    [self webSocketSendMessageByChatSendModel:joinSend Subscribe:groupData];
    
    __block ChatRoomInfoModel *roomInfo = nil;
    [self.clientInfo.roomInfoList enumerateObjectsUsingBlock:^(ChatRoomInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.identity integerValue] == groupData.identifier) {
            roomInfo = obj;
            *stop = YES;
        }
    }];
    
    if (roomInfo.welcomeMsg.length) {
        NSError *error;
        Message *welcomMsg = [MTLJSONAdapter modelOfClass:[Message class]
                                       fromJSONDictionary:@{
                                                            @"id":@(kGroupWelcomMsgIdentity),
                                                            @"content":@{@"text":roomInfo.welcomeMsg},
                                                            @"chatType":@"TextMessage",
                                                            @"userId":@(0)
                                                            }
                                                    error:&error];
//        welcomMsg.curTime = [NSDate date];
        NSDate *date = [NSDate date];
        NSTimeZone *zone = [NSTimeZone systemTimeZone];
        NSInteger interval = [zone secondsFromGMTForDate: date];
        NSDate *localeDate = [date  dateByAddingTimeInterval: interval];
        welcomMsg.curTime = localeDate;
        [groupData addMessage:welcomMsg];
    }
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        if (self.connectStatus <= ChatUtilityConnectStatus_CONNECTED_JOINNING) {
//            [SVProgressHUD showErrorWithStatus:@"加入房间失败，重新请求中"];
//            [self webSocketResetConnect];
//        }
//
//    });
    
    return groupData;

}

-(ChatUtilityPrivateSubscribeData *)webSocketSubsribeUserOnlineStatus:(NSInteger)userId{
    __block ChatUtilityPrivateSubscribeData *data;
    
    if (self.connectStatus < ChatUtilityConnectStatus_CONNECTED_WAITJOIN) {
        return nil;
    }
    
    [self.privatesubScribeList enumerateObjectsUsingBlock:^(ChatUtilityPrivateSubscribeData * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.identifier == userId) {
            data = obj;
        }
    }];
    
    if (!data) {
        data = [[ChatUtilityPrivateSubscribeData alloc] init];
        data.identifier = userId;
        ChatUserInfoEntity *entity = [ChatUtilityPrivateSubscribeData getUserInfoListEntityByUserId:data.identifier].firstObject;
        
        NSError *error;
        data.userModel = [MTLManagedObjectAdapter modelOfClass:[ChatUserInfoModel class] fromManagedObject:entity error:&error];
        [self.privatesubScribeList addObject:data];
        
        if (self.privatesubScribeList.count > kPrivateSubscribeMaxCount) {
            ChatUtilityPrivateSubscribeData *oldData = [self.privatesubScribeList firstObject];
            if (oldData.defaultSubscription) {
                [oldData.defaultSubscription unsubscribe];
                oldData.defaultSubscription = nil;
            }
            [self.privatesubScribeList removeObject:oldData];
        }
    }

    ChatSendRequestPrivateModel *historyModel = [ChatSendRequestPrivateModel creatSendModel:kChatSendRequestHistoryPrivate Date:[NSDate date]];
    historyModel.userId = userId;
    [CHAT_UTIL webSocketSendMessageByChatSendModel:historyModel Subscribe:data];
    
    data.defaultSubscription = [self.client subscribeTo:[NSString stringWithFormat:@"/server/user/%li",data.identifier]
                                                headers:@{}
                                         messageHandler:^(STOMPMessage *message) {
                                             NSError *error;
                                             NSDictionary *body = [message.body mj_JSONObject];
                                             Message *messageBody = [MTLJSONAdapter modelOfClass:[Message class] fromJSONDictionary:body error:&error];
                                             [data updateUserLineStatuByMessage:messageBody];
                                         }];
    return data;
}
-(void)webSocketSubscribeSystem{
    
    if (self.systemSubscription) {
        [self.systemSubscription unsubscribe];
        self.systemSubscription = nil;
    }
    
    @weakify(self);
    self.systemSubscription = [self.client subscribeTo:[NSString stringWithFormat:@"/user/%@/v2_queue",CHAT_UTIL.clientInfo.userInfo.fk]
                            headers:@{}
                     messageHandler:^(STOMPMessage *message) {
                         @strongify(self);
                         NSDictionary *body = [message.body mj_JSONObject];
                         __block Message *messageModel;
                         
                         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                             NSError *error;
                             messageModel = [MTLJSONAdapter modelOfClass:[Message class] fromJSONDictionary:body error:&error];
                             
                             __block ChatUtilityGroupSubscribeData *data = nil;
                             [self.groupSubscribeList enumerateObjectsUsingBlock:^(ChatUtilityGroupSubscribeData * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                 if (obj.identifier == messageModel.content.roomId) {
                                     data = obj;
                                     *stop = YES;
                                 }
                             }];
                             
                             if (messageModel.chatFrom == ChatMessageFrom_ME &&
                                 messageModel.chatType == ChatMessageType_REDCOLLOAR) {
//                                 GT_OC_OUT_SET(@"CHAT", YES, @"领取红包成功 date:%@",[messageModel.curTime getTimeFormatStringWithFormat:@""]);
                                 
                                 
                                 /**草tmd如果是红包消息，还tmd是被劳资开了，那劳资就要数据库记录一下下，没问题吧？**/
                                 NSManagedObjectContext *contex = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_defaultContext]];
                                 RedPackEntity *entity = [RedPackEntity MR_createEntityInContext:contex];
                                 [contex MR_saveWithBlockAndWait:^(NSManagedObjectContext * _Nonnull localContext) {
                                     
                                     RedPackEntity *localentity = [entity MR_inContext:localContext];;
                                     localentity.userID = [messageModel.userId integerValue];
                                     localentity.redpackID = messageModel.content.redCollarPacketId;
                                     localentity.roomID = messageModel.content.roomId;
                                     localentity.timeStamp = [[NSDate date] timeIntervalSince1970];
                                 }];
                                 [contex MR_saveToPersistentStoreAndWait];
                                 [data.messageList enumerateObjectsUsingBlock:^(Message * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                     if (obj.content.redPacketId == messageModel.content.redCollarPacketId) {
                                         obj.hasRedPack = YES;
                                     }
                                 }];
                             }
                             
                             /** 权限消息**/
                             if (messageModel.chatType == ChatMessageType_RULE) {
                                 data.ruleModel.isKick = [NSNumber numberWithBool:messageModel.content.isKick];
                                 data.ruleModel.isGag = [NSNumber numberWithBool:messageModel.content.isGag];
                                 data.ruleModel.isBanImg = [NSNumber numberWithBool: messageModel.content.isBanImg];
                                 data.ruleModel.isDelMessage = [NSNumber numberWithBool:messageModel.content.isDelMessage];
                                 data.ruleModel.isSendImg =  [NSNumber numberWithBool:messageModel.content.isSendImg];
                                 
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     if (self.multicastDelegate) {
                                         [self.multicastDelegate ChatUilityScribeData:data BackgroundMessage:messageModel];
                                     }
                                 });
                                 return;
                             }
                             
                             /** 踢出消息**/
                             if (messageModel.chatType == ChatMessageType_KICK){
                                 [self.clientInfo.roomInfoList enumerateObjectsUsingBlock:^(ChatRoomInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                     if (messageModel.content.roomId == [obj.identity integerValue]) {
                                         [self.clientInfo.roomInfoList removeObject:obj];
                                         *stop = YES;
                                     }
                                 }];
                                 [data.defaultSubscription unsubscribe];
                                 data.defaultSubscription = nil;
                                 if (self.multicastDelegate) {
                                     [self.multicastDelegate ChatUilityScribeData:data BackgroundMessage:messageModel];
//                                     [self clean];
//                                     [PPNetworkCache removeHttpCacheForKey:kInfoKey];
//                                     self.infoStatus = ChatUtilityClineInfoStatus_UNREQUEST;
                                 }
                                 return;
                             }
                             
                             if (messageModel.chatType == ChatMessageType_REDCOLLOAR_ERROR) {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     if (self.multicastDelegate) {
                                         [self.multicastDelegate ChatUilityScribeData:data BackgroundMessage:messageModel];
                                     }
                                 });
                                 return;
                             }
                             /** 删除消息**/
                             if (messageModel.chatType == ChatMessageType_REMOVE) {
                                 [data removeMessage:messageModel];
                                 [data removeUnreadMeesage:messageModel];
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     if (self.multicastDelegate) {
                                         [self.multicastDelegate  ChatUilityScribeData:data RemoveMessage:messageModel];
                                     }
                                 });
                                 return;
                             }
                             /** 禁言消息**/
                             if (messageModel.chatType == ChatMessageType_UNSPEACK) {
                                 data.ruleModel.isSpeak = [NSNumber numberWithBool:NO];
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     if (self.multicastDelegate) {
                                         [self.multicastDelegate ChatUilityScribeData:data RemoveMessage:messageModel];
                                     }
                                 });
                                 return;
                             }
                             
                             /** 禁图消息**/
                             if (messageModel.chatType == ChatMessageType_UNIMAGE) {
                                 data.ruleModel.isSendImg = [NSNumber numberWithBool:NO];
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     if (self.multicastDelegate) {
                                         [self.multicastDelegate  ChatUilityScribeData:data RemoveMessage:messageModel];
                                     }
                                 });
                                 return;
                             }
                             
                             /** 系统消息（发送失败,其他异常）**/
                             if (messageModel.chatType == ChatMessageType_SYSTEM){
                                 
                                 ChatSendMessageModel *sendModel = (ChatSendMessageModel *)[data getSendingListObjectBy:messageModel.content.sendId];
                                 sendModel.sendStatus = ChatMessageSendStatus_FAIL;

                                 if (sendModel) {
//                                     GT_OC_OUT_SET(@"CHAT", YES, @"消息发送失败 duration = %.2f", [[NSDate date] timeIntervalSinceDate:sendModel.date]);
                                     
                                     __block Message *replaceMessage = nil;
                                     [data.messageList enumerateObjectsUsingBlock:^(Message * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                         if ([sendModel.chatSendId isEqualToString:obj.content.sendId] &&
                                             obj.chatType == sendModel.messageType) {
                                             obj.sendStatus = ChatMessageSendStatus_FAIL;
                                             replaceMessage = obj;
                                             *stop = YES;
                                         }
                                     }];
                                     if (self.multicastDelegate) {
                                         dispatch_async(dispatch_get_main_queue(), ^{
                                             [self.multicastDelegate ChatUilityScribeData:data ReplaceMessage:replaceMessage];
                                             
                                         });
                                     }
                                 }
                             }
                             BOOL isContain = [data messageListContain:messageModel];
                             [data addMessage:messageModel];
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 if (self.multicastDelegate && data.roomInfoModel && data.ruleModel) {
                                     if (isContain) {
                                         [self.multicastDelegate ChatUilityScribeData:data ReplaceMessage:messageModel];
                                     }else{
                                         [self.multicastDelegate ChatUilityScribeData:data InsertMessage:messageModel];
                                     }
                                 }
                             });
                         });
                     }];
}

-(void)webSocketSubscribePrivate{
    if (self.privateSubscription) {
        [self.privateSubscription unsubscribe];
        self.privateSubscription = nil;
    }
    @weakify(self);
    self.privateSubscription = [self.client subscribeTo:[NSString stringWithFormat:@"/user/%@/private",self.clientInfo.userInfo.fk]
                                                headers:@{}
                                         messageHandler:^(STOMPMessage *message) {
        @strongify(self);
        NSDictionary *body = [message.body mj_JSONObject];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            __block Message *messageModel;
            __block ChatUtilityPrivateSubscribeData *data = nil;
            if (body) {
                NSError *error;
                messageModel = [MTLJSONAdapter modelOfClass:[Message class] fromJSONDictionary:body error:&error];
                [self.privatesubScribeList enumerateObjectsUsingBlock:^(ChatUtilityPrivateSubscribeData * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    
                    if ([obj.userModel.privateChatId integerValue] == [messageModel.privateChatId integerValue] ||
                        obj.identifier == [messageModel.userId integerValue]) {
                        data = obj;
                        *stop = YES;
                    }
                }];
            }
            if (!data) {
                data = [[ChatUtilityPrivateSubscribeData alloc] init];
                if (messageModel.chatFrom != ChatMessageFrom_ME) {
                    data.identifier = [messageModel.userId integerValue];
                    ChatUserInfoEntity *entity = [ChatUtilityPrivateSubscribeData getUserInfoListEntityByUserId:data.identifier].firstObject;
                    NSError *error;
                    data.userModel = [MTLManagedObjectAdapter modelOfClass:[ChatUserInfoModel class] fromManagedObject:entity error:&error];
                }
                
                
                [self.privatesubScribeList addObject:data];
                
                if (self.privatesubScribeList.count > kPrivateSubscribeMaxCount) {
                    ChatUtilityPrivateSubscribeData *oldData = [self.privatesubScribeList firstObject];
                    if (oldData.defaultSubscription) {
                        [oldData.defaultSubscription unsubscribe];
                        oldData.defaultSubscription = nil;
                    }
                    [self.privatesubScribeList removeObject:oldData];
                }
                
            }
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showNotifactionByMessage:messageModel Data:data];
                if (self.multicastDelegate) {
                    ChatSendModel *sendModel = [data getSendingListObjectBy:messageModel.content.sendId];
                    if (sendModel) {
                        /** 消息类型未自己正在发送的类型 **/
//                        GT_OC_OUT_SET(@"CHAT", YES, @"消息发送成功 duration = %.2f", [[NSDate date] timeIntervalSinceDate:sendModel.date]);
                        [data pauseSendTime];
                        [data removeSendModel:sendModel];
                        [data replaceMessage:messageModel BySendModel:sendModel];
                        [data resumeSendTime];
                        if (self.multicastDelegate) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self.multicastDelegate ChatUilityScribeData:data ReplaceMessage:messageModel];
                            });
                        }
                        
                    }else if (messageModel.chatType == ChatMessageType_REMOVE) {
                        
                        /** 删除消息**/
                        [data removeMessage:messageModel];
                        [data removeUnreadMeesage:messageModel];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.multicastDelegate  ChatUilityScribeData:data RemoveMessage:messageModel];
                        });
                        
                        if (messageModel.chatFrom == ChatMessageFrom_ME) {
                            [data addMessage:messageModel];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self.multicastDelegate ChatUilityScribeData:data InsertMessage:messageModel];
                            });
                        }
                    }else{
                        [data addMessage:messageModel];
                        [data addUnreadMessage:messageModel];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.multicastDelegate ChatUilityScribeData:data InsertMessage:messageModel];
                        });
                    }
                }
            });
        });
    }];
}

-(void)webSocketSubsribeRequest{
    if (self.requestSubscription) {
        [self.requestSubscription unsubscribe];
        self.requestSubscription = nil;
    }
    @weakify(self);
    self.requestSubscription = [self.client subscribeTo:[NSString stringWithFormat:@"/user/%@/request",self.clientInfo.userInfo.fk]
                                                headers:@{}
                                         messageHandler:^(STOMPMessage *message) {
        @strongify(self);
        NSDictionary *messageBody = [message.body mj_JSONObject];
        NSString *requestId = [messageBody objectForKey:@"requestId"];
        ChatSendRequestModel *model = [self getSendingRequestModel:requestId];
        
        __block ChatUtilitySubscribeData *data = nil;
        if ([model isKindOfClass:[ChatSendRequestGroupModel class]]) {
            [self.groupSubscribeList enumerateObjectsUsingBlock:^(ChatUtilityGroupSubscribeData * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.identifier == ((ChatSendRequestGroupModel *)model).roomId) {
                    data = obj;
                    *stop = YES;
                }
            }];
        }else if ([model isKindOfClass:[ChatSendRequestPrivateModel class]]){
            [self.privatesubScribeList enumerateObjectsUsingBlock:^(ChatUtilityPrivateSubscribeData * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.identifier == ((ChatSendRequestPrivateModel *)model).userId) {
                    data = obj;
                    *stop = YES;
                }
            }];
        }
        
        if ([model.destination isEqualToString:kChatSendRequestJoinGroup]) {
            [self joinRoom:messageBody Data:data SendModel:model];
        }else if ([model.destination isEqualToString:kChatSendRequestUserInfoPrivate]){
            [self getChatUserInfo:messageBody Data:data];
        }else if ([model.destination isEqualToString:kChatSendRequestHistoryPrivate]){
            [self getHistoryMessageList:messageBody Data:data SendModel:model];
        }else if ([model.destination isEqualToString:kChatSendRequestHistoryGroup]){
            [self getHistoryMessageList:messageBody Data:data SendModel:model];
        }else if ([model.destination isEqualToString:kChatSendRequestRevokePrivate]){
            [self revokeUserMessage:messageBody Data:data SendModel:model];
        }else if ([model.destination isEqualToString:kChatSendRequestUserInfoGroup]){
            [self getPlantUserInfoRecord:messageBody Data:data SendModel:model];
        }else if ([model.destination isEqualToString:kChatSendRequestImageKey]){
            self.imageKey = messageBody[@"response"];
        }
        [data removeSendModel:model];
    }];
}

-(void)webSocketSendMessageByChatSendModel:(ChatSendModel *)model Subscribe:(ChatUtilitySubscribeData *)data{
    if (self.connectStatus < ChatUtilityConnectStatus_CONNECTING) {
        [self webSocketResetConnect];
    }
    if ([model isKindOfClass:[ChatSendMessageModel class]]) {
        ChatSendMessageModel *sendModel = (ChatSendMessageModel *)model;
        Message *message = [[Message alloc] init];
        message.userId = [NSNumber numberWithInteger:CHAT_UTIL.clientInfo.userInfo.userId];
        message.curTime = [NSDate date];
        message.chatFrom = ChatMessageFrom_ME;
        message.sendStatus = ChatMessageSendStatus_SENDING;
        message.content = [MessageContent new];
        message.content.sendId = sendModel.chatSendId;
        
        if (sendModel.messageType == ChatMessageType_IMG) {
            
            NSDictionary *imgInfo = [[sendModel.content mj_JSONObject] objectForKey:@"imgInfo"];
            
            message.chatType = ChatMessageType_IMG;
            message.content.thumbnail = imgInfo[@"img"];
            message.content.img = imgInfo[@"img"];
            message.content.img_width = imgInfo[@"img_width"];
            message.content.thumbnail_width = imgInfo[@"sm_width"];
            message.content.img_height = imgInfo[@"img_height"];
            message.content.thumbnail_height = imgInfo[@"sm_width"];
            
//            ChatUtilityGroupSubscribeData *lastData = self.groupSubscribeList.lastObject;
//            NSInteger roomId = [sendModel isKindOfClass:[ChatSendMessageGroupModel class]] ? ((ChatSendMessageGroupModel *)sendModel).roomId : lastData.identifier;
//            NSInteger toUserId = [sendModel isKindOfClass:[ChatSendMessagePrivateModel class]] ? ((ChatSendMessagePrivateModel *)sendModel).userId : 0;
            
//            [self uploadImageData:sendModel.content RoomID:roomId SendId:sendModel.chatSendId toUserId:toUserId Completed:^(BOOL success) {
//                if (!success) {
//                    [data.messageList enumerateObjectsUsingBlock:^(Message * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                        if ([obj.content.sendId isEqualToString:sendModel.chatSendId] && self.multicastDelegate) {
//                            message.sendStatus = ChatMessageSendStatus_FAIL;
//                            if (self.multicastDelegate) {
//                                dispatch_async(dispatch_get_main_queue(), ^{
//                                    [self.multicastDelegate ChatUilityScribeData:data ReplaceMessage:message];
//                                });
//                            }
//                            *stop = YES;
//                        }
//                    }];
//                }
//            }];
            
        }else if (sendModel.messageType == ChatMessageType_TEXT){
            
            message.chatType = ChatMessageType_TEXT;
            message.content.text = [[sendModel.content mj_JSONObject] objectForKey:@"content"];
            message.content.attributedText = [MessageContent replaceEmojiByText:message.content.text];
            
        }
        
        [data addMessage:message];
        
        if (self.multicastDelegate){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.multicastDelegate ChatUilityScribeData:data InsertMessage:message];
            });
        }
    }
    if (data) {
        [data addSendModel:model];
    }
    if ([model.destination isEqualToString:kChatSendRequestImageKey]) {
        [self.utilRequestSendingList safeAddObject:model];
    }
    
    
    NSString *distination = (NSString *)model.destination;
    
    [self.client sendTo:distination
                headers:model.header
                   body:[model valueForKey:@"content"]];
    
}


-(void)webSocketRemoveMessage:(Message *)message Data:(ChatUtilitySubscribeData *)data{
    if (![message.identity integerValue]) {
        [data removeMessage:message];
        [data removeSendModelByPredicate:[NSPredicate predicateWithFormat:@"chatSendId = %@",message.content.sendId]];
        return;
    }
    if ([data isKindOfClass:[ChatUtilityGroupSubscribeData class]]) {
        if ([message.identity integerValue]) {
            [self chatUntilDeleteMessageByUserID:[message.userId integerValue]
                                          RoomID:((ChatUtilityGroupSubscribeData *)data).identifier
                                              FK:CHAT_UTIL.clientInfo.userInfo.fk
                                       MessageID:[message.identity integerValue]
                                       Completed:^(BOOL success) {
                [data removeMessage:message];
            }];
        }
    }else if ([data isKindOfClass:[ChatUtilityPrivateSubscribeData class]]){
        ChatSendRequestPrivateModel *model = [ChatSendRequestPrivateModel creatSendModel:kChatSendRequestRevokePrivate Date:[NSDate date]];
        model.userId = data.identifier;
        model.messageId = [message.identity integerValue];
        [self webSocketSendMessageByChatSendModel:model Subscribe:data];
    }
}

-(void)updateAllUnreadMessageCount:(NSInteger)count FromePrivateSubscribeData:(ChatUtilityPrivateSubscribeData *)data{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        __block NSUInteger index = NSNotFound;
        [self.unreadPrivateMessageUserList enumerateObjectsUsingBlock:^(ChatUserInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isEqual:data.userModel] || obj.userId == data.userModel.userId) {
                index = idx;
                *stop = YES;
            }
        }];
        
        Message *last = [data getLastMessage];
        data.userModel.lastNewDate = last.curTime;
        if (index == NSNotFound) {
            [[self mutableArrayValueForKey:@"unreadPrivateMessageUserList"] safeAddObject:data.userModel];
        }else{
//            [[self mutableArrayValueForKey:@"unreadPrivateMessageUserList"] exchangeObjectAtIndex:0 withObjectAtIndex:index];
            [self.unreadPrivateMessageUserList removeObjectAtIndex:index];
            [[self mutableArrayValueForKey:@"unreadPrivateMessageUserList"] insertObject:data.userModel atIndex:0];
        }
    });
}

-(NSInteger)getUnreadMessageCountByUserId:(NSInteger)uerId{
    ChatUtilityPrivateSubscribeData *data = [self getPrivateSubsribeDataByUserId:uerId];
    if (data) {
        return data.unreadCount;
    }
    return 0;
}

-(ChatUtilityPrivateSubscribeData *)getPrivateSubsribeDataByUserId:(NSInteger)uerId{
    __block ChatUtilityPrivateSubscribeData *data = nil;
    [self.privatesubScribeList enumerateObjectsUsingBlock:^(ChatUtilityPrivateSubscribeData * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.userModel.userId == uerId) {
            data = obj;
            *stop = YES;
        }
    }];
    
    if (!data) {
        data = [self webSocketSubsribeUserOnlineStatus:uerId];
    }
    return data;
}

-(void)removeUnreadMessagePrivateUser:(ChatUserInfoModel *)userModel{
    [[self mutableArrayValueForKey:@"unreadPrivateMessageUserList"] removeObject:userModel];
}
#pragma mark - Private
-(void)showJoinRoomPswInput:(ChatUtilityGroupSubscribeData *)data{
    dispatch_async(dispatch_get_main_queue(), ^{
        @weakify(self);
        [Bet365AlertSheet showInput:@"请输入房间密码"
                            Message:(data.hasSubmitPsw ? @"密码错误" : @"")
                        Placeholder:@"房间密码"
                            Handler:^(NSString *text) {
                                @strongify(self);
                                data.hasSubmitPsw = YES;
                                ChatSendRequestGroupModel *joinSend = [ChatSendRequestGroupModel creatSendModel:kChatSendRequestJoinGroup Date:[NSDate date]];
                                joinSend.roomId = data.identifier;
                                joinSend.joinRoomPsw = text;
                                [self webSocketSendMessageByChatSendModel:joinSend Subscribe:data];
                            }];
    });
    
}
-(void)receiveGroupMessage:(Message *)messageModel Data:(ChatUtilityGroupSubscribeData *)groupData{
    if (!messageModel) {
        return;
    }
    if ([messageModel.userId integerValue] == 0 ||
        ![ChatUtilitySubscribeData getUserInfoListEntityByUserId:[messageModel.userId integerValue]].count) {
        [self.messageListFromNoneUser safeAddObject:messageModel];
        //若消息的用户信息无法在本地查询，请求服务端获取会员个人信息 4.19蛤蟆吉
        ChatSendRequestGroupModel *plantUserRequest = [ChatSendRequestGroupModel creatSendModel:kChatSendRequestUserInfoGroup Date:[NSDate date]];
        plantUserRequest.roomId = groupData.identifier;
        plantUserRequest.userId = [messageModel.userId integerValue];
        [self webSocketSendMessageByChatSendModel:plantUserRequest Subscribe:groupData];
    }

    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (messageModel.chatType == ChatMessageType_REMOVEALL) {
            [groupData removeAllMessage];
            [groupData removeAllUnredaMessage];
            [groupData removeAllSendModel];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.multicastDelegate) {
                    [self.multicastDelegate ChatUilityScribeData:groupData RemoveMessage:nil];
                }
            });
            return ;
        }
        if (messageModel.chatFrom == ChatMessageFrom_ME &&
            messageModel.chatType == ChatMessageType_REDCOLLOAR) {
            /**草tmd如果是红包消息，还tmd是被劳资开了，那劳资就要数据库记录**/
            NSManagedObjectContext *contex = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_defaultContext]];
            RedPackEntity *entity = [RedPackEntity MR_createEntityInContext:contex];
            [contex MR_saveWithBlockAndWait:^(NSManagedObjectContext * _Nonnull localContext) {
                
                RedPackEntity *localentity = [entity MR_inContext:localContext];;
                localentity.userID = [messageModel.userId integerValue];
                localentity.redpackID = messageModel.content.redCollarPacketId;
                localentity.roomID = groupData.identifier;
                localentity.timeStamp = [[NSDate date] timeIntervalSince1970];
            }];
            [groupData.messageList enumerateObjectsUsingBlock:^(Message * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.content.redPacketId == messageModel.content.redCollarPacketId) {
                    obj.hasRedPack = YES;
                }
            }];
        }
        if (messageModel.chatType == ChatMessageType_JOIN) {
            NSError *userError;
            NSDictionary *userDic = [MTLJSONAdapter JSONDictionaryFromModel:messageModel.content error:&userError];
            if (!userError) {
                ChatUserInfoModel *userInfoModel = [MTLJSONAdapter modelOfClass:ChatUserInfoModel.class fromJSONDictionary:userDic error:&userError];
                [groupData updateUserInfoModel:userInfoModel];
            }
            
            
            if ([messageModel.content.showVipEnter boolValue]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.multicastDelegate ChatUilityScribeData:groupData BackgroundMessage:messageModel];
                });
            }else{
                ChatUserInfoModel *userInfoModel = [groupData getUserInfoModelByUserId:messageModel.content.userId];
                if (!userInfoModel) {
                    userInfoModel = [MTLJSONAdapter modelOfClass:[ChatUserInfoModel class] fromJSONDictionary:[MTLJSONAdapter JSONDictionaryFromModel:messageModel.content error:nil] error:nil];
                }
                userInfoModel.status = YES;
                [groupData updateUserInfoModel:userInfoModel];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.multicastDelegate ChatUilityScribeData:groupData JoinRoomMessage:messageModel];
                });
            }
            return;
        }
        if (messageModel.chatType == ChatMessageType_LEAVE) {
            if (messageModel.content.userId) {
                ChatUserInfoModel *userInfoModel = [groupData getUserInfoModelByUserId:messageModel.content.userId];
                if (!userInfoModel) {
                    userInfoModel = [MTLJSONAdapter modelOfClass:[ChatUserInfoModel class] fromJSONDictionary:[MTLJSONAdapter JSONDictionaryFromModel:messageModel.content error:nil] error:nil];
                    
                }
                userInfoModel.status = NO;
                [groupData updateUserInfoModel:userInfoModel];
            }
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showNotifactionByMessage:messageModel Data:groupData];
            if (self.multicastDelegate) {
                ChatSendModel *sendModel = [groupData getSendingListObjectBy:messageModel.content.sendId];
                if (sendModel) {
                    /** 消息类型未自己正在发送的类型 **/
//                    GT_OC_OUT_SET(@"CHAT", YES, @"消息发送成功 duration = %.2f", [[NSDate date] timeIntervalSinceDate:sendModel.date]);
                    [groupData pauseSendTime];
                    [groupData removeSendModel:sendModel];
                    [groupData replaceMessage:messageModel BySendModel:sendModel];
                    [groupData resumeSendTime];
                    if (self.multicastDelegate) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.multicastDelegate ChatUilityScribeData:groupData ReplaceMessage:messageModel];
                        });
                    }
                    
                }else if (messageModel.chatType == ChatMessageType_REMOVE) {
                    
                    /** 删除消息**/
                    [groupData removeMessage:messageModel];
                    [groupData removeUnreadMeesage:messageModel];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.multicastDelegate  ChatUilityScribeData:groupData RemoveMessage:messageModel];
                    });
                    
                    if (messageModel.chatFrom == ChatMessageFrom_ME) {
                        /** 传递消息**/
                        [groupData addMessage:messageModel];
                        if (groupData.ruleModel && groupData.roomInfoModel) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self.multicastDelegate ChatUilityScribeData:groupData InsertMessage:messageModel];
                            });
                        }
                    }
                }else if (messageModel.chatType == ChatMessageType_WIN){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.multicastDelegate ChatUilityScribeData:groupData BackgroundMessage:messageModel];
                    });
                }else if (messageModel.chatType == ChatMessageType_JOIN){
                    
                }else{
                    /** 传递消息**/
                    [groupData addMessage:messageModel];
                    [groupData addUnreadMessage:messageModel];
                    
                    if (groupData.ruleModel && groupData.roomInfoModel) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.multicastDelegate ChatUilityScribeData:groupData InsertMessage:messageModel];
                        });
                    }
                }
            }
        });
    });
}
-(void)webSocketDisConnectAll{
    if (![NET_DATA_MANAGER currentNetworkStatus]) {
        return;
    }
    NSMutableArray *list = [self.disConectClientList mutableCopy];
    [list enumerateObjectsUsingBlock:^(STOMPClient * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj disconnect];
        [self.disConectClientList removeObject:obj];
    }];
}

/**  加入房间成功 **/
-(void)joinRoom:(NSDictionary *)messageBody Data:(ChatUtilityGroupSubscribeData *)groupData SendModel:(ChatSendRequestModel *)model{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSDictionary *body = [[messageBody objectForKey:@"status"] integerValue] == 200 ? [messageBody objectForKey:@"response"] : nil;
        
        if (body) {
            self.connectStatus = ChatUtilityConnectStatus_CONNECTED_JOINED;
            if (groupData.defaultSubscription) {
                [groupData.defaultSubscription unsubscribe];
            }
            @weakify(self);
            groupData.defaultSubscription = [self.client subscribeTo:[NSString stringWithFormat:@"/server/v2/message/%li",groupData.identifier]
                                                             headers:@{}
                                                      messageHandler:^(STOMPMessage *message) {
                                                          @strongify(self);
                                                          NSDictionary *body = [message.body mj_JSONObject];
                                                          __block Message *messageModel;
                                                          if (body) {
                                                              NSError *error;
                                                              messageModel = [MTLJSONAdapter modelOfClass:[Message class] fromJSONDictionary:body error:&error];
                                                              [self receiveGroupMessage:messageModel Data:groupData];
                                                              
                                                          }
                                                      }];
            
            __block NSError *error;
            
            /** 权限**/
            ChatRuleModel *rule = [MTLJSONAdapter modelOfClass:[ChatRuleModel class] fromJSONDictionary:body[@"roleInfo"] error:&error];
            groupData.ruleModel = rule;
            
            /** 用户列表**/
            groupData.userList = [MTLJSONAdapter modelsOfClass:[ChatUserInfoModel class] fromJSONArray:body[@"userList"] error:&error].mutableCopy;
            __block NSMutableArray *list = @[].mutableCopy;
            [groupData.userList enumerateObjectsUsingBlock:^(ChatUserInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj.privateChat integerValue]) {
                    [list addObject:obj];
                }
            }];
            /** 更新【我的】用户权限**/
            __block NSMutableDictionary *mineInfoData = [MTLJSONAdapter JSONDictionaryFromModel:CHAT_UTIL.clientInfo.userInfo error:&error].mutableCopy;
            [groupData.userList enumerateObjectsUsingBlock:^(ChatUserInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.userId == CHAT_UTIL.clientInfo.userInfo.userId) {
                    [mineInfoData addEntriesFromDictionary:[MTLJSONAdapter JSONDictionaryFromModel:obj error:&error]];
                    *stop = YES;
                }
            }];
            if (mineInfoData.allKeys.count) {
                ChatUserInfoModel *userInfo = [MTLJSONAdapter modelOfClass:[ChatUserInfoModel class] fromJSONDictionary:mineInfoData error:&error];
                userInfo.fk = CHAT_UTIL.clientInfo.userInfo.fk;
                CHAT_UTIL.clientInfo.userInfo = userInfo;
                [groupData updateUserInfoModel:CHAT_UTIL.clientInfo.userInfo];
            }
            
            /** 私聊用户列表**/
            NSArray *unreadPrivateUserList = [MTLJSONAdapter modelsOfClass:[ChatUserInfoModel class] fromJSONArray:body[@"privateChat"] error:&error];
            [groupData setUserList:unreadPrivateUserList];
            [unreadPrivateUserList enumerateObjectsUsingBlock:^(ChatUserInfoModel * obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ChatUtilityPrivateSubscribeData *data = [self getPrivateSubsribeDataByUserId:obj.userId];
                
                [data addUnreadMessageOnlyCount:obj.unreadCount];
                [self updateAllUnreadMessageCount:obj.unreadCount FromePrivateSubscribeData:data];
            }];
            
            /** 房间信息**/
            NSMutableDictionary *roomData = @{}.mutableCopy;
            [CHAT_UTIL.clientInfo.roomInfoList enumerateObjectsUsingBlock:^(ChatRoomInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj.identity integerValue] == groupData.identifier) {
                    NSDictionary *dic = [MTLJSONAdapter JSONDictionaryFromModel:obj error:&error];
                    if (dic) {
                        [roomData addEntriesFromDictionary:dic];
                    }
                    *stop = YES;
                }
            }];
            
            [roomData addEntriesFromDictionary:body[@"userRoom"]];
            groupData.roomInfoModel = [MTLJSONAdapter modelOfClass:[ChatRoomInfoModel class] fromJSONDictionary:roomData error:&error];
            
            /** 消息记录**/
            NSMutableArray *messageList = @[].mutableCopy;
            //Message *toppingMessage = [MTLJSONAdapter modelOfClass:[Message class] fromJSONDictionary:body[@"toppingMessage"] error:&error];
            [body[@"chatMessageList"] enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                
                Message *m = [MTLJSONAdapter modelOfClass:[Message class] fromJSONDictionary:obj error:&error];
                
                if (m.chatType == ChatMessageType_RED) {
                    NSString *predicateStr = [NSString stringWithFormat:@"(userID = %li) AND (redpackID = %@) AND (roomID = %li)",CHAT_UTIL.clientInfo.userInfo.userId,m.content.redPacketId,groupData.identifier];
                    RedPackEntity *entity = [RedPackEntity MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:predicateStr] inContext:[NSManagedObjectContext MR_defaultContext]];
                    if (entity) {
                        m.hasRedPack = YES;
                    }
                }
                if (m) {
                    [messageList addObject:m];
                }
            }];
            [groupData addMessagesFromArray:messageList];
            [self reSendLoadingMessageBySubscribeData:groupData];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.multicastDelegate) {
                    [self.multicastDelegate ChatUtilityJoinGroupScribeData:groupData];
                    /** 私聊列表添加客服入口,禅道12868 **/
                    [self sortPrivateUserInfoList];
                }
            });
            
        }else{
            
            self.connectStatus = ChatUtilityConnectStatus_CONNECTED_JOINFAIL;
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.multicastDelegate) {
                    [self.multicastDelegate ChatUtilityJoinFaildGroupScribeData:groupData FaildMsg:messageBody[@"message"]];
                }
                if ([model.destination isEqualToString:kChatSendRequestJoinGroup] &&
                    [messageBody[@"message"] isEqualToString:@"房间密码不正确"]) {
                    [self showJoinRoomPswInput:groupData];
                }
            });
        }
    });
}

/**  撤回私聊消息 **/
-(void)revokeUserMessage:(NSDictionary *)body Data:(ChatUtilitySubscribeData *)data SendModel:(ChatSendRequestModel *)model{
    if ([body[@"status"] integerValue] == 200) {
        [data removeMessageByPredicate:[NSPredicate predicateWithFormat:@"identity = %li",model.messageId]];
    }else{
        [SVProgressHUD showErrorWithStatus:body[@"message"]];
    }
}

/**  获取私聊用户信息 **/
-(void)getChatUserInfo:(NSDictionary *)messageBody Data:(ChatUtilitySubscribeData *)data{
    ChatUtilityGroupSubscribeData *group = (ChatUtilityGroupSubscribeData *)data;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        if ([[messageBody objectForKey:@"status"] integerValue] == 200){
            NSDictionary *body = [messageBody objectForKey:@"response"];
            NSError *error;
            ChatUserInfoModel *model = [MTLJSONAdapter modelOfClass:[ChatUserInfoModel class] fromJSONDictionary:body error:&error];
            model.privateChat = @(1);
            if (model) {
                [group updateUserInfoModel:model];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.multicastDelegate) {
                    [self.multicastDelegate ChatUtilityScribeData:group JumpPrivateChatByUserInfo:model];
                    
                }
            });
            
        }else{
            [SVProgressHUD showErrorWithStatus:messageBody[@"message"]];
        }
        
    });
    
}

/**  私人消息标记已读 **/
-(void)addReadRecord:(NSDictionary *)body Data:(ChatUtilitySubscribeData *)data{
    
}

/**  计划员信息 **/
-(void)getPlantUserInfoRecord:(NSDictionary *)body Data:(ChatUtilitySubscribeData *)data SendModel:(ChatSendRequestModel *)model{
    
    if ([[body objectForKey:@"status"] integerValue] == 200){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSDictionary *messageBody = [body objectForKey:@"response"];
            NSError *error;
            ChatUserInfoModel *model = [MTLJSONAdapter modelOfClass:[ChatUserInfoModel class] fromJSONDictionary:messageBody error:&error];
            model.privateChat = @(1);
            [(ChatUtilityGroupSubscribeData *)data updateUserInfoModels:@[model]];
            if (model) {
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId = %li",model.userId];
                NSArray <Message *>*list = [self.messageListFromNoneUser filteredArrayUsingPredicate:predicate];
                [list enumerateObjectsUsingBlock:^(Message * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    [self.messageListFromNoneUser removeObject:obj];
                    [self receiveGroupMessage:obj Data:(ChatUtilityGroupSubscribeData *)data];
                    
                }];
            }else{
                
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.multicastDelegate) {
                    [self.multicastDelegate ChatUtilityScribeData:(ChatUtilityGroupSubscribeData *)data UpdateUserInfo:model];
                }
            });
        });
    }else{
        
        __block Message *missMsg = nil;
        [self.messageListFromNoneUser enumerateObjectsUsingBlock:^(Message * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.identity integerValue] == model.messageId) {
                missMsg = obj;
                *stop = YES;
            }
        }];

        if (missMsg) {
            NSError *error;
            NSMutableDictionary  *log = model.getHeader.mutableCopy;
            NSDictionary * messageData = [MTLJSONAdapter JSONDictionaryFromModel:missMsg error:&error];
            [log addEntriesFromDictionary:messageData];
            [NET_DATA_MANAGER requestSendChatErrorData:log success:^(id responseObject) {
                
            } failure:^(NSError *error) {
                
            }];
        }
        
        [SVProgressHUD showErrorWithStatus:body[@"message"]];
    }
}



/**  历史消息 **/
-(void)getHistoryMessageList:(NSDictionary *)messageBody Data:(ChatUtilitySubscribeData *)data SendModel:(ChatSendRequestModel *)model{
    
    
    NSMutableArray *messageList = @[].mutableCopy;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        if ([data isKindOfClass:[ChatUtilityGroupSubscribeData class]]) {
            NSDictionary *body = [[messageBody objectForKey:@"status"] integerValue] == 200 ? [messageBody objectForKey:@"response"] : nil;
            if (body) {
                NSError *error;
                
                ChatUtilityGroupSubscribeData *groupData = (ChatUtilityGroupSubscribeData *)data;
                NSArray <ChatUserInfoModel *>*userlist = [MTLJSONAdapter modelsOfClass:[ChatUserInfoModel class] fromJSONArray:body[@"userList"] error:&error].mutableCopy;
                [groupData updateUserInfoModels:userlist];
                
                [[MTLJSONAdapter modelsOfClass:[Message class] fromJSONArray:body[@"message"] error:&error] enumerateObjectsUsingBlock:^(Message * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (obj.chatType == ChatMessageType_RED) {
                        NSString *predicateStr = [NSString stringWithFormat:@"(userID = %li) AND (redpackID = %@) AND (roomID = %li)",CHAT_UTIL.clientInfo.userInfo.userId,obj.content.redPacketId,groupData.identifier];
                        RedPackEntity *entity = [RedPackEntity MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:predicateStr]];
                        if (entity) {
                            obj.hasRedPack = YES;
                        }
                    }
                    
                    if (obj) {
                        if (![ChatUtilitySubscribeData getUserInfoListEntityByUserId:[obj.userId integerValue]].count) {
                            [self.messageListFromNoneUser safeAddObject:obj];
                            //若消息的用户信息无法在本地查询，请求服务端获取会员个人信息 4.19蛤蟆吉
                            ChatSendRequestGroupModel *plantUserRequest = [ChatSendRequestGroupModel creatSendModel:kChatSendRequestUserInfoGroup Date:[NSDate date]];
                            plantUserRequest.roomId = groupData.identifier;
                            plantUserRequest.userId = [obj.userId integerValue];
                            [self webSocketSendMessageByChatSendModel:plantUserRequest Subscribe:groupData];
                        }else{
                            [messageList addObject:obj];
                        }
                        
                        
                    }
                }];
                
            }
            
            
        }else if ([data isKindOfClass:[ChatUtilityPrivateSubscribeData class]]){
            NSArray *list = [[messageBody objectForKey:@"status"] integerValue] == 200 ? [messageBody objectForKey:@"response"] : nil;
            
            if (list) {
                NSError *error;
                [[MTLJSONAdapter modelsOfClass:[Message class] fromJSONArray:list error:&error] enumerateObjectsUsingBlock:^(Message * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (obj) {
                        [messageList addObject:obj];
                    }
                }];
            }
        }
        
        [data addMessagesFromArray:messageList];
//        [data addUnreadMessage:nil];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (self.multicastDelegate) {
                //TODO
                [self.multicastDelegate ChatUilityScribeData:data getMessageHistory:messageList];
                if ([model.destination isEqualToString:kChatSendRequestHistoryPrivate] &&
                    !model.messageId &&
                    [[messageBody objectForKey:@"status"] integerValue] == 200) {
                    [self.multicastDelegate ChatUtilityJoinPrivateScribeData:(ChatUtilityPrivateSubscribeData *)data];
                }
            }
        });
    });
    
    

}

-(ChatSendRequestModel *)getSendingRequestModel:(NSString *)requestId{
    __block ChatSendRequestModel *sendRequestModel = nil;
    [self.privatesubScribeList enumerateObjectsUsingBlock:^(ChatUtilityPrivateSubscribeData * _Nonnull data, NSUInteger dataIdx, BOOL * _Nonnull dataStop) {
        [data.sendingList enumerateObjectsUsingBlock:^(ChatSendModel * _Nonnull send, NSUInteger sendIdx, BOOL * _Nonnull sendStop) {
            if ([send isKindOfClass:[ChatSendRequestModel class]] && [send.chatSendId isEqualToString:requestId]) {
                sendRequestModel = (ChatSendRequestModel *)send;
                *sendStop = YES;
                *dataStop = YES;
            }
        }];
    }];
    
    if (!sendRequestModel) {
        [self.groupSubscribeList enumerateObjectsUsingBlock:^(ChatUtilityGroupSubscribeData * _Nonnull data, NSUInteger dataIdx, BOOL * _Nonnull dataStop) {
            [data.sendingList enumerateObjectsUsingBlock:^(ChatSendModel * _Nonnull send, NSUInteger sendIdx, BOOL * _Nonnull sendStop) {
                if ([send isKindOfClass:[ChatSendRequestModel class]] && [send.chatSendId isEqualToString:requestId]) {
                    sendRequestModel = (ChatSendRequestModel *)send;
                    *sendStop = YES;
                    *dataStop = YES;
                }
            }];
        }];
    }
    
    if (!sendRequestModel) {
        [self.utilRequestSendingList enumerateObjectsUsingBlock:^(ChatSendRequestModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.chatSendId isEqualToString:requestId]) {
                sendRequestModel = (ChatSendRequestModel *)obj;
                *stop = YES;
            }
        }];
        if (sendRequestModel) {
            [self.utilRequestSendingList removeObject:sendRequestModel];
        }
    }
    return sendRequestModel;
}

-(void)receiveSingleMessage{
    
}
-(void)sortPrivateUserInfoList{
    NSLog(@"");
    NSArray *list = [self.unreadPrivateMessageUserList sortedArrayUsingComparator:^NSComparisonResult(ChatUserInfoModel*  _Nonnull obj1, ChatUserInfoModel*  _Nonnull obj2) {
        if ([obj1.lastNewDate timeIntervalSince1970] > [obj2.lastNewDate timeIntervalSince1970]) {
            return NSOrderedDescending;
        }
        return NSOrderedAscending;
    }];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.multicastDelegate ChatUtilityUpdateAllUnreadMessage:list];
    });
}

-(void)getImageKey:(void(^)(NSString *key))handle{
    self.imageKeyPickHandle = handle;
    if (!self.imageKey || !self.imageKey.length) {
        ChatSendRequestModel *imgRequestModel = [ChatSendRequestModel creatSendModel:kChatSendRequestImageKey Date:[NSDate date]];
        [self webSocketSendMessageByChatSendModel:imgRequestModel Subscribe:nil];
        return;
    }
    self.imageKeyPickHandle ? self.imageKeyPickHandle(self.imageKey) : nil;
}


#pragma mark - MulticastDelegateMethod
-(void)addChatUtilityDelegate:(id)delegate{
    if (!self.multicastDelegate) {
        self.multicastDelegate = (GCDMulticastDelegate <ChatUtilityDataDelegate> *)[[GCDMulticastDelegate alloc] init];
    }
    [self.multicastDelegate addDelegate:delegate delegateQueue:dispatch_get_main_queue()];
    NSLog(@"multicastDelegate = %li",[self.multicastDelegate count]);
}


-(void)removeChatUtilityDelegate:(id)delegate{
    if (self.multicastDelegate) {
        [self.multicastDelegate removeDelegate:delegate];
//        [self.multicastDelegate removeDelegate:delegate delegateQueue:dispatch_get_main_queue()];
        NSLog(@"multicastDelegate = %li",[self.multicastDelegate count]);
    }
}

#pragma mark - TimerManagerDelegate
-(void)cycleTimerWithKey:(NSString *)key RestCount:(NSInteger)count{
    if ([key isEqualToString:kImageKeyOverdueCycleName]) {
        [self getImageKey:nil];
    }
}

#pragma mark - NetRequest


-(void)getChatClienInfo:(void(^)(ChatInfoModel *info))success{
    if (!BET_CONFIG.common_config.isShowChat) {
        success ? success(nil) : nil;
        return;
    }
    
    
//    self.clientInfo = [MTLJSONAdapter modelOfClass:[ChatInfoModel class] fromJSONDictionary:[PPNetworkCache getHttpCacheForKey:kInfoKey] error:&error];
    if (self.clientInfo) {
        self.infoStatus = ChatUtilityClineInfoStatus_SUCCESS;
        success ? success(self.clientInfo) : nil;
    }else{
        self.infoStatus = ChatUtilityClineInfoStatus_REQUESTING;
    }
    SubmitLogModel *model = [[SubmitLogModel alloc] init];
    model.events = LOG_CHAT_INFO;
    
    [NET_DATA_MANAGER requestChatPostInitponseCache:nil success:^(id responseObject) {
        [PPNetworkCache saveHttpCache:responseObject forKey:kInfoKey];
        self.error = nil;
        NSError *error;
        self.clientInfo = [MTLJSONAdapter modelOfClass:[ChatInfoModel class] fromJSONDictionary:responseObject error:&error];
        
        if (self.clientInfo) {
            NSHTTPCookieStorage* cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
            NSArray<NSHTTPCookie *> *cookies = [cookieStorage cookiesForURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SerVer_Url,CHAT_POSTINIT_URL]]];
            [cookies enumerateObjectsUsingBlock:^(NSHTTPCookie * _Nonnull cookie, NSUInteger idx, BOOL * _Nonnull stop) {
                
                NSMutableDictionary *properties = [[cookie properties] mutableCopy];
                //将cookie过期时间设置为一年
                NSDate *expiresDate = [NSDate dateWithTimeIntervalSinceNow:3600*24*30*12];
                properties[NSHTTPCookieExpires] = expiresDate;
                //删除Cookies的discard字段，应用退出，会话结束的时候继续保留Cookies
                [properties removeObjectForKey:NSHTTPCookieDiscard];
                //重新设置改动后的Cookies
                [cookieStorage setCookie:[NSHTTPCookie cookieWithProperties:properties]];
                if ([properties[NSHTTPCookieName] isEqualToString:@"ybcsid"]) {
                    self.clientInfo.ybcsid =properties[NSHTTPCookieValue];
                }
            }];
        }
        
        self.infoStatus = ChatUtilityClineInfoStatus_SUCCESS;
        [self webSocketResetConnect];
        [self getChatClienInfoSlideMenus:nil];
        [self getSuspendDelegateJson:nil];
        model.endTime = [NSDate date];
        model.content = self.clientInfo!=nil ? @"聊天配置信息SUCCESS" : @"聊天配置信息FAIL(解析失败)";
    //        GT_OC_OUT_SET(@"CHAT", YES, @"%@ duration = %.2f",model.content, model.duration);
        if (model.duration >= 10) {
            [BAI_CLOUD_MANAGER sendObject:model BucketName:Baidu_LOG_BUCKET_PATCH Success:^{
                
            } Fail:^(NSError *error) {
                
            }];
        }
        success ? success(self.clientInfo) : nil;
    } failure:^(NSError *error) {
        
        self.error = error;
        if (self.error.chatFailCode) {
            self.error.msg = [NSString stringWithFormat:@"%@聊天室维护中",CHAT_UTIL.error.msg];
        }else if (self.error.chatNoRoom){
            if (USER_DATA_MANAGER.isLogin) {
                if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"TEST"]) {
                    self.error.msg = @"请注册为正式会员";
                }else{
                    self.error.msg = @"请联系上级代理或客服，加入聊天室";
                }
            }else{
                self.error.msg = @"请登录后进入聊天室";
            }
        }
        [SVProgressHUD showErrorWithStatus:CHAT_UTIL.error.msg];
        
        if (!self.clientInfo) {
            self.infoStatus = ChatUtilityClineInfoStatus_FAIL;
        }
        model.endTime = [NSDate date];
        model.content = self.clientInfo!=nil ? @"聊天配置信息FAIL(走缓存数据)" : @"聊天配置信息FAIL(请求失败)";
//        GT_OC_OUT_SET(@"CHAT", YES, @"%@ duration = %.2f",model.content, model.duration);
        if (model.duration >= 10) {
            [BAI_CLOUD_MANAGER sendObject:model BucketName:Baidu_LOG_BUCKET_PATCH Success:^{
                
            } Fail:^(NSError *error) {
                
            }];
        }
        success ? success(nil) : nil;
    }];
}

-(void)getSuspendDelegateJson:(void(^)(ChatSuspendDelegateModel *model))success{
    void(^handle)(id obj) = ^(id obj){
        NSError *error;
        self.suspendModel = [MTLJSONAdapter modelOfClass:[ChatSuspendDelegateModel class] fromJSONDictionary:obj error:&error];
        success ? success(self.suspendModel) : nil;
    };
    [NET_DATA_MANAGER requestChatDelegateResponseCache:^(id responseCache) {
        handle(responseCache);
    } Success:^(id responseObject) {
        handle(responseObject);
    } Failure:^(NSError *error) {
        handle(nil);
    }];
    
}

-(void)getChatClienInfoSlideMenus:(void(^)(NSArray <ChatSlideMenuModel *>*list))success{
    
//    if (!USER_DATA_MANAGER.isLogin) {
//        success ? success(nil) : nil;
//        return;
//    }
    self.slideMenulist = @[].mutableCopy;
    [NET_DATA_MANAGER requestChatSlideMenusSuccess:^(id responseObject) {
        __block NSError *error;
        [((NSArray *)responseObject) enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ChatSlideMenuModel *model = [MTLJSONAdapter modelOfClass:[ChatSlideMenuModel class] fromJSONDictionary:obj error:&error];
            if (model) {
                [self.slideMenulist addObject:model];
            }
        }];
        success ? success(self.slideMenulist) : nil;
    } Failure:^(NSError *error) {
        success ? success(nil) : nil;
    }];
   
}



-(NSURLSessionTask *)uploadImageData:(NSData *)imageData
                        Completed:(void(^)(NSDictionary *imageInfo))completed{
    [SVProgressHUD showWithStatus:@"图片上传中"];
    return [NET_DATA_MANAGER uploadChatImageData:imageData ServerUrl:self.clientInfo.uploadHost ImageKey:self.imageKey progress:nil success:^(id responseObject) {
        [SVProgressHUD showSuccessWithStatus:@"图片上传成功"];
        completed ? completed(responseObject) : nil;
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"图片上传失败"];
        completed ? completed(nil) : nil;
    }];
//    return [NET_DATA_MANAGER uploadChatImageData:imageData RoomID:roomId SendId:sendId toUserId:toUserId progress:^(NSProgress *progress) {
//    } success:^(id responseObject) {
//        //        NSDictionary *data = responseObject;
//        //        if ([[data objectForKey:@"code"] integerValue] == 200) {
//        //            NSError *error;
//        //            UploadImgModel *model = [MTLJSONAdapter modelOfClass:[UploadImgModel class] fromJSONDictionary:responseObject error:&error];
//        //            completed ? completed(model) : nil;
//        //
//        //        }else{
//        //            dispatch_async(dispatch_get_main_queue(), ^{
//        //                [SVProgressHUD showSuccessWithStatus:@"发送失败"];
//        //            });
//        //        }
//
//    } failure:^(NSError *error) {
//
//    }];
    
}

-(NSURLSessionTask *)updateUserAvator:(NSString *)avatar
                                    NickName:(NSString *)nickName
                               Completed:(void(^)(BOOL success))completed{
    return [NET_DATA_MANAGER requestUpdateUserAvator:avatar NickName:nickName ResponseCache:nil success:^(id responseObject) {
        completed ? completed(YES) : nil;
    } failure:^(NSError *error) {
        completed ? completed(NO) : nil;
    }];
}

-(NSURLSessionTask *)getAvatorListCompleted:(void(^)(NSDictionary *data))completed{
    return [NET_DATA_MANAGER requestChatGetAvatorListRponseCache:^(id responseCache) {
        NSMutableDictionary *data = @{}.mutableCopy;
        [[responseCache allKeys] enumerateObjectsUsingBlock:^(NSString *  _Nonnull key, NSUInteger idx, BOOL * _Nonnull iStop) {
            NSMutableArray *list = [NSMutableArray array];
            [((NSArray *)responseCache[key]) enumerateObjectsUsingBlock:^(NSDictionary  *_Nonnull dic, NSUInteger jdx, BOOL * _Nonnull jStop) {
                [list addObjectsFromArray:dic.allValues];
            }];
            [data setObject:list forKey:key];
        }];
        completed ? completed(data) : nil;
    } success:^(id responseObject) {
        NSMutableDictionary *data = @{}.mutableCopy;
        [[responseObject allKeys] enumerateObjectsUsingBlock:^(NSString *  _Nonnull key, NSUInteger idx, BOOL * _Nonnull iStop) {
            NSMutableArray *list = [NSMutableArray array];
            [((NSArray *)responseObject[key]) enumerateObjectsUsingBlock:^(NSDictionary  *_Nonnull dic, NSUInteger jdx, BOOL * _Nonnull jStop) {
                [list addObjectsFromArray:dic.allValues];
            }];
            [data setObject:list forKey:key];
        }];
        completed ? completed(data) : nil;
        
    } failure:^(NSError *error) {
        completed ? completed(nil) : nil;
    }];
}




-(NSURLSessionTask *)getOpenInfoByLotteryID:(NSString *)lotteryID Completed:(void(^)(LotteryOpenInfoModel * model))completed{
    return [NET_DATA_MANAGER requestTheGameOpenTimeByLotteryID:lotteryID ResponseCache:nil success:^(id responseObject) {
         NSError *error;
        LotteryOpenInfoModel *model = [MTLJSONAdapter modelOfClass:[LotteryOpenInfoModel class] fromJSONDictionary:responseObject error:&error];
        model.lotteryID = lotteryID;
        completed ? completed(model) : nil;
    } failure:^(NSError *error) {
        completed ? completed(nil) : nil;
    }];
}


-(NSURLSessionTask *)getAllOpenInfoAtRoom:(NSInteger)roomID Completed:(void(^)(NSMutableArray <LotteryOpenInfoModel *>* modelList))completed{
    __block ChatRoomInfoModel *roomInfo;
    [self.clientInfo.roomInfoList enumerateObjectsUsingBlock:^(ChatRoomInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.identity integerValue] == roomID) {
            roomInfo = obj;
        }
    }];

    return [NET_DATA_MANAGER requestTheGameOpenTimeListresponseCache:nil success:^(id responseObject) {
        
        NSMutableDictionary *data = @{}.mutableCopy;
        [roomInfo.planGameList enumerateObjectsUsingBlock:^(NSString * _Nonnull Lotteryid, NSUInteger idx, BOOL * _Nonnull stop) {
            id obj = [responseObject objectForKey:Lotteryid];
            if (obj) {
                [data setObject:obj forKey:Lotteryid];
            }
        }];
        __block NSMutableArray *list = @[].mutableCopy;
        __block NSError *error;
        [[data allKeys] enumerateObjectsUsingBlock:^(NSString *  _Nonnull key, NSUInteger idx, BOOL * _Nonnull stop) {
            id value = data[key];
            if ([value isKindOfClass:[NSDictionary class]]) {
                NSMutableDictionary *dic = [value mutableCopy];
                [dic safeSetObject:key forKey:@"lotteryID"];
                [dic safeSetObject:responseObject[@"serverTime"] forKey:@"serverTime"];
                LotteryOpenInfoModel *model = [MTLJSONAdapter modelOfClass:[LotteryOpenInfoModel class] fromJSONDictionary:dic error:&error];
                if (!error && model) {
                    [list addObject:model];
                }
            }
        }];
        __block NSMutableArray *sortList = @[].mutableCopy;

        [roomInfo.planGameList enumerateObjectsUsingBlock:^(NSString * _Nonnull Lotteryid, NSUInteger idx, BOOL * _Nonnull stop) {
            [list enumerateObjectsUsingBlock:^(LotteryOpenInfoModel *  _Nonnull model, NSUInteger i, BOOL * _Nonnull iStop) {
                if ([Lotteryid isEqualToString:model.lotteryID]) {
                    
                    [sortList addObject:model];
                    *iStop = YES;
                }
            }];
        }];
//        [LOTTERY_FACTORY.gameDataJson.lastObject enumerateObjectsUsingBlock:^(KFCPHomeGameJsonModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//            [list enumerateObjectsUsingBlock:^(LotteryOpenInfoModel *  _Nonnull model, NSUInteger i, BOOL * _Nonnull iStop) {
//                if ([obj.GameId isEqualToString:model.lotteryID]) {
//                    [sortList addObject:model];
//                    *iStop = YES;
//                }
//            }];
//        }];
        completed ? completed(sortList) : nil;
    } failure:^(NSError *error) {
        completed ? completed(nil) : nil;
    }];
}

-(NSURLSessionTask *)chatUntilBanUserByUserID:(NSInteger)userID RoomID:(NSInteger)roomID FK:(NSString *)fk MessageID:(NSInteger)messageID Completed:(void(^)(BOOL success))completed{
    return [NET_DATA_MANAGER requestChatBanUserID:userID RoomID:roomID FK:fk MessageId:messageID Success:^(id responseObject) {
        completed ? completed(YES) : nil;
    } Failure:^(NSError *error) {
        completed ? completed(NO) : nil;
    }];
}

-(NSURLSessionTask *)chatUntilBanImgByUserID:(NSInteger)userID RoomID:(NSInteger)roomID FK:(NSString *)fk MessageID:(NSInteger)messageID Completed:(void(^)(BOOL success))completed{
    return [NET_DATA_MANAGER requestChatBanImgUserID:userID RoomID:roomID FK:fk MessageId:messageID Success:^(id responseObject) {
        completed ? completed(YES) : nil;
    } Failure:^(NSError *error) {
        completed ? completed(NO) : nil;
    }];
}

-(NSURLSessionTask *)chatUntilKikeUserByUserID:(NSInteger)userID RoomID:(NSInteger)roomID FK:(NSString *)fk MessageID:(NSInteger)messageID Completed:(void(^)(BOOL success))completed{
    return [NET_DATA_MANAGER requestChatKikeUserID:userID RoomID:roomID FK:fk MessageId:messageID Success:^(id responseObject) {
        completed ? completed(YES) : nil;
    } Failure:^(NSError *error) {
        completed ? completed(NO) : nil;
    }];
}

-(NSURLSessionTask *)chatUntilDeleteMessageByUserID:(NSInteger)userID RoomID:(NSInteger)roomID FK:(NSString *)fk MessageID:(NSInteger)messageID Completed:(void(^)(BOOL success))completed{
    return [NET_DATA_MANAGER requestChatDeleteMessageUserID:userID RoomID:roomID FK:fk MessageId:messageID Success:^(id responseObject) {
        completed ? completed(YES) : nil;
    } Failure:^(NSError *error) {
        completed ? completed(NO) : nil;
    }];
}

-(NSURLSessionTask *)getLotteryOpenHistoryByLotteryID:(NSInteger)lotteryID
                               PageSize:(NSInteger)pageSize
                              Completed:(void(^)(NSArray <ChatLotteryTopResultModel *>*list))completed{
    NSMutableArray *list = @[].mutableCopy;
    return [NET_DATA_MANAGER requestLotteryOpenTopResultsByLotteryID:lotteryID PageSize:pageSize ResponseCache:nil success:^(id responseObject) {
        NSArray *data = responseObject;
        [data enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull dic, NSUInteger idx, BOOL * _Nonnull stop) {
            NSError *error;
            ChatLotteryTopResultModel *model = [MTLJSONAdapter modelOfClass:[ChatLotteryTopResultModel class] fromJSONDictionary:dic error:&error];
            if (!error && model) {
                [list addObject:model];
            }
        }];
        completed ? completed(list) : nil;
    } failure:^(NSError *error) {
        completed ? completed(nil) : nil;
    }];
}

-(NSURLSessionTask *)getUserInfoWithUserID:(NSInteger)userID RoomID:(NSInteger)roomID Completed:(void(^)(ChatUserInfoModel *model))completed{
    return [NET_DATA_MANAGER requestChatUserInfoWithUserID:userID RoomID:roomID Success:^(id responseObject) {
        NSError *error;
        ChatUserInfoModel *model = [MTLJSONAdapter modelOfClass:[ChatUserInfoModel class] fromJSONDictionary:responseObject error:&error];
        completed ? completed(model) : nil;
    } Failure:^(NSError *error) {
        completed ? completed(nil) : nil;
    }];
}

-(NSURLSessionTask *)submitChatbeelLotteryWithUserID:(NSInteger)userID
                                   orderNo:(NSString *)orderNo
                                  PlayCode:(NSString *)playCode
                                TotalMoney:(NSNumber *)totalMoney
                                              RoomID:(NSInteger)roomId
                                 Completed:(void(^)(BOOL success))completed{
    return [NET_DATA_MANAGER requestChatbeelLotteryWithorderNo:orderNo PlayCode:playCode TotalMoney:totalMoney UserId:userID RoomID:roomId Success:^(id responseObject) {
        completed ? completed(YES) : nil;
    } Failure:^(NSError *error) {
        completed ? completed(NO) : nil;
    }];
}

-(NSURLSessionTask *)getRedDetailByMessage:(Message *)message
                                 Completed:(void(^)(ChatRedPacketModel *model))completed{
    NSString *identity = message.content.redPacketId.length ? message.content.redPacketId : message.content.redCollarPacketId;
    return [NET_DATA_MANAGER requestGetRedDetailByRedId:identity ResponseCache:nil success:^(id responseObject) {
        ChatUserInfoEntity *entity = [ChatUtilitySubscribeData getUserInfoListEntityByUserId:[message.userId integerValue]].firstObject;
        
        NSError *error;
        ChatRedPacketModel *model = [MTLJSONAdapter modelOfClass:[ChatRedPacketModel class] fromJSONDictionary:responseObject error:&error];
        model.activityChatRedpacket.createrId = [message.userId integerValue];
        model.activityChatRedpacket.createrName = entity.nickName;
        model.activityChatRedpacket.createrAvator = entity.avatar;
        completed ? completed(model) : nil;
    } failure:^(NSError *error) {
        completed ? completed(nil) : nil;
    }];
}

-(NSURLSessionTask *)getTopListByRoomId:(NSInteger)roomId
                              Completed:(void(^)(NSArray <ChatTopListModel *>*list))completed{
    return [NET_DATA_MANAGER requestTopListByRoomId:roomId
                                      ResponseCache:nil
                                            success:^(id responseObject) {
                                                NSError *error;
                                                NSArray *list = [MTLJSONAdapter modelsOfClass:[ChatTopListModel class] fromJSONArray:responseObject error:&error];
                                                completed ? completed(list) : nil;
                                            }
                                            failure:^(NSError *error) {
                                            completed ? completed(nil) : nil;
    }];
}

-(NSURLSessionTask *)removeAllMessageListByRoomId:(NSInteger)roomId Message:(Message *)message{
    
    NSInteger messageId = message ? [message.identity integerValue] : 1;
    return [NET_DATA_MANAGER requestRemoveAllMessageWithIdentity:messageId RoomId:roomId Success:^(id responseObject) {
        
    } Failure:^(NSError *error) {
        
    }];
}

-(NSURLSessionTask *)getEmojiListWithKey:(NSString *)key KeyWord:(NSString *)keyword Page:(NSUInteger)page Rows:(NSUInteger)rows completed:(void(^)(NSArray <NetEmojiModel *> *list))completed{
    
    return [NET_DATA_MANAGER requestGetEmojiListWithUrl:[NSString stringWithFormat:@"%@/api/chat/gif/query",self.clientInfo.imageHost] Key:key KeyWord:keyword Page:page Rows:rows success:^(id responseObject) {
        NSError *error;
        NSArray *list = [MTLJSONAdapter modelsOfClass:NetEmojiModel.class fromJSONArray:responseObject[@"data"] error:&error];
        completed ? completed(list) : nil;
    } failure:^(NSError *error) {
        completed ? completed(@[]) : nil;
    }];
}
#pragma mark - UserNotifaction
-(void)showNotifactionByMessage:(Message *)message Data:(ChatUtilityGroupSubscribeData *)data{
    
    if (message == nil || message.chatFrom == ChatMessageFrom_ME) {
        return;
    }
    
    ChatUserInfoEntity *entity = [ChatUtilitySubscribeData getUserInfoListEntityByUserId:[message.userId integerValue]].firstObject;
    if (message.chatType == ChatMessageType_TEXT ||
        message.chatType == ChatMessageType_RED ||
        message.chatType == ChatMessageType_IMG ||
        message.chatType == ChatMessageType_LOTTERY){
        
        if (BET_CONFIG.userSettingData.chatFollowStatus == ChatFollowStatus_ONLY) {
            if ([entity.level integerValue] >= 100){
                
            }else if (message.content.isTopping){
                
            }else if ([data isKindOfClass:[ChatUtilityGroupSubscribeData class]]){
                /** 他人消息,切在关注列表**/
                __block BOOL isContain = NO;
                NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
                NSArray *entityList = [data getFollowEntityListInContext:context];
                [entityList enumerateObjectsUsingBlock:^(ChatFollowEntity * _Nonnull entity, NSUInteger entityIdx, BOOL * _Nonnull entityStop) {
                    if (entity.followUserID == [message.userId integerValue]) {
                        isContain = YES;
                        *entityStop = YES;
                    }
                }];
                if (!isContain)
                    return;
            }
        }
        
        if (BET_CONFIG.userSettingData.hideChatMessageNotification) {
            return;
        }
        if ([[NAVI_MANAGER getCurrentVC] isKindOfClass:[ChatViewController class]] &&
            BET_CONFIG.userSettingData.openChatMeessageNotifacationSound){
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            AudioServicesPlaySystemSound(1007);
        }
        if (@available(iOS 10.0, *)) {
            UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
            
            if ([data isKindOfClass:[ChatUtilityGroupSubscribeData class]]){
                content.title = ((ChatUtilityGroupSubscribeData *)data).roomInfoModel.name;
            }
            content.subtitle = entity.nickName;
            if (message.chatType == ChatMessageType_IMG) {
                content.body = @"[图片]";
            }else if (message.chatType == ChatMessageType_RED){
                content.body = @"[红包]";
            }else if (message.chatType == ChatMessageType_LOTTERY){
                content.body = @"[彩票]";
            }else{
                content.body = message.content.text;
            }
            self.notiUnreadCount += 1;
            content.badge = @(self.notiUnreadCount);
            if (BET_CONFIG.userSettingData.openChatMeessageNotifacationSound) {
                UNNotificationSound *sound = [UNNotificationSound soundNamed:@"caodi.m4a"];
                content.sound = sound;
            }
            
            
            UNTimeIntervalNotificationTrigger *trigger1 = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:1 repeats:NO];
            
            NSString *requertIdentifier = [NSString stringWithFormat:@"%@/%li",CHAT_NOTIFCATION_IDENTIFIER,[message.identity integerValue]];
            UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:requertIdentifier content:content trigger:trigger1];
            
            [[UNUserNotificationCenter currentNotificationCenter] addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
                NSLog(@"Error:%@",error);
            }];
        }else if (@available(iOS 8.0, *)){
            
            UILocalNotification *locationNo = [[UILocalNotification alloc] init];
            locationNo.fireDate = [NSDate dateWithTimeIntervalSinceNow:10];
            if (message.chatType == ChatMessageType_IMG) {
                locationNo.alertBody = @"[图片]";
            }else if (message.chatType == ChatMessageType_RED){
                locationNo.alertBody = @"[红包]";
            }else if (message.chatType == ChatMessageType_LOTTERY){
                locationNo.alertBody = @"[彩票]";
            }else{
                locationNo.alertBody = message.content.text;
            }
            if (@available(iOS 8.2,*)) {
                if ([data isKindOfClass:[ChatUtilityGroupSubscribeData class]]){
                    locationNo.alertTitle = ((ChatUtilityGroupSubscribeData *)data).roomInfoModel.name;
                }
                
            }
            locationNo.alertAction = entity.nickName;
            locationNo.hasAction = YES;
            locationNo.soundName = UILocalNotificationDefaultSoundName;
            locationNo.applicationIconBadgeNumber = -1;
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
            
            locationNo.userInfo = @{CHAT_NOTIFCATION_KEY:[NSString stringWithFormat:@"%@/%li",CHAT_NOTIFCATION_IDENTIFIER,[message.identity integerValue]]};
            [[UIApplication sharedApplication] scheduleLocalNotification:locationNo];
            
            if (![[NAVI_MANAGER getCurrentVC] isKindOfClass:[ChatViewController class]]) {
                EBBannerView *banner = [EBBannerView bannerWithBlock:^(EBBannerViewMaker *make) {
                    make.style = 11;
                    SDWebImageManager *manager = [SDWebImageManager sharedManager];
                    NSString* key = [manager cacheKeyForURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SerVer_Url,entity.avatar]]];
                    SDImageCache* cache = [SDImageCache sharedImageCache];
                    UIImage *icon = [cache imageFromDiskCacheForKey:key];
                    if (!icon) {
                        icon = [UIImage imageNamed:@"chat_userAvator_placeHolder"];
                    }
                    make.icon = icon;
                    make.title = entity.nickName;
                    if (message.chatType == ChatMessageType_IMG) {
                        make.content = @"[图片]";
                    }else if (message.chatType == ChatMessageType_RED){
                        make.content = @"[红包]";
                    }else if (message.chatType == ChatMessageType_LOTTERY){
                        make.content = @"[彩票]";
                    }else{
                        make.content = message.content.text;
                    }
                    make.date = [message.curTime getTimeFormatStringWithFormat:@"HH:mm:ss"];
                    
                    
                }];
                [banner show];
            }
        }
    }
}

#pragma mark - STOMPClientDelegate
- (void) websocketDidDisconnect: (NSError *)error{
    self.connectStatus = ChatUtilityConnectStatus_DISCONNECTE;
    [self webSocketClose];
    [CHAT_UTIL webSocketReconnectCompleted:nil];
}

#pragma mark - GET/SET
-(STOMPClient *)client{
    if (!_client) {
        NSURL *websocketUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/webchat/websocket",CHAT_UTIL.clientInfo.chatUrl]];
        _client = [[STOMPClient alloc] initWithURL:websocketUrl webSocketHeaders:nil useHeartbeat:YES];
        _client.delegate = self;
    }
    return _client;
}

-(NSMutableArray<ChatUtilityPrivateSubscribeData *> *)privatesubScribeList{
    if (!_privatesubScribeList) {
        _privatesubScribeList = @[].mutableCopy;
    }
    return _privatesubScribeList;
}

-(NSMutableArray<ChatUtilityGroupSubscribeData *> *)groupSubscribeList{
    if (!_groupSubscribeList) {
        _groupSubscribeList = @[].mutableCopy;
    }
    return _groupSubscribeList;
}

-(NSMutableArray<STOMPClient *> *)disConectClientList{
    if (!_disConectClientList) {
        _disConectClientList = @[].mutableCopy;
    }
    return _disConectClientList;
}

-(NSMutableArray <ChatUserInfoModel *>*)unreadPrivateMessageUserList{
    if (!_unreadPrivateMessageUserList) {
        _unreadPrivateMessageUserList = @[].mutableCopy;
    }
    return _unreadPrivateMessageUserList;
}

-(NSMutableArray *)messageListFromNoneUser{
    if (!_messageListFromNoneUser) {
        _messageListFromNoneUser = [NSMutableArray array];
    }
    return _messageListFromNoneUser;
}

-(AFHTTPSessionManager *)manager
{
    if (!_manager) {
        _manager=[AFHTTPSessionManager manager];
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",
                                                              @"text/plain",
                                                              @"text/json",
                                                              @"application/json",
                                                              @"text/javascript",
                                                              @"image/jpeg",
                                                              @"image/png", nil];
        _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        _manager.requestSerializer.timeoutInterval = 20;
    }
    return _manager;
}

-(NSMutableArray<ChatSendRequestModel *> *)utilRequestSendingList{
    if (!_utilRequestSendingList) {
        _utilRequestSendingList = @[].mutableCopy;
    }
    return _utilRequestSendingList;
}

-(void)setImageKey:(NSString *)imageKey{
    _imageKey = imageKey;
    if (!imageKey || imageKey.length) {
        return;
    }
    self.imageKeyPickHandle ? self.imageKeyPickHandle(_imageKey) : nil;
}
@end
