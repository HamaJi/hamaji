//
//  GameLobby3MenuCell.h
//  Bet365
//
//  Created by super_小鸡 on 2019/9/4.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllLiveData.h"

NS_ASSUME_NONNULL_BEGIN

@interface GameLobby3MenuCell : UICollectionViewCell

@property (nonatomic,weak)GameLobby3MenuModel *menu;

@end

NS_ASSUME_NONNULL_END
