//
//  HomeHengFuReusableView.h
//  Bet365
//
//  Created by luke on 2018/11/16.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICollectionSectionColorReusableView.h"
@class Bet365HengFuModel;

@protocol HomeHengFuReusableViewDelegate <NSObject>

- (void)HomeHengFuReusableViewTapwebUrlWithUrl:(NSString *)webUrl;

@end

@interface HomeHengFuReusableView : UICollectionSectionColorReusableView
/** 模型 */
@property (nonatomic,strong) Bet365HengFuModel *model;
/** 代理 */
@property (nonatomic,weak) id<HomeHengFuReusableViewDelegate>delegate;

+(NSString *)getIdentifier;

@end
