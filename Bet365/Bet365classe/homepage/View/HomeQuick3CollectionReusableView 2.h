//
//  HomeQuick3CollectionReusableView.h
//  Bet365
//
//  Created by super_小鸡 on 2019/9/5.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class HomeQuick3CollectionReusableView;

@protocol HomeQuick3CollectionReusableViewDelegate <NSObject>

@required

-(void)didTapnHomeQuick3CollectionReusableView:(HomeQuick3CollectionReusableView *)reusableView RouterKey:(RouterKey*)routerKey;


@end

@interface HomeQuick3CollectionReusableView : UICollectionReusableView

@property (nonatomic,strong) NSString *imgPath;

@property (nonatomic,assign) NSUInteger dateHour;

@property (nonatomic,weak)id<HomeQuick3CollectionReusableViewDelegate> delegate;

-(void)setContentArc:(UIRectCorner)corners radii:(CGSize)radii;

@end

NS_ASSUME_NONNULL_END
