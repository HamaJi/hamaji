//
//  KFCPHomeGameJsonModel.m
//  Bet365
//
//  Created by jesse on 2017/6/26.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "KFCPHomeGameJsonModel.h"

@implementation KFCPHomeGameJsonModel
+(BOOL)propertyIsOptional:(NSString *)propertyName
{
    return YES;
}
+(JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc]initWithModelToJSONDictionary:@{@"GameId":@"id"}];
}

+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"GameId":@"id"};
}

@end
