//
//  GameCategoryJsonModel.h
//  Bet365
//
//  Created by Ziping Ren on 2019/11/20.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface GameCategoryJsonModel : JSONModel

//Name of the category
@property(nonatomic,copy) NSString * name;

//Value of the category
@property(nonatomic,copy) NSString * value;

@end

NS_ASSUME_NONNULL_END
