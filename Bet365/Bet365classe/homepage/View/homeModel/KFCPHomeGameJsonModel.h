//
//  KFCPHomeGameJsonModel.h
//  Bet365
//
//  Created by jesse on 2017/6/26.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <JSONModel/JSONModel.h>
@class OpenInfo;
@interface KFCPHomeGameJsonModel : JSONModel
@property(copy,nonatomic)NSString *openNum;
@property(copy,nonatomic)NSString *turnLength;
@property(nonatomic,copy)NSString *GameId;//游戏ID
@property(nonatomic,copy)NSString *name;//游戏名字
@property(nonatomic,copy)NSString *isOffcial;//是否为官方的
@property(nonatomic,copy)NSString *isCredit;//是否为信用的
@property(nonatomic,copy)NSString *code;//gamecode
@property(nonatomic,copy)NSString *type;//游戏类型
@property(nonatomic,copy)NSString *rules;//返回期数类型
@property(nonatomic,copy)NSString *open;//是否停用
@property(nonatomic,copy)NSString *jsType;//是否极速
@property(nonatomic,copy)NSString<Optional> *cate;//排序类型
@property(copy,nonatomic)NSString *openFrequency;/**当前游戏周期描述**/
@property (nonatomic,strong) OpenInfo *info;
@end
