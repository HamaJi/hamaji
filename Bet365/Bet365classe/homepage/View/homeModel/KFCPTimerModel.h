//
//  KFCPTimerModel.h
//  Bet365
//
//  Created by jesse on 2017/6/30.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OpenInfo : NSObject
@property (nonatomic,strong) lotteryCurModel *cur;
@property (nonatomic,strong) lotteryPreModel *pre;
@end

@interface KFCPTimerModel : NSObject
@property(nonatomic,copy)NSString *timer;
@property(nonatomic,copy)NSString *gameId;
@end
