//
//  KFCPDragonModel.h
//  Bet365
//
//  Created by jesse on 2017/7/24.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface KFCPDragonModel : JSONModel
@property(nonatomic,copy)NSString *name;
@property(nonatomic,copy)NSString *openValue;
@property(nonatomic,copy)NSString *repeatCount;
@end
