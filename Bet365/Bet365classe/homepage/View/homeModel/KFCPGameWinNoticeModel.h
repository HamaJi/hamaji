//
//  KFCPGameWinNoticeModel.h
//  Bet365
//
//  Created by jesse on 2017/6/27.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface KFCPGameWinNoticeModel : JSONModel
@property(nonatomic,copy)NSString *winMoney;
@property(nonatomic,copy)NSString *gameName;
@property(nonatomic,copy)NSString *name;
@end
