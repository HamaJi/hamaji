//
//  HomeWaterFallCollectionReusableView.m
//  Bet365
//
//  Created by super_小鸡 on 2019/9/4.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "HomeWaterFallCollectionReusableView.h"
#import "WaterFallUCollectionViewFlowLayout.h"
#import "GameLobby3MenuCell.h"
#import "GameLobby3Cell.h"
#import "GameLobby3NameMenuCell.h"
#define LEFTITEMSIZE (CGSizeMake(100.0, 45.0))

@interface UIButton (ActivityIndicatorView)

@property (nonatomic)UIActivityIndicatorView *indicatorView;

@end

@implementation UIButton (ActivityIndicatorView)

-(UIActivityIndicatorView *)indicatorView{
    return objc_getAssociatedObject(self, _cmd);
}

-(void)setIndicatorView:(UIActivityIndicatorView *)indicatorView{
    objc_setAssociatedObject(self, @selector(indicatorView), indicatorView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(instancetype)init{
    if (self = [super init]) {
        
    }
    return self;
}



@end

@interface HomeWaterFallCollectionReusableView()
<UICollectionViewDelegate,
UICollectionViewDataSource,
UIGestureRecognizerDelegate>
{
    WaterFallUCollectionViewFlowLayout *_layout;
}

@property (nonatomic,strong)UIView *leftListView;

//@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,strong) UICollectionView *collectionView;

@property (nonatomic,assign) NSInteger index;

@property (nonatomic,strong) UIPanGestureRecognizer *pan;

@property (nonatomic,strong) UIImageView *bottomImageView;

@property (nonatomic,strong) NSNumber *kBQualificationSpacing;



@end

@implementation HomeWaterFallCollectionReusableView


-(instancetype)init{
    if (self = [super init]) {
        [self addObser];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        
        [self addObser];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addObser];
    }
    return self;
}

-(void)addObser{
//    self.tableView.delegate = self;
//    self.tableView.dataSource = self;
    [self leftListView];
    @weakify(self);
    [RACObserve(self, index) subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (_collectionView) {
                [self.collectionView removeFromSuperview];
                self.collectionView = nil;
            }
            GameLobby3Model *model = [self.list safeObjectAtIndex:self.index];
            _layout = [[WaterFallUCollectionViewFlowLayout alloc]init];
            _layout.type = HorizontalType;
            _layout.numberOfColumns = 5;
            _layout.columnGap = 5;
            _layout.rowGap = 5;
            _layout.insets = UIEdgeInsetsMake(5, 5, -5, -5);
            if (model.nextMenu.count == 1) {
                _layout.rowHeight = (WIDTH - LEFTITEMSIZE.width - 10 - 5 ) / 730 *1140;
            }else{
                _layout.rowHeight = (WIDTH - LEFTITEMSIZE.width - 10 - 5 ) / 730 *285;
            }
            
            NSMutableArray *widthList = @[].mutableCopy;
            [model.nextMenu enumerateObjectsUsingBlock:^(GameLobby3MenuModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                CGFloat rowHeight = _layout.rowHeight;
                CGFloat scale = 0.0;
                if (obj.icon.width > 0 && obj.icon.height > 0) {
                    scale = obj.icon.width / obj.icon.height;
                }else{
                    scale = 1.0;
                }
                CGFloat height = rowHeight;
                CGFloat width = height * scale;
                [widthList addObject:[NSNumber numberWithDouble:width]];
            }];
            
            _layout.itemWidths = widthList;
            [self updateLeftMenuSelected];
            [self.collectionView reloadData];
            self.bottomImageView.alpha = 0;
            self.collectionView.contentOffset = CGPointMake(self.collectionView.contentOffset.x, 0);
        });
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.index = 0;
    });
}

#pragma mark - Public
-(BOOL)getCanScrollWithVelocity:(CGFloat)velocity{
    CGFloat contentOffsetY = self.collectionView.contentOffset.y;
    
    if (self.collectionView.contentSize.height <= self.collectionView.bounds.size.height) {
        return NO;
    }
    if (contentOffsetY <= 0) {
        return (velocity < 0);
    }
    if(contentOffsetY + self.collectionView.bounds.size.height - self.collectionView.contentSize.height >= -1){
        return (velocity > 0);
    }
    return YES;
}
-(BOOL)getSuperCanScrollWithVelocity:(CGFloat)velocity{
    CGFloat contentOffsetY = self.collectionView.contentOffset.y;
    if (self.collectionView.contentSize.height <= self.collectionView.bounds.size.height) {
        if (velocity < 0 &&
            self.index + 1 < self.list.count) {
            return NO;
        }
//        else if (velocity > 0 &&
//                  self.index - 1 > 0 && self.list.count){
//            return NO;
//        }
        return YES;
    }
    if (contentOffsetY == 0) {
        return (velocity > 0);
    }
    if(contentOffsetY + self.collectionView.bounds.size.height - self.collectionView.contentSize.height >= -1){
        if (velocity < 0 &&
            self.index + 1 < self.list.count) {
            return NO;
        }
//        else if (velocity > 0 &&
//                  self.index - 1 > 0 && self.list.count){
//            return NO;
//        }
        return (velocity < 0);
    }
    return NO;
}

#pragma mark - Private
-(void)showBottomSlidePanOffsetY:(CGFloat)offsetY Velocity:(CGFloat)velocity{
    
    CGFloat contentOffsetY = self.collectionView.contentOffset.y - offsetY;
    CGFloat fabsY = fabs(offsetY);
    if (contentOffsetY + self.collectionView.bounds.size.height >= self.collectionView.contentSize.height || contentOffsetY <= 0) {
        if (fabsY >= [self.kBQualificationSpacing floatValue]) {
            if (velocity < 0) {
                if (self.index + 1 < self.list.count) {
                    [self.pan setTranslation:CGPointMake(0, 0) inView:self.collectionView];
                    [self.pan setEnabled:NO];
                    [self.pan setEnabled:YES];
                    self.index ++;
                }
            }else{
                [self.pan setTranslation:CGPointMake(0, 0) inView:self.collectionView];
//                if (self.index - 1 >= 0 && self.list.count) {
//                    [self.pan setTranslation:CGPointMake(0, 0) inView:self.collectionView];
//                    self.index --;
//                }
            }
        }
    }
}

-(void)updateLeftMenuSelected{
    [self.leftListView.subviews enumerateObjectsUsingBlock:^(__kindof UIButton * _Nonnull btn, NSUInteger idx, BOOL * _Nonnull stop) {
        [btn setEnabled:!(idx == self.index)];
    }];
}

#pragma mark - UITableViewDataSource
//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//    return self.list.count;
//}
//
//-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return 1;
//}
//
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    if (section == 0) {
//        return 5.0;
//    }
//    return 0.0;
//}
//
//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    return 5.0;
//}
//
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 45;
//}
//
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    UITableViewHeaderFooterView * header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"head"];
//    [header setBackgroundColor:[UIColor skinViewScreenColor]];
//    [header.contentView setBackgroundColor:[UIColor skinViewScreenColor]];
//    return header;
//}
//
//-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//    UITableViewHeaderFooterView * foot = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"foot"];
//    [foot setBackgroundColor:[UIColor skinViewScreenColor]];
//    [foot.contentView setBackgroundColor:[UIColor skinViewScreenColor]];
//
//    return foot;
//}
//
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    GameLobby3Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"GameLobby3Cell"];
//    if (!cell) {
//        cell = [[NSBundle mainBundle] loadNibNamed:@"GameLobby3Cell" owner:self options:nil].firstObject;
//        [tableView registerNib:[UINib nibWithNibName:@"GameLobby3Cell" bundle:nil] forCellReuseIdentifier:@"GameLobby3Cell"];
//    }
//    GameLobby3Model *model = [self.list safeObjectAtIndex:indexPath.section];
//    cell.model = model;
//    [cell setSelected:(self.index == indexPath.section) animated:NO];
//    return cell;
//}
//
//#pragma mark - UITableViewDelegate
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (self.index == indexPath.section) {
//        return;
//    }
//    self.index = indexPath.section;
//}

#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    GameLobby3Model *model = [self.list safeObjectAtIndex:self.index];
    return model.nextMenu.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    GameLobby3Model *model = [self.list safeObjectAtIndex:self.index];
    GameLobby3MenuModel *menu = [model.nextMenu safeObjectAtIndex:indexPath.row];
    if (menu.name.length && menu.icon.width == 240.0 && menu.icon.height == 285.0) {
        GameLobby3NameMenuCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GameLobby3NameMenuCell" forIndexPath:indexPath];
        cell.menu = menu;
        return cell;
    }else{
        GameLobby3MenuCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GameLobby3MenuCell" forIndexPath:indexPath];
        cell.menu = menu;
        return cell;
    }
}


#pragma mark - UICollectionViewDelegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.delegate && [self.delegate respondsToSelector:@selector(waterFallCollectionReusableView:didSelectRowAtUrl:)]) {
        GameLobby3Model *model = [self.list safeObjectAtIndex:self.index];
        GameLobby3MenuModel *menu = [model.nextMenu safeObjectAtIndex:indexPath.item];
        [self.delegate waterFallCollectionReusableView:self didSelectRowAtUrl:menu.url];
    }
}

#pragma mark - Events
-(void)scrollPan:(UIPanGestureRecognizer *)otherGestureRecognizer{
    if (otherGestureRecognizer.state == UIGestureRecognizerStateBegan) {
        
    }else if (otherGestureRecognizer.state == UIGestureRecognizerStateChanged) {

        CGFloat velocity = [otherGestureRecognizer velocityInView:otherGestureRecognizer.view].y;
        [self.delegate waterFallCollectionReusableView:self SuperScrollEnable:[self getSuperCanScrollWithVelocity:velocity]];
        if ([self getCanScrollWithVelocity:velocity]) {
            [otherGestureRecognizer setTranslation:CGPointMake(0, 0) inView:self.collectionView];
            return;
        }

        CGFloat offsetY = [otherGestureRecognizer translationInView:otherGestureRecognizer.view].y;
        [self showBottomSlidePanOffsetY:offsetY Velocity:velocity];
        
    }else if (otherGestureRecognizer.state == UIGestureRecognizerStateEnded ||
              otherGestureRecognizer.state == UIGestureRecognizerStateFailed ||
              otherGestureRecognizer.state == UIGestureRecognizerStateCancelled){
        [self.delegate waterFallCollectionReusableView:self SuperScrollEnable:YES];
    }
}
-(void)tapLeftMenuAction:(UIButton *)sender{
    if (self.index == sender.tag) {
        return;
    }
    self.index = sender.tag;
}

#pragma mark - GET/SET
//-(UITableView *)tableView{
//    if (!_tableView) {
//        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
//        _tableView.backgroundColor = [UIColor clearColor];
//        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//        [_tableView registerClass:UITableViewHeaderFooterView.class forHeaderFooterViewReuseIdentifier:@"head"];
//        [_tableView registerClass:UITableViewHeaderFooterView.class forHeaderFooterViewReuseIdentifier:@"foot"];
//        [self addSubview:_tableView];
//        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.bottom.equalTo(self);
//            make.width.mas_equalTo(100);
//            make.left.equalTo(self).offset(5);
//        }];
//        [_tableView setBounces:NO];
//    }
//    return _tableView;
//}

-(UIView *)leftListView{
    if (!_leftListView) {
        _leftListView = [[UIView alloc] init];
        [self addSubview:_leftListView];
        [_leftListView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(5);
            make.bottom.top.equalTo(self);
            make.width.mas_equalTo(LEFTITEMSIZE.width);
        }];
    }
    return _leftListView;
}


-(UICollectionView *)collectionView{
    if (!_collectionView) {
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:_layout];
        [_collectionView setBackgroundColor:[UIColor clearColor]];
        [_collectionView registerNib:[UINib nibWithNibName:@"GameLobby3MenuCell" bundle:nil] forCellWithReuseIdentifier:@"GameLobby3MenuCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"GameLobby3NameMenuCell" bundle:nil] forCellWithReuseIdentifier:@"GameLobby3NameMenuCell"];
        [self addSubview:_collectionView];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView addGestureRecognizer:self.pan];
        [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.leftListView.mas_right);
            make.top.bottom.right.equalTo(self);
        }];
        [_collectionView setBounces:YES];
        self.bottomImageView.alpha = 0;
    }
    return _collectionView;
}

-(UIImageView *)bottomImageView{
    if (!_bottomImageView) {
        _bottomImageView = [[UIImageView alloc] init];
        _bottomImageView.backgroundColor = [UIColor whiteColor];
        
        [self addSubview:_bottomImageView];
        [_bottomImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.top.right.equalTo(self.collectionView);
            make.height.mas_equalTo(30);
        }];
    }
    return _bottomImageView;
}


-(void)setList:(NSArray<GameLobby3Model *> *)list{
    if ([_list isEqualToArray:list]) {
        return;
    }
    _list = list;
    [_list enumerateObjectsUsingBlock:^(GameLobby3Model * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        btn.indicatorView = [[UIActivityIndicatorView alloc] init];
        [btn.indicatorView setHidesWhenStopped:YES];
        [btn addSubview:btn.indicatorView];
        [btn.indicatorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(btn);
        }];
        [btn.indicatorView setColor:[UIColor skinNaviBgColor]];
        [btn.indicatorView startAnimating];
        [btn sd_setImageWithURL:[NSURL URLWithString:obj.activeIcon] forState:UIControlStateDisabled completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            [btn.indicatorView stopAnimating];
        }];
        [btn sd_setImageWithURL:[NSURL URLWithString:obj.icon] forState:UIControlStateNormal completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            [btn.indicatorView stopAnimating];
        }];
        btn.contentMode = UIViewContentModeScaleAspectFit;
        btn.tag = idx;
        [btn addTarget:self action:@selector(tapLeftMenuAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.leftListView addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.leftListView);
            make.right.equalTo(self.leftListView);
            make.width.equalTo(btn.mas_height).multipliedBy(LEFTITEMSIZE.width / LEFTITEMSIZE.height);
            make.top.equalTo(self.leftListView).offset(idx * LEFTITEMSIZE.height + (idx + 1) * 5.0);
            make.height.mas_equalTo(LEFTITEMSIZE.height);
        }];
        
        if (idx == 0) {
            [btn setEnabled:NO];
        }
    }];
    if (self.index != 0) {
        self.index = 0;
    }
}
-(void)setDelegate:(id<HomeWaterFallCollectionReusableViewDelegate>)delegate{
    _delegate = delegate;

}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

-(UIPanGestureRecognizer *)pan{
    if (!_pan) {
        _pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(scrollPan:)];
        _pan.delegate = self;
    }
    return _pan;
}

-(NSNumber *)kBQualificationSpacing{
    if (!_kBQualificationSpacing) {
        _kBQualificationSpacing = @(100);
    }
    return _kBQualificationSpacing;
}


@end
