//
//  HomeUserOnlineView.h
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/12/5.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeUserOnlineView : UIView

@property (nonatomic,assign) NSUInteger online;

@end

NS_ASSUME_NONNULL_END
