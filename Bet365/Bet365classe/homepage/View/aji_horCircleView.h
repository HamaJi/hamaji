//
//  aji_horCircleView.h
//  Bet365
//
//  Created by HHH on 2018/7/2.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

//typedef NS_ENUM(NSInteger, HorCircleBearings) {
//    HorCircleBearings_LEFT = 0,
//    HorCircleBearings_RIGHT,
//};

@class aji_horCircleView;
@protocol aji_horCircleViewDelegate<NSObject>

-(void)HorCircleView:(aji_horCircleView *)view TapUpAtIndex:(NSUInteger)idx;

-(void)HorCircleView:(aji_horCircleView *)view MoveCompletedWasIndex:(NSUInteger)idx;

-(void)stopAnimationHorCircleView:(aji_horCircleView *)view;

@end
@interface aji_horCircleView : UIView

@property (nonatomic,strong)NSArray <NSAttributedString *>*list;

@property (nonatomic,assign)NSUInteger index;

@property (nonatomic,strong)UIColor *fontCorlor;

@property (nonatomic,weak) id<aji_horCircleViewDelegate> delegate;

@property (nonatomic,assign) BOOL isDisplayScreen;

-(void)startAnimation;

-(void)stopAnimation;


@end
