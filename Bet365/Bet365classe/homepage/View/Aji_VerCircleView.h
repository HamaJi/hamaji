//
//  Aji_VerCircleView.h
//  Bet365
//
//  Created by super_小鸡 on 2019/1/7.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class Aji_VerCircleView;

@protocol Aji_VerCircleViewDelegate<NSObject>

-(void)VerCicleView:(Aji_VerCircleView *)view TapUpAtIndex:(NSUInteger)idx;

-(void)VerCicleView:(Aji_VerCircleView *)view MoveCompletedWasIndex:(NSUInteger)idx;

@end

@interface Aji_VerCircleView : UIView

@property (nonatomic, strong)NSArray <NSAttributedString *>*strList;

@property (nonatomic,assign)NSInteger index;

@property (nonatomic,assign,readonly)BOOL isPlay;

@property (nonatomic,weak)id<Aji_VerCircleViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
