//
//  HomeCusHeadItemView.h
//  Bet365
//
//  Created by adnin on 2018/12/8.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HomeCusHeadItemView;

@protocol HomeCusHeadItemViewDelegate<NSObject>

-(void)HomeCusHeadItemView:(HomeCusHeadItemView *)view TapCusTemplate:(CusTemplate *)kTemplate;

@end

@interface HomeCusHeadItemView : UIView

@property (nonatomic,weak)CusTemplate *kTemplate;

@property (nonatomic,weak)id<HomeCusHeadItemViewDelegate> delegate;

@property (nonatomic,assign)BOOL selected;

@end
