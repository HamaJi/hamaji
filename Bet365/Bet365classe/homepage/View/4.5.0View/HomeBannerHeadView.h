//
//  HomeBannerHeadView.h
//  Bet365
//
//  Created by adnin on 2018/11/11.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICollectionSectionColorReusableView.h"
@class HomeBannerHeadView;

@protocol HomeBannerHeadViewwDelegate<NSObject>

-(void)HomeBannerHeadView:(HomeBannerHeadView *)view TapSDCycleIdx:(NSUInteger)idx;

@end

@interface HomeBannerHeadView : UICollectionSectionColorReusableView

@property (nonatomic,weak) id <HomeBannerHeadViewwDelegate> delegate;

@property (nonatomic,strong)NSArray *imageURLStringsGroup;

@end
