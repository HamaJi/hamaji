//
//  HomeBannerHeadView.m
//  Bet365
//
//  Created by adnin on 2018/11/11.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "HomeBannerHeadView.h"
@interface HomeBannerHeadView()<SDCycleScrollViewDelegate>

@property(nonatomic,strong) SDCycleScrollView *cycleScrollview;

@end
@implementation HomeBannerHeadView

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self addObserver];
    }
    return self;
}

-(instancetype)init{
    if (self = [super init]) {
        [self addObserver];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addObserver];
    }
    return self;
}

#pragma mark - Private
-(void)addObserver{
    RAC(self.cycleScrollview,imageURLStringsGroup) = RACObserve(self, imageURLStringsGroup);
}

#pragma mark - SDCycleScrollViewDelegate
-(void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    if (self.delegate && [self.delegate respondsToSelector:@selector(HomeBannerHeadView:TapSDCycleIdx:)]) {
        [self.delegate HomeBannerHeadView:self TapSDCycleIdx:index];
    }
}

#pragma mark - GET/SET
-(SDCycleScrollView *)cycleScrollview{
    if (!_cycleScrollview) {
        _cycleScrollview = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero delegate:self placeholderImage:[UIImage imageNamed:@"lunbo"]];
        _cycleScrollview.autoScrollTimeInterval = 6;
        _cycleScrollview.pageDotColor = [UIColor whiteColor];
        _cycleScrollview.pageControlStyle = SDCycleScrollViewPageContolStyleAnimated;
        _cycleScrollview.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self addSubview:_cycleScrollview];
        [_cycleScrollview mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return _cycleScrollview;
}
@end
