//
//  HomeCusHeadView.h
//  Bet365
//
//  Created by adnin on 2018/11/13.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICollectionSectionColorReusableView.h"

@class HomeCusHeadView;

@protocol HomeCusHeadViewDelegate<NSObject>

-(void)HomeCusHeadView:(HomeCusHeadView *)view TapIndex:(NSInteger)idx;

@end

@interface HomeCusHeadView : UICollectionSectionColorReusableView

@property (nonatomic,strong)NSArray <CusTemplate *>*items;

@property (nonatomic,weak) id <HomeCusHeadViewDelegate> delegate;

@property (nonatomic,assign) NSInteger index;

@property (nonatomic,assign) NSUInteger horPageMaxCount;

@end
