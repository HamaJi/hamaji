//
//  HomeHorCycleHeadView.m
//  Bet365
//
//  Created by adnin on 2018/11/11.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "HomeHorCycleHeadView.h"
#import "YLMarquee.h"

@interface HomeHorCycleHeadView()<aji_horCircleViewDelegate>

@property (nonatomic,strong)aji_horCircleView *horCircleView;

@property (nonatomic,strong)UIImageView *line;

@property (nonatomic,strong) UIImageView *iconImageView;

@property (nonatomic,strong) YLMarquee *marView;

@end

@implementation HomeHorCycleHeadView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor skinViewScreenColor];
        
    }
    return self;
}

#pragma mark - aji_horCircleViewDelegate
-(void)HorCircleView:(aji_horCircleView *)view TapUpAtIndex:(NSUInteger)idx{
    if (self.delegate && [self.delegate respondsToSelector:@selector(HomeHorCycleHeadView:TapUpAtIndex:)]) {
        [self.delegate HomeHorCycleHeadView:self TapUpAtIndex:idx];
    }
}

-(void)HorCircleView:(aji_horCircleView *)view MoveCompletedWasIndex:(NSUInteger)idx{
    
}

-(void)stopAnimationHorCircleView:(aji_horCircleView *)view{
    
}

-(void)stopHorCycleAnimation{
    [self.horCircleView stopAnimation];
}

#pragma mark - GET/SET
-(UIImageView *)line{
    if (!_line) {
        _line = [[UIImageView alloc] init];
        [self addSubview:_line];
        [_line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self);
            make.height.mas_equalTo(0.5);
        }];
        _line.backgroundColor = [UIColor skinViewKitSelColor];
    }
    return _line;
}

-(UIImageView *)iconImageView{
    if (!_iconImageView) {
        UIImage *mImage = [[UIImage imageNamed:@"home_announcement_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        _iconImageView = [[UIImageView alloc] initWithImage:mImage];
        [self addSubview:_iconImageView];
        [_iconImageView setTintColor:[UIColor skinViewKitSelColor]];
        [_iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset(5);
            make.left.equalTo(self).offset(10);
            make.bottom.equalTo(self).offset(-5);
            make.width.equalTo(_iconImageView.mas_height).multipliedBy(46.0 / 40.0);
        }];
    }
    return _iconImageView;
}
-(aji_horCircleView *)horCircleView{
    if (!_horCircleView) {
        _horCircleView = [[aji_horCircleView alloc] init];
        _horCircleView.backgroundColor = [UIColor clearColor];
        _horCircleView.fontCorlor = [UIColor skinTextItemNorColor];
        [self addSubview:_horCircleView];
        [_horCircleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.iconImageView.mas_right).offset(5);
            make.top.right.bottom.equalTo(self);
        }];
        _horCircleView.delegate = self;
    }
    return _horCircleView;
}

-(void)setItems:(NSArray<NoticeModel *> *)items{
    if (!items.count) {
        
    }
    _items = items;
    NSMutableArray *list = @[].mutableCopy;
    [_items enumerateObjectsUsingBlock:^(NoticeModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.noticeContent) {
            NSMutableAttributedString *mStr = [obj.noticeContent htmlStr].mutableCopy;
            if ([BET_CONFIG.skin.skinName isEqualToString:[UIColor skinBlackName]]) {
                NSString *newStr = [NSString stringWithFormat:@"<span style=\"color:white\">%@</span>", obj.noticeContent];
                mStr = [newStr htmlStr].mutableCopy;
            }
            if (mStr) {
                [list safeAddObject:mStr];
            }
        }
    }];
    self.horCircleView.list = list;
    [self.horCircleView startAnimation];
    
}

- (YLMarquee *)marView
{
    if (!_marView) {
        _marView = [[YLMarquee alloc] init];
        _marView.textColor = [UIColor blackColor];
        _marView.textFont = [UIFont systemFontOfSize:15];
        [_marView addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
            [UIViewController routerJumpToUrl:kRouterNotice];
        }];
        _marView.velocity = 50;
        [self addSubview:_marView];
        [_marView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.iconImageView.mas_right).offset(5);
            make.right.equalTo(self);
            make.top.equalTo(self);
            make.bottom.equalTo(self);
        }];
    }
    return _marView;
}

@end
