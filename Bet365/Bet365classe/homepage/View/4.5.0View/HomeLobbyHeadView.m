//
//  HomeLobbyHeadView.m
//  Bet365
//
//  Created by luke on 2019/6/17.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "HomeLobbyHeadView.h"
#import <HMSegmentedControl.h>
#import "AllLiveData.h"

@interface HomeLobbyHeadView ()

@property (nonatomic,strong) UIButton *selectBtn;
@property (nonatomic,strong) HMSegmentedControl *segment;
@property (nonatomic,strong) NSArray *titleArrs;
@property (nonatomic,strong) NSMutableArray *btnArrs;

@end

@implementation HomeLobbyHeadView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor skinViewBgColor];
        self.clipsToBounds = YES;
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.btnArrs enumerateObjectsUsingBlock:^(UIButton *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj borderForColor:JesseGrayColor(235) borderWidth:0.5 borderType:UIBorderSideTypeTop | UIBorderSideTypeBottom];
    }];
    [self.segment borderForColor:JesseGrayColor(235) borderWidth:0.5 borderType:UIBorderSideTypeTop | UIBorderSideTypeBottom];
}

- (void)setLobbyData:(NSArray<GameCenterModel *> *)lobbyData
{
    if (!lobbyData) {
        return;
    }
    if (_lobbyData == lobbyData) {
        return;
    }
    _lobbyData = lobbyData;
    [self.btnArrs removeAllObjects];
    [lobbyData enumerateObjectsUsingBlock:^(GameCenterModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        
        UIButton *btn = [UIButton addButtonWithTitle:obj.name font:14 color:[self getTextColor:obj.type] target:self action:@selector(tapClick:)];
        UIImage *norImg = [UIImage imageNamed:[NSString stringWithFormat:@"lobby_%li_nor",obj.type]];
        UIImage *selImg = [UIImage imageNamed:[NSString stringWithFormat:@"lobby_%li_sel",obj.type]];
        [btn setImage:norImg forState:UIControlStateSelected];
        [btn setImage:selImg forState:UIControlStateNormal];
        
        [btn setTitleColor:[UIColor skinTextTitileColor] forState:UIControlStateSelected];
        [btn setBackgroundImage:[UIImage imageWithColor:[self getTextColor:obj.type]] forState:UIControlStateSelected];
        if ([BET_CONFIG.skin.skinName isEqualToString:[UIColor skinPinkName]]) {
            [btn setBackgroundImage:[UIImage imageWithColor:[UIColor skinNaviBgColor]] forState:UIControlStateNormal];
        }else{
            [btn setBackgroundImage:[UIImage imageWithColor:[UIColor skinViewBgColor]] forState:UIControlStateNormal];
        }
        [btn setImagePosition:LXMImagePositionTop spacing:5];
        if (idx == 0) {
            btn.selected = YES;
            self.selectBtn = btn;
        }
        btn.tag = 999 + idx;
        [self addSubview:btn];
        [self.btnArrs addObject:btn];
    }];
    if (self.btnArrs.count > 1) {
        [self.btnArrs mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:0 leadSpacing:0 tailSpacing:0];
        [self.btnArrs mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.height.mas_equalTo(70);
        }];
    }else if(self.btnArrs.count == 1){
        [self.btnArrs mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.left.right.equalTo(self);
            make.height.mas_equalTo(70);
        }];
    }
    GameCenterModel *model = lobbyData.firstObject;
    if (model.type == GameCenterType_lottery || model.type == GameCenterType_ele || model.type == GameCenterType_chess || model.type == GameCenterType_electricContest) {
        self.segment.sectionTitles = [ALL_DATA getSegmentWithType:model.type];
        self.segment.selectedSegmentIndex = 0;
    }else{
        [self.segment mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0);
        }];
    }
}

- (void)tapClick:(UIButton *)sender
{
    self.selectBtn.selected = NO;
    sender.selected = YES;
    self.selectBtn = sender;
    NSInteger tag = sender.tag - 999;
    GameCenterModel *model = self.lobbyData[tag];
    [self.segment setSelectedSegmentIndex:0];
    if (model.type == GameCenterType_lottery ||
        model.type == GameCenterType_ele ||
        model.type == GameCenterType_chess ||
        model.type == GameCenterType_electricContest) {
        [self.segment mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(35);
        }];
        self.segment.sectionTitles = [ALL_DATA getSegmentWithType:model.type];
    }else{
        [self.segment mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0);
        }];
    }
    [self layoutIfNeeded];
    if (self.delegate && [self.delegate respondsToSelector:@selector(HomeLobbyHeadViewTap:)]) {
        [self.delegate HomeLobbyHeadViewTap:model.type];
    }
}

- (void)segmentClick:(HMSegmentedControl *)handle
{
    NSInteger tag = self.selectBtn.tag - 999;
    GameCenterModel *model = self.lobbyData[tag];
    NSInteger segemntIndex = handle.selectedSegmentIndex;
    NSString *cate = nil;
    AllLiveGamesModel *allModel = nil;
    if (model.type == GameCenterType_lottery) {
        cate = ALL_DATA.filterBaseSource[segemntIndex].value;
    }else{
        switch (model.type) {
                case GameCenterType_ele:
                allModel = ALL_DATA.eleDatas[segemntIndex];
                break;
                case GameCenterType_electricContest:
                allModel = ALL_DATA.electricContestDatas[segemntIndex];
                break;
                case GameCenterType_chess:
                allModel = ALL_DATA.chessDatas[segemntIndex];
                break;
            default:
                break;
        }
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(HomeLobbyHeadViewTap:segmentTap:model:)]) {
        [self.delegate HomeLobbyHeadViewTap:model.type segmentTap:cate model:allModel];
    }
}

- (UIColor *)getTextColor:(GameCenterType)index
{
    return @[JesseColor(225, 92, 93),
             JesseColor(166, 204, 104),
             JesseColor(116, 179, 223),
             JesseColor(236,175,80),
             JesseColor(230, 119, 130),
             JesseColor(168,135,235),
             JesseColor(194, 121, 227)
             ][index];
}

#pragma mark - SET/GET
- (HMSegmentedControl *)segment
{
    if (!_segment) {
        _segment = [[HMSegmentedControl alloc] init];
        _segment.backgroundColor = [UIColor skinViewBgColor];
        _segment.selectedSegmentIndex = 0;
        _segment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationNone;
        _segment.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
        _segment.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor skinViewKitSelColor],NSFontAttributeName : [UIFont systemFontOfSize:IS_IPHONE_5s ? 12 : 14]};
        _segment.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor skinViewKitDisColor],NSFontAttributeName : [UIFont systemFontOfSize:IS_IPHONE_5s ? 12 : 14]};
        [_segment addTarget:self action:@selector(segmentClick:) forControlEvents:UIControlEventValueChanged];
        [self addSubview:_segment];
        [_segment mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.bottom.equalTo(self);
            make.height.mas_equalTo(35);
        }];
    }
    return _segment;
}

- (NSMutableArray *)btnArrs
{
    if (!_btnArrs) {
        _btnArrs = [NSMutableArray array];
    }
    return _btnArrs;
}

@end
