//
//  HomeLobbyHeadView.h
//  Bet365
//
//  Created by luke on 2019/6/17.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICollectionSectionColorReusableView.h"
@class GameCenterModel;
@class AllLiveGamesModel;
@protocol HomeLobbyHeadViewDelegate <NSObject>

- (void)HomeLobbyHeadViewTap:(GameCenterType)lobbyType;

- (void)HomeLobbyHeadViewTap:(GameCenterType)lobbyType segmentTap:(NSString *_Nullable)cate model:(AllLiveGamesModel *_Nonnull)model;

@end

NS_ASSUME_NONNULL_BEGIN

@interface HomeLobbyHeadView : UICollectionSectionColorReusableView
@property (nonatomic,weak) id<HomeLobbyHeadViewDelegate>delegate;
@property (nonatomic,strong) NSArray <GameCenterModel *> *lobbyData;
@end

NS_ASSUME_NONNULL_END
