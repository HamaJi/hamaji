//
//  HomeSliderHeadView.m
//  Bet365
//
//  Created by super_小鸡 on 2019/9/22.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "HomeSliderHeadView.h"
#import <iCarousel/iCarousel.h>
#import "TimerManager.h"

#define HOMESLIDER_TIMECYCLE_KEY @"HOMESLIDER_TIMECYCLE_KEY"

#define HOMESLIDER_TIMECYCLE_SPACING 6

#define HOMESLIDER_TIMECYCLE_TOPING 10.0

@interface HomeSliderHeadView()
<iCarouselDelegate,
iCarouselDataSource,
TimerManagerDelegate,
UIGestureRecognizerDelegate>
{
    BOOL _hasUpdateHeight;
}

@property (strong, nonatomic) iCarousel *carousel;

@property (strong, nonatomic) NSMutableDictionary *imageViewData;

@property (strong, nonatomic) UIPageControl *pageControl;

@property (strong, nonatomic) UIGestureRecognizer *gestureRecognizer;

@property (nonatomic,assign) CGFloat maxHeight;

@end

@implementation HomeSliderHeadView

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self addObserver];
    }
    return self;
}

-(instancetype)init{
    if (self = [super init]) {
        [self addObserver];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addObserver];
    }
    return self;
}
-(void)dealloc{
    [TIMER_MANAGER removeDelegate:self];
}


-(void)reloadData{
    [self.carousel reloadData];
}
#pragma mark - Private
-(void)addObserver{
    self.carousel.delegate = self;
    self.carousel.dataSource = self;
    [TIMER_MANAGER addDelegate:self];
    [self.carousel addGestureRecognizer:self.gestureRecognizer];
    @weakify(self);
    [[RACObserve(self, maxHeight) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        if (self.delegate && [self.delegate respondsToSelector:@selector(HomeSliderHeadView:UpdateHeight:)]) {
            [self.delegate HomeSliderHeadView:self UpdateHeight:self.maxHeight];
        }
    }];
    //    RAC(self.cycleScrollview,imageURLStringsGroup) = RACObserve(self, imageURLStringsGroup);
}

//-(void)updateHeight{
//    if (_hasUpdateHeight) {
//        return;
//    }
//    _hasUpdateHeight = YES;
//    __block BOOL all = YES;
//    __block CGFloat maxHeight = 0.0;
//    [self.list enumerateObjectsUsingBlock:^(SliderModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        if (!obj.iconSize.height) {
//            all = NO;
//        }else if(obj.iconSize.height > maxHeight){
//            maxHeight = obj.iconSize.height;
//        }
//    }];
//    if (all && self.delegate && [self.delegate respondsToSelector:@selector(HomeSliderHeadView:UpdateHeight:)]) {
//        [self.delegate HomeSliderHeadView:self UpdateHeight:maxHeight];
//    }
//}

#pragma mark - TimerManagerDelegate
-(void)cycleTimerWithKey:(NSString *)key RestCount:(NSInteger)count{
    if ([key isEqualToString:HOMESLIDER_TIMECYCLE_KEY]) {
        if (self.carousel.currentItemIndex + 1 < self.list.count) {
            [self.carousel scrollToItemAtIndex:self.carousel.currentItemIndex + 1 animated:YES];
        }else if(self.carousel.currentItemIndex + 1 >= self.list.count ){
            if (self.carousel.currentItemIndex != 0) {
                [self.carousel scrollToItemAtIndex:0 animated:YES];
            }
        }
    }
}

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return self.list.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view{
    UIImageView *imageView = [self.imageViewData objectForKey:[NSString stringWithFormat:@"%li",index]];
    @weakify(self);
    if (!imageView) {
        imageView = [[UIImageView alloc] init];
        imageView.clipsToBounds = YES;
        imageView.cornerRadius = 8.0;
        imageView.userInteractionEnabled = YES;
        [self.imageViewData setObject:imageView forKey:[NSString stringWithFormat:@"%li",index]];
        [imageView addTapGestureRecognizer:^(UITapGestureRecognizer *recognizer, NSString *gestureId) {
            SliderModel *slider = [self.list safeObjectAtIndex:[gestureId integerValue]];
            if (self.delegate && [self.delegate respondsToSelector:@selector(HomeSliderHeadView:TapSlider:)]) {
                [self.delegate HomeSliderHeadView:self TapSlider:slider];
            }
        } tapGestureId:[NSString stringWithFormat:@"%li",index]];
    }
    SliderModel *model = [self.list safeObjectAtIndex:index];
    NSString *url = model.icon;
    UIImage *cacheImage = [[SDImageCache sharedImageCache] imageFromCacheForKey:url];
    if (!cacheImage) {
        imageView.ycz_width = WIDTH - 15;
        imageView.ycz_height = self.bounds.size.height - 20;
        imageView.ycz_y = (self.bounds.size.height - imageView.ycz_height) / 2.0;
    }
    [imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (image && !error) {
            @strongify(self);
            CGFloat scale = image.size.width / image.size.height;
            imageView.ycz_width = WIDTH - 15;
            imageView.ycz_height = imageView.ycz_width / scale + HOMESLIDER_TIMECYCLE_TOPING;
            if (self.bounds.size.height >= imageView.ycz_height) {
                imageView.ycz_y = (self.bounds.size.height - imageView.ycz_height) / 2.0 + HOMESLIDER_TIMECYCLE_TOPING;
            }else{
                imageView.ycz_height = self.bounds.size.height + HOMESLIDER_TIMECYCLE_TOPING;
                imageView.ycz_width = imageView.ycz_height *scale;
                imageView.ycz_y = 0;
            }
            model.iconSize = CGSizeMake(imageView.ycz_width, imageView.ycz_height);
            self.maxHeight = imageView.ycz_height + 2*HOMESLIDER_TIMECYCLE_TOPING;
//            [self updateHeight];
        }
    }];
    return imageView;
}

- (CATransform3D)carousel:(iCarousel *)carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform{
    
    transform = CATransform3DRotate(transform, M_PI / 8.0f, 0.0f, 1.0f, 0.0f);
    return CATransform3DTranslate(transform, 0.0f, 0.0f, offset * carousel.itemWidth);
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value{
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            return YES;
        }
        case iCarouselOptionSpacing:
        {
            return value * 1.2f;
        }
        case iCarouselOptionFadeMax:
        {
            if (carousel.type == iCarouselTypeCustom)
            {
                return 0.0f;
            }
            return value;
        }
        default:
        {
            return value;
        }
    }
}

-(void)carouselDidEndScrollingAnimation:(iCarousel *)carousel{
    self.pageControl.currentPage = carousel.currentItemIndex;
}

#pragma mark - Events
-(void)gestureRecognizerAciton:(UIGestureRecognizer *)recognizer{
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        [TIMER_MANAGER removeTimerWithKey:HOMESLIDER_TIMECYCLE_KEY];
    }else if (recognizer.state == UIGestureRecognizerStateChanged) {
        
    }else if (recognizer.state == UIGestureRecognizerStateEnded ||
              recognizer.state == UIGestureRecognizerStateFailed ||
              recognizer.state == UIGestureRecognizerStateCancelled){
        [TIMER_MANAGER addCycleTimerWithKey:HOMESLIDER_TIMECYCLE_KEY andReduceScope:HOMESLIDER_TIMECYCLE_SPACING];
    }
}

#pragma mark - UIGestureRecognizerDelegate
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

#pragma mark - GET/SET
-(iCarousel *)carousel{
    if (!_carousel) {
        _carousel = [[iCarousel alloc] init];
        _carousel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _carousel.type = iCarouselTypeRotary;
        _carousel.stopAtItemBoundary = NO;
        _carousel.scrollToItemBoundary = YES;
        _carousel.decelerationRate = 0.75;
        _carousel.backgroundColor = [UIColor skinViewScreenColor];
        [self addSubview:_carousel];
        [_carousel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
            
        }];
    }
    return _carousel;
}

-(void)setList:(NSArray<SliderModel *> *)list{
    if ([_list isEqualToArray:list]) {
        return;
    }
    _list = list;
    [self.imageViewData removeAllObjects];
    [self.carousel reloadData];
    self.pageControl.numberOfPages = _list.count;
    [TIMER_MANAGER addCycleTimerWithKey:HOMESLIDER_TIMECYCLE_KEY andReduceScope:HOMESLIDER_TIMECYCLE_SPACING];
}

-(NSMutableDictionary *)imageViewData{
    if (!_imageViewData) {
        _imageViewData = @{}.mutableCopy;
    }
    return _imageViewData;
}
-(UIPageControl *)pageControl{
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] init];
        _pageControl.hidesForSinglePage = YES;
        [self addSubview:_pageControl];
        [_pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.bottom.equalTo(self);
        }];
    }
    return _pageControl;
}

-(UIGestureRecognizer *)gestureRecognizer{
    if (!_gestureRecognizer) {
        _gestureRecognizer = [[UIGestureRecognizer alloc] initWithTarget:self action:@selector(gestureRecognizerAciton:)];
        _gestureRecognizer.delegate = self;
    }
    return _gestureRecognizer;
}

-(void)setMaxHeight:(CGFloat)maxHeight{
    _maxHeight = MAX(_maxHeight, maxHeight);
}
@end
