//
//  HomeHorCycleHeadView.h
//  Bet365
//
//  Created by adnin on 2018/11/11.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "aji_horCircleView.h"
#import "UICollectionSectionColorReusableView.h"

@class HomeHorCycleHeadView;

@protocol HomeHorCycleHeadViewDelegate<NSObject>

-(void)HomeHorCycleHeadView:(HomeHorCycleHeadView *)view TapUpAtIndex:(NSUInteger)idx;
@end

@interface HomeHorCycleHeadView : UICollectionSectionColorReusableView

@property (nonatomic,strong)NSArray <NoticeModel *>*items;

@property (nonatomic,weak) id <HomeHorCycleHeadViewDelegate> delegate;

-(void)stopHorCycleAnimation;

@end
