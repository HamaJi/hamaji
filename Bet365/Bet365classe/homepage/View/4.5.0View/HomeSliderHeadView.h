//
//  HomeSliderHeadView.h
//  Bet365
//
//  Created by super_小鸡 on 2019/9/22.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICollectionSectionColorReusableView.h"


NS_ASSUME_NONNULL_BEGIN
@class HomeSliderHeadView;

@protocol HomeSliderHeadViewDelegate<NSObject>

-(void)HomeSliderHeadView:(HomeSliderHeadView *)view TapSlider:(SliderModel *)slider;

-(void)HomeSliderHeadView:(HomeSliderHeadView *)view UpdateHeight:(CGFloat)height;

@end

@interface HomeSliderHeadView : UICollectionSectionColorReusableView

@property (nonatomic,weak) id <HomeSliderHeadViewDelegate> delegate;

@property (nonatomic,strong)NSArray <SliderModel *> *list;

-(void)reloadData;

@end

NS_ASSUME_NONNULL_END
