//
//  HomeLotteryHeadView.m
//  Bet365
//
//  Created by jesse on 2017/6/29.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "HomeLotteryHeadView.h"
#define buttonHeight 50
#define desLabelHeight 21
#define IndrowHeight 2
#define barHeight 38
#define Y 2
@interface HomeLotteryHeadView()
@property(nonatomic,strong)UIView *barView;
@property(nonatomic,strong)UIView *selectBackView;
@property(nonatomic,strong)UIView *indrow;
@property(nonatomic,strong)UIButton *currenButton;
@end
@implementation HomeLotteryHeadView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}
#pragma mark--creatUI
-(void)creatUIAndCurrenType:(HomePageKindType)kindType
{
    if (self.barView!=nil&&self.selectBackView!=nil) {
        self.currenButton.enabled = YES;
        self.currenButton = (UIButton *)[self viewWithTag:60+kindType];
        self.currenButton.enabled = NO;
        self.indrow.ycz_centerX = self.currenButton.superview.ycz_centerX;
        [self.indrow.layer.sublayers enumerateObjectsUsingBlock:^(CALayer * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isKindOfClass:[CAShapeLayer class]]) {
                if (self.currenButton.tag==60) {
                    [((CAShapeLayer *)obj) setStrokeColor:JesseColor(128, 192, 37).CGColor];
                    [((CAShapeLayer *)obj) setFillColor:JesseColor(128, 192, 37).CGColor];
                }else{
                    [((CAShapeLayer *)obj) setStrokeColor:JesseColor(206, 73, 73).CGColor];
                    [((CAShapeLayer *)obj) setFillColor:JesseColor(206, 73, 73).CGColor];
                }
            }
        }];
        
    }else{
        [self barView];
        [self selectBackView];
    }
}
#pragma mark--点击触发
-(void)desImageClick:(UIButton *)clickButton
{
    self.currenButton.enabled = YES;
    clickButton.enabled = NO;
    self.currenButton = clickButton;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.indrow.ycz_centerX = self.currenButton.superview.ycz_centerX;
        [self.indrow.layer.sublayers enumerateObjectsUsingBlock:^(CALayer * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isKindOfClass:[CAShapeLayer class]]) {
                if (clickButton.tag==60) {
                    [((CAShapeLayer *)obj) setStrokeColor:JesseColor(128, 192, 37).CGColor];
                    [((CAShapeLayer *)obj) setFillColor:JesseColor(128, 192, 37).CGColor];
                }else{
                    [((CAShapeLayer *)obj) setStrokeColor:JesseColor(206, 73, 73).CGColor];
                    [((CAShapeLayer *)obj) setFillColor:JesseColor(206, 73, 73).CGColor];
                }
            }
        }];

    } completion:^(BOOL finished) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(HomeLotteryHeadView:tapLotteryType:)]) {
            [self.delegate HomeLotteryHeadView:self tapLotteryType:(HomePageKindType)(self.currenButton.tag-60)];
        }
    }];
}
#pragma mark--初始化bar
-(UIView *)barView
{
    if (_barView==nil) {
        _barView = [[UIView alloc]init];
        [self addSubview:_barView];
        [_barView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(0);
            make.right.mas_equalTo(self.mas_right).offset(0);
            make.top.mas_equalTo(self.mas_top).offset(0);
            make.height.mas_equalTo(barHeight);
        }];
        UIView *lightIndrow = [[UIView alloc]initWithFrame:CGRectMake(0, 37, WIDTH, 0.5)];
        
        lightIndrow.backgroundColor = [UIColor skinViewKitSelColor];
        [_barView addSubview:lightIndrow];
        UIView *indrow = [[UIView alloc]initWithFrame:CGRectMake(8, 8, 2, 22)];
        indrow.backgroundColor =[UIColor redColor];
        [_barView addSubview:indrow];
        UILabel *deslabel = [[UILabel alloc]init];
        deslabel.text = @"热门彩票";
        deslabel.textAlignment = NSTextAlignmentLeft;
        deslabel.textColor = [UIColor skinTextHeadColor];
        deslabel.adjustsFontSizeToFitWidth = YES;
        [_barView addSubview:deslabel];
        [deslabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(indrow.mas_right).offset(5);
            make.top.mas_equalTo(_barView.mas_top).offset(8);
            make.height.mas_equalTo(22);
        }];
        self.backgroundColor = [UIColor skinViewBgColor];
       
    }
    return _barView;
}
#pragma mark--selctView
-(UIView *)selectBackView
{
    if (_selectBackView==nil) {
        _selectBackView = [[UIView alloc]initWithFrame:CGRectMake(0, barHeight, WIDTH,75)];
        _selectBackView.backgroundColor = [UIColor skinViewBgColor];
        [self addSubview:_selectBackView];
        for (NSInteger i = 0 ; i<2; i++) {
            UIView *BackView = [[UIView alloc]initWithFrame:CGRectMake(WIDTH/2*i+i*1, 0, WIDTH/2, 75)];
            [_selectBackView addSubview:BackView];
            UIButton *Desbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            [Desbutton setBackgroundImage:[UIImage imageNamed:ImageName(i)] forState:UIControlStateNormal];
            [Desbutton setBackgroundImage:[UIImage imageNamed:ImageName(i)] forState:UIControlStateDisabled];
            [Desbutton.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            Desbutton.tag = 60+i;
            [Desbutton addTarget:self action:@selector(desImageClick:) forControlEvents:UIControlEventTouchUpInside];
            [BackView addSubview:Desbutton];
            [Desbutton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(BackView);
                make.top.mas_equalTo(BackView.mas_top).offset(Y);
                make.width.mas_equalTo(buttonHeight);
                make.height.mas_equalTo(buttonHeight);
            }];
            UILabel *desLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 52, WIDTH/2, desLabelHeight)];
            desLabel.text = desName(i);
            desLabel.textColor = [UIColor skinTextItemNorColor];
           
            desLabel.font = [UIFont systemFontOfSize:16];
            desLabel.textAlignment = NSTextAlignmentCenter;
            [BackView addSubview:desLabel];
            if (i==0) {
                UIView *scrollviewIndrow = [[UIView alloc]initWithFrame:CGRectMake(0, 73, 100, 2)];
                scrollviewIndrow.ycz_centerX = BackView.ycz_centerX;
                self.indrow = scrollviewIndrow;
                [BackView addSubview:self.indrow];
                UIBezierPath *path = [UIBezierPath bezierPath];
                [path setLineWidth:2];
                CGPoint moveOne = CGPointMake(47, 0);
                CGPoint lineTwo = CGPointMake(50,-3);
                CGPoint lineThree = CGPointMake(53, 0);
                CGPoint lineFour = CGPointMake(100, 0);
                [path moveToPoint:CGPointMake(0, 0)];
                [path addLineToPoint:moveOne];
                [path addLineToPoint:lineTwo];
                [path addLineToPoint:lineThree];
                [path addLineToPoint:lineFour];
                [path closePath];
                CAShapeLayer *layer = [CAShapeLayer layer];
                layer.path = path.CGPath;
                layer.strokeColor = JesseColor(128, 192, 37).CGColor;
                layer.fillColor = JesseColor(128, 192, 37).CGColor;
                layer.lineWidth = path.lineWidth;
                [self.indrow.layer addSublayer:layer];
                self.currenButton = Desbutton;
                UIView *indrowDown = [[UIView alloc]init];
                indrowDown.backgroundColor = JesseColor(236, 236, 236);
                [_selectBackView addSubview:indrowDown];
                [indrowDown mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_equalTo(BackView.mas_right).offset(0);
                    make.centerY.mas_equalTo(_selectBackView);
                    make.height.mas_equalTo(56);
                    make.width.mas_equalTo(1);
                }];
                
            }
        }
    }
    return _selectBackView;
}
NSString *ImageName(NSInteger num){
    return @[@"Kfcoffical",@"kfcCredit"][num];
}
NSString *desName(NSInteger num){
    return @[@"官方玩法",@"信用玩法"][num];
}
@end
