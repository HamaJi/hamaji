//
//  HomeDefaultCell.h
//  Bet365
//
//  Created by jesse on 2017/6/29.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeDefaultCell : UICollectionViewCell
-(void)updateCurrenDes:(NSString *)lotteryName AndDate:(NSString *)date AndLogImageName:(NSString *)lotteryLogName AndKindLog:(NSString *)kindLog;
-(void)upMoreImage:(UIImage *)image;/**传递当前更多玩法描述**/
-(void)updateDate:(NSString *)date;
@end
