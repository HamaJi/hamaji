//
//  HomeCusHeadItemView.m
//  Bet365
//
//  Created by adnin on 2018/12/8.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "HomeCusHeadItemView.h"

@interface HomeCusHeadItemView()
@property (strong, nonatomic) UIImageView *iconImageView;
@property (strong, nonatomic) UILabel *titileLabel;
@property (strong, nonatomic) UIImageView *downClipView;
@property (strong, nonatomic) UIView *contentView;
@end

@implementation HomeCusHeadItemView

-(instancetype)init{
    if (self = [super init]) {
        [self setUI];
        [self addObserver];
    }
    return self;
}


-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self setUI];
        [self addObserver];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setUI];
        [self addObserver];
    }
    return self;
}

-(void)setUI{

    UIImage *img = [UIImage imageNamed:@ "home_cushead_donw" ];
    img = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.contentView setBackgroundColor:[UIColor skinViewCusHeadBgColor]];
    [self.downClipView setTintColor:[UIColor skinViewCusHeadBgColor]];
    [self.downClipView setImage:img];
    
    self.userInteractionEnabled = YES;
}

-(void)addObserver{
    
    @weakify(self);
    [self addTapGestureRecognizer:^(UITapGestureRecognizer *recognizer, NSString *gestureId) {
        @strongify(self);
        if (self.delegate && [self.delegate respondsToSelector:@selector(HomeCusHeadItemView:TapCusTemplate:)]) {
            [self.delegate HomeCusHeadItemView:self TapCusTemplate:self.kTemplate];
        }
    }];
    
    [[RACObserve(self, selected) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        if ([x boolValue]) {
            self.downClipView.hidden = NO;
            [self.titileLabel setFont:[UIFont boldSystemFontOfSize:15]];
        }else{
            self.downClipView.hidden = YES;
            [self.titileLabel setFont:[UIFont boldSystemFontOfSize:13]];
        }
    }];
}




#pragma mark - GET/SET
-(void)setKTemplate:(CusTemplate *)kTemplate{
    _kTemplate = kTemplate;
//    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SerVer_Url,@""]] placeholderImage:[UIImage imageNamed:@"gameLoading"]];
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:kTemplate.headImage] placeholderImage:[UIImage imageNamed:@"gameLoading"]];
    self.titileLabel.text = kTemplate.headName;
}

-(UIImageView *)iconImageView{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.contentMode = UIViewContentModeScaleAspectFit;
        [_iconImageView setContentHuggingPriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisVertical];
        [_iconImageView setContentHuggingPriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
        
        [_iconImageView setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisVertical];
        [_iconImageView setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
        
        [self.contentView addSubview:_iconImageView];
        [_iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.contentView);
            make.top.equalTo(self.contentView).offset(5);
            
        }];
    }
    return _iconImageView;
}

-(UILabel *)titileLabel{
    if (!_titileLabel) {
        _titileLabel = [UILabel setAllocLabelWithText:@"" FontOfSize:11 rgbColor:0x000000];
        _titileLabel.textColor = [UIColor skinViewKitSelColor];
        [self.contentView addSubview:_titileLabel];
        [_titileLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.iconImageView.mas_bottom).offset(5);
            make.bottom.equalTo(self.contentView.mas_bottom).offset(-5);
            make.centerX.equalTo(self.contentView);
        }];
    }
    return _titileLabel;
}

-(UIView *)contentView{
    if (!_contentView) {
        _contentView = [[UIView alloc] init];
        _contentView.backgroundColor = [UIColor clearColor];
        [self addSubview:_contentView];
        [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self).offset(-12);
            make.left.right.top.equalTo(self);
        }];
    }
    return _contentView;
}

-(UIImageView *)downClipView{
    if (!_downClipView) {
        _downClipView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_cushead_donw"]];
        [self addSubview:_downClipView];
        [_downClipView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView.mas_bottom).offset(-4);
            make.bottom.equalTo(self);
            make.centerX.equalTo(self);
        }];
    }
    return _downClipView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
