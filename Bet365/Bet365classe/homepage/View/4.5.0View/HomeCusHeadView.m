//
//  HomeCusHeadView.m
//  Bet365
//
//  Created by adnin on 2018/11/13.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "HomeCusHeadView.h"
#import "HomeCusHeadItemView.h"

#define HOMECUSHEAD_MIN_WIDHT (WIDTH / (self.horPageMaxCount * 1.0))
@interface HomeCusHeadView()<HomeCusHeadItemViewDelegate>
@property (nonatomic,strong)UIScrollView *scrollView;
@property (nonatomic,strong)NSMutableArray <HomeCusHeadItemView *>*itemViewList;
@end

@implementation HomeCusHeadView

-(instancetype)init{
    if (self = [super init]) {
        
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        
    }
    return self;
}

-(UIScrollView *)scrollView{
    
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        [self addSubview:_scrollView];
//        _scrollView.bounces = NO;
        _scrollView.scrollEnabled  =YES;
        _scrollView.userInteractionEnabled = YES;
        _scrollView.backgroundColor = [UIColor clearColor];
        [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return _scrollView;
}

-(void)setItems:(NSArray<CusTemplate *> *)items{
    if ([_items isEqualToArray:items]) {
        return;
    }
    _items = items;
    [self.scrollView removeAllSubviews];
    [self.itemViewList removeAllObjects];
    if (!_items.count) {
        return;
    }
    if (_items.count < self.horPageMaxCount) {
        self.horPageMaxCount = _items.count;
    }
    UIView *contentView = [[UIView alloc] init];
    [_items enumerateObjectsUsingBlock:^(CusTemplate * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        HomeCusHeadItemView *view = [[HomeCusHeadItemView alloc] init];
        view.kTemplate = obj;
        view.delegate = self;
        [contentView addSubview:view];
        [self.itemViewList addObject:view];
    }];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.scrollView addSubview:contentView];
        [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.equalTo(self.scrollView);
            make.width.mas_equalTo(HOMECUSHEAD_MIN_WIDHT *self.itemViewList.count);
            make.height.equalTo(self.scrollView);
        }];
        
        if (self.itemViewList.count > 1) {
            [self.itemViewList mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedItemLength:HOMECUSHEAD_MIN_WIDHT leadSpacing:0 tailSpacing:0];
            [self.itemViewList mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.bottom.equalTo(contentView);
            }];
        }else if(self.itemViewList.count == 1){
            [self.itemViewList.firstObject mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.top.bottom.equalTo(contentView);
                make.width.mas_equalTo(HOMECUSHEAD_MIN_WIDHT);
            }];
        }
        
        self.index = 0;
        [contentView setBackgroundColor:[UIColor skinViewCusBgColor]];
        [self.scrollView setContentSize:CGSizeMake(HOMECUSHEAD_MIN_WIDHT * self.itemViewList.count, self.bounds.size.height)];
        [self layoutIfNeeded];
    });

//    if (_items.count > 1) {
//        [self.itemViewList mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedItemLength:80 leadSpacing:0 tailSpacing:0];
////        [[contentView subviews] mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:0 leadSpacing:0 tailSpacing:0];
//    }else{
//        [self.itemViewList mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.right.equalTo(contentView);
//        }];
//    }
//    [[contentView subviews] mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.bottom.equalTo(contentView);
//    }];
//
//    [self.scrollView addSubview:contentView];
//    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.top.bottom.right.equalTo(self.scrollView);
//        make.height.mas_equalTo(self.mas_height);
//    }];

    

}

#pragma mark - HomeCusHeadItemViewDelegate
-(void)HomeCusHeadItemView:(HomeCusHeadItemView *)view TapCusTemplate:(CusTemplate *)kTemplate{
    NSInteger idx = [self.items indexOfObject:kTemplate];
    if (idx == NSNotFound) {
        idx = 0;
    }
    _index = idx;
    if (self.delegate && [self.delegate respondsToSelector:@selector(HomeCusHeadView:TapIndex:)]) {
        [self.delegate HomeCusHeadView:self TapIndex:self.index];
    }
}
-(NSMutableArray <HomeCusHeadItemView *>*)itemViewList{
    if (!_itemViewList) {
        _itemViewList = @[].mutableCopy;
    }
    return _itemViewList;
}

-(void)setIndex:(NSInteger)index{
    _index = index;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.itemViewList enumerateObjectsUsingBlock:^(HomeCusHeadItemView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.selected = (idx == index);
        }];
    });
}

-(NSUInteger)horPageMaxCount{
    if (!_horPageMaxCount) {
        _horPageMaxCount = 4;
    }
    return _horPageMaxCount;
}


@end

