//
//  HomeLotteryHeadView.h
//  Bet365
//
//  Created by jesse on 2017/6/29.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICollectionSectionColorReusableView.h"

typedef enum{
HomePageOfficalType = 0,
HomePageCreditType = 1
}HomePageKindType;

@class HomeLotteryHeadView;

@protocol HomeLotteryHeadViewDelegate<NSObject>

-(void)HomeLotteryHeadView:(HomeLotteryHeadView *)view tapLotteryType:(HomePageKindType)type;

@end

@interface HomeLotteryHeadView : UICollectionSectionColorReusableView

-(void)creatUIAndCurrenType:(HomePageKindType)kindType;

@property (nonatomic,weak)id<HomeLotteryHeadViewDelegate> delegate;

@end
