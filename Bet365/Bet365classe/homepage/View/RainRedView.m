//
//  RainRedView.m
//  Bet365
//
//  Created by super_小鸡 on 2019/1/11.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "RainRedView.h"
#import <pop/POP.h>
#import "CountDownView.h"
#import "SBTickerView.h"
#import "SBTickView.h"
#import "Aji_VerCircleView.h"
@interface RainRedView()<Aji_VerCircleViewDelegate>
{
    NSString *_currentClock;
}

@property (nonatomic,strong)UIImageView *iconImageView;

@property (nonatomic,strong)UIImageView *bgImgView;

@property(nonatomic,strong)UIImageView *oldManImgView;

@property(nonatomic,strong)UIButton *submitBtn;

@property (nonatomic,strong)UILabel *centerToastLabel;

@property (nonatomic,strong)CountDownView *downView;

@property (nonatomic,strong)Aji_VerCircleView *verCircleView;

@property (nonatomic,strong)UIButton *rightRuleBtn;

#pragma mark - Animation
@property (nonatomic,strong)POPBasicAnimation *upwardAnimation;

@property (nonatomic,strong)POPBasicAnimation *fullAnimation;

@property (nonatomic,assign)NSTimeInterval time;

@property (nonatomic,assign) RACDisposable *disposable;

#pragma mark -
@property (nonatomic,assign)BOOL isPlay;
@end

@implementation RainRedView

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self setUI];
        [self addObserver];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setUI];
        [self addObserver];
    }
    return self;
}

-(instancetype)init{
    if (self = [super init]) {
        [self setUI];
        [self addObserver];
    }
    return self;
}

-(void)setUI{
    self.backgroundColor = COLOR_WITH_HEX(0x3A0C06);
    [self addSubview:self.iconImageView];
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(30, 24));
    }];
    
    [self addSubview:self.verCircleView];
    [self.verCircleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.equalTo(self);
        make.left.equalTo(self.iconImageView.mas_right);
        make.height.mas_equalTo(40);
        make.centerY.equalTo(self.iconImageView);
    }];
    
    
    
    [self addSubview:self.rightRuleBtn];
    [self.rightRuleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.verCircleView);
        make.right.equalTo(self).offset(-10);
    }];
    
    [self addSubview:self.bgImgView];
    [self.bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self);
        make.top.equalTo(self.verCircleView.mas_bottom);
    }];
    
    [self addSubview:self.oldManImgView];
    [self.oldManImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.verCircleView.mas_bottom).offset(40);
    }];
    
    [self addSubview:self.centerToastLabel];
    [self.centerToastLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.oldManImgView.mas_bottom).offset(-20);
    }];
    
    [self addSubview:self.downView];
    [self.downView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.centerToastLabel.mas_bottom).offset(10);
        make.width.mas_equalTo(180);
        make.height.mas_equalTo(38);
    }];
    
    [self addSubview:self.submitBtn];
    [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.downView.mas_bottom).offset(10);
    }];
    
}
-(void)dealloc{
    
}

-(void)stopTimeAnimation{
    if (self.disposable) {
        [self.disposable dispose];
        self.disposable = nil;
    }
    
}
-(void)addObserver{
    @weakify(self);
    self.disposable = [[RACSignal interval:1 onScheduler:[RACScheduler mainThreadScheduler]] subscribeNext:^(NSDate * _Nullable x) {
        @strongify(self);
        NSString *timeStamp = [NSDate getTimeFormatStringWithTimeStamp:self.time Format:@"HHmmss"];
        [self.downView.tickerViewList enumerateObjectsUsingBlock:^(SBTickerView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (![[_currentClock substringWithRange:NSMakeRange(idx, 1)] isEqualToString:[timeStamp substringWithRange:NSMakeRange(idx, 1)]] && time >= 0) {
                [obj setBackView:[SBTickView tickViewWithTitle:[timeStamp substringWithRange:NSMakeRange(idx, 1)] fontSize:30]];
                [obj tick:SBTickerViewTickDirectionDown animated:YES completion:nil];
            }
        }];
        _currentClock = timeStamp;
        self.time--;
    }];

}
-(void)showWithModel:(HomeRedPageEnvelopeInfoModel *)model{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.oldManImgView.layer pop_addAnimation:self.upwardAnimation forKey:@"upwardAnimation"];
        if (!model) {
            return;
        }
        if ([model.stat isEqualToString:@"-1"]) {
            self.centerToastLabel.text = model.message;
            return;
        }
        
        NSTimeInterval startTime = [model.start_time timeIntervalSinceDate:model.c_time];
        NSTimeInterval endTime = [model.end_time timeIntervalSinceDate:model.c_time];
        if (startTime > 0) {
            self.centerToastLabel.text = @"本轮红包下期开始时间";
        }else if (endTime <= 0) {
            self.centerToastLabel.text = @"本轮红包已结束";
        }else if (endTime > 0) {
            self.centerToastLabel.text = @"距离运气红包结束";
        }
        self.time = startTime > 0 ? startTime : endTime;
    });
}



-(void)stop{
    [self.oldManImgView.layer pop_removeAllAnimations];
}

-(void)VerCicleView:(Aji_VerCircleView *)view TapUpAtIndex:(NSUInteger)idx{
    
}

-(void)VerCicleView:(Aji_VerCircleView *)view MoveCompletedWasIndex:(NSUInteger)idx{
    
}

#pragma mark - Events
-(void)submitAction{
    if (self.delegate && [self.delegate respondsToSelector:@selector(RainRedView:TapActionType:)]) {
        [self.delegate RainRedView:self TapActionType:RainRedViewTapActionType_OPEN];
    }
}

-(void)ruleAction{
    if (self.delegate && [self.delegate respondsToSelector:@selector(RainRedView:TapActionType:)]) {
        [self.delegate RainRedView:self TapActionType:RainRedViewTapActionType_RULE];
    }
    
}

#pragma mark - GET/SET
-(UIImageView *)iconImageView{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"redpage_notice"]];
        
    }
    return _iconImageView;
}
-(Aji_VerCircleView *)verCircleView{
    if (!_verCircleView) {
        _verCircleView = [[Aji_VerCircleView alloc] init];
    }
    return _verCircleView;
}

-(UIButton *)rightRuleBtn{
    if (!_rightRuleBtn) {
        _rightRuleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rightRuleBtn setTitle:@"游戏规则" forState:UIControlStateNormal];
        [_rightRuleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_rightRuleBtn.titleLabel setFont:[UIFont systemFontOfSize:16]];
        [_rightRuleBtn addTarget:self action:@selector(ruleAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rightRuleBtn;
}

-(UIImageView *)bgImgView{
    if (!_bgImgView) {
        UIImage* img=[UIImage imageNamed:@"sunday_readpage_bg"];
//        UIEdgeInsets edge=UIEdgeInsetsMake(20, 20, 20,20);
        
        //UIImageResizingModeStretch：拉伸模式，通过拉伸UIEdgeInsets指定的矩形区域来填充图片
        //UIImageResizingModeTile：平铺模式，通过重复显示UIEdgeInsets指定的矩形区域来填充图
        img= [img stretchableImageWithLeftCapWidth:10 topCapHeight:img.size.height - 20];
        _bgImgView = [[UIImageView alloc] initWithImage:img];
    }
    return _bgImgView;
}

-(UIImageView *)oldManImgView{
    if (!_oldManImgView) {
        _oldManImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sunday_redpage_oldman"]];
        _oldManImgView.layer.anchorPoint = CGPointMake(0.5, 0.0);
        
    }
    return _oldManImgView;
}

-(POPBasicAnimation *)upwardAnimation{
    if (!_upwardAnimation) {
        _upwardAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerPositionY];
        _upwardAnimation.toValue = @(40);
        _upwardAnimation.fromValue = @(60);
        _upwardAnimation.duration = 1.5;
        _upwardAnimation.repeatForever=YES;
        _upwardAnimation.removedOnCompletion = NO;
        _upwardAnimation.autoreverses = YES;
    }
    return _upwardAnimation;
}

-(UILabel *)centerToastLabel{
    if (!_centerToastLabel) {
        _centerToastLabel = [UILabel setAllocLabelWithText:@"没有配置活动" FontOfSize:15 rgbColor:0xFFFFFF];
    }
    return _centerToastLabel;
}

-(CountDownView *)downView{
    if (!_downView) {
        _downView = [[[NSBundle mainBundle] loadNibNamed:@"CountDownView" owner:self options:nil] firstObject];
        
    }
    return _downView;
}

-(UIButton *)submitBtn{
    if (!_submitBtn) {
        _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_submitBtn setImage:[UIImage imageNamed:@"sunday_redpage_submit"] forState:UIControlStateNormal];
        [_submitBtn addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _submitBtn;
}



-(void)setVerModelList:(NSArray<HomeRedPageWinModel *> *)verModelList{
    if ([_verModelList isEqualToArray:verModelList]) {
        return;
    }
    
    self.verCircleView.delegate = self;
    self.verCircleView.backgroundColor = COLOR_WITH_HEX(0x400703);
    NSMutableArray <NSAttributedString *>*list = @[].mutableCopy;
    [verModelList enumerateObjectsUsingBlock:^(HomeRedPageWinModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *str = [NSString stringWithFormat:@"会员：%@ 成功中奖 %.2f元",obj.account,obj.money];
        NSMutableAttributedString *maStr = [[NSAttributedString alloc] initWithString:str].mutableCopy;
        [maStr addColor:COLOR_WITH_HEX(0xd6ab51) substring:str];
        [maStr addFont:[UIFont systemFontOfSize:13] substring:@"会员："];
        [maStr addFont:[UIFont systemFontOfSize:13] substring:obj.account];
        [maStr addFont:[UIFont systemFontOfSize:13] substring:@" 成功中奖 "];
        [maStr addColor:COLOR_WITH_HEX(0xF4C191) substring:@" 成功中奖 "];
        [maStr addFont:[UIFont systemFontOfSize:15] substring:StringFormatWithFloat(obj.money)];
        [maStr addColor:COLOR_WITH_HEX(0xC9B83F) substring:StringFormatWithFloat(obj.money)];
        if (maStr) {
            [list addObject:maStr];
        }
    }];
    self.verCircleView.strList = list;
}
@end
