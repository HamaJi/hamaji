//
//  DrawWinnerRevolveView.m
//  Bet365
//
//  Created by adnin on 2019/3/21.
//  Copyright © 2019年 HHH. All rights reserved.
//

#import "DrawWinnerRevolveView.h"
@interface DrawWinnerRevolveView()

@property (nonatomic,strong)NSMutableDictionary *cornerDic;

@end

@implementation DrawWinnerRevolveView

- (instancetype)initWithFrame:(CGRect)frame withData:(NSDictionary*)dataDic{
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self setData:dataDic];
    }
    return self;
}

- (void)setData:(NSDictionary*)dataDic{
    self.cornerDic = [[NSMutableDictionary alloc]init];
    int totalCount = 0;
    for (NSString *key in dataDic) {
        totalCount += [(NSNumber*)dataDic[key] intValue];
    }
    
    for (NSString *key in dataDic) {
        double corner = [(NSNumber*)dataDic[key] doubleValue] / (double)totalCount * 360;
        [self.cornerDic setObject:@(corner) forKey:key];
        
    }
}

-(void)drawRect:(CGRect)rect{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextClearRect(ctx, rect);
    
    srand((unsigned)time(0));
    
    CGPoint center = CGPointMake(self.frame.size.width/2.0, self.frame.size.width/2.0);
    
    float angle_start = radians(-90);
    float angle_end = radians(-90);
    int i = 0;
    
    
    UIColor *greenColor = COLOR_WITH_HEX(0x98E2B6);
    UIColor *whiteColor = [UIColor whiteColor];
    
    //遍历字典，画饼
    for (NSString *key in self.cornerDic) {
        
        angle_start = angle_end;
        angle_end = angle_start+ radians([(NSNumber*)self.cornerDic[key] doubleValue]);
        
        UIColor *color = nil;
        if (i%2 == 0) {
            color = greenColor;
        }else{
            color = whiteColor;
        }
        //画扇形
        drawArc(ctx, center, self.frame.size.width/2-self.layer.borderWidth, angle_start, angle_end, color);
        i++;
        
        CATextLayer *txtLayer = [self textLayer:key rotate:angle_start + (angle_end-angle_start)/2];
        [self.layer addSublayer:txtLayer];
    }
}



- (CATextLayer*)textLayer:(NSString*)text rotate:(CGFloat)angel
{
    CATextLayer *txtLayer = [CATextLayer layer];
    //设置每个layer的长度都为转盘的直径
    txtLayer.frame = CGRectMake(0, 0, self.frame.size.width, 23);
    txtLayer.anchorPoint = CGPointMake(0.5, 0.5);
    
    NSMutableAttributedString *mAtext = [[NSMutableAttributedString alloc] initWithString:text];
    
    [mAtext addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:18] range:NSMakeRange(0, text.length)];
    [mAtext addAttribute:NSVerticalGlyphFormAttributeName value:@(1) range:NSMakeRange(0, text.length)];
    [mAtext addAttribute:NSWritingDirectionAttributeName value:@[@(NSWritingDirectionLeftToRight)] range:NSMakeRange(0, text.length)];
    
    
    
    txtLayer.string = mAtext;
    txtLayer.alignmentMode = [NSString stringWithFormat:@"right"];
    [txtLayer setPosition:CGPointMake(self.frame.size.width/2, self.frame.size.width/2)];

    txtLayer.transform = CATransform3DMakeRotation(angel,0,0,1);
    return txtLayer;
}
        
        static inline float radians(double degrees) {
            return degrees * M_PI / 180;
        }
        
        static inline void drawArc(CGContextRef ctx, CGPoint point, float radius,float angle_start, float angle_end, UIColor* color)
        {
            CGContextSetFillColor(ctx, CGColorGetComponents( [color CGColor]));
            CGContextMoveToPoint(ctx, point.x, point.y);
            
            //画扇形
            CGContextAddArc(ctx, point.x, point.y, radius,  angle_start, angle_end, 0);
            CGContextFillPath(ctx);
            
            //分割线
            CGContextSetRGBStrokeColor(ctx, 1, 1, 1, 1);
            
            //设置线条宽度
            CGContextSetLineWidth(ctx, 2);
            CGContextMoveToPoint(ctx, point.x, point.y);
            
            //另一端的坐标
            CGPoint point1 = CGPointMake(point.x + radius*cos(angle_start), point.y + radius*sin(angle_start));
            CGContextAddLineToPoint(ctx, point1.x, point1.y);
            CGContextStrokePath(ctx);
        }
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
