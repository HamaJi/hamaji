//
//  HomeUserOnlineView.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/12/5.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "HomeUserOnlineView.h"
#import "UIView+PanPosition.h"

@interface HomeUserOnlineView ()<UIViewPanPositionSerializing>

@property (nonatomic,strong) UIImageView *bgImageView;

@property (nonatomic,strong) UILabel *titileLabel;

@end

@implementation HomeUserOnlineView

-(instancetype)init{
    if (self = [super init]) {
        [self setUI];
    }
    return self;
}
-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self setUI];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setUI];
    }
    return self;
}

#pragma mark - Private
-(void)setUI{
    self.beAllowedPanPostion = YES;
    self.backgroundColor = [UIColor clearColor];
    self.clipsToBounds = YES;
    self.layer.cornerRadius = 25.0 / 2.0;
    
    [self addSubview:self.bgImageView];
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self addSubview:self.titileLabel];
    [self.titileLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(10);
        make.right.equalTo(self).offset(-10);
        make.top.equalTo(self).offset(5);
        make.bottom.equalTo(self).offset(-5);
        make.height.mas_equalTo(15);
    }];
}
#pragma mark - UIViewPanPositionSerializing
-(void)panAtPosition:(CGPoint)position{
    
}

#pragma mark - GET/SET
-(UIImageView *)bgImageView{
    if (!_bgImageView) {
        _bgImageView = [[UIImageView alloc] init];
        _bgImageView.backgroundColor = COLOR_WITH_HEX_ALP(0x8dd3fb, 0.5);
    }
    return _bgImageView;
}

-(UILabel *)titileLabel{
    if (!_titileLabel) {
        _titileLabel = [UILabel setAllocLabelWithText:@"当前在线人数:0" FontOfSize:14 rgbColor:0xffffff];
    }
    return _titileLabel;
}

-(void)setOnline:(NSUInteger)online{
    _online = online;
    self.titileLabel.text = [NSString stringWithFormat:@"当前在线人数:%li",_online];
    [self layoutIfNeeded];
}
@end
