//
//  CountDownView.m
//  Bet365
//
//  Created by super_小鸡 on 2019/1/14.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "CountDownView.h"
#import "SBTickView.h"

@interface CountDownView()
@property (weak, nonatomic) IBOutlet SBTickerView *ticker1;
@property (weak, nonatomic) IBOutlet SBTickerView *ticker2;
@property (weak, nonatomic) IBOutlet SBTickerView *ticker3;
@property (weak, nonatomic) IBOutlet SBTickerView *ticker4;
@property (weak, nonatomic) IBOutlet SBTickerView *ticker5;
@property (weak, nonatomic) IBOutlet SBTickerView *ticker6;

@end
@implementation CountDownView

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        dispatch_async(dispatch_get_main_queue(), ^{ 
            self.tickerViewList = @[self.ticker1,self.ticker2,self.ticker3,self.ticker4,self.ticker5,self.ticker6];
            [self.tickerViewList enumerateObjectsUsingBlock:^(SBTickerView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [obj setFrontView:[SBTickView tickViewWithTitle:@"0" fontSize:30]];
            }];
        });
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.tickerViewList = @[self.ticker1,self.ticker2,self.ticker3,self.ticker4,self.ticker5,self.ticker6];
            [self.tickerViewList enumerateObjectsUsingBlock:^(SBTickerView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [obj setFrontView:[SBTickView tickViewWithTitle:@"0" fontSize:30]];
            }];
        });
    }
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
