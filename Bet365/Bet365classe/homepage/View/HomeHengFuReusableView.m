//
//  HomeHengFuReusableView.m
//  Bet365
//
//  Created by luke on 2018/11/16.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "HomeHengFuReusableView.h"
#import "Bet365HengFuModel.h"

@interface HomeHengFuReusableView ()
/**  */
@property (nonatomic,strong) UIImageView *hengfuImageView;

@end

@implementation HomeHengFuReusableView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self hengfuImageView];
    }
    return self;
}

- (void)setModel:(Bet365HengFuModel *)model{
    _model = model;
    NSURL *url = nil;
    if ([model.hfImage hasPrefix:@"http"]) {
        url = [NSURL URLWithString:model.hfImage];
    }else{
        url = [NSURL URLWithString:StringFormatWithStr(SerVer_Url, model.hfImage)];
    }
    [self.hengfuImageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"lunbo"]];
}

+ (NSString *)getIdentifier
{
    return @"HomeHengFuReusableView";
}

- (UIImageView *)hengfuImageView
{
    if (!_hengfuImageView) {
        _hengfuImageView = [[UIImageView alloc] init];
        _hengfuImageView.userInteractionEnabled = YES;
        [_hengfuImageView setContentMode:UIViewContentModeRedraw];
        [self addSubview:_hengfuImageView];
        [_hengfuImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self).mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
        @weakify(self);
        [_hengfuImageView addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
            @strongify(self);
            if (self.delegate && [self.delegate respondsToSelector:@selector(HomeHengFuReusableViewTapwebUrlWithUrl:)]){
                [self.delegate HomeHengFuReusableViewTapwebUrlWithUrl:self.model.webUrl];
            }
        }];
    }
    return _hengfuImageView;
}

@end
