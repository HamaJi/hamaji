//
//  aji_horCircleView.m
//  Bet365
//
//  Created by HHH on 2018/7/2.
//  Copyright © 2018年 jesse. All rights reserved.
//
typedef NS_ENUM(NSInteger, aji_horCircleViewStatus) {
    aji_horCircleViewStatus_ready = 0,
    aji_horCircleViewStatus_playing,
    aji_horCircleViewStatus_stop,
};
#import "aji_horCircleView.h"
#define HOR_CIRCLE_ANIMATION_KEY @"animationViewPosition"
#import <pop/POP.h>
@interface aji_horCircleView(){
    
}
@property (nonatomic,strong)UILabel *firLabel;
@property (nonatomic,assign)CGSize labelSize;
//@property (nonatomic,strong)UIBezierPath *movePath;
//@property (nonatomic,strong)CAKeyframeAnimation *moveAnimation;
@property (nonatomic,strong)POPBasicAnimation *upwardAnimation;
@property (nonatomic,assign)aji_horCircleViewStatus status;
@end
@implementation aji_horCircleView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self addObser];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addObser];
    }
    return self;
}
-(instancetype)init{
    if (self = [super init]) {
        [self addObser];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

-(void)addObser{

    self.clipsToBounds = YES;
    self.userInteractionEnabled = YES;
    @weakify(self);
    [self addTapGestureRecognizer:^(UITapGestureRecognizer *recognizer, NSString *gestureId) {
        @strongify(self);
        if (self.delegate && [self.delegate respondsToSelector:@selector(HorCircleView:TapUpAtIndex:)]) {
            [self.delegate HorCircleView:self TapUpAtIndex:self.index];
        }
    } tapGestureId:@"aji_horCircleView_tapUp"];

}
- (void)animationDidStop:(POPAnimation *)anim finished:(BOOL)finished{
    if (self.status != aji_horCircleViewStatus_stop) {
        self.status = aji_horCircleViewStatus_ready;
    }
    [self.firLabel.layer pop_removeAllAnimations];
    if (finished && self.status != aji_horCircleViewStatus_stop) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(HorCircleView:MoveCompletedWasIndex:)]) {
            [self.delegate HorCircleView:self MoveCompletedWasIndex:self.index];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self startAnimation];
        });
    }else{
        if (self.delegate && [self.delegate respondsToSelector:@selector(stopAnimationHorCircleView:)]) {
            [self.delegate stopAnimationHorCircleView:self];
        }
    }
}

#pragma mark - Public
-(void)startAnimation{
    if (self.status == aji_horCircleViewStatus_playing)
        return;
    if (_upwardAnimation) {
        self.upwardAnimation = nil;
    }
    NSAttributedString *attributedText = [self.list safeObjectAtIndex:self.index];
    if (!attributedText) {
        if (self.list.count) {
            attributedText = self.list.firstObject;
            self.index = 1;
        }else{
            return;
        }
    }else{
        self.index += 1;
    }
    NSString *text = attributedText.string;
    self.status = aji_horCircleViewStatus_playing;
    self.firLabel.attributedText = attributedText;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.labelSize = [text  sizeWithAttributes:attributedText.attributes];
        if (self.labelSize.width && self.labelSize.height){
            [self.firLabel.layer pop_removeAnimationForKey:HOR_CIRCLE_ANIMATION_KEY];
            [self.firLabel.layer pop_addAnimation:self.upwardAnimation forKey:HOR_CIRCLE_ANIMATION_KEY];
        }else{
            self.status = aji_horCircleViewStatus_ready;
        }
    });
}

-(void)stopAnimation{
    self.status = aji_horCircleViewStatus_stop;
    [self.firLabel.layer pop_removeAllAnimations];
}

- (void)setFontCorlor:(UIColor *)fontCorlor
{
    _fontCorlor = fontCorlor;
    self.firLabel.textColor = fontCorlor;
}


#pragma mark  - GET/SET

-(UILabel *)firLabel{
    if (!_firLabel) {
        
        _firLabel = [UILabel setAllocLabelWithText:@"" FontOfSize:15 rgbColor:0x000000];
        [_firLabel.layer setAnchorPoint:CGPointMake(0, 0.5)];
        [self addSubview:_firLabel];
        [_firLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(self);
            make.left.equalTo(self.mas_right);
        }];
    }
    return _firLabel;
}

-(POPBasicAnimation *)upwardAnimation{
    if (!_upwardAnimation) {
        _upwardAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerPositionX];
        _upwardAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
        _upwardAnimation.toValue = @(- self.labelSize.width);
        _upwardAnimation.fromValue = @(self.bounds.size.width);
        _upwardAnimation.duration = ceil(self.labelSize.width / self.bounds.size.width) * 6.0;
        @weakify(self);
        _upwardAnimation.completionBlock = ^(POPAnimation *anim, BOOL finished) {
            @strongify(self);
            [self animationDidStop:anim finished:finished];
        };
        _upwardAnimation.animationDidStartBlock = ^(POPAnimation *anim) {
            @strongify(self);
            self.status = aji_horCircleViewStatus_playing;
        };
    }
    return _upwardAnimation;
}

-(void)setList:(NSArray<NSAttributedString *> *)list{
    __block BOOL isKind = NO;
    [_list enumerateObjectsUsingBlock:^(NSAttributedString * _Nonnull _obj, NSUInteger _idx, BOOL * _Nonnull _stop) {
        NSAttributedString *obj = [list safeObjectAtIndex:_idx];
        if ([_obj isEqualToAttributedString:obj]) {
            isKind = YES;
        }
    }];
    if (!isKind) {
        _list = list;
    }
}
@end
