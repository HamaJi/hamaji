//
//  HomeCycleReusableView.h
//  Bet365
//
//  Created by HHH on 2018/7/22.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "aji_horCircleView.h"

@class HomeCycleReusableView;

@protocol HomeCycleReusableViewDelegate<NSObject>

-(void)HomeCycleReusableView:(HomeCycleReusableView *)view TapSDCycleIdx:(NSUInteger)idx;

-(void)HomeCycleReusableView:(HomeCycleReusableView *)view TaphorCircleView:(aji_horCircleView *)horCircleView;

@end

@interface HomeCycleReusableView : UICollectionReusableView

@property (nonatomic,strong)NSArray <NSAttributedString *>*noticeList;

@property (nonatomic,strong)NSArray *imageURLStringsGroup;

@property (nonatomic,weak) id<HomeCycleReusableViewDelegate> delegate;

+(NSString *)getIdentifier;

@end
