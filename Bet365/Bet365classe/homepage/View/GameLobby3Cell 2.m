//
//  GameLobby3Cell.m
//  Bet365
//
//  Created by super_小鸡 on 2019/9/4.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "GameLobby3Cell.h"

@interface GameLobby3Cell()

@property (weak, nonatomic) IBOutlet UIImageView *bubleImageView;

@end

@implementation GameLobby3Cell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    [[RACSignal combineLatest:@[[RACObserve(self, model) distinctUntilChanged],
                                [RACObserve(self, selected) distinctUntilChanged]] reduce:^id _Nullable(GameLobby3Cell *model,NSNumber *selected){
                                    return @(model != nil);
                                }] subscribeNext:^(id  _Nullable x) {
                                    if ([x boolValue]) {
                                        if (self.selected) {
                                            [self.bubleImageView sd_setImageWithURL:[NSURL URLWithString:self.model.activeIcon] placeholderImage:nil];
                                        }else{
                                            [self.bubleImageView sd_setImageWithURL:[NSURL URLWithString:self.model.icon] placeholderImage:nil];
                                        }
                                    }
                                }];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
