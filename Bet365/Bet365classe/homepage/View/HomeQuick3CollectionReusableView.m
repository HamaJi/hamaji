//
//  HomeQuick3CollectionReusableView.m
//  Bet365
//
//  Created by super_小鸡 on 2019/9/5.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "HomeQuick3CollectionReusableView.h"

@interface HomeQuick3CollectionReusableView()

@property (nonatomic,strong) UIImageView *mask;

@property (nonatomic,strong) UIImageView *bgImageView;

@property (nonatomic,strong) UILabel *regardsLabel;

@property (nonatomic,strong) UILabel *loginLabel;

@property (nonatomic,strong) UILabel *ucLabel;

@end

@implementation HomeQuick3CollectionReusableView

-(instancetype)init{
    if (self = [super init]) {
        [self setUI];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setUI];
    }
    return self;
}

-(void)setDelegate:(id<HomeQuick3CollectionReusableViewDelegate>)delegate{
    _delegate = delegate;
    [self.loginLabel setUserInteractionEnabled:(_delegate != nil)];
    [self.ucLabel setUserInteractionEnabled:(_delegate != nil)];
    @weakify(self);
    [[RACSignal combineLatest:@[[RACObserve(USER_DATA_MANAGER.userInfoData, account) distinctUntilChanged],
                                [RACObserve(USER_DATA_MANAGER, isLogin) distinctUntilChanged]] reduce:^id _Nonnull(NSString *account,NSNumber *login){
                                    return @(account.length && [login boolValue]);
                                }] subscribeNext:^(id  _Nullable x) {
                                    @strongify(self);
                                    NSString *loginTitile = [x boolValue] ? USER_DATA_MANAGER.userInfoData.account : @"请登录";
                                    NSMutableAttributedString *mAtr = [[NSMutableAttributedString alloc] initWithString:loginTitile];
                                    [mAtr addUnderlineForSubstring:loginTitile];
                                    [mAtr addFont:[UIFont systemFontOfSize:13] substring:loginTitile];
                                    [mAtr addColor:COLOR_WITH_HEX(0xffffff) substring:loginTitile];
                                    self.loginLabel.attributedText = mAtr;
                                }];
}

-(void)setUI{

    
    [self addSubview:self.bgImageView];
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(5);
        make.left.equalTo(self).offset(5);
        make.right.equalTo(self).offset(-5);
        make.bottom.equalTo(self);
    }];
    
    if (![BET_CONFIG.allocation.skinName isEqualToString:[UIColor skinGoldName]]) {
        [self addSubview:self.mask];
        [self.mask mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset(5);
            make.left.equalTo(self).offset(5);
            make.right.equalTo(self).offset(-5);
            make.bottom.equalTo(self);
        }];
    }
    
    
    [self addSubview:self.regardsLabel];
    [self.regardsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bgImageView).offset(10);
        make.centerY.equalTo(self.bgImageView);
    }];
    
    [self addSubview:self.loginLabel];
    [self.loginLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.regardsLabel.mas_right).offset(5);
        make.centerY.equalTo(self.bgImageView);
    }];
    
    [self addSubview:self.ucLabel];
    [self.ucLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.bgImageView.mas_right).offset(-10);
        make.centerY.equalTo(self.bgImageView);
        make.height.mas_equalTo(22);
        make.width.mas_equalTo(65);
    }];

}

-(void)setDateHour:(NSUInteger)dateHour{
    //    6 -8  上午好!     8 -11  中午好!   11 - 17  下午好   17 -23  晚上好     23 - 6   深夜了!
    NSString *hello = nil;
    if (dateHour > 6 && dateHour <= 8) {
        hello = @"上午好!";
    }else if (dateHour > 8 && dateHour <= 11) {
        hello = @"中午好!";
    }else if (dateHour > 11 && dateHour <= 17) {
        hello = @"下午好!";
    }else if (dateHour > 17 && dateHour <= 23) {
        hello = @"晚上好!";
    }else if (dateHour > 23 || dateHour <= 6) {
        hello = @"深夜了!";
    }
    self.regardsLabel.text = hello;
}

//-(void)setImgPath:(NSString *)imgPath{
//    if ([_imgPath isEqualToString:imgPath]) {
//        return;
//    }
//    _imgPath = imgPath;
//    UIImage *image = [[UIImage imageNamed:imgPath] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
//    self.imageView.image = image;
//}

-(UIImageView *)mask{
    if (!_mask) {
        _mask = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_quick2_head_mask"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_mask.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)];
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = _mask.bounds;
            maskLayer.path = maskPath.CGPath;
            _mask.layer.mask = maskLayer;
        });
    }
    return _mask;
}
-(UIImageView *)bgImageView{
    if (!_bgImageView) {
        _bgImageView = [[UIImageView alloc] init];
        if ([BET_CONFIG.allocation.skinName isEqualToString:[UIColor skinGoldName]]) {
            _bgImageView.image = [UIImage imageNamed:@"home_quick2_head_gold"];
        }else{
            _bgImageView.backgroundColor = [UIColor skinViewContentColor];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_bgImageView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)];
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = _bgImageView.bounds;
            maskLayer.path = maskPath.CGPath;
            _bgImageView.layer.mask = maskLayer;
        });
    }
    return _bgImageView;
}

-(UILabel *)regardsLabel{
    if (!_regardsLabel) {
        _regardsLabel = [UILabel setAllocLabelWithText:@"" FontOfSize:12 rgbColor:0xffffff];
    }
    return _regardsLabel;
}

-(UILabel *)loginLabel{
    if (!_loginLabel) {
        _loginLabel = [UILabel setAllocLabelWithText:@"请登录" FontOfSize:13 rgbColor:0xffffff];
        
        @weakify(self);
        [_loginLabel addTapGestureRecognizer:^(UITapGestureRecognizer *recognizer, NSString *gestureId) {
            @strongify(self);
            if (self.delegate && [self.delegate respondsToSelector:@selector(didTapnHomeQuick3CollectionReusableView:RouterKey:)]) {
                if (USER_DATA_MANAGER.isLogin) {
//                    [self.delegate didTapnHomeQuick3CollectionReusableView:self RouterKey:kRouterCenter];
                }else{
                    [self.delegate didTapnHomeQuick3CollectionReusableView:self RouterKey:kRouterLogin];
                }
                
            }
        }];
        
        
    }
    return _loginLabel;
}

-(UILabel *)ucLabel{
    if (!_ucLabel) {
        _ucLabel = [UILabel setAllocLabelWithText:@"会员中心" FontOfSize:13 rgbColor:0xffffff];
        _ucLabel.textAlignment = NSTextAlignmentCenter;
        NSString *loginTitile = @"会员中心";
        NSMutableAttributedString *mAtr = [[NSMutableAttributedString alloc] initWithString:loginTitile];
        [mAtr addUnderlineForSubstring:loginTitile];
        [mAtr addColor:COLOR_WITH_HEX(0xffffff) substring:loginTitile];
        [mAtr addFont:[UIFont systemFontOfSize:13] substring:loginTitile];
        _ucLabel.attributedText = mAtr;
        @weakify(self);
        [_ucLabel addTapGestureRecognizer:^(UITapGestureRecognizer *recognizer, NSString *gestureId) {
            @strongify(self);
            if (self.delegate && [self.delegate respondsToSelector:@selector(didTapnHomeQuick3CollectionReusableView:RouterKey:)]) {
                [self.delegate didTapnHomeQuick3CollectionReusableView:self RouterKey:kRouterCenter];
            }
        }];
    }
    return _ucLabel;
}


@end
