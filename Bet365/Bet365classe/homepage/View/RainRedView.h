//
//  RainRedView.h
//  Bet365
//
//  Created by super_小鸡 on 2019/1/11.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeRedPageWinModel.h"
#import "HomeRedPageInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, RainRedViewTapActionType) {
    /** 开启红包*/
    RainRedViewTapActionType_OPEN = 0,
    /** 红包规则*/
    RainRedViewTapActionType_RULE
};

@class RainRedView;

@protocol RainRedViewDelegate <NSObject>

- (void)RainRedView:(RainRedView *)rainRedView TapActionType:(RainRedViewTapActionType)actionType;

@end

@interface RainRedView : UIView

@property (nonatomic,weak)id<RainRedViewDelegate> delegate;

@property (nonatomic,strong)NSArray <HomeRedPageWinModel *>* verModelList;

-(void)showWithModel:(HomeRedPageEnvelopeInfoModel *)model;

-(void)stopTimeAnimation;

@end

NS_ASSUME_NONNULL_END
