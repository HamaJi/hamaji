//
//  Aji_VerCircleView.m
//  Bet365
//
//  Created by super_小鸡 on 2019/1/7.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "Aji_VerCircleView.h"
#import <pop/POP.h>

@interface Aji_VerCircleView()
@property (nonatomic,strong)UILabel *titileLabel;
@end

@implementation Aji_VerCircleView

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self setUI];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setUI];
    }
    return self;
}

-(instancetype)init{
    if (self = [super init]) {
        [self setUI];
    }
    return self;
}

-(void)setUI{
    self.clipsToBounds = YES;
    [self addSubview:self.titileLabel];
    [self.titileLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.centerY.equalTo(self.mas_centerY);
    }];
}

-(void)show{

    if (self.index + 1 > self.strList.count) {
        self.index = 0;
    }
    NSAttributedString *mstr = [self.strList safeObjectAtIndex:self.index];
    self.titileLabel.attributedText = mstr;
    self.index ++;
    [self bottom2CenterAnimation:^(BOOL finished) {
        [self center2TopAnimation:^(BOOL finished) {
            [self show];
        }];
    }];
}

-(void)reset{
    [self stop];
    [self show];
}

-(void)stop{
    _isPlay = NO;
    self.index = 0;
    [self.titileLabel.layer pop_removeAllAnimations];
}

-(void)center2TopAnimation:(void(^)(BOOL finished))completed{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        POPBasicAnimation* positionAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerPositionY];
        positionAnimation.toValue = @(-self.titileLabel.bounds.size.height / 2.0);
        positionAnimation.fromValue = @(self.bounds.size.height / 2.0);
        positionAnimation.duration = 1;
        [positionAnimation setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
            [self.titileLabel.layer pop_removeAllAnimations];
            completed ? completed(finished) : nil;
        }];
        [self.titileLabel.layer pop_addAnimation:positionAnimation forKey:@"center2TopAnimation"];
    });
}

-(void)bottom2CenterAnimation:(void(^)(BOOL finished))completed{
    dispatch_async(dispatch_get_main_queue(), ^{
        POPBasicAnimation *positionAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerPositionY];
        positionAnimation.fromValue = @(self.bounds.size.height );
        positionAnimation.toValue = @(self.bounds.size.height / 2.0);
        positionAnimation.duration = 1;
        [positionAnimation setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
            completed ? completed(finished) : nil;
        }];
        [self.titileLabel.layer pop_addAnimation:positionAnimation forKey:@"bottom2CenterAnimation"];
    });
}

#pragma mark - GET/SET
-(UILabel *)titileLabel{
    if (!_titileLabel) {
        _titileLabel = [UILabel setAllocLabelWithText:@"" FontOfSize:15 rgbColor:0x000000];
        [_titileLabel.layer setAnchorPoint:CGPointMake(0.5, 0.5)];
    }
    return _titileLabel;
}

-(void)setStrList:(NSArray<NSAttributedString *> *)strList{
    if ([_strList isEqualToArray:strList]) {
        return;
    }
    _strList = strList;
    [self reset];
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

