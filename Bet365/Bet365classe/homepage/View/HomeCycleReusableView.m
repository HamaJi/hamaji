//
//  HomeCycleReusableView.m
//  Bet365
//
//  Created by HHH on 2018/7/22.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "HomeCycleReusableView.h"

@interface HomeCycleReusableView()<SDCycleScrollViewDelegate,aji_horCircleViewDelegate>

@property (nonatomic,strong)SDCycleScrollView *cycleScrollview;
@property (nonatomic,strong)aji_horCircleView *horCircleView;
@property (nonatomic,strong)UILabel *noticLabel;
@property (nonatomic,strong)UIImageView *line;
@end

@implementation HomeCycleReusableView

-(instancetype)init{
    if (self = [super init]) {
        [self addObserver];
    }
    return self;
}
-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self addObserver];
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addObserver];
    }
    return self;
}

-(void)addObserver{
    RAC(self.horCircleView,list) = RACObserve(self, noticeList);
    @weakify(self);
    [RACObserve(self, noticeList) subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        [self.horCircleView startAnimation];
    }];
    [self setBackgroundColor:COLOR_WITH_HEX(0xffffff)];
}
#pragma mark - PubLish
+(NSString *)getIdentifier{
    return @"HomeCycleReusableView";
}

#pragma mark - SDCycleScrollViewDelegate
-(void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    if (self.delegate && [self.delegate respondsToSelector:@selector(HomeCycleReusableView:TapSDCycleIdx:)]) {
        [self.delegate HomeCycleReusableView:self TapSDCycleIdx:index];
    }
}
#pragma GET/SET
-(SDCycleScrollView *)cycleScrollview{
    if (!_cycleScrollview){
        _cycleScrollview = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero delegate:self placeholderImage:[UIImage imageNamed:@"lunbo"]];
        _cycleScrollview.pageDotColor = [UIColor whiteColor];
        //_cycleScrollview.pageControlStyle = SDCycleScrollViewPageContolStyleNone;
        _cycleScrollview.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _cycleScrollview.autoScrollTimeInterval = 2.0f;
        [self addSubview:_cycleScrollview];
        [_cycleScrollview mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.equalTo(self);
            make.bottom.equalTo(self.horCircleView.mas_top);
        }];
    }
    return _cycleScrollview;
}
-(UIImageView *)line{
    if (!_line) {
        _line = [[UIImageView alloc] init];
        [self addSubview:_line];
        [_line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self);
            make.height.mas_equalTo(0.5);
        }];
        _line.backgroundColor = [UIColor groupTableViewBackgroundColor];
    }
    return _line;
}
-(UILabel *)noticLabel{
    if (!_noticLabel) {
        _noticLabel = [UILabel setAllocLabelWithText:@"公告" FontOfSize:15 rgbColor:0xFF0000];
        _noticLabel.textAlignment = NSTextAlignmentCenter;
        _noticLabel.layer.cornerRadius = 2;
        _noticLabel.layer.masksToBounds = YES;
        _noticLabel.layer.borderWidth = 1;
        _noticLabel.layer.borderColor = COLOR_WITH_HEX(0xFF0000).CGColor;
        [self addSubview:_noticLabel];
        [_noticLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.line.mas_top).offset(-7.5);
            make.left.equalTo(self).offset(5);
            make.height.mas_equalTo(30);
            make.width.mas_equalTo(44);
        }];
        
    }
    return _noticLabel;
}
-(aji_horCircleView *)horCircleView{
    if (!_horCircleView) {
        
        _horCircleView = [[aji_horCircleView alloc] init];
        [self addSubview:_horCircleView];
        [_horCircleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.noticLabel.mas_right).offset(5);
            make.right.equalTo(self);
            make.height.mas_equalTo(44.5);
            make.bottom.equalTo(self.line.mas_top);
        }];
        _horCircleView.delegate = self;
    }
    return _horCircleView;
}
-(void)setImageURLStringsGroup:(NSArray *)imageURLStringsGroup{
    __block BOOL isKind = NO;
    [_imageURLStringsGroup enumerateObjectsUsingBlock:^(NSString * _Nonnull _obj, NSUInteger _idx, BOOL * _Nonnull _stop) {
        NSString *obj = [imageURLStringsGroup safeObjectAtIndex:_idx];
        if ([_obj isEqualToString:obj]) {
            isKind = YES;
        }
    }];
    if (!isKind) {
        _imageURLStringsGroup = imageURLStringsGroup;
        self.cycleScrollview.imageURLStringsGroup = _imageURLStringsGroup;
        [self.cycleScrollview setAutoScroll:YES];
    }
}


#pragma mark - aji_horCircleViewDelegate
-(void)HorCircleView:(aji_horCircleView *)view TapUpAtIndex:(NSUInteger)idx{
    if (self.delegate && [self.delegate respondsToSelector:@selector(HomeCycleReusableView:TaphorCircleView:)]) {
        [self.delegate HomeCycleReusableView:self TaphorCircleView:view];
    }
}
@end
