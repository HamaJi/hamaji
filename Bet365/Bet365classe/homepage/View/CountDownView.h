//
//  CountDownView.h
//  Bet365
//
//  Created by super_小鸡 on 2019/1/14.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBTickerView.h"
NS_ASSUME_NONNULL_BEGIN

@interface CountDownView : UIView

@property (nonatomic,strong)NSArray <SBTickerView *>*tickerViewList;

@end

NS_ASSUME_NONNULL_END
