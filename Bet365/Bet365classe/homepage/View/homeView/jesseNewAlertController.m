//
//  jesseNewAlertController.m
//  Bet365
//
//  Created by jesse on 2018/1/27.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "jesseNewAlertController.h"
static void *markViewKey = &markViewKey;
static void *timeStorageKey =&timeStorageKey;
@implementation jesseNewAlertController
/**初始化当前背景**/
-(instancetype)init{
    if (self=[super init]) {
     
    }
    return self;
}
-(void)presentAlertView:(KFCPHomeAlertView *)alertView isSpringAnimation:(BOOL)isAnimation anTimer:(CGFloat)time{

    [self checkCurrenViewHierarchy];  /**处理层级**/
    objc_setAssociatedObject(self, markViewKey, alertView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    NSDictionary *ParameterDic = @{@"isSpring":@(isAnimation),@"timer":@(time)};
    objc_setAssociatedObject(self,timeStorageKey,ParameterDic,OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    self.popView = [self window];
    self.markView = [[UIView alloc]init];
    self.markView.frame = self.popView.frame;
    [self.popView addSubview:self.markView];
    [self setAnnimationColor:alertView];
}

-(void)setAnnimationColor:(KFCPHomeAlertView *)alert{
    
    /**执行当前任务**/
    void(^startAnimation)()=^(){
        CGAffineTransform trans = CGAffineTransformMakeRotation(30);
        alert.transform = trans;
        [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.3 initialSpringVelocity:5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            alert.transform =CGAffineTransformIdentity;
            alert.center = self.markView.center;
        } completion:^(BOOL finished) {
            
        }];
    };
    void(^addSubView)()=^(){
        alert.ycz_centerX = self.markView.ycz_centerX;
        alert.ycz_y = -alert.size.height-alert.size.height/2;
        [self.markView addSubview:alert];
        startAnimation();
    };
    [UIView animateWithDuration:0.2 delay:0 usingSpringWithDamping:0.5 initialSpringVelocity:5 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.markView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    } completion:^(BOOL finished) {
        if (finished)addSubView();
    }];
}
-(void)checkCurrenViewHierarchy{
    if (self.markView)[self.markView removeFromSuperview];
}
-(void)dismiss{
    id alert = objc_getAssociatedObject(self, markViewKey);
    KFCPHomeAlertView *alertObject = alert;
    void (^dismissObject)() = ^(){
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            alertObject.ycz_centerY = self.markView.ycz_height + alertObject.ycz_height;
            self.markView.alpha = 0;
        } completion:^(BOOL finished) {
            [alertObject removeFromSuperview];
            [self.markView removeFromSuperview];
        }];

    };
    
    
    [UIView animateWithDuration:0.2 delay:0 usingSpringWithDamping:0.2 initialSpringVelocity:5 options:UIViewAnimationOptionCurveEaseIn animations:^{
        alertObject.transform = CGAffineTransformMakeRotation(-0.2);
        alertObject.ycz_centerY -=50;
    } completion:^(BOOL finished) {
        dismissObject();
    }];

}
-(void)removeObjectView{
    id alert = objc_getAssociatedObject(self, markViewKey);
    KFCPHomeAlertView *alertObject = alert;
    [self.markView removeFromSuperview];
    [alertObject removeFromSuperview];
    
}
-(UIWindow *)window{
//    NSEnumerator *superRator = [UIApplication.sharedApplication.windows reverseObjectEnumerator];
//    for (UIWindow *window in superRator) {
//        BOOL isScreen = (window.screen==[UIScreen mainScreen]);
//        BOOL ishide = window.isHidden&&window.alpha>0;
//        if (isScreen&&ishide&&[window isKeyWindow]) {
//            return window;
//        }
//    }
    return [UIApplication sharedApplication].delegate.window;
}
@end
@implementation  UIViewController(jesseNewAlertController)
-(void)setJesse_alertController:(jesseNewAlertController *)jesse_alertController{
    self.jesse_alertController = jesse_alertController;
}
-(jesseNewAlertController *)jesse_alertController{
    id popController = objc_getAssociatedObject(self, _cmd);
    if (popController==nil) {
        popController = [[jesseNewAlertController alloc]init];
        objc_setAssociatedObject(self, @selector(jesse_alertController), popController, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

    }
    return popController;
}
@end
