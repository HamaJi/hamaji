//
//  KFCPActivityController.m
//  Bet365
//
//  Created by jesse on 2017/7/4.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "KFCPActivityController.h"
#import "RequestUrlHeader.h"
#import "LukeUserAdapter.h"
#import "UIViewControllerSerializing.h"
@interface KFCPActivityController ()
<
WKNavigationDelegate,
WKUIDelegate,
UIWebViewDelegate,
UIViewControllerSerializing
>

@property (nonatomic,strong)NSString *hongbaoUrlString;

@end


@implementation KFCPActivityController

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    return YES;
}

+(void)load{
    
//<<<<<<< HEAD
//=======
//    [MGJRouter registerURLPattern:ROUTER_YHHD toHandler:^(NSDictionary *routerParameters) {
//        if ([ROUTER_YHHD isEqualToString:routerParameters[MGJRouterParameterURL]]) {
//                KFCPActivityController *activit = [[KFCPActivityController alloc]init];
//                activit.hidesBottomBarWhenPushed = YES;
//                NSString *stringBy = [NSString stringWithFormat:@"%@%@?_t=%@",SerVer_Url,ActivityhtmlUrl,[LukeUserAdapter getNowTimeTimestamp]];
//                [activit loadhongBaoUrl:stringBy];
//                activit.safari_url = stringBy;
//                [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:activit animated:YES];
//        }
//    }];
//
//    [MGJRouter registerURLPattern:ROUTER_WEB_REQUEST toHandler:^(NSDictionary *routerParameters) {
//        if ([ROUTER_WEB_REQUEST isEqualToString:routerParameters[MGJRouterParameterURL]]) {
//            NSString *url = routerParameters[MGJRouterParameterUserInfo][@"url"];
//            KFCPActivityController *activit = [[KFCPActivityController alloc]init];
//            activit.hidesBottomBarWhenPushed = YES;
//            [activit loadhongBaoUrl:url];
//            activit.safari_url = url;
//            [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:activit animated:YES];
//        }
//    }];
//
//    [MGJRouter registerURLPattern:ROUTER_WEB_HONGBAO toHandler:^(NSDictionary *routerParameters) {
//        if ([ROUTER_WEB_HONGBAO isEqualToString:routerParameters[MGJRouterParameterURL]]) {
//            KFCPActivityController *activit = [[KFCPActivityController alloc]init];
//            activit.hidesBottomBarWhenPushed = YES;
//            [activit loadhongBaoUrl:StringFormatWithStr(SerVer_Url, ROUTER_WEB_HONGBAO)];
//            activit.safari_url = StringFormatWithStr(SerVer_Url, ROUTER_WEB_HONGBAO);
//            [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:activit animated:YES];
//        }
//    }];
//
//    [MGJRouter registerURLPattern:ROUTER_SPORTS_GAME toHandler:^(NSDictionary *routerParameters) {
//        NSString *value = routerParameters[MGJRouterParameterUserInfo][@"type"];
//        if ([ROUTER_SPORTS_GAME isEqualToString:routerParameters[MGJRouterParameterURL]] &&
//            [value isEqualToString:@"bb"]) {
//            if ([USER_DATA_MANAGER shouldPush2Login]) {
//                if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"TEST"]) {
//                    [SVProgressHUD showErrorWithStatus:@"试玩帐号不能进入游戏，请注册成为会员"];
//                }else if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"VHY"]) {
//                    [SVProgressHUD showErrorWithStatus:@"推广帐号不能进入游戏"];
//                }else if([USER_DATA_MANAGER shouldPush2Login]){
//                    KFCPActivityController *activityVC = [[KFCPActivityController alloc] init];
//                    NSString *stringBy = [NSString stringWithFormat:@"%@/liveRedirect.html?isMobile=true&liveCode=%@&gameType=%@",SerVer_Url,value,@""];
//                    [activityVC loadhongBaoUrl:stringBy];
//                    activityVC.safari_url = stringBy;
//                    [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:activityVC animated:YES];
//                    activityVC.hidesBottomBarWhenPushed = YES;
//
//                }
//            }
//        }
//    }];
//
//    [MGJRouter registerURLPattern:ROUTER_AGENT_DECLARE toHandler:^(NSDictionary *routerParameters) {
//        if ([ROUTER_AGENT_DECLARE isEqualToString:routerParameters[MGJRouterParameterURL]]) {
//            if ([USER_DATA_MANAGER shouldPush2Login]) {
//                KFCPActivityController *activityVC = [[KFCPActivityController alloc] init];
//                NSString *stringBy = StringFormatWithStr(SerVer_Url, @"/views/app_agendDelaer.html");
//                [activityVC loadhongBaoUrl:stringBy];
//                activityVC.safari_url = stringBy;
//                [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:activityVC animated:YES];
//                activityVC.hidesBottomBarWhenPushed = YES;
//            }
//        }
//    }];
//>>>>>>> master
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self createRightBarItemWithTitle:@"跳转Safari" action:@selector(openSafari)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    JesseAppdelegate.allowRotation = YES;
    if (self.hongbaoUrlString) {
        [self loadhongBaoUrl:self.hongbaoUrlString];
    }
//    [SVProgressHUD showWithStatus:@"加载中..."];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    JesseAppdelegate.allowRotation = NO;
    [SVProgressHUD dismiss];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    NSNumber *resetOrientationTarget = [NSNumber numberWithInt:UIInterfaceOrientationUnknown];
    [[UIDevice currentDevice] setValue:resetOrientationTarget forKey:@"orientation"];
    NSNumber *orientationTarget = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:orientationTarget forKey:@"orientation"];
}

#pragma mark--跳转web
-(void)loadWebBy:(NSString *)requestUrl
{
    self.safari_url = requestUrl;
    [SVProgressHUD dismissWithCompletion:nil];
    [SVProgressHUD showWithStatus:@"加载中..."];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:requestUrl]];
    [self.web loadRequest:request];
}
#pragma mark--打开safari浏览器
-(void)openSafari{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.safari_url]];
}
#pragma mark--跳转safari
-(void)loadWebOpenUrlBySafari:(NSString *)url forParameter:(NSString *)joinString{
    if(joinString)url=[NSString stringWithFormat:@"%@?%@",url,joinString];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}
#pragma mark--跳转web
-(void)loadWebUrl:(NSString *)url dismissView:(void (^)())disBlock{
    disBlock();
    self.safari_url = url;
    [SVProgressHUD dismissWithCompletion:nil];
    [SVProgressHUD showWithStatus:@"加载中..."];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [self.web loadRequest:request];
}
-(void)loadhongBaoUrl:(NSString *)url{
    self.safari_url = url;
    [SVProgressHUD dismissWithCompletion:^{
        
    }];
    [SVProgressHUD showWithStatus:@"加载中..."];
    if (![self.hongbaoUrlString isEqualToString:url]) {
        self.hongbaoUrlString = url;
    }
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?_t=%@",url,[LukeUserAdapter getNowTimeTimestamp]]]];
    [self.specialWeb loadRequest:request];

}
-(void)loadWebHtmlBy:(NSString *)url{
    [self.specialWeb loadHTMLString:url baseURL:[NSURL URLWithString:SerVer_Url]];
}
-(void)dealloc
{
    [SVProgressHUD dismissWithCompletion:nil];
}
#pragma mark--webDelegae
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    NSString *obs = request.URL.absoluteString;
    if ([obs hasPrefix:@"alipays://"] || [obs hasPrefix:@"alipay://"]||[obs hasPrefix:@"mqqapi://"]||[obs hasPrefix:@"weixin://"]) {
        BOOL bSucc = [[UIApplication sharedApplication]openURL:request.URL];
        if (!bSucc) {
            // 未安装支付宝 进入网页支付
        }
        return NO;
    }
    return YES;
}
#pragma WKWebview--delegate
-(void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    NSString *url  = navigationAction.request.URL.absoluteString;
    if (navigationAction.targetFrame == nil) {
        [webView loadRequest:navigationAction.request];
    }
    [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if ([url containsString:@"/Home/Detail"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
    decisionHandler(WKNavigationActionPolicyAllow);
}
-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    [SVProgressHUD dismissWithCompletion:^{
    }];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [SVProgressHUD dismissWithCompletion:^{
    }];
}

#pragma mark--初始化web
-(WKWebView *)web
{
    if (_web==nil) {
        _web = [[WKWebView alloc]init];
        _web.navigationDelegate = self;
        [self.view addSubview:_web];
        [_web mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self.view);
        }];
    }
    return _web;
}
-(UIWebView *)specialWeb{
    if (_specialWeb==nil) {
        _specialWeb = [[UIWebView alloc]init];
        _specialWeb.scalesPageToFit = YES;
        _specialWeb.delegate = self;
        [self.view addSubview:_specialWeb];
        [_specialWeb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self.view);
        }];
    }
    return _specialWeb;
}
@end

