//
//  KFCPActivityController.m
//  Bet365
//
//  Created by jesse on 2017/7/4.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "redPickLuckywController.h"

@interface redPickLuckywController ()<UIWebViewDelegate>

@end

@implementation redPickLuckywController
+(void)load{
    [MGJRouter registerURLPattern:ROUTER_SPORTS_GAME toHandler:^(NSDictionary *routerParameters) {
        NSString *value = routerParameters[MGJRouterParameterUserInfo][@"type"];
        if ([ROUTER_SPORTS_GAME isEqualToString:routerParameters[MGJRouterParameterURL]] &&
             [value isEqualToString:@"bb"]) {
            if ([USER_DATA_MANAGER shouldPush2Login]) {
                if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"TEST"]) {
                    [SVProgressHUD showErrorWithStatus:@"试玩帐号不能进入游戏，请注册成为会员"];
                }else if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"VHY"]) {
                    [SVProgressHUD showErrorWithStatus:@"推广帐号不能进入游戏"];
                }else if([USER_DATA_MANAGER shouldPush2Login]){
                    redPickLuckywController *activityVC = [[redPickLuckywController alloc] init];
                    [activityVC loadhongBaoUrl:[NSString stringWithFormat:@"%@/liveRedirect.html?liveCode=%@&gameType=%@",SerVer_Url,value,@""]];
                    [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:activityVC animated:YES];
                    activityVC.hidesBottomBarWhenPushed = YES;
                    
                }
            }
        }
    }];
    
    [MGJRouter registerURLPattern:ROUTER_AGENT_DECLARE toHandler:^(NSDictionary *routerParameters) {
        if ([ROUTER_AGENT_DECLARE isEqualToString:routerParameters[MGJRouterParameterURL]]) {
            if ([USER_DATA_MANAGER shouldPush2Login]) {
                redPickLuckywController *activityVC = [[redPickLuckywController alloc] init];
                [activityVC loadhongBaoUrl:StringFormatWithStr(SerVer_Url, @"/views/app_agendDelaer.html")];
                [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:activityVC animated:YES];
                activityVC.hidesBottomBarWhenPushed = YES;
            }
        }
    }];
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[customLoadingTool initCustomManager] disMissFresh];
}

-(void)loadhongBaoUrl:(NSString *)url{
    [[customLoadingTool initCustomManager] showRefreshActivityAnimationForLoading];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [self.specialWeb loadRequest:request];
}
-(void)dealloc
{
    [[customLoadingTool initCustomManager] disMissFresh];
}
#pragma mark--webDelegae
-(void)webViewDidFinishLoad:(UIWebView *)webView{
     [[customLoadingTool initCustomManager] disMissFresh];
   // NSString *string = @"document.documentElement.innerHTML";
    NSString *replace  =  @"document.getElementsByClassName('vux-header')[0].remove();";
    [webView stringByEvaluatingJavaScriptFromString:replace];
}
-(UIWebView *)specialWeb{
    if (_specialWeb==nil) {
        self.automaticallyAdjustsScrollViewInsets = NO;
        _specialWeb = [[UIWebView alloc]init];
        _specialWeb.delegate = self;
        [self.view addSubview:_specialWeb];
        [_specialWeb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self.view);
        }];
    }
    return _specialWeb;
}
@end

