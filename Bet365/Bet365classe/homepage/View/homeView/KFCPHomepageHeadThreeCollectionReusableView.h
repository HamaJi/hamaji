//
//  KFCPHomepageHeadThreeCollectionReusableView.h
//  Bet365
//
//  Created by jesse on 2017/6/29.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KFCPHomepageHeadThreeCollectionReusableView : UICollectionReusableView
-(void)updateCurrenPaoMa:(NSArray *)PaoMaDataArr;
-(void)updateWinList:(NSArray *)winList;
@end
