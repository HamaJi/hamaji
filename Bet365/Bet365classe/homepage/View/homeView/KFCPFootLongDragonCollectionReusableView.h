//
//  KFCPFootLongDragonCollectionReusableView.h
//  Bet365
//
//  Created by jesse on 2017/7/24.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KFCPDragonModel.h"
#import "Bet365CQSSCCollectionViewController.h"
typedef void(^typeSelect)(SSCTotalMessageType type);
@interface KFCPFootLongDragonCollectionReusableView : UICollectionReusableView
-(void)updateCurrenGamelongDragonBy:(NSArray <KFCPDragonModel *>*)modelArr;//长龙数据
-(void)updateCurrenGameLotteryWay:(SSCTotalMessageType)sscType longObject:(NSArray <KFCPDragonModel *>*)longDragonArr lottery:(NSArray *)lotteryWayArr;
@property(copy,nonatomic)typeSelect selectType;/**选择类型**/
@end
