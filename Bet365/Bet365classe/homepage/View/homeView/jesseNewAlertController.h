//
//  jesseNewAlertController.h
//  Bet365
//
//  Created by jesse on 2018/1/27.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface jesseNewAlertController : NSObject
-(void)presentAlertView:(KFCPHomeAlertView *)alertView isSpringAnimation:(BOOL)isAnimation anTimer:(CGFloat)time;/**配合跳转**/
-(void)dismiss;
-(void)removeObjectView;
@property(nonatomic,strong)UIView *popView;/**主view**/
@property(nonatomic,strong)UIView *markView;/**MarkView**/
@end
@interface UIViewController(jesseNewAlertController)
@property(nonatomic,strong)jesseNewAlertController *jesse_alertController;/**当前配合展示**/
@end
