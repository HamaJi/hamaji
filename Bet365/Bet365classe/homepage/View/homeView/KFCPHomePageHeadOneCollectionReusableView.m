//
//  KFCPHomePageHeadOneCollectionReusableView.m
//  Bet365
//
//  Created by jesse on 2017/6/29.
//  Copyright © 2017年 jesse. All rights reserved.
//
#import "KFCPHomePageHeadOneCollectionReusableView.h"
#import <SDCycleScrollView.h>
#import "KFCPActivityController.h"
#import "LukeLiveViewController.h"
#import "KFCPHomeGameJsonModel.h"
#import "LukeUserAdapter.h"
#import "KFCPActivityController.h"
#import "ElecWZRYAllViewController.h"
#import "TuLongClassFicationViewController.h"
#import "Bet365QPViewController.h"
#import "jesseBankWapWebViewController.h"
#import "aji_horCircleView.h"
#import "LotteryFactory.h"
#import "SportEleLiveModel.h"
#define LunBoheight 170
#define AnnouncementHeight 45

#define ButtonBackHeight 83
#define BackColor JesseColor(236, 236, 236)

#define RowHeight 93
#define indorwHeight 15
#define IMAGEHEIGHT 59
#define TITLEHEIGHT 20
#define RetailtyUrl @"%@/liveRedirect.html?isMobile=true&liveCode=%@&gameType=%@"//真人接口获取返回

@interface retailCell:UICollectionViewCell
/** 模型 */
@property (nonatomic,strong) SportEleLiveModel *item;
@end
@interface retailCell()
{
    UIImageView *_gameLogo;
    UIImageView *_styleImage;
    UILabel *_gameTitle;
}
@end
@implementation retailCell
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
    }
    return self;
}
- (void)setupUI
{
    _gameTitle = [[UILabel alloc] init];
    [_gameTitle sizeToFit];
    _gameTitle.font = [UIFont systemFontOfSize:15];
    [self.contentView addSubview:_gameTitle];
    [_gameTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView).offset(-5);
        make.centerX.equalTo(self.contentView);
    }];
    _gameLogo = [[UIImageView alloc] init];
    _gameLogo.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:_gameLogo];
    [_gameLogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.contentView).offset(10);
        make.top.equalTo(self.contentView).offset(5);
        make.trailing.equalTo(self.contentView).offset(-10);
        make.height.mas_equalTo(_gameLogo.mas_width);
    }];
    _styleImage = [[UIImageView alloc] init];
    _styleImage.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:_styleImage];
    [_styleImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.top.trailing.equalTo(self.contentView);
        make.height.mas_equalTo(_styleImage.mas_width);
    }];
}

- (void)setItem:(SportEleLiveModel *)item
{
    _item = item;
    _gameTitle.text = item.name;
    [_gameLogo sd_setImageWithURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",SerVer_Url,item.logoSrc] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    [_styleImage sd_setImageWithURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",SerVer_Url,item.styleSrc] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
}
@end
NSString *const registerRetailCell = @"retailCell";
#define COLLCOLOR JesseColor(238, 238, 238)
@interface KFCPHomePageHeadOneCollectionReusableView()<SDCycleScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong) aji_horCircleView *circleView;
@property(nonatomic,strong)SDCycleScrollView *SdLunbo;
@property(nonatomic,strong)NSArray *lunBoImageArr;
@property(nonatomic,strong)UIView *buttonBackView;//按钮背景
@property(nonatomic,strong)NSArray *buttonImageArr;//button图片
@property(nonatomic,assign)BOOL isLogstate;//是否为登录状态
@property(assign,nonatomic)BOOL loadFinishLunbo;/**是否读取完配置**/
/**  */
@property (nonatomic,strong) UICollectionView *collectionView;
/**  */
@property (nonatomic,strong) NSArray *storageValueArr;

@end
@implementation KFCPHomePageHeadOneCollectionReusableView
- (UICollectionView *)collectionView
{
    if (_collectionView == nil) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.minimumLineSpacing = 8;
        layout.minimumInteritemSpacing = 8;
        layout.itemSize = CGSizeMake(collectionWidth, collectionWidth * 1.2);
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.scrollEnabled = NO;
        _collectionView.backgroundColor = COLLCOLOR;
        [_collectionView registerClass:[retailCell class] forCellWithReuseIdentifier:registerRetailCell];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.showsHorizontalScrollIndicator = NO;
        [self addSubview:_collectionView];
        [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(self);
            make.top.equalTo(self.buttonBackView.mas_bottom);
            make.height.mas_equalTo(0);
        }];
    }
    return _collectionView;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.storageValueArr.count;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(8, 8, 8, 8);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    retailCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:registerRetailCell forIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor whiteColor];
    cell.contentView.layer.cornerRadius = 10;
    cell.contentView.layer.masksToBounds = YES;
    cell.item = self.storageValueArr[indexPath.row];
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    SportEleLiveModel *item = self.storageValueArr[indexPath.row];
//    if (item.isHref && item.GameId && [item.type isEqualToString:@"彩票"]) {
//        [self pushViewControllerWithGameId:[NSString stringWithFormat:@"%ld",item.GameId]];
//    }
//    if (item.isHref && !item.GameId && [item.type isEqualToString:@"彩票"]) {
//        [UIViewController routerJumpToUrl:kRouterGCDT];
//    }
//    if (!item.isHref && item.link.length) {
//        if (USER_DATA_MANAGER.isLogin){
//          KFCPActivityController *actVC = [[KFCPActivityController alloc] init];
//            if ([item.type isEqualToString:@"快速充值"]) {
//                [actVC loadhongBaoUrl:item.link];
//            }else{
//                [actVC loadWebBy:item.link];
//            }
//            [[KFCPHomePageHeadOneCollectionReusableView findViewController:self.superview].navigationController pushViewController:actVC animated:YES];
//        }else{
//            [[KFCPHomePageHeadOneCollectionReusableView findViewController:self.superview].navigationController pushViewController:[[jesseLogBet365ViewController alloc] init] animated:YES];
//        }
//    }
//    if (item.isHref && [item.type isEqualToString:@"体育"]) {
////        Bet365SportViewController *sport = [[Bet365SportViewController alloc] init];
////        [[KFCPHomePageHeadOneCollectionReusableView findViewController:self.superview].navigationController pushViewController:sport animated:YES];
//    }
//    if (item.isHref && ![self isContainChinese:item.type] && ![item.name isEqualToString:@"AG捕鱼"] && ![item.name isEqualToString:@"AG视讯"] && ![item.name isEqualToString:@"AG国际馆"]) {
//        [self pushKFCPActivityControllerWithType:item.type gameId:@""];
//    }
//    if (item.isHref && ![self isContainChinese:item.type] && [item.name isEqualToString:@"AG捕鱼"]) {
//        [self pushKFCPActivityControllerWithType:item.type gameId:@"6"];
//    }
//    if (item.isHref && ![self isContainChinese:item.type] && ([item.name isEqualToString:@"AG视讯"] || [item.name isEqualToString:@"AG国际馆"])) {
//        [self pushKFCPActivityControllerWithType:item.type gameId:@"1"];
//    }
//    if (item.isHref && [item.type isEqualToString:@"真人视讯"]) {
//        [[KFCPHomePageHeadOneCollectionReusableView findViewController:self.superview].navigationController pushViewController:[[LukeLiveViewController alloc] init] animated:YES];
//    }
//    if (item.isHref && [item.type isEqualToString:@"棋牌"]) {
//        Bet365QPViewController *vc = [[Bet365QPViewController alloc] init];
//        [[KFCPHomePageHeadOneCollectionReusableView findViewController:self.superview].navigationController pushViewController:vc animated:YES];
//    }
//    if (item.isHref && [item.type isEqualToString:@"电子"]) {
////        [[KFCPHomePageHeadOneCollectionReusableView findViewController:self.superview].navigationController pushViewController:[[ElectronicGamesListViewController alloc] init] animated:YES];
//    }
//    if (item.isHref && [item.name isEqualToString:@"王者荣耀"]) {
//        [[KFCPHomePageHeadOneCollectionReusableView findViewController:self.superview].navigationController pushViewController:[[ElecWZRYAllViewController alloc] init] animated:YES];
//    }
//    if (item.isHref && [item.name isEqualToString:@"长龙排行"]) {
//        TuLongClassFicationViewController *tulong = [[TuLongClassFicationViewController alloc]init];
//        [tulong enterCurrenTuLongMain:ClassFication];
//        [[KFCPHomePageHeadOneCollectionReusableView findViewController:self.superview].navigationController pushViewController:tulong animated:YES];
//    }
}
//判断输入的字符串是否有中文
-(BOOL)isContainChinese:(NSString *)str
{
    for(int i=0; i< [str length];i++)
    {
        int a = [str characterAtIndex:i];
        if( a > 0x4e00 && a < 0x9fff)//判断输入的是否是中文
        {
            return YES;
        }
    }
    return NO;
}
- (void)pushKFCPActivityControllerWithType:(NSString *)type gameId:(NSString *)gameId
{
//    if (USER_DATA_MANAGER.isLogin) {
//        if ([type isEqualToString:@"lmg"]) {
//            NSString *url = [NSString stringWithFormat:BBUrl,type,gameId];
//            ElectronticWebDesViewController *webVC = [[ElectronticWebDesViewController alloc]init];
////            webVC.url = url;
//            [[KFCPHomePageHeadOneCollectionReusableView findViewController:self.superview].navigationController pushViewController:webVC animated:YES];
//        }else{
//
//        }
//
//    }else{
//        [[KFCPHomePageHeadOneCollectionReusableView findViewController:self.superview].navigationController pushViewController:[[jesseLogBet365ViewController alloc] init] animated:YES];
//    }
}

- (void)pushViewControllerWithGameId:(NSString *)gameId{
    [UIViewController enterLotteryByLotteryId:gameId];
}
//返回玩法对应KindType
LotteryType MovetcheckGameType(NSString *parameterNumber){
    __block NSString *gameType;
    [LOTTERY_FACTORY.lotteryData enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        KFCPHomeGameJsonModel *model = obj;
        if ([parameterNumber integerValue]==[model.GameId integerValue]) {
            gameType = model.type;
            *stop = YES;
        }
    }];
    NSDictionary *Gamedic=@{@"ssc":[NSString stringWithFormat:@"%d",LotteryType_CQSSC],@"fc3d":[NSString stringWithFormat:@"%d",LotteryType_FC3D],@"pk10":[NSString stringWithFormat:@"%d",LotteryType_BJPK10],@"bjkl8":[NSString stringWithFormat:@"%d",LotteryType_BJKL8],@"gdklsf":[NSString stringWithFormat:@"%d",LotteryType_GDHL10],@"gxklsf":[NSString stringWithFormat:@"%d",LotteryType_GXKL],@"pcdd":[NSString stringWithFormat:@"%d",LotteryType_PCDD],@"k3":[NSString stringWithFormat:@"%d",LotteryType_KSK3],@"11x5":[NSString stringWithFormat:@"%d",LotteryType_GD11TO5],@"lhc":[NSString stringWithFormat:@"%d",LotteryType_SIX]};
    return [Gamedic[gameType] intValue];
}
#pragma mark--初始化init
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self collectionView];
        [RACObserve(self, isLogstate) subscribeNext:^(id  _Nullable x) {
            UIButton *testb = (UIButton *)[self viewWithTag:13];
            UILabel *testL = (UILabel *)[self viewWithTag:53];
            UILabel *logRegisterL = (UILabel *)[self viewWithTag:51];
            if ([x boolValue]) {
                [testb setImage:[UIImage imageNamed:@"1-6"] forState:UIControlStateNormal];
                testL.text = @"投注记录";
                logRegisterL.text = @"会员中心";
            }else{
                [testb setImage:[UIImage imageNamed:@"1-5"] forState:UIControlStateNormal];
                testL.text = @"试玩";
                logRegisterL.text = @"登入注册";
            }
        }];
    }
    return self;
}

-(void)ReloadCurrenBy:(NSArray *)currenUrlImageArr And:(NSString *)Announcement isLogin:(BOOL)isLogin ItemArr:(NSArray *)datas height:(CGFloat)height
{
    [self.SdLunbo mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(height);
    }];
    self.isLogstate = isLogin;
    NSArray *list = nil;
    if (Announcement) {
        NSMutableAttributedString *attStr = [Announcement htmlStr];
        attStr ? (list = @[attStr]) : nil;
    }
    self.circleView.list = list;
    self.lunBoImageArr = currenUrlImageArr;
    self.SdLunbo.imageURLStringsGroup = currenUrlImageArr[0];
    NSInteger rows = datas.count ? (datas.count - 1) / 4 + 1 : 0;
    CGFloat collectionHeight = (collectionWidth * 1.2 + 8) * rows + 8;
    [self.collectionView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(collectionHeight);
    }];
    self.storageValueArr = datas;
    [self.collectionView reloadData];
}

#pragma mark--sddelegate
-(void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    NSArray *selectArr = self.lunBoImageArr[1];
    [self.HomeHeadDelegate loadSDLunBoUrl:selectArr[index]];
}
#pragma mark--button点击效应触发
-(void)buttonClick:(UIButton *)currenClick
{
    AppDelegate *app = (id)[UIApplication sharedApplication].delegate;
    (currenClick.tag==10)?[self.HomeHeadDelegate topUpWithDraws]:(currenClick.tag==11)?[self.HomeHeadDelegate logRegister]:(currenClick.tag==13)?(USER_DATA_MANAGER.isLogin)?[self.HomeHeadDelegate BetRecord]:[self.HomeHeadDelegate testPlay]:(currenClick.tag==12)?[self.HomeHeadDelegate PreferActivt]:[self.HomeHeadDelegate NowChatCustomService];
    
}
#pragma mark--获取指定button
-(void)getCurrenButton:(void (^)(UIButton *))service
{
    
    service([self viewWithTag:14]);
}

#pragma mark--初始化跑马灯
-(aji_horCircleView *)circleView{
    if (!_circleView) {
        _circleView = [[aji_horCircleView alloc] init];
        [self addSubview:_circleView];
        [_circleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).offset(5);
            make.right.equalTo(self);
            make.top.equalTo(self.SdLunbo.mas_bottom);
            make.height.mas_equalTo(45);
        }];
    }
    return _circleView;
}
#pragma mark--初始化轮播
-(SDCycleScrollView *)SdLunbo
{
    if (_SdLunbo==nil){
        _SdLunbo = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero delegate:self placeholderImage:[UIImage imageNamed:@"lunbo"]];
        _SdLunbo.pageDotColor = [UIColor whiteColor];
        _SdLunbo.pageControlStyle = SDCycleScrollViewPageContolStyleAnimated;
        _SdLunbo.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self addSubview:_SdLunbo];
        [_SdLunbo mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.top.equalTo(self);
            make.height.mas_equalTo(0);
        }];
    }
    return _SdLunbo;
}

#pragma mark--初始化当前buttonback
-(UIView *)buttonBackView
{
    if (_buttonBackView==nil) {
        _buttonBackView = [[UIView alloc]init];
        _buttonBackView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_buttonBackView];
        [_buttonBackView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(self);
            make.top.equalTo(self.circleView.mas_bottom);
            make.height.mas_equalTo(ButtonBackHeight);
        }];
        UIView *indrow = [[UIView alloc]init];
        indrow.backgroundColor = BackColor;
        [_buttonBackView addSubview:indrow];
        [indrow mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.top.equalTo(_buttonBackView);
            make.height.mas_equalTo(1);
        }];
        
        UIView *bottomLine = [[UIView alloc]init];
        bottomLine.backgroundColor = BackColor;
        [_buttonBackView addSubview:bottomLine];
        [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.bottom.equalTo(_buttonBackView);
            make.height.mas_equalTo(1);
        }];
        
        CGFloat height = 68;
        CGFloat width = WIDTH/5;
        CGFloat y = 1;
        for (NSInteger i = 0 ; i<5; i++) {
            CGFloat x = width*i;
            UIView *BC = [[UIView alloc]initWithFrame:CGRectMake(x, y, width, height)];
            [_buttonBackView addSubview:BC];
            /*******添加虚线******/
            UIView *rightView = [[UIView alloc]initWithFrame:CGRectMake(width-1, 0, 1, height)];
            [BC addSubview:rightView];
            CAShapeLayer *shapelayer = [CAShapeLayer layer];
            [shapelayer setBounds:rightView.bounds];
            [shapelayer setPosition:CGPointMake(0, 35)];
            [shapelayer setStrokeColor:BackColor.CGColor];
            [shapelayer setFillColor:[UIColor clearColor].CGColor];
            [shapelayer setLineWidth:1];
            [shapelayer setLineJoin:kCALineJoinRound];
            [shapelayer setLineDashPattern:[NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:1], nil]];
            CGMutablePathRef Path = CGPathCreateMutable();
            CGPathMoveToPoint(Path, NULL, 0, 0);
            CGPathAddLineToPoint(Path, NULL, 0, height+14);
            [shapelayer setPath:Path];
            CGPathRelease(Path);
            [rightView.layer addSublayer:shapelayer];
            UIButton *ButtonM = [UIButton buttonWithType:UIButtonTypeCustom];
            ButtonM.tag = 10+i;
            [ButtonM setImage:[UIImage imageNamed:self.buttonImageArr[i]] forState:UIControlStateNormal];
            [ButtonM.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            [ButtonM addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
            [BC addSubview:ButtonM];
            [ButtonM mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(BC.mas_top).offset(6);
                make.centerX.mas_equalTo(BC);
                make.width.mas_equalTo(44);
                make.height.mas_equalTo(44);
            }];
            ButtonM.layer.cornerRadius = 22;
            ButtonM.layer.masksToBounds = YES;
            UILabel *desL = [[UILabel alloc]init];
            desL.tag = 50+i;
            desL.text = des(i);
            desL.textAlignment = NSTextAlignmentCenter;
            desL.textColor = labelColor(i);
            desL.font = [UIFont systemFontOfSize:15];
            [BC addSubview:desL];
            [desL mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(BC.mas_top).offset(50);
                make.centerX.mas_equalTo(BC);
                make.height.mas_equalTo(30);
                make.width.mas_equalTo(width);
            }];
        }
    }
    return _buttonBackView;
}

#pragma 初始当前ImageArr
-(NSArray *)buttonImageArr
{
    if (_buttonImageArr==nil) {
        _buttonImageArr = @[@"1-3",@"1-7",@"1-4",@"1-5",@"1-2"];
    }
    return _buttonImageArr;
}
#pragma mark--返回当前文本
NSString *des(NSInteger num){
    return @[@"充/提款",@"会员中心",@"优惠活动",@"试玩",@"在线客服"][num];
}
#pragma mark--返回当前颜色对应
UIColor *labelColor(NSInteger num){
    
    return @[JesseColor(237, 185, 63),JesseColor(0, 156, 136),JesseColor(83, 182, 83),JesseColor(230,46,37),JesseColor(0, 122, 255)][num];
}

@end

