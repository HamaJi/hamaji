//
//  KFCPActivityController.h
//  Bet365
//
//  Created by jesse on 2017/7/4.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface KFCPActivityController : Bet365ViewController

-(void)loadWebBy:(NSString *)requestUrl;

-(void)loadingCode:(NSString *)code
              type:(NSString *)type
              kind:(NSString *)kind emu:(NSInteger)emu;
-(void)loadWebUrl:(NSString *)url dismissView:(void(^)())disBlock;/**销毁当前view**/
-(void)loadhongBaoUrl:(NSString *)url;
-(void)loadWebHtmlBy:(NSString *)url;

-(void)loadWebOpenUrlBySafari:(NSString *)url forParameter:(NSString *)joinString;/**浏览器打开**/
@property(nonatomic,strong)WKWebView *web;
@property(nonatomic,strong)UIWebView *specialWeb;
@property(nonatomic,copy)NSString *safari_url;//浏览器域名
@end
