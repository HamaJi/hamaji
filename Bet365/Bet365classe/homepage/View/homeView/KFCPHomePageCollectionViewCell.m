//
//  KFCPHomePageCollectionViewCell.m
//  Bet365
//
//  Created by jesse on 2017/6/29.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "KFCPHomePageCollectionViewCell.h"
#define BackColor JesseColor(236, 236, 236)

@interface KFCPHomePageCollectionViewCell()
@property(nonatomic,strong)UIImageView *LotteryLog;
@property(nonatomic,strong)UILabel *LotteryName;
@property(nonatomic,strong)UILabel *date;
@property(nonatomic,strong)UILabel *logLabel;
@end
@implementation KFCPHomePageCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self creatUI];
    }
    return self;
}
#pragma mark--构建布局
-(void)creatUI
{
    [self LotteryLog];
    [self LotteryName];
    [self date];
    [self logLabel];
}
#pragma mark--更新当前数据
-(void)updateCurrenDes:(NSString *)lotteryName AndDate:(NSString *)date AndLogImageName:(NSString *)lotteryLogName AndKindLog:(NSString *)kindLog
{
    self.LotteryName.text = lotteryName;
    self.date.text = date;
    [self.logLabel setHidden:NO];
    if(kindLog.length==0)[self.logLabel setHidden:YES];
    self.logLabel.text=kindLog;
    [self.LotteryLog sd_setImageWithURL:[NSURL URLWithString:lotteryLogName] placeholderImage:[UIImage imageNamed:@"lunbo"]];
}
-(void)upMoreImage:(UIImage *)image{
    self.LotteryLog.image = image;
}
#pragma mark--更新当前时间
-(void)updateDate:(NSString *)date
{
    self.date.text = date;
}
#pragma mark--创建LotteryName
-(UILabel *)LotteryName
{
    if (_LotteryName==nil) {
        _LotteryName = [[UILabel alloc]init];
        _LotteryName.textAlignment = NSTextAlignmentLeft;
        _LotteryName.font = [UIFont systemFontOfSize:17];
        _LotteryName.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:_LotteryName];
        [_LotteryName mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.LotteryLog.mas_right).offset(5);
            make.top.mas_equalTo(self.contentView.mas_top).offset(20);
            make.height.mas_equalTo(23);
        }];
    }
    return _LotteryName;
}
#pragma mark--时间
-(UILabel *)date
{
    if (_date==nil) {
        _date = [[UILabel alloc]init];
        _date.textAlignment = NSTextAlignmentCenter;
        _date.font =(IS_IPHONE_5s)?[UIFont systemFontOfSize:12]:[UIFont systemFontOfSize:14];
        _date.adjustsFontSizeToFitWidth = YES;
        _date.textColor = [UIColor darkGrayColor];
        [self.contentView addSubview:_date];
        [_date mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.LotteryName);
            make.top.mas_equalTo(self.LotteryName.mas_bottom).offset(0);
            make.height.mas_equalTo(20);
        }];
    }
    return _date;
}
#pragma mark--lotteryLog
-(UIImageView *)LotteryLog
{
    if (_LotteryLog==nil) {
        _LotteryLog = [[UIImageView alloc]init];
        _LotteryLog.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_LotteryLog];
        [_LotteryLog mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.contentView);
            make.left.mas_equalTo(self.contentView.mas_left).offset(12);
            make.width.mas_offset(45);
            make.height.mas_offset(45);
        }];
    }
    return _LotteryLog;
}
#pragma mark--labelLog
-(UILabel *)logLabel
{
    if (_logLabel==nil) {
        UIView *rightIndrow = [[UIView alloc]init];
        rightIndrow.backgroundColor = BackColor;
        [self.contentView addSubview:rightIndrow];
        [rightIndrow mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.contentView);
            make.width.mas_equalTo(1);
            make.height.mas_equalTo(56);
            make.right.mas_equalTo(self.contentView.mas_right).offset(0);
        }];
        UIView *bottomIndrow = [[UIView alloc]init];
        bottomIndrow.backgroundColor = BackColor;
        [self.contentView addSubview:bottomIndrow];
        [bottomIndrow mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(0);
            make.width.mas_equalTo(WIDTH/2);
            make.height.mas_equalTo(1);
        }];

        _logLabel = [[UILabel alloc]init];
        _logLabel.textColor = [UIColor whiteColor];
        _logLabel.backgroundColor = [UIColor orangeColor];
        _logLabel.font = [UIFont systemFontOfSize:14];
        _logLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_logLabel];
        [_logLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView.mas_right).offset(-5);
            make.top.mas_equalTo(self.contentView.mas_top).offset(5);
            make.width.mas_equalTo(20);
            make.height.mas_equalTo(20);
        }];
        _logLabel.layer.cornerRadius = 10;
        _logLabel.layer.masksToBounds = YES;
    }
    return _logLabel;
}
@end
