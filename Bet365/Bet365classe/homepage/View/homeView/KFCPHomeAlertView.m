//
//  KFCPHomeAlertView.m
//  Bet365
//
//  Created by jesse on 2017/7/4.
//  Copyright © 2017年 jesse. All rights reserved.
#import "KFCPHomeAlertView.h"
#import <objc/message.h>
#define titleHeight 50
#define desHeight 120
#define sureHeight 42
#define ALERTWIDTH WIDTH-60
#define IMAGEHEIGHT 150
static void *layerKey = &layerKey;
@interface jesseNewAlertObjectButton()
@property(copy,nonatomic)void(^handlerBlock)(jesseNewAlertObjectButton *alertButton);

@end
@implementation jesseNewAlertObjectButton
+(instancetype)initTitle:(NSString *)Maintitle AndHandler:(void (^)(jesseNewAlertObjectButton *))handler{
    return [[self alloc] init:Maintitle AndHandler:handler];
}
-(instancetype)init:(NSString *)title AndHandler:(void(^)(jesseNewAlertObjectButton *handle))handler{
    if (self=[super init]) {
        self.handlerBlock = handler;
        self.titleLabel.font = [UIFont systemFontOfSize:16];
        [self setTitle:title forState:UIControlStateNormal];
        [self setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        [self setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [self.titleLabel setAdjustsFontSizeToFitWidth:YES];
        [self addTarget:self action:@selector(handlerClick) forControlEvents:UIControlEventTouchUpInside];
        self.horizontalLayer = [CALayer layer];
        self.horizontalLayer.backgroundColor = [UIColor greenColor].CGColor;
        [self.layer addSublayer:self.horizontalLayer];
        self.verticalLayer = [CALayer layer];
        self.verticalLayer.backgroundColor = [UIColor greenColor].CGColor;
        [self.layer addSublayer:self.verticalLayer];

    }
    return self;
}
-(void)layoutSubviews{
    [super layoutSubviews];
    CGFloat lineWidth = (self.lineWidth>0)?self.lineWidth:1/[UIScreen mainScreen].scale;
    self.horizontalLayer.frame = CGRectMake(0, 0, self.frame.size.width,lineWidth);
    self.verticalLayer.frame = CGRectMake(0, 0, lineWidth, self.frame.size.height);
}
/**触发信息**/
-(void)handlerClick{
    self.handlerBlock(self);
}

/**修改颜色**/
-(void)setLineColor:(UIColor *)lineColor{
    _lineColor = lineColor;
    self.verticalLayer.backgroundColor = lineColor.CGColor;
    self.horizontalLayer.backgroundColor = lineColor.CGColor;
}
@end
@interface KFCPHomeAlertView()
{
    CGSize _contentSize;
    CGFloat _paddingLeft,_paddingRight,_paddingBottom;/**左右间距一致**/
    CGFloat _spacing;
}
@property(nonatomic,strong)UILabel *deslable;
@property(nonatomic,strong)UIView *showAletView;
@property(nonatomic,strong)UIView *alertImageView;
@property(strong,nonatomic)UIView *updateVersionBack;/**添加当前版本更新模版**/
@property(nonatomic,copy)NSString *des;
@property(nonatomic,copy)NSString *titleN;
@property(strong,nonatomic)UIColor *titleColor;
@property(nonatomic,strong)UIImageView *markImage;
@property(copy,nonatomic)NSString *imageN;
@property(strong,nonatomic)NSArray *updateSource;/**Property:更新资源**/
@property(assign,nonatomic)BOOL isBetAlert;/**下注提示**/
@property(assign,nonatomic)BOOL isImageAlert;
@property(assign,nonatomic)BOOL isNextAlert;/**下次打开是否弹出**/
@property(assign,nonatomic)BOOL isUpdateLager;/**是否为更新模块**/
@property CGFloat verButtonHeight;/**button模块高度**/
/**<##>**/
@end
static void *jesseNewAlertKey = &jesseNewAlertKey;
@implementation KFCPHomeAlertView
/***************************初始化NewObject****************************/
-(instancetype)initNewAlertMessageWaring:(NSString *)title anMessage:(NSString *)message AndContentSize:(CGFloat)contentSize{
    if (self==[super init]) {
        self.verButtonHeight = 49;
        if(contentSize>0)_contentSize.width=contentSize; else _contentSize.width=200;
        _paddingLeft=15,_paddingRight=15,_spacing = 10;
        if (title.length) {
            UILabel *titleLabel = [[UILabel alloc]init];
            titleLabel.text = title;
            titleLabel.numberOfLines = 0;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.font  = [UIFont systemFontOfSize:22];
            [self addSubview:titleLabel];
            titleLabel.textColor = [UIColor orangeColor];
            [titleLabel sizeToFit];
            titleLabel.ycz_y = _spacing;
            titleLabel.ycz_centerX = _contentSize.width/2;
            _contentSize.height = titleLabel.bottom;
        }
        if (message.length) {
            UIScrollView *scrollview = [[UIScrollView alloc]init];
            scrollview.bounces = NO;
            [self addSubview:scrollview];
            UILabel *messageL = [[UILabel alloc]init];
            messageL.text = message;
            messageL.numberOfLines = 0;
            messageL.font = [UIFont systemFontOfSize:16];
            messageL.textColor = [UIColor grayColor];
            [scrollview addSubview:messageL];
            if ([message containsString:@"location"]||[message containsString:@"href"]||[message containsString:@"onclick"]) {
                messageL.userInteractionEnabled = YES;
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onclick)];
                [messageL addGestureRecognizer:tap];
            }
            NSString *htmlString = [NSString stringWithFormat:@"<head><style>body{font-family:arial,Helvetica;font-size:16;color:gray;word-wrap:break-word;}</style></head>"];
            NSMutableAttributedString *attr = [[NSMutableAttributedString alloc]initWithData:[[NSString stringWithFormat:@"%@%@",htmlString,message] dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType,NSDocumentTypeDocumentAttribute:NSPlainTextDocumentType} documentAttributes:nil error:nil];
            messageL.attributedText = attr;
            NSArray *match = [message regularExpressionWithPattern:URL_PATTERN];
            if (match.count > 0) {
                [messageL sizeToFit];
            }else{
                messageL.size = [messageL sizeThatFits:CGSizeMake(_contentSize.width-2*_paddingLeft, MAXFLOAT)];
            }
            if ((messageL.size.height+_contentSize.height)>HEIGHT-180) {
                messageL.ycz_y = 0;
                messageL.ycz_x = _paddingLeft;
                scrollview.ycz_y = _contentSize.height;
                scrollview.ycz_centerX = self.center.x;
                scrollview.size = CGSizeMake(_contentSize.width, HEIGHT-180);
                scrollview.contentSize = CGSizeMake(messageL.size.width + _paddingRight + _paddingLeft,messageL.size.height);
            }else{
                messageL.ycz_y = 0;
                messageL.ycz_x = _paddingLeft;
                scrollview.ycz_y = _contentSize.height;
                scrollview.ycz_centerX =self.center.x;
                scrollview.size = CGSizeMake(_contentSize.width, messageL.size.height);
                scrollview.contentSize = CGSizeMake(messageL.size.width + _paddingRight + _paddingLeft,messageL.size.height);
            }
            _contentSize.height = scrollview.bottom;
        }
        if (!title.length&&!message.length) {
            self.size = CGSizeZero;
        }else{
            self.size = _contentSize;
        }
        self.layer.cornerRadius = 10;
        self.layer.masksToBounds = YES;
        self.backgroundColor = [UIColor whiteColor];
        
    }
    return self;
}
/**处理descriptionButton**/
-(void)addActionButton:(jesseNewAlertObjectButton *)leftButton right:(jesseNewAlertObjectButton *)rightButton{
    [self clearAllSubButton:self.subviews];
    objc_setAssociatedObject(self, jesseNewAlertKey, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    leftButton.size = CGSizeMake(_contentSize.width/2, self.verButtonHeight);
    leftButton.ycz_y = _contentSize.height;
    leftButton.ycz_x = leftButton.edges.left;
    rightButton.frame = leftButton.frame;
    rightButton.ycz_x = leftButton.right;
    rightButton.verticalLayer.hidden = NO;
    [self addSubview:leftButton];
    [self addSubview:rightButton];
    self.size = CGSizeMake(_contentSize.width, leftButton.bottom);
}
/**clearButton**/
-(void)clearAllSubButton:(NSArray *)subviews{
    for (int i = 0; i<subviews.count; i++) {
        if ([subviews[i] isKindOfClass:[jesseNewAlertObjectButton class]]) {
            [((jesseNewAlertObjectButton *)subviews[i]) removeFromSuperview];
        }
    }
}






























/*******************************************************/
#pragma mark--内容涵盖
-(void)updateDesAlertView:(NSString *)des And:(NSString *)titleN 
{
    self.isImageAlert = NO;
    self.isBetAlert = NO;
    self.isUpdateLager = NO;
    self.des = des;
    self.titleN = titleN;
    [self doAlertTask];
}
/**下注内容覆盖**/
-(void)updateDesAlertView:(NSString *)des And:(NSString *)titleN AndIsBet:(BOOL)betAlert{
    self.isImageAlert = NO;
    self.isUpdateLager = NO;
    self.isBetAlert = betAlert;
    self.des = titleN;
    self.titleN = des;
    [self doAlertTask];
}
/**当前图片弹框**/
-(void)updateImageTitle:(NSString *)imageName AndTitle:(NSString *)title titleColor:(UIColor *)titleC AndDes:(NSString *)des AndDesColor:(UIColor *)ColorD NextAlert:(BOOL)needNextAlert{
    self.isImageAlert = YES;
    self.isUpdateLager = NO;
    self.titleN = title;
    self.titleColor = titleC;
    self.des = des;
    self.imageN = imageName;
    self.isNextAlert = needNextAlert;
    [self doAlertTask];
}
/**更新模版修改**/
-(void)showUpdateMessageType:(NSString *)bundleVersion updateSource:(NSArray *)source andSourceTextolor:(UIColor *)sourceColor ShowImageName:(NSString *)imageName{
    self.titleN = bundleVersion;
    self.updateSource = source;
    self.titleColor = sourceColor;
    self.imageN = imageName;
    self.isUpdateLager = YES;
    [self doAlertTask];
}
//方法触发
-(void)sure:(UIButton *)button
{
    [UIView animateWithDuration:0.3 animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    }];
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:1/0.8 options:0 animations:^{
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        self.showAletView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished) {
        [self.showAletView removeFromSuperview];
        self.showAletView=nil;
        [self removeFromSuperview];
        self.remove();
    }];
}
//bet确定
-(void)betSure:(UIButton *)button{
    [UIView animateWithDuration:0.3 animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    }];
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:1/0.8 options:0 animations:^{
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        if (self.isImageAlert) {
            self.alertImageView.transform =CGAffineTransformMakeScale(0.01, 0.01);
        }else{
            self.showAletView.transform = CGAffineTransformMakeScale(0.01, 0.01);
        }
    } completion:^(BOOL finished) {
        if (self.isImageAlert) {
              self.alertImageView.transform =CGAffineTransformMakeScale(0, 0);
        }else{
             self.showAletView.transform = CGAffineTransformMakeScale(0, 0);
        }
        [self.delegate sureTraceBetting];
    }];

}
-(void)cancle{
    [UIView animateWithDuration:0.3 animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    }];
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:1/0.8 options:0 animations:^{
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        if (self.isImageAlert) {
            self.alertImageView.transform = CGAffineTransformMakeScale(0.01, 0.01);
        }else{
             self.showAletView.transform = CGAffineTransformMakeScale(0.01, 0.01);
        }
    } completion:^(BOOL finished) {
        if (self.isImageAlert) {
            self.alertImageView.transform = CGAffineTransformMakeScale(0, 0);
        }else{
            self.showAletView.transform = CGAffineTransformMakeScale(0, 0);
        }

        [self.delegate deleteBackview];
    }];
}
-(void)nextNoAlertOpen{
    [UIView animateWithDuration:0.3 animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    }];
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:1/0.8 options:0 animations:^{
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
      self.alertImageView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished) {
        [self.delegate nextNoOpenAlertDelegate];
    }];
}
/**手势判断跳转优惠**/
-(void)onclick{
    self.activit();
}
#pragma mark--初始化当前对应弹框
-(UIView *)showAletView
{
    if (_showAletView==nil) {
        CGRect boundSize = [self.des boundingRectWithSize:CGSizeMake(WIDTH-40-20, 500) options:NSStringDrawingTruncatesLastVisibleLine |
                            NSStringDrawingUsesLineFragmentOrigin |
                            NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17]} context:nil];
        _showAletView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH-60, titleHeight+sureHeight+boundSize.size.height+20)];
        _showAletView.center = self.center;
        _showAletView.backgroundColor = [UIColor whiteColor];
        _showAletView.transform = CGAffineTransformMakeScale(0, 0);
        [[UIApplication sharedApplication].keyWindow addSubview:_showAletView];
        _showAletView.layer.cornerRadius = 3;
        _showAletView.layer.masksToBounds = YES;
        UIView *titleFatherV = [jesseGuideUIConfig getCurrnViewUiByRect:CGRectMake(0, 0, ALERTWIDTH, titleHeight) AndBackColor:[UIColor whiteColor] AndUseMasonry:NO];
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, ALERTWIDTH, titleHeight-15)];
        titleLabel.text = self.titleN;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.font = [UIFont systemFontOfSize:19];
        [titleFatherV addSubview:titleLabel];
        [_showAletView addSubview:titleFatherV];
        self.deslable = [[UILabel alloc]initWithFrame:CGRectMake(10, titleHeight, ALERTWIDTH-20, boundSize.size.height)];
        self.deslable.textColor = [UIColor darkGrayColor];
        self.deslable.adjustsFontSizeToFitWidth = NO;
        self.deslable.textAlignment = NSTextAlignmentCenter;
        self.deslable.numberOfLines=0;
        self.deslable.text = self.des;
        self.deslable.font = [UIFont systemFontOfSize:17];
        [_showAletView addSubview:self.deslable];
        UIView *indrow = [[UIView alloc]initWithFrame:CGRectMake(0, titleHeight+boundSize.size.height+20, ALERTWIDTH, 1)];
        indrow.backgroundColor = JesseColor(230, 230, 230);
        [_showAletView addSubview:indrow];
        if (self.isBetAlert) {
            UIView *BetBackView = [jesseGuideUIConfig getCurrnViewUiByRect:CGRectMake(0, titleHeight+boundSize.size.height+21, ALERTWIDTH, sureHeight-3) AndBackColor:[UIColor whiteColor] AndUseMasonry:NO];
            CGFloat ButtonWidth = (WIDTH-60)/2-0.5;
            CGFloat ButtonHeight = sureHeight-3;
            CGFloat ButtonY = 0;
            for (int i=0; i<2; i++) {
                CGFloat ButtonX = ButtonWidth*i+1*i;
                [BetBackView addSubview:[self alertButton:YES And:10+i AndFrame:CGRectMake(ButtonX, ButtonY, ButtonWidth, ButtonHeight) titleColor:JesseColor(34, 177, 38)]];
                if (i==0) {
                    [BetBackView addSubview:[jesseGuideUIConfig getCurrnViewUiByRect:CGRectMake(ButtonWidth, 0, 1, sureHeight-3) AndBackColor:JesseColor(230, 230, 230) AndUseMasonry:NO]];
                }
            }
            [_showAletView addSubview:BetBackView];
        }else{
            [_showAletView addSubview:[self alertButton:NO And:0 AndFrame:CGRectMake(0, titleHeight+boundSize.size.height+21, ALERTWIDTH, sureHeight-3) titleColor:JesseColor(34, 177, 38)]];
        }

    }
    return _showAletView;
}
-(UIView *)alertImageView{
    if (_alertImageView==nil) {
        CGRect boundSize = [self.des boundingRectWithSize:CGSizeMake(WIDTH-60-20, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine |
                            NSStringDrawingUsesLineFragmentOrigin |
                            NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil];
        CGFloat nextAlertValue = (self.isNextAlert)?sureHeight:0;
        _alertImageView = [[UIView alloc]initWithFrame:CGRectMake(0,0 , WIDTH-60, boundSize.size.height+15+IMAGEHEIGHT+titleHeight+sureHeight+nextAlertValue)];
        _alertImageView.backgroundColor = [UIColor whiteColor];
        _alertImageView.center = self.center;
        [self addSubview:_alertImageView];
        self.markImage = [jesseGuideUIConfig getCurrenImageUiRect:CGRectMake(0, 15, IMAGEHEIGHT, IMAGEHEIGHT) AndImage:[UIImage imageNamed:self.imageN] AnduseMasonry:YES];
        [_alertImageView addSubview:self.markImage];
        [self.markImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(_alertImageView);
            make.top.mas_equalTo(_alertImageView.mas_top).offset(15);
            make.width.height.mas_equalTo(IMAGEHEIGHT);
        }];
        UIView *titleFatherV = [jesseGuideUIConfig getCurrnViewUiByRect:CGRectMake(0, IMAGEHEIGHT+15, ALERTWIDTH, titleHeight-10) AndBackColor:[UIColor whiteColor] AndUseMasonry:NO];
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, ALERTWIDTH, titleHeight-10)];
        titleLabel.text = self.titleN;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.textColor = self.titleColor;
        titleLabel.font = [UIFont systemFontOfSize:19];
        [titleFatherV addSubview:titleLabel];
        [_alertImageView addSubview:titleFatherV];
        self.deslable = [jesseGuideUIConfig getCurrenLableUiByRect:CGRectMake(10, IMAGEHEIGHT+15+titleHeight-10, WIDTH-60-20, boundSize.size.height) AndLabelTitle:self.des AndTitleColor:[UIColor blackColor] AndTextAlignment:NSTextAlignmentCenter AndIsAdWidth:NO AndBackColor:[UIColor clearColor] AndUseMasonry:NO];
        self.deslable.font = [UIFont systemFontOfSize:14];
        self.deslable.numberOfLines = 0;
        [_alertImageView addSubview:self.deslable];
        UIView *indrow = [[UIView alloc]initWithFrame:CGRectMake(0, titleHeight+boundSize.size.height+IMAGEHEIGHT+15, ALERTWIDTH, 1)];
        indrow.backgroundColor = JesseColor(230, 230, 230);
        [_alertImageView addSubview:indrow];
        UIView *BetBackView = [jesseGuideUIConfig getCurrnViewUiByRect:CGRectMake(0, titleHeight+boundSize.size.height+16+IMAGEHEIGHT, ALERTWIDTH, sureHeight-3) AndBackColor:[UIColor whiteColor] AndUseMasonry:NO];
        CGFloat ButtonWidth = (WIDTH-60)/2-0.5;
        CGFloat ButtonHeight = sureHeight-3;
        CGFloat ButtonY = 0;
        for (int i=0; i<2; i++) {
            CGFloat ButtonX = ButtonWidth*i+1*i;
            [BetBackView addSubview:[self alertButton:YES And:10+i AndFrame:CGRectMake(ButtonX, ButtonY, ButtonWidth, ButtonHeight) titleColor:self.titleColor]];
            if (i==0) {
                [BetBackView addSubview:[jesseGuideUIConfig getCurrnViewUiByRect:CGRectMake(ButtonWidth, 0, 1, sureHeight-3) AndBackColor:JesseColor(230, 230, 230) AndUseMasonry:NO]];
            }
        }
        [_alertImageView addSubview:BetBackView];
        if (self.isNextAlert) {
            UIView *indrowT = [jesseGuideUIConfig getCurrnViewUiByRect:CGRectMake(0, titleHeight+boundSize.size.height+16+IMAGEHEIGHT+sureHeight, ALERTWIDTH, 1) AndBackColor:JesseColor(230, 230, 230) AndUseMasonry:NO];
            [_alertImageView addSubview:indrowT];
            UIButton *nextAlert = [jesseGuideUIConfig getCurrenButtonUiByRect:CGRectMake(0, titleHeight+boundSize.size.height+16+IMAGEHEIGHT+sureHeight+1, ALERTWIDTH, sureHeight-1) AndButtonTitle:@"下次不再弹出该提示" AndTitleColor:self.titleColor AndtextAlignment:NSTextAlignmentCenter And:@selector(nextNoAlertOpen) Andtarget:self AndlayerCorner:0 AndUseMasonry:NO];
            nextAlert.titleLabel.font = [UIFont systemFontOfSize:19];
            [_alertImageView addSubview:nextAlert];
        }


    }
    return _alertImageView;
}
-(UIView *)updateVersionBack{
    if (_updateVersionBack==nil) {
        _updateVersionBack = [[UIView alloc]init];
        CGFloat width = 312;
        CGFloat height = 190+32+(20*self.updateSource.count)+18+(50*2);
        _updateVersionBack = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
        _updateVersionBack.backgroundColor = [UIColor whiteColor];
        _updateVersionBack.center = self.center;
        [self addSubview:_updateVersionBack];
        _updateVersionBack.layer.cornerRadius = 12;
        _updateVersionBack.layer.masksToBounds = YES;
        UIImageView *markImage = [[UIImageView alloc]init];
        [markImage setImage:[UIImage imageNamed:self.imageN]];
        [_updateVersionBack addSubview:markImage];
        [markImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_updateVersionBack.mas_top).offset(20);
            make.centerX.mas_equalTo(_updateVersionBack);
            make.width.mas_equalTo(170);
            make.height.mas_equalTo(150);
        }];
        UILabel *updateTitleLabel = [jesseGuideUIConfig getCurrenLableUiByRect:CGRectMake(0, 190, 312, 30) AndLabelTitle:self.titleN AndTitleColor:self.titleColor AndTextAlignment:NSTextAlignmentCenter AndIsAdWidth:YES AndBackColor:[UIColor clearColor] AndUseMasonry:NO];
        updateTitleLabel.font = [UIFont systemFontOfSize:19];
        [_updateVersionBack addSubview:updateTitleLabel];
        UIView *updateB = [jesseGuideUIConfig getCurrnViewUiByRect:CGRectMake(0, 222, 312, 20*self.updateSource.count) AndBackColor:[UIColor clearColor] AndUseMasonry:NO];
        [_updateVersionBack addSubview:updateB];
        CGFloat x = 27;
        CGFloat swidth = 312-27;
        CGFloat sheight = 20;
        for (int i = 0; i<self.updateSource.count; i++) {
            CGFloat y = sheight*i;
            UILabel *updateSub = [jesseGuideUIConfig getCurrenLableUiByRect:CGRectMake(x, y, swidth, sheight) AndLabelTitle:self.updateSource[i] AndTitleColor:JesseColor(94, 94, 94) AndTextAlignment:NSTextAlignmentLeft AndIsAdWidth:YES AndBackColor:[UIColor clearColor] AndUseMasonry:NO];
            updateSub.font = [UIFont systemFontOfSize:16];
            [updateB addSubview:updateSub];
        }
        UIView *selectUpdate = [jesseGuideUIConfig getCurrnViewUiByRect:CGRectMake(0, 240+20*self.updateSource.count, 312, 100) AndBackColor:[UIColor whiteColor] AndUseMasonry:NO];
        [_updateVersionBack addSubview:selectUpdate];
        CGFloat selectX = 0;
        CGFloat selectW = 312;
        CGFloat selectH = 49;
        for (int j = 0; j<2; j++) {
            CGFloat selectY = 1+selectH*j+1*j;
            UIView *indrowUpdate = [jesseGuideUIConfig getCurrnViewUiByRect:CGRectMake(0, j*1+selectH*j, 312, 1) AndBackColor:JesseColor(217, 222, 228) AndUseMasonry:NO];
            [selectUpdate addSubview:indrowUpdate];
            UIButton *selectB = [jesseGuideUIConfig getCurrenButtonUiByRect:CGRectMake(selectX, selectY,selectW, selectH) AndButtonTitle:@[@"现在更新",@"下次再说"][j] AndTitleColor:self.titleColor AndtextAlignment:NSTextAlignmentCenter And:@selector(updateSelect:) Andtarget:self AndlayerCorner:0 AndUseMasonry:NO];
            selectB.titleLabel.font = [UIFont systemFontOfSize:17];
            selectB.tag = 1000+j;
            [selectUpdate addSubview:selectB];
        }
        
    }
    return _updateVersionBack;
}
-(UIButton *)alertButton:(BOOL)isBet And:(NSInteger)ButtonTag AndFrame:(CGRect)frame titleColor:(UIColor *)color{
    UIButton *sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sureButton.frame = frame;
    [sureButton setTitle:(isBet)?@[@"取消",@"确定"][ButtonTag-10]:@"确定" forState:UIControlStateNormal];
    [sureButton setTitleColor:color forState:UIControlStateNormal];
    [sureButton.titleLabel setFont:[UIFont systemFontOfSize:19]];
    if (!isBet) {
      [sureButton addTarget:self action:@selector(sure:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        NSString *methond = @[@"cancle",@"betSure:"][ButtonTag-10];
        [sureButton addTarget:self action:NSSelectorFromString(methond) forControlEvents:UIControlEventTouchUpInside];
    }
    return sureButton;
}
/**做加载任务**/
-(void)doAlertTask{
    [UIView animateWithDuration:0.3 animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
    }];
    if (self.isUpdateLager) {
        [self updateVersionBack];
        self.updateVersionBack.transform = CGAffineTransformMakeScale(0.01, 0.01);
    }else{
    (self.isImageAlert)?[self alertImageView]:[self showAletView];
        if (self.isImageAlert) {
            self.alertImageView.transform =CGAffineTransformMakeScale(0.01, 0.01);
        }else{
            self.showAletView.transform = CGAffineTransformMakeScale(0.01, 0.01);
        }
    }
    
    [UIView animateWithDuration:0.3 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:1/0.8 options:0 animations:^{
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        if (self.isUpdateLager) {
            self.updateVersionBack.transform = CGAffineTransformIdentity;
        }else{
        if (self.isImageAlert) {
            self.alertImageView.transform = CGAffineTransformIdentity;
        }else{
           self.showAletView.transform = CGAffineTransformIdentity;
        }
        }
    } completion:^(BOOL finished) {
        
    }];
}
-(void)updateSelect:(UIButton *)selectB{
    NSInteger tag = selectB.tag-1000;
    [UIView animateWithDuration:0.3 animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    }];
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:1/0.8 options:0 animations:^{
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        if (self.isUpdateLager) {
            self.updateVersionBack.transform = CGAffineTransformMakeScale(0.01, 0.01);
        }else{
            if (self.isImageAlert) {
                self.alertImageView.transform = CGAffineTransformIdentity;
            }else{
                self.showAletView.transform = CGAffineTransformIdentity;
            }
        }
    } completion:^(BOOL finished) {
        (tag==0)?[self.updateDataSource nowUpdate]:[self.updateDataSource nextUpdate];
    }];
}
@end
