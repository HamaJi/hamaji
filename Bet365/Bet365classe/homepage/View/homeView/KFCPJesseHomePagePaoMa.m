//
//  KFCPJesseHomePagePaoMa.m
//  Bet365
//
//  Created by jesse on 2017/6/30.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "KFCPJesseHomePagePaoMa.h"
#import "KFCPGameWinNoticeModel.h"
#define START_TAG  100
@interface KFCPJesseHomePagePaoMa()
{
    
    UIScrollView *_abstractScrollview;
    
    CGPoint _currentOffset;
    
    NSInteger _startIndex;
    
    NSArray *_itemsArray;
    
    UIFont *_textFont;
    
    UIColor *_textColor;
    
    CGFloat _intervalTime;
    
    UIImage *_backgroundImage;
    
    JessePaomaViewOrientationStyle _style;
    
    CGFloat _height;
    
    CGFloat _width;
    
    NSTimer *_timer;
}

-(void)makeUI;

- (void)startTimer;

-(void)releaseTimer;

@end
@implementation KFCPJesseHomePagePaoMa

- (instancetype)initWithFrame:(CGRect)frame backgroundImage:(UIImage *)image textFont:(UIFont *)font intervalTime:(NSInteger)intervalTime textColor:(UIColor *)color itemsArray:(NSArray *)itemsArray DLPaomaViewOrientationStyle:(JessePaomaViewOrientationStyle)style
{
    if (self = [super initWithFrame:frame]) {
        
        _itemsArray = itemsArray;
        
        _backgroundImage = image;
        
        _textFont = font;
        
        _textColor = color;
        
        _intervalTime = intervalTime;
        
        _style = style;
        
        _currentOffset = CGPointMake(0,0);
        
        _width = self.frame.size.width;
        
        _height = self.frame.size.height;
        
       // assert([itemsArray count]!=0);
        
        [self makeUI];
        
        [self startTimer];
        
    }
    return self;
}
-(void)updateCurrenAttery:(NSArray *)atterArr
{
    if (atterArr.count>_itemsArray.count) {
        [_timer setFireDate:[NSDate distantPast]];
        [_timer invalidate];
        _timer = nil;
        [self makeUI];
        [self startTimer];
    }else{
        _itemsArray = atterArr;
        for (NSInteger i = 0; i<atterArr.count; i++) {
            UIView *nowLabel = [_abstractScrollview viewWithTag:START_TAG+i];
            [nowLabel.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                UILabel *sub = obj;
                sub.text = destext(_itemsArray[i],idx);
                sub.textColor = desColor(idx);
            }];
            if (i==atterArr.count-1) {
                NSInteger remaring = ceil(_height/20);
                for (NSInteger d = 0; d<remaring+1;d++) {
                    UIView *nowLabel = [_abstractScrollview viewWithTag:START_TAG+i+1+(d+1)];
                    [nowLabel.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        UILabel *sub = obj;
                        sub.text = destext(_itemsArray[i],idx);
                        sub.textColor = desColor(idx);
                    }];
  
                }
            }
        }
    }
}
-(void)makeUI
{
    _startIndex = 0;
    [_abstractScrollview removeFromSuperview];
    _abstractScrollview=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0,_width,_height)];
    _abstractScrollview.backgroundColor = [UIColor clearColor];
    [self addSubview:_abstractScrollview];
    NSInteger remaring = ceil(_height/20);
    [_abstractScrollview setContentSize:CGSizeMake(_width, ([_itemsArray count]+1+remaring)*20)];
        
        for (int i=0; i<[_itemsArray count]; i++)
        {
            UIView *subBack = [[UIView alloc]initWithFrame:CGRectMake(0, 20*i, _width, 20)];
            subBack.tag = START_TAG+i;
            [_abstractScrollview addSubview:subBack];
            for (NSInteger j = 0; j<3; j++) {
                UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(j*(WIDTH/3), 0 , WIDTH/3, 20)];
                label.textAlignment = NSTextAlignmentLeft;
                label.font = [UIFont systemFontOfSize:12];
                label.adjustsFontSizeToFitWidth = YES;
                label.text = destext(_itemsArray[i], i);
                label.textColor = desColor(i);
                [subBack addSubview:label];
            }
            if (i == [_itemsArray count]-1) {
                UIView *subBack = [[UIView alloc]initWithFrame:CGRectMake(0, 20*(i+1), _width, 20)];
                subBack.tag = START_TAG+i+1;
                [_abstractScrollview addSubview:subBack];
                for (NSInteger n = 0; n<3; n++) {
                    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(n*(WIDTH/3), 0 , WIDTH/3, 20)];
                    label.textAlignment = NSTextAlignmentLeft;
                    label.font = [UIFont systemFontOfSize:12];
                    label.adjustsFontSizeToFitWidth = YES;
                    label.text = destext(_itemsArray[0], n);
                   label.textColor = desColor(n);
                   [subBack addSubview:label];
               }
                NSInteger remaring = ceil(_height/20);
                for (NSInteger c = 0; c<remaring; c++) {
                    UIView *subBack = [[UIView alloc]initWithFrame:CGRectMake(0, 20*(i+1+(c+1)), _width, 20)];
                    subBack.tag = START_TAG+i+1+(c+1);
                    [_abstractScrollview addSubview:subBack];
                    for (NSInteger n = 0; n<3; n++) {
                        UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(n*(WIDTH/3), 0 , WIDTH/3, 20)];
                        label.textAlignment = NSTextAlignmentLeft;
                        label.font = [UIFont systemFontOfSize:12];
                        label.adjustsFontSizeToFitWidth = YES;
                        KFCPGameWinNoticeModel *model = [_itemsArray safeObjectAtIndex:i];
                        label.text = destext(model, n);
                        label.textColor = desColor(n);
                        [subBack addSubview:label];
                    }

                }
            }
        }
    _abstractScrollview.contentOffset=CGPointMake(0, 0);
}

#pragma mark--取出当前对应文本
NSString *destext(KFCPGameWinNoticeModel *winModel,NSInteger num){
    return (num==0)?[NSString stringWithFormat:@"🎁%@",winModel.name]:(num==1)?[NSString stringWithFormat:@"喜中¥%@元",winModel.winMoney]:[NSString stringWithFormat:@"购买%@",winModel.gameName];
}

#pragma mark--取出当前对应文本颜色
UIColor *desColor(NSInteger num){
    if (num == 1) {
        return [UIColor skinTextItemSelSubColor];
    }
    return [UIColor skinTextItemNorSubColor];
}

#pragma mark--修改附属文本
NSArray *atterStringArr(NSArray *gameWinModelArr){
    NSMutableArray *attersData = [[NSMutableArray alloc]init];
    [gameWinModelArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        KFCPGameWinNoticeModel *winModel = obj;
        NSAttributedString *atterS = atterSN(winModel);
        [attersData addObject:atterS];
    }];
    return attersData;
}
NSAttributedString *atterSN(KFCPGameWinNoticeModel *winModel){
    NSString *win = [NSString stringWithFormat:@"喜中¥%@元",winModel.winMoney];
    NSString *LotteryName = [NSString stringWithFormat:@"购买%@",winModel.gameName];
    NSString *fatherS = [NSString stringWithFormat:@"%@       %@       %@",winModel.name,win,LotteryName];
    NSMutableAttributedString *atter = [[NSMutableAttributedString alloc]initWithString:fatherS];
    NSDictionary *dicOne = @{NSFontAttributeName:[UIFont systemFontOfSize:12],NSForegroundColorAttributeName:[UIColor lightGrayColor]};
    [atter setAttributes:dicOne range:NSMakeRange(0, fatherS.length)];
    NSDictionary *dicTwo = @{NSFontAttributeName:[UIFont systemFontOfSize:12],NSForegroundColorAttributeName:[UIColor redColor]};
    [atter setAttributes:dicTwo range:[fatherS rangeOfString:LotteryName]];
    return atter;
}

- (void)startTimer
{
    if (_timer == nil) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:_intervalTime target:self selector:@selector(updateTitle) userInfo:nil repeats:YES];
    }
}

- (void)releaseTimer
{
    if ([_timer isValid]) {
        [_timer invalidate];
        _timer = nil;
    }
}

- (void)updateTitle
{
    CGPoint point = _abstractScrollview.contentOffset;
    UIView *lastLabel = (UIView *)[_abstractScrollview viewWithTag:(START_TAG + _itemsArray.count)];
        if (point.y>= lastLabel.frame.origin.y) {
            _startIndex = 0;
            _abstractScrollview.contentOffset = _currentOffset;
        }
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        [UIView setAnimationDuration:_intervalTime];
        [UIView setAnimationDelegate:self];
        
        UIView *currentView = (UIView *)[_abstractScrollview viewWithTag:(START_TAG  + _startIndex + 1)];
        CGPoint pointmiddle = CGPointMake(0, currentView.frame.origin.y);
        
        _startIndex ++;
        _abstractScrollview.contentOffset = pointmiddle;
        
        [UIView commitAnimations];
    
}

- (void)dealloc
{
    [self releaseTimer];
}

@end
