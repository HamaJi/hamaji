//
//  KFCPHomePageHeadOneCollectionReusableView.h
//  Bet365
//
//  Created by jesse on 2017/6/29.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

#define BBUrl @"/api/live/play?liveCode=%@&gameType=%@"
@protocol KFCPHomeHeadDelegate<NSObject>
-(void)topUpWithDraws;//充/提款
-(void)logRegister;/**注册**/
-(void)testPlay;//试玩
-(void)PreferActivt;//优惠活动
-(void)BetRecord;//投注记录跳转
-(void)NowChatCustomService;/**在线客服**/
-(void)loadSDLunBoUrl:(NSString *)url;/**进入当前轮播事件**/
@end
typedef void (^customServiceBlock)();;
@interface KFCPHomePageHeadOneCollectionReusableView : UICollectionReusableView
-(void)ReloadCurrenBy:(NSArray *)currenUrlImageArr And:(NSString *)Announcement isLogin:(BOOL)isLogin ItemArr:(NSArray *)datas height:(CGFloat)height;//传递创建
-(void)getCurrenButton:(void(^)(UIButton *))service;
@property(nonatomic,weak)id <KFCPHomeHeadDelegate> HomeHeadDelegate;
@property(copy,nonatomic)customServiceBlock customBlock;/**客服回调**/
//返回玩法对应KindType
LotteryType MovetcheckGameType(NSString *parameterNumber);
@end
