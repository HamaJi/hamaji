//
//  KFCPHomeAlertView.h
//  Bet365
//
//  Created by jesse on 2017/7/4.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface jesseNewAlertObjectButton:UIButton
@property(strong,nonatomic)UIColor *lineColor;
@property(nonatomic,assign)UIEdgeInsets edges;
@property(nonatomic,assign)CGFloat lineWidth;
@property(strong,nonatomic)CALayer *verticalLayer;/**垂直layer**/
@property(strong,nonatomic)CALayer *horizontalLayer;/**横向layer**/
+(instancetype)initTitle:(NSString *)Maintitle AndHandler:(void(^)(jesseNewAlertObjectButton *alertButton))handler;
@end
@protocol jesselagerAnimationDelegate<NSObject>
-(void)deleteBackview;
-(void)sureTraceBetting;
-(void)nextNoOpenAlertDelegate;
@end
@protocol updateDelegate<NSObject>
-(void)nowUpdate;/**现在更新**/
-(void)nextUpdate;/**下次更新**/
@end
typedef void (^removeBlock)();
typedef void (^pushActivit)();
@interface KFCPHomeAlertView : UIView
//赋值创建
-(void)updateDesAlertView:(NSString *)des And:(NSString *)titleN;
//赋值修改当前布局属性
-(void)updateDesAlertView:(NSString *)des And:(NSString *)titleN AndIsBet:(BOOL)betAlert;
-(void)updateImageTitle:(NSString *)imageName AndTitle:(NSString *)title titleColor:(UIColor *)titleC AndDes:(NSString *)des AndDesColor:(UIColor *)ColorD NextAlert:(BOOL)needNextAlert;
-(void)showUpdateMessageType:(NSString *)bundleVersion updateSource:(NSArray *)source andSourceTextolor:(UIColor *)sourceColor ShowImageName:(NSString *)imageName;
-(instancetype)initNewAlertMessageWaring:(NSString *)title anMessage:(NSString *)message AndContentSize:(CGFloat)contentSize;
-(void)addActionButton:(jesseNewAlertObjectButton *)leftButton right:(jesseNewAlertObjectButton *)rightButton;
@property(nonatomic,copy)removeBlock remove;
@property(nonatomic,copy)pushActivit activit;
@property(nonatomic,weak)id <jesselagerAnimationDelegate> delegate;
@property(weak,nonatomic)id <updateDelegate> updateDataSource;/**更新代理**/
@end
