//
//  KFCPJesseHomePagePaoMa.h
//  Bet365
//
//  Created by jesse on 2017/6/30.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef  NS_ENUM(NSInteger,JessePaomaViewOrientationStyle){
    JessePaomaViewHorizontalStyle,
    JessePaomaViewVerticalStyle,
};
@interface KFCPJesseHomePagePaoMa : UIView
- (instancetype)initWithFrame:(CGRect)frame backgroundImage:(UIImage *)image textFont:(UIFont *)font intervalTime:(NSInteger)intervalTime textColor:(UIColor *)color itemsArray:(NSArray *)itemsArray DLPaomaViewOrientationStyle:(JessePaomaViewOrientationStyle)style;
-(void)updateCurrenAttery:(NSArray *)atterArr;
@end
