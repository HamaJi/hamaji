//
//  KFCPFootLongDragonCollectionReusableView.m
//  Bet365
//
//  Created by jesse on 2017/7/24.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "KFCPFootLongDragonCollectionReusableView.h"
#define USCOLOR JesseColor(41, 197, 102)
typedef enum {
    totalDXType = 0,
    totalDSType = 1,
    totalTigerDragonType = 2
}totalType;
@interface KFCPFootLongDragonCollectionReusableView()
{
    UIView *_backView;//创建当前长龙backview
    NSArray *_gameData;//当前长龙数据
    NSArray *_lotteryWayData;/**路子数据**/
    UIView *_totalBack;//长龙路子Back
    UIView *_buttonBack;/**button**/
    UIScrollView *_subButtonBack;/**子背景Select**/
    UIScrollView *_lotteryWayBack;/**路子数据**/
    UIView *_wayBackTotal;//路子背景
    
}
@property(assign,nonatomic)SSCTotalMessageType messageType;/**SSC选择类型**/
@property(assign,nonatomic)totalType subTotal;/**总和类型**/
@property(strong,nonatomic)NSArray *lotteryWayTitleArr;/**路子数据**/
@property(strong,nonatomic)NSArray *totalDataArr;/**总和数据**/
@property(strong,nonatomic)NSMutableArray *setArr;/**坐标数组**/
@property(strong,nonatomic)UILabel *alertLabel;/**提示label**/
@property(strong,nonatomic)UIButton *currenLotteryButton;/**当前路子选择**/
@property(strong,nonatomic)UIButton *subButton;/**子button**/
@property(strong,nonatomic)NSMutableArray *alreadyShowArr;
@property(assign,nonatomic)BOOL isAlreadyShowWay;/**是否展示过路子**/
@property(assign,nonatomic)BOOL isAlreadyShowLongDragon;/**是否展示过长龙<##>**/
-(void)creatCurrenView;
@end
@implementation KFCPFootLongDragonCollectionReusableView
#pragma mark--初始化当前
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.lotteryWayTitleArr = @[@"路子",@"长龙"];
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        
    }
    return self;
}
-(NSMutableArray *)setArr{return _setArr?_setArr:(_setArr=@[].mutableCopy);};
-(NSMutableArray *)alreadyShowArr{return _alreadyShowArr?_alreadyShowArr:(_alreadyShowArr=@[].mutableCopy);};
-(void)updateCurrenGamelongDragonBy:(NSArray<KFCPDragonModel *> *)modelArr
{
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];//删除当前数据展示
    _gameData = modelArr;
    [self creatCurrenView];
}
/**长龙路子修改**/
-(void)updateCurrenGameLotteryWay:(SSCTotalMessageType)sscType longObject:(NSArray<KFCPDragonModel *> *)longDragonArr lottery:(NSArray *)lotteryWayArr{
    if ([customLoadingTool initCustomManager].isRefreshLotteryWay) {
        self.isAlreadyShowWay = NO;
    }
    if (sscType==lotteryWay&&self.isAlreadyShowWay) {
        if (lotteryWayArr.count!=0) {
            [_wayBackTotal setHidden:NO];
            [self bringSubviewToFront:_wayBackTotal];
            [_backView setHidden:YES];
            [_alertLabel setHidden:YES];
            return;
        }
        
    }else if (sscType==longDragon&&self.isAlreadyShowLongDragon){
        if (longDragonArr.count!=0) {
            [_backView setHidden:NO];
            [self bringSubviewToFront:_backView];
            [_wayBackTotal setHidden:YES];
            [_alertLabel setHidden:YES];
            return;
        }
    }
    _gameData = longDragonArr;
    _lotteryWayData = lotteryWayArr;
    self.messageType = sscType;
    if (self.isAlreadyShowLongDragon||self.isAlreadyShowWay) {
    }else{
          [self creatSelectBack];
    }
    if (self.messageType==lotteryWay) {
        [_backView setHidden:YES];
        if (_lotteryWayData.count>0) {
            [self creatLotteryWayAndLongDragonDataBackView];
        }else{
            [self creatAlertLabel];
        }
    }else{
        if (_gameData.count>0) {
            [self creatCurrenView];
        }else{
            [self creatAlertLabel];
        }
    }
}
/**触发方法**/
-(void)clickSelect:(UIButton *)button{
    self.currenLotteryButton.backgroundColor = [UIColor clearColor];
    [self.currenLotteryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.messageType = (int)button.tag-200;
    self.selectType(self.messageType);
    self.currenLotteryButton.enabled = YES;
    button.enabled = NO;
    self.currenLotteryButton = button;
    self.currenLotteryButton.backgroundColor = JesseColor(200, 48, 69);
    [self.currenLotteryButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
}
/**创建当前提示**/
-(void)creatAlertLabel{
    [_alertLabel removeFromSuperview];
    UILabel *alertLabel = [jesseGuideUIConfig getCurrenLableUiByRect:CGRectMake(0, 0, 0, 0) AndLabelTitle:@"暂无数据" AndTitleColor:[UIColor groupTableViewBackgroundColor] AndTextAlignment:NSTextAlignmentCenter AndIsAdWidth:YES AndBackColor:[UIColor clearColor] AndUseMasonry:YES];
    alertLabel.textColor = [UIColor grayColor];
    alertLabel.font = [UIFont systemFontOfSize:18];
    _alertLabel = alertLabel;
    [self addSubview:_alertLabel];
    [_alertLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self);
        make.height.mas_equalTo(100);
        make.width.mas_equalTo(100);
    }];
    self.isAlreadyShowLongDragon = YES;
    self.isAlreadyShowWay = YES;
}
/**总和点击**/
-(void)totalClick:(UIButton *)totalClick{
    self.subTotal = (int)totalClick.tag-200;
    [self dotaskTotalType:self.subTotal];
    self.subButton.backgroundColor = JesseColor(228, 228, 228);
    self.subButton.enabled = YES;
    totalClick.enabled = NO;
    self.subButton = totalClick;
    self.subButton.backgroundColor = JesseColor(254, 204, 109);
}
/**CreatSelectBack**/
-(void)creatSelectBack{
    _totalBack = [[UIView alloc]init];
    _totalBack.backgroundColor = JesseColor(245, 241, 229);
    [self addSubview:_totalBack];
    [_totalBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self).insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    _buttonBack = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
    _buttonBack.backgroundColor = JesseColor(236, 233, 217);
    [self addSubview:_buttonBack];
    CGFloat y = 0;
    CGFloat width = WIDTH/2;
    CGFloat height = 40;
    for (int b = 0; b<2; b++) {
        CGFloat x = b*width;
        UIButton *selectB = [UIButton buttonWithType:UIButtonTypeCustom];
        selectB.frame = CGRectMake(x, y, width, height);
        selectB.backgroundColor = (self.messageType==b)?JesseColor(200, 48, 69):[UIColor clearColor];
        [selectB setTitleColor:(self.messageType==b)?[UIColor whiteColor]:[UIColor blackColor] forState:UIControlStateNormal];
        selectB.layer.borderWidth = 0.5;
        selectB.layer.borderColor = JesseColor(191, 190, 189).CGColor;
        selectB.layer.cornerRadius = 0.5;
        selectB.layer.masksToBounds = YES;
        selectB.tag = 200+b;
        [selectB setTitle:self.lotteryWayTitleArr[b] forState:UIControlStateNormal];
        if (b==1) {
            self.currenLotteryButton = selectB;
        }
        [selectB addTarget:self action:@selector(clickSelect:) forControlEvents:UIControlEventTouchUpInside];
        [_buttonBack addSubview:selectB];
    }
}
#pragma mark--创建长龙
-(void)creatCurrenView
{
    _backView = [[UIView alloc]init];
    _backView.tag = 102;
    CGFloat width = WIDTH;
    CGFloat height  = 21;
    [self addSubview:_backView];
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top).offset(40);
        make.leading.trailing.bottom.equalTo(self);
    }];
    for (int i = 0; i<_gameData.count; i++) {
        KFCPDragonModel *dragonM = _gameData[i];
        CGFloat y = 21*i;
        UIView *Desback = [[UIView alloc]initWithFrame:CGRectMake(0, y, width, height)];
        [_backView addSubview:Desback];
        for (int c = 0 ; c<3;c++) {
            UILabel *desLabel = [[UILabel alloc]initWithFrame:CGRectMake((WIDTH/3)*c, 0, WIDTH/3, 20)];
            desLabel.font = [UIFont boldSystemFontOfSize:15];
            desLabel.textColor = (c==2)?JesseColor(33, 150, 243):[UIColor darkGrayColor];
            desLabel.textAlignment = NSTextAlignmentCenter;
            desLabel.adjustsFontSizeToFitWidth = YES;
            desLabel.text = (c==0)?dragonM.name:(c==1)?dragonM.openValue:[NSString stringWithFormat:@"%@",dragonM.repeatCount];
            [Desback addSubview:desLabel];
        }
        UIView *indrow = [[UIView alloc]initWithFrame:CGRectMake(0, 20, WIDTH, 1)];
        indrow.backgroundColor = JesseColor(222, 222, 222);
        [Desback addSubview:indrow];
    }
    self.isAlreadyShowLongDragon = YES;
    [customLoadingTool initCustomManager].isRefreshLotteryWay = NO;
}
/**创建路子长龙数据**/
-(void)creatLotteryWayAndLongDragonDataBackView{
    [_wayBackTotal removeFromSuperview];
    /**Sub创建**/
    _wayBackTotal = [jesseGuideUIConfig getCurrnViewUiByRect:CGRectMake(0, 40, WIDTH, 60+(WIDTH/13)*6) AndBackColor:[UIColor clearColor] AndUseMasonry:NO];
    _wayBackTotal.tag = 101;
    [self addSubview:_wayBackTotal];
    _subButtonBack = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 60)];
    _subButtonBack.bounces = NO;
    _subButtonBack.contentSize = CGSizeMake(((WIDTH-60)/3)*LOTTERY_FACTORY.lotteryWay.count+15*LOTTERY_FACTORY.lotteryWay.count+15, 0);
    _subButtonBack.backgroundColor = JesseColor(245, 241, 229);
    [_wayBackTotal addSubview:_subButtonBack];
    CGFloat subY = 10;
    CGFloat subHeight = 40;
    CGFloat subWidth = (WIDTH-60)/3;
    for (int s = 0 ; s<LOTTERY_FACTORY.lotteryWay.count; s++) {
        luziModel *luziM = LOTTERY_FACTORY.lotteryWay[s];
        CGFloat subX = subWidth*s+15*s+15;
        UIButton *subButton  = [UIButton buttonWithType:UIButtonTypeCustom];
        subButton.frame = CGRectMake(subX, subY, subWidth, subHeight);
        [subButton setTitle:luziM.name forState:UIControlStateNormal];
        [subButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        subButton.titleLabel.font = [UIFont systemFontOfSize:15];
        subButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        subButton.tag = 200+s;
        subButton.layer.cornerRadius = 6;
        subButton.layer.masksToBounds = YES;
        [subButton setBackgroundColor:(s==0)?JesseColor(254, 204, 109):JesseColor(228, 228, 228)];
        [subButton addTarget:self action:@selector(totalClick:) forControlEvents:UIControlEventTouchUpInside];
        if(s==0) {
            self.subButton = subButton;
        }
        [_subButtonBack addSubview:subButton];
    }
    /**wayCreat**/
    _lotteryWayBack = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 60, WIDTH, (WIDTH/13)*6)];
    _lotteryWayBack.bounces = NO;
    _lotteryWayBack.backgroundColor = JesseColor(245, 241, 229);
    [_wayBackTotal addSubview:_lotteryWayBack];
    [self dotaskTotalType:0];
    [_lotteryWayBack setContentOffset:CGPointMake(_lotteryWayBack.contentSize.width-WIDTH, 0) animated:NO];
    self.isAlreadyShowWay = YES;
    [customLoadingTool initCustomManager].isRefreshLotteryWay = NO;
}
-(void)dotaskTotalType:(NSInteger)typeIndex{
    
    [_lotteryWayBack.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    luziModel *lModel = _lotteryWayData[typeIndex];
    NSArray *objArr = lModel.luzhi;
   NSArray *setObjArr = [self setNumberArr:objArr];
    NSArray *ParameterArr = setObjArr[setObjArr.count-1];
    NSDictionary *desParameterArr = ParameterArr[ParameterArr.count-1];
    NSInteger number = [desParameterArr[@"x"] integerValue]+1;
    CGFloat prizeBallWidth = WIDTH/13;
    _lotteryWayBack.contentSize = CGSizeMake(prizeBallWidth*number, 0);
    void (^ballRead)(NSInteger x,NSInteger y,UIView *fatherView) = ^(NSInteger x ,NSInteger y ,UIView *fatherView){
        NSEnumerator *enumTotal = [setObjArr reverseObjectEnumerator];
        for (NSArray *setsubarr in enumTotal){
            NSEnumerator *subEnum = [setsubarr reverseObjectEnumerator];
            for (NSDictionary *subdic in subEnum){
                if ([subdic[@"x"] integerValue]==x&&[subdic[@"y"] integerValue]==y) {
                    UILabel *ballView = [[UILabel alloc]init];
                    ballView.text = subdic[@"text"];
                    ballView.adjustsFontSizeToFitWidth = YES;
                    ballView.textColor =[UIColor whiteColor];
                    ballView.font = [UIFont boldSystemFontOfSize:15];
                    ballView.textAlignment = NSTextAlignmentCenter;
                    ballView.backgroundColor = [self exportBallColor:ballView.text];
                    [fatherView addSubview:ballView];
                    [ballView mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.edges.mas_equalTo(fatherView).insets(UIEdgeInsetsMake(0.5, 0.5, 0.5, 0.5));
                    }];
                    ballView.layer.cornerRadius = ((WIDTH/13)-1)/2;
                    ballView.layer.masksToBounds = YES;
                }
            }
        }
    };
    for (NSInteger ballX = 0; ballX<number;ballX++) {
            for (NSInteger ballY = 0; ballY<6; ballY++) {
                CGFloat prizeBallX = ballX*prizeBallWidth;
                CGFloat prizeBallY = prizeBallWidth*ballY;
                UIView *ballNewView = [[UIView alloc]initWithFrame:CGRectMake(prizeBallX, prizeBallY, prizeBallWidth, prizeBallWidth)];
                ballNewView.layer.borderColor = JesseColor(191, 190, 189).CGColor;
                ballNewView.layer.borderWidth = 1;
                ballNewView.backgroundColor = [UIColor clearColor];
                [_lotteryWayBack addSubview:ballNewView];
                ballRead(ballX,ballY,ballNewView);
         }
    }

}
-(NSArray *)setNumberArr:(NSArray *)parameter{
    [self.setArr removeAllObjects];
    [self.alreadyShowArr removeAllObjects];
    BOOL alreadyShowFrist = NO;
    for (int i = 0; i<parameter.count; i++) {
        NSMutableArray *subSetArr = [[NSMutableArray alloc]init];
        if ([parameter[i] isKindOfClass:[NSNull class]]) {
            return 0;
        }
        NSMutableArray *compareFrist = [[NSMutableArray alloc] initWithArray:parameter[i]];
        for (int t = 0; t<compareFrist.count; t++) {
            NSString *objS = compareFrist[t];
            if (![self isPureInt:objS]) {
                NSString *finishDelete = [objS substringWithRange:NSMakeRange(0, 1)];
                [compareFrist replaceObjectAtIndex:t withObject:finishDelete];
            }
        }
        if (alreadyShowFrist) {
            if ([self.alreadyShowArr[i-1][@"isBeyond"] boolValue]&&(compareFrist.count>=[self.alreadyShowArr[i-1][@"beyond"] integerValue])){
                [self.alreadyShowArr addObject:@{@"isBeyond":[NSNumber numberWithBool:YES],@"beyond":@([self.alreadyShowArr[i-1][@"beyond"] integerValue]-1),@"continue":([self.alreadyShowArr[i-1][@"continue"] integerValue]==0)?@(0):@([self.alreadyShowArr[i-1][@"continue"] integerValue]-1)}];
            }else{
                if ([self.alreadyShowArr[i-1][@"continue"] integerValue]>0) {
                    [self.alreadyShowArr addObject:@{@"isBeyond":[NSNumber numberWithBool:NO],@"beyond":@(compareFrist.count),@"continue":@([self.alreadyShowArr[i-1][@"continue"] integerValue]-1)}];
                }else{
                    [self.alreadyShowArr addObject:@{@"isBeyond":[NSNumber numberWithBool:NO],@"beyond":@(compareFrist.count),@"continue":@(0)}];
                }
            }
        }else{
            if (compareFrist.count>6) {
                [self.alreadyShowArr addObject:@{@"isBeyond":[NSNumber numberWithBool:YES],@"beyond":@(6),@"continue":@(compareFrist.count-6)}];
                alreadyShowFrist = YES;
            }else{
                [self.alreadyShowArr addObject:@{@"isBeyond":[NSNumber numberWithBool:NO],@"beyond":@(compareFrist.count),@"continue":@(0)}];
            }

        }
        for (int c = 0; c<compareFrist.count; c++) {
            BOOL isBeyond = [self.alreadyShowArr[i][@"isBeyond"] boolValue];
            NSInteger beyondCount = [self.alreadyShowArr[i][@"beyond"] integerValue];
            if (i==2) {
                
            }
            if (isBeyond) {
                if (c+1>beyondCount) {
                       [subSetArr addObject:@{@"x":[@(compareFrist.count-c+i) stringValue],@"y":[@(beyondCount-1) stringValue],@"text":compareFrist[c]}];
                }else{
                    [subSetArr addObject:@{@"x":[@(i) stringValue],@"y":[@(c) stringValue],@"text":compareFrist[c]}];
                }
            }else{
                [subSetArr addObject:@{@"x":[@(i) stringValue],@"y":[@(c) stringValue],@"text":compareFrist[c]}];
                
            }
        }
        [self.setArr addObject:subSetArr];
    }
    return self.setArr;
}
-(UIColor *)exportBallColor:(NSString *)colorText{
    UIColor *objColor;
    if([self isPureInt:colorText]){
        int number = [colorText intValue];
        switch (number%5) {
            case 0:
                return [UIColor redColor];
                break;
                case 1:
                return [UIColor blueColor];
                break;
                case 2:
                return [UIColor grayColor];
                break;
                case 3:
                return [UIColor orangeColor];
                break;
                case 4:
                return [UIColor greenColor];
                break;
            default:
                break;
        }
    }else{
        NSDictionary *colorDic = @{@"单":JesseColor(198, 41, 49),@"大":JesseColor(198, 41, 49),@"龙":JesseColor(198, 41, 49),@"质":[UIColor redColor],@"小":JesseColor(18, 19, 200),@"虎":JesseColor(18, 19, 200),@"红":[UIColor redColor],@"绿":[UIColor greenColor],@"蓝":[UIColor blueColor],@"合":[UIColor greenColor],@"奇":[UIColor redColor],@"偶":[UIColor blueColor],@"上":[UIColor redColor],@"中":[UIColor grayColor],@"下":[UIColor blueColor],@"金":[UIColor orangeColor],@"木":JesseColor(165, 127, 102),@"水":[UIColor blueColor],@"火":[UIColor redColor],@"土":JesseColor(136, 136, 136),@"福":[UIColor orangeColor],@"禄":[UIColor blueColor],@"寿":[UIColor orangeColor],@"喜":[UIColor redColor]};
        if ([colorDic.allKeys containsObject:colorText]) {
            return [colorDic valueForKey:colorText];
        }else{
            return JesseColor(18, 19, 200);
        }
    }
    return objColor;
}
/**判断纯数字**/
-(BOOL)isPureInt:(NSString *)string{
    NSScanner *scan = [NSScanner scannerWithString:string];
    int val;
    return [scan scanInt:&val]&&[scan isAtEnd];
}
@end
