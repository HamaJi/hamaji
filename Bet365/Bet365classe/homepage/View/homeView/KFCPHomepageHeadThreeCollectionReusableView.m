//
//  KFCPHomepageHeadThreeCollectionReusableView.m
//  Bet365
//
//  Created by jesse on 2017/6/29.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "KFCPHomepageHeadThreeCollectionReusableView.h"
#import "KFCPJesseHomePagePaoMa.h"
#import "KFCPGameWinNoticeModel.h"
#define SectionThreeHeight 262
#define SectionThreeBarHeight 37
#define WiningListHeight 210
@interface KFCPHomepageHeadThreeCollectionReusableView()
@property(nonatomic,strong)KFCPJesseHomePagePaoMa *JessePaoMa;
@property(nonatomic,strong)UIView *barView;
@property(nonatomic,strong)NSArray *PaoMaDataArr;
@end
@implementation KFCPHomepageHeadThreeCollectionReusableView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}
#pragma mark--方法刷新
-(void)updateCurrenPaoMa:(NSArray *)PaoMaDataArr
{
    self.PaoMaDataArr = PaoMaDataArr;
    [self barView];
    [self JessePaoMa];
    [self.JessePaoMa updateCurrenAttery:self.PaoMaDataArr];
}
-(void)updateWinList:(NSArray *)winList
{
    self.PaoMaDataArr =winList;
    [self.JessePaoMa updateCurrenAttery:self.PaoMaDataArr];
}
#pragma mark--初始化bar
-(UIView *)barView
{
    if (_barView==nil) {
        _barView = [[UIView alloc]init];
        [self addSubview:_barView];
        [_barView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(0);
            make.right.mas_equalTo(self.mas_right).offset(0);
            make.top.mas_equalTo(self.mas_top).offset(0);
            make.height.mas_equalTo(SectionThreeBarHeight);
        }];

        UIView *lightIndrow = [[UIView alloc]initWithFrame:CGRectMake(0, SectionThreeBarHeight-1, WIDTH, 1)];
        lightIndrow.backgroundColor = JesseColor(236, 236, 236);
        [_barView addSubview:lightIndrow];
        UIView *indrow = [[UIView alloc]initWithFrame:CGRectMake(8, 9, 2, 17)];
        indrow.backgroundColor =[UIColor redColor];
        [_barView addSubview:indrow];
        UILabel *deslabel = [[UILabel alloc]init];
        deslabel.text = @"最新中奖榜";
        deslabel.font = [UIFont systemFontOfSize:14];
        deslabel.textAlignment = NSTextAlignmentLeft;
        deslabel.textColor = [UIColor darkGrayColor];
        deslabel.adjustsFontSizeToFitWidth = YES;
        [_barView addSubview:deslabel];
        [deslabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(indrow.mas_right).offset(5);
            make.top.mas_equalTo(_barView.mas_top);
            make.height.mas_equalTo(36);
        }];

    }
    return _barView;
}
#pragma mark--初始化当前跑马
-(KFCPJesseHomePagePaoMa *)JessePaoMa
{
    if (self.PaoMaDataArr.count==0||self.PaoMaDataArr==nil) {
        _JessePaoMa=nil;
        return _JessePaoMa;
    }
    if (_JessePaoMa==nil) {
        _JessePaoMa = [[KFCPJesseHomePagePaoMa alloc]initWithFrame:CGRectMake(0, SectionThreeBarHeight, WIDTH, SectionThreeHeight-SectionThreeBarHeight) backgroundImage:nil textFont:[UIFont systemFontOfSize:12] intervalTime:1.0 textColor:[UIColor redColor] itemsArray:self.PaoMaDataArr DLPaomaViewOrientationStyle:JessePaomaViewVerticalStyle];
        [self addSubview:_JessePaoMa];
    }
    return _JessePaoMa;
}
@end
