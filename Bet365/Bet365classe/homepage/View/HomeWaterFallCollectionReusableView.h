//
//  HomeWaterFallCollectionReusableView.h
//  Bet365
//
//  Created by super_小鸡 on 2019/9/4.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllLiveData.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, HomeWallterFallScrollStatus) {
    HomeWallterFallScrollStatus_OTHER = 0,
    HomeWallterFallScrollStatus_TOPSPACING,
    HomeWallterFallScrollStatus_BOTTOMSPACING,
};

@class HomeWaterFallCollectionReusableView;

@protocol HomeWaterFallCollectionReusableViewDelegate <NSObject>

@required

- (void)waterFallCollectionReusableView:(HomeWaterFallCollectionReusableView *)reusableView didSelectRowAtUrl:(NSString *)url;

- (void)waterFallCollectionReusableView:(HomeWaterFallCollectionReusableView *)reusableView SuperScrollEnable:(BOOL)scrollEnable;

- (UIScrollView *)waterFallCollectionReusableViewSuperScrollView:(HomeWaterFallCollectionReusableView *)reusableView;



@end

@interface HomeWaterFallCollectionReusableView : UICollectionReusableView

@property (nonatomic,strong) NSArray <GameLobby3Model *>* list;

//@property (nonatomic,readonly)UIButton *

@property (nonatomic,weak) id<HomeWaterFallCollectionReusableViewDelegate> delegate;

-(BOOL)getSuperCanScrollWithVelocity:(CGFloat)velocity;



@end

NS_ASSUME_NONNULL_END
