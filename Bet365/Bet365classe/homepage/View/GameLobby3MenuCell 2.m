//
//  GameLobby3MenuCell.m
//  Bet365
//
//  Created by super_小鸡 on 2019/9/4.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "GameLobby3MenuCell.h"

@interface GameLobby3MenuCell()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@end

@implementation GameLobby3MenuCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.indicator setColor:[UIColor skinNaviBgColor]];
    [self.indicator setHidesWhenStopped:YES];
    
    // Initialization code
}


-(void)setMenu:(GameLobby3MenuModel *)menu{
    UIImage *cacheImage = [[SDImageCache sharedImageCache] imageFromCacheForKey:menu.icon];
    if (!cacheImage) {
        [self.indicator startAnimating];
    }
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:menu.icon] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (image && !error) {
            [self.indicator stopAnimating];
        }
    }];
}

@end
