//
//  KFCPHomeViewController.m
//  Bet365
//  Created by jesse on 2017/6/26.
//  Copyright © 2017年 jesse. All rights reserved.
#import "KFCPHomeViewController.h"
#import "NetWorkMannager+Home.h"
#import "KFCPHomeGameJsonModel.h"
#import "KFCPGameWinNoticeModel.h"
#import "jesseSixOpenPrizeBet365Model.h"
#import "jesseBet365periodModel.h"
#import "AppDelegate.h"
#import "SportsModuleShare.h"
#import "Bet365SportViewController.h"
#import "KFCPHomePageHeadOneCollectionReusableView.h"
#import "KFCPHomePageHeadTwoCollectionReusableView.h"
#import "KFCPHomepageHeadThreeCollectionReusableView.h"
#import "KFCPHomePageCollectionViewCell.h"
#import "KFCPTimerModel.h"
#import "BetTotal365ViewController.h"
#import "KFCPHomeAlertView.h"
#import "KFCPActivityController.h"
#import "PushuToViewController.h"
#import "KFCPTransition.h"
#import "jesseinvertTranisition.h"
#import "LukeRecordViewController.h"
#import "KFCPWhiteTableViewCell.h"
#import "jesseGuidePageViewController.h"
#import "Bet365RecordViewController.h"
#import "LukeUserAdapter.h"
#import "jesseTestPlayViewController.h"
#import "jesseNewAlertController.h"
#import "redPickLuckywController.h"
#import "HomePageFactory.h"
#import "TimerManager.h"
#import "KFCPBuyLotteryController.h"
#import "HomeCycleReusableView.h"
#import "HomeHengFuReusableView.h"
#import "HomeCollectionViewCell.h"
#import "LukeLiveViewController.h"
#import "Bet365QPViewController.h"
#import "ElecWZRYAllViewController.h"
#import "TuLongClassFicationViewController.h"
#import "ElectronticWebDesViewController.h"
#import "DragSwipeButton.h"
#import "NSString+changeNum.h"
#import "SportEleLiveModel.h"
#import "LukeNoticeViewController.h"
#import "ChatViewController.h"
#import "ChatRightViewController.h"
#import "NoteViewController.h"
#import "Bet365QPViewController.h"
#import "GuideViewController.h"
#import "DismissingAlphaAnimator.h"
#import "PresentingAlphaAnimator.h"
#import "Bet365HengFuModel.h"
#import "HongBaoView.h"
#import "Bet365FishController.h"
#import "ChatStickMeesageView.h"
#import "HomeRedPageViewController.h"
#define SectionOneHeight 142
#define SectionTwoHeight 113
#define BarHeight 37
#define SelectKind 76
#define SectionThreeHeight 262
#define SectionThreeBarHeight 37
#define SectionThreewhiteHeight 15
#define WiningListHeight 210
#define RowHeight 83
#define BackColor JesseColor(236, 236, 236)
//屏幕比例
#define screenRate [UIScreen mainScreen].bounds.size.width/320
//按钮的初始大小
#define iconSize 50
//按钮执行移动动画的最终位置
#define showFrame CGRectMake(self.view.center.x - iconSize * 0.5 * screenRate, (200 - iconSize * 0.5) * screenRate, iconSize * screenRate, iconSize * screenRate)
@interface KFCPHomeViewController ()
<UICollectionViewDelegate,
UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout,
UINavigationControllerDelegate,
HomeCycleReusableViewDelegate,
HomeHengFuReusableViewDelegate,
HomePageHeadTwoCollectionReusableViewDelegate,
TimerManagerDelegate,
UIViewControllerTransitioningDelegate,
NoteViewControllerDelegate>{
}
//KFCPHomeHeadDelegate
@property (nonatomic,strong)ChatStickMeesageView *stickMessageView;
@property(nonatomic,strong)UICollectionView *mainCollectionView;
@property(nonatomic,strong)NSArray *SDImageArr;//轮播所有MNS数据
@property(nonatomic,strong)NSMutableArray <NSArray *>*gameDataArr;//游戏数据
@property(nonatomic,strong)NSArray *WinListArr;//中奖榜数据
@property(nonatomic,strong)NSMutableArray *NowGameValueArr;//开奖时间数据
@property(nonatomic,strong)NSMutableArray *gameTimerS;//游戏数据
@property(nonatomic,strong)NSMutableArray *GameSTimerResult;//最终返回
@property(nonatomic,assign)HomePageKindType homePageOneType;//信用官方切换
/**  */
@property (nonatomic,strong) NSArray *datas;
@property(nonatomic,strong)UIImageView *customServiceImage;
@property(nonatomic,assign)BOOL prizeCacheNow;/**开奖是否为缓存**/
@property(nonatomic,assign)BOOL updateShow;/**更新提示**/
@property (nonatomic,assign)BOOL allRefreshing;
@property (nonatomic,assign)BOOL winRefreshing;
@property (nonatomic,assign)BOOL isDP;
/** 横幅模型 */
@property (nonatomic,strong) Bet365HengFuModel *hengFuModel;
/** 红包view */
@property (nonatomic,strong) HongBaoView *hbView;

@property (nonatomic,strong) NoteViewController *noteVC;
@end
static NSString *const registerHomePageHeadOne = @"KFCHomePageHeadOne";
static NSString *const registerHomePageHeadKFTwo = @"KFCHomepageHeadtwo";
static NSString *const registerHomePageHeadThree = @"KFCHomePageHeadThree";
static NSString *const registerHomePageCell = @"KFCHomePageCell";
static NSString *const registerHomewhiteCell = @"homeWhiteCellN";
@implementation KFCPHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.delegate = self;
    [self addObserver];
    [self getAllRequestSuccesseFul];
    self.naviLoginStatus = Bet365NaviLogoItemStatus_DEFAULTS;
}
-(void)viewDidAppear:(BOOL)animated{
    
}
-(void)info_updateLoginStatus{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.allRefreshing || self.allRefreshing)
            return;
        [self.mainCollectionView reloadData];
    });
}
-(void)addObserver{
    @weakify(self);
    RACSignal *hasUpdateSignal = [RACObserve(BET_CONFIG, configHasUpdate) distinctUntilChanged];
    
    RACSignal *isShoRedSignal = [RACObserve(BET_CONFIG.common_config, isShowFloatRedPacket) distinctUntilChanged];
    
    [[RACSignal combineLatest:@[hasUpdateSignal,isShoRedSignal] reduce:^id _Nonnull(NSNumber *configHasUpdate, NSNumber *isShoRed){
        return @([configHasUpdate boolValue] && [isShoRed boolValue]);
    }] subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        dispatch_async(dispatch_get_main_queue(), ^{
            BOOL value = [x boolValue];
            if (value) {
                NSString *redPackPath = [NSString stringWithFormat:@"%@%@",SerVer_Url,[BET_CONFIG.common_config.redPacketPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                UIImage *redPackCache = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:redPackPath];
                if (!redPackCache) {
                    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:redPackPath] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                        if (finished&&!error) {
                            self.hbView.height = image.size.height/image.size.width * 80 + 30;
                            self.hbView.hongbaoImage = image;
                        }
                    }];
                }else{
                    self.hbView.height = redPackCache.size.height/redPackCache.size.width * 80 + 30;
                    self.hbView.hongbaoImage = redPackCache;
                }
                if (self.allRefreshing || self.allRefreshing){
                    return;
                }
                [self.mainCollectionView reloadData];
            }else{
                if (_hbView) {
                    [self.hbView removeFromSuperview];
                    self.hbView = nil;
                }
            }
        });

        
    }];
    [[RACObserve(USER_DATA_MANAGER, isLogin) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.allRefreshing || self.allRefreshing)
                return;
            [self.mainCollectionView reloadData];
        });
    }];
    [TIMER_MANAGER addDelegate:self];
    [TIMER_MANAGER addCycleTimerWithKey:TIMER_HOME_WINDATA_REPLACE andReduceScope:1];
    [TIMER_MANAGER addCycleTimerWithKey:TIMER_HOME_WIN andReduceScope:10];
    [RACObserve(BET_CONFIG.common_config, isDP) subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.mainCollectionView reloadData];
        });
        
    }];
    [[RACObserve(self, gameDataArr) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        NSLog(@"%@",x);
    }];
}

-(void)updataCurrenCofig{
    [self getHomeDataFromResponseCompleted:^{
        [self.mainCollectionView reloadData];
        [self getImageHeithCompelted:^{
            [[customLoadingTool initCustomManager] disMissFresh];
            [self.mainCollectionView.mj_header endRefreshing];
            dispatch_async(dispatch_get_main_queue(), ^{
               [self.mainCollectionView reloadData];
            });
        }];
        
    }];
}
-(void)getAllRequestSuccesseFul{
    /** 蛤蟆吉 **/
    [self getHomeDataFromeCache:^{

        id ppCache = [PPNetworkCache getHttpCacheForKey:[NSString stringWithFormat:@"%@%@",SerVer_Url,TheGameJsonList]];
            (!ppCache) ? [[customLoadingTool initCustomManager] showRefreshActivityAnimationForLoading]:nil;
            [self getHomeDataFromResponseCompleted:^{
                [self getImageHeithCompelted:^{
                    [[customLoadingTool initCustomManager] disMissFresh];
                    if (BET_CONFIG.noticeModel.index_notice.count > 0) {
                        
                        [self presentViewController:self.noteVC animated:YES completion:nil];
                    }
//                    if (BET_CONFIG.noticeModel.index_notice.noticeContent) {
//                        [Bet365NoticeView showWithNoticeTitle:BET_CONFIG.noticeModel.index_notice.noticeTitle noticeContent:BET_CONFIG.noticeModel.index_notice.noticeContent];
//                    }
                    JesseAppdelegate.isfinishHomeLoad = YES;/**完成加载《为引导页config》**/
                    if ([JesseAppdelegate.window.rootViewController isKindOfClass:[jesseGuidePageViewController class]])[[NSNotificationCenter defaultCenter] postNotificationName:@"GuideLaodOberser" object:nil];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.mainCollectionView reloadData];
                    });
                }];
            }];
       }];
}

/**首页数据刷新**/
-(void)loadHomePageData{
    [[customLoadingTool initCustomManager] showRefreshActivityAnimationForLoading];
    [self updataCurrenCofig];
}
#pragma mark - TimerMannagerDelegate
-(void)cycleTimerWithKey:(NSString *)key RestCount:(NSInteger)count{
    
    if (self.allRefreshing || self.winRefreshing) {
        return;
    }
    if ([key isEqualToString: TIMER_HOME_WIN]) {
        self.winRefreshing = YES;
        __block HomeOpenWinCamp *camp;
        dispatch_group_t group = dispatch_group_create();
        
        dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_semaphore_t sema = dispatch_semaphore_create(0);
            [HomePageFactory getGameOpenWinStatusDataReceiveObjType:NetReceiveObjType_RESOPONSE Completed:^(HomePageData *data) {
                if (data.response) {
                    camp = (HomeOpenWinCamp *)data.response;
                    
                    //处理当前首页开奖情况
                }
                dispatch_semaphore_signal(sema);
            }];
            
            dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        });
        
        dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_semaphore_t sema = dispatch_semaphore_create(0);
            [HomePageFactory getWinDataReceiveObjType:NetReceiveObjType_RESOPONSE Completed:^(HomePageData *data) {
                if (data.response) {
                    self.WinListArr = (NSArray *)data.response;
                    dispatch_semaphore_signal(sema);
                }
            }];
            dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        });
        
        dispatch_group_notify(group, dispatch_get_main_queue(), ^{
            self.GameSTimerResult = [[NSMutableArray alloc] initWithArray:camp.gameSTimerResult];
            self.NowGameValueArr = [[NSMutableArray alloc] initWithArray:camp.nowGameValueArr];
            self.gameTimerS = [[NSMutableArray alloc] initWithArray:camp.gameTimerS];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (!self.allRefreshing) {
                    [self.mainCollectionView reloadData];
                }
                self.winRefreshing = NO;
            });
        });
    }else if ([key isEqualToString: TIMER_HOME_WINDATA_REPLACE]){
        [self calculateDateBybetween];
    }
}
#pragma mark - HomeCycleReusableViewDelegate
-(void)HomeCycleReusableView:(HomeCycleReusableView *)view TapSDCycleIdx:(NSUInteger)idx{
    NSString *url = [self.SDImageArr lastObject][idx];
    if (url.length > 0) {
        [NAVI_MANAGER jumpToUrl:url];
    }
}
-(void)HomeCycleReusableView:(HomeCycleReusableView *)view TaphorCircleView:(aji_horCircleView *)horCircleView{
    if ([USER_DATA_MANAGER shouldPush2Login]) {
        LukeNoticeViewController *vc = [[LukeNoticeViewController alloc] init];
        vc.noticeType = NoticeType_All;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
#pragma mark - HomePageHeadTwoCollectionReusableViewDelegate
-(void)HomePageHeadTwoCollectionReusableView:(KFCPHomePageHeadTwoCollectionReusableView *)view tapLotteryType:(HomePageKindType)type{
    self.homePageOneType = type;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mainCollectionView reloadSections:[NSIndexSet indexSetWithIndex:HomeCollectionViewSection_LOTTERY]];
    });
    
}

#pragma mark - HomeHengFuReusableViewDelegate
- (void)HomeHengFuReusableViewTapwebUrlWithUrl:(NSString *)webUrl
{
    if ([USER_DATA_MANAGER shouldPush2Login]) {
        [NAVI_MANAGER jumpToUrl:webUrl];
    }
    NSLog(@"%@",webUrl);
}

#pragma mark--HeadOneDelegate
-(void)pushService{
    
    [self comitAnimation];
    self.view.userInteractionEnabled = NO;
}
/**轮播事件处理**/
-(void)loadSDLunBoUrl:(NSString *)url{
    if (url.length>1) {
        KFCPActivityController *lunboActive  = [[KFCPActivityController alloc]init];
        [lunboActive loadhongBaoUrl:url];
        [self.navigationController pushViewController:lunboActive animated:YES];
    }
}
#pragma mark--带入动画
-(void)comitAnimation{
    __weak __typeof__(self) weakSelf = self;
    HomeCollectionViewCell *cell = (HomeCollectionViewCell *)[self.mainCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
    CGRect pointRect = [[cell getSeverImg] convertRect:[cell getSeverImg].bounds toView:self.view];
    self.customServiceImage = [[UIImageView alloc]initWithFrame:CGRectMake(pointRect.origin.x, pointRect.origin.y, 50, 50)];
    self.customServiceImage.image = [UIImage imageNamed:@"1-2"];
    self.customServiceImage.contentMode = UIViewContentModeScaleAspectFit;
    self.customServiceImage.layer.cornerRadius = 25;
    self.customServiceImage.layer.masksToBounds = YES;;
    [self.view addSubview:self.customServiceImage];
    //     创建一个缩放动画并执行
    CABasicAnimation *scalAnimation = [self creatScaleAnimansWithFromValue:@1 andToValue:@2];
    [self.customServiceImage.layer addAnimation:scalAnimation forKey:@"frameShow"];
    //    用uiview动画改变按钮的frame，因为涉及到事件，用CABasicAnimation处理会很麻烦一点而且可能会出现问题
    [UIView animateWithDuration:0.4 animations:^{
        self.customServiceImage.frame = showFrame;
        self.IconCenter = self.customServiceImage.center;
        weakSelf.customServiceImage.contentMode = UIViewContentModeScaleToFill;
        weakSelf.customServiceImage.image = [UIImage imageNamed:@"1-2"];
    } completion:^(BOOL finished) {
        //        动画执行完以后，也就是按钮到达最终展开时的位置触发pushu方法
        [self.customServiceImage removeFromSuperview];
        self.navigationController.delegate = self;
        PushuToViewController *pushVC = [[PushuToViewController alloc]init];
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bar"]];
        self.view.userInteractionEnabled = YES;
        pushVC.IconCenter = weakSelf.IconCenter;
        [weakSelf.navigationController pushViewController:pushVC animated:YES];
        
    }];
}
- (CABasicAnimation *)creatScaleAnimansWithFromValue:(id)fromValue andToValue:(id)toValue {
    //缩放动画
    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    scaleAnimation.repeatCount = 0;
    scaleAnimation.fillMode = kCAFillModeForwards;
    scaleAnimation.removedOnCompletion = NO;
    scaleAnimation.duration = 0.4;
    scaleAnimation.fromValue = fromValue;
    scaleAnimation.toValue = toValue;
    return scaleAnimation;
}
#pragma mark - UINavigationControllerDelegate
//导航控制器的代理方法，在这里监听push事件，并拿自定义的跳转方式替换默认的
- (id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation
                                                fromViewController:(UIViewController *)fromVC
                                                  toViewController:(UIViewController *)toVC{
    if (operation == UINavigationControllerOperationPush) {
        //push的跳转
        if ([NSStringFromClass([toVC class])isEqualToString:@"PushuToViewController"]) {
            KFCPTransition *ping = [[KFCPTransition alloc]init];
            ping.IconCenter = self.IconCenter;
            return ping;
        }else{
            return nil;
        }
    }else{
        
        return nil;
    }
}

//移除通知
-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark--mainCollectionViewDelegate
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return  4;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section == HomeCollectionViewSection_CYCLE) {
        return 5;
    }else if (section == HomeCollectionViewSection_GAME){
        
        if (BET_CONFIG.common_config.isDP) {
            return self.datas.count;
        }else{
            return 0;
        }
    }else if (section == HomeCollectionViewSection_LOTTERY){
        
        if (((NSArray *)self.gameDataArr[self.homePageOneType]).count>16) {
            return 16;
        }else{
            return ((NSArray *)self.gameDataArr[self.homePageOneType]).count;
        }
        return 0;
    }else if(section == HomeCollectionViewSection_WIN){
        return 0;
    }
    return 0;
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (section == HomeCollectionViewSection_GAME) {
        return UIEdgeInsetsMake(0, HomeCollectionCellGameSpacing, 0, HomeCollectionCellGameSpacing);
    }
    return UIEdgeInsetsMake(0, 0, 0, 0);
    
}
- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if (section == HomeCollectionViewSection_GAME) {
        return HomeCollectionCellGameSpacing;
    }else if (section == HomeCollectionViewSection_CYCLE){
        return HomeCollectionCellCycleSpacing;
    }
    return 0.0;
}
- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    if (section == HomeCollectionViewSection_GAME) {
        return HomeCollectionCellGameSpacing;
    }
    return 0.0;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == HomeCollectionViewSection_CYCLE || indexPath.section == HomeCollectionViewSection_GAME) {
        return homeCellSize(indexPath.section);
    }
    return CGSizeMake(WIDTH/2, RowHeight);
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (section==HomeCollectionViewSection_CYCLE) {
        CGFloat height = self.SDImageArr.count ? BET_CONFIG.config.lunboHeight : 0.0;
        height += 45;
        return CGSizeMake(WIDTH, height);
    }else if (section == HomeCollectionViewSection_GAME){
        return CGSizeMake(WIDTH, (self.hengFuModel && self.hengFuModel.hfImage.length > 1 && self.hengFuModel.webUrl.length > 1) ? 110 : 10);
    }else if (section==HomeCollectionViewSection_LOTTERY){
        if (!self.gameDataArr.count) {
            return CGSizeMake(0.0, 0.0);
        }
        return (((NSArray *)self.gameDataArr[0]).count==0)?CGSizeMake(0, 0):CGSizeMake(WIDTH,SectionTwoHeight);
    }else{
        return CGSizeMake(WIDTH, SectionThreeHeight);
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    if(section == HomeCollectionViewSection_GAME ||
       section == HomeCollectionViewSection_LOTTERY){
        return CGSizeMake(WIDTH, 10);
    }
    return CGSizeZero;
}
-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==HomeCollectionViewSection_CYCLE) {
        if (kind==UICollectionElementKindSectionHeader) {
            HomeCycleReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[HomeCycleReusableView getIdentifier] forIndexPath:indexPath];
            __block NSMutableArray *list = @[].mutableCopy;
            
            [BET_CONFIG.noticeModel.roll_notice enumerateObjectsUsingBlock:^(NoticeModel  * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.noticeContent) {
                    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:obj.noticeContent];
                    attStr ? ([list addObject:attStr]) : nil;
                }
            }];
            
            if (!list.count) {
                NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:@"数据正在努力获取......"];
                [attStr setAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor],
                                        NSFontAttributeName : [UIFont systemFontOfSize:15]} range:NSMakeRange(0,@"数据正在努力获取......".length)];
                attStr ? [list addObject:attStr] : nil;
            }
            header.noticeList = list;
            
            if (self.SDImageArr.count) {
                header.imageURLStringsGroup = self.SDImageArr.firstObject;
            }
            header.delegate = self;
            return header;
        }
    }else if(indexPath.section == HomeCollectionViewSection_GAME){
        if (kind==UICollectionElementKindSectionHeader) {
            HomeHengFuReusableView *hengfu = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[HomeHengFuReusableView getIdentifier] forIndexPath:indexPath];
            hengfu.delegate = self;
            hengfu.model = self.hengFuModel;
            return hengfu;
        }else if(kind==UICollectionElementKindSectionFooter){
            UICollectionReusableView *reusable = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"foot" forIndexPath:indexPath];
            return reusable;
        }
    }else if (indexPath.section==HomeCollectionViewSection_LOTTERY){
        if (kind==UICollectionElementKindSectionHeader) {
            KFCPHomePageHeadTwoCollectionReusableView *headTwo = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:registerHomePageHeadKFTwo forIndexPath:indexPath];
            [headTwo creatUIAndCurrenType:self.homePageOneType];
            headTwo.delegate = self;
            return headTwo;
        }else if(kind==UICollectionElementKindSectionFooter){
            UICollectionReusableView *reusable = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"foot" forIndexPath:indexPath];
            return reusable;
        }
        
    }else if (indexPath.section==HomeCollectionViewSection_WIN){
        if (kind==UICollectionElementKindSectionHeader) {
            KFCPHomepageHeadThreeCollectionReusableView *headThree = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:registerHomePageHeadThree forIndexPath:indexPath];
            [headThree updateCurrenPaoMa:self.WinListArr];
            return headThree;
        }
        
    }
    return nil;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    if (indexPath.section == HomeCollectionViewSection_CYCLE) {
        HomeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[HomeCollectionViewCell getIdentifier] forIndexPath:indexPath];
        
        [cell updateConfigWithCellType:indexPath.section
                              ImageStr:getHomeCollectionCycleCellImage(indexPath)
                                Titile:getHomeCollectionCycleCellTitile(indexPath)
                             textColor:getHomeCollectionCellCycleColor(indexPath)];
        return cell;
    }
    
    if (indexPath.section == HomeCollectionViewSection_GAME) {
        HomeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[HomeCollectionViewCell getIdentifier] forIndexPath:indexPath];
        cell.item = [self.datas safeObjectAtIndex:indexPath.row];
        return cell;
    }
    if (indexPath.section == HomeCollectionViewSection_LOTTERY) {
        KFCPHomePageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:registerHomePageCell forIndexPath:indexPath];
        KFCPHomeGameJsonModel *model = self.gameDataArr[self.homePageOneType][indexPath.row];
        NSInteger indexNumber = 0;
        
        if (((NSArray *)[self.gameDataArr safeObjectAtIndex:self.homePageOneType]).count>16) {
            indexNumber = 15;
        }else{
            
            indexNumber = ((NSArray *)[self.gameDataArr safeObjectAtIndex:self.homePageOneType]).count;
        }
        if (indexPath.row==indexNumber) {
            [cell updateCurrenDes:@"更多游戏" AndDate:@"更多游戏玩法" AndLogImageName:@"" AndKindLog:@""];
            [cell upMoreImage:[UIImage imageNamed:@"moreGame"]];
        }else{
            [cell updateCurrenDes:model.name
                          AndDate:model.openFrequency
                  AndLogImageName:[NSString stringWithFormat:jesse(@"/images/gameLogo/%@.png"),model.GameId]
                       AndKindLog:(self.homePageOneType == HomePageOfficalType) ? @"官" : (((NSArray *)[self.gameDataArr safeObjectAtIndex:0]).count==0)?@"":@"信"];
        }
        return cell;
    }
    
    return nil;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == HomeCollectionViewSection_CYCLE) {
        switch (indexPath.row) {
                case 0:
            {
                if ([USER_DATA_MANAGER shouldPush2Login]) {
                    self.tabBarController.selectedIndex = 4;
                }
            }
                break;
                case 1:
            {
                if (USER_DATA_MANAGER.isLogin) {
                        self.tabBarController.selectedIndex = 4;
                }else{
                    [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:[[jesseLogBet365ViewController alloc] init] animated:YES];
                }
            }
                break;
                case 2:
            {
                KFCPActivityController *activit = [[KFCPActivityController alloc]init];
                [activit loadhongBaoUrl:[NSString stringWithFormat:@"%@%@?_t=%@",SerVer_Url,ActivityhtmlUrl,[LukeUserAdapter getNowTimeTimestamp]]];
                [self.navigationController pushViewController:activit animated:YES];
            }
                break;
                
                case 3:
            {
                [self comitAnimation];
                self.view.userInteractionEnabled = NO;
            }
                break;
                case 4:
            {
                if (USER_DATA_MANAGER.isLogin) {
                    if (BET_CONFIG.common_config.isDP) {
                        [self presentViewController:[[Bet365RecordViewController alloc] init] animated:YES completion:nil];
                    }else{
                        [self.navigationController pushViewController:[[LukeRecordViewController alloc]init] animated:YES];
                    }
                }else{
                    if (BET_CONFIG.registerConfig.trailUserValidCode) {
                        jesseTestPlayViewController *testPlay = [[jesseTestPlayViewController alloc]init];
                        [self.navigationController pushViewController:testPlay animated:YES];
                    }else{
                        [USER_DATA_MANAGER requestTestRegister:^(BOOL success) {
                        }];
                    }
                }
            }
                break;
            default:
                break;
        }
        
    }else if (indexPath.section == HomeCollectionViewSection_GAME){
        SportEleLiveModel *item = self.datas[indexPath.row];
        if ([item.type isEqualToString:@"聊天室"]) {
            [NAVI_MANAGER jumpToUrl:ROUTER_CHAT];
        }else if (item.isHref && item.GameId) {
            [self pushViewControllerWithGameId:[NSString stringWithFormat:@"%ld",item.GameId]];
        }else if (item.isHref && !item.GameId && [item.type isEqualToString:@"彩票"]) {
            JesseAppdelegate.tabbar.selectedIndex = 1;
        }else if (!item.isHref && item.link.length) {
            KFCPActivityController *actVC = [[KFCPActivityController alloc] init];
            NSString *strLink = nil;
            if (![item.link hasPrefix:@"http"]) {
                strLink = StringFormatWithStr(SerVer_Url, item.link);
            }else{
                strLink = item.link;
            }
            NSString *percentS = [strLink stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            if ([item.type isEqualToString:@"快速充值"] || [item.type isEqualToString:@"抢红包"]) {
                [actVC loadhongBaoUrl:percentS];
            }else if ([item.type isEqualToString:@"连环夺宝"]){
                [actVC loadWebOpenUrlBySafari:percentS forParameter:[self getPushGameCurrenToken]];
            }else{
                [actVC loadhongBaoUrl:percentS];
            }
            [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:actVC animated:YES];
        }else if (item.isHref && [item.type isEqualToString:@"体育"]) {
            Bet365SportViewController *sport = [[Bet365SportViewController alloc] init];
            [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:sport animated:YES];
        }else if (item.isHref && ![item.type IsChinese] && ![item.name isEqualToString:@"AG捕鱼"] && ![item.name isEqualToString:@"AG视讯"] && ![item.name isEqualToString:@"AG国际馆"]) {
            if (item.subId && item.subId.length > 0) {
                if ([USER_DATA_MANAGER shouldPush2Login]) {
                    ElectronticWebDesViewController *webVC = [[ElectronticWebDesViewController alloc]init];
                    webVC.liveCode = item.type;
                    webVC.gameType = item.subId;
                    [self.navigationController pushViewController:webVC animated:YES];
                }
            }else if ([item.type isEqualToString:@"mgLive"]){
                if ([USER_DATA_MANAGER shouldPush2Login]) {
                    ElectronticWebDesViewController *webVC = [[ElectronticWebDesViewController alloc]init];
                    webVC.liveCode = @"mg";
                    webVC.gameType = @"70585";
                    [self.navigationController pushViewController:webVC animated:YES];
                }
            }else{
                
                [self pushActivityControllerWithType:item.type gameId:@""];
            }
        }else if (item.isHref && ![item.type IsChinese] && [item.name isEqualToString:@"AG捕鱼"]) {
            [self pushActivityControllerWithType:item.type gameId:@"6"];
        }else if (item.isHref && ![item.type IsChinese] && ([item.name isEqualToString:@"AG视讯"] || [item.name isEqualToString:@"AG国际馆"])) {
            [self pushActivityControllerWithType:item.type gameId:@"1"];
        }else if (item.isHref && [item.type isEqualToString:@"真人视讯"]) {
            LukeLiveViewController *VC = [[LukeLiveViewController alloc] init];
            VC.gameType = Bet365GameType_Live;
            [self.navigationController pushViewController:VC animated:YES];
        }else if (item.isHref && [item.type isEqualToString:@"棋牌"]) {
            LukeLiveViewController *VC = [[LukeLiveViewController alloc] init];
            VC.gameType = Bet365GameType_Chess;
            [self.navigationController pushViewController:VC animated:YES];
        }else if (item.isHref && [item.type isEqualToString:@"电子"]) {
            LukeLiveViewController *VC = [[LukeLiveViewController alloc] init];
            VC.gameType = Bet365GameType_Elec;
            [self.navigationController pushViewController:VC animated:YES];
        }else if (item.isHref && [item.name isEqualToString:@"王者荣耀"]){
            [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:[[ElecWZRYAllViewController alloc] init] animated:YES];
        }else if (item.isHref && [item.name isEqualToString:@"长龙排行"]) {
            TuLongClassFicationViewController *tulong = [[TuLongClassFicationViewController alloc]init];
            [tulong enterCurrenTuLongMain:ClassFication];
            [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:tulong animated:YES];
        }else if ([item.type isEqualToString:@"金宝棋牌"]) {
            if (item.subId && item.subId.length>0) {
                 [self pushActivityControllerWithType:@"jb" gameId:item.subId];
            }else{
                Bet365QPViewController *vc = [[Bet365QPViewController alloc] init];
                vc.VcType = Bet365ElecQiPaiControllerType_Elec;
                vc.type = @"jb";
                [self.navigationController pushViewController:vc animated:YES];
            }
        }else if ([item.type isEqualToString:@"开元棋牌"]) {
            if (item.subId && item.subId.length>0) {
                [self pushActivityControllerWithType:@"ky" gameId:item.subId];
            }else{
                Bet365QPViewController *vc = [[Bet365QPViewController alloc] init];
                vc.VcType = Bet365ElecQiPaiControllerType_Elec;
                vc.type = @"ky";
                [self.navigationController pushViewController:vc animated:YES];
            }
        }else if ([item.type isEqualToString:@"ag电子"]) {
            Bet365QPViewController *vc = [[Bet365QPViewController alloc] init];
            vc.VcType = Bet365ElecQiPaiControllerType_Elec;
            vc.type = @"ag";
            [self.navigationController pushViewController:vc animated:YES];
        }else if ([item.type isEqualToString:@"bbin电子"]) {
            Bet365QPViewController *vc = [[Bet365QPViewController alloc] init];
            vc.VcType = Bet365ElecQiPaiControllerType_Elec;
            vc.type = @"bbin";
            [self.navigationController pushViewController:vc animated:YES];
        }else if ([item.type isEqualToString:@"捕鱼"]) {
            Bet365FishController *fishVC = [[Bet365FishController alloc] init];
            [self.navigationController pushViewController:fishVC animated:YES];
        }else if ([item.type isEqualToString:@"棋乐游"]) {
            if (item.subId && item.subId.length>0) {
                [self pushActivityControllerWithType:@"qly" gameId:item.subId];
            }else{
                Bet365QPViewController *vc = [[Bet365QPViewController alloc] init];
                vc.VcType = Bet365ElecQiPaiControllerType_Elec;
                vc.type = @"qly";
                [self.navigationController pushViewController:vc animated:YES];
            }
        }else if ([item.type isEqualToString:@"其他棋牌"]) {
            if (item.subId && item.subId.length>0) {
                [self pushActivityControllerWithType:@"lucky" gameId:item.subId];
            }else{
                Bet365QPViewController *vc = [[Bet365QPViewController alloc] init];
                vc.VcType = Bet365ElecQiPaiControllerType_Elec;
                vc.type = @"lucky";
                [self.navigationController pushViewController:vc animated:YES];
            }
        }else if ([item.type isEqualToString:@"推广赚钱"]) {
            [NAVI_MANAGER jumpToUrl:item.link];
        }
    }else if (indexPath.section == HomeCollectionViewSection_LOTTERY){
        if (((NSArray *)self.gameDataArr[self.homePageOneType]).count==indexPath.row) {
            return;
        }
        NSInteger indexNumber = 0;
        if (((NSArray *)self.gameDataArr[self.homePageOneType]).count>16) {
            indexNumber = 15;
        }else{
            indexNumber = ((NSArray *)self.gameDataArr[self.homePageOneType]).count-1;
        }
        if (indexPath.row==indexNumber&&((NSArray *)self.gameDataArr[self.homePageOneType]).count>16) {
            self.tabBarController.selectedIndex = 1;
        }else{
            if (!JesseAppdelegate.ContainVistorKey) {
                KFCPHomeGameJsonModel *model = self.gameDataArr[self.homePageOneType][indexPath.row];
                [self pushViewControllerWithGameId:model.GameId];
            }else{
                if (![BET_CONFIG.config.visitors_can_use_cp_view isEqualToString:@"1"] &&
                    !USER_DATA_MANAGER.isLogin) {
                    jesseLogBet365ViewController *loginC = [[jesseLogBet365ViewController alloc]init];
                    [self.navigationController pushViewController:loginC animated:YES];
                }else{
                    KFCPHomeGameJsonModel *model = self.gameDataArr[self.homePageOneType][indexPath.row];
                    [self pushViewControllerWithGameId:model.GameId];
                }
            }
        }
    }
}

#pragma mark--计算时间
-(void)calculateDateBybetween;
{
    __block int replaceN;
    [self.gameTimerS enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        KFCPTimerModel *betweentTime = obj;
        KFCPTimerModel *result = self.GameSTimerResult[idx];
        NSString *match = @"(^[\u4e00-\u9fa5]+$)";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF matches %@", match];
        if (![predicate evaluateWithObject:result.timer]) {
            replaceN = [betweentTime.timer intValue];
            replaceN=replaceN-1;
            betweentTime.timer = [NSString stringWithFormat:@"%d",replaceN];
            [self.gameTimerS replaceObjectAtIndex:idx withObject:betweentTime];
            result.timer = currenTime(replaceN);
            [self.GameSTimerResult replaceObjectAtIndex:idx withObject:result];
        }
    }];
}
//将某个时间戳转化成时间
#pragma mark--doJsonTimeTask<对当前时间格式化>
NSString *currenTime(int betweent){
    
    if (betweent==0||betweent<0) {
        return @"开奖中";
    }
    int H = betweent/3600;
    int M = betweent%3600/60;
    int S = betweent%3600%60;
    if (H<10&&S>=10&&M>=10){
        return [NSString stringWithFormat:@"0%d:%d:%d",H,M,S];
    }else if (M<10&&H>=10&&S>=10){
        return [NSString stringWithFormat:@"%d:0%d:%d",H,M,S];
    }else if (S<10&&H>=10&&M>=10){
        return [NSString stringWithFormat:@"%d:%d:0%d",H,M,S];
    }else if (H<10&&M<10&&S>=10){
        return [NSString stringWithFormat:@"0%d:0%d:%d",H,M,S];
    }else if (H<10&&S<10&&M>=10){
        return [NSString stringWithFormat:@"0%d:%d:0%d",H,M,S];
    }else if (M<10&&S<10&&H>=10){
        return [NSString stringWithFormat:@"%d:0%d:0%d",H,M,S];
    }else if (M<10&&H<10&&S<10){
        return [NSString stringWithFormat:@"0%d:0%d:0%d",H,M,S];
    }else{
        return [NSString stringWithFormat:@"%d:%d:%d",H,M,S];
    }
}

#pragma mark--初始化当前
-(NSMutableArray *)GameSTimerResult{return _GameSTimerResult?_GameSTimerResult:(_GameSTimerResult=@[].mutableCopy);};
#pragma mark--collectionview初始化
-(UICollectionView *)mainCollectionView
{
    if (_mainCollectionView==nil) {
        self.automaticallyAdjustsScrollViewInsets=NO;
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing=0;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _mainCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 64, WIDTH, HEIGHT-64-49) collectionViewLayout:layout];
        _mainCollectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _mainCollectionView.delegate = self;
        _mainCollectionView.dataSource = self;
        _mainCollectionView.showsHorizontalScrollIndicator = NO;
        [self.view addSubview:_mainCollectionView];
        /**添加刷新组件**/
        MJRefreshStateHeader *heder = [MJRefreshStateHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadHomePageData)];
        _mainCollectionView.mj_header = heder;
        
        /** 轮播图\公告栏**/
        [_mainCollectionView registerClass:[HomeCycleReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[HomeCycleReusableView getIdentifier]];
        
        /** 横幅**/
        [_mainCollectionView registerClass:[HomeHengFuReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[HomeHengFuReusableView getIdentifier]];
        
        /** 主入口、真人体育**/
        [_mainCollectionView registerNib:[UINib nibWithNibName:@"HomeCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:[HomeCollectionViewCell getIdentifier]];
        
        /** 彩票**/
        [_mainCollectionView registerClass:[KFCPHomePageHeadTwoCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:registerHomePageHeadKFTwo];
        [_mainCollectionView registerClass:[KFCPHomePageCollectionViewCell class] forCellWithReuseIdentifier:registerHomePageCell];
        
        
        [_mainCollectionView registerClass:[KFCPHomepageHeadThreeCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:registerHomePageHeadThree];
        
        [_mainCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];
        [_mainCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"foot"];
        
    }
    return _mainCollectionView;
}

#pragma mark--获取当前时时最新数据
-(void)getCurrenAtTheTimeData:(void (^)(NSMutableArray *, NSMutableArray * ,NSMutableArray *,BOOL isCache))AtTimeBlock{
    AtTimeBlock(self.NowGameValueArr,self.gameTimerS,self.GameSTimerResult,self.prizeCacheNow);
}

NSString *getHomeCollectionCycleCellImage(NSIndexPath *indexPath){
    switch (indexPath.row) {
        case 0:
            return @"1-3";
        case 1:
            return @"1-7";
        case 2:
            return @"1-4";
        case 3:
            return @"1-2";
        case 4:
            return USER_DATA_MANAGER.isLogin ? @"1-6" : @"1-5";
        default:
            return nil;
    }
}
NSString *getHomeCollectionCycleCellTitile(NSIndexPath *indexPath){
    switch (indexPath.row) {
        case 0:
            return @"充/提款";
        case 1:
            return USER_DATA_MANAGER.isLogin ? @"会员中心" :@"登录注册";
        case 2:
            return @"优惠活动";
        case 3:
            return @"在线客服";
        case 4:
            return USER_DATA_MANAGER.isLogin ? @"投注记录" : @"试玩";
        default:
            return nil;
    }
}

UIColor *getHomeCollectionCellCycleColor(NSIndexPath *indexPath){
    return @[
             JesseColor(237, 185, 63),
             JesseColor(0, 156, 136),
             JesseColor(83, 182, 83),
             JesseColor(230,46,37),
             JesseColor(0, 122, 255)
             ][indexPath.row];
}

#pragma mark - PUSH
- (void)pushViewControllerWithGameId:(NSString *)gameId{
    if (![BET_CONFIG.config.visitors_can_use_cp_view isEqualToString:@"1"] &&
        !USER_DATA_MANAGER.isLogin) {
        jesseLogBet365ViewController *loginC = [[jesseLogBet365ViewController alloc]init];
        [self.navigationController pushViewController:loginC animated:YES];
    }else{
        [self homePushEnterFactory:(self.homePageOneType!=HomePageOfficalType)?YES:NO FactoryGameId:gameId TuLongEnterValue:NO];
    }
}

#pragma mark - GET/SET
-(NSMutableArray <NSArray *>*)gameDataArr{
    if(!_gameDataArr){
        _gameDataArr = [[NSMutableArray alloc] initWithArray:@[@[],@[]]];
    }
    return _gameDataArr;
}

- (HongBaoView *)hbView
{
    if (!_hbView) {
        _hbView = [[HongBaoView alloc] initWithFrame:CGRectMake(WIDTH - 80, 300, 80, 100)];
        [self.view insertSubview:_hbView aboveSubview:self.mainCollectionView];
    }
    return _hbView;
}

-(ChatStickMeesageView *)stickMessageView{
    if (!_stickMessageView) {
        _stickMessageView = [[[NSBundle mainBundle] loadNibNamed:@"ChatStickMeesageView" owner:self options:nil] firstObject];
    }
    return _stickMessageView;
}

#pragma mark - NetRequest
-(void)getImageHeithCompelted:(void(^)())completed{
    
    if (((NSArray *)[self.SDImageArr safeObjectAtIndex:0]).count > 0) {
        UIImage *bannerImage = [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:self.SDImageArr[0][0]];
        if (bannerImage) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"轮播高度(蛤蟆吉)获取成功");
                CGFloat getSizeWidth = CGImageGetWidth(bannerImage.CGImage);
                CGFloat getSizeHeight = CGImageGetHeight(bannerImage.CGImage);
                CGFloat scale = WIDTH/getSizeWidth;
                CGFloat scaleResultHeight = getSizeHeight * scale;
                BET_CONFIG.config.lunboHeight = scaleResultHeight;
                [BET_CONFIG.config save];
                completed ? completed() : nil;
            });
        }else{
            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:self.SDImageArr[0][0]] options:SDWebImageAvoidAutoSetImage progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                if (finished&&!error) {
                    CGFloat getSizeWidth = CGImageGetWidth(image.CGImage);
                    CGFloat getSizeHeight = CGImageGetHeight(image.CGImage);
                    CGFloat scale = WIDTH/getSizeWidth;
                    CGFloat scaleResultHeight = getSizeHeight * scale;
                    BET_CONFIG.config.lunboHeight = scaleResultHeight;
                    NSLog(@"轮播高度(蛤蟆吉)获取成功");
                }else{
                    BET_CONFIG.config.lunboHeight = 0.0;
                    NSLog(@"轮播高度(蛤蟆吉)获取失败");
                }
                [BET_CONFIG.config save];
                completed ? completed() : nil;
            }];
        }
    }else{
        BET_CONFIG.config.lunboHeight = 0.0;
        [BET_CONFIG.config save];
        completed ? completed() : nil;
    }
}

-(void)getHomeDataFromeCache:(void(^)())completed{
    [HomePageFactory getNoticeReceiveObjType:NetReceiveObjType_CACHE Completed:^(HomePageData *data) {
    }];

    [LOTTERY_FACTORY getGameDataReceiveObjType:NetReceiveObjType_RESOPONSE Completed:^(LotteryRequestData *data) {
        
        if (data.response) {
            self.gameDataArr = [[NSMutableArray alloc] initWithArray:data.response];
        }else{
            self.gameDataArr = nil;
        }
    }];
    [HomePageFactory getWinDataReceiveObjType:NetReceiveObjType_CACHE Completed:^(HomePageData *data) {
        self.WinListArr = [NSArray arrayWithArray:data.response];
    }];
    
    
    /** 游客返点获取 **/
    [HomePageFactory getVisitorsComeBackReceiveObjType:NetReceiveObjType_CACHE Completed:^(HomePageData *data) {
    }];

    
    //**六合彩**//
    [HomePageFactory getSixOpenReceiveObjType:NetReceiveObjType_CACHE Completed:^(HomePageData *data) {
    }];
    
    //** 开奖状态**/
    [HomePageFactory getGameOpenWinStatusDataReceiveObjType:NetReceiveObjType_CACHE Completed:^(HomePageData *data) {
        if (data.response) {
            
            HomeOpenWinCamp *camp = (HomeOpenWinCamp *)data.response;
            self.GameSTimerResult = [[NSMutableArray alloc] initWithArray:camp.gameSTimerResult];
            self.NowGameValueArr = [[NSMutableArray alloc] initWithArray:camp.nowGameValueArr];
            self.gameTimerS = [[NSMutableArray alloc] initWithArray:camp.gameTimerS];
            [self calculateDateBybetween];
            
            
        }
    }];

    //** 游戏设置配置 **/
    [HomePageFactory getGamePlaySettingDataReceiveObjType:NetReceiveObjType_CACHE Completed:^(HomePageData *data) {
    } ];
    
    [HomePageFactory getBannerUrlsReceiveObjType:NetReceiveObjType_CACHE Completed:^(HomePageData *data) {
        self.SDImageArr = data.response;
        
        SportsShareInstance.BannerArrs = data.response;
    }];
    
    [HomePageFactory getZhenRenDataReceiveObjType:NetReceiveObjType_CACHE Completed:^(HomePageData *data) {
        if (data.response) {
            self.datas = [SportEleLiveModel mj_objectArrayWithKeyValuesArray:data.response];
        }else{
            self.datas = nil;
        }
    }];
    LOTTERY_FACTORY.gameDataJson = [NSArray arrayWithArray:self.gameDataArr];//赋值全局使用
    [self.mainCollectionView reloadData];
    completed ? completed() : nil;
}


/** 线程池处理(首页界面数据)**/
-(void)getHomeDataFromResponseCompleted:(void(^)())completed{
    self.allRefreshing = YES;
    __block NSArray *WinListArr;
    __block HomeOpenWinCamp *camp;
    __block NSArray *zhenData;
    dispatch_group_t group = dispatch_group_create();
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);

        /** 公告栏数据 **/
        [HomePageFactory getNoticeReceiveObjType:NetReceiveObjType_RESOPONSE Completed:^(HomePageData *data) {
            dispatch_semaphore_signal(sema);//1
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });

    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);

        /** 游戏数据 **/
        [LOTTERY_FACTORY getGameDataReceiveObjType:NetReceiveObjType_RESOPONSE Completed:^(LotteryRequestData *data) {
            dispatch_semaphore_signal(sema);
        }];

        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });

    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);

        /** 中奖数据 **/
        [HomePageFactory getWinDataReceiveObjType:NetReceiveObjType_RESOPONSE Completed:^(HomePageData *data) {
            WinListArr = data.response;
            
            dispatch_semaphore_signal(sema);//1
        }];

        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });

    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);

        /** 游客返点获取 **/
        [HomePageFactory getVisitorsComeBackReceiveObjType:NetReceiveObjType_RESOPONSE Completed:^(HomePageData *data) {
          
            dispatch_semaphore_signal(sema);//1
        }];

        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });

    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);

        //**六合彩**//
        [HomePageFactory getSixOpenReceiveObjType:NetReceiveObjType_RESOPONSE Completed:^(HomePageData *data) {
            
            dispatch_semaphore_signal(sema);//1
        }];

        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);

        //** 开奖状态**/
        [HomePageFactory getGameOpenWinStatusDataReceiveObjType:NetReceiveObjType_RESOPONSE Completed:^(HomePageData *data) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (data.response) {
                    camp = (HomeOpenWinCamp *)data.response;
                }
            });
            dispatch_semaphore_signal(sema);//1
        }];
        
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });

    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        /** 轮播图 **/
        
        [HomePageFactory getBannerUrlsReceiveObjType:NetReceiveObjType_RESOPONSE Completed:^(HomePageData *data ) {

            SportsShareInstance.BannerArrs = data.response;
            dispatch_semaphore_signal(sema);//1
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });

    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);

        //** 游戏设置配置 **/
        [HomePageFactory getGamePlaySettingDataReceiveObjType:NetReceiveObjType_RESOPONSE Completed:^(HomePageData *data) {
            dispatch_semaphore_signal(sema);
        }];
        
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sem = dispatch_semaphore_create(0);
        [HomePageFactory getAppHengfuDataReceiveObjType:NetReceiveObjType_RESOPONSE Completed:^(HomePageData *data) {
            if (data.response) {
                self.hengFuModel = [Bet365HengFuModel mj_objectWithKeyValues:data.response];
            }
            dispatch_semaphore_signal(sem);
        }];
        dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);

        //** 真人、体育 **/
        [HomePageFactory getZhenRenDataReceiveObjType:NetReceiveObjType_RESOPONSE Completed:^(HomePageData *data) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                zhenData = [SportEleLiveModel mj_objectArrayWithKeyValuesArray:data.response];
                SportsShareInstance.homeGameList = zhenData;
            });
            dispatch_semaphore_signal(sema);//1
        }];

        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.WinListArr = WinListArr;
            self.gameDataArr = LOTTERY_FACTORY.gameDataJson.mutableCopy;
            self.GameSTimerResult = [[NSMutableArray alloc] initWithArray:camp.gameSTimerResult];
            self.NowGameValueArr = [[NSMutableArray alloc] initWithArray:camp.nowGameValueArr];
            self.gameTimerS = [[NSMutableArray alloc] initWithArray:camp.gameTimerS];
            //重新计算
            [self calculateDateBybetween];
            self.SDImageArr = SportsShareInstance.BannerArrs;
            if (!SportsShareInstance.BannerArrs.count) {
                BET_CONFIG.config.lunboHeight = 0.0;
                [BET_CONFIG.config save];
            }
            self.datas = zhenData;
            if (LOTTERY_FACTORY.gameDataJson) {
                self.gameDataArr = [[NSMutableArray alloc] initWithArray:LOTTERY_FACTORY.gameDataJson];
            }else{
                self.gameDataArr = nil;
            }
            if (!self.gameDataArr.count || !((NSArray *)self.gameDataArr[0]).count) {
                self.homePageOneType = HomePageCreditType;
            }else{
                self.homePageOneType = (JesseAppdelegate.configValue == 1)?HomePageCreditType:HomePageOfficalType;
            }
            completed ? completed() : nil;
            NSLog(@"winRefreshing ----------首页全刷完毕----------");
            self.allRefreshing = NO;
        });
    });
}
#pragma mark - UIViewControllerTransitioningDelegate
-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    if ([dismissed isKindOfClass:[GuideViewController class]]) {
        return [DismissingAlphaAnimator new];
    }else{
        return [DismissingTopAnimator new];
    }
}
-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    if ([presented isKindOfClass:[GuideViewController class]]) {
        return [PresentingAlphaAnimator new];
    }else{
        return [PresentingTopAnimator new];
    }

}
#pragma mark - NoteViewControllerDelegate
- (void)notePushViewControllerWithHref:(NSString *)href
{
    [NAVI_MANAGER jumpToUrl:href];
//    if ([href hasPrefix:@"http"]) {
//        KFCPActivityController *vc = [[KFCPActivityController alloc] init];
//        [vc loadWebBy:href];
//        [self.navigationController pushViewController:vc animated:YES];
//    }else if ([href containsString:@"game"]) {
//        NSCharacterSet* nonDigits =[[NSCharacterSet decimalDigitCharacterSet] invertedSet];
//        int remainSecond =[[href stringByTrimmingCharactersInSet:nonDigits] intValue];
//        [self enterLotteryFactory:[NSString stringWithFormat:@"%d",remainSecond]];
//    }else{
//        KFCPActivityController *activit = [[KFCPActivityController alloc]init];
//        [activit loadhongBaoUrl:[NSString stringWithFormat:@"%@%@?_t=%@",SerVer_Url,ActivityhtmlUrl,[LukeUserAdapter getNowTimeTimestamp]]];
//        [self.navigationController pushViewController:activit animated:YES];
//    }
}
-(NoteViewController *)noteVC{
    if (!_noteVC) {
        _noteVC = [[NoteViewController alloc] init];
        _noteVC.transitioningDelegate = self;
        _noteVC.modalPresentationStyle = UIModalPresentationCustom;
        _noteVC.delegate = self;
    }
    return _noteVC;
}
@end

