//
//  CustomWebViewController.h
//  Bet365
//
//  Created by adnin on 2019/3/6.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSUInteger, CustomWebRequestOperation) {
    /** UIWebView*/
    CustomWebRequestOperation_WEB = 0,
    /** WKWebView*/
    CustomWebRequestOperation_WK,
};
typedef void(^CustomWebViewControllerDismissHandle)();
/**
 Tips
 CustomWebRequestOperation全走外部控制，CustomWebViewController不控制该变量值
 1、红包
 2、体育视讯
 3、web
 */
@interface CustomWebViewController : Bet365ViewController

@property (nonatomic,assign)BOOL isOpenSafari;

@property (nonatomic,readonly,getter=getParseUrl)NSString *parseURL;

@property (nonatomic,copy)CustomWebViewControllerDismissHandle dismissHandle;

-(void)loadUrl:(NSString *)urlStr Operation:(CustomWebRequestOperation)operation;

-(void)loadUrl:(NSString *)urlStr CookieParamers:(NSDictionary *)paramers Operation:(CustomWebRequestOperation)operation;

-(void)loadAttr:(MenuAttrTemplate *)attr Operation:(CustomWebRequestOperation)operation;

-(void)loadCode:(NSString *)code Type:(NSString *)type Kind:(NSString *)kind ValiCode:(NSString *)valiCode Operation:(CustomWebRequestOperation)operation;

-(void)loadCode:(NSString *)code Type:(NSString *)type Kind:(NSString *)kind Operation:(CustomWebRequestOperation)operation;

@end
