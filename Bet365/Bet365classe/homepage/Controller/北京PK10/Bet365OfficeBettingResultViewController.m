
//
//  Bet365OfficeBettingResultViewController.m
//  Bet365
//
//  Created by jesse on 2017/5/25.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "Bet365OfficeBettingResultViewController.h"
#import "Bet365OfficalBettingHeadView.h"
#import "Bet365OfficalofficalBettingDesView.h"
#import "Bet365BettingOfficalResultTableViewCell.h"
#import "Bet365OfficalResultModel.h"
#import "Bet365OfficalTraceViewController.h"
#import "AppDelegate.h"
#import "jesselagerBackview.h"
#import "JESSEKFCPAlgorithmManager.h"
#import "jesseLogBet365ViewController.h"
#import "Bet365TabbarViewController.h"
#import "KFCPHomeAlertView.h"
#import <PINCache.h>
@interface Bet365OfficeBettingResultViewController ()<UITableViewDelegate,UITableViewDataSource,HandBettingDelegate,returnCurrenAddOrReduceMoneydelegate,currenBettingWayDelegate,jesselagerAnimationDelegate>
@property(nonatomic,strong)Bet365OfficalBettingHeadView *headView;
@property(nonatomic,strong)Bet365OfficalofficalBettingDesView *desBettingView;
@property(nonatomic,strong)UITableView *bettingResTableView;
@property(nonatomic,strong)NSMutableArray *allDataArr;
@property(nonatomic,assign)double currentype;
@property(nonatomic,assign)LotteryType currenKindType;
@property(nonatomic,strong)KFCPHomeAlertView *lagerbackS;
@property(nonatomic,strong)NSDictionary *currenBettingParadic;
@property(nonatomic,assign)int r6count;
@end
static NSString *const registerOfficlResultCell = @"officalResultCell";
@implementation Bet365OfficeBettingResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
#pragma mark--setUI
-(void)setUI
{
    self.view.backgroundColor = [UIColor whiteColor];
    [self headView];
    [self desBettingView];
}
#pragma mark--手动投注
-(void)handBetting
{
    [self.delegate returnCurrenBettingArr:self.allDataArr];//传递最新投注类容
    //销毁当前控制器
    [self dismissViewControllerAnimated:YES completion:nil];
}
/**返回上级控制器**/
-(void)comeBack{
    [SVProgressHUD dismiss];
    [self.delegate returnCurrenBettingArr:self.allDataArr];
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark-- 机选投注<随机>
-(void)arcBetting
{
    
    Bet365OfficalResultModel *lastModel = self.allDataArr[self.allDataArr.count-1];
    if (tapliDic()[lastModel.code][@"format"]==nil) {
        [JesseAppdelegate showErrorOnlyText:@"该玩法暂不支持随机投注!" AndDelay:1.5f];
        return;
    }
    Bet365OfficalResultModel *randModel = [[Bet365OfficalResultModel alloc]init];
    randModel.unitype = lastModel.unitype;
    randModel.mulite = 1;
    randModel.rebet = lastModel.rebet;
    randModel.currenOdds = lastModel.currenOdds;
    randModel.currenBettingNumber = 1.0;
    randModel.code = lastModel.code;
    CGFloat totalM = 2*randModel.currenBettingNumber;
    if (randModel.unitype==UnitJiaoType) {
        totalM=totalM/10;
    }else if (randModel.unitype==UnitFenType){
        totalM = totalM/100;
    }
    randModel.totalBettingMoney = totalM;
    randModel.profitMoney = [randModel.currenOdds[0] floatValue]*randModel.mulite-randModel.totalBettingMoney;
    NSString *currenNumberValue;
    if (self.currentype == (double)911101101101||self.currentype==(double)911101102102||self.currentype == (double)910101101101||self.currentype==(double)910101102102) {
        currenNumberValue = number();
        randModel.showContent = number();
    }else if(self.currentype==(double)911101101102||self.currentype==(double)911101102103||self.currentype==(double)910101101102||self.currentype==(double)910101102103){
        currenNumberValue = UnitaryNumber();
        randModel.showContent = UnitaryNumber();
    }else if (self.currentype==(double)911101101103||self.currentype==(double)911101102101||self.currentype==(double)910101101103||self.currentype==(double)910101102101){
        [JesseAppdelegate showErrorOnlyText:@"该玩法暂不支持随机投注!" AndDelay:1.5f];
        return;
    }else{
        __block NSString *contenSN;
        [[JESSEKFCPAlgorithmManager shareCurrenManager]makeRandmNumberBy:tapliDic()[[NSString stringWithFormat:@"%@",lastModel.code]] And:tapliDic()[@"numberLen"] ByContent:^(id  contenS) {
            if ([contenS isKindOfClass:[NSArray class]]) {
                contenSN = ((NSArray *)contenS)[1];
                randModel.currenBettingNumber = [((NSArray *)contenS)[0] integerValue];
                CGFloat totalM = 2*randModel.currenBettingNumber;
                if (randModel.unitype==UnitJiaoType) {
                    totalM=totalM/10;
                }else if (randModel.unitype==UnitFenType){
                    totalM = totalM/100;
                }
                randModel.totalBettingMoney = totalM;
                randModel.profitMoney = [randModel.currenOdds[0] floatValue]*randModel.mulite-randModel.totalBettingMoney;
            }else{
                contenSN = contenS;
            }
        }];
        currenNumberValue = contenSN;
        randModel.showContent = contenSN;
    }
    randModel.NumberValue = currenNumberValue;
    randModel.name = lastModel.name;
    NSDictionary *randomDic = tapliDic()[[NSString stringWithFormat:@"%@",lastModel.code]];
    if ((randomDic[@"algorithm"]&&[[NSString stringWithFormat:@"%@",randomDic[@"algorithm"][0]]isEqualToString:@"R7"])||((randomDic[@"algorithm"]&&[[NSString stringWithFormat:@"%@",randomDic[@"tpl"]]isEqualToString:@"danshi2"])||[randomDic[@"tpl"]isEqualToString:@"renxuan1"])||([[NSString stringWithFormat:@"%@",randomDic[@"algorithm"][0]]isEqualToString:@"R6"]&&[randomDic[@"name"] rangeOfString:@"定位胆"].length>0)) {
        [[JESSEKFCPAlgorithmManager shareCurrenManager]makeSpecalRandmNumberBy:tapliDic()[[NSString stringWithFormat:@"%@",lastModel.code]] And:lastModel And:tapliDic()[@"numberLen"] returnContenModel:^(Bet365OfficalResultModel *randModel) {
            if ([[NSString stringWithFormat:@"%@",randomDic[@"algorithm"][0]]isEqualToString:@"R7"]||[randomDic[@"tpl"]isEqualToString:@"danshi2"]||[randomDic[@"tpl"]isEqualToString:@"hezhi2"]||[randomDic[@"tpl"]isEqualToString:@"renxuan1"]) {
                
            }else{
                randModel.profitMoney = [randModel.currenOdds[0] floatValue]*3-randModel.totalBettingMoney;
           
            }
            [self.allDataArr addObject:randModel];
        }];
    }else{
        [self.allDataArr addObject:randModel];
        
    }
 
    ALGORIGH_MANAGER.R6BetposCount = self.r6count;
    [self.desBettingView updatekeyBordDesBy:self.allDataArr];
    [self.bettingResTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    
}
NSString *Currtype(){
    AppDelegate *app = (id)[UIApplication sharedApplication].delegate;
    return app.currenType;
}
#pragma mark--返回对应玩法算法框架
static inline NSDictionary *tapliDic(){
    AppDelegate *app = (id)[UIApplication sharedApplication].delegate;
    return app.tplConfig;
}
#pragma mark--返回随机对应值
static inline NSString *number(){
    
    int numberOne = rand()%10;
    int numberTwo = rand()%10;
    int numberThree = rand()%10;
    NSString *res = [NSString stringWithFormat:@"%d|%d|%d",numberOne,numberTwo,numberThree];
    return res;
}
#pragma mark--返回单式类型随机值
static inline NSString *UnitaryNumber(){
    int numberOne = rand()%10;
    int numberTwo = rand()%10;
    int numberThree = rand()%10;
    NSString *res = [NSString stringWithFormat:@"%d,%d,%d",numberOne,numberTwo,numberThree];
    return res;
}
#pragma mark--数据触发修改
-(void)updateCurren:(NSArray *)dataArr AndCurrenType:(double)currenType AndKindType:(LotteryType)kindType
{
    self.r6count = ALGORIGH_MANAGER.R6BetposCount;
    self.currentype = currenType;
    self.currenKindType = kindType;
    [self.desBettingView updatekeyBordDesBy:dataArr];
    [self.allDataArr removeAllObjects];
    self.allDataArr = [[NSMutableArray alloc]initWithArray:dataArr];
    [self bettingResTableView];
}
#pragma mark--增减修改
-(void)returnCurrenMulite:(NSInteger)currenMulite AndCurrenCurrenMoney:(CGFloat)totalMoney AndcurrenTag:(NSInteger)currentag
{
    Bet365OfficalResultModel *model = self.allDataArr[currentag-10];
    model.mulite = currenMulite;
    model.totalBettingMoney = totalMoney;    
    model.profitMoney = model.mulite*[model.currenOdds[0] floatValue]-model.totalBettingMoney;
    [self.allDataArr replaceObjectAtIndex:currentag-10 withObject:model];
    ALGORIGH_MANAGER.R6BetposCount = self.r6count;
    [self.desBettingView updatekeyBordDesBy:self.allDataArr];
}
#pragma mark--删除代理
-(void)deteCurrenResultBy:(NSInteger)CurrenTag
{
    [self.allDataArr removeObjectAtIndex:CurrenTag-10];
    if (self.allDataArr.count==0) {
        [self.delegate returnCurrenBettingArr:self.allDataArr];
        //销毁当前控制器
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    [self.bettingResTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.desBettingView updatekeyBordDesBy:self.allDataArr];
}
#pragma mark--确认下注
-(void)sureBetting
{
    __block NSArray *allPara;
    __block NSString *totalMoney;
    __block NSInteger TotalBetNumberOffical;
    __block NSInteger TotalNumberOffical;
    [self returnBettingResult:^(NSArray *ParameterArr, NSString *totalM, NSInteger TotalBetNumber, NSInteger TotalNumber) {
        allPara = ParameterArr;
        totalMoney = totalM;
        TotalBetNumberOffical = TotalBetNumber;
        TotalNumberOffical = TotalNumber;
    }];
    NSString *turnNumber = [customLoadingTool initCustomManager].turnNumber;
    NSMutableDictionary *currenParameterDic = [[NSMutableDictionary alloc]init];
    currenParameterDic[@"content"] = allPara;
    currenParameterDic[@"gameId"] = BET_CONFIG.lottery_id;
    currenParameterDic[@"totalMoney"] = totalMoney;
    currenParameterDic[@"turnNum"] = turnNumber;
    self.currenBettingParadic=currenParameterDic;
    [self.lagerbackS updateDesAlertView:([BET_CONFIG.config.hy_is_cancel isEqualToString:@"1"])?@"确定投注吗?":@"请注意:投注成功后不可撤单!" And:[NSString stringWithFormat:@"第【%@】期\n总共%ld笔%ld单,合计%@元",turnNumber,TotalBetNumberOffical,TotalNumberOffical,totalMoney] AndIsBet:YES];
}
#pragma mark--LagerBackDelegate
-(void)deleteBackview
{
    [self.lagerbackS removeFromSuperview];
    self.lagerbackS = nil;
}
-(void)sureTraceBetting
{
    [SVProgressHUD showWithStatus:@"下注中..."];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [[manager operationQueue] cancelAllOperations];
    manager.responseSerializer.acceptableContentTypes = [self returnCurrenResponType];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"application/json;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 5.f;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
        [manager POST:OfficalBettingUrl parameters:self.currenBettingParadic progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                [self.allDataArr removeAllObjects];
                [self.lagerbackS removeFromSuperview];
                self.lagerbackS = nil;
                [self.delegate returnCurrenBettingArr:self.allDataArr];
                [USER_DATA_MANAGER requestUserStatusCompleted:^(BOOL success) {
                }];
                [self dismissViewControllerAnimated:YES completion:^{
                    [JesseAppdelegate showImageLoad:@"successFul" AnddismissDelay:0.5f AndDesText:@"下注成功"];
                }];
            });
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           [SVProgressHUD dismiss];
            if (!error.userInfo[@"com.alamofire.serialization.response.error.data"]) {
                [JesseAppdelegate showImageLoad:@"error" AnddismissDelay:1.5 AndDesText:@"网络连接超时,请重试!"];
                [self dismissViewControllerAnimated:NO completion:nil];
                [self.lagerbackS removeFromSuperview];
                self.lagerbackS = nil;
                return ;
            }
            NSData *data = error.userInfo[@"com.alamofire.serialization.response.error.data"];
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            if ([dic[@"code"] isKindOfClass:[NSString class]]) {
                if ([dic[@"code"] isEqualToString:@"BET/BET_ODDS_ERROR"]) {
                    [JesseAppdelegate showErrorOnlyText:@"赔率发生变化,请再次进入!" AndDelay:2.0f];
                    [[PINCache sharedCache] removeObjectForKey:BET_CONFIG.lottery_id];
                    [[PINCache sharedCache] removeObjectForKey:[NSString stringWithFormat:@"official%@",BET_CONFIG.lottery_id]];
                    [[PINCache sharedCache] removeObjectForKey:[NSString stringWithFormat:@"config%@",BET_CONFIG.lottery_id]];
                    [self dismissViewControllerAnimated:YES completion:^{
                        [self.delegate dismissBetController];
                    }];
                }else{
                    if ([dic[@"msg"] isEqualToString:@"奖金值不正确"]) {
                        [JesseAppdelegate showErrorOnlyText:@"赔率发生变化,请再次进入!" AndDelay:2.0f];
                        [[PINCache sharedCache] removeObjectForKey:BET_CONFIG.lottery_id];
                        [[PINCache sharedCache] removeObjectForKey:[NSString stringWithFormat:@"official%@",BET_CONFIG.lottery_id]];
                        [[PINCache sharedCache] removeObjectForKey:[NSString stringWithFormat:@"config%@",BET_CONFIG.lottery_id]];
                        [self dismissViewControllerAnimated:YES completion:^{
                            [self.delegate dismissBetController];
                        }];
                    }else{
                        [JesseAppdelegate showErrorOnlyText:dic[@"msg"] AndDelay:1.5f];
                    }
                }
            }else{
                if ([dic[@"msg"] isEqualToString:@"奖金值不正确"]) {
                    [JesseAppdelegate showErrorOnlyText:@"赔率发生变化,请再次进入!" AndDelay:2.0f];
                    [[PINCache sharedCache] removeObjectForKey:BET_CONFIG.lottery_id];
                    [[PINCache sharedCache] removeObjectForKey:[NSString stringWithFormat:@"official%@",BET_CONFIG.lottery_id]];
                    [[PINCache sharedCache] removeObjectForKey:[NSString stringWithFormat:@"config%@",BET_CONFIG.lottery_id]];
                    [self dismissViewControllerAnimated:YES completion:^{
                        [self.delegate dismissBetController];
                    }];
                    
                }else{
                    [JesseAppdelegate showErrorOnlyText:dic[@"msg"] AndDelay:1.5f];
                    [self dismissViewControllerAnimated:NO completion:nil];
                }
                
            }
            if (dic[@"code"]) {

            }
            [self.lagerbackS removeFromSuperview];
            self.lagerbackS = nil;
        }];
}

#pragma mark--返回当前网络请求类型
-(NSSet *)returnCurrenResponType
{
    return [NSSet setWithObjects:@"text/html",@"text/plain",@"application/json",@"text/josn",@"text/javascript", nil];
}
#pragma mark--下注整理
-(void)returnBettingResult:(void(^)(NSArray *ParameterArr,NSString *totalM,NSInteger TotalBetNumber,NSInteger TotalNumber))parameterBlock
{
    __block CGFloat totalM = 0.0;
    __block NSInteger TotalBetNumber = 0;
    __block NSInteger TotalNumber = 0;
    NSDecimalNumberHandler *hander = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain scale:4 raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    NSMutableArray *AllData = [[NSMutableArray alloc]init];
    [self.allDataArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        Bet365OfficalResultModel *model = obj;
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        NSString *betmodel = @"2";
        if (model.unitype == UnitJiaoType) {
            betmodel = @"0.2";
        }else if (model.unitype==UnitFenType){
            betmodel = @"0.02";
        }
        NSDecimalNumber *profit = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.5f",model.profitMoney]];
        NSDecimalNumber *profitResult = [profit decimalNumberByRoundingAccordingToBehavior:hander];
        NSDecimalNumber *betingMoney = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.5f",model.totalBettingMoney]];
        NSDecimalNumber *ResbettMoney = [betingMoney decimalNumberByRoundingAccordingToBehavior:hander];
        dic[@"betInfo"] = model.NumberValue;
        dic[@"betModel"] = [NSString stringWithFormat:@"%d",model.unitype];
        dic[@"money"] = betmodel;
        dic[@"canWinMoney"] = [NSString stringWithFormat:@"%@",profitResult];
        dic[@"cateName"] = model.name;
        dic[@"poschoose"] = model.poschoose;
        dic[@"poschooseName"] = model.poschooseName;
        dic[@"code"] = model.code;
        dic[@"totalMoney"] = [NSString stringWithFormat:@"%@",ResbettMoney];
        dic[@"multiple"] = [NSString stringWithFormat:@"%ld",model.mulite];
        NSMutableString *currenOdds = [[NSMutableString alloc]init];
        [model.currenOdds enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDecimalNumberHandler *hander = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain scale:4 raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
                NSDecimalNumber *currenOddsValue = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.5f",[obj doubleValue]]];
                NSDecimalNumber *resultOdds = [currenOddsValue decimalNumberByRoundingAccordingToBehavior:hander];
                [currenOdds appendFormat:@"%@,",resultOdds];
        }];
        
        [currenOdds deleteCharactersInRange:NSMakeRange(currenOdds.length-1, 1)];
        
        NSDictionary *CurrenDic = tapliDic()[[NSString stringWithFormat:@"%@",model.code]];
        if ([[NSString stringWithFormat:@"%@",CurrenDic[@"algorithm"][0]] isEqualToString:@"R10"]||([[NSString stringWithFormat:@"%@",CurrenDic[@"algorithm"][0]] isEqualToString:@"R11"]&&[CurrenDic[@"tpl"]isEqualToString:@"num7"])) {
            NSDecimalNumberHandler *R10hander = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain scale:4 raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
            NSDecimalNumber *R10currenOddsValue = [NSDecimalNumber decimalNumberWithString:@""];
            R10currenOddsValue = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.5f",[model.currenOdds[model.OddsIndex] doubleValue]]];
            NSDecimalNumber *R10resultOdds = [R10currenOddsValue decimalNumberByRoundingAccordingToBehavior:R10hander];
            dic[@"odds"] = [NSString stringWithFormat:@"%@",R10resultOdds];
        }else{
            if (model.currenOdds.count>1) {
                dic[@"odds"] = [NSString stringWithFormat:@"%@",currenOdds];
            }else{
                NSDecimalNumber *currenNodds = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",currenOdds]];
                dic[@"odds"] = [NSString stringWithFormat:@"%@",[currenNodds decimalNumberByRoundingAccordingToBehavior:hander]];
            }
        }
        
        dic[@"rebate"] = [NSString stringWithFormat:@"%.2f",model.rebet];
        dic[@"showContent"] = model.NumberValue;
        dic[@"totalNums"] = [NSString stringWithFormat:@"%.0f",model.currenBettingNumber];
        totalM += model.totalBettingMoney;
        TotalBetNumber+=model.currenBettingNumber;
        TotalNumber++;
        [AllData addObject:dic];
    }];
    NSDecimalNumber *TotalMoney = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.5f",totalM]];
    NSDecimalNumber *totalMoneyResult = [TotalMoney decimalNumberByRoundingAccordingToBehavior:hander];
    parameterBlock(AllData,[NSString stringWithFormat:@"%@",totalMoneyResult],TotalBetNumber,TotalNumber);
}
#pragma mark--确认追号
-(void)sureTrace
{
    __weak typeof(self)weakself = self;
    Bet365OfficalTraceViewController *TraceC = [[Bet365OfficalTraceViewController alloc]init];
    [TraceC setBlock:^(){
        //金额刷新
        [USER_DATA_MANAGER requestUserStatusCompleted:^(BOOL success) {
        }];
        [weakself.allDataArr removeAllObjects];
        [weakself.delegate returnCurrenBettingArr:self.allDataArr];
    }];
    [TraceC setPopRoot:^{
        [self dismissViewControllerAnimated:YES completion:^{
            [weakself.delegate dismissBetController];
        }];
    }];
    [TraceC updateCurrenTrceDataArr:self.allDataArr And:self.currenKindType];
    [self presentViewController:TraceC animated:YES completion:nil];
}
#pragma mark--tableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.allDataArr.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Bet365OfficalResultModel *model = self.allDataArr[indexPath.row];
    CGRect bound = [model.NumberValue boundingRectWithSize:CGSizeMake(WIDTH-60, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesDeviceMetrics attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17]} context:nil];
    return 80+bound.size.height;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Bet365BettingOfficalResultTableViewCell *desCell = [tableView dequeueReusableCellWithIdentifier:registerOfficlResultCell forIndexPath:indexPath];
    desCell.tag = 10+indexPath.row;
    [desCell updateContenViewBy:self.allDataArr[indexPath.row]];
    desCell.delegate = self;
    return desCell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
#pragma mark--初始化Head
-(Bet365OfficalBettingHeadView *)headView
{
    if (_headView==nil){
        _headView = [[Bet365OfficalBettingHeadView alloc]initWithFrame:CGRectMake(0, NAVGATIONSTATEBARHEIGHT, WIDTH, 90)];
        _headView.delegate = self;
        [self.view addSubview:_headView];
    }
    return _headView;
}
#pragma mark--初始化投注Des
-(Bet365OfficalofficalBettingDesView *)desBettingView
{
    if (_desBettingView==nil) {
        CGFloat distance = TABBARBOTTOMDISTANCE;
        _desBettingView = [[Bet365OfficalofficalBettingDesView alloc]initWithFrame:CGRectMake(0, HEIGHT-(40+distance), WIDTH, 40+distance)];
        _desBettingView.delegate =self;
        [self.view addSubview:_desBettingView];
    }
    return _desBettingView;
}
#pragma mark--初始化tab
-(UITableView *)bettingResTableView
{
    if (_bettingResTableView==nil) {
        CGFloat distance = TABBARBOTTOMDISTANCE;
        CGFloat distanceY = NAVGATIONSTATEBARHEIGHT;
        CGFloat distanceChange = distanceY+90;
        _bettingResTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, distanceChange, WIDTH, HEIGHT-distanceChange-(40+distance)) style:UITableViewStylePlain];
        _bettingResTableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _bettingResTableView.showsVerticalScrollIndicator = YES;
        _bettingResTableView.showsHorizontalScrollIndicator = NO;
        _bettingResTableView.bounces = NO;
        _bettingResTableView.allowsSelection = NO;
        _bettingResTableView.dataSource =self;
        _bettingResTableView.delegate = self;
        _bettingResTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:_bettingResTableView];
        [_bettingResTableView registerClass:[Bet365BettingOfficalResultTableViewCell class] forCellReuseIdentifier:registerOfficlResultCell];
    }
    return _bettingResTableView;
}
#pragma mark--初始化当前追号背景
-(KFCPHomeAlertView *)lagerbackS
{
    if (_lagerbackS==nil) {
        _lagerbackS = [[KFCPHomeAlertView alloc]init];
        _lagerbackS.frame = CGRectMake(0, 0, WIDTH, HEIGHT);
        _lagerbackS.delegate = self;
        [_lagerbackS setRemove:^(){
            
        }];
        [[UIApplication sharedApplication].keyWindow addSubview:_lagerbackS];
    }
    return _lagerbackS;
}

@end

