//
//  CustomWebViewController.m
//  Bet365
//
//  Created by adnin on 2019/3/6.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "CustomWebViewController.h"
#import <WebKit/WebKit.h>
#import "UIViewControllerSerializing.h"
#import "NetWorkMannager+electronic.h"
#import "Bet365FootBallViewController.h"
#import "NetWorkMannager+activity.h"

@interface CustomWebViewController ()
<WKNavigationDelegate,
WKUIDelegate,
UIWebViewDelegate,
UIViewControllerSerializing>

@property (nonatomic,strong)UIWebView *webView;

@property (nonatomic,strong)WKWebView *wkWebView;

@property (nonatomic,strong)NSURLRequest *request;

@property (nonatomic,strong)UILabel *errorLabel;

@property (nonatomic,strong)NSString *code;

@property (nonatomic,strong)NSString *type;

@property (nonatomic,strong)NSString *kind;

@property (nonatomic,strong)NSString *urlString;

@end

@implementation CustomWebViewController

RouterKey *const kRouterYHHD = @"yhhd";

RouterKey *const kRouterHTTPWEB = @"webRequest";

RouterKey *const kRouterAgentDeclare = @"agent/declare";

RouterKey *const kRouterSportsGame = @"sports/game";

RouterKey *const kRouterZXKF = @"zxkf";

RouterKey *const kRouterLiveRedirect = @"liveRedirect.html";

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    if (!parameters) {
        return YES;
    }
    if ([parameters allKeys].count == 0) {
        return YES;
    }
//    NSString *type = parameters[@"type"];
    NSString *code = parameters[@"code"];
//    NSString *kind = parameters[@"kind"];
    NSString *url = parameters[@"url"];
    NSString *routerKey = parameters[@"routerKey"];
    if ([code isEqualToString:@"ft"] ||
        [code isEqualToString:@"bk"]){
        return YES;
    }else if ([url hasPrefix:@"http"]) {
        return YES;
    }else if (code && code.length > 0) {
        return YES;
    }else if (routerKey && [routerKey isEqualToString:@"yhhd"]){
        return YES;
    }
    return [USER_DATA_MANAGER shouldPush2Login];
}

+(void)load{
    [self share];
    [self jump];
}

+(void)jump{
    
    
    [self registerJumpRouterKey:kRouterZXKF toHandle:^(NSDictionary *parameters) {
        CustomWebViewController *vc = [[CustomWebViewController alloc] initWithNibName:@"CustomWebViewController" bundle:nil];
        [vc loadUrl:BET_CONFIG.common_config.zxkfPath Operation:CustomWebRequestOperation_WEB];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }];
    
    [self registerJumpRouterKey:kRouterSportsGame toHandle:^(NSDictionary *parameters) {
        NSString *type = parameters[@"type"];
        NSString *code = parameters[@"code"];
        NSString *kind = parameters[@"kind"];
        if ([code isEqualToString:@"ft"] ||
            [code isEqualToString:@"bk"]){
            Bet365FootBallViewController *vc = [[Bet365FootBallViewController alloc] init];
            vc.sportType = code;
            [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
            vc.hidesBottomBarWhenPushed  = YES;
        }else if ([[NAVI_MANAGER getCurrentVC] testPlayForGameType:type liveCode:code gameKind:kind]){
            if ([USER_DATA_MANAGER shouldPush2Login]) {
                CustomWebViewController *vc = [[CustomWebViewController alloc] initWithNibName:@"CustomWebViewController" bundle:nil];
                [vc loadCode:code Type:type Kind:kind Operation:CustomWebRequestOperation_WK];
                [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
                vc.hidesBottomBarWhenPushed = YES;
                vc.isOpenSafari = YES;
            }
        }
    }];
    
    [self registerJumpRouterKey:kRouterYHHD toHandle:^(NSDictionary *parameters) {
        [self routerShareInstanceByUrl:kRouterYHHD toHandle:^(UIViewController *vc) {
            if (vc) {
                [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
                vc.hidesBottomBarWhenPushed = YES;
            }
        }];
    }];
    
    [self registerJumpRouterKey:kRouterHTTPWEB toHandle:^(NSDictionary *parameters) {
        
        NSString *url = parameters[@"url"];
        NSString * qqUrl = nil;
        NSString * qqUrlPrefix = @"http://wpa.qq.com/msgrd?";
        NSString * qqNumberKeyStr = @"uin=";
        if([url hasPrefix:qqUrlPrefix] && [url containsString:qqNumberKeyStr]){
            NSArray * strArr = [[url stringByReplacingOccurrencesOfString:qqUrlPrefix withString:@""] componentsSeparatedByString:@"&"];
            if(strArr && strArr.count){
                for(NSString * str in strArr){
                    if([str hasPrefix:qqNumberKeyStr]){
                        NSString * qqNumber = [str stringByReplacingOccurrencesOfString:qqNumberKeyStr withString:@""];
                        if ([qqNumber isNoBlankString]) {
                            qqUrl = [NSString stringWithFormat:@"mqq://im/chat?chat_type=wpa&version=1&src_type=web&uin=%@", qqNumber];
                            break;
                        }
                    }
                }
            }
        }
        if([qqUrl isNoBlankString]){
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:qqUrl]];
        }else{
            CustomWebViewController *vc = [[CustomWebViewController alloc] initWithNibName:@"CustomWebViewController" bundle:nil];
            [vc loadUrl:url Operation:CustomWebRequestOperation_WEB];
            vc.hidesBottomBarWhenPushed = YES;
            [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        }
    }];
    
    [self registerJumpRouterKey:kRouterAgentDeclare toHandle:^(NSDictionary *parameters) {
        CustomWebViewController *vc = [[CustomWebViewController alloc] initWithNibName:@"CustomWebViewController" bundle:nil];
        [vc loadUrl:StringFormatWithStr(SerVer_Url, @"/views/app_agendDelaer.html") Operation:CustomWebRequestOperation_WEB];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }];
    
    [self registerJumpRouterKey:kRouterLiveRedirect toHandle:^(NSDictionary *parameters) {
        NSString *type = parameters[@"type"];
        NSString *code = parameters[@"code"];
        NSString *kind = parameters[@"kind"];
        if ([code isEqualToString:@"ft"] ||
            [code isEqualToString:@"bk"]){
            Bet365FootBallViewController *vc = [[Bet365FootBallViewController alloc] init];
            vc.sportType = code;
            [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
            vc.hidesBottomBarWhenPushed  = YES;
        }else if ([[NAVI_MANAGER getCurrentVC] testPlayForGameType:type liveCode:code gameKind:kind]){
            if ([USER_DATA_MANAGER shouldPush2Login]) {
                CustomWebViewController *vc = [[CustomWebViewController alloc] initWithNibName:@"CustomWebViewController" bundle:nil];
                [vc loadCode:code Type:type Kind:kind Operation:CustomWebRequestOperation_WK];
                [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
                vc.hidesBottomBarWhenPushed = YES;
                vc.isOpenSafari = YES;
            }
        }
    }];
}

+(void)share{
    [self registerInstanceRouterKey:kRouterYHHD toHandle:^UIViewController *(NSDictionary *parameters) {
        CustomWebViewController *vc = [[CustomWebViewController alloc] initWithNibName:@"CustomWebViewController" bundle:nil];
        [vc loadUrl:[NSString stringWithFormat:@"%@%@",SerVer_Url,ActivityhtmlUrl] Operation:CustomWebRequestOperation_WEB];
        return vc;
    }];
    
    [self registerInstanceRouterKey:kRouterHTTPWEB toHandle:^UIViewController *(NSDictionary *parameters) {
        NSString *url = parameters[@"url"];
        CustomWebViewController *vc = [[CustomWebViewController alloc] initWithNibName:@"CustomWebViewController" bundle:nil];
        [vc loadUrl:url Operation:CustomWebRequestOperation_WEB];
        return vc;
    }];
    
    [self registerInstanceRouterKey:kRouterAgentDeclare toHandle:^UIViewController *(NSDictionary *parameters) {
        CustomWebViewController *vc = [[CustomWebViewController alloc] initWithNibName:@"CustomWebViewController" bundle:nil];
        [vc loadUrl:StringFormatWithStr(SerVer_Url, @"/views/app_agendDelaer.html") Operation:CustomWebRequestOperation_WEB];
        return vc;
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.naviRightItem = Bet365NaviRightItemNone;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    if (self.isOpenSafari) {
        [self createRightBarItemWithTitle:@"跳转Safari" action:@selector(openSafari)];
    }
    [self updateBackItem];
    [self addObserver];
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    if (self.code.length) {
        if (([USER_DATA_MANAGER.userInfoData.transferStatus integerValue] == 1 || [BET_CONFIG.config.user_transfer_stauts isEqualToString:@"1"]) && USER_DATA_MANAGER.isLogin) {
            [NET_DATA_MANAGER requestGetfreeTransfer:self.code success:^(id responseObject) {
                
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
            }];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    JesseAppdelegate.allowRotation = YES;
    [SVProgressHUD showWithStatus:@"加载中..."];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    JesseAppdelegate.allowRotation = NO;
    [SVProgressHUD dismiss];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    NSNumber *resetOrientationTarget = [NSNumber numberWithInt:UIInterfaceOrientationUnknown];
    [[UIDevice currentDevice] setValue:resetOrientationTarget forKey:@"orientation"];
    NSNumber *orientationTarget = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:orientationTarget forKey:@"orientation"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

-(void)updateBackItem{
    BOOL canGoBack = NO;
    if (_wkWebView) {
        canGoBack = [self.wkWebView canGoBack];
    }else if (_webView){
        canGoBack = [self.webView canGoBack];
    }
    
    if (canGoBack) {
        UIImage *close = [UIImage imageNamed:@ "chat_close_icon" ];
        close = [close imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        UIBarButtonItem *closeItem = [[UIBarButtonItem alloc] initWithImage:close style:UIBarButtonItemStylePlain target:self action:@selector(close:)];
        
        UIImage *back = [UIImage imageNamed:@ "left_lage"];
        back = [back imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithImage:back style:UIBarButtonItemStylePlain target:self action:@selector(back:)];
        self.navigationItem.leftBarButtonItems = @[backItem,closeItem];
    
    }else{
        self.navigationController.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(back:)];
    }
}
-(void)addObserver{
    [[RACObserve(USER_DATA_MANAGER, isLogin) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        if (self.request) {
            if (_webView) {
                [_webView loadRequest:self.request];
            }else if (_wkWebView){
                [_wkWebView loadRequest:self.request];
            }
        }
    }];
}

-(void)loadRequest:(NSURLRequest *)request Operation:(CustomWebRequestOperation)operation{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.request = request;
        if (operation == CustomWebRequestOperation_WEB) {
            [self.webView loadRequest:request];
        }else if(operation == CustomWebRequestOperation_WK){
            [self.wkWebView loadRequest:request];
        }
    });
}

#pragma mark - Public
-(void)loadUrl:(NSString *)urlStr Operation:(CustomWebRequestOperation)operation{
    self.urlString = urlStr;
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]];
    [self loadRequest:request Operation:operation];
}

-(void)loadUrl:(NSString *)urlStr CookieParamers:(NSDictionary *)paramers Operation:(CustomWebRequestOperation)operation{
    self.urlString = urlStr;
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.urlString]];
    [paramers.allKeys enumerateObjectsUsingBlock:^(NSString * _Nonnull key, NSUInteger idx, BOOL * _Nonnull stop) {
        [request addValue:paramers[key] forHTTPHeaderField:key];
    }];
    [self loadRequest:request Operation:operation];
}

-(void)loadCode:(NSString *)code Type:(NSString *)type Kind:(NSString *)kind Operation:(CustomWebRequestOperation)operation{
    
    self.type = type;
    self.code = code;
    self.kind = kind;
    
    [self queryUrlOperation:operation];
}

-(void)loadCode:(NSString *)code Type:(NSString *)type Kind:(NSString *)kind ValiCode:(NSString *)valiCode Operation:(CustomWebRequestOperation)operation{
    
    self.type = type;
    self.code = code;
    self.kind = kind;
    
    //TODO 是否是根据试玩走不同接口？VHY咋办？
    if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"TEST"] || [USER_DATA_MANAGER.userInfoData.type isEqualToString:@"VHY"]) {
        if (!valiCode.length) {
            [SVProgressHUD showErrorWithStatus:@"验证码未填写"];
            return;
        }
        [self testPlayDetailWithValiCode:valiCode QueryOperation:operation];
    }else{
        [self testPlayDetailWithValiCode:valiCode QueryOperation:operation];
    }
}


-(void)loadAttr:(MenuAttrTemplate *)attr Operation:(CustomWebRequestOperation)operation{
    [self loadCode:attr.liveCode Type:attr.gameType Kind:attr.gameKind Operation:operation];
}

#pragma mark - WKNavigationDelegate
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    NSString *url = navigationAction.request.URL.absoluteString;
    if ([url hasSuffix:@"plist"] || [url containsString:@"canopen"]) {
        [[UIApplication sharedApplication] openURL:navigationAction.request.URL];
    }
    if (navigationAction.targetFrame == nil) {
        [webView loadRequest:navigationAction.request];
    }
    [url stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
    if ([url containsString:@"/Home/Detail"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
    decisionHandler(WKNavigationActionPolicyAllow);
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation{
    [SVProgressHUD dismissWithCompletion:nil];
    [self updateBackItem];
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    [SVProgressHUD dismissWithCompletion:nil];
    [self updateBackItem];
    NSLog(@"%@",error);
}
#pragma mark - UIWebViewDelegate
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSString *obs = request.URL.absoluteString;
    if ([obs hasPrefix:@"alipays://"] || [obs hasPrefix:@"alipay://"]||[obs hasPrefix:@"mqqapi://"]||[obs hasPrefix:@"weixin://"]) {
        BOOL bSucc = [[UIApplication sharedApplication]openURL:request.URL];
        if (!bSucc) {
            // 未安装支付宝 进入网页支付
        }
        return NO;
    }
    if ([obs hasSuffix:kRouterChatIndex]) {
        [UIViewController routerJumpToUrl:kRouterChatIndex];
        return NO;
    }
    if ([obs containsString:@"#/"] &&
        [obs containsString:SerVer_Url]) {
        RouterKey *router = [[obs componentsSeparatedByString:@"#/"] safeObjectAtIndex:1];
        [UIViewController routerJumpToUrl:router];
        return NO;
    }
    return YES;
}


-(void)webViewDidFinishLoad:(UIWebView *)webView{
    // NSString *string = @"document.documentElement.innerHTML";
    NSString *replace  =  @"document.getElementsByClassName('vux-header')[0].remove();";
    [webView stringByEvaluatingJavaScriptFromString:replace];
    [self updateBackItem];
    [SVProgressHUD dismiss];
    
}

#pragma mark - Events
- (void)openSafari
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.urlString]];
}

-(void)close:(UIBarButtonItem *)sender{
    [self.navigationController popViewControllerAnimated:YES];
    self.dismissHandle ? self.dismissHandle() : nil;
}

-(void)back:(UIBarButtonItem *)sender{
    if (_wkWebView) {
        [self.wkWebView goBack];
    }
    if (_webView) {
        [self.webView goBack];
    }
}



#pragma mark - GET/SET
-(UIWebView *)webView{
    if (!_webView) {
        _webView = [[UIWebView alloc]init];
        _webView.scalesPageToFit = YES;
        _webView.delegate = self;
        _webView.mediaPlaybackRequiresUserAction = NO;
        [self.view addSubview:_webView];
        [_webView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.mas_equalTo(self.view);
        }];
    }
    return _webView;
}

-(WKWebView *)wkWebView{
    if (!_wkWebView) {
        WKWebViewConfiguration *conf = [[WKWebViewConfiguration alloc] init];
        /*
         * WKWebViewConfiguration mediaTypesRequiringUserActionForPlayback 版本需要大于ios10
         */
        if (@available(iOS 10.0,*)){
            conf.requiresUserActionForMediaPlayback = NO;
            conf.allowsAirPlayForMediaPlayback = YES;
            conf.mediaTypesRequiringUserActionForPlayback = NO;
        }
        conf.allowsInlineMediaPlayback = YES;
        _wkWebView = [[WKWebView alloc]initWithFrame:CGRectZero configuration:conf];
        _wkWebView.navigationDelegate = self;
        _wkWebView.allowsBackForwardNavigationGestures = YES;
        _wkWebView.UIDelegate = self;
        [self.view addSubview:_wkWebView];
        [_wkWebView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.mas_equalTo(self.view);
        }];
    }
    return _wkWebView;
}

-(UILabel *)errorLabel{
    if (!_errorLabel) {
        _errorLabel = [[UILabel alloc] init];
        _errorLabel.numberOfLines = 0;
        _errorLabel.preferredMaxLayoutWidth = WIDTH - 20;
        [self.view addSubview:_errorLabel];
        [_errorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.view);
        }];
    }
    return _errorLabel;
}

-(NSString *)getParseUrl{
    return _urlString;
}

#pragma mark - NetRequest
- (void)queryUrlOperation:(CustomWebRequestOperation)operation{
    //TODO 是否需要传 gameKind
    [NET_DATA_MANAGER requestGetLive_ele_qipaiWithLiveCode:self.code
                                                  gameType:self.type
                                                  gameKind:self.kind
                                                   Success:^(id responseObject) {
                                                       [SVProgressHUD dismiss];

                                                       if ([self.code hasPrefix:@"pt"]) {
                                                           NSArray *temp = [responseObject regularExpressionWithPattern:URL_PATTERN];
                                                           [temp enumerateObjectsUsingBlock:^(NSTextCheckingResult *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                                               self.urlString = [responseObject substringWithRange:obj.range];

                                                               [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.urlString]];
                                                           }];

                                                       }else{
                                                           
                                                           NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
                                                           __block NSString *cookie = nil;
                                                           [storage.cookies enumerateObjectsUsingBlock:^(NSHTTPCookie * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                                               cookie = [NSString stringWithFormat:@"%@=%@",obj.name,obj.value];
                                                           }];
                                                           NSMutableDictionary *paramers = @{}.mutableCopy;
                                                           [paramers safeSetObject:cookie forKey:@"token"];
                                                           [self loadUrl:responseObject CookieParamers:paramers Operation:operation];
                                                       }
    }
                                                   failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        self.errorLabel.text = error.msg;
    }];
}

- (void)testPlayDetailWithValiCode:(NSString *)valiCode QueryOperation:(CustomWebRequestOperation)operation{
    [NET_DATA_MANAGER requestGetLiveTestPlayParameterLiveCode:self.code
                                                     gameType:self.type
                                                     gameKind:self.kind
                                                  MobileValue:@"true"
                                                    vaildCode:valiCode
                                                      Success:^(id responseObject) {
        
                                                          [SVProgressHUD dismiss];
                                                          
                                                          if ([self.code hasPrefix:@"pt"]) {
                                                              NSArray *temp = [responseObject regularExpressionWithPattern:URL_PATTERN];
                                                              [temp enumerateObjectsUsingBlock:^(NSTextCheckingResult *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                                                  self.urlString = [responseObject substringWithRange:obj.range];
                                                                  
                                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.urlString]];
                                                              }];
                                                              
                                                          }else{
                                                              
                                                              NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
                                                              __block NSString *cookie = nil;
                                                              [storage.cookies enumerateObjectsUsingBlock:^(NSHTTPCookie * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                                                  cookie = [NSString stringWithFormat:@"%@=%@",obj.name,obj.value];
                                                              }];
                                                              NSMutableDictionary *paramers = @{}.mutableCopy;
                                                              [paramers safeSetObject:cookie forKey:@"token"];
                                                              [self loadUrl:responseObject CookieParamers:paramers Operation:operation];
                                                          }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        self.errorLabel.text = error.msg;
        NSLog(@"%@",error);
    }];
    
}

#pragma mark - GET/SET
-(void)setUrlStr:(NSString *)urlStr{
    
    if([urlStr containsString:@"?"]){
        _urlString = [NSString stringWithFormat:@"%@?_t=%@",urlStr,[NSDate getNowTimeTimestamp]];
    }else{
        _urlString = [NSString stringWithFormat:@"%@&_t=%@",urlStr,[NSDate getNowTimeTimestamp]];
    }
}

@end
