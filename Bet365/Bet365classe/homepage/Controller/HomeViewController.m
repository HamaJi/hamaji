//
//  HomeViewController.m
//  Bet365
//
//  Created by adnin on 2018/11/11.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "HomeViewController.h"

#import "HomeAction.h"
#import "HongBaoView.h"

//#import "UICollectionViewFlowLayout+Add.h"

@interface HomeViewController ()
<HomeActionDelegate>

@property (nonatomic,strong)HomeCollectionView *collectionView;

@property (nonatomic,strong)HomeAction *action;

@property (nonatomic,strong) HongBaoView *hbView;


@end

RouterKey *const kRouterHome = @"home";

@implementation HomeViewController

+(void)load{
    
    [self registerJumpRouterKey:kRouterHome toHandle:^(NSDictionary *parameters) {
        
        HomeViewController *vc = [[HomeViewController alloc] init];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }];
    
    [self registerInstanceRouterKey:kRouterHome toHandle:^UIViewController *(NSDictionary *parameters) {
        return [[HomeViewController alloc] init];
    }];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.naviLoginStatus = Bet365NaviLogoItemStatus_DEFAULTS;
    self.action.delegate = self;
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    [self addObserver];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.action kViewWillDisappear];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.action kViewDidAppear];
}

-(void)dealloc{
    self.collectionView = nil;
}
-(void)addObserver{
    @weakify(self);
    [RACObserve(BET_CONFIG.common_config, isShowFloatRedPacket) subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        dispatch_async(dispatch_get_main_queue(), ^{
            BOOL value = [x boolValue];
            if (value) {
                NSString *redPackPath = [NSString stringWithFormat:@"%@%@",SerVer_Url,[BET_CONFIG.common_config.redPacketPath stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet]];
                UIImage *redPackCache = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:redPackPath];
                if (!redPackCache) {
                    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:redPackPath]
                                                                          options:0 progress:nil
                                                                        completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                                                                            if (finished&&!error) {
                                                                                CGFloat width = image.size.width < 80 ? image.size.width : 80;
                                                                                self.hbView.width = width;
                                                                                self.hbView.height = image.size.height/image.size.width * width + 30;
                                                                                self.hbView.hongbaoImage = image;
                                                                            }
                                                                        }];

                }else{
                    CGFloat width = redPackCache.size.width;
                    self.hbView.width = width;
                    self.hbView.height = redPackCache.size.height/redPackCache.size.width * width + 30;
                    self.hbView.hongbaoImage = redPackCache;
                }
            }else{
                if (_hbView) {
                    [self.hbView removeFromSuperview];
                    self.hbView = nil;
                }
            }
        });
    }];
}

#pragma mark - GET/SET


-(HomeCollectionView *)collectionView{
    if (!_collectionView) {
        self.automaticallyAdjustsScrollViewInsets=NO;
        UICollectionViewFlowColorLayout *layout = [[UICollectionViewFlowColorLayout alloc]init];
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing=0;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        
        //        layout.sectionHeadersPinToVisibleBoundsAll = YES;
        _collectionView = [[HomeCollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _collectionView.showsHorizontalScrollIndicator = NO;
        [_collectionView setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:_collectionView];
        [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
    }
    return _collectionView;
}




-(HomeAction *)action{
    if (!_action) {
        _action = [[HomeAction alloc] init];
        _action.collectionView = self.collectionView;
    }
    return _action;
}

- (HongBaoView *)hbView
{
    if (!_hbView) {
        _hbView = [[HongBaoView alloc] initWithFrame:CGRectMake(WIDTH - 80, 300, 100, 125)];
        [self.view insertSubview:_hbView aboveSubview:self.collectionView];
    }
    return _hbView;
}


@end
