//
//  NetWorkMannager+Home.h
//  Bet365
//
//  Created by HHH on 2018/7/5.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NetWorkMannager.h"
#import "RequestUrlHeader.h"
#import "BetTotal365ViewController.h"
@interface NetWorkMannager (Home)

/**
 LunBoImageUrl
 
 */
-(NSURLSessionTask *)requestLunBoImageUrlresponseCache:(HttpRequestCache)responseCache
                                               success:(HttpRequestSuccess)success
                                               failure:(HttpRequestFailed)failure;

/**
 TheAnnouncementUrl

 */
-(NSURLSessionTask *)requestTheAnnouncementUrlresponseCache:(HttpRequestCache)responseCache
                                                    success:(HttpRequestSuccess)success
                                                    failure:(HttpRequestFailed)failure;

/**
 TheGameJsonList

 */
-(NSURLSessionTask *)requestTheGameJsonListresponseCache:(HttpRequestCache)responseCache
                                                 success:(HttpRequestSuccess)success
                                                 failure:(HttpRequestFailed)failure;
/**
 TheWinNingList
 
 */
-(NSURLSessionTask *)requestTheWinNingListresponseCache:(HttpRequestCache)responseCache
                                                success:(HttpRequestSuccess)success
                                                failure:(HttpRequestFailed)failure;
/**
 TouristUrl
 
 */
-(NSURLSessionTask *)requestTouristUrlresponseCache:(HttpRequestCache)responseCache
                                            success:(HttpRequestSuccess)success
                                            failure:(HttpRequestFailed)failure;

/**
 TheSixOpenUrl

 */
-(NSURLSessionTask *)requestTheSixOpenUrlresponseCache:(HttpRequestCache)responseCache
                                               success:(HttpRequestSuccess)success
                                               failure:(HttpRequestFailed)failure;


/**
 TheGameOpenTimeList

 */
-(NSURLSessionTask *)requestTheGameOpenTimeListresponseCache:(HttpRequestCache)responseCache
                                                     success:(HttpRequestSuccess)success
                                                     failure:(HttpRequestFailed)failure;


/**
 Sport_ele_liveUrl

 */
-(NSURLSessionTask *)requestSport_ele_liveUrlresponseCache:(HttpRequestCache)responseCache
                                                   success:(HttpRequestSuccess)success
                                                   failure:(HttpRequestFailed)failure;

/**
 GAME_PLAY_SETTING

 */
-(NSURLSessionTask *)requestGamePlaySettingresponseCache:(HttpRequestCache)responseCache
                                                 success:(HttpRequestSuccess)success
                                                 failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestGetApphengFuresponseCache:(HttpRequestCache)responseCache
                                                 success:(HttpRequestSuccess)success
                                                 failure:(HttpRequestFailed)failure;

- (NSURLSessionTask *)requestGetRedPageWinlistCache:(HttpRequestCache)responseCache success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

- (NSURLSessionTask *)requestGetRedPageInfoCache:(HttpRequestCache)responseCache success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

/**
 开启红包

 @param success 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)requestOpenRedPagesuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

/**
 红包公告

 @param success 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)requestGetRedPackageNoticesuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

/**
 红包倒计时

 @param success 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)requestGetRedEnvelopeInfosuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

/**
 红包规则

 @param success 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)requestBaseMsgsuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

/**
 拆红包

 @param success 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)requestPostDrawRedEnvelopesuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

/**
 领红包

 @param redId id
 @param success 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)requestPostWeekendWithId:(NSInteger)redId success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

/**
 彩票规则

 @param type <#type description#>
 @param gameType <#gameType description#>
 @param success <#success description#>
 @param failure <#failure description#>
 @return <#return value description#>
 */
- (NSURLSessionTask *)requestGetRulesWithType:(NSString *)type currenGameType:(currenGameType)gameType gameID:(NSString *)gameID success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

/**
 周末红包

 @param success <#success description#>
 @param failure <#failure description#>
 @return <#return value description#>
 */
- (NSURLSessionTask *)requestQueryWeekActivityQualificationSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;


- (NSURLSessionTask *)requestGetConfig_open_timeSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

- (NSURLSessionTask *)requestGetApp_dpSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;
//在线人数
- (NSURLSessionTask *)requestUserOnlineCountSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;
@end
