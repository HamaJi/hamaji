//
//  HomePageFactory.m
//  Bet365
//
//  Created by HHH on 2018/7/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "HomePageFactory.h"
#import "NetWorkMannager+sports.h"
#import "NetWorkMannager+Home.h"
#import <SDWebImage/SDWebImageManager.h>
#import "NetWorkMannager+Account.h"
#import "KFCPHomeGameJsonModel.h"
#import "KFCPGameWinNoticeModel.h"
#import "HomePageCanva.h"
#import "KFCPTimerModel.h"
#import "SportsModuleShare.h"
#import "LotteryFactory.h"
#import "AllLiveData.h"

@implementation HomePageData

@end

@implementation HomePageFactory

static HomePageFactory *instance;
+(instancetype)shareInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[HomePageFactory alloc] init];
    });
    return instance;
}
-(instancetype)init{
    if (self = [super init]) {
        [self updateSubject];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (SerVer_Url.length) {
                [self getAllRequest];
            }
            
        });
    }
    return self;
}
#pragma mark - Public
-(void)getAllRequest{
    dispatch_group_t group = dispatch_group_create();
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [HomePageFactory getHorCanvaDataCompleted:^(BOOL success, NetReceiveObjType type) {
            if (type != NetReceiveObjType_CACHE) {
                dispatch_semaphore_signal(sema);
            }
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [HomePageFactory getBannerUrlsCompleted:^(BOOL success, NetReceiveObjType type) {
            if (type != NetReceiveObjType_CACHE) {
                dispatch_semaphore_signal(sema);
            }
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [HomePageFactory getZhenRenDataCompleted:^(BOOL success, NetReceiveObjType type) {
            if (type != NetReceiveObjType_CACHE) {
                dispatch_semaphore_signal(sema);
            }
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [NET_DATA_MANAGER requestGetSportOnOffCache:nil Success:^(id responseObject) {
            self.sportModel = [SportOnOffModel mj_objectWithKeyValues:responseObject];
            dispatch_semaphore_signal(sema);
        } failure:^(NSError *error) {
            
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
//    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
//        [HomePageFactory getGamePlaySettingDataCompleted:^(BOOL success, NetReceiveObjType type) {
//            if (type != NetReceiveObjType_CACHE) {
//                dispatch_semaphore_signal(sema);
//            }
//        }];
//        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
//    });
    
    
//    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
//        [NET_DATA_MANAGER requestTheSixOpenUrlresponseCache:nil success:^(id responseObject) {
//            self.lhcList = (NSArray *)responseObject;
//            dispatch_semaphore_signal(sema);
//        } failure:^(NSError *error) {
//
//        }];
//        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
//    });
    
    
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [NET_DATA_MANAGER requestGetApp_dpSuccess:^(id responseObject) {
            ALL_DATA.appModel = [AppdpModel mj_objectWithKeyValues:responseObject];
            dispatch_semaphore_signal(sema);
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [self.updateSubject sendNext:@(NetReceiveObjType_RESOPONSE)];
//        [self.updateSubject sendCompleted];
    });
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.updateSubject sendNext:@(NetReceiveObjType_CACHE)];
    });
}
#pragma mark - NetRequest
// ** 获取轮播广告**/
+(NSURLSessionTask *)getBannerUrlsCompleted:(void(^)(BOOL success,NetReceiveObjType type))completed{
    
    void(^lunboData)(NSString *res,NetReceiveObjType type) = ^(NSString *res,NetReceiveObjType type){
        if (!res) {
            completed ? completed(NO,type) : nil;
            return ;
        }
        NSMutableArray *imageArrs = [NSMutableArray arrayWithArray:[res regularExpressionWithPattern:@"src=\".*?\""]];
        NSMutableArray *hrefArrs = [NSMutableArray arrayWithArray:[res regularExpressionWithPattern:@"href=\".*?\""]];
        if (imageArrs.count > 2) {
            [imageArrs removeObjectAtIndex:0];
            [imageArrs removeLastObject];
            [hrefArrs removeObjectAtIndex:0];
            [hrefArrs removeLastObject];
        }
        NSMutableArray *imageTemp = [NSMutableArray array];
        NSMutableArray *hrefTemp = [NSMutableArray array];
        [imageArrs enumerateObjectsUsingBlock:^(NSTextCheckingResult * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSMutableString *imageUrl = [NSMutableString stringWithString:[res substringWithRange:obj.range]];
            [imageUrl deleteString:@"src=\""];
            [imageUrl deleteString:@"\""];
            NSString *imageUrlStr = nil;
            if (![imageUrl hasPrefix:@"http"]) {
                imageUrlStr = [StringFormatWithStr(SerVer_Url, imageUrl) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            }else{
                imageUrlStr = [imageUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            }
            if (imageUrlStr) {
                [imageTemp addObject:imageUrlStr];
            }
        }];
        [hrefArrs enumerateObjectsUsingBlock:^(NSTextCheckingResult * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSMutableString *hrefUrl = [NSMutableString stringWithString:[res substringWithRange:obj.range]];
            [hrefUrl deleteString:@"href=\""];
            [hrefUrl deleteString:@"\""];
            if (hrefUrl) {
                [hrefTemp addObject:hrefUrl];
            }
        }];
        
        if (imageTemp.count && hrefTemp.count &&
            type != NetReceiveObjType_FAIL) {
            HOME_FACTORY.bannerUrlList = @[imageTemp,hrefTemp];
        }
        completed ? completed(imageTemp.count && hrefTemp.count,type) : nil;
    };
    
    return [NET_DATA_MANAGER requestLunBoImageUrlresponseCache:^(id responseCache) {
        lunboData(responseCache,NetReceiveObjType_CACHE);
    } success:^(id responseObject) {
        lunboData(responseObject,NetReceiveObjType_RESOPONSE);
    } failure:^(NSError *error) {
        lunboData(nil,NetReceiveObjType_FAIL);
    }];
}

/** 公告栏数据 **/
+ (void)getNoticeReceiveObjType:(NetReceiveObjType)type Completed:(void(^)(HomePageData*data))completed{
    
    __block HomePageData *data = [[HomePageData alloc] init];
    
    /**AnnouncementDataCache**/
    void(^AnnouncementBlock)(NSDictionary *dataDic,NetReceiveObjType type ,NSError *error) = ^(NSDictionary *dataDic,NetReceiveObjType type ,NSError *error){
        data.type = type;
        data.response = dataDic;
        data.error = error;
        if (!error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                BET_CONFIG.noticeModel = [Bet365NoticeModel mj_objectWithKeyValues:dataDic];
            });
        }
        completed(data);
    };
    if (type == NetReceiveObjType_CACHE) {
        [NET_DATA_MANAGER requestTheAnnouncementUrlresponseCache:^(id responseCache) {
            AnnouncementBlock(responseCache,NetReceiveObjType_CACHE,nil);
        } success:nil failure:nil];
    }else{
        [NET_DATA_MANAGER requestTheAnnouncementUrlresponseCache:nil success:^(id responseObject) {
            AnnouncementBlock(responseObject,NetReceiveObjType_RESOPONSE,nil);
        } failure:^(NSError *error) {
            AnnouncementBlock(nil,NetReceiveObjType_FAIL,error);
        }];
    }
    
    
}

+ (void)getGameDataReceiveObjType:(NetReceiveObjType)type Completed:(void(^)(HomePageData*data))completed{
    
    __block HomePageData*data = [HomePageData new];
    
    void(^gameJson)(NSArray *arrData,NetReceiveObjType type ,NSError *error) = ^(NSArray *arrData,NetReceiveObjType type ,NSError *error){
        data.type = type;
        if (!arrData || error) {
            data.error = error;
            completed(data);
            return;
        }
        NSMutableArray *gameJson = [[NSMutableArray alloc]init];
        [arrData enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *NowDic = obj;
            KFCPHomeGameJsonModel *homeModel = [[KFCPHomeGameJsonModel alloc]initWithDictionary:NowDic error:nil];
            [gameJson addObject:homeModel];
        }];
        NSArray *gameArr = [[NSArray alloc] init];
        gameArr = detailofficalAndCredit(gameJson);
        data.response = gameArr;
        completed(data);
    };
    if (type == NetReceiveObjType_CACHE) {
        [NET_DATA_MANAGER requestTheGameJsonListresponseCache:^(id responseCache) {
            gameJson(responseCache,NetReceiveObjType_CACHE,nil);

        } success:nil failure:nil];
    }else{
        [NET_DATA_MANAGER requestTheGameJsonListresponseCache:nil success:^(id responseObject) {

            gameJson(responseObject,NetReceiveObjType_RESOPONSE,nil);

        } failure:^(NSError *error) {

            gameJson(nil,NetReceiveObjType_FAIL,error);
        }];
    }
    
    
}

/** 中奖数据**/
+(void)getWinDataReceiveObjType:(NetReceiveObjType)type Completed:(void(^)(HomePageData *data))completed{
    
    __block HomePageData*data = [HomePageData new];
    
    void(^winDataBlock)(NSArray *resArr,NetReceiveObjType type ,NSError *error) = ^(NSArray *resArr,NetReceiveObjType type ,NSError *error){
        
        data.type = type;
        if (!resArr || [resArr isKindOfClass:[NSString class]] || error) {
            data.error = error;
            completed(data);
            return;
        }
        NSMutableArray *WinGameJson = [[NSMutableArray alloc]init];
        [resArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *resDic = obj;
            KFCPGameWinNoticeModel *model = [[KFCPGameWinNoticeModel alloc]initWithDictionary:resDic error:nil];
            [WinGameJson addObject:model];
        }];
        data.response = WinGameJson;
        completed(data);
    };
    
    if (type == NetReceiveObjType_CACHE) {
        [NET_DATA_MANAGER requestTheWinNingListresponseCache:^(id responseCache) {
            winDataBlock(responseCache,NetReceiveObjType_CACHE,nil);
        } success:nil failure:nil];
    }else{
        [NET_DATA_MANAGER requestTheWinNingListresponseCache:nil success:^(id responseObject) {
            winDataBlock(responseObject,NetReceiveObjType_RESOPONSE,nil);
        } failure:^(NSError *error) {

            winDataBlock(nil,NetReceiveObjType_FAIL,error);
        }];
    }
}

/** 游客返点**/
+(void)getVisitorsComeBackReceiveObjType:(NetReceiveObjType)type Completed:(void(^)(HomePageData*data))completed{
    
    __block HomePageData*data = [HomePageData new];

    void(^visitorsBlock)(NSDictionary *dic,NetReceiveObjType type ,NSError *error) = ^(NSDictionary *dic,NetReceiveObjType type ,NSError *error){
        
        data.type = type;
        if (!dic || ![dic isKindOfClass:[NSDictionary class]] || error) {
            data.error = error;
            completed(data);
            return;
        }
//        dispatch_async(dispatch_get_main_queue(), ^{
//            JesseAppdelegate.touristRebate =  dic[@"visitors_rebate"];
//            JesseAppdelegate.configValue = [dic[@"play_type_config"] integerValue];
//        });
        data.response = dic;
        completed(data);
        
    };
    
    if (type == NetReceiveObjType_CACHE) {
        [NET_DATA_MANAGER requestTouristUrlresponseCache:^(id responseCache) {
            visitorsBlock(responseCache,NetReceiveObjType_CACHE,nil);
        } success:nil failure:nil];
    }else{
        [NET_DATA_MANAGER requestTouristUrlresponseCache:nil success:^(id responseObject) {
            visitorsBlock(responseObject,NetReceiveObjType_RESOPONSE,nil);
        } failure:^(NSError *error) {
            visitorsBlock(nil,NetReceiveObjType_FAIL,error);
        }];
    }
    
    
}

+(void)getGameOpenWinStatusDataReceiveObjType:(NetReceiveObjType)type Completed:(void(^)(HomePageData *data))completed{
    
    __block HomePageData *data = [HomePageData new];
    
    __block HomeOpenWinCamp *camp = [[HomeOpenWinCamp alloc] init];
    
    void(^gamePrizeCacheBlock)(NSDictionary *respones,NetReceiveObjType type ,NSError *error) = ^(NSDictionary *respones,NetReceiveObjType type ,NSError *error){
        data.type = type;
        if (!respones || ![respones isKindOfClass:[NSDictionary class]] || error) {
            data.error = error;
            completed(data);
            return;
        }
        NSMutableArray *gameSTimerResult = [[NSMutableArray alloc]init];
        NSMutableArray *gameTimeArr = [[NSMutableArray alloc]init];
        NSMutableArray *gameValueArr = [[NSMutableArray alloc]init];
        NSMutableDictionary *IntanceDic = [[NSMutableDictionary alloc] initWithDictionary:respones];
        /**先取出servertime**/
        NSString *serverTime = IntanceDic[@"serverTime"];
        camp.serverTime = [IntanceDic[@"serverTime"] longValue];
        [IntanceDic removeObjectForKey:@"serverTime"];
        [IntanceDic enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            NSMutableArray *SubGameA = [[NSMutableArray alloc]init];
            NSDictionary *mainRespone =obj;
            lotteryCurModel *curModel = [[lotteryCurModel alloc] initWithDictionary:mainRespone[@"cur"] error:nil];
            lotteryPreModel *preModel = [[lotteryPreModel alloc] initWithDictionary:mainRespone[@"pre"] error:nil];
            preModel.gameId = (NSString *)key;
            HomeDetailData(gameTimeArr, curModel.closeTime, serverTime, preModel.gameId);
            if ([curModel.status integerValue]==0) {
                KFCPTimerModel *timeResultModle = [[KFCPTimerModel alloc]init];
                timeResultModle.timer = @"";
                timeResultModle.gameId = [NSString stringWithFormat:@"%@",preModel.gameId];
                [gameSTimerResult addObject:timeResultModle];
            }else if ([curModel.status integerValue]==1){
                KFCPTimerModel *timeResultModle = [[KFCPTimerModel alloc]init];
                timeResultModle.timer = @"封盘中";
                timeResultModle.gameId = [NSString stringWithFormat:@"%@",preModel.gameId];
                [gameSTimerResult addObject:timeResultModle];
            }else{
                KFCPTimerModel *timeResultModle = [[KFCPTimerModel alloc]init];
                timeResultModle.timer = @"开奖中";
                timeResultModle.gameId = [NSString stringWithFormat:@"%@",preModel.gameId];
                [gameSTimerResult addObject:timeResultModle];
            }
            [SubGameA addObject:curModel];
            [SubGameA addObject:preModel];
            [gameValueArr addObject:SubGameA];
        }];
        camp.gameSTimerResult = gameSTimerResult;
        camp.nowGameValueArr = gameValueArr;
        camp.gameTimerS = gameTimeArr;
        data.response = camp;
        LOTTERY_FACTORY.allOpenInfo = camp;
//        AppDelegate *app = (id)[UIApplication sharedApplication].delegate;
//        [app updateCurrenGameValue:camp.nowGameValueArr AndTimeArr:camp.gameTimerS AndresultDataArr:camp.gameSTimerResult isCacheNow:camp.prizeCacheNow];
        completed(data);
    };
    if (type == NetReceiveObjType_CACHE) {
        [NET_DATA_MANAGER requestTheGameOpenTimeListresponseCache:^(id responseCache) {
            if (responseCache) {
                camp.prizeCacheNow = YES;
            }
            gamePrizeCacheBlock(responseCache,NetReceiveObjType_CACHE,nil);
        } success:nil failure:nil];
    }else{
        [NET_DATA_MANAGER requestTheGameOpenTimeListresponseCache:nil success:^(id responseObject) {
            camp.prizeCacheNow = NO;
            gamePrizeCacheBlock(responseObject,NetReceiveObjType_RESOPONSE,nil);
            
            
        } failure:^(NSError *error) {
            gamePrizeCacheBlock(nil,NetReceiveObjType_FAIL,error);
        }];
    }
    
}

+(NSURLSessionTask *)getZhenRenDataCompleted:(void(^)(BOOL success,NetReceiveObjType type))completed{
    
    void(^handle)(id obj,NetReceiveObjType type) = ^(id obj,NetReceiveObjType type){
        if (!obj) {
            completed ? completed(NO,type) : nil;
            return ;
        }
        NSArray *arr = [SportEleLiveModel mj_objectArrayWithKeyValuesArray:obj];
        if (type != NetReceiveObjType_FAIL) {
            HOME_FACTORY.zhenrenList = arr;
        }
        completed(HOME_FACTORY.zhenrenList.count != 0,type);
    };
    
    return [NET_DATA_MANAGER requestSport_ele_liveUrlresponseCache:^(id responseCache) {
        
        handle(responseCache,NetReceiveObjType_CACHE);
    } success:^(id responseObject) {
        handle(responseObject,NetReceiveObjType_RESOPONSE);
    } failure:^(NSError *error) {
        handle(nil,NetReceiveObjType_FAIL);
    }];
}

//+(NSURLSessionTask *)getGamePlaySettingDataCompleted:(void(^)(BOOL success,NetReceiveObjType type))completed{
//    void(^detailConfig)(id obj,NetReceiveObjType type) = ^(id obj,NetReceiveObjType type){
//        if (!obj) {
//            completed ? completed(NO,type) : nil;
//            return ;
//        }
//        NSMutableArray *arr = [NSMutableArray array];
//        NSEnumerator *cofigEnum = [obj reverseObjectEnumerator];
//        for (NSDictionary *cofigDic in cofigEnum) {
//            lotteryCofigModel *model = [[lotteryCofigModel alloc]initWithDictionary:cofigDic error:nil];
//            [arr addObject:model];
//        }
//        if (type != NetReceiveObjType_FAIL) {
//            JesseAppdelegate.setConfig = arr;
//        }
//
//        completed(JesseAppdelegate.setConfig.count != 0,type);
//    };
//
//    return [NET_DATA_MANAGER requestGamePlaySettingresponseCache:^(id responseCache) {
//        detailConfig(responseCache,NetReceiveObjType_CACHE);
//    } success:^(id responseObject) {
//        detailConfig(responseObject,NetReceiveObjType_RESOPONSE);
//    } failure:^(NSError *error) {
//        detailConfig(nil,NetReceiveObjType_FAIL);
//    }];
//
//}

+ (NSURLSessionTask *)getHorCanvaDataCompleted:(void(^)(BOOL success,NetReceiveObjType type))completed{
    void(^handle)(id obj,NetReceiveObjType type) = ^(id obj,NetReceiveObjType type){
        if (!obj) {
            completed ? completed(NO,type) : nil;
            return ;
        }
        Bet365HengFuModel *model = [[Bet365HengFuModel alloc] initWithDict:obj];
        if (type != NetReceiveObjType_FAIL) {
            HOME_FACTORY.horCanvaModel = model;
        }
        completed(HOME_FACTORY.horCanvaModel != nil,type);
    };
    
    return [NET_DATA_MANAGER requestGetApphengFuresponseCache:^(id responseCache) {
        handle(responseCache,NetReceiveObjType_CACHE);
    } success:^(id responseObject) {
        handle(responseObject,NetReceiveObjType_RESOPONSE);
    } failure:^(NSError *error) {
        handle(nil,NetReceiveObjType_FAIL);
    }];
}

+ (NSURLSessionTask *)getUserOnlineCountCompleted:(void(^)(NSUInteger count))completed{
    void(^handle)(id obj) = ^(id obj){
        NSUInteger count = 0;
        if (!obj) {
            count = 0;
        }else{
            count = [[obj objectForKey:@"count"] integerValue];
        }
        completed ? completed(count) : nil;
    };
    
    return [NET_DATA_MANAGER requestUserOnlineCountSuccess:^(id responseObject) {
        handle(responseObject);
    } failure:^(NSError *error) {
        handle(nil);
    }];
}



+(void)getRedPageWinlistCompleted:(void (^)(NetReceiveObjType typ,NSArray<HomeRedPageWinModel *>* list))completed{
    [NET_DATA_MANAGER requestGetRedPageWinlistCache:^(id responseCache) {
        NSError *error;
        NSArray *list = [MTLJSONAdapter modelsOfClass:[HomeRedPageWinModel class] fromJSONArray:responseCache error:&error];
        completed ? completed(NetReceiveObjType_CACHE,list) : nil;
    } success:^(id responseObject) {
        NSError *error;
        NSArray *list = [MTLJSONAdapter modelsOfClass:[HomeRedPageWinModel class] fromJSONArray:responseObject error:&error];
        completed ? completed(NetReceiveObjType_RESOPONSE,list) : nil;
    } failure:^(NSError *error) {
        completed ? completed(NetReceiveObjType_FAIL,nil) : nil;
    }];
}

+(NSURLSessionTask *)getRedPageInfoCompleted:(void (^)(NetReceiveObjType typ,HomeRedPageInfoModel *model))completed{
    return [NET_DATA_MANAGER requestGetRedPageInfoCache:nil success:^(id responseObject) {
        NSError *error;
        HomeRedPageInfoModel *model = [MTLJSONAdapter modelOfClass:[HomeRedPageInfoModel class] fromJSONDictionary:responseObject error:&error];
        completed ? completed(NetReceiveObjType_RESOPONSE,model) : nil;
    } failure:^(NSError *error) {
        completed ? completed(NetReceiveObjType_FAIL,nil) : nil;
    }];
}

+(void)openRedPageCompleted:(void (^)(id responseObject,NSError *error))completed{
    [NET_DATA_MANAGER requestOpenRedPagesuccess:^(id responseObject) {
        completed ? completed(responseObject,nil) : nil;
    } failure:^(NSError *error) {
        completed ? completed(nil,error) : nil;
    }];
}

+ (void)getRedPackageNoticeCompleted:(void (^)(NSString *))completed
{
    [NET_DATA_MANAGER requestGetRedPackageNoticesuccess:^(id responseObject) {
        completed ? completed(responseObject[@"app_redPackage_notice"]) : nil;
    } failure:^(NSError *error) {
        completed ? completed(nil) : nil;
    }];
}

+ (void)getRedEnvelopeInfoCompleted:(void (^)(NetReceiveObjType, HomeRedPageEnvelopeInfoModel *))completed
{
    [NET_DATA_MANAGER requestGetRedEnvelopeInfosuccess:^(id responseObject) {
        HomeRedPageEnvelopeInfoModel *model = [HomeRedPageEnvelopeInfoModel mj_objectWithKeyValues:responseObject];
        completed ? completed(NetReceiveObjType_RESOPONSE,model) : nil;
    } failure:^(NSError *error) {
        completed ? completed(NetReceiveObjType_FAIL,nil) : nil;
    }];
}

+ (void)getBaseMsgCompleted:(void (^)(NetReceiveObjType, id))completed
{
    [NET_DATA_MANAGER requestBaseMsgsuccess:^(id responseObject) {
        completed ? completed(NetReceiveObjType_RESOPONSE,responseObject) : nil;
    } failure:^(NSError *error) {
        completed ? completed(NetReceiveObjType_RESOPONSE,nil) : nil;
    }];
}

+ (void)getQueryWeekActivityQualificationCompleted:(void (^)(id))completed
{
    [NET_DATA_MANAGER requestQueryWeekActivityQualificationSuccess:^(id responseObject) {
        completed ? completed(responseObject) : nil;
    } failure:^(NSError *error) {
        completed ? completed(nil) : nil;
    }];
}

//对当前时间数据进行处理
void HomeDetailData(NSMutableArray *nowTimeArr,NSString *closedateString,NSString *severicedateString,NSString *gameID){
    NSDate *severiceT = stampTimerBy(severicedateString);
    NSDate *closeDate = stampTimerBy(closedateString);
    NSTimeInterval result = [closeDate timeIntervalSinceDate:severiceT];
    int AsdateNow = result;
    KFCPTimerModel *timeModel = [[KFCPTimerModel alloc]init];
    timeModel.timer = [NSString stringWithFormat:@"%d",AsdateNow];
    timeModel.gameId = gameID;
    [nowTimeArr addObject:timeModel];
}

//将某个时间戳转化成 时间
NSDate *stampTimerBy(NSString *times){
    NSString * timeStampString = times;
    NSTimeInterval interval=[timeStampString doubleValue] / 1000;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    return  date;
}

#pragma mark - GET/SET

-(RACSubject *)updateSubject{
    if (!_updateSubject) {
        _updateSubject = [RACSubject subject];
    }
    return _updateSubject;
}
@end
