//
//  LotteryForBetFactory.m
//  Bet365
//
//  Created by jesse on 2018/8/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "LotteryForBetFactory.h"
@implementation LotteryData
@end
@implementation LotteryForBetFactory
+(void)GetCreditData:(NetReceiveObjType)type forGameParameterId:(NSString *)gameId Completed:(void (^)(LotteryData *))completed returnComplted:(responeTask)taskComplted{
    __block LotteryData *data = [[LotteryData alloc] init];
    if (type==NetReceiveObjType_CACHE) {
       taskComplted([NET_DATA_MANAGER GET:GameDataRequestUrl(gameId) RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:nil responseCache:^(id responseCache) {
            data.respone = responseCache;
            data.type = NetReceiveObjType_CACHE;
            data.error = nil;
            completed(data);
       } success:nil failure:nil]);
    }else{
       taskComplted([NET_DATA_MANAGER GET:GameDataRequestUrl(gameId) RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:nil responseCache:^(id responseCache) {
        } success:^(id responseObject) {
            data.respone = responseObject;
            data.type = NetReceiveObjType_RESOPONSE;
            data.error = nil;
        } failure:^(NSError *error) {
            data.respone = nil;
            data.type = NetReceiveObjType_FAIL;
            data.error = error;
        }]);
    }
}
@end
