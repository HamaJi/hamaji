//
//  TurnTablePrizeModel.h
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/17.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "MTLModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TurnTablePrizeModel : MTLModel<MTLJSONSerializing>

@property (nonatomic,strong) NSNumber*min;
@property (nonatomic,strong) NSNumber*max;
@property (nonatomic,strong) NSNumber *prizeId;
@property (nonatomic,strong) NSNumber *probability;
@property (nonatomic,strong) NSNumber *quantity;

@property (nonatomic,strong) NSString *prizeDesc;
@property (nonatomic,strong) NSString *prizeMoney;
@property (nonatomic,strong) NSString *prizeName;

@end

NS_ASSUME_NONNULL_END
