//
//  LotteryForBetFactory.h
//  Bet365
//
//  Created by jesse on 2018/8/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RequestUrlHeader.h"
typedef void(^responeTask)(NSURLSessionTask *task);
@interface LotteryData : NSObject
@property(nonatomic,assign)NetReceiveObjType type;
@property(nonatomic,strong)id respone;
@property(nonatomic,strong)NSError *error;
@end
@interface LotteryForBetFactory : NSObject
+(void)GetCreditData:(NetReceiveObjType)type forGameParameterId:(NSString *)gameId Completed:(void(^)(LotteryData *data))completed returnComplted:(responeTask)taskComplted;
@end
