//
//  TurnTableRotaryModel.h
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/18.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "MTLModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TurnTableRotaryModel : MTLModel<MTLJSONSerializing>

@property (nonatomic,strong) NSString *account;
@property (nonatomic,strong) NSString *activityRecordName;
@property (nonatomic,strong) NSString *createDatetime;
@property (nonatomic,strong) NSString *prize;
@property (nonatomic,strong) NSString *settlementCreatetime;
@property (nonatomic,strong) NSString *settlementType;
@property (nonatomic,strong) NSString *status;
@property (nonatomic,strong) NSString *userId;
@property (nonatomic,strong) NSString *remark;

@property (nonatomic,strong) NSNumber *activityId;
@property (nonatomic,strong) NSNumber *identity;
@property (nonatomic,strong) NSNumber *money;
@property (nonatomic,strong) NSNumber *ruleId;

@end

NS_ASSUME_NONNULL_END
