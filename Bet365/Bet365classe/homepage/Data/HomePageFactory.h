//
//  HomePageFactory.h
//  Bet365
//
//  Created by HHH on 2018/7/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HomePageCanva.h"

#import "KFCPGameWinNoticeModel.h"
#import "SportEleLiveModel.h"
#import "Bet365HengFuModel.h"
#define HOME_FACTORY [HomePageFactory shareInstance]


#import "HomeRedPageWinModel.h"
#import "HomeRedPageInfoModel.h"

@interface HomePageData : NSObject
@property (nonatomic,assign)NetReceiveObjType type;
@property (nonatomic,strong)id response;
@property (nonatomic,strong)NSError *error;
@end

@interface HomePageFactory : NSObject

@property (nonatomic,strong)NSArray <NSArray <NSString *>*>*bannerUrlList;

@property (nonatomic,strong)NSArray <SportEleLiveModel *>*zhenrenList;
@property (nonatomic,strong)NSArray <NSArray <NSString *>*>*lhcList;
@property (nonatomic,strong) Bet365HengFuModel *horCanvaModel;
@property (nonatomic,strong) SportOnOffModel *sportModel;
@property (nonatomic,strong)RACSubject *updateSubject;

+(instancetype)shareInstance;

+(NSURLSessionTask *)getBannerUrlsCompleted:(void(^)(BOOL success,NetReceiveObjType type))completed;

+(NSURLSessionTask *)getLotteryListCompleted:(void(^)(BOOL success,NetReceiveObjType type))completed;

+(NSURLSessionTask *)getZhenRenDataCompleted:(void(^)(BOOL success,NetReceiveObjType type))completed;

+(NSURLSessionTask *)getHorCanvaDataCompleted:(void(^)(BOOL success,NetReceiveObjType type))completed;

-(void)getAllRequest;

/** 蛤蟆吉TODO MainFlex删除*/
+(void)getGameOpenWinStatusDataReceiveObjType:(NetReceiveObjType)type Completed:(void(^)(HomePageData *data))completed;
/** 蛤蟆吉TODO MainFlex删除*/
+(void)getWinDataReceiveObjType:(NetReceiveObjType)type Completed:(void(^)(HomePageData *data))completed;
/** 蛤蟆吉TODO MainFlex删除*/
+ (void)getNoticeReceiveObjType:(NetReceiveObjType)type Completed:(void(^)(HomePageData*data))completed;
/** 蛤蟆吉TODO MainFlex删除*/
+ (void)getGameDataReceiveObjType:(NetReceiveObjType)type Completed:(void(^)(HomePageData*data))completed;
/** 蛤蟆吉TODO MainFlex删除*/
+(void)getVisitorsComeBackReceiveObjType:(NetReceiveObjType)type Completed:(void(^)(HomePageData*data))completed;
/** 蛤蟆吉TODO MainFlex删除*/
+(void)getGamePlaySettingDataReceiveObjType:(NetReceiveObjType)type Completed:(void(^)(HomePageData *data))completed;
/**
 首页红包

 @param completed <#completed description#>
 */
+(void)getRedPageWinlistCompleted:(void (^)(NetReceiveObjType typ,NSArray<HomeRedPageWinModel *>* list))completed;

+(NSURLSessionTask *)getRedPageInfoCompleted:(void (^)(NetReceiveObjType typ,HomeRedPageInfoModel *model))completed;

/**
 打开红包

 @param completed <#completed description#>
 */
+(void)openRedPageCompleted:(void (^)(id responseObject,NSError *error))completed;

/**
 红包公告

 @param completed <#completed description#>
 */
+(void)getRedPackageNoticeCompleted:(void (^)(NSString *app_redPackage_notice))completed;


/**
 红包倒计时

 @param completed <#completed description#>
 */
+(void)getRedEnvelopeInfoCompleted:(void (^)(NetReceiveObjType type,HomeRedPageEnvelopeInfoModel *model))completed;

/**
 红包规则

 @param completed <#completed description#>
 */
+(void)getBaseMsgCompleted:(void (^)(NetReceiveObjType type,id responseObject))completed;

/**
 周末红包

 @param completed <#completed description#>
 */
+(void)getQueryWeekActivityQualificationCompleted:(void (^)(id responseObject))completed;

//用户在线人数
+ (NSURLSessionTask *)getUserOnlineCountCompleted:(void(^)(NSUInteger count))completed;
@end
