//
//  TurnTableRotaryModel.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/18.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "TurnTableRotaryModel.h"

@implementation TurnTableRotaryModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"account":@"account",
             @"activityRecordName":@"activityRecordName",
             @"createDatetime":@"createDatetime",
             @"prize":@"prize",
             @"settlementCreatetime":@"settlementCreatetime",
             @"settlementType":@"settlementType",
             @"status":@"status",
             @"userId":@"userId",
             @"remark":@"remark",
             
             @"activityId":@"activityId",
             @"identity":@"id",
             @"money":@"money",
             @"ruleId":@"ruleId",
             };
}


@end
