//
//  TurnTablePrizeModel.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/17.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "TurnTablePrizeModel.h"

@implementation TurnTablePrizeModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"max":@"max",
             @"min":@"min",
             
             @"prizeId":@"prizeId",
             @"probability":@"probability",
             @"quantity":@"quantity",
             
             @"prizeDesc":@"prizeDesc",
             @"prizeMoney":@"prizeMoney",
             @"prizeName":@"prizeName",
             };
}

+ (NSValueTransformer *)minJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString *min){
//                NSMutableArray <NSNumber *>*list = @[].mutableCopy;
//                [[min componentsSeparatedByString:@","] enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                    [list safeAddObject:[NSNumber numberWithFloat:[obj floatValue]]];
//                }];
//                return list;
                NSArray *list = [min componentsSeparatedByString:@","];
                if (list.count > 1) {
                    return min;
                }
                return @([list.firstObject floatValue]);
            } reverseBlock:^id(id min) {
                
//                NSMutableArray *list = @[].mutableCopy;
//                [min enumerateObjectsUsingBlock:^(NSNumber * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                    [list addObject:[obj stringValue]];
//                }];
//                return [list componentsJoinedByString:@","];
                if ([min isKindOfClass:[NSNumber class]]) {
                    return [((NSNumber *)min) stringValue];
                }
                return min;
            }];
}

+ (NSValueTransformer *)maxJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString *max){
//                NSMutableArray <NSNumber *>*list = @[].mutableCopy;
//                [[max componentsSeparatedByString:@","] enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                    [list safeAddObject:[NSNumber numberWithFloat:[obj floatValue]]];
//                }];
//                return list;
                NSArray *list = [max componentsSeparatedByString:@","];
                if (list.count > 1) {
                    return max;
                }
                return @([list.firstObject floatValue]);
            } reverseBlock:^id(id max) {
//                NSMutableArray *list = @[].mutableCopy;
//                [max enumerateObjectsUsingBlock:^(NSNumber * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                    [list addObject:[obj stringValue]];
//                }];
//                return [list componentsJoinedByString:@","];
                if ([max isKindOfClass:[NSNumber class]]) {
                    return [((NSNumber *)max) stringValue];
                }
                return max;
            }];
}
@end
