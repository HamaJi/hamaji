//
//  NetWorkMannager+TurnTable.h
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/17.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "NetWorkMannager.h"

NS_ASSUME_NONNULL_BEGIN

@interface NetWorkMannager (TurnTable)

-(NSURLSessionTask *)requestGetPrizeSuccess:(HttpRequestSuccess)success
                                    failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestRotaryTableWelfareDetailSuccess:(HttpRequestSuccess)success
                                                    failure:(HttpRequestFailed)failure;

-(NSURLSessionTask *)requestDrawRotaryTableSuccess:(HttpRequestSuccess)success
                                           failure:(HttpRequestFailed)failure;

@end

NS_ASSUME_NONNULL_END
