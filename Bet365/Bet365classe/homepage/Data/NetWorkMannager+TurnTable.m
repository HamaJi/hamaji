//
//  NetWorkMannager+TurnTable.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/17.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "NetWorkMannager+TurnTable.h"
#import "RequestUrlHeader.h"
@implementation NetWorkMannager (TurnTable)

-(NSURLSessionTask *)requestGetPrizeSuccess:(HttpRequestSuccess)success
                                               failure:(HttpRequestFailed)failure
{
    return [self GET:TURNTABLE_PRIZE RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:nil responseCache:nil success:success failure:failure];
}

-(NSURLSessionTask *)requestRotaryTableWelfareDetailSuccess:(HttpRequestSuccess)success
                                    failure:(HttpRequestFailed)failure
{
    return [self GET:TURNTABLE_WELFAREDATAIL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:nil responseCache:nil success:success failure:failure];
}

-(NSURLSessionTask *)requestDrawRotaryTableSuccess:(HttpRequestSuccess)success
                                                    failure:(HttpRequestFailed)failure
{
    return [self POST:TURNTABLE_DRAWROTARY RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:nil responseCache:nil success:success failure:failure];
}

@end
