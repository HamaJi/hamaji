//
//  HomeAction.m
//  Bet365
//
//  Created by adnin on 2018/11/11.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "HomeAction.h"

#pragma mark - Commond
#import "RequestUrlHeader.h"

#pragma mark - Data
#import "HomePageFactory.h"
#import "MainAllocationModel.h"
#import "Bet365ElectronicModel.h"
#import "AllLiveData.h"
#import "SportEleLiveModel.h"

#pragma mark - View
#import "HomeCollectionViewCell.h"
#import "HomeNewCollectionViewCell.h"
#import "HomeBannerHeadView.h"
#import "HomeHorCycleHeadView.h"
#import "HomeLotteryHeadView.h"
#import "HomeWinHeadView.h"
#import "HomeHengFuReusableView.h"
#import "HomeDefaultCell.h"
#import "HomeCusHeadView.h"
#import "HomeLobbyHeadView.h"
#import "HomeWaterFallCollectionReusableView.h"
#import "HomeQuick3CollectionReusableView.h"
#import "HomeSliderHeadView.h"
#import "HomeUserOnlineView.h"


#pragma mark - Controller
#import "NoteViewController.h"
#import "GuideViewController.h"
#import "LukeNoticeViewController.h"
#import "ChatViewController.h"

#pragma mark - Animator
#import "DismissingAlphaAnimator.h"
#import "DismissingTopAnimator.h"
#import "PresentingAlphaAnimator.h"
#import "PresentingTopAnimator.h"


#define TIME_CYCLE_USERONLINE @"TIME_CYCLE_USERONLINE"

@interface HomeAction()
<
UICollectionViewDelegate,
UICollectionViewDataSource,
UICollectionViewFlowColorLayoutDelegate,
UIViewControllerTransitioningDelegate,
HomeCusHeadViewDelegate,
HomeHorCycleHeadViewDelegate,
HomeLotteryHeadViewDelegate,
HomeBannerHeadViewwDelegate,
HomeSliderHeadViewDelegate,
HomeHengFuReusableViewDelegate,
HomeLobbyHeadViewDelegate,
HomeWaterFallCollectionReusableViewDelegate,
HomeQuick3CollectionReusableViewDelegate,
UIGestureRecognizerDelegate,
TimerManagerDelegate>
{
    NSInteger _kCusIndex; //自定义模块下标
    HomePageKindType _kLotteryType;
    GameCenterType _kLobbyType;
    
    BOOL _mainCanScroll;
}

@property (nonatomic,strong)MainAllocationModel *mainAllocationModel;

@property (nonatomic,strong)NSArray <NSArray <NSString *>*>*bannerUrlList;

@property (nonatomic,strong) Bet365NoticeModel *noticeModel;

@property (nonatomic,strong)NSArray <NSArray <KFCPHomeGameJsonModel *>*>*lotteryList;

@property (nonatomic,strong)NSArray <KFCPGameWinNoticeModel *>*winList;

@property (nonatomic,strong) Bet365HengFuModel *horCanvaModel;

@property (nonatomic,strong)HomeOpenWinCamp *allOpenInfo;

@property (nonatomic,strong) NSArray *gameLobbyData;

@property (nonatomic,strong) NSArray *slideList;

@property (nonatomic,assign) CGFloat slideHeight;

@property (nonatomic,strong) NSArray <GameCenterModel *> *lobbyData;

@property (nonatomic,strong) NSArray <GameLobby3Model *> *lobby3Data;

@property (nonatomic,strong) NSArray <SportEleLiveModel *> *dpList;

@property (nonatomic,weak) HomeWaterFallCollectionReusableView *lobby3Head;

@property (nonatomic,assign) HomeWallterFallScrollStatus mainScrollStatus;

@property (nonatomic,strong) HomeUserOnlineView *onlineView;

@property (nonatomic,strong) UIPanGestureRecognizer *pan;


@end

@implementation HomeAction

#pragma mark - UICollectionViewDataSource
-(instancetype)init{
    if (self = [super init]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            @weakify(self);
//            [[RACSignal combineLatest:@[HOME_FACTORY.updateSubject,
//                                        LOTTERY_FACTORY.updateSubject,
//                                        BET_CONFIG.updateSubject,
//                                        RACObserve(USER_DATA_MANAGER, isLogin),
//                                        RACObserve(BET_CONFIG, noticeModel)] reduce:^id _Nonnull(NSNumber *homeSign,NSNumber *lotterhSign,NSNumber *configSign,NSNumber *isLogin,NoticeModel *noticeModel){
//                                            if ([homeSign integerValue] == NetReceiveObjType_RESOPONSE) {
//                                                @strongify(self);
//                                                [self.collectionView.mj_header endRefreshing];
//                                            }
//                                            return nil;
//                                        }] subscribeNext:^(id  _Nullable x) {
//                                            dispatch_async(dispatch_get_main_queue(), ^{
//                                                @strongify(self);
//                                                [self updateData];
//                                            });
//                                        }];
            [SVProgressHUD showWithStatus:@"加载中..."];
            
            
            [[RACSignal merge:@[HOME_FACTORY.updateSubject,
                                LOTTERY_FACTORY.updateSubject,
                                BET_CONFIG.updateSubject,
                                ALL_DATA.updateSubject,
                                RACObserve(USER_DATA_MANAGER, isLogin)]] subscribeNext:^(id  _Nullable x) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    @strongify(self);
                    [self.collectionView.mj_header endRefreshing];
                    [self updateData];
                    [SVProgressHUD dismiss];
                });
            }];
            
            [BET_CONFIG getNoticeCompleted:^(BOOL success) {
                self.noticeModel = BET_CONFIG.noticeModel;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.collectionView reloadData];
                    [UIViewController routerJumpToUrl:kRouterNoticeIndex];
                });
            }];
            [[RACObserve(self, lobbyData) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
                NSLog(@"");
            }];
            
            
        });

    }
    return self;
}

-(void)updateData{

    _kCusIndex = 0;
    _kLotteryType = [BET_CONFIG.config.play_type_config integerValue] == 1 ? HomePageCreditType : HomePageOfficalType;
    self.mainAllocationModel = BET_CONFIG.allocation;
    self.lotteryList = LOTTERY_FACTORY.lotteryClassifyList;
    self.allOpenInfo = LOTTERY_FACTORY.allOpenInfo;
    self.winList = LOTTERY_FACTORY.winList;
    
    self.lobbyData = ALL_DATA.lobbyData;
    _kLobbyType = self.lobbyData.firstObject.type;
    self.bannerUrlList = HOME_FACTORY.bannerUrlList;
    self.horCanvaModel = HOME_FACTORY.horCanvaModel;
    self.dpList = HOME_FACTORY.zhenrenList;
    self.lobby3Data = ALL_DATA.lobby3ModelList;
    self.slideList = BET_CONFIG.sliderList;
    [self.collectionView reloadData];
    [self getImageHeithCompelted:^{
        [self.collectionView reloadData];
        if (self.lobbyData.count > 0) {
            [self HomeLobbyHeadViewTap:_kLobbyType segmentTap:@"all" model:nil];
        }
    }];
    __block BOOL showOnline = NO;
    [self.mainAllocationModel.templateList_flex enumerateObjectsUsingBlock:^(HomeFlexTemplate * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.type == HomeFlexTemplateType_AppOnLineCount) {
            showOnline = YES;
            *stop = YES;
        }
    }];
    if (showOnline) {
        self.onlineView.hidden = NO;
        [TIMER_MANAGER addDelegate:self];
        [TIMER_MANAGER addCycleTimerWithKey:TIME_CYCLE_USERONLINE andReduceScope:8];
    }else{
        self.onlineView.hidden = YES;
        [TIMER_MANAGER removeTimerWithKey:TIME_CYCLE_USERONLINE];
        [TIMER_MANAGER removeDelegate:self];
    }
}

-(void)dealloc{
    self.collectionView.delegate = nil;
    self.collectionView.dataSource = nil;
}

-(void)kViewWillDisappear{
    __block HomeHorCycleHeadView *noticeView = nil;
    [[self.collectionView visibleSupplementaryViewsOfKind:UICollectionElementKindSectionHeader] enumerateObjectsUsingBlock:^(UICollectionReusableView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:HomeHorCycleHeadView.class]) {
            noticeView = (HomeHorCycleHeadView *)obj;
            *stop = YES;
        }
    }];
    if (noticeView && self.noticeModel) {
        [noticeView stopHorCycleAnimation];
    }
}

-(void)kViewDidAppear{
    __block HomeHorCycleHeadView *noticeView = nil;
    [[self.collectionView visibleSupplementaryViewsOfKind:UICollectionElementKindSectionHeader] enumerateObjectsUsingBlock:^(UICollectionReusableView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:HomeHorCycleHeadView.class]) {
            noticeView = (HomeHorCycleHeadView *)obj;
            *stop = YES;
        }
    }];
    if (noticeView && self.noticeModel) {
        noticeView.items = self.noticeModel.roll_notice;
        noticeView.delegate = self;
    }
}
#pragma mark - TimerManagerDelegate
-(void)cycleTimerWithKey:(NSString *)key RestCount:(NSInteger)count{
    if ([TIME_CYCLE_USERONLINE isEqualToString:key]) {
        @weakify(self);
        [HomePageFactory getUserOnlineCountCompleted:^(NSUInteger count) {
            @strongify(self);
            self.onlineView.online = count;
        }];
    }
}
#pragma mark - UICollectionViewDataSource
- (UIColor *)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout backgroundColorForSection:(NSInteger)section{
    HomeFlexTemplate *template =  self.mainAllocationModel.templateList_flex[section];
    if (template.type == HomeFlexTemplateType_CUS) {
        return [UIColor skinViewCusBgColor];
    }else if (template.type == HomeFlexTemplateType_INDEX_QUICK){
        return [UIColor skinViewQuickBgColor];
    }
    return [UIColor clearColor];
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return  self.mainAllocationModel.templateList_flex.count;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    HomeFlexTemplate *template =  self.mainAllocationModel.templateList_flex[section];
    switch (template.type) {
        case HomeFlexTemplateType_CUS:
        {
            CusTemplate *template = [self.mainAllocationModel.templateList_cus safeObjectAtIndex:_kCusIndex];
            if (template) {
                return template.items.count;
            }else{
                return 0;
            }
        }
        case HomeFlexTemplateType_INDEX_DP:
            if (BET_CONFIG.common_config.isFlexDp) {
                return self.mainAllocationModel.templateList_dpGame.count;
            }else{
                return self.dpList.count;
            }
            
        case HomeFlexTemplateType_INDEX_WINNER:
            return 0;
        case HomeFlexTemplateType_INDEX_QUICK:
            
            return templateList_quick_login(self.mainAllocationModel.templateList_quick).count;
        case HomeFlexTemplateType_INDEX_QUICK2:
            
            return templateList_quick_login(self.mainAllocationModel.templateList_quick).count;
        case HomeFlexTemplateType_NOTICE:
            return 0;
        case HomeFlexTemplateType_LOTTERY:
        {
            NSInteger count = [(NSArray *)[self.lotteryList safeObjectAtIndex:_kLotteryType] count];
            if (count > 15) {
                count = 15;
            }
            if (count % 2) {
                return count + 1;
            }
            return count;
        }
        case HomeFlexTemplateType_BANNER:
            return 0;
        case HomeFlexTemplateType_SLIDER:
            return 0;
        case HomeFlexTemplateType_Lobby:
        {
            return self.gameLobbyData.count;
        }
        default:
            return 0;
    }
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    HomeFlexTemplate *template =  self.mainAllocationModel.templateList_flex[section];
    if (template.type == HomeFlexTemplateType_INDEX_DP) {
        return UIEdgeInsetsMake(0, HomeCollectionCellSpacing_GAME, 0, HomeCollectionCellSpacing_GAME);
    }else if (template.type == HomeFlexTemplateType_CUS){
        return UIEdgeInsetsMake(0, HomeCollectionCellSpacing_CUS, 0, HomeCollectionCellSpacing_CUS);
    }else if (template.type == HomeFlexTemplateType_Lobby){
        if (_kLobbyType != GameCenterType_lottery) {
            return UIEdgeInsetsMake(0, HomeCollectionCellSpacing_LOBBY, 0, HomeCollectionCellSpacing_LOBBY);
        }
    }else if (template.type == HomeFlexTemplateType_INDEX_QUICK2) {
        return UIEdgeInsetsMake(0, HomeCollectionCellSpacing_QUICK2, 0, HomeCollectionCellSpacing_QUICK2);
    }
    return UIEdgeInsetsMake(0, 0, 0, 0);
    
}
- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    HomeFlexTemplate *template =  self.mainAllocationModel.templateList_flex[section];
    if (template.type == HomeFlexTemplateType_INDEX_DP) {
        return HomeCollectionCellSpacing_GAME;
    }else if (template.type == HomeFlexTemplateType_INDEX_QUICK){
        return HomeCollectionCellSpacing_QUICK;
    }else if (template.type == HomeFlexTemplateType_CUS){
        return HomeCollectionCellSpacing_CUS;
    }else if (template.type == HomeFlexTemplateType_Lobby){
        if (_kLobbyType != GameCenterType_lottery) {
            return HomeCollectionCellSpacing_LOBBY;
        }
    }
    return 0.0;
}
- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    HomeFlexTemplate *template =  self.mainAllocationModel.templateList_flex[section];
    if (template.type == HomeFlexTemplateType_INDEX_DP) {
        return HomeCollectionCellSpacing_GAME;
    }else if (template.type == HomeFlexTemplateType_Lobby){
        if (_kLobbyType != GameCenterType_lottery) {
            return HomeCollectionCellSpacing_LOBBY;
        }
    }
    return 0.0;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    HomeFlexTemplate *template =  self.mainAllocationModel.templateList_flex[indexPath.section];
    if (template.type == HomeFlexTemplateType_Lobby) {
        if (_kLobbyType == GameCenterType_lottery) {
            return CGSizeMake(WIDTH/2.0, 83);
        }
    }else if (template.type == HomeFlexTemplateType_INDEX_QUICK) {
        NSInteger count = templateList_quick_login(self.mainAllocationModel.templateList_quick).count;
        if (count < HomeCollectionCellMaxCoun_QUICK) {
            return CGSizeMake((WIDTH - HomeCollectionCellSpacing_QUICK * (count - 1)) / count,
                              WIDTH / 5.0 + (IS_IPHONE_5s ?  5 : 10));
        }else{
            return CGSizeMake((WIDTH - HomeCollectionCellSpacing_QUICK * (HomeCollectionCellMaxCoun_QUICK - 1)) / HomeCollectionCellMaxCoun_QUICK,
                              WIDTH / 5.0 + (IS_IPHONE_5s ?  5 : 10));
        }
        
    }else if (template.type == HomeFlexTemplateType_INDEX_QUICK2){
        NSInteger count = templateList_quick_login(self.mainAllocationModel.templateList_quick).count;
        if (count < HomeCollectionCellMaxCoun_QUICK) {
            return CGSizeMake((WIDTH - HomeCollectionCellSpacing_QUICK2 * 2) / count,
                              WIDTH / 5.0 + (IS_IPHONE_5s ?  5 : 10));
        }else{
            return CGSizeMake((WIDTH - HomeCollectionCellSpacing_QUICK2 * 2) / HomeCollectionCellMaxCoun_QUICK,
                              WIDTH / 5.0 + (IS_IPHONE_5s ?  5 : 10));
        }
    }
    return homeNewCellSize(template.type);
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    HomeFlexTemplate *template =  self.mainAllocationModel.templateList_flex[section];
    switch (template.type) {
        case HomeFlexTemplateType_CUS:
            return CGSizeMake(WIDTH, WIDTH / 4.0 + 12.0);
        case HomeFlexTemplateType_INDEX_DP:
            return CGSizeMake(WIDTH, 5.0);
        case HomeFlexTemplateType_INDEX_WINNER:
            return CGSizeMake(WIDTH, 37);
        case HomeFlexTemplateType_INDEX_QUICK2:
            return CGSizeMake(WIDTH, 40);
        case HomeFlexTemplateType_NOTICE:
            return CGSizeMake(WIDTH, 25);
        case HomeFlexTemplateType_LOTTERY:
        {
            if (![(NSArray *)[self.lotteryList safeObjectAtIndex:HomePageOfficalType] count]) {
                return CGSizeMake(0.0, 0.0);
            }else{
                return CGSizeMake(WIDTH,113.0);
            }
        }
        case HomeFlexTemplateType_BANNER:
        {
            if (!self.bannerUrlList.count || ![(NSArray *)[self.bannerUrlList safeObjectAtIndex:0] count]) {
                return CGSizeMake(0.0, 0.0);
            }else{
                return CGSizeMake(WIDTH,BET_CONFIG.config.lunboHeight);
            }
        }
        case HomeFlexTemplateType_SLIDER:
        {
            if (!self.slideList.count) {
                return CGSizeMake(0.0, 0.0);
            }else{
                if (self.slideHeight != 0) {
                    return CGSizeMake(WIDTH,self.slideHeight);
                }
                return CGSizeMake(WIDTH,100);
            }
        }
        case HomeFlexTemplateType_HORCANVA:
        {
            if (!self.horCanvaModel || !self.horCanvaModel.hfImage.length) {
                return CGSizeMake(0.0, 0.0);
            }else{
                CGFloat getSizeWidth = 1194;
                CGFloat getSizeHeight = 247;
                CGFloat scale = WIDTH / getSizeWidth;
                CGFloat scaleResultHeight = getSizeHeight * scale;
                return CGSizeMake(WIDTH,scaleResultHeight);
            }
        }
        case HomeFlexTemplateType_Lobby:
        {
            if (self.lobbyData.count == 0) {
                return CGSizeZero;
            }else if (_kLobbyType == GameCenterType_lottery || _kLobbyType == GameCenterType_electricContest || _kLobbyType == GameCenterType_ele || _kLobbyType == GameCenterType_chess) {
                return CGSizeMake(WIDTH,105.0);
            }else{
                return CGSizeMake(WIDTH,70.0);
            }
        }
        case HomeFlexTemplateType_Lobby3:
        {
            if (self.lobby3Data.count == 0) {
                return CGSizeZero;
            }
            CGFloat height = self.lobby3Data.count * 50 + 5;
            return CGSizeMake(WIDTH, MAX(height, 355));
        }
        default:
            return CGSizeZero;
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    HomeFlexTemplate *template =  self.mainAllocationModel.templateList_flex[section];
    if (template.type == HomeFlexTemplateType_INDEX_WINNER) {
        return CGSizeMake(WIDTH, 240);
    }else if (template.type == HomeFlexTemplateType_INDEX_DP ||
             template.type == HomeFlexTemplateType_CUS){
        return CGSizeMake(WIDTH, 5);
    }else if (template.type == HomeFlexTemplateType_INDEX_QUICK2){
        return CGSizeMake(WIDTH, 5);
    }
    return CGSizeZero;;
}
-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        HomeFlexTemplate *template =  self.mainAllocationModel.templateList_flex[indexPath.section];
        UICollectionReusableView *defaultView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:collectionHeadIdentifier(template.type) forIndexPath:indexPath];
        switch (template.type) {
            case HomeFlexTemplateType_CUS:
            {
                HomeCusHeadView *view = (HomeCusHeadView *)defaultView;
                view.index = _kCusIndex;
                view.horPageMaxCount = 5;
                view.items = self.mainAllocationModel.templateList_cus;
                view.delegate = self;
                
            }
                break;
            case HomeFlexTemplateType_INDEX_WINNER:
            {
                HomeWinHeadView *view = (HomeWinHeadView *)defaultView;
                [view updateCurrenPaoMa:self.winList];
            }
                break;
            case HomeFlexTemplateType_NOTICE:
            {
                HomeHorCycleHeadView *view = (HomeHorCycleHeadView *)defaultView;
                view.items = self.noticeModel.roll_notice;
                view.delegate = self;
            }
                break;
            case HomeFlexTemplateType_LOTTERY:
            {
                HomeLotteryHeadView *view = (HomeLotteryHeadView *)defaultView;
                [view creatUIAndCurrenType:_kLotteryType];
                view.delegate = self;
            }
                break;
            case HomeFlexTemplateType_BANNER:
            {
                HomeBannerHeadView *view = (HomeBannerHeadView *)defaultView;
                view.imageURLStringsGroup = [self.bannerUrlList safeObjectAtIndex:0];
                view.delegate = self;
            }
                break;
            case HomeFlexTemplateType_SLIDER:
            {
                HomeSliderHeadView *view = (HomeSliderHeadView *)defaultView;
                view.list = self.slideList;
                view.delegate = self;
            }
                
                break;
            case HomeFlexTemplateType_HORCANVA:
            {
                HomeHengFuReusableView *view = (HomeHengFuReusableView *)defaultView;
                view.delegate = self;
                view.model = self.horCanvaModel;
            }
                break;
            case HomeFlexTemplateType_Lobby:
            {
                HomeLobbyHeadView *view = (HomeLobbyHeadView *)defaultView;
                view.delegate = self;
                view.lobbyData = self.lobbyData;
                
            }
                break;
            case HomeFlexTemplateType_Lobby3:
            {
                HomeWaterFallCollectionReusableView *view = (HomeWaterFallCollectionReusableView *)defaultView;
                view.delegate = self;
                view.list = self.lobby3Data;
                self.lobby3Head = view;
            }
                break;
                
            case HomeFlexTemplateType_INDEX_QUICK2:
            {
                HomeQuick3CollectionReusableView *view = (HomeQuick3CollectionReusableView *)defaultView;
                view.dateHour = [[NSDate date] hour];
                view.delegate = self;
            }
                break;
                
                
            default:
                break;
        }
        return defaultView;
    }else{
        UICollectionReusableView *defaultView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"foot" forIndexPath:indexPath];
        return defaultView;
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    HomeFlexTemplate *template =  self.mainAllocationModel.templateList_flex[indexPath.section];
    if (template.type == HomeFlexTemplateType_Lobby) {
        id model = self.gameLobbyData[indexPath.item];
        if ([model isKindOfClass:KFCPHomeGameJsonModel.class]) {
            HomeDefaultCell *defaultCell = [collectionView dequeueReusableCellWithReuseIdentifier:collectionCellIdentifier(HomeFlexTemplateType_LOTTERY) forIndexPath:indexPath];
            [defaultCell updateCurrenDes:((KFCPHomeGameJsonModel *)model).name
                                 AndDate:((KFCPHomeGameJsonModel *)model).openFrequency
                         AndLogImageName:[NSString stringWithFormat:jesse(@"/images/gameLogo/%@.png"),((KFCPHomeGameJsonModel *)model).GameId]
                              AndKindLog:nil];
            return defaultCell;
        }
        if ([model isKindOfClass:Bet365ElectronicModel.class]) {
            HomeNewCollectionViewCell *defaultCell = [collectionView dequeueReusableCellWithReuseIdentifier:collectionCellIdentifier(HomeFlexTemplateType_CUS) forIndexPath:indexPath];
            defaultCell.model = model;
            return defaultCell;
        }
        
        if ([model isKindOfClass:AllLiveGamesModel.class]) {
            HomeNewCollectionViewCell *defaultCell = [collectionView dequeueReusableCellWithReuseIdentifier:collectionCellIdentifier(HomeFlexTemplateType_CUS) forIndexPath:indexPath];
            if (_kLobbyType == GameCenterType_live || _kLobbyType == GameCenterType_sports) {
                defaultCell.allModel = model;
            }else{
                defaultCell.allModel = model;
            }
            return defaultCell;
        }
    }else{
        UICollectionViewCell *defaultCell = [collectionView dequeueReusableCellWithReuseIdentifier:collectionCellIdentifier(template.type) forIndexPath:indexPath];
        switch (template.type) {
                case HomeFlexTemplateType_CUS:
            {
                CusTemplate *cus_template = [self.mainAllocationModel.templateList_cus safeObjectAtIndex:_kCusIndex];
                CusTemplateItem *item = [cus_template.items safeObjectAtIndex:indexPath.item];
                HomeNewCollectionViewCell *cell = (HomeNewCollectionViewCell *)defaultCell;
                cell.cusTemplate = item;
            }
                break;
                case HomeFlexTemplateType_INDEX_QUICK:
            {
                
                QuickTemplate *item = [templateList_quick_login(self.mainAllocationModel.templateList_quick) safeObjectAtIndex:indexPath.item];
                HomeNewCollectionViewCell *cell = (HomeNewCollectionViewCell *)defaultCell;
                cell.quickTemplate = item;
                cell.quickColor = getHomeCollectionQuickCellTextColor(indexPath);
//                cell.quickColor = [UIColor skinTextItemNorColor];
                
            }
                break;
            case HomeFlexTemplateType_INDEX_QUICK2:
            {
                
                QuickTemplate *item = [templateList_quick_login(self.mainAllocationModel.templateList_quick) safeObjectAtIndex:indexPath.item];
                HomeNewCollectionViewCell *cell = (HomeNewCollectionViewCell *)defaultCell;
                cell.quickTemplate = item;
                NSUInteger count =  templateList_quick_login(self.mainAllocationModel.templateList_quick).count;
                if (indexPath.item == 0) {
                    [cell setContentArc:UIRectCornerBottomLeft radii:CGSizeMake(10, 10)];
                }else if(indexPath.item + 1 == count){
                    [cell setContentArc:UIRectCornerBottomRight radii:CGSizeMake(10, 10)];
                }
                
                cell.quickColor = COLOR_WITH_HEX(0x77674d);
                
            }
                break;
                case HomeFlexTemplateType_INDEX_DP:
            {
                HomeNewCollectionViewCell *cell = (HomeNewCollectionViewCell *)defaultCell;
                if (BET_CONFIG.common_config.isFlexDp) {
                    DpTemplate *item = [self.mainAllocationModel.templateList_dpGame safeObjectAtIndex:indexPath.item];
                    cell.dpTemplate = item;
                }else{
                    SportEleLiveModel *item = [self.dpList safeObjectAtIndex:indexPath.item];
                    cell.item = item;
                }
                
            }
                break;
                case HomeFlexTemplateType_LOTTERY:
            {
                HomeDefaultCell *cell = (HomeDefaultCell *)defaultCell;
                KFCPHomeGameJsonModel *model = [[self.lotteryList safeObjectAtIndex:_kLotteryType] safeObjectAtIndex:indexPath.item];
                if (model && indexPath.item < 15) {
                    [cell updateCurrenDes:model.name
                                  AndDate:model.openFrequency
                          AndLogImageName:[NSString stringWithFormat:jesse(@"/images/gameLogo/%@.png"),model.GameId]
                               AndKindLog:(_kLotteryType == HomePageOfficalType) ? @"官" : (((NSArray *)[self.lotteryList safeObjectAtIndex:0]).count==0)?@"":@"信"];
                }else{
                    [cell updateCurrenDes:@"更多游戏" AndDate:@"更多游戏玩法" AndLogImageName:@"" AndKindLog:@""];
                    [cell upMoreImage:[UIImage imageNamed:@"moreGame"]];
                }
            }
                break;
            default:
                break;
        }
        return defaultCell;
    }
    return nil;
}

#pragma mark - UICollectionViewDelegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    HomeFlexTemplate *template =  self.mainAllocationModel.templateList_flex[indexPath.section];
    RouterKey *routerKey = nil;
    __block MenuAttrTemplate *attrItem = nil;
    switch (template.type) {
        case HomeFlexTemplateType_CUS:
        {
            CusTemplate *cus_template = [self.mainAllocationModel.templateList_cus safeObjectAtIndex:_kCusIndex];
            CusTemplateItem *item = [cus_template.items safeObjectAtIndex:indexPath.item];
            routerKey = item.link;
            if (item.type == TemplateJumpType_ROUTER_PARAMERS) {
                attrItem = item.attr;
            }
            
        }
            break;
        case HomeFlexTemplateType_INDEX_QUICK:
        {
            QuickTemplate *item = [templateList_quick_login(self.mainAllocationModel.templateList_quick) safeObjectAtIndex:indexPath.item];
            routerKey = item.link;
        }
            break;
        case HomeFlexTemplateType_INDEX_QUICK2:
        {
            QuickTemplate *item = [templateList_quick_login(self.mainAllocationModel.templateList_quick) safeObjectAtIndex:indexPath.item];
            routerKey = item.link;
        }
            break;
        case HomeFlexTemplateType_INDEX_DP:
        {
            if (BET_CONFIG.common_config.isFlexDp) {
                DpTemplate *item = [self.mainAllocationModel.templateList_dpGame safeObjectAtIndex:indexPath.item];
                routerKey = item.link;
            }else{
                SportEleLiveModel *item = [self.dpList safeObjectAtIndex:indexPath.item];
                if (!item._b) {
                    if (item.href.length > 0) {
                        routerKey = item.href;
                    }else{
                        [ALL_DATA.liveDatas enumerateObjectsUsingBlock:^(AllLiveGamesModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            if ([item.type isEqualToString:obj.liveCode]) {
                                attrItem = [MenuAttrTemplate new];
                                attrItem.liveCode = obj.liveCode;
                                attrItem.gameKind = obj.kind;
                                attrItem.gameType = obj.gameType;
                                *stop = YES;
                            }
                        }];
                    }
                }else{
//                    NSString *url = item.link;
//                    if (![item.link hasPrefix:@"http"]) {
//                        url = [NSString stringWithFormat:@"%@%@",SerVer_Url,item.link];
//                    }
                    routerKey = item.href;
                }
            }

        }
            break;
        case HomeFlexTemplateType_LOTTERY:
        {
            KFCPHomeGameJsonModel *item = [[self.lotteryList safeObjectAtIndex:_kLotteryType] safeObjectAtIndex:indexPath.item];
            if (!item || indexPath.item > 14) {
                routerKey = kRouterGCDT;
            }else {
                if (_kLotteryType == HomePageOfficalType) {
                    routerKey = [NSString stringWithFormat:@"%@/%@/official",kRouterLottery,item.GameId];
                }else{
                    routerKey = [NSString stringWithFormat:@"%@/%@/credit",kRouterLottery,item.GameId];
                }
            }
        }
            break;
        case HomeFlexTemplateType_Lobby:
        {
            if (_kLobbyType == GameCenterType_lottery) {
                KFCPHomeGameJsonModel *item = [self.gameLobbyData safeObjectAtIndex:indexPath.item];
                if (_kLotteryType == HomePageOfficalType) {
                    routerKey = [NSString stringWithFormat:@"%@/%@/official",kRouterLottery,item.GameId];
                }else{
                    routerKey = [NSString stringWithFormat:@"%@/%@/credit",kRouterLottery,item.GameId];
                }
            }else if (_kLobbyType == GameCenterType_live || _kLobbyType == GameCenterType_sports) {
                AllLiveGamesModel *allModel = self.gameLobbyData[indexPath.item];
                attrItem = [MenuAttrTemplate new];
                attrItem.liveCode = allModel.liveCode;
                attrItem.gameKind = allModel.kind;
                attrItem.gameType = allModel.gameType;
//                routerKey = allModel.url;
            }else{
                Bet365ElectronicModel *model = self.gameLobbyData[indexPath.item];
                attrItem = [MenuAttrTemplate new];
                attrItem.liveCode = model.liveCode;
                attrItem.gameKind = model.firstKind;
                attrItem.gameType = model.gameType;
            }
        }
            break;
            
        default:
            break;
    }
    if (attrItem) {
        [UIViewController routerJumpToAttr:attrItem];
    }else if(routerKey){
//        if ([routerKey containsString:@"liveRedirect.html"]) {
//            [UIViewController routerJumpToUrl:[NSString stringWithFormat:@"%@%@",SerVer_Url,routerKey]];
//        }else{
//            [UIViewController routerJumpToUrl:routerKey];
//        }
        [UIViewController routerJumpToUrl:routerKey];
    }
}

#pragma mark - HomeCusHeadViewDelegate
-(void)HomeCusHeadView:(HomeCusHeadView *)view TapIndex:(NSInteger)idx{
    _kCusIndex = idx;
    __block NSInteger section = NSNotFound;
    [self.mainAllocationModel.templateList_flex enumerateObjectsUsingBlock:^(HomeFlexTemplate * _Nonnull obj, NSUInteger index, BOOL * _Nonnull stop) {
        if (obj.type == HomeFlexTemplateType_CUS) {
            section = index;
            *stop = YES;
        }
    }];
    if (section != NSNotFound) {
//        [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:section]];
        [self.collectionView reloadData];
    }
    
}

#pragma mark - HomeHorCycleHeadViewDelegate
-(void)HomeHorCycleHeadView:(HomeHorCycleHeadView *)view TaphorCircleView:(aji_horCircleView *)horCircleView{
    if ([USER_DATA_MANAGER shouldPush2Login]) {
        LukeNoticeViewController *vc = [[LukeNoticeViewController alloc] init];
        vc.noticeType = NoticeType_All;
        [((UIViewController *)self.delegate).navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - HomeLotteryHeadViewDelegate
-(void)HomeLotteryHeadView:(HomeLotteryHeadView *)view tapLotteryType:(HomePageKindType)type{
    _kLotteryType = type;
    [self.collectionView reloadData];
}

#pragma mark - HomeBannerHeadViewwDelegate
-(void)HomeBannerHeadView:(HomeBannerHeadView *)view TapSDCycleIdx:(NSUInteger)idx{
    NSString *url = [self.bannerUrlList lastObject][idx];
    if (![url includeChinese] && url.length > 0) {
        [UIViewController routerJumpToUrl:url];
    }
}

#pragma mark - HomeSliderHeadViewDelegate
-(void)HomeSliderHeadView:(HomeSliderHeadView *)view TapSlider:(SliderModel *)slider{
//    NSString *url = [self.bannerUrlList lastObject][idx];
    if (slider.url.length) {
        [UIViewController routerJumpToUrl:slider.url];
    }
    
}

-(void)HomeSliderHeadView:(HomeSliderHeadView *)view UpdateHeight:(CGFloat)height{
    if (self.slideHeight >= height) {
        return;
    }
    self.slideHeight = height;
    [self.collectionView reloadData];
    dispatch_async(dispatch_get_main_queue(), ^{
        [view reloadData];
    });
    
    
}

#pragma mark - HomeHengFuReusableViewDelegate
- (void)HomeHengFuReusableViewTapwebUrlWithUrl:(NSString *)webUrl{
    [UIViewController routerJumpToUrl:webUrl];
}

#pragma mark - HomeHorCycleHeadViewDelegate
- (void)HomeHorCycleHeadView:(HomeHorCycleHeadView *)view TapUpAtIndex:(NSUInteger)idx {
    
}

#pragma mark - HomeLobbyHeadViewDelegate
- (void)HomeLobbyHeadViewTap:(GameCenterType)lobbyType
{
    _kLobbyType = lobbyType;
    self.gameLobbyData = nil;
    [self.collectionView reloadData];
    @weakify(self);
    [ALL_DATA getAllLiveDataType:lobbyType cate:@"all" model:nil compelted:^(NSArray * _Nonnull arr) {
        @strongify(self);
        self.gameLobbyData = arr;
        [self.collectionView reloadData];
    }];
}

- (void)HomeLobbyHeadViewTap:(GameCenterType)lobbyType segmentTap:(NSString *)cate model:(AllLiveGamesModel *)model
{
    if ([model.liveCode isEqualToString:@"im"]) {
        [UIViewController routerJumpToCode:model.liveCode Type:model.gameType Kind:model.kind];
    }else{
        @weakify(self);
        [ALL_DATA getAllLiveDataType:lobbyType cate:cate model:model compelted:^(NSArray * _Nonnull arr) {
            @strongify(self);
            self.gameLobbyData = arr;
            [self.collectionView reloadData];
        }];
    }
}

#pragma mark - HomeWaterFallCollectionReusableViewDelegate
- (void)waterFallCollectionReusableView:(HomeWaterFallCollectionReusableView *)reusableView didSelectRowAtUrl:(NSString *)url{
    [UIViewController routerJumpToUrl:url];
}

- (void)waterFallCollectionReusableView:(HomeWaterFallCollectionReusableView *)reusableView SuperScrollEnable:(BOOL)scrollEnable{
    [self.collectionView setScrollEnabled:scrollEnable];
}

- (void)waterFallCollectionReusableView:(HomeWaterFallCollectionReusableView *)reusableView SuperScrollOffsetY:(CGFloat)scrollOffsetY{
//    [self.collectionView setContentOffset:CGPointMake(self.collectionView.contentOffset.x, self.collectionView.contentOffset.y - scrollOffsetY)];
}

#pragma mark - HomeQuick3CollectionReusableViewDelegate
-(void)didTapnHomeQuick3CollectionReusableView:(HomeQuick3CollectionReusableView *)reusableView RouterKey:(RouterKey*)routerKey{
    [UIViewController routerJumpToUrl:routerKey];
}

#pragma mark - UIScrollView
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
}

#pragma mark - Events
-(void)scrollPan:(UIPanGestureRecognizer *)otherGestureRecognizer{
    if (otherGestureRecognizer.state == UIGestureRecognizerStateEnded ||
        otherGestureRecognizer.state == UIGestureRecognizerStateFailed ||
        otherGestureRecognizer.state == UIGestureRecognizerStateCancelled){
        return;
    }
    
    
    UIView *windowView = ((UIViewController *)self.delegate).view;
    CGFloat contentOffsetY = _collectionView.contentOffset.y;
    CGFloat velocity  = [_collectionView.panGestureRecognizer velocityInView:_collectionView].y;
    
    /*  */
    __block HomeWaterFallCollectionReusableView *lobby3Header = nil;
    [[self.collectionView visibleSupplementaryViewsOfKind:UICollectionElementKindSectionHeader] enumerateObjectsUsingBlock:^(UICollectionReusableView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[HomeWaterFallCollectionReusableView class]]) {
            lobby3Header = (HomeWaterFallCollectionReusableView *)obj;
            *stop = YES;
        }
    }];
    
    
    
    
    if (otherGestureRecognizer.state == UIGestureRecognizerStateBegan) {

    }else if (otherGestureRecognizer.state == UIGestureRecognizerStateChanged) {
        
        

        CGFloat velocity = [otherGestureRecognizer velocityInView:otherGestureRecognizer.view].y;
        CGPoint offset = [otherGestureRecognizer translationInView:otherGestureRecognizer.view];
        CGFloat contentOffsetY = self.collectionView.contentOffset.y - offset.y;
        
        if (self.collectionView.contentSize.height <= self.collectionView.bounds.size.height) {
            self.collectionView.contentOffset = CGPointMake(self.collectionView.contentOffset.x, 0);
            return;
        }
        if (contentOffsetY <=0) {
            self.collectionView.contentOffset = CGPointMake(self.collectionView.contentOffset.x, 0);
            return;
        }
        CGFloat crHeight = self.collectionView.bounds.size.height;
        CGFloat ccHiehgt = self.collectionView.contentSize.height;
        if(contentOffsetY + crHeight - ccHiehgt >= -1){
            self.collectionView.contentOffset = CGPointMake(self.collectionView.contentOffset.x, self.collectionView.contentSize.height - self.collectionView.bounds.size.height);
            return;
        }
        [otherGestureRecognizer setTranslation:CGPointMake(0, 0) inView:self.collectionView];
        
        if (lobby3Header && ![lobby3Header getSuperCanScrollWithVelocity:velocity]) {
            return;
        }
        [self.collectionView setContentOffset:CGPointMake(self.collectionView.contentOffset.x, contentOffsetY)];
    }
    
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark - UIViewControllerTransitioningDelegate
-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    if ([dismissed isKindOfClass:[GuideViewController class]]) {
        return [DismissingAlphaAnimator new];
    }else{
        return [DismissingTopAnimator new];
    }
}
-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    if ([presented isKindOfClass:[GuideViewController class]]) {
        return [PresentingAlphaAnimator new];
    }else{
        return [PresentingTopAnimator new];
    }
}

//#pragma mark - NoteViewControllerDelegate
//- (void)notePushViewControllerWithHref:(NSString *)href{
//    [UIViewController routerJumpToUrl:href];
//}

#pragma mark - NetRequest
-(void)getImageHeithCompelted:(void(^)())completed{
    if ([(NSArray *)[self.bannerUrlList safeObjectAtIndex:0] count] > 0) {
        UIImage *bannerImage = [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:self.bannerUrlList[0][0]];
        if (bannerImage) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"轮播高度(蛤蟆吉)获取成功");
                CGFloat getSizeWidth = CGImageGetWidth(bannerImage.CGImage);
                CGFloat getSizeHeight = CGImageGetHeight(bannerImage.CGImage);
                CGFloat scale = WIDTH/getSizeWidth;
                CGFloat scaleResultHeight = getSizeHeight * scale;
                BET_CONFIG.config.lunboHeight = scaleResultHeight;
                [BET_CONFIG.config save];
                completed ? completed() : nil;
            });
        }else{
            
            
            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:self.bannerUrlList[0][0]] options:SDWebImageDownloaderContinueInBackground progress:nil
                                                                completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                                                                    if (finished&&!error) {
                                                                        CGFloat getSizeWidth = CGImageGetWidth(image.CGImage);
                                                                        CGFloat getSizeHeight = CGImageGetHeight(image.CGImage);
                                                                        CGFloat scale = WIDTH/getSizeWidth;
                                                                        CGFloat scaleResultHeight = getSizeHeight * scale;
                                                                        BET_CONFIG.config.lunboHeight = scaleResultHeight;
                                                                    }else{
                                                                        BET_CONFIG.config.lunboHeight = 0.0;
                                                                    }
                                                                    [BET_CONFIG.config save];
                                                                    completed ? completed() : nil;
                                                                }];
        }
    }else{
        BET_CONFIG.config.lunboHeight = 0.0;
        [BET_CONFIG.config save];
        completed ? completed() : nil;
    }
}

#pragma mark - GET/SET
-(void)setNoticeModel:(Bet365NoticeModel *)noticeModel{
    _noticeModel = noticeModel;
}

-(void)setCollectionView:(HomeCollectionView *)collectionView{
    /** 轮播图*/
    [collectionView registerClass:[HomeBannerHeadView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:collectionHeadIdentifier(HomeFlexTemplateType_BANNER)];
    
    [collectionView registerClass:[HomeSliderHeadView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:collectionHeadIdentifier(HomeFlexTemplateType_SLIDER)];
    
    
    /** 公告栏*/
    [collectionView registerClass:[HomeHorCycleHeadView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:collectionHeadIdentifier(HomeFlexTemplateType_NOTICE)];
    
    /** 彩票*/
    [collectionView registerClass:[HomeLotteryHeadView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:collectionHeadIdentifier(HomeFlexTemplateType_LOTTERY)];
    
    /** 中奖列表*/
    [collectionView registerClass:[HomeWinHeadView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:collectionHeadIdentifier(HomeFlexTemplateType_INDEX_WINNER)];
    
    /** 自定义Head*/
    [collectionView registerClass:[HomeCusHeadView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:collectionHeadIdentifier(HomeFlexTemplateType_CUS)];
    
    /** 横幅*/
    [collectionView registerClass:[HomeHengFuReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:collectionHeadIdentifier(HomeFlexTemplateType_HORCANVA)];
    
    /** 大厅二*/
    [collectionView registerClass:[HomeLobbyHeadView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:collectionHeadIdentifier(HomeFlexTemplateType_Lobby)];
    
    /** 大厅3*/
    [collectionView registerClass:[HomeWaterFallCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:collectionHeadIdentifier(HomeFlexTemplateType_Lobby3)];
    
    /** 大厅3*/
    [collectionView registerClass:[HomeQuick3CollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:collectionHeadIdentifier(HomeFlexTemplateType_INDEX_QUICK2)];
    
    
    
    /** Cell*/
    [collectionView registerClass:[HomeDefaultCell class] forCellWithReuseIdentifier:collectionCellIdentifier(HomeFlexTemplateType_LOTTERY)];
    
    [collectionView registerNib:[UINib nibWithNibName:@"HomeNewCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:collectionCellIdentifier(HomeFlexTemplateType_INDEX_QUICK)];
    
    [collectionView registerNib:[UINib nibWithNibName:@"HomeNewCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:collectionCellIdentifier(HomeFlexTemplateType_INDEX_QUICK2)];
    
    [collectionView registerNib:[UINib nibWithNibName:@"HomeNewCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:collectionCellIdentifier(HomeFlexTemplateType_CUS)];
    
    [collectionView registerNib:[UINib nibWithNibName:@"HomeNewCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:collectionCellIdentifier(HomeFlexTemplateType_INDEX_DP)];

    [collectionView registerNib:[UINib nibWithNibName:@"HomeNewCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:collectionCellIdentifier(HomeFlexTemplateType_CUS)];
    
    
    /**占位*/
    [collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:collectionHeadIdentifier(HomeFlexTemplateType_NONE)];
    
    [collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"foot"];
    
    collectionView.mj_header = [MJRefreshStateHeader headerWithRefreshingBlock:^{
        [HOME_FACTORY getAllRequest];
        [BET_CONFIG getConfigCompleted:nil];
    }];
    
    collectionView.delegate = self;
    collectionView.dataSource = self;
    
    
    _collectionView = collectionView;
//    _collectionView.scrollEnabled = NO;
//    [_collectionView addGestureRecognizer:self.pan];
}



//-(NoteViewController *)noteVC{
//    if (!_noteVC) {
//        _noteVC = [[NoteViewController alloc] init];
//        _noteVC.transitioningDelegate = self;
//        _noteVC.modalPresentationStyle = UIModalPresentationCustom;
//        _noteVC.delegate = self;
//    }
//    return _noteVC;
//}

-(void)setLobbyData:(NSArray<GameCenterModel *> *)lobbyData{
    if ([_lobbyData isEqualToArray:lobbyData] ) {
        return;
    }
    _lobbyData = lobbyData;
}

-(UIPanGestureRecognizer *)pan{
    if (!_pan) {
        _pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(scrollPan:)];
        _pan.delegate = self;
    }
    return _pan;
}

-(HomeUserOnlineView *)onlineView{
    if (!_onlineView) {
        _onlineView = [[HomeUserOnlineView alloc] init];
        UIView *view = ((UIViewController *)self.delegate).view;
        [view addSubview:_onlineView];
        [_onlineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(view).offset(-10);
            make.top.equalTo(view).offset(10);
        }];
    }
    return _onlineView;
}

NSString *collectionHeadIdentifier(HomeFlexTemplateType type){
    switch (type) {

        case HomeFlexTemplateType_CUS:
            return @"HomeCusHeadView";
        case HomeFlexTemplateType_INDEX_DP:
            return @"header";
        case HomeFlexTemplateType_INDEX_WINNER:
            return @"HomeWinHeadView";
        case HomeFlexTemplateType_INDEX_QUICK:
            return @"header";
        case HomeFlexTemplateType_NOTICE:
            return @"HomeHorCycleHeadView";
        case HomeFlexTemplateType_LOTTERY:
            return @"HomeLotteryHeadView";
        case HomeFlexTemplateType_BANNER:
            return @"HomeBannerHeadView";
        case HomeFlexTemplateType_SLIDER:
            return @"HomeSliderHeadView";
        case HomeFlexTemplateType_HORCANVA:
            return @"HomeHengFuReusableView";
        case HomeFlexTemplateType_Lobby:
            return @"HomeLobbyHeadView";
        case HomeFlexTemplateType_Lobby3:
            return @"HomeWaterFallCollectionReusableView";
        case HomeFlexTemplateType_INDEX_QUICK2:
            return @"HomeQuick3CollectionReusableView";
        default:
            return @"header";
    }
}


NSString *collectionCellIdentifier(HomeFlexTemplateType type){
    switch (type) {
            
        case HomeFlexTemplateType_INDEX_DP:
            return [NSString stringWithFormat:@"%@_GAME",[HomeNewCollectionViewCell getIdentifier]];
        case HomeFlexTemplateType_INDEX_QUICK:
            return [NSString stringWithFormat:@"%@_QUICK",[HomeNewCollectionViewCell getIdentifier]];
        case HomeFlexTemplateType_INDEX_QUICK2:
            return [NSString stringWithFormat:@"%@_QUICK2",[HomeNewCollectionViewCell getIdentifier]];
        case HomeFlexTemplateType_LOTTERY:
            return @"HomeDefaultCell_LOTTERY";
        case HomeFlexTemplateType_CUS:
            return @"HomeDefaultCell_CUS";
        default:
            return @"cell";
    }
}

UIColor *getHomeCollectionQuickCellTextColor(NSIndexPath *indexPath){
    NSUInteger idx = indexPath.item % 5;
    return @[JesseColor(237, 185, 63),
             JesseColor(0, 156, 136),
             JesseColor(83, 182, 83),
             JesseColor(230,46,37),
             JesseColor(0, 122, 255)
             ][idx];
}

NSArray <QuickTemplate *>*templateList_quick_login(NSArray <QuickTemplate *>*quickTemplateList){
    NSMutableArray *arr = @[].mutableCopy;
    for (QuickTemplate * _Nonnull obj in quickTemplateList) {
        if (USER_DATA_MANAGER.isLogin && obj.displayType == HomeQuickTemplateDisplayType_LOGIN) {
            [arr addObject:obj];
        }else if (!USER_DATA_MANAGER.isLogin && obj.displayType == HomeQuickTemplateDisplayType_UNLOGIN){
            [arr addObject:obj];
        }else if(obj.displayType == HomeQuickTemplateDisplayType_DEFAULTS){
            [arr addObject:obj];
        }
    }
    return arr;
}

@end
