//
//  NetWorkMannager+Home.m
//  Bet365
//
//  Created by HHH on 2018/7/5.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "NetWorkMannager+Home.h"
#import "LukeUserAdapter.h"

@implementation NetWorkMannager (Home)

-(NSURLSessionTask *)requestLunBoImageUrlresponseCache:(HttpRequestCache)responseCache
                                               success:(HttpRequestSuccess)success
                                               failure:(HttpRequestFailed)failure
{
    return [self GET:LunBoImageUrl RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:nil responseCache:responseCache success:success failure:failure];
}

- (NSURLSessionTask *)requestTheSixOpenUrlresponseCache:(HttpRequestCache)responseCache success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:TheSixOpenUrl RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:responseCache success:success failure:failure];
}

-(NSURLSessionTask *)requestSport_ele_liveUrlresponseCache:(HttpRequestCache)responseCache
                                                  success:(HttpRequestSuccess)success
                                                  failure:(HttpRequestFailed)failure
{
    return [self GET:[NSString stringWithFormat:@"%@?_t=%@",sport_ele_liveUrl,[LukeUserAdapter getNowTimeTimestamp]] RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:responseCache success:success failure:failure];
}

- (NSURLSessionTask *)requestGetApphengFuresponseCache:(HttpRequestCache)responseCache success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:APP_HENGFU_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:responseCache success:success failure:failure];
}

- (NSURLSessionTask *)requestGetRedPageWinlistCache:(HttpRequestCache)responseCache success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:HOME_REDPAGE_WINLIST_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:responseCache success:success failure:failure];
}

- (NSURLSessionTask *)requestUserOnlineCountSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:HOME_USERONLINE_USERL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}


- (NSURLSessionTask *)requestGetRedPageInfoCache:(HttpRequestCache)responseCache success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:HOME_REDPAGE_INFO RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:responseCache success:success failure:failure];
}

- (NSURLSessionTask *)requestOpenRedPagesuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:HOME_OPEN_REDPAGE RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetRedPackageNoticesuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:HOME_REDPAGE_NOTICE RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetRedEnvelopeInfosuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:HOME_REDPAGE_ENVELOPE RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestBaseMsgsuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:BASE_MSG_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestPostDrawRedEnvelopesuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self POST:REDPAGE_DRAW_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestPostWeekendWithId:(NSInteger)redId success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:@(redId) forKey:@"id"];
    return [self POST:REDPAGE_WEEKEND_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetRulesWithType:(NSString *)type currenGameType:(currenGameType)gameType gameID:(NSString *)gameID success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSString *official = gameType == CreDicPlayKindTYpe ? type : StringFormatWithStr(type, @"_official");
    NSString *lhc = nil;
    if ([type isEqualToString:@"lhc"] && ![gameID isEqualToString:@"70"]) {
        lhc = @"300";
    }else if ([type isEqualToString:@"fc3d"]) {
        if ([gameID isEqualToString:@"86"] || [gameID isEqualToString:@"83"] || [gameID isEqualToString:@"84"] || [gameID isEqualToString:@"85"] || [gameID isEqualToString:@"87"] || [gameID isEqualToString:@"88"]) {
//            lhc = @"100";
            lhc = @"";
        }else{
            lhc = @"";
        }
    }else {
        lhc = @"";
    }
    return [self GET:[NSString stringWithFormat:@"/data/json/rules/%@%@.json",official,lhc] RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestQueryWeekActivityQualificationSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:USER_DATA_MANAGER.userInfoData.userId forKey:@"userId"];
    return [self GET:REDPAGE_WEEK_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetConfig_open_timeSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:OPEN_TIME_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestGetApp_dpSuccess:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    return [self GET:APP_DP_URL RequestType:PPSerializerType_JSON ResponseType:PPSerializerType_JSON parameters:nil responseCache:nil success:success failure:failure];
}


@end
