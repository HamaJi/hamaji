//
//  HomeAction.h
//  Bet365
//
//  Created by adnin on 2018/11/11.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UICollectionViewFlowColorLayout.h"
#import "HomeCollectionView.h"
@class HomeAction;

@protocol HomeActionDelegate<NSObject>

@end

@interface HomeAction : NSObject

@property (nonatomic,weak)HomeCollectionView *collectionView;

@property (nonatomic,weak)id<HomeActionDelegate> delegate;

-(void)kViewWillDisappear;

-(void)kViewDidAppear;

@end
