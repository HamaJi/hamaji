//
//  NetWorkMannager+Loan.h
//  Bet365
//
//  Created by wind on 2019/11/11.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "NetWorkMannager.h"
#import "RequestUrlHeader.h"

NS_ASSUME_NONNULL_BEGIN

@interface NetWorkMannager (Loan)

- (NSURLSessionTask *)requestLoanRecordUrlParameters:(NSDictionary *)parameters
                                             success:(HttpRequestSuccess)success
                                             failure:(HttpRequestFailed)failure;

//借钱
- (NSURLSessionTask *)requestLoanApplyMoneyUrlParameters:(NSDictionary *)parameters
                                                 success:(HttpRequestSuccess)success
                                                 failure:(HttpRequestFailed)failure;


//还钱
- (NSURLSessionTask *)requestLoanRepayMoneyUrlParameters:(NSDictionary *)parameters
                                                 success:(HttpRequestSuccess)success
                                                 failure:(HttpRequestFailed)failure;

@end

NS_ASSUME_NONNULL_END
