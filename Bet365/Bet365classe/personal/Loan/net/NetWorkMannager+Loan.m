//
//  NetWorkMannager+Loan.m
//  Bet365
//
//  Created by wind on 2019/11/11.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "NetWorkMannager+Loan.h"

@implementation NetWorkMannager (Loan)

- (NSURLSessionTask *)requestLoanRecordUrlParameters:(NSDictionary *)parameters
                                             success:(HttpRequestSuccess)success
                                             failure:(HttpRequestFailed)failure
{
    return [self GET:USER_LOAN_RECORD_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestLoanApplyMoneyUrlParameters:(NSDictionary *)parameters
                                             success:(HttpRequestSuccess)success
                                             failure:(HttpRequestFailed)failure
{
    return [self GET:USER_LOAN_APPLY_MONEY_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

- (NSURLSessionTask *)requestLoanRepayMoneyUrlParameters:(NSDictionary *)parameters
                                                 success:(HttpRequestSuccess)success
                                                 failure:(HttpRequestFailed)failure
{
    return [self GET:USER_LOAN_REPAY_MONEY_URL RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:parameters responseCache:nil success:success failure:failure];
}

@end
