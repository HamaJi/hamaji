//
//  LukePushMessageItem.m
//  Bet365
//
//  Created by luke on 17/6/15.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukePushMessageItem.h"

@implementation LukePushMessageItem

+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"PushID" : @"id"};
}

@end
