//
//  PrmtApplicationModel.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/28.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "PrmtLobbyModel.h"

@implementation PrmtYHApplication
+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"activeId": @"id",
             @"banner": @"banner",
             @"details": @"details",
             };
}

kJSONTransformerMethodImpTemplate(NSValueTransformer *, banner, @selector(iconTransformer))

kJSONTransformerMethodImpTemplate(NSValueTransformer *, details, @selector(iconTransformer))

@end

@implementation PrmtLobbyModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"bottomTxt": @"bottomTxt",
             @"headCenterLogo": @"banner",
             @"headLeftLogo": @"headLeftLogo",
             @"yhApplication":@"yhApplication",
             };
}

kJSONTransformerMethodImpTemplate(NSValueTransformer *, headCenterLogo, @selector(iconTransformer))

kJSONTransformerMethodImpTemplate(NSValueTransformer *, headLeftLogo, @selector(iconTransformer))

+ (NSValueTransformer *)yhApplicationJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSArray * yhApplication){
                NSError *error;
                NSArray *list = [MTLJSONAdapter modelsOfClass:PrmtYHApplication.class fromJSONArray:yhApplication error:&error];
                return list;
            } reverseBlock:^id(NSArray <PrmtYHApplication *>*list) {
                NSError *error;
                NSArray *yhApplication = [MTLJSONAdapter JSONArrayFromModels:list error:&error];
                return yhApplication;
            }];
}
@end
