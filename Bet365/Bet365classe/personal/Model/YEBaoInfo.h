//
//  YEBaoInfo.h
//  Bet365
//
//  Created by super_小鸡 on 2019/6/9.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "MTLModel.h"

NS_ASSUME_NONNULL_BEGIN


@interface YEBaoInerestInfo : MTLModel<MTLJSONSerializing>

@property(copy,nonatomic)NSDate *date;

@property(copy,nonatomic)NSString *settleTime;
@property(copy,nonatomic)NSString *userAccount;

@property(copy,nonatomic)NSNumber *identity;
@property(copy,nonatomic)NSNumber *interestMoney;
@property(copy,nonatomic)NSNumber *interestRate;
@property(copy,nonatomic)NSNumber *userId;
@property(copy,nonatomic)NSNumber *yubaoMoney;


//@property (assign, nonatomic) BOOL hasHistoryAnimation;

@end

@interface YEBaoInfo : MTLModel<MTLJSONSerializing>

@property(copy,nonatomic)NSNumber *interestActiveRate;
@property(copy,nonatomic)NSNumber *interestBaseRate;
@property(copy,nonatomic)NSNumber *isOpen;
@property(copy,nonatomic)NSNumber *money;
@property(copy,nonatomic)NSNumber *maxMoney;
@property(copy,nonatomic)NSNumber *totalInterest;
@property(copy,nonatomic)NSNumber *activeDml;
//@property(copy,nonatomic)NSNumber *activeDml;
//maxMoney

@property(strong,nonatomic)NSMutableArray <YEBaoInerestInfo *>*interest;

@end

NS_ASSUME_NONNULL_END
