//
//  LukeUserRechItem.h
//  Bet365
//
//  Created by luke on 17/7/1.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LukeUserRechItem : NSObject
@property (nonatomic,copy) NSString *rechRemark;

@property (nonatomic,copy) NSString *operatorTime;

@property (nonatomic,assign) NSInteger otherDiscountMoney;

@property (nonatomic,assign) NSInteger UserRechID;

@property (nonatomic,copy) NSString *rechTime;

@property (nonatomic,assign) NSInteger rechStatus;

@property (nonatomic,copy) NSString *ipAddress;

@property (nonatomic,copy) NSString *hyLevel;

@property (nonatomic,copy) NSString *macOs;

@property (nonatomic,copy) NSString *addTime;

@property (nonatomic,assign) NSInteger superId;

@property (nonatomic,copy) NSString *browser;

@property (nonatomic,assign) NSInteger mormDml;

@property (nonatomic,assign) NSInteger rechMoney;

@property (nonatomic,copy) NSString *updateTime;

@property (nonatomic,copy) NSString *superPath;

@property (nonatomic,copy) NSString *rechPerson;

@property (nonatomic,copy) NSString *userNickName;

@property (nonatomic,assign) NSInteger discountMoney;

@property (nonatomic,assign) NSInteger reckonDmlFlag;

@property (nonatomic,copy) NSString *superName;

@property (nonatomic,copy) NSString *userAccount;

@property (nonatomic,assign) NSInteger actualMoney;

@property (nonatomic,assign) NSInteger rechMode;

@property (nonatomic,copy) NSString *rechOrderNo;

@property (nonatomic,assign) NSInteger reckonPointFlag;

@property (nonatomic,assign) NSInteger discountDml;

@property (nonatomic,copy) NSString *operatorAccount;
@property (nonatomic,copy) NSString *rechName;
@property (nonatomic,copy) NSString *payeeName;
@property (nonatomic,assign) NSInteger userId;

@property (nonatomic,assign) NSInteger accountMoney;

@property (nonatomic,assign) NSInteger rechSource;


@end
