//
//  LukeTraceItem.h
//  Bet365
//
//  Created by luke on 17/6/24.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LukeTraceTurnItem : NSObject

@property (nonatomic,copy) NSString *traceOrderNo;

@property (nonatomic,assign) NSInteger status;

@property (nonatomic,assign) NSInteger multiple;

@property (nonatomic,copy) NSString *endTime;

@property (nonatomic,copy) NSString *turnNum;

@property (nonatomic,copy) NSString *startTime;

@end

@interface LukeTraceItem : NSObject

@property (nonatomic,copy) NSString *dlName;

@property (nonatomic,assign) NSInteger gameId;

@property (nonatomic,assign) CGFloat traceTotalMoney;

@property (nonatomic,copy) NSString *gdName;

@property (nonatomic,copy) NSString *oddsName;

@property (nonatomic,assign) NSInteger winTraceMoney;

@property (nonatomic,assign) NSInteger traceCount;

@property (nonatomic,copy) NSString *account;

@property (nonatomic,assign) NSInteger rebate;

@property (nonatomic,copy) NSString *addTime;

@property (nonatomic,assign) NSInteger betModel;

@property (nonatomic,copy) NSString *poschoose;

@property (nonatomic,assign) NSInteger dlId;

@property (nonatomic,assign) NSInteger finishCount;

@property (nonatomic,copy) NSString *cateName;

@property (nonatomic,copy) NSString *betInfo;

@property (nonatomic,assign) NSInteger tableIndex;

@property (nonatomic,assign) NSInteger cancelCount;

@property (nonatomic,assign) NSInteger model;

@property (nonatomic,copy) NSString *fullName;

@property (nonatomic,assign) NSInteger status;

@property (nonatomic,copy) NSString *poschooseName;

@property (nonatomic,assign) NSInteger winCount;

@property (nonatomic,assign) NSInteger betSrc;

@property (nonatomic,copy) NSString *userPaths;

@property (nonatomic,assign) NSInteger totalMoney;

@property (nonatomic,assign) NSInteger stopAfterWin;

@property (nonatomic,copy) NSString *orderNo;

@property (nonatomic,copy) NSString *turnNum;

@property (nonatomic,copy) NSString *lotteryFormula;

@property (nonatomic,assign) CGFloat finishMoney;

@property (nonatomic,copy) NSString *userType;

@property (nonatomic,assign) NSInteger totalNums;

@property (nonatomic,copy) NSString *catePaths;

@property (nonatomic,copy) NSString *odds;

@property (nonatomic,copy) NSString *dgdName;

@property (nonatomic,copy) NSString *cateCode;

@property (nonatomic,assign) NSInteger money;

@property (nonatomic,copy) NSString *zdlName;

@property (nonatomic,assign) CGFloat cancelMoney;

@property (nonatomic,assign) NSInteger userId;

@property (nonatomic,copy) NSString *formatAddTime;

@end
