//
//  LukeReportItem.h
//  Bet365
//
//  Created by luke on 17/6/24.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LukeGetReportItem : NSObject
@property (nonatomic,copy) NSString *dlName;

@property (nonatomic,assign) NSInteger gameId;

@property (nonatomic,assign) NSInteger rebateMoney;

@property (nonatomic,copy) NSString *gdName;

@property (nonatomic,assign) CGFloat reward;

@property (nonatomic,copy) NSString *trackOrderNo;

@property (nonatomic, assign) BOOL modified;

@property (nonatomic,copy) NSString *oddsName;

@property (nonatomic,copy) NSString *account;

@property (nonatomic,assign) NSInteger rebate;

@property (nonatomic,copy) NSString *addTime;

@property (nonatomic,copy) NSString *statTime;

@property (nonatomic,assign) NSInteger betModel;

@property (nonatomic,copy) NSString *poschoose;

@property (nonatomic,assign) NSInteger dlId;

@property (nonatomic,copy) NSString *cateName;

@property (nonatomic,assign) NSInteger drawMoney;

@property (nonatomic,copy) NSString *betInfo;

@property (nonatomic,assign) NSInteger multiple;

@property (nonatomic,copy) NSString *fullName;

@property (nonatomic,assign) NSInteger status;

@property (nonatomic,copy) NSString *betStartTime;

@property (nonatomic,assign) NSInteger winCount;

@property (nonatomic,assign) NSInteger result;

@property (nonatomic,assign) NSInteger model;

@property (nonatomic,copy) NSString *poschooseName;

@property (nonatomic,assign) CGFloat totalMoney;

@property (nonatomic,copy) NSString *orderNo;

@property (nonatomic,copy) NSString *turnNum;

@property (nonatomic,copy) NSString *userType;

@property (nonatomic,assign) NSInteger totalNums;

@property (nonatomic,copy) NSString *betEndTime;

@property (nonatomic,copy) NSString *odds;

@property (nonatomic,copy) NSString *userName;

@property (nonatomic,copy) NSString *dgdName;

@property (nonatomic,copy) NSString *cateCode;
@property (nonatomic,copy) NSString *openNum;
@property (nonatomic,assign) NSInteger money;

@property (nonatomic,copy) NSString *zdlName;

@property (nonatomic,copy) NSString *formatAddTime;

@property (nonatomic,assign) NSInteger userId;

@end

@interface LukeReportItem : NSObject

@property (nonatomic,assign) NSInteger userId;

@property (nonatomic,assign) CGFloat betMoney;

@property (nonatomic,assign) NSInteger gameId;
@property (nonatomic,assign) NSInteger betCount;

@property (nonatomic,copy) NSString *statTime;
@property (nonatomic,copy) NSString *gameName;

@property (nonatomic,assign) CGFloat winMoney;

@property (nonatomic,assign) CGFloat rebateMoney;

@property (nonatomic,assign) CGFloat effeMoney;

@end
