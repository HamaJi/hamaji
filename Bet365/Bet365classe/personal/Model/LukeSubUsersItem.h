//
//  LukeSubUsersItem.h
//  Bet365
//
//  Created by luke on 17/7/1.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LukeSubUsersItem : NSObject

@property (nonatomic,assign) NSInteger state;

@property (nonatomic,copy) NSString *loginTime;

@property (nonatomic,assign) NSInteger daysWithoutLogin;

@property (nonatomic,copy) NSString *regIp;

@property (nonatomic,copy) NSString *password;

@property (nonatomic,copy) NSString *type;

@property (nonatomic,strong) NSNumber *userId;

@property (nonatomic,assign) NSInteger superId;

@property (nonatomic,copy) NSString *operatingSystem;

@property (nonatomic,copy) NSString *qq;

@property (nonatomic,copy) NSString *hyLevel;

@property (nonatomic,copy) NSString *fullName;

@property (nonatomic,copy) NSString *intrCode;

@property (nonatomic, assign) BOOL isDl;

@property (nonatomic,copy) NSString *addTime;

@property (nonatomic,assign) CGFloat rebate;

@property (nonatomic,assign) CGFloat money;

@property (nonatomic,assign) NSInteger regWay;

@property (nonatomic,copy) NSString *updateTime;

@property (nonatomic,copy) NSString *browser;

@property (nonatomic,assign) NSInteger dlLevel;

@property (nonatomic,copy) NSString *superName;

@property (nonatomic,assign) NSInteger limitBet;

@property (nonatomic,copy) NSString *userAgent;

@property (nonatomic,copy) NSString *superPath;

@property (nonatomic,copy) NSString *account;

/** 额外增加的属性判断是否展开 */
@property (nonatomic,assign) BOOL isOpen;

@property (nonatomic,assign) NSInteger growthLevel;

@end
