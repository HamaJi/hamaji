//
//  LukeGameItem.h
//  Bet365
//
//  Created by luke on 17/6/21.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LiveListConfigItem : NSObject

@property (nonatomic,copy) NSString *gameType;

@property (nonatomic,copy) NSString *gameName;
@property (nonatomic,copy) NSString *imageSrc;

@end

@interface LukeGameItem : NSObject
@property (nonatomic,assign) NSInteger isOffcial;

@property (nonatomic,assign) NSInteger open;

@property (nonatomic,assign) NSInteger amount;

@property (nonatomic,copy) NSString *restStartDate;

@property (nonatomic,copy) NSString *code;
@property (nonatomic,copy) NSString *type;
@property (nonatomic,assign) NSInteger sort;
@property (nonatomic,strong) NSNumber *jsType;

@property (nonatomic,copy) NSString *openNumFormat;

@property (nonatomic,copy) NSString *restEndDate;

@property (nonatomic,copy) NSString *rules;

@property (nonatomic,assign) NSInteger openNum;

@property (nonatomic,copy) NSString *name;

@property (nonatomic,assign) NSInteger openLength;

@property (nonatomic,assign) NSInteger GameID;

@property (nonatomic,assign) NSInteger isCredit;

@property (nonatomic,assign) NSInteger cate;

@property (nonatomic,assign) NSInteger maxReward;

@property (nonatomic,copy) NSString *openFrequency;

@property (nonatomic,copy) NSString *turnFormat;

@property (nonatomic,assign) NSInteger turnLength;

@property (nonatomic,assign) NSInteger isBan;
@end

