//
//  LukeUserAdapter.h
//  Bet365
//
//  Created by luke on 17/6/12.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LukeUserAdapter : NSObject

+ (instancetype)sharedObject;

//邮箱
+ (BOOL) validateEmail:(NSString *)email;
//手机号码验证
+ (BOOL) validateMobile:(NSString *)mobile;
//微信号
+ (BOOL)validateWechat:(NSString *)wechat;
//登录用户名
+ (BOOL)loginUserName:(NSString *)name;
//用户名
+ (BOOL) validateUserName:(NSString *)name;
//密码
+ (BOOL) validatePassword:(NSString *)passWord;
//验证QQ号
+(BOOL)validateIsValidQQ:(NSString *)targetString;
//验证只能输入n位的数字
+(BOOL)validateIsOnlyManyNumber:(NSString *)targetString withFigure:(NSInteger)figure;
//昵称
+ (BOOL) validateNickname:(NSString *)nickname;
//银行卡号
+(BOOL)isBankCard:(NSString *)cardNumber;

//正整数
+ (BOOL)validateNumber:(NSString *)number;
//匹配正浮点数
+ (BOOL)validateZhengNumber:(NSString *)number;

//颜色转化
+ (UIColor *)colorWithHexString:(NSString *)color;

+ (NSString *)formatFloat:(NSString *)f;
+ (NSString *)decimalNumberWithId:(CGFloat)object;
//时间的比较
+ (NSComparisonResult)timeCompareWithOtherTime:(NSString *)otherTime;
/**
 * @brief  检查obj上次触发事件和当前时间的间隔是否大于最小间隔interval
 * @param obj
 * @param interval 最小间隔
 * @return 如果间隔大于interval，则返回YES，并且把事件触发时间更新为当前时间；否则返回NO
 * @note 如果obj为nil，返回YES，不做间隔检测
 */
+ (BOOL)lastTriggerDateOfObject:(id)obj greatThanInterval:(NSTimeInterval)interval;
+(NSString *)getNowTimeTimestamp;
@end
