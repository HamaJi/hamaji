//
//  LukeLiveRecordItem.h
//  Bet365
//
//  Created by luke on 2017/11/20.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, TransferStatus) {
    /** 手动转换模式*/
    TransferStatus_Manual = 0,
    /** 自动转换模式*/
    TransferStatus_auto
};

typedef NS_ENUM(NSInteger, TranferUserType) {
    /** 额度转换类型*/
    TranferUserType_Type = 0,
    /** 转换金额*/
    TranferUserType_Money,
    /** 账户信息*/
    TranferUserType_Account,
    /** 平台账户余额查询*/
    TranferUserType_Inquire
};

@interface LukeLiveGamesItem : NSObject
@property (nonatomic,copy) NSString *code;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *betTimeDesc;
@property (nonatomic,copy) NSString *type;
/** 余额 */
@property (nonatomic,strong) NSNumber *balance;
@property (nonatomic,strong) NSNumber *isFix;
@end

@interface LukeLiveRecordItem : NSObject
@property (nonatomic,copy) NSString *updateTime;

@property (nonatomic,copy) NSString *gmtBetTime;

@property (nonatomic,copy) NSString *validAmount;

@property (nonatomic,copy) NSString *betTime;
@property (nonatomic,copy) NSString *betContent;
@property (nonatomic,copy) NSString *gameKind;

@property (nonatomic,copy) NSString *betAmount;

@property (nonatomic,copy) NSString *winAmount;

@property (nonatomic,copy) NSString *gameType;

@property (nonatomic,copy) NSString *addTime;

@property (nonatomic,copy) NSString *billNo;

@property (nonatomic,copy) NSString *account;

@property (nonatomic,copy) NSString *statTime;

@property (nonatomic,assign) NSInteger settle;

@end
