//
//  LukeSpreadInfoItem.h
//  Bet365
//
//  Created by luke on 17/6/29.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LukeSpreadInfoItem : NSObject
/**  */
@property (nonatomic,assign) NSInteger SpreadId;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *webPath;
@property (nonatomic,copy) NSString *wapPath;

@end
