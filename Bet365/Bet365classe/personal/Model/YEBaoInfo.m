//
//  YEBaoInfo.m
//  Bet365
//
//  Created by super_小鸡 on 2019/6/9.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "YEBaoInfo.h"

@implementation YEBaoInerestInfo

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"date": @"date",
             @"settleTime": @"settleTime",
             @"userAccount": @"userAccount",
             @"identity": @"id",
             
             @"interestMoney": @"interestMoney",
             @"interestRate": @"interestRate",
             @"userId":@"userId",
             @"yubaoMoney": @"yubaoMoney"
             };
}
+ (NSValueTransformer *)dateJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSString *timeStr){
                NSDate *date = [NSDate dateWithString:timeStr Format:@"yyyy-MM-dd"];
                return date;
            } reverseBlock:^id(NSDate *date) {
                NSString *time = [date getTimeFormatStringWithFormat:@"yyyy-MM-dd"];
                return time;
            }];
}

@end

@implementation YEBaoInfo

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    return @{@"interest": @"interest",
             @"interestActiveRate": @"interestActiveRate",
             @"interestBaseRate": @"interestBaseRate",
             @"interestBaseRate": @"interestBaseRate",
             @"isOpen": @"isOpen",
             @"money": @"money",
             @"maxMoney" : @"maxMoney",
             @"totalInterest":@"totalInterest",
             @"activeDml":@"activeDml"
             };
}

+ (NSValueTransformer *)interestJSONTransformer{
    
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:
            ^id(NSArray <NSDictionary *>*dataList){
                NSError *error;
                NSMutableArray <YEBaoInerestInfo *>*modelList = [MTLJSONAdapter modelsOfClass:YEBaoInerestInfo.class fromJSONArray:dataList error:&error].mutableCopy;
//                [modelList sortUsingComparator:^NSComparisonResult(YEBaoInerestInfo *  _Nonnull obj1, YEBaoInerestInfo *  _Nonnull obj2) {
//                    if ([obj1.date timeIntervalSince1970] > [obj2.date timeIntervalSince1970]) {
//                        return NSOrderedDescending;
//                    }
//                    return NSOrderedAscending;
//                }];
//                if (modelList.count > 7) {
//                    [modelList removeObjectsInRange:NSMakeRange(0, modelList.count - 7)];
//                }
                return modelList;
            } reverseBlock:^id(NSArray <YEBaoInerestInfo *>*modelList) {
                NSError *error;
                return [MTLJSONAdapter JSONArrayFromModels:modelList error:&error];
            }];
}

@end
