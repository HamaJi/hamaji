//
//  LukeUserAdapter.m
//  Bet365
//
//  Created by luke on 17/6/12.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeUserAdapter.h"

@implementation LukeUserAdapter

+ (instancetype)sharedObject
{
    static LukeUserAdapter *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[LukeUserAdapter alloc] init];
    });
    return shared;
}

//邮箱
+ (BOOL) validateEmail:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

//手机号码验证
+ (BOOL) validateMobile:(NSString *)mobile
{
    //手机号以13， 15，18开头，八个 \d 数字字符
    //NSString *phoneRegex = @"^((13[0-9])|(15[^4,\\D])|(18[0,0-9]))\\d{8}$";
    NSString *phoneRegex = @"^[0-9]{11}";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:mobile];
}

//微信号
+ (BOOL)validateWechat:(NSString *)wechat
{
    NSString *userNameRegex = @"^[^\u4e00-\u9fa5]{0,}$";
    NSPredicate *userNamePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",userNameRegex];
    BOOL B = [userNamePredicate evaluateWithObject:wechat];
    return B;
}

//登录用户名
+ (BOOL)loginUserName:(NSString *)name
{
    NSString *userNameRegex = @"[0-9A-Za-z]{0,}$";
    NSPredicate *userNamePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",userNameRegex];
    BOOL B = [userNamePredicate evaluateWithObject:name];
    return B;
}

//用户名
+ (BOOL)validateUserName:(NSString *)name
{
    NSString *userNameRegex = @"[a-zA-Z0-9]{4,13}$";
    NSPredicate *userNamePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",userNameRegex];
    BOOL B = [userNamePredicate evaluateWithObject:name];
    return B;
}


//密码
+ (BOOL) validatePassword:(NSString *)passWord
{
    NSString *passWordRegex = @"[0-9A-Za-z]{6,12}$";
    NSPredicate *passWordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",passWordRegex];
    return [passWordPredicate evaluateWithObject:passWord];
}


//验证QQ号
+(BOOL)validateIsValidQQ:(NSString *)targetString
{
    
    NSString *regex = @"^[1-9]*[1-9][0-9]*$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [predicate evaluateWithObject:targetString];
}

//验证只能输入n位的数字
+(BOOL)validateIsOnlyManyNumber:(NSString *)targetString withFigure:(NSInteger)figure
{
    
    NSString *regex = [NSString stringWithFormat:@"^\\d{%ld}$",figure];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [predicate evaluateWithObject:targetString];
}
//昵称
+ (BOOL) validateNickname:(NSString *)nickname
{
    NSString *nicknameRegex = @"^[\u4e00-\u9fa5_0-9]{2,10}$";
    NSPredicate *passWordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",nicknameRegex];
    return [passWordPredicate evaluateWithObject:nickname];
}

//正整数
+ (BOOL)validateNumber:(NSString *)number
{
    NSString *phoneRegex = @"^[0-9]*$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:number];
}

+ (BOOL)validateZhengNumber:(NSString *)number
{
    NSString *phoneRegex = @"^[1-9]d*.d*|0.d*[1-9]d*$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:number];
}

//颜色转化
+ (UIColor *)colorWithHexString:(NSString *)color
{
    NSString *cString = [[color stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) {
        return [UIColor clearColor];
    }
    // 判断前缀
    if ([cString hasPrefix:@"0X"])
        cString = [cString substringFromIndex:2];
    if ([cString hasPrefix:@"#"])
        cString = [cString substringFromIndex:1];
    if ([cString length] != 6)
        return [UIColor clearColor];
    // 从六位数值中找到RGB对应的位数并转换
    NSRange range;
    range.location = 0;
    range.length = 2;
    //R、G、B
    NSString *rString = [cString substringWithRange:range];
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:1.0f];
}

+ (NSString *)decimalNumberWithId:(CGFloat)object
{
    NSString *dStr      = [NSString stringWithFormat:@"%lf", object];
    NSDecimalNumber *dn = [NSDecimalNumber decimalNumberWithString:dStr];
    return [NSString stringWithFormat:@"%@",[dn stringValue]];
}

#pragma mark 判断银行卡号是否合法
+(BOOL)isBankCard:(NSString *)cardNumber{
    NSString *nicknameRegex = @"\\d{10,}";
    NSPredicate *passWordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",nicknameRegex];
    return [passWordPredicate evaluateWithObject:cardNumber];
}
+ (NSString *)formatFloat:(NSString *)f
{
    NSDecimalNumberHandler *handler = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundDown scale:3 raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:YES];
    NSDecimalNumber *number = [NSDecimalNumber decimalNumberWithString:f];
    return [NSString stringWithFormat:@"%@",[number decimalNumberByRoundingAccordingToBehavior:handler]];
}
+ (NSComparisonResult)timeCompareWithOtherTime:(NSString *)otherTime
{
    NSDateFormatter *dateF = [[NSDateFormatter alloc] init];
    [dateF setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateF setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:8]];
    NSDate *date = [NSDate dateUTC];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate:date];
    NSDate *localDate = [date dateByAddingTimeInterval:interval];
    NSString *str = [dateF stringFromDate:localDate];
    NSDate *nowDate = [dateF dateFromString:str];
    
    NSDate *needDate = [dateF dateFromString:otherTime];
    NSComparisonResult result = [nowDate compare:needDate];
    return result;
}

+(NSString *)getNowTimeTimestamp{
    
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    
    NSTimeInterval a=[dat timeIntervalSince1970];
    
    NSString*timeString = [NSString stringWithFormat:@"%0.f", a];//转为字符型
    
    return timeString;
}

+ (BOOL)lastTriggerDateOfObject:(id)obj greatThanInterval:(NSTimeInterval)interval
{
    if (!obj) {
        return YES;
    }
    
    static NSMapTable *mapTable = nil;
    
    if (!mapTable) {
        mapTable = [NSMapTable weakToStrongObjectsMapTable];
    }
    
    NSDate *date = [mapTable objectForKey:obj];
    
    if (!date || fabs([date timeIntervalSinceNow]) > interval) {
        //之前没记录，或者时间间隔已大于interval，更新为当前时间
        [mapTable setObject:[NSDate dateUTC] forKey:obj];
        return YES;
    } else {
        return NO;
    }
}

@end
