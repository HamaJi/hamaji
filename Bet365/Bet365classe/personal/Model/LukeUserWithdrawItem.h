//
//  LukeUserWithdrawItem.h
//  Bet365
//
//  Created by luke on 17/7/11.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LukeUserWithdrawItem : NSObject

@property (nonatomic,assign) NSInteger cashMode;

@property (nonatomic,assign) NSInteger cashStatus;

@property (nonatomic,copy) NSString *cashOrderNo;

@property (nonatomic,copy) NSString *userNickName;

@property (nonatomic,copy) NSString *bankAddress;

@property (nonatomic,copy) NSString *superPath;

@property (nonatomic,copy) NSString *bankCard;

@property (nonatomic,copy) NSString *operatorTime;

@property (nonatomic,copy) NSString *macOs;

@property (nonatomic,copy) NSString *ipAddress;

@property (nonatomic,assign) NSInteger accountMoney;

@property (nonatomic,copy) NSString *fk;

@property (nonatomic,copy) NSString *userLevel;

@property (nonatomic,copy) NSString *userAccount;

@property (nonatomic,copy) NSString *browser;

@property (nonatomic,assign) NSInteger UserWithdrawID;

@property (nonatomic,assign) NSInteger cashMoney;

@property (nonatomic,assign) NSInteger counterFee;

@property (nonatomic,assign) NSInteger superId;

@property (nonatomic,copy) NSString *addTime;

@property (nonatomic,copy) NSString *superName;

@property (nonatomic,copy) NSString *operatorAccount;

@property (nonatomic,assign) NSInteger policeFlag;

@property (nonatomic,copy) NSString *bankName;

@property (nonatomic,assign) NSInteger approveMoney;

@property (nonatomic,copy) NSString *approveReason;

@property (nonatomic,copy) NSString *userAgent;

@property (nonatomic,copy) NSString *cashReason;

@property (nonatomic,assign) NSInteger userId;

@end
