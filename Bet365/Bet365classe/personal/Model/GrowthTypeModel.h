//
//  GrowthTypeModel.h
//  Bet365
//
//  Created by luke on 2019/11/4.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GrowthTypeListModel : NSObject

@property (nonatomic,assign) NSInteger userId;
@property (nonatomic,copy) NSString *userAccount;
@property (nonatomic,assign) NSInteger growthLevel;
@property (nonatomic,copy) NSString *addDesc;
@property (nonatomic,assign) NSInteger growthValue;
@property (nonatomic,copy) NSString *addTime;
@property (nonatomic,copy) NSString *addType;
@property (nonatomic,assign) NSInteger addGrowth;
@property (nonatomic,assign) NSInteger currentLevel;
@property (nonatomic,assign) NSInteger currentGrowthValue;

@end

@interface GrowthTypeModel : NSObject

@property (nonatomic,strong) NSArray <GrowthTypeListModel *> *data;
@property (nonatomic,assign) NSInteger totalCount;

@end

NS_ASSUME_NONNULL_END
