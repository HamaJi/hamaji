//
//  QotuoModel.h
//  Bet365
//
//  Created by jesse on 2017/12/3.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QotuoModel : NSObject
@property(copy,nonatomic)NSString *code;
@property(copy,nonatomic)NSString *name;
@property(copy,nonatomic)NSString *balance;
@end
