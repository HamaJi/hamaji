//
//  LukeReportMagItem.m
//  Bet365
//
//  Created by luke on 17/6/26.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeReportMagItem.h"

@implementation Bet365ReportItem

+ (NSDictionary *)mj_objectClassInArray
{
    return @{@"data" : [LukeReportMagItem class]};
}

@end

@implementation Bet365OtherDataReportItem

@end

@implementation LukeReportMagItem

+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"ReportMagID" : @"id"};
}

@end
