//
//  LukeReportMagItem.h
//  Bet365
//
//  Created by luke on 17/6/26.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Bet365OtherDataReportItem;

@interface Bet365ReportItem : NSObject
/**  */
@property (nonatomic,strong) NSArray *data;
/**  */
@property (nonatomic,strong) NSNumber *totalCount;
/**  */
@property (nonatomic,strong) Bet365OtherDataReportItem *otherData;

@end

@interface Bet365OtherDataReportItem : NSObject
/**  */
@property (nonatomic,strong) NSNumber *incomeSum;
/**  */
@property (nonatomic,strong) NSNumber *paySum;

@end

@interface LukeReportMagItem : NSObject

@property (nonatomic,assign) NSInteger ReportMagID;

@property (nonatomic,strong) NSNumber *money;

@property (nonatomic,copy) NSString *cateCode;

@property (nonatomic,assign) NSInteger gameId;

@property (nonatomic,copy) NSString *gameCode;

@property (nonatomic,copy) NSString *userPaths;

@property (nonatomic,strong) NSNumber *balance;

@property (nonatomic,copy) NSString *operateName;

@property (nonatomic,assign) NSInteger userId;

@property (nonatomic,copy) NSString *orderNo;

@property (nonatomic,copy) NSString *remark;

@property (nonatomic,assign) NSInteger tableIndex;

@property (nonatomic,assign) NSInteger tranType;

@property (nonatomic,copy) NSString *addTime;

@property (nonatomic,copy) NSString *account;

@property (nonatomic,copy) NSString *turnNum;

@property (nonatomic,copy) NSString *content;
@end
