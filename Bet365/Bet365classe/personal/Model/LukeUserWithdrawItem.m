//
//  LukeUserWithdrawItem.m
//  Bet365
//
//  Created by luke on 17/7/11.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeUserWithdrawItem.h"

@implementation LukeUserWithdrawItem

+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"id" : @"UserWithdrawID"};
}

@end
