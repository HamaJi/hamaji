//
//  LukeRechargeOrderItem.m
//  Bet365
//
//  Created by luke on 2017/11/20.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeRechargeOrderItem.h"

@implementation LukeRechargeOrderItem

+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"RechargeId" : @"id"};
}

@end
