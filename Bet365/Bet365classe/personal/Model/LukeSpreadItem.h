//
//  LukeSpreadItem.h
//  Bet365
//
//  Created by luke on 17/6/30.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LukeSpreadItem : NSObject
/**  */
@property (nonatomic,assign) NSInteger SpreaID;
/**  */
@property (nonatomic,copy) NSString *addTime;

@property (nonatomic,assign) NSInteger agentId;
@property (nonatomic,copy) NSString *agentAccount;
@property (nonatomic,copy) NSString *externalUrl;
@property (nonatomic,copy) NSString *code;
@property (nonatomic,assign) NSInteger userType;
@property (nonatomic,assign) NSInteger spreadType;
@property (nonatomic,copy) NSString *spreadTypeName;
@property (nonatomic,assign) NSInteger effectiveDays;
@property (nonatomic,strong) NSNumber *rebate;
@property (nonatomic,assign) NSInteger visitCount;
@property (nonatomic,assign) NSInteger registCount;
/**  */
@property (nonatomic,assign) BOOL effective;

@end
