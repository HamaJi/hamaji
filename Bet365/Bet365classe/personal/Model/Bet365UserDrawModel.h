//
//  Bet365UserDrawModel.h
//  Bet365
//
//  Created by luke on 2018/8/24.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UserBankData;

@interface Bet365UserDrawLimitModel : NSObject

/**  */
@property (nonatomic,strong) NSNumber *singleMaxLimit;
@property (nonatomic,strong) NSNumber *bankAddressLimit;
@property (nonatomic,strong) NSNumber *dayMaxCountLimit;//超过提现次数
@property (nonatomic,strong) NSNumber *allowDml;//打码量不足是否允许提现
@property (nonatomic,strong) NSNumber *singleMinLimit;
@property (nonatomic,strong) NSNumber *dayMaxLimit;
@property (nonatomic,strong) NSNumber *dayMaxCount;
@property (nonatomic,strong) NSNumber *counterFee;
@property (nonatomic,strong) NSNumber *counterFeeMode;
@property (nonatomic,strong) NSNumber *relaxQuota;
@property (nonatomic,strong) NSNumber *civilMoneyFlag;
@property (nonatomic,strong) NSNumber *civilMoneyRadio;
@property (nonatomic,strong) NSNumber *timeInterval;
@property (nonatomic,strong) NSNumber *vCode;
@property (nonatomic,strong) NSNumber *upUntreated;
/**  */
@property (nonatomic,copy) NSString *withDrawNotice;

@property (nonatomic,strong) NSNumber *userMemo;

@end

@interface Bet365UserDrawModel : NSObject
@property (nonatomic,strong) NSNumber *sumDeductMoney;
@property (nonatomic,strong) NSNumber *sumMoney;
@property (nonatomic,strong) NSNumber *count;
@property (nonatomic,strong) NSNumber *allDmlPass;
/**  */
@property (nonatomic,strong) Bet365UserDrawLimitModel *limit;
@property (nonatomic,strong) NSNumber *remainRequiredDml;
@end
