//
//  LukePushMessageItem.h
//  Bet365
//
//  Created by luke on 17/6/15.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LukePushMessageItem : NSObject

@property (nonatomic,assign) NSInteger userId;

@property (nonatomic,assign) NSInteger readStatus;

@property (nonatomic,copy) NSString *messageTitle;

@property (nonatomic,assign) NSInteger PushID;

@property (nonatomic,copy) NSString *operatorAccount;

@property (nonatomic,assign) NSInteger acceptRemoveFlag;

@property (nonatomic,assign) NSInteger sendRemoveFlag;

@property (nonatomic,copy) NSString *addTime;

@property (nonatomic,copy) NSString *messageContent;

@property (nonatomic,copy) NSString *userAccount;

@property (nonatomic,copy) NSString *readTime;

@end
