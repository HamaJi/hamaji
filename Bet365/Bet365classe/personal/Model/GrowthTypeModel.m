//
//  GrowthTypeModel.m
//  Bet365
//
//  Created by luke on 2019/11/4.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "GrowthTypeModel.h"

@implementation GrowthTypeListModel

- (id)mj_newValueFromOldValue:(id)oldValue property:(MJProperty *)property
{
    if ([property.name isEqualToString:@"addType"]) {
        switch ([oldValue integerValue]) {
            case 1:
                return @"充值";
                break;
            case 2:
                return @"签到";
                break;
            case 3:
                return @"完善资料";
                break;
            case 4:
                return @"绑定银行卡";
            case 5:
                return @"其他";
                break;
                break;
            default:
                break;
        }
    }
    return oldValue;
}

@end

@implementation GrowthTypeModel

+ (NSDictionary *)mj_objectClassInArray
{
    return @{@"data" : [GrowthTypeListModel class]};
}

@end
