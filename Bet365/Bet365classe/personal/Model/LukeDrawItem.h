//
//  LukeDrawItem.h
//  Bet365
//
//  Created by luke on 17/6/20.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Bet365UserDrawAllModel : NSObject
/**  */
@property (nonatomic,strong) NSArray *rows;
/**  */
@property (nonatomic,strong) NSNumber *sumAllDml;
/**  */
@property (nonatomic,strong) NSNumber *yetWithdraw;
/**  */
@property (nonatomic,strong) NSNumber *sumAllDeduct;
/**  */
@property (nonatomic,strong) NSNumber *relaxQuota;
/**  */
@property (nonatomic,strong) NSNumber *requireDML;

@property (nonatomic,strong) NSNumber *remainRequiredDml;

@end

@interface LukeDrawItem : NSObject
@property (nonatomic,assign) NSInteger DrawID;

@property (nonatomic,assign) NSInteger mormDml;

@property (nonatomic,assign) NSInteger discountDeduct;

@property (nonatomic,assign) NSInteger relaxQuota;

@property (nonatomic,assign) NSInteger videoDml;

@property (nonatomic,assign) NSInteger discountMoney;

@property (nonatomic,assign) NSInteger sportsDml;

@property (nonatomic,assign) NSInteger discountAdopt;

@property (nonatomic,assign) NSInteger userId;

@property (nonatomic,assign) NSInteger allDeduct;

@property (nonatomic,copy) NSString *endTime;

@property (nonatomic,assign) NSInteger mormDeduct;

@property (nonatomic,copy) NSString *civilMoneyFlag;

@property (nonatomic,assign) NSInteger mormAdopt;

@property (nonatomic,copy) NSString *userAccount;

@property (nonatomic,copy) NSString *beginTime;

@property (nonatomic,assign) NSInteger discountDml;

@property (nonatomic,assign) NSInteger rechMoney;

@property (nonatomic,assign) NSInteger cpDml;
@end
