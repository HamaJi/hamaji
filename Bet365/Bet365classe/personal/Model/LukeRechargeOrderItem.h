//
//  LukeRechargeOrderItem.h
//  Bet365
//
//  Created by luke on 2017/11/20.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LukeRechargeOrderItem : NSObject

@property (nonatomic,assign) NSInteger RechargeId;

@property (nonatomic,copy) NSString *userLevel;

@property (nonatomic,assign) NSInteger payAccountId;

@property (nonatomic,copy) NSString *ipAddress;

@property (nonatomic,copy) NSString *payAccountOwner;

@property (nonatomic,assign) NSInteger mode;

@property (nonatomic,copy) NSString *payAccountAccount;

@property (nonatomic,copy) NSString *addTime;
@property (nonatomic,copy) NSString *auditTime;

@property (nonatomic,copy) NSString *superAccount;

@property (nonatomic,copy) NSString *payType;

@property (nonatomic,copy) NSString *payTypeName;

@property (nonatomic,assign) NSInteger superId;

@property (nonatomic,assign) NSInteger normalDml;

@property (nonatomic,copy) NSString *browser;

@property (nonatomic,copy) NSString *userAgent;

@property (nonatomic,copy) NSString *superPath;

@property (nonatomic,assign) NSInteger status;

@property (nonatomic,copy) NSString *userAccount;

@property (nonatomic,assign) CGFloat totalAmount;

@property (nonatomic,assign) NSInteger dmlFlag;

@property (nonatomic,assign) NSInteger pointFlag;

@property (nonatomic,copy) NSString *orderNo;

@property (nonatomic,assign) CGFloat discountAmount;

@property (nonatomic,copy) NSString *os;

@property (nonatomic,assign) NSInteger userBalance;

@property (nonatomic,assign) NSInteger discountDml;
@property (nonatomic,strong) NSNumber *discountType;

@property (nonatomic,assign) NSInteger userId;

@property (nonatomic,assign) CGFloat amount;

@property (nonatomic,strong) NSString *remarks;
@end
