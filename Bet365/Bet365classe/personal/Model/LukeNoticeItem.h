//
//  LukeNoticeItem.h
//  Bet365
//
//  Created by luke on 17/6/19.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LukeNoticeItem : NSObject
@property (nonatomic,copy) NSString *endDate;

@property (nonatomic,assign) NSInteger NoticeID;
@property (nonatomic,assign) NSInteger status;
@property (nonatomic,copy) NSString *operatorAccount;

@property (nonatomic,copy) NSString *updateTime;

@property (nonatomic,copy) NSString *noticeTitle;

@property (nonatomic,copy) NSString *noticeContent;

@property (nonatomic,copy) NSString *noticeType;

@property (nonatomic,copy) NSString *beginDate;

@property (nonatomic,copy) NSString *addTime;
@property (nonatomic,copy) NSString *typeName;

@end
