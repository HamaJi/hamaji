//
//  Bet365PersonalRwsModel.h
//  Bet365
//
//  Created by luke on 2018/8/23.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Bet365PersonalRwBetModel : NSObject
/**  */
@property (nonatomic,strong) NSNumber *discountMoney;
/**  */
@property (nonatomic,strong) NSNumber *otherDiscount;
/**  */
@property (nonatomic,strong) NSNumber *rechDiscount;
/**  */
@property (nonatomic,strong) NSNumber *rechMoney;
/**  */
@property (nonatomic,strong) NSNumber *rechUserCount;
/**  */
@property (nonatomic,strong) NSNumber *withdrawMoney;
@end

@interface Bet365PersonalBetModel : NSObject
/**  */
@property (nonatomic,strong) NSNumber *bettingMoney;
/**  */
@property (nonatomic,strong) NSNumber *rebateMoney;
/**  */
@property (nonatomic,strong) NSNumber *winMoney;
/**  */
@property (nonatomic,strong) NSNumber *winOrcloseMoney;
/**  */
@property (nonatomic,strong) NSString *statTime;
@end

@interface Bet365PersonalRwsModel : NSObject
/**  */
@property (nonatomic,strong) Bet365PersonalBetModel *cpBetReport;
/**  */
@property (nonatomic,strong) NSNumber *dlRebate;
@property (nonatomic,strong) NSNumber *dlBonuus;
@property (nonatomic,strong) NSNumber *dlDayWage;
/**  */
@property (nonatomic,strong) Bet365PersonalBetModel *ftBetReport;
/**  */
@property (nonatomic,strong) Bet365PersonalBetModel *liveBetReport;
/**  */
@property (nonatomic,strong) NSNumber *money;
/**  */
@property (nonatomic,strong) NSNumber *registCount;
/**  */
@property (nonatomic,strong) Bet365PersonalRwBetModel *rwReport;
/**  */
@property (nonatomic,strong) NSNumber *teamCount;

/**  */
@property (nonatomic,strong) NSNumber *promotionBonus;

/**  */
@property (nonatomic,strong) NSNumber *promotionBonusUserCount;


@end
