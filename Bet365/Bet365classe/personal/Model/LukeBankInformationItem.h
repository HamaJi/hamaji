//
//  LukeBankInformationItem.h
//  Bet365
//
//  Created by luke on 17/6/20.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LukeBankInformationItem : NSObject

@property (nonatomic,assign) NSInteger ascriptionType;

@property (nonatomic,copy) NSString *configKey;

@property (nonatomic,assign) NSInteger enableStatus;

@property (nonatomic,assign) NSInteger configSort;

@property (nonatomic,assign) NSInteger configId;

@property (nonatomic,copy) NSString *configRemark;

@property (nonatomic,copy) NSString *configValue;

@property (nonatomic,copy) NSString *enlargeMemo;

@property (nonatomic,copy) NSString *configName;

@property (nonatomic,assign) NSInteger configType;

@end
