//
//  Bet365CpHistoryModel.h
//  Bet365
//
//  Created by luke on 2018/8/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Bet365TransListModel : NSObject
/**  */
@property (nonatomic,assign) NSInteger type;
/**  */
@property (nonatomic,copy) NSString *desc;
/**  */
@property (nonatomic,strong) NSNumber *reportType;

@end

@interface Bet365GameReportListModel : NSObject
/**  */
@property (nonatomic,copy) NSString *gameCode;
/**  */
@property (nonatomic,strong) NSNumber *betAmount;
/**  */
@property (nonatomic,strong) NSNumber *winAmount;
/**  */
@property (nonatomic,strong) NSNumber *validAmount;

@end

@interface Bet365CpHistoryModel : NSObject
/**  */
@property (nonatomic,copy) NSString *statTime;
/**  */
@property (nonatomic,strong) NSNumber *bettingMoney;
/**  */
@property (nonatomic,strong) NSNumber *winMoney;
/**  */
@property (nonatomic,strong) NSNumber *rebateMoney;
/**  */
@property (nonatomic,strong) NSNumber *winOrcloseMoney;
/**  */
@property (nonatomic,strong) NSNumber *validMoney;

@end
