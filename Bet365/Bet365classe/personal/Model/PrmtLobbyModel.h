//
//  PrmtApplicationModel.h
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/28.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "MTLModel+Define.h"


@interface PrmtYHApplication : MTLModel<MTLJSONSerializing>

@property (nonatomic,strong) NSString *activeId;
@property (nonatomic,strong) NSString *banner;
@property (nonatomic,strong) NSString *details;

@end

@interface PrmtLobbyModel : MTLModel<MTLJSONSerializing>

@property (nonatomic,strong) NSString *bottomTxt;
@property (nonatomic,strong) NSString *headCenterLogo;
@property (nonatomic,strong) NSString *headLeftLogo;
@property (nonatomic,strong) NSArray <PrmtYHApplication *>*yhApplication;

@end

