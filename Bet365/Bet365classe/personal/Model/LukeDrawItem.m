//
//  LukeDrawItem.m
//  Bet365
//
//  Created by luke on 17/6/20.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeDrawItem.h"

@implementation Bet365UserDrawAllModel

+ (NSDictionary *)mj_objectClassInArray
{
    return @{@"rows" : [LukeDrawItem class]};
}

@end

@implementation LukeDrawItem

+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"DrawID" : @"id"};
}

@end

