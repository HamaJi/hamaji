//
//  Bet365DrawModel.h
//  Bet365
//
//  Created by luke on 2018/8/9.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Bet365DrawModel : NSObject
@property (nonatomic,assign) NSInteger cashMode;

@property (nonatomic,copy) NSString *cashReason;
@property (nonatomic,copy) NSString *approveReason;
@property (nonatomic,copy) NSString *operatorTime;

@property (nonatomic,assign) NSInteger policeFlag;

@property (nonatomic,strong) NSNumber *approveMoney;
@property (nonatomic,strong) NSNumber *accountMoney;
@property (nonatomic,strong) NSNumber *cashMoney;

@property (nonatomic,copy) NSString *userLevel;

@property (nonatomic,copy) NSString *userMemo;

@property (nonatomic,copy) NSString *ipAddress;

@property (nonatomic,copy) NSString *macOs;

@property (nonatomic,copy) NSString *cashOrderNo;
@property (nonatomic,copy) NSString *bankName;
@property (nonatomic,copy) NSString *bankCard;
@property (nonatomic,copy) NSString *bankAddress;

@property (nonatomic,copy) NSString *addTime;

@property (nonatomic,assign) NSInteger cashStatus;

@property (nonatomic,strong) NSNumber *counterFee;

@property (nonatomic,assign) NSInteger superId;

@property (nonatomic,copy) NSString *browser;

@property (nonatomic,copy) NSString *superPath;

@property (nonatomic,copy) NSString *userNickName;

//@property (nonatomic,copy) NSString *userMemo;
@end
