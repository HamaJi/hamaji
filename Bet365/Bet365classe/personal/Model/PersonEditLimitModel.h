//
//  PersonEditLimitModel.h
//  Bet365
//
//  Created by luke on 2019/3/15.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersonEditLimitModel : NSObject

@property (nonatomic, copy) NSNumber * fullName;
@property (nonatomic, copy) NSNumber * email;
@property (nonatomic, copy) NSNumber * phone;
@property (nonatomic, copy) NSNumber * qq;
@property (nonatomic, copy) NSNumber * weixin;
@property (nonatomic, copy) NSNumber * birthday;
@property (nonatomic, assign) BOOL checkPhoneSMSCode;

@end
