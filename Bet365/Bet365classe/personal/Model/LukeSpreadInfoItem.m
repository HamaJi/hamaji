//
//  LukeSpreadInfoItem.m
//  Bet365
//
//  Created by luke on 17/6/29.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeSpreadInfoItem.h"

@implementation LukeSpreadInfoItem

+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"SpreadId" : @"id"};
}

@end
