//
//  NetWorkMannager+Prmt.h
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/28.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "NetWorkMannager.h"

NS_ASSUME_NONNULL_BEGIN

@interface NetWorkMannager (Prmt)

- (NSURLSessionTask *)requestPrmtLobbyCache:(HttpRequestCache)cache Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

- (NSURLSessionTask *)requestPrmtApply:(NSString *)activityId Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure;

@end

NS_ASSUME_NONNULL_END
