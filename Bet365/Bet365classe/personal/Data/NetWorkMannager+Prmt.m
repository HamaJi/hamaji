//
//  NetWorkMannager+Prmt.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/28.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "NetWorkMannager+Prmt.h"
#import "RequestUrlHeader.h"
@implementation NetWorkMannager (Prmt)
- (NSURLSessionTask *)requestPrmtLobbyCache:(HttpRequestCache)cache Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure{
    return [self GET:USER_PRMT_LOBY RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_JSON parameters:nil responseCache:cache success:success failure:failure];
}

- (NSURLSessionTask *)requestPrmtApply:(NSString *)activityId Success:(HttpRequestSuccess)success failure:(HttpRequestFailed)failure
{
    
    NSMutableDictionary *parameters =[NSMutableDictionary dictionary];
    [parameters safeSetObject:activityId forKey:@"activityId"];
    return [self POST:USER_PRMT_APPLY RequestType:PPSerializerType_HTTP ResponseType:PPSerializerType_HTTP parameters:parameters responseCache:nil success:success failure:failure];
}
@end


