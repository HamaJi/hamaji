//
//  jesseNewBankViewController.h
//  Bet365
//
//  Created by jesse on 2017/11/18.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
/**MODEL**/
/**获取所有支付Model**/
@interface payModel:JSONModel
@property(copy,nonatomic)NSString *addTime;/**Property:添加时间**/
@property(strong,nonatomic)NSNumber *bankFlag;/**Property:bankFlag**/
@property(copy,nonatomic)NSString *code;/**Property:code**/
@property(strong,nonatomic)NSNumber *bankId;/**Property:支付ID**/
@property(copy,nonatomic)NSString *name;/**Property:支付类型**/
@property(strong,nonatomic)NSNumber *onlinePayEnabled;/**网银支付是否开启**/
@property(copy,nonatomic)NSString *rechargeDesc;/**Property:充值描述**/
@property(strong,nonatomic)NSNumber *sort;/****/
@property(strong,nonatomic)NSNumber *status;/**Property:开关**/
@property(strong,nonatomic)NSNumber *transferEnabled;/**Property:转移标示**/
@property(copy,nonatomic)NSString *promptS;/**提示语**/
@end

/**转账ModelEnd**/
/**线上支付MODEL**/
@interface onlineModel:JSONModel
@property(copy,nonatomic)NSString *addTime;
@property(strong,nonatomic)NSString *banks;
@property(copy,nonatomic)NSString *callbackDomain;
@property(strong,nonatomic)NSNumber *bankId;
@property(strong,nonatomic)NSNumber *merchantId;
@property(copy,nonatomic)NSString *name;
@property(copy,nonatomic)NSString *payType;
@property(copy,nonatomic)NSString *rechargeAmount;
@property(strong,nonatomic)NSNumber *rechargeTimes;
@property(copy,nonatomic)NSString *remarks;
@property(strong,nonatomic)NSNumber *sort;
@property(strong,nonatomic)NSNumber *status;
@property(copy,nonatomic)NSString *tpInterfaceId;
@property(copy,nonatomic)NSString *tpMerchantName;
@property(copy,nonatomic)NSString *tpPayType;
@property(copy,nonatomic)NSString *transportUrl;
@property(copy,nonatomic)NSString *updateTime;
@property(copy,nonatomic)NSString *userLevels;
@property(strong,nonatomic)NSNumber *viewType;
@end

/**支付类型<在线-转账集合Model>**/
/**转账Model**/
@interface TransferMoneyModel:JSONModel
@property(copy,nonatomic)NSString *account;
@property(copy,nonatomic)NSString *addTime;
@property(copy,nonatomic)NSString *bankAddress;
@property(strong,nonatomic)NSNumber *bankFlag;
@property(copy,nonatomic)NSString *bankName;
@property(strong,nonatomic)NSNumber *bankId;
@property(copy,nonatomic)NSString *name;
@property(copy,nonatomic)NSString *owner;
@property(copy,nonatomic)NSString *payType;
@property(copy,nonatomic)NSString *qrCode;
@property(strong,nonatomic,readwrite)NSString *friendPayAccounts;
@property(strong,nonatomic)NSNumber *rechargeAmount;
@property(strong,nonatomic)NSNumber *rechargeTimes;
@property(copy,nonatomic)NSString *remarks;
@property(strong,nonatomic)NSNumber *sort;
@property(strong,nonatomic)NSNumber *status;
@property(copy,nonatomic)NSString *updateTime;
@property(copy,nonatomic)NSString *userLevels;
@property(copy,nonatomic)NSString *orderRemark;
@property(copy,nonatomic)NSString *orderRemarkStatus;
@property(strong,nonatomic)NSNumber *subPayType;


@end

/**线上支付MODELEND**/
@interface TransferOnlieModel:NSObject
@property(nonatomic,assign)NSInteger markWay;  /**线上转账汇款标识**/
@property(nonatomic,assign)NSInteger sortState;//排序
@property(nonatomic,strong)TransferMoneyModel *transModel;/**转账Model**/
@property(nonatomic,strong)onlineModel *onlineModel;/**线上Model**/

@property(strong,nonatomic)NSString *rechangeImage;
@property(strong,nonatomic)NSString *rechangeLink;
@property(assign,nonatomic)CGFloat height;
@property(assign,nonatomic)CGFloat width;

@end

/**支付类型<在线-转账集合Model>END**/
/**MODELEND**/

/**cellKind**/
/*****************/
/**BanTransFer**/
/**BanktransferMainView**/

static void *RemarkPlaceHolderKey = &RemarkPlaceHolderKey;



@interface jesseNewBankViewController : UIViewController
@end
