//
//  SecRechargeTableHeaderView.m
//  Bet365
//
//  Created by adnin on 2019/5/5.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "SecRechargeTableHeaderView.h"
#import "aji_horCircleView.h"
#import <pop/POP.h>

#define kCollectionCellHorizontalMaxCount   4
#define kCollectionCellSpacing              10.0
#define kCollectionCellWidth                ((WIDTH - (kCollectionCellHorizontalMaxCount + 1) * kCollectionCellSpacing) / (kCollectionCellHorizontalMaxCount * 1.0))
#define kCollectionCellHeight               40.0

#pragma mark ================================= AmoundCell =================================

@interface AmoundCell : UICollectionViewCell

@property (nonatomic)NSString *titileStr;

@end

@interface AmoundCell()

@property (nonatomic,strong) UILabel *moneyLabel;

@property (nonatomic,assign) BOOL isSelect;

@end

@implementation AmoundCell



-(void)setTitileStr:(NSString *)titileStr{
    self.moneyLabel.text = titileStr.length ? [titileStr stringByAppendingString:@"元"] : @"--";
}

-(UILabel *)moneyLabel{
    if (!_moneyLabel) {
        _moneyLabel = [UILabel setAllocLabelWithText:@"--" FontOfSize:18 rgbColor:0xffffff];
        [self addSubview:_moneyLabel];
        [_moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self);
        }];
        self.clipsToBounds = YES;
        self.layer.cornerRadius = 6;
        self.layer.borderWidth = 1;
    }
    return _moneyLabel;
}

-(void)setIsSelect:(BOOL)isSelect{
    _isSelect = isSelect;
    if (_isSelect) {
        self.moneyLabel.textColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor redColor];
        self.layer.borderColor = [UIColor redColor].CGColor;
    }else{
        self.moneyLabel.textColor = [UIColor redColor];
        self.backgroundColor = [UIColor whiteColor];
        self.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
    }
}
@end

#pragma mark ================================= SecRechargeTableHeaderView =================================

@interface SecRechargeTableHeaderView()
<aji_horCircleViewDelegate,
UICollectionViewDelegate,
UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout,
UITextFieldDelegate>

@property (nonatomic,strong)UIView *topContainView;

@property (nonatomic,strong)aji_horCircleView *circleView;

@property (nonatomic,strong)UILabel *nameLabel;

@property (nonatomic,strong)UILabel *balanceLabel;

@property (nonatomic,strong)UITextField *textField;

@property (nonatomic,strong)UICollectionView *collectionView;

@property (nonatomic,strong)NSMutableArray <NSString *>*amoundDataList;

@property (nonatomic,strong)NSNumber *amoundSelectIdx;


@end

@implementation SecRechargeTableHeaderView

static NSString * const staticAmoundList[8] = {
    @"50",
    @"100",
    @"300",
    @"500",
    @"1000",
    @"2000",
    @"3000",
    @"5000"
};

-(instancetype)init{
    if (self = [super init]) {
        [self setUI];
        [self addObser];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setUI];
        [self addObser];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self setUI];
        [self addObser];
    }
    return self;
}

-(void)showOpacityAnimation{
    [self closeOpacityAnimation];
    
    POPBasicAnimation *opacity=[POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    opacity.fromValue = @(1.0);
    opacity.toValue = @(0.0);
    opacity.duration = 0.5;
    opacity.repeatCount = 6;
    
    opacity.removedOnCompletion = YES;
    opacity.autoreverses = YES;
    
    [self.textField.layer pop_addAnimation:opacity forKey:@"OpacityAnimation"];
    [self.collectionView.layer pop_addAnimation:opacity forKey:@"OpacityAnimation"];
}

-(void)closeOpacityAnimation{
    [self.textField.layer pop_removeAllAnimations];
    [self.collectionView.layer pop_removeAllAnimations];
    self.textField.layer.opacity = 1.0;
    self.collectionView.layer.opacity = 1.0;
}

-(void)starHorCycleAnimation{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.circleView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(self.circleView.list.count ? 30 : 0);
        }];
        
        if (self.circleView.list.count) {
            [self.circleView startAnimation];
        }
    });
}

-(void)stopHorCycleAnimation{
    [self.circleView stopAnimation];
}
#pragma mark - Private
-(void)addObser{
    @weakify(self);
    [[RACSignal combineLatest:@[[RACObserve(self, payModel) distinctUntilChanged],
                                [RACObserve(self, headerModel) distinctUntilChanged]] reduce:^id _Nonnull(RechargePayModel *payModel,RechargeHeaderModel *headrModel){
                                    if (payModel && headrModel) {
                                        return @(YES);
                                    }
                                    return @(NO);
                                }] subscribeNext:^(id  _Nullable x) {
                                    if (![x boolValue]) {
                                        return;
                                    }
                                    @strongify(self);
                                    [NSObject cancelPreviousPerformRequestsWithTarget:self];
                                    [self performSelector:@selector(updateData) afterDelay:0.1];
                                }];

}

-(void)setUI{
    self.backgroundColor = [UIColor whiteColor];
    
    [self addSubview:self.topContainView];
    [self.topContainView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self);
        make.height.mas_equalTo(190);
    }];
    
    [self.topContainView addSubview:self.circleView];
    [self.circleView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (is_IphoneX) {
            make.top.equalTo(self.topContainView).offset(88 + 10);
        }else{
            make.top.equalTo(self.topContainView).offset(64 + 10);
        }
        make.left.right.equalTo(self.topContainView);
        make.height.mas_equalTo(0);
    }];
    
    [self.topContainView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.circleView.mas_bottom).offset(10);
        make.centerX.equalTo(self.topContainView);
    }];
    [self.nameLabel setContentHuggingPriority:UILayoutPriorityFittingSizeLevel forAxis:UILayoutConstraintAxisVertical];
    [self.nameLabel setContentCompressionResistancePriority:UILayoutPriorityFittingSizeLevel forAxis:UILayoutConstraintAxisVertical];
    
    [self.topContainView addSubview:self.balanceLabel];
    [self.balanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.topContainView);
        make.top.equalTo(self.nameLabel.mas_bottom).offset(10);
        make.bottom.equalTo(self.topContainView).offset(-10);
        make.height.mas_equalTo(self.nameLabel);
    }];
    
    [self addSubview:self.textField];
    [self addSubview:self.collectionView];
    
    [self kUpdateConstraints];
}

-(void)updateData{
    if (([self.payModel.riskControlType integerValue] == 2 ||
         [self.payModel.riskControlType integerValue] == 3)) {
            RechargePayAccountsModel *accountsModel = (RechargePayAccountsModel *)self.payModel;
            /**
             1-10@40|10|100
             **/
            NSString *riskControlValue = accountsModel.riskControlValue;
            NSRange range = [riskControlValue rangeOfString:@"@"];
            if (range.length) {
                riskControlValue = [riskControlValue substringFromIndex:range.location + 1];
            }
            NSArray *amoundlist = [[riskControlValue componentsSeparatedByString:@"|"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"length > 0"]];
            
            amoundlist = [amoundlist sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                if ([obj1 integerValue] > [obj2 integerValue]) {
                    return NSOrderedDescending;
                }
                return NSOrderedAscending;
            }];
            
            if (![self compareAmountList:amoundlist]) {
                self.amoundDataList = [amoundlist mutableCopy];
                [amoundlist enumerateObjectsUsingBlock:^(NSString *  _Nonnull amound, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (![amound isShouldNumber]) {
                        [self.amoundDataList removeObject:amound];
                    }else if ([amound floatValue] < [accountsModel.minAmountPerOrder floatValue] ||
                              (self.payModel.maxAmountPerOrder && [amound floatValue] > [accountsModel.maxAmountPerOrder floatValue])){
                        [self.amoundDataList removeObject:amound];
                    }
                }];
                self.textField.text = @"";
                [self textFieldDidEndEditing:self.textField];
            }
        }else{
            NSMutableArray *staticList = @[].mutableCopy;
            for (int i = 0; i < 8; i++) {
                [staticList addObject:staticAmoundList[i]];
            }
            if (![self compareAmountList:staticList]) {
                self.amoundDataList = nil;
                self.textField.text = @"";
            }
            if (self.delegate && [self.delegate respondsToSelector:@selector(updateSecRechargeTableHeaderViewAmound:PayModel:inHeaderModel:)]) {
                [self.delegate updateSecRechargeTableHeaderViewAmound:self.textField.text.length ? self.textField.text : nil PayModel:self.payModel inHeaderModel:self.headerModel];
            }
        }
    [self kUpdateConstraints];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
}

-(void)kUpdateConstraints{
    CGFloat textHeight = 0.0f;

    if (self.payModel &&
        ([self.payModel.riskControlType integerValue] == 2 ||
         [self.payModel.riskControlType integerValue] == 3)) {
        textHeight = 0.0f;
    }else {
        textHeight = 50.0f;
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        [self.textField mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.topContainView.mas_bottom).offset(15);
            make.left.equalTo(self).offset(10);
            make.right.equalTo(self).offset(-10);
            make.height.mas_equalTo(textHeight);
        }];
        [self layoutIfNeeded];
    }];
    
    NSUInteger maxCount = self.amoundDataList.count / kCollectionCellHorizontalMaxCount + (self.amoundDataList.count % kCollectionCellHorizontalMaxCount > 0 ? 1 : 0);
    CGFloat collectHeight = (maxCount + 1) * kCollectionCellSpacing + maxCount * kCollectionCellHeight;
    
    [UIView animateWithDuration:0.2 animations:^{
        [self.collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.textField.mas_bottom).offset(5);
            make.left.right.equalTo(self);
            make.bottom.equalTo(self);
            make.height.mas_equalTo(collectHeight);
        }];
        [self layoutIfNeeded];
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.delegate && [self.delegate respondsToSelector:@selector(updateSecRechargeTableHeaderViewHeight:)]) {
            [self.delegate updateSecRechargeTableHeaderViewHeight:collectHeight + 190 + textHeight + 20];
        }
    });
}

#pragma mark -- UICollectionViewDelegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *amound = [self.amoundDataList safeObjectAtIndex:indexPath.item];
    AmoundCell *cell = (AmoundCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if (cell.isSelect) {
        goto didSelectAmound;
        return;
    }
    self.amoundSelectIdx = [NSNumber numberWithInteger:indexPath.item];
    [self.collectionView reloadData];
    didSelectAmound:
    if (amound) {
        self.textField.text = amound;
        [self closeOpacityAnimation];
        if (self.delegate && [self.delegate respondsToSelector:@selector(updateSecRechargeTableHeaderViewAmound:PayModel:inHeaderModel:)]) {
            [self.delegate updateSecRechargeTableHeaderViewAmound:amound
                                                         PayModel:self.payModel
                                                    inHeaderModel:self.headerModel];
        }
    }
}
#pragma mark -- UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return  1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.amoundDataList.count;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(kCollectionCellSpacing, kCollectionCellSpacing, kCollectionCellSpacing, kCollectionCellSpacing);
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return kCollectionCellSpacing;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return kCollectionCellSpacing;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(kCollectionCellWidth, kCollectionCellHeight);
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeZero;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeZero;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    AmoundCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AmoundCell" forIndexPath:indexPath];
    cell.titileStr = [self.amoundDataList safeObjectAtIndex:indexPath.item];
    cell.isSelect = (self.amoundSelectIdx && [self.amoundSelectIdx integerValue] == indexPath.item);
    return cell;
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self closeOpacityAnimation];
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.text.length) {
        __block NSNumber *index = nil;
        [self.amoundDataList enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isEqualToString:textField.text]) {
                index = [NSNumber numberWithInteger:idx];
                *stop = YES;
            }
        }];
        
        if (!index) {
            goto outer_sendSign;
        }else if ([index integerValue] != [self.amoundSelectIdx integerValue]) {
            self.amoundSelectIdx = index;
            [self.collectionView reloadData];
            if (self.delegate && [self.delegate respondsToSelector:@selector(updateSecRechargeTableHeaderViewAmound:PayModel:inHeaderModel:)]) {
                [self.delegate updateSecRechargeTableHeaderViewAmound:textField.text PayModel:self.payModel inHeaderModel:self.headerModel];
            }
        }
        return;
    }
    outer_sendSign:
    self.amoundSelectIdx = nil;
    [self.collectionView reloadData];
    if (self.delegate && [self.delegate respondsToSelector:@selector(updateSecRechargeTableHeaderViewAmound:PayModel:inHeaderModel:)]) {
        [self.delegate updateSecRechargeTableHeaderViewAmound:textField.text.length ? textField.text : nil PayModel:self.payModel inHeaderModel:self.headerModel];
    }
    
}


#pragma mark - GET/SET
-(UIView *)topContainView{
    if (!_topContainView) {
        _topContainView = [[UIView alloc] init];
        [_topContainView setBackgroundColor:[UIColor redColor]];
    }
    return _topContainView;
}

-(aji_horCircleView *)circleView{
    if (!_circleView) {
        _circleView = [[aji_horCircleView alloc] init];
    }
    _circleView.clipsToBounds = YES;
    _circleView.delegate = self;
    [_circleView setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisVertical];
    [_circleView setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisVertical];
    return _circleView;
}

-(UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [UILabel setAllocLabelWithText:@"--" FontOfSize:18 rgbColor:0xffffff];
        _nameLabel.textAlignment = kCTTextAlignmentCenter;
    }
    return _nameLabel;
}

-(UILabel *)balanceLabel{
    if (!_balanceLabel) {
        _balanceLabel = [UILabel setAllocLabelWithText:@"00.0" FontOfSize:18 rgbColor:0xffffff];
        _balanceLabel.textAlignment = kCTTextAlignmentCenter;
    }
    return _balanceLabel;
}

-(UITextField *)textField{
    if (!_textField) {
        _textField = [[UITextField alloc] initWithFrame:CGRectZero];
        _textField.font = [UIFont fontWithName:@"STHeitiJ-Medium" size:35];
        _textField.placeholder = @"在此输入您的充值金额";
        _textField.delegate = self;
        _textField.keyboardType = UIKeyboardTypeDecimalPad;
        UILabel *yuanLabel = [UILabel setAllocLabelWithText:@"元" FontOfSize:25 rgbColor:0x000000];
        yuanLabel.font = [UIFont boldSystemFontOfSize:20];
        yuanLabel.textColor = [UIColor grayColor];
        [_textField addSubview:yuanLabel];
        [yuanLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(_textField);
            make.right.equalTo(_textField).offset(-10);
        }];
    }
    return _textField;
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT) collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsHorizontalScrollIndicator = NO;
        
        [_collectionView registerClass:[AmoundCell class] forCellWithReuseIdentifier:@"AmoundCell"];
        
    }
    return _collectionView;
}

-(void)setCiclrList:(NSArray<NSAttributedString *> *)ciclrList{
    
    _circleView.list = ciclrList;
    [self starHorCycleAnimation];
}

-(void)setName:(NSString *)name{
    _nameLabel.text = name ? name : @"--";
}

-(void)setBalance:(NSNumber *)balance{
    _balanceLabel.text = [NSString stringWithFormat:@"%.2f元",[balance doubleValue]];
}

-(NSMutableArray <NSString *>*)amoundDataList{
    if (!_amoundDataList) {
        self.amoundSelectIdx = nil;
        _amoundDataList = @[].mutableCopy;
        for (int i = 0; i<8; i++) {
            NSString *amound = staticAmoundList[i];
            if ([amound doubleValue] >= [self.payModel.minAmountPerOrder doubleValue]) {
                if (!self.payModel.maxAmountPerOrder) {
                    [_amoundDataList addObject:amound];
                }else if ([amound doubleValue] <= [self.payModel.maxAmountPerOrder doubleValue]){
                    [_amoundDataList addObject:amound];
                }
            }
        }
    }
    return _amoundDataList;
}

-(BOOL)compareAmountList:(NSArray *)otherList{
    __block BOOL sameAsOtherList = YES;

    if (self.amoundDataList.count != otherList.count) {
        sameAsOtherList =  NO;
    }else{
        [self.amoundDataList enumerateObjectsUsingBlock:^(NSString * _Nonnull listString, NSUInteger listidx, BOOL * _Nonnull listStop) {
            if (![listString isEqualToString:otherList[listidx]]) {
                sameAsOtherList = NO;
                *listStop = YES;
            }
        }];
    }
    return sameAsOtherList;
}

#pragma mark - aji_horCircleViewDelegate
- (void)HorCircleView:(aji_horCircleView *)view MoveCompletedWasIndex:(NSUInteger)idx {
    
}

- (void)HorCircleView:(aji_horCircleView *)view TapUpAtIndex:(NSUInteger)idx {
    
}

#pragma mark - GET/SET

@end
