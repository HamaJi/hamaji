//
//  SecRechargeTableViewCell.m
//  Bet365
//
//  Created by adnin on 2019/5/6.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "SecRechargeTableViewCell.h"

#pragma mark ================================= SecRechargeTableViewCell =================================

@interface SecRechargeTableViewCell()
{
    RechargePayModel *_model;
    NSString *_iconCode;
}


@end


@implementation SecRechargeTableViewCell

@dynamic model;

@dynamic iconCode;

-(void)setIsChoose:(BOOL)isChoose{
    _isChoose = isChoose;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.contentView.backgroundColor = _isChoose ? [UIColor groupTableViewBackgroundColor] : [UIColor whiteColor];
    });
}

+(NSString *)getNibsName:(SecRechargeTableViewCellStatus)status{
    return @[@"SecRechargeNoneCell",
             @"SecRechargeSingleLabelCell",
             @"SecRechargeDoubleLabelCell",
             @"SecRechargeFiveLabelCell",
             @"SecRechargeImageCell"][status];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(NSString *)iconCode{
    return _iconCode;
}

-(void)setIconCode:(NSString *)iconCode{
    if ([iconCode hasPrefix:@"https://"] || [iconCode hasPrefix:@"http://"]) {
        _iconCode = iconCode;
    }else if (iconCode){
        _iconCode = [NSString stringWithFormat:@"%@/data/image/pay-type-icon/%@.png",SerVer_Url,iconCode];
    }
}



-(RechargePayModel *)model{
    return _model;
}

-(void)setModel:(RechargePayModel *)model{
    _model = model;
}

@end

#pragma mark ================================= SecRechargeNoneCell =================================

@interface SecRechargeNoneCell()

@end

@implementation SecRechargeNoneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

#pragma mark ================================= SecRechargeSingleLabelCell =================================

@interface SecRechargeSingleLabelCell()

@property (weak, nonatomic) IBOutlet UIImageView *iconImage;

@property (weak, nonatomic) IBOutlet UILabel *titileLabel;

@end

@implementation SecRechargeSingleLabelCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.backgroundColor = [UIColor whiteColor];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


-(void)setModel:(RechargePayModel *)model{
    [super setModel:model];
    self.titileLabel.text = self.model ? self.model.name : @"";
}

-(void)setIconCode:(NSString *)iconCode{
    [super setIconCode:iconCode];
    [self.iconImage sd_setImageWithURL:[NSURL URLWithString:self.iconCode] placeholderImage:[UIImage imageNamed:@"bankPay"]];
}


@end

#pragma mark ================================= SecRechargeDoubleLabelCell =================================

@interface SecRechargeDoubleLabelCell()

@property (weak, nonatomic) IBOutlet UIImageView *iconImage;

@property (weak, nonatomic) IBOutlet UILabel *firTitileLabel;

@property (weak, nonatomic) IBOutlet UILabel *secTitileLabel;

@end

@implementation SecRechargeDoubleLabelCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setModel:(RechargePayModel *)model{
    [super setModel:model];
    self.firTitileLabel.text = self.model.name ? self.model.name : @"";
    self.secTitileLabel.text = self.model.remarks ? self.model.remarks : @"";
}

-(void)setIconCode:(NSString *)iconCode{
    [super setIconCode:iconCode];
    [self.iconImage sd_setImageWithURL:[NSURL URLWithString:self.iconCode] placeholderImage:[UIImage imageNamed:@"bankPay"]];
}

@end

#pragma mark ================================= SecRechargeFiveLabelCell =================================

@interface SecRechargeFiveLabelCell()

@property (weak, nonatomic) IBOutlet UILabel *firTitileLabel;

@property (weak, nonatomic) IBOutlet UILabel *secTitileLabel;

@property (weak, nonatomic) IBOutlet UILabel *thirdTitileLabel;

@property (weak, nonatomic) IBOutlet UILabel *fourTitileLabel;

@property (weak, nonatomic) IBOutlet UILabel *fiveTitileLabel;

@property (weak, nonatomic) IBOutlet UIImageView *iconImage;

@end

@implementation SecRechargeFiveLabelCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setModel:(RechargePayModel *)model{
    [super setModel:model];
    if ([self.model isKindOfClass:[RechargePayAccountsModel class]]) {
        RechargePayAccountsModel *accountModel = (RechargePayAccountsModel *)self.model;
        self.firTitileLabel.text = accountModel.account ? [NSString stringWithFormat:@"收款账号:%@",accountModel.account] : @"";
        self.secTitileLabel.text = accountModel.owner ? [NSString stringWithFormat:@"收款人:%@",accountModel.owner] : @"";
        self.thirdTitileLabel.text = accountModel.bankName ? [NSString stringWithFormat:@"收款银行:%@",accountModel.bankName] : @"";
        self.fourTitileLabel.text = accountModel.bankAddress ? [NSString stringWithFormat:@"收款支行:%@",accountModel.bankAddress] : @"";
    }
    self.fiveTitileLabel.text = self.model.remarks ? self.model.remarks : @"";
}

-(void)setIconCode:(NSString *)iconCode{
    [super setIconCode:iconCode];
    [self.iconImage sd_setImageWithURL:[NSURL URLWithString:self.iconCode] placeholderImage:[UIImage imageNamed:@"bankBack"]];
}
@end

#pragma mark ================================= SecRechargeImageCell =================================

@interface SecRechargeImageCell()

@property (weak, nonatomic) IBOutlet UIImageView *iconImage;

@end

@implementation SecRechargeImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setModel:(RechargePayModel *)model{
    [super setModel:model];
    RechargePayCardModel *cardModel = (RechargePayCardModel *)self.model;
    [self.iconImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SerVer_Url,cardModel.rechangeImage]] placeholderImage:[UIImage imageNamed:@"bankPay"]];

}

-(void)setIconCode:(NSString *)iconCode{
    [super setIconCode:iconCode];
}

@end


