//
//  jesseNewBankViewController.m
//  Bet365
//
//  Created by jesse on 2017/11/18.
//  Copyright © 2017年 jesse. All rights reserved.
#import "jesseNewBankViewController.h"
#import "Bet365traceTextView.h"
#import <MJRefreshGifHeader.h>
#import <JSONModel.h>
#import "JesseRechargeConfigBankModel.h"
#import "KFCPHomeAlertView.h"
#import "JesseNewbankTransFerViewController.h"
#import "jesseBankOnlineViewController.h"
#import "jesseWechatScanCodeViewController.h"
#import "jesseMaintenanceTableViewCell.h"
#import "NetWorkMannager+Account.h"
#import "TopUpCardCell.h"
#import "aji_horCircleView.h"
#import <IQKeyboardManager.h>
#import "UIViewControllerSerializing.h"
#define BACKBUTTONTOPHEIGHT 20
#define BACKHEIGHT 20
#define BALANCEVIEWTOPHEIGHT 55
#define BALANCEVIEWHEIGHT 49
#define BALANCEBOTTOM 13
#define TABLEVIEWHEIGHT 188
#define BANKVIEWWIDTH 243
#define BANKBACKHEIGHT 119
#define BANKBUTTON 102
static NSString *const bankTransferNormalCell = @"BanktransferNormalCell";
static NSString *const RegisterOnlineCell = @"rechOnlineCell";
static NSString *const reisterMaintenanceNewCell = @"tenanceCell";
/**TOPBanlaceDelegate**/
@protocol TOPRECHARGEDELEGATE<NSObject>
-(void)popNavgation;/**返回上级**/
-(void)rechArgeRecord;/**投注记录**/
@end
/**选择面板Delegate**/
@protocol selectMainBoardDelegate<NSObject>
-(void)returnCurrenRechargeMoney:(NSInteger)money;/**传入金额**/
@end
/**银行转账delegate**/
@protocol bankTransDelegate<NSObject>
-(void)returnBankTrans:(NSInteger)num;
@end

@implementation payModel
+(BOOL)propertyIsOptional:(NSString *)propertyName{
    return YES;
}
+(JSONKeyMapper *)keyMapper{
    return [[JSONKeyMapper alloc]initWithModelToJSONDictionary:@{@"bankId":@"id"}];
}
@end

@implementation TransferMoneyModel
+(BOOL)propertyIsOptional:(NSString *)propertyName{
    return YES;
}
+(JSONKeyMapper *)keyMapper{
    return [[JSONKeyMapper alloc]initWithModelToJSONDictionary:@{@"bankId":@"id"}];
}
@end
@implementation TransferOnlieModel
@end
@implementation onlineModel
+(BOOL)propertyIsOptional:(NSString *)propertyName{
    return YES;
}
+(JSONKeyMapper *)keyMapper{
    return [[JSONKeyMapper alloc]initWithModelToJSONDictionary:@{@"bankId":@"id"}];
}
@end

@interface transferBankBackView:UIView
-(instancetype)initMain:(CGRect)frame parameter:(TransferOnlieModel *)model AndMark:(NSInteger)mark;
@property(nonatomic,weak)id <bankTransDelegate> delegate;
@end
@interface transferBankBackView()
{
    UILabel *_bankAccount;
    UILabel *_bankName;
    UILabel *_bankAdress;
    UILabel *_accountName;
    UILabel *_markLabel;
}
@property(nonatomic,strong)UIButton *MainButton;
@property(nonatomic,assign)NSInteger mark;
@property(nonatomic,strong)TransferOnlieModel *model;

@end
@implementation transferBankBackView
-(instancetype)initMain:(CGRect)frame parameter:(TransferOnlieModel *)model AndMark:(NSInteger)mark{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = JesseColor(255, 255, 255);
        self.mark = mark;
        self.model = model;
        [self layoutUI];
    }
    return self;
}
-(void)layoutUI{
    [self MainButton];
    _bankAccount.text = self.model.transModel.account;
    _bankName.text = self.model.transModel.bankName;
    _bankAdress.text = self.model.transModel.bankAddress;
    _accountName.text = self.model.transModel.owner;
    if (self.model.transModel.remarks && self.model.transModel.remarks.length > 0) {
        _markLabel.text = [NSString stringWithFormat:@"备注:%@",self.model.transModel.remarks];
    }
}
-(UIButton *)MainButton{
    if (_MainButton==nil) {
        _MainButton = [jesseGuideUIConfig getCurrenButtonUiByRect:CGRectMake(0, 0, WIDTH-40, 138) AndButtonTitle:@"" AndTitleColor:[UIColor clearColor] AndtextAlignment:NSTextAlignmentCenter And:@selector(bankClick:) Andtarget:self AndlayerCorner:0 AndUseMasonry:NO];
        [_MainButton setImage:[UIImage imageNamed:@"bankBack"] forState:UIControlStateNormal];
        [self addSubview:_MainButton];
        _bankAccount = [jesseGuideUIConfig getCurrenLableUiByRect:CGRectMake(18, 0, WIDTH-60, 25) AndLabelTitle:@"" AndTitleColor:JesseColor(255, 255, 255) AndTextAlignment:NSTextAlignmentLeft AndIsAdWidth:YES AndBackColor:[UIColor clearColor] AndUseMasonry:NO];
        _bankAccount.numberOfLines = 0;
        _bankAccount.font = [UIFont boldSystemFontOfSize:17];
        [_MainButton addSubview:_bankAccount];
        _accountName = [jesseGuideUIConfig getCurrenLableUiByRect:CGRectMake(18, 25, WIDTH-60, 25) AndLabelTitle:@"" AndTitleColor:JesseColor(255, 255, 255) AndTextAlignment:NSTextAlignmentLeft AndIsAdWidth:YES AndBackColor:[UIColor clearColor] AndUseMasonry:NO];
        _accountName.numberOfLines = 0;
        _accountName.font = [UIFont boldSystemFontOfSize:17];
        [_MainButton addSubview:_accountName];
        _bankName = [jesseGuideUIConfig getCurrenLableUiByRect:CGRectMake(18, 50, WIDTH-60, 25) AndLabelTitle:@"" AndTitleColor:JesseColor(255, 255, 255) AndTextAlignment:NSTextAlignmentLeft AndIsAdWidth:YES AndBackColor:[UIColor clearColor] AndUseMasonry:NO];
        _bankName.numberOfLines = 0;
        _bankName.font = [UIFont boldSystemFontOfSize:17];
        [_MainButton addSubview:_bankName];
        _bankAdress = [jesseGuideUIConfig getCurrenLableUiByRect:CGRectMake(18, 75, WIDTH-60, 25) AndLabelTitle:@"" AndTitleColor:JesseColor(255, 255, 255) AndTextAlignment:NSTextAlignmentLeft AndIsAdWidth:YES AndBackColor:[UIColor clearColor] AndUseMasonry:NO];
        _bankAdress.numberOfLines = 0;
        _bankAdress.font = [UIFont boldSystemFontOfSize:17];
        [_MainButton addSubview:_bankAdress];
        _markLabel = [jesseGuideUIConfig getCurrenLableUiByRect:CGRectMake(18, 100, WIDTH-70, 38) AndLabelTitle:@"" AndTitleColor:[UIColor whiteColor] AndTextAlignment:NSTextAlignmentLeft AndIsAdWidth:NO AndBackColor:[UIColor clearColor] AndUseMasonry:NO];
        _markLabel.adjustsFontSizeToFitWidth = YES;
        _markLabel.font = [UIFont boldSystemFontOfSize:17];
        _markLabel.numberOfLines = 0;
        [self addSubview:_markLabel];
    }
    return _MainButton;
}
-(void)bankClick:(UIButton *)bank{
    [self.delegate returnBankTrans:self.mark];
}
@end
/**BanktransferMainViewEnd**/
typedef void(^cellBlock)(NSInteger num);
@interface BankTransferCell:UITableViewCell
-(void)updateCurrenBankTransfer:(TransferOnlieModel *)transFerOnlineM; /**进行处理数据**/
@property(nonatomic,copy)cellBlock block;/**回調当前任务**/
@property(strong,nonatomic)NSMutableArray *newObjArr;/**新对象**/
@end
@interface BankTransferCell()<bankTransDelegate>
@end
@implementation BankTransferCell
-(void)updateCurrenBankTransfer:(TransferOnlieModel *)transFerOnlineM{
    [self.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        transferBankBackView *trans = [[transferBankBackView alloc]initMain:CGRectMake(0, 0, WIDTH-40, 138) parameter:transFerOnlineM AndMark:0];
       trans.center = self.contentView.center;
        trans.delegate = self;
        trans.layer.cornerRadius = 5;
        trans.layer.masksToBounds = YES;
        [self.contentView addSubview:trans];
}
/**TransDelegate**/
-(void)returnBankTrans:(NSInteger)num{
    self.block(0);
}
-(NSMutableArray *)newObjArr{return _newObjArr?_newObjArr:(_newObjArr=@[].mutableCopy);};
@end
/**BanTransFerEnd**/
/**OnlieCellStart**/
@interface onlineCell:UITableViewCell
-(void)updateCurren:(NSString *)loadimage isHttpLoad:(BOOL)http AndName:(NSString *)name AndMark:(NSString *)mark;
@end
@interface onlineCell()
{
    UIImageView *_boardImage;
    UILabel *_nameLabel;
    UILabel *_markLabel;
}
@end
@implementation onlineCell
-(void)updateCurren:(NSString *)loadimage isHttpLoad:(BOOL)http AndName:(NSString *)name AndMark:(NSString *)mark{
    [self.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    _boardImage = [[UIImageView alloc]init];
    _boardImage.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:_boardImage];
    [_boardImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView.mas_left).offset(19);
        make.centerY.mas_equalTo(self.contentView);
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(40);
    }];
    [_boardImage sd_setImageWithURL:[NSURL URLWithString:loadimage] placeholderImage:[UIImage imageNamed:@"bankPay"]];
//    (!http)?[_boardImage setImage:[UIImage imageNamed:@"pay"]]:[_boardImage sd_setImageWithURL:[NSURL URLWithString:loadimage]];
    UIView *contentview = [jesseGuideUIConfig getCurrnViewUiByRect:CGRectMake(77, 0, WIDTH-92, 68) AndBackColor:[UIColor clearColor] AndUseMasonry:NO];
    [self.contentView addSubview:contentview];
    UIView *indrow = [jesseGuideUIConfig getCurrnViewUiByRect:CGRectMake(0, 68, WIDTH, 1) AndBackColor:[UIColor groupTableViewBackgroundColor] AndUseMasonry:NO];
    [self.contentView addSubview:indrow];
    if (mark.length>0) {
        _nameLabel = [jesseGuideUIConfig getCurrenLableUiByRect:CGRectMake(0,5,  WIDTH-92, 14) AndLabelTitle:name AndTitleColor:JesseColor(106, 106, 108) AndTextAlignment:NSTextAlignmentLeft AndIsAdWidth:YES AndBackColor:[UIColor clearColor] AndUseMasonry:NO];
        _nameLabel.font = [UIFont systemFontOfSize:16];
        [contentview addSubview:_nameLabel];
        _markLabel = [jesseGuideUIConfig getCurrenLableUiByRect:CGRectMake(0, 19, WIDTH-92, 49) AndLabelTitle:mark AndTitleColor:JesseColor(142, 142, 142) AndTextAlignment:NSTextAlignmentLeft AndIsAdWidth:NO AndBackColor:[UIColor clearColor] AndUseMasonry:NO];
        _markLabel.numberOfLines = 0;
        _markLabel.font = [UIFont systemFontOfSize:13];
        [contentview addSubview:_markLabel];
    }else{
        _nameLabel = [jesseGuideUIConfig getCurrenLableUiByRect:CGRectMake(0,0,  WIDTH-92, 68) AndLabelTitle:name AndTitleColor:JesseColor(106, 106, 108) AndTextAlignment:NSTextAlignmentLeft AndIsAdWidth:YES AndBackColor:[UIColor clearColor] AndUseMasonry:NO];
        _nameLabel.font = [UIFont systemFontOfSize:16];
        [contentview addSubview:_nameLabel];
    }
}
@end
/**OnlineCellEnd**/
/******************/
/**CellKindEnd**/
/**段头**/
@interface sectionTitle:UIView
{
    UILabel *_label;
}
-(instancetype)initTitle:(CGRect)rect;
@end
@implementation sectionTitle
-(instancetype)initTitle:(CGRect)rect{
    self = [super initWithFrame:rect];
    if (self) {
        self.backgroundColor = JesseColor(231, 233, 232);
        [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self layoutUI];
    }
    return self;
}
-(void)layoutUI{
    _label = [jesseGuideUIConfig getCurrenLableUiByRect:CGRectMake(0, 0, 0, 0) AndLabelTitle:@"选择充值方式" AndTitleColor:JesseColor(135, 135, 135) AndTextAlignment:NSTextAlignmentLeft AndIsAdWidth:NO AndBackColor:[UIColor clearColor] AndUseMasonry:YES];
    _label.font = [UIFont systemFontOfSize:17];
    [self addSubview:_label];
    [_label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(19);
        make.top.right.bottom.equalTo(self);
    }];
}
@end
/**NormalSectionView**/
typedef void(^sectionBlock)(NSInteger num,BOOL openvalue);
@interface normalSectionView:UITableViewHeaderFooterView
-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier AndCurrenTag:(NSInteger)tag;
-(void)updateCurrenSourceDesName:(NSString *)desName AndPrompt:(NSString *)prompt;
@property(nonatomic,copy)sectionBlock block;
-(void)update:(BOOL)currenOpenValue;
@end
@interface normalSectionView()
{
    UILabel *_desLabel;
    UIImageView *_markImage;
    UILabel *_promptLabel;
}
@property(nonatomic,assign)NSInteger markTag;
@property(nonatomic,assign)BOOL open;
@end
@implementation normalSectionView
/**初始化Main**/
-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier AndCurrenTag:(NSInteger)tag{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = JesseColor(255, 255, 255);
        [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        self.markTag = tag;
        self.open = NO;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
        self.userInteractionEnabled = YES;
        [self addGestureRecognizer:tap];
        [self layoutUI];
    }
    return self;
}

/**LayoutUI**/
-(void)layoutUI{
    _markImage = [jesseGuideUIConfig getCurrenImageUiRect:CGRectMake(0, 0, 0, 0) AndImage:[UIImage imageNamed:@"right_lage"] AnduseMasonry:YES];
    _markImage.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:_markImage];
    [_markImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(self).offset(-10);
        make.centerY.mas_equalTo(self);
        make.width.height.mas_equalTo(15);
    }];
    _desLabel = [jesseGuideUIConfig getCurrenLableUiByRect:CGRectMake(0, 0, 0, 0) AndLabelTitle:@"-" AndTitleColor:JesseColor(36, 37, 39) AndTextAlignment:NSTextAlignmentLeft AndIsAdWidth:YES AndBackColor:[UIColor clearColor] AndUseMasonry:YES];
    _desLabel.font = [UIFont systemFontOfSize:17];
    [self addSubview:_desLabel];
    [_desLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).offset(10);
        make.centerY.mas_equalTo(self);
    }];
    UIView *indrow = [jesseGuideUIConfig getCurrnViewUiByRect:CGRectZero AndBackColor:JesseColor(205, 205, 206) AndUseMasonry:YES];
    [self addSubview:indrow];
    [indrow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.bottom.equalTo(self);
        make.height.mas_equalTo(0.5);
    }];
}
/**更新信息**/
-(void)updateCurrenSourceDesName:(NSString *)desName AndPrompt:(NSString *)prompt{
    _desLabel.text = desName;
    
}
/**手势触发**/
-(void)tap:(UITapGestureRecognizer *)tap{
    self.open = !self.open;
    if (self.open) {
        [UIView animateWithDuration:0.2 animations:^{
            _markImage.transform = CGAffineTransformMakeRotation(M_PI_2);
        }];
    }else{
        [UIView animateWithDuration:0.2 animations:^{
            _markImage.transform = CGAffineTransformIdentity;
        }];
    }
    self.block(self.markTag,self.open);
}
-(void)update:(BOOL)currenOpenValue{
    if (currenOpenValue) {
        _markImage.transform = CGAffineTransformMakeRotation(M_PI_2);
    }else{
        _markImage.transform = CGAffineTransformIdentity;
    }
}
@end

@interface selectView:UIView
-(instancetype)initSelectMain:(CGRect)frame;/**初始化当前frame**/
-(void)cancleSelect;/**取消点击**/
-(void)selectAnimationMoney:(NSInteger)num;/**选择当前充值金额**/
@property(nonatomic,strong)NSMutableArray *BoolArr;/**存储当前选择中**/
@property(nonatomic,strong)NSArray *rechargeMoneyArr;/**存储固定金额**/
@property(nonatomic,weak)id <selectMainBoardDelegate> delegate;
@property(nonatomic,assign)NSInteger index;
@end
@implementation selectView
/**1**/
-(instancetype)initSelectMain:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.index = 0;
        [self layoutUI];
    }
    return self;
}
/**初始化当前存储数组**/
-(NSMutableArray *)BoolArr{
    if (_BoolArr==nil) {
        _BoolArr = [[NSMutableArray alloc]init];
        for (int i = 0; i<8; i++) {
            if (i==0) {
                [_BoolArr addObject:[NSNumber numberWithBool:YES]];
            }else{
                [_BoolArr addObject:[NSNumber numberWithBool:NO]];
            }
        }
    }
    return _BoolArr;
}
-(NSArray *)rechargeMoneyArr{
    if (_rechargeMoneyArr==nil) {
        _rechargeMoneyArr = @[@"50",@"100",@"300",@"500",@"1000",@"2000",@"3000",@"5000"];
    }
    return _rechargeMoneyArr;
}
/**LayoutUI**/
-(void)layoutUI{
    CGFloat width = (WIDTH-52)/4;
    CGFloat height = 43;
    CGFloat cornerRadius = 3.8;
    for (int i = 0; i<8;i++ ) {
        CGFloat x = (i%4)*width+(i%4)*8;
        CGFloat y = (i/4)*43+(i/4)*7;
        UIButton *moneyButton = [jesseGuideUIConfig getCurrenButtonUiByRect:CGRectMake(x, y, width, height) AndButtonTitle:[NSString stringWithFormat:@"%@元",self.rechargeMoneyArr[i]] AndTitleColor:JesseColor(220, 21, 28) AndtextAlignment:NSTextAlignmentCenter And:@selector(moneyClick:) Andtarget:self AndlayerCorner:cornerRadius AndUseMasonry:NO];
        moneyButton.titleLabel.font = [UIFont systemFontOfSize:16];
        moneyButton.tag = 300+i;
        moneyButton.backgroundColor = ([self.BoolArr[i] boolValue])?JesseColor(220, 21, 28) :JesseColor(255, 255, 255);
        [moneyButton setTitleColor: ([self.BoolArr[i] boolValue])?JesseColor(255, 255, 255):JesseColor(220, 21, 28) forState:UIControlStateNormal];
        moneyButton.layer.cornerRadius =cornerRadius;
        moneyButton.layer.masksToBounds = YES;
        moneyButton.layer.borderWidth = 1;
        moneyButton.layer.borderColor =([self.BoolArr[i] boolValue])?[UIColor clearColor].CGColor:JesseColor(204, 204, 204).CGColor;
        [self addSubview:moneyButton];
    }
}
/**2**/
-(void)cancleSelect{
    [self.BoolArr replaceObjectAtIndex:self.index withObject:[NSNumber numberWithBool:NO]];
    [UIView animateWithDuration:0.25 animations:^{
        UIButton *button = (UIButton *)[self viewWithTag:300+self.index];
        button.backgroundColor = JesseColor(255, 255, 255);
        button.titleLabel.textColor = JesseColor(220, 21, 28);
        button.layer.borderColor =JesseColor(204, 204, 204).CGColor;
    }];
}
/**3**/
-(void)selectAnimationMoney:(NSInteger)num{
    
    [self.BoolArr replaceObjectAtIndex:self.index withObject:[NSNumber numberWithBool:NO]];
    [UIView animateWithDuration:0.25 animations:^{
        UIButton *button = (UIButton *)[self viewWithTag:300+self.index];
        button.backgroundColor = JesseColor(255, 255, 255);
        button.titleLabel.textColor = JesseColor(220, 21, 28);
        button.layer.borderColor =JesseColor(204, 204, 204).CGColor;
    } completion:^(BOOL finished) {
        UIButton *button =(UIButton *)[self viewWithTag:300+num];
        button.backgroundColor = JesseColor(220, 21, 28);
        button.titleLabel.textColor = JesseColor(255, 255, 255);
        button.layer.borderColor = [UIColor clearColor].CGColor;
    }];
    self.index = num;
    [self.BoolArr replaceObjectAtIndex:self.index withObject:[NSNumber numberWithBool:YES]];
    
}
/**button触发**/
-(void)moneyClick:(UIButton *)money{
    [self.BoolArr replaceObjectAtIndex:self.index withObject:[NSNumber numberWithBool:NO]];
    [UIView animateWithDuration:0.25 animations:^{
        UIButton *button = (UIButton *)[self viewWithTag:300+self.index];
        button.backgroundColor = JesseColor(255, 255, 255);
        button.titleLabel.textColor = JesseColor(220, 21, 28);
        button.layer.borderColor =JesseColor(204, 204, 204).CGColor;
    } completion:^(BOOL finished) {
        UIButton *button =(UIButton *)[self viewWithTag:money.tag];
        button.backgroundColor = JesseColor(220, 21, 28);
        button.titleLabel.textColor = JesseColor(255, 255, 255);
        button.layer.borderColor = [UIColor clearColor].CGColor;
    }];
    self.index =  money.tag-300;
    [self.BoolArr replaceObjectAtIndex:money.tag-300 withObject:[NSNumber numberWithBool:YES]];
    [self.delegate returnCurrenRechargeMoney:[self.rechargeMoneyArr[money.tag-300] integerValue]];
}
@end
@interface payHeadView:UIView
-(instancetype)initView:(CGRect)frame;/**初始化当前**/
-(NSString *)currenMoney;
@end
@interface payHeadView()<selectMainBoardDelegate,UITextFieldDelegate>
{
    Bet365traceTextView *_textView;/**输入框**/
    
    
}
@property(nonatomic,strong)selectView *rechArgeMoneyBoard;/**充值面板**/
@property(strong,nonatomic)UIView *scanRechargeView;
@property(strong,nonatomic)NSArray *boradRechArge;/**充值存储**/
@end
@implementation payHeadView
-(instancetype)initView:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = JesseColor(246, 246, 246);
        [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self layoutUI];
    }
    return self;
}
-(selectView *)rechArgeMoneyBoard{
    if (_rechArgeMoneyBoard==nil) {
        _rechArgeMoneyBoard = [[selectView alloc]initSelectMain:CGRectMake(14, 82, WIDTH-(14*2), 93)];
        _rechArgeMoneyBoard.delegate = self;
        [self addSubview:_rechArgeMoneyBoard];
    }
    return _rechArgeMoneyBoard;
}
-(NSArray *)boradRechArge{
    if (_boradRechArge==nil) {
        _boradRechArge = @[@"50",@"100",@"300",@"500",@"1000",@"2000",@"3000",@"5000"];
    }
    return _boradRechArge;
}
/**LayoutUI<##>**/
-(void)layoutUI{
    _textView = [[Bet365traceTextView alloc]init];
    _textView.font = [UIFont systemFontOfSize:17];
    _textView.delegate = self;
    _textView.textColor = JesseColor(149, 149, 149);
    _textView.keyboardType = UIKeyboardTypeDecimalPad;
    _textView.attributedPlaceholder = [[NSAttributedString alloc]initWithString:@"输入您的充值金额" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17],NSForegroundColorAttributeName:JesseColor(149, 149, 149)}];
    _textView.borderStyle = UITextBorderStyleNone;
    _textView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_textView];
    [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top).offset(19);
        make.left.mas_equalTo(self.mas_left).offset(14);
        make.right.mas_equalTo(self.mas_right).offset(-14);
        make.height.mas_equalTo(49);
    }];
    /**Board**/
    [self rechArgeMoneyBoard];
}
/**充值面板代理触发**/
-(void)returnCurrenRechargeMoney:(NSInteger)money{
    _textView.text = [@(money) stringValue];
}
/**获取当前充值金额**/
-(NSString *)currenMoney{
    return  _textView.text;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *resultstring = textField.text;
    if (range.length==0) {
        if ([resultstring containsString:@"."]&&[string isEqualToString:@"."]) {
            return NO;
        }
        resultstring = [textField.text stringByAppendingString:string];
    }else{
        NSMutableString *stringMain = [[NSMutableString alloc]initWithString:resultstring];
        [stringMain deleteCharactersInRange:NSMakeRange(stringMain.length-1,1)];
        resultstring = stringMain;
    }
    if ([self.boradRechArge containsObject:resultstring]) {
        [self.boradRechArge enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([resultstring integerValue]==[obj integerValue]) {
                [self.rechArgeMoneyBoard selectAnimationMoney:idx];
            }
        }];
    }else{
        [self.rechArgeMoneyBoard cancleSelect];
    }
    return YES;
}
@end
@interface jesseNewBankViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *moneyLb;
@property (weak, nonatomic) IBOutlet UILabel *accountLb;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@property(strong,nonatomic)UITableView *maintableview;
@property(strong,nonatomic)payHeadView *payHead;/**表头**/
@property(strong,nonatomic)KFCPHomeAlertView *alertView;/**提示框**/
@property(strong,nonatomic)NSURLSessionTask *sessionTask;/**网络执行任务**/
@property(strong,nonatomic)NSMutableArray *dataArr;/**当前数据数组**/
@property(strong,nonatomic)NSMutableArray *sectionBoolValue;/**value**/
@property(strong,nonatomic)AFHTTPSessionManager *manager;/**网络执行者**/
@property(strong,nonatomic)NSMutableArray *allPaySourceArr;/**所有支付充值数据**/
@property(strong,nonatomic)NSMutableArray *bankNameArr;
@property(strong,nonatomic)NSMutableArray *newjudeBankFigArr;/**进行newbankfig判断**/
@property (strong, nonatomic) IBOutlet aji_horCircleView *noticeView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *noticeContstraintHeight;

-(void)getPayAccountProgress:(void(^)(BOOL progress))progressBlock Parameter:(NSDictionary *)parameter bankTotal:(void(^)(NSArray *))bankArr;/**获取所有转账账户**/
-(void)getAllchannelProgress:(void(^)(BOOL progress))progressBlock Parameter:(NSDictionary *)parameter bankTotal:(void(^)(NSArray *))bankArr;/**获取所有在线支付通道**/
-(void)getCurrenGetSubAccount:(NSDictionary *)parameter AndSection:(NSInteger)section;/**获取当前子账号**/
-(void)GetRechargeConfig:(NSDictionary *)parameter And:(TransferOnlieModel *)model and:(void(^)(BOOL iscode,NSString *scanRemark))greenGo;/**获取限制信息**/
@end
static jesseNewBankViewController *controller = nil;



@implementation jesseNewBankViewController


-(AFHTTPSessionManager *)manager{
    if (_manager==nil) {
        _manager = [AFHTTPSessionManager manager];
        _manager.responseSerializer.acceptableContentTypes = [jesseGuideUIConfig returnResponType];
        _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        [_manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
        _manager.requestSerializer.timeoutInterval = 5.f;
        [_manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];

    }
    return _manager;
}
-(payHeadView *)payHead{
    if (_payHead==nil) {
        _payHead = [[payHeadView alloc]initView:CGRectMake(0, 0, WIDTH, TABLEVIEWHEIGHT)];
    }
    return _payHead;
}
#pragma mark--初始化弹框
-(KFCPHomeAlertView *)alertView
{
    if (_alertView==nil) {
        _alertView = [[KFCPHomeAlertView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
        [[UIApplication sharedApplication].keyWindow addSubview:_alertView];
        [self.alertView setRemove:^(){
            _alertView = nil;
        }];
    }
    return _alertView;
}

-(NSMutableArray *)dataArr{
    return _dataArr?_dataArr:(_dataArr=@[].mutableCopy);
};

-(NSMutableArray *)newjudeBankFigArr{
    return _newjudeBankFigArr ?_newjudeBankFigArr:(_newjudeBankFigArr=@[].mutableCopy);
};

-(NSMutableArray *)bankNameArr{
    if (_bankNameArr==nil) {
        _bankNameArr = [[NSMutableArray alloc]init];
        for (int i = 0; i<2; i++) {
            NSMutableArray *sonA = [[NSMutableArray alloc]init];
            [_bankNameArr addObject:sonA];
        }
    }
    return _bankNameArr;
};
/**sectionBoolValue**/
-(NSMutableArray *)sectionBoolValue{
    if (_sectionBoolValue==nil) {
        _sectionBoolValue = [[NSMutableArray alloc]init];
        for (int i = 0; i<self.dataArr.count; i++) {
            [_sectionBoolValue addObject:[NSNumber numberWithBool:NO]];
        }
    }
    return _sectionBoolValue;
}
-(NSMutableArray *)allPaySourceArr{
    if (_allPaySourceArr==nil) {
        _allPaySourceArr = [[NSMutableArray alloc]init];
        for (int i = 0; i<self.dataArr.count; i++) {
            NSMutableArray *arr = [[NSMutableArray alloc]init];
            [_allPaySourceArr addObject:arr];
        }
    }
    return _allPaySourceArr;
}
/**网络任务处理**/
-(void)mjRefresh{
    [self.dataArr removeAllObjects];
    [self.tableView reloadData];
    [[customLoadingTool initCustomManager] showRefreshActivityAnimationForLoading];
    dispatch_group_t group = dispatch_group_create();
    //一凡 2019-3-17新增充值卡
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        __block NSArray *topUpCardArr = [NSArray array];
    __block NSArray *mainArr = [NSArray array];
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [self getRechargeCard:^(BOOL success,NetReceiveObjType type,id responseObject) {
            if (type != NetReceiveObjType_CACHE) {
                
                if ([responseObject isKindOfClass:[NSArray class]]) {
                    topUpCardArr = (id)responseObject;
                }
                
                dispatch_semaphore_signal(sema);//1
            }
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        self.sessionTask = [self.manager GET:[jesseGuideUIConfig returnPayInterface:GetAll] parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            [[customLoadingTool initCustomManager] disMissFresh];
            [self.tableView.mj_header endRefreshing];
            NSData *data = (id)responseObject;
            NSArray *array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            mainArr = array;
            dispatch_semaphore_signal(sema);//1
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            dispatch_semaphore_signal(sema);//1
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        
        
        [mainArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *dic = obj;
            payModel *model = [[payModel alloc]initWithDictionary:dic error:nil];
            model.promptS = @"点击查看您要充值的选项";
            [self.dataArr addObject:model];
        }];
        
        
        __block BOOL isIos = NO;
        [topUpCardArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *dic = obj;
            if ([dic[@"rechangeIsIos"] integerValue] == 1) {
                isIos = YES;
            }
        }];
        
        if (isIos) {
            payModel *model = [[payModel alloc]init];
            model.promptS = @"点击查看您要充值的选项";
            model.name = @"充值卡";
            model.code = @"topUp";
            
            [self.dataArr addObject:model];
        }
        
        
        if (self.sectionBoolValue!=nil) {
            if (self.dataArr.count != self.sectionBoolValue.count) {
                [self.sectionBoolValue removeAllObjects];
                self.sectionBoolValue = nil;
                [self.allPaySourceArr removeAllObjects];
                self.allPaySourceArr = nil;
            }
        }
        
        
        
        
        //-----------------充值卡子账户----------------------
        NSMutableArray *source = [[NSMutableArray alloc]init];
        [topUpCardArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *dic = obj;
            if ([dic[@"rechangeIsIos"] integerValue] == 1) {
                
                TransferOnlieModel *model = [[TransferOnlieModel alloc]init];
                model.markWay = 2;
                TransferMoneyModel *transModel = [[TransferMoneyModel alloc]init];
                model.rechangeImage = dic[@"rechangeImage"];
                model.rechangeLink = dic[@"rechangeLink"];
                model.sortState = 0;
                model.transModel = transModel;
                
                [[SDWebImageDownloader sharedDownloader]downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SerVer_Url,dic[@"rechangeImage"]]] options:SDWebImageDownloaderLowPriority progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                    NSLog(@"with:%f        height:%f",image.size.width,image.size.height);
                    model.width = image.size.width;
                    model.height = image.size.height;
                }];
                
                [source addObject:model];
            }
        }];
        
        NSMutableArray *list = [self.allPaySourceArr safeObjectAtIndex:self.allPaySourceArr.count-1];
        [ list addObjectsFromArray:source];
        NSSortDescriptor *soreDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"sortState" ascending:YES];
        
        NSArray *newsortArr = [list sortedArrayUsingDescriptors:[NSArray arrayWithObject:soreDescriptor]];
        if (newsortArr) {
            [self.allPaySourceArr safeReplaceObjectAtIndex:self.allPaySourceArr.count-1 withObject:newsortArr.mutableCopy];
        }else{
            [self.allPaySourceArr safeReplaceObjectAtIndex:self.allPaySourceArr.count-1 withObject:@[].mutableCopy];
        }
        
        
        
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        [[customLoadingTool initCustomManager] disMissFresh];
    });
    
}
                          
/**获取所有子账户**/
-(void)getCurrenGetSubAccount:(NSDictionary *)parameter AndSection:(NSInteger)section{
    if ([[self.allPaySourceArr safeObjectAtIndex:section] count] > 0){
        [self.tableView reloadData];
        return;
    }
    
    [[customLoadingTool initCustomManager] showRefreshActivityAnimationForLoading];
    __block NSInteger errorCount = 0;
    __block NSArray *transfer;
    __block NSArray *online;
    
    dispatch_group_t group = dispatch_group_create();
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [self getPayAccountProgress:^(BOOL progress) {
            if (progress) {
            }else{
                dispatch_semaphore_signal(sema);//1
            }
        } Parameter:parameter bankTotal:^(NSArray *bankArr) {
            transfer = bankArr;
            dispatch_semaphore_signal(sema);//1
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    
    

    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [self getAllchannelProgress:^(BOOL progress) {
            if (progress) {
            }else{
                errorCount++;
                dispatch_semaphore_signal(sema);//1
            }
        } Parameter:parameter bankTotal:^(NSArray *bankArr) {
            online = bankArr;
            dispatch_semaphore_signal(sema);//1
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (errorCount>0) {
                [[customLoadingTool initCustomManager] disMissFresh];
                [SVProgressHUD showErrorWithStatus:@"任务处理失败"];
                NSMutableArray *list = [self.allPaySourceArr safeObjectAtIndex:section];
                if (list) {
                    [self.allPaySourceArr[section] removeAllObjects];
                }
                
                [self.sectionBoolValue replaceObjectAtIndex:section withObject:[NSNumber numberWithBool:NO]];
                normalSectionView *normal = (normalSectionView *)[self.tableView headerViewForSection:section];
                [normal update:NO];
            }else{
                NSMutableArray *list = [self.allPaySourceArr safeObjectAtIndex:section];
                [ list addObjectsFromArray:transfer];
                [list addObjectsFromArray:online];
                NSLog(@"list:%@",list);
                NSSortDescriptor *soreDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"sortState" ascending:YES];
                NSArray *newsortArr = [list sortedArrayUsingDescriptors:[NSArray arrayWithObject:soreDescriptor]];
                if (newsortArr) {
                    [self.allPaySourceArr replaceObjectAtIndex:section withObject:newsortArr.mutableCopy];
                }else{
                    [self.allPaySourceArr replaceObjectAtIndex:section withObject:@[].mutableCopy];
                }
                
                [[customLoadingTool initCustomManager] disMissFresh];
                [self.tableView reloadData];
            }
        });
    });


    
}
/**获取充值卡账户**/
-(void)getRechargeCard:(void(^)(BOOL success,NetReceiveObjType type, id responseObject))completed{
    [NET_DATA_MANAGER requestRechargCardCache:^(id responseCache) {
        
        completed ? completed(YES,NetReceiveObjType_CACHE,responseCache) : nil;
    } Success:^(id responseObject) {
        /** 一凡TODO**/
        
        completed ? completed(YES,NetReceiveObjType_RESOPONSE,responseObject) : nil;
    } failure:^(NSError *error) {
        completed ? completed(NO,NetReceiveObjType_FAIL,error) : nil;
    }];
}

-(void)getPayAccountProgress:(void (^)(BOOL))progressBlock Parameter:(NSDictionary *)parameter bankTotal:(void (^)(NSArray *))bankArr{
    NSMutableArray *source = [[NSMutableArray alloc]init];
    [self.manager GET:[jesseGuideUIConfig returnPayInterface:GetAllAccount] parameters:parameter progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSData *data = (NSData *)responseObject;
        NSArray *main = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        
        NSLog(@"main:%@",main);
        [main enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *dic = obj;
            TransferOnlieModel *model = [[TransferOnlieModel alloc]init];
            model.markWay = 0;
            TransferMoneyModel *transModel = [[TransferMoneyModel alloc]initWithDictionary:dic error:nil];
            model.sortState = [transModel.sort integerValue];
            model.transModel = transModel;
            [source addObject:model];
        }];
        bankArr(source);
        progressBlock(YES);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        progressBlock(NO);
    }];
}
/**获取所有在线支付**/
-(void)getAllchannelProgress:(void (^)(BOOL))progressBlock Parameter:(NSDictionary *)parameter bankTotal:(void (^)(NSArray *))bankArr{
    NSMutableArray *source = [[NSMutableArray alloc]init];
    [self.manager GET:[jesseGuideUIConfig returnPayInterface:GetAllChannelAccount] parameters:parameter progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSData *data = (NSData *)responseObject;
        NSArray  *main = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"main1:%@",main);
        [main enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *dic = obj;
            TransferOnlieModel *model = [[TransferOnlieModel alloc]init];
            model.markWay = 1;
            onlineModel *online = [[onlineModel alloc]initWithDictionary:dic error:nil];
            model.onlineModel = online;
            model.sortState = [online.sort integerValue];
            [source addObject:model];
        }];
        bankArr(source);
        progressBlock(YES);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        progressBlock(NO);
    }];
}
/**获取限制信息**/
-(void)GetRechargeConfig:(NSDictionary *)parameter And:(TransferOnlieModel *)model and:(void (^)(BOOL iscode,NSString *scanRemark))greenGo{
    [[customLoadingTool initCustomManager] showRefreshActivityAnimationForLoading];
    self.sessionTask = [self.manager GET:[jesseGuideUIConfig returnPayInterface:RechargeConfig] parameters:parameter progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [[customLoadingTool initCustomManager] disMissFresh];
        NSData *data = (NSData *)responseObject;
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        if ([dic.allKeys containsObject:@"rechargeTip"]) {
            if (dic[@"rechargeTip"]) {
                BET_CONFIG.rechargeAlert = dic[@"rechargeTip"];
            }
        }
        
        if ([dic.allKeys containsObject:@"rechargeRemarkColor"]){
            if (dic[@"rechargeRemarkColor"]) {
                BET_CONFIG.remark_color = dic[@"rechargeRemarkColor"];
            }
        }
        if ([[self.payHead currenMoney] integerValue]==0||[self.payHead currenMoney].length==0) {
            [self.alertView updateDesAlertView:@"请输入金额" And:@"温馨提示"];
        }else if ([[self.payHead currenMoney] integerValue]>[dic[@"maxAmount"] integerValue]&&[dic[@"maxAmount"] integerValue]!=0){
            [self.alertView updateDesAlertView:[NSString stringWithFormat:@"亲,您输入的金额已超出最高充值金额%@元,请重新输入!",dic[@"maxAmount"]] And:@"温馨提示"];
        }else if ([[self.payHead currenMoney] doubleValue]<[dic[@"minAmount"] doubleValue]){
            [self.alertView updateDesAlertView:[NSString stringWithFormat:@"亲,您输入的金额已低于最低充值金额%@元",dic[@"minAmount"]] And:@"温馨提示"];
        }else{
            greenGo(([dic[@"validateCodeEnabled"] integerValue]==1)?YES:NO,dic[@"scanRemark"]);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[customLoadingTool initCustomManager] disMissFresh];
        [self.alertView updateDesAlertView:@"网络连接超时" And:@"温馨提示"];
    }];
}
/**逻辑代码处理**/
-(NSMutableArray *)loadRefresh{
    NSMutableArray *refreshArr = [[NSMutableArray alloc]init];
    for (int i = 0; i<15; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"gif-%d",i+1]];
        UIImage *newImage = [self uiimageDraw:image And:0.15];
        [refreshArr addObject:newImage];
    }
    return  refreshArr;
}
/**代理处理**/
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataArr.count+1;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if (section==0) {
        return 0;
    }else{
        if ([self.sectionBoolValue[section-1] boolValue]) {
            if (((NSArray *)self.allPaySourceArr[section-1]).count==0) {
                return 1;
            }
            return ((NSArray *)self.allPaySourceArr[section-1]).count;
        }else{
            return 0;
        }
   }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (((NSArray *)self.allPaySourceArr[indexPath.section-1]).count==0) {
        return 69;
    }
    if ([self JudgeBankfig:self.allPaySourceArr[indexPath.section-1][indexPath.row]]) {
        
        return 150;
    }else{
        
        TransferOnlieModel *transOnline = self.allPaySourceArr[indexPath.section-1][indexPath.row];
        if (transOnline.markWay==2 && transOnline.width) {//充值卡
            return transOnline.height * ((WIDTH-26) / transOnline.width) + 20;
//            return transOnline.height + 10;
        }else{
            return 69;
        }
        
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return 36;
    }else{
        return 69;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    if (((NSArray *)self.allPaySourceArr[indexPath.section-1]).count==0) {
        cell = (jesseMaintenanceTableViewCell *)[tableView dequeueReusableCellWithIdentifier:reisterMaintenanceNewCell forIndexPath:indexPath];
        return cell;
    }
    if ([self JudgeBankfig:self.allPaySourceArr[indexPath.section-1][indexPath.row]]) {
        __weak typeof(self)weakself = self;
        cell = (BankTransferCell *)[tableView dequeueReusableCellWithIdentifier:bankTransferNormalCell forIndexPath:indexPath];
        [((BankTransferCell *)cell) updateCurrenBankTransfer:self.allPaySourceArr[indexPath.section-1][indexPath.row]];
        [((BankTransferCell *)cell) setBlock:^(NSInteger num) {
            TransferOnlieModel *model = weakself.allPaySourceArr[indexPath.section-1][indexPath.row];
            NSString *transferM;
            if (model.markWay==0) {
                transferM = @"1";
            }else{
                transferM = @"2";
            }
            payModel *codeModel = weakself.dataArr[indexPath.section-1];
            [weakself GetRechargeConfig:@{@"mode":transferM,@"payType":codeModel.code} And:model and:^(BOOL iscode,NSString *scanRemark) {
                JesseNewbankTransFerViewController *newTransC = [[JesseNewbankTransFerViewController alloc]init];
                newTransC.isCode = iscode;
                [newTransC loadCurrenTransfer:model.transModel AndrechArgeMoney:[self.payHead currenMoney]];
                [self.navigationController pushViewController:newTransC animated:YES];
            }];
        }];
    }else{
        
        TransferOnlieModel *transOnline = self.allPaySourceArr[indexPath.section-1][indexPath.row];
        if (transOnline.markWay==2) {
            
            static NSString *cellID = @"TopUpCardCell";
            cell = [tableView dequeueReusableCellWithIdentifier:cellID];
            if (nil == cell) {
                cell = [[NSBundle mainBundle] loadNibNamed:@"TopUpCardCell" owner:self options:nil].firstObject;
            }
            [((TopUpCardCell *)cell) setCellLogo:[NSString stringWithFormat:@"%@%@",SerVer_Url,transOnline.rechangeImage]];
            
            
        }else{
            
            cell = (onlineCell *)[tableView dequeueReusableCellWithIdentifier:RegisterOnlineCell forIndexPath:indexPath];
            
            payModel *payM = self.dataArr[indexPath.section-1];
            if (transOnline.markWay==0) {
                [((onlineCell *)cell) updateCurren:[NSString stringWithFormat:@"%@/data/image/pay-type-icon/%@.png",SerVer_Url,payM.code] isHttpLoad:YES AndName:transOnline.transModel.name AndMark:transOnline.transModel.remarks];
            }else {
                [((onlineCell *)cell) updateCurren:[NSString stringWithFormat:@"%@/data/image/pay-type-icon/%@.png",SerVer_Url,payM.code] isHttpLoad:YES AndName:transOnline.onlineModel.name AndMark:transOnline.onlineModel.remarks];
            }
        }
        
        
    }
    return cell;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section==0) {
        return [[sectionTitle alloc] initTitle:CGRectMake(0, 0, WIDTH, 36)];
    }else{
        NSString *identifier = [NSString stringWithFormat:@"normalSectionView%li",section];
        normalSectionView *normalView =  [tableView dequeueReusableHeaderFooterViewWithIdentifier:identifier];
        if (!normalView) {
            normalView = [[normalSectionView alloc] initWithReuseIdentifier:identifier AndCurrenTag:section];
        }
        normalView.tag = section;
        payModel *model = [self.dataArr safeObjectAtIndex:section-1];
        [normalView update:[self.sectionBoolValue[section-1] boolValue]];
        [normalView updateCurrenSourceDesName:model.name AndPrompt:nil];
        @weakify(self);
        [normalView setBlock:^(NSInteger num, BOOL openvalue) {
            @strongify(self);
            BOOL open = ([self.sectionBoolValue safeObjectAtIndex:num - 1] != nil);
            if (model && open) {
                [self.sectionBoolValue replaceObjectAtIndex:num-1 withObject:[NSNumber numberWithBool:![[self.sectionBoolValue safeObjectAtIndex:num - 1] boolValue]]];
                [self getCurrenGetSubAccount:@{@"payType":model.code} AndSection:section-1];
            }
        }];
        return normalView;
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [jesseGuideUIConfig getCurrnViewUiByRect:CGRectMake(0, 0, WIDTH, 0.1) AndBackColor:[UIColor clearColor] AndUseMasonry:NO];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    payModel *Pmodel = self.dataArr[indexPath.section-1];
    if ((((NSArray *)self.allPaySourceArr[indexPath.section-1]).count==0)||(indexPath.section==1&&[Pmodel.bankId integerValue]==1)) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        return;
    }
    if ([self JudgeBankfig:self.allPaySourceArr[indexPath.section-1][indexPath.row]]) {
        return;
    }
    TransferOnlieModel *model = self.allPaySourceArr[indexPath.section-1][indexPath.row];
    
    if ([[tableView cellForRowAtIndexPath:indexPath] isKindOfClass:[TopUpCardCell class]]) {//充值卡点击事件
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        //TODO
//        [NAVI_MANAGER jumpToUrl:model.rechangeLink];
    }else{
        
        
        @weakify(self);
        [self GetRechargeConfig:@{@"mode":(model.markWay==0)?@"1":@"2",@"payType":Pmodel.code} And:model and:^(BOOL iscode,NSString *scanRemark) {
            @strongify(self);
            if (model.markWay==0) {
                NSString *code = [NSString stringWithFormat:@"%@/data/image/%@",SerVer_Url,model.transModel.qrCode];
                code = [code stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                jesseWechatScanCodeViewController *codeC = [[jesseWechatScanCodeViewController alloc]init];
                codeC.scanRemark = scanRemark;
                [codeC loadCurrenBy:code
                          andisCode:iscode
                                And:model.transModel
                       andArgeMoney:[self.payHead currenMoney]
                          LoadAlert:model.transModel.name
                                And:([model.transModel.payType isEqualToString:@"wechat"]||[model.transModel.payType isEqualToString:@"wxhb"])?wechat:([model.transModel.payType isEqualToString:@"alipay"])?alipay:([model.transModel.payType isEqualToString:@"qq"])?qqPay:otherPay];
                [self.navigationController pushViewController:codeC animated:YES];
            }else{
                NSData *data;
                [self.bankNameArr[0] removeAllObjects];
                [self.bankNameArr[1] removeAllObjects];
                if (model.onlineModel.banks){
                    data = [model.onlineModel.banks dataUsingEncoding:NSUTF8StringEncoding];
                    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                    [dic enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
                        [self.bankNameArr[0] addObject:key];
                        [self.bankNameArr[1] addObject:obj];
                    }];
                }
                jesseBankOnlineViewController *online = [[jesseBankOnlineViewController alloc]init];
                payViewType type = ([model.onlineModel.viewType integerValue]==1)?codeType:([model.onlineModel.viewType integerValue]==2)?wapType:pushType;
                [online loadPay:self.bankNameArr viewTyple:type andArgeMoney:[self.payHead currenMoney] andCode:iscode And:model.onlineModel];
                
                [self.navigationController pushViewController:online animated:YES];
            }
        }];
    }
    
    
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(self.noticeView.list.count){
        [self.noticeView startAnimation];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    NSMutableArray *notiStrList = @[].mutableCopy;
    [BET_CONFIG.noticeModel.rech_notice enumerateObjectsUsingBlock:^(NoticeModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *data = [obj.noticeContent mj_JSONObject];
        NSArray *subList = [data objectForKey:@"subList"];
        NSString *defaultStr = [data objectForKey:@"default"];
        __block NSMutableAttributedString *mNotiStr = nil;
        [subList enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull dic, NSUInteger subIdx, BOOL * _Nonnull subStop) {
            if([USER_DATA_MANAGER.userInfoData.hyLevel isEqualToString:[dic objectForKey:@"subKey"]]){
                /**取层级**/
                mNotiStr = [[[dic objectForKey:@"noticeContent"] htmlStr] mutableCopy];
                [mNotiStr addColor:[UIColor whiteColor] substring:[dic objectForKey:@"noticeContent"]];
                [mNotiStr addFont:[UIFont systemFontOfSize:16] substring:[dic objectForKey:@"noticeContent"]];
            }
        }];
        if(!mNotiStr){
            /**取默认**/
            mNotiStr = [[defaultStr htmlStr] mutableCopy];
            [mNotiStr addColor:[UIColor whiteColor] substring:defaultStr];
            [mNotiStr addFont:[UIFont systemFontOfSize:16] substring:defaultStr];
        }
        if(mNotiStr){
            [notiStrList addObject:mNotiStr];
        }
    }];
    if(notiStrList.count){
        self.noticeContstraintHeight.constant = 30;
        self.noticeView.list = notiStrList;
        [self.noticeView startAnimation];
    }else{
        self.noticeContstraintHeight.constant = 0;
    }
    
    self.accountLb.text = USER_DATA_MANAGER.userInfoData.account;
    self.moneyLb.text = [NSString roundingWithFloat:USER_DATA_MANAGER.userInfoData.money];
    self.tableView.tableHeaderView = [self payHead];
    [self.tableView registerClass:[BankTransferCell class] forCellReuseIdentifier:bankTransferNormalCell];
    [self.tableView registerClass:[onlineCell class] forCellReuseIdentifier:RegisterOnlineCell];
    [self.tableView registerClass:[jesseMaintenanceTableViewCell class] forCellReuseIdentifier:reisterMaintenanceNewCell];
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingTarget:self refreshingAction:@selector(mjRefresh)];
    [header setImages:[self loadRefresh] duration:0.2 forState:MJRefreshStateRefreshing];
    [header setImages:[self loadRefresh] forState:MJRefreshStatePulling];
    NSString *title = [[NSBundle mainBundle] infoDictionary][@"CFBundleDisplayName"];
    [header setTitle:title forState:MJRefreshStateRefreshing];
    self.tableView.mj_header = header;
    [header beginRefreshing];
    self.backBtn.hidden = self.navigationController.viewControllers.count == 1;
}
#pragma mark--生命周期
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:NO];
    [self.noticeView stopAnimation];
}
/**详细任务处理**/
-(UIImage *)uiimageDraw:(UIImage *)image And:(CGFloat)sale{
    UIGraphicsBeginImageContext(CGSizeMake(image.size.width*sale, image.size.height*sale));
    [image drawInRect:CGRectMake(0, 0, image.size.width*sale, image.size.height*sale)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
/**检查当前运作id是否bankfig**/
-(BOOL)JudgeBankfig:(TransferOnlieModel *)model{
    if (model.markWay==1) {
        return NO;
    }else{
        TransferMoneyModel *transModel = model.transModel;
        BOOL bankfig = NO;
        if ([transModel.bankFlag integerValue]==1) {
            bankfig = YES;
        }else{
            bankfig = NO;
        }
        return bankfig;
    }
    
}
/**给出高度**/
-(CGFloat)getCurrenheight:(NSArray *)bankArr{
    NSEnumerator *enumer = [bankArr reverseObjectEnumerator];
    [self.newjudeBankFigArr removeAllObjects];
    for (TransferOnlieModel *model in enumer) {
        if (model.markWay == 0) {
            TransferMoneyModel *transModel = model.transModel;
            if ([transModel.bankFlag integerValue]==1) {
                [self.newjudeBankFigArr addObject:transModel];
            }
        }
    }
    
    return   6*self.newjudeBankFigArr.count+119*self.newjudeBankFigArr.count+12;
    
}

- (IBAction)menuClick:(id)sender {
    [self showRightMenuAtIdx:Bet365NaviItemsStatus_MENUE];
}

- (IBAction)backClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



@end

