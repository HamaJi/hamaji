//
//  PersonPageCellItemView.m
//  Bet365
//
//  Created by HHH on 2018/8/5.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "PersonPageCellItemView.h"
@interface PersonPageCellItemView()
@property (strong, nonatomic) IBOutlet UIImageView *iconImageView;
@property (strong, nonatomic) IBOutlet UILabel *titileLabel;

@end

@implementation PersonPageCellItemView

-(void)setBuge:(NSUInteger)buge{
    _buge = buge;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.bugeView.hidden = _buge ? NO : YES;
        if (_buge > 99) {
            self.bugeView.text = @"99+";
        }else{
            self.bugeView.text = [NSString stringWithFormat:@"%li",_buge];
        }
    });
}

-(void)setIconUrl:(NSString *)iconUrl{
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([iconUrl hasPrefix:@"http"]) {
            [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:iconUrl]];
        }else{
            [self.iconImageView setImage:[UIImage imageNamed:iconUrl]];
        }
        
    });
    
}

-(void)setTitile:(NSString *)titile{
    self.backgroundColor = [UIColor skinViewBgColor];
    self.titileLabel.textColor = [UIColor skinTextItemNorColor];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.titileLabel.text = titile;
    });
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
