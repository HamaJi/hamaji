
//
//  PersonPageHeaderView.m
//  Bet365
//
//  Created by HHH on 2018/8/5.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "PersonPageHeaderView.h"
@interface PersonPageHeaderView()
@property (strong, nonatomic) IBOutlet UIButton *currentLevelBtn;
@property (strong, nonatomic) IBOutlet UIButton *levelRuleBtn;
@property (strong, nonatomic) IBOutlet UIButton *submitSignBtn;
@property (strong, nonatomic) IBOutlet UILabel *totalGrowthValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *growthValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *growthDiffrentValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *growthProportionLabel;
@property (strong, nonatomic) IBOutlet UIProgressView *growthProgress;
@property (strong, nonatomic) IBOutlet UIView *signContentView;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;

@property (weak, nonatomic) IBOutlet UIView *bottomContentView;
@property (weak, nonatomic) IBOutlet UIButton *recharBtn;
@property (weak, nonatomic) IBOutlet UIButton *drawBtn;
@property (weak, nonatomic) IBOutlet UIImageView *oldImageView;

@end
@implementation PersonPageHeaderView

-(instancetype)init{
    if (self = [super init]) {
        [self addObserver];
        [self setUI];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self addObserver];
        [self setUI];
    }
    return self;
}

-(void)setUI{
    dispatch_async(dispatch_get_main_queue(), ^{
        @weakify(self);
        self.growthValueLabel.userInteractionEnabled = YES;
        [self.growthValueLabel addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
            @strongify(self);
            [self growthValueDetailed];
        }];
        [self.bgImageView setImage:[UIImage imageNamed:@"usercenter_headerbg"]];
        if ([BET_CONFIG.skin.skinName isEqualToString:[UIColor skinWihteName]]) {
            [self.bgImageView setBackgroundColor:JesseColor(196, 7, 26)];
        }else{
            [self.bgImageView setBackgroundColor:[UIColor skinNaviBgColor]];
        }
        self.bottomContentView.backgroundColor = [UIColor skinViewBgColor];
        [self.recharBtn setTitleColor:[UIColor skinTextHeadColor] forState:UIControlStateNormal];
        [self.drawBtn setTitleColor:[UIColor skinTextHeadColor] forState:UIControlStateNormal];
    });
}
-(void)addObserver{
    
    UIImage *image = [[UIImage imageNamed:@"person_center_bg"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.bgImageView setImage:image];
    if ([BET_CONFIG.skin.skinName isEqualToString:[UIColor skinWihteName]]) {
        [self.bgImageView setTintColor:COLOR_WITH_HEX(0xcd3833)];
    }else{
        [self.bgImageView setTintColor:[UIColor skinViewContentColor]];
    }
    @weakify(self);
    
    [[RACSignal combineLatest:@[[RACObserve(self, currentValue) distinctUntilChanged],
                                [RACObserve(self, totalValueInLevel) distinctUntilChanged],
                                [RACObserve(self, totalValue) distinctUntilChanged]] reduce:^id (NSNumber *current,NSNumber *totalInLevel,NSNumber *totalValue){
                                    if ([totalInLevel integerValue]) {
                                        return @(YES);
                                    }
                                    return @(NO);
                                }] subscribeNext:^(id  _Nullable x) {
                                    @strongify(self);
                                    if ([x boolValue]) {
                                        [self updateGrow];
                                    }
                                }];
    
    [[RACObserve(self, hasSignIn) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        [self.submitSignBtn setSelected:[x boolValue]];
    }];
    
    [[RACObserve(self, isShowGrowthLevel) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (_isShowGrowthLevel) {
                [self.oldImageView removeFromSuperview];
                [self addSubview:self.signContentView];
                [self.signContentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.top.right.equalTo(self);
                    make.bottom.equalTo(self.bottomContentView.mas_top).offset(-10);
                }];
                
                [self.bottomContentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self).offset(10);
                    make.bottom.right.equalTo(self).offset(-10);
                    make.height.mas_equalTo(60);
                }];
                
            }else{
                [self.signContentView removeFromSuperview];
                [self.oldImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.top.right.equalTo(self);
                }];
                [self.bottomContentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self).offset(10);
                    make.right.bottom.equalTo(self).offset(-10);
                    make.centerY.equalTo(self.oldImageView.mas_bottom);
                    make.bottom.equalTo(self).offset(40);
                }];
            }
        });
    }];
}

-(void)updateGrow{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.growthProgress setProgress:self.currentValue / (self.totalValueInLevel / 1.0) animated:YES];
        self.growthValueLabel.text = [NSString stringWithFormat:@"%li",self.currentValue];
        NSInteger diffence = self.totalValueInLevel - self.currentValue;
        if (diffence > 0) {
            self.growthDiffrentValueLabel.text = [NSString stringWithFormat:@"升级还需要%li点成长值",diffence];
        }else{
            self.growthDiffrentValueLabel.text = @"";
        }
        self.growthProportionLabel.text = [NSString stringWithFormat:@"%li/%li",self.currentValue,self.totalValueInLevel];
//        self.totalGrowthValueLabel.text = [NSString stringWithFormat:@"总成长值:%li",self.totalValue];
    });
}

- (void)growthValueDetailed
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(PersonPageHeaderView:didTapHeaderAction:)]) {
        [self.delegate PersonPageHeaderView:self didTapHeaderAction:PersonPageHeaderViewAction_GROWTYPE];
    }
}

- (IBAction)tapAction:(id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(PersonPageHeaderView:didTapHeaderAction:)]) {
        [self.delegate PersonPageHeaderView:self didTapHeaderAction:((UIButton *)sender).tag];
    }
}

- (IBAction)tapSignInAction:(id)sender{
    if (USER_DATA_MANAGER.isLogin ) {
        if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"TEST"]) {
            [self.viewController alertWithTitle:@"提示" message:@"试玩账号不能签到" cancelActionTitle:@"取消" cancelHandler:nil sureActionTitle:@"立即注册" sureHandler:^(UIAlertAction *action) {
                [UIViewController routerJumpToUrl:kRouterRegister];
            }];
//            [SVProgressHUD showErrorWithStatus:@"试玩账号无法签到，请登录平台账号"];
            return;
        }
//        if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"VHY"]) {
//            [self.viewController alertWithTitle:@"提示" message:@"推广账号不能签到" cancelActionTitle:@"取消" cancelHandler:nil sureActionTitle:@"立即注册" sureHandler:^(UIAlertAction *action) {
//                [UIViewController routerJumpToUrl:kRouterRegister];
//            }];
////            [SVProgressHUD showErrorWithStatus:@"推广账号无法签到，请登录平台账号"];
//            return;
//        }
        
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(PersonPageHeaderView:submitSigin:)]) {
        [self.delegate PersonPageHeaderView:self submitSigin:self.hasSignIn];
    }
}

- (IBAction)tapLevelRuleAction:(id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(PersonPageHeaderView:didTapHeaderAction:)]) {
         [self.delegate PersonPageHeaderView:self didTapHeaderAction:PersonPageHeaderViewAction_GROWRULE];
    }
}

-(void)updateGrowth:(NSInteger)value TotalGrowth:(NSInteger)totalValueInLevel UpdateGrowth:(NSInteger)updateValue{
    
}

-(void)setLevel:(NSInteger)level{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!level) {
            [self.currentLevelBtn setTitle:@"普通会员" forState:UIControlStateNormal];
        }else{
            [self.currentLevelBtn setTitle:[NSString stringWithFormat:@"VIP%li",level] forState:UIControlStateNormal];
        }
    });
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
