//
//  LoginCenterView.m
//  Bet365
//
//  Created by super_小鸡 on 2019/8/15.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "LoginCenterView.h"


#import "NetWorkMannager+Account.h"
#import "LoginAccountCenterView.h"
#import "LoginPhoneCenterView.h"
#import "LoginCenterView+Overrides.h"



@interface LoginCenterView()
<VerifyManagerDelegate>







@end

@implementation LoginCenterView

LoginRequestKey *const kLRKeyAccount = @"account";

LoginRequestKey *const kLRKeyLoginPhone = @"phone";

LoginRequestKey *const kLRKeyPassword = @"password";

LoginRequestKey *const kLRKeyPwdWithoutMD5 = @"kLRKeyPwdWithoutMD5";

LoginRequestKey *const kLRKeyValiCode = @"valiCode";

LoginRequestKey *const kLRKeyVcode = @"vCode";

LoginRequestKey *const kLRKeySMScode = @"code";


-(instancetype)init{
    if (self = [super init]) {
        [self addObser];
    }
    return self;
}
-(void)dealloc{
    
}
-(void)extractLoginParamers{
    [self.paramers removeAllObjects];
    @weakify(self);
    void(^loginRespondParamers)(BOOL success) = ^(BOOL success){
        @strongify(self);
        if(self.delegate && [self.delegate respondsToSelector:@selector(loginCenterView:LoginWithParamers:)]){
            [self.delegate loginCenterView:self LoginWithParamers:success ? self.paramers : nil];
        }
    };
    
    if ([self isKindOfClass:[LoginAccountCenterView class]]) {
        
        LoginAccountCenterView *loginView = (LoginAccountCenterView *)self;
        self.paramers = nil;
        if (!loginView.identity.length) {
            [SVProgressHUD showErrorWithStatus:@"帐号必填哦"];
            loginRespondParamers(NO);
            return;
        }
        if (!loginView.password.length) {
            [SVProgressHUD showErrorWithStatus:@"密码必填哦"];
            loginRespondParamers(NO);
            return;
        }
        [self.paramers safeSetObject:loginView.identity forKey:kLRKeyAccount];

        [self.paramers safeSetObject:loginView.password forKey:kLRKeyPwdWithoutMD5];
        //MD5
        if (BET_CONFIG.config.user_password_type && [BET_CONFIG.config.user_password_type isEqualToString:@"0"]) {
            [self.paramers safeSetObject:[loginView.password MD5] forKey:kLRKeyPassword];
        }else{
            [self.paramers safeSetObject:loginView.password forKey:kLRKeyPassword];
        }
        
        [self verifyAuther:^(BOOL success) {
            loginRespondParamers(success);
        }];
    }else if ([self isKindOfClass:[LoginPhoneCenterView class]]){
        LoginPhoneCenterView *loginView = (LoginPhoneCenterView *)self;
        if (!loginView.identity.length) {
            [SVProgressHUD showErrorWithStatus:@"手机号码必填哦"];
            loginRespondParamers(NO);
            return;
        }
        if (!loginView.smsCode.length) {
            [SVProgressHUD showErrorWithStatus:@"短信验证码必填哦"];
            loginRespondParamers(NO);
            return;
        }
        [self.paramers safeSetObject:loginView.identity forKey:kLRKeyLoginPhone];
        [self.paramers safeSetObject:loginView.smsCode forKey:kLRKeySMScode];
        loginRespondParamers(YES);
    }
}


#pragma mark - Overrides
-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self addObser];
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addObser];
    }
    return self;
}


-(void)addObser{
    @weakify(self);
    
    [RACObserve(self, verifyType) subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([x integerValue] == VerifyAuthorType_TENCENT) {
                [self.verifyCodeView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(0);
                }];
            }else if ([x integerValue] == VerifyAuthorType_IMAGE) {
                [self.verifyCodeView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(45);
                }];
                [self updateVerifyImage];
            }else if ([x integerValue] == VerifyAuthorType_GT3) {
                [self.verifyCodeView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(0);
                }];
            }else{
                [self.verifyCodeView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(0);
                }];
            }
        });
    }];
    
}

-(void)updateVerifyImage{
    if(self.verifyType != VerifyAuthorType_IMAGE ){
        return;
    }
    if (!self.identity.length) {
        return;
    }
    [self.verifyCodeView.dateBtn setBackgroundImage:nil forState:UIControlStateNormal];
    self.verifyCodeView.textfield.text = @"";
    [NET_DATA_MANAGER requestGetVCodeByRandom:YES Success:^(id responseObject) {
        [self.verifyCodeView.dateBtn setBackgroundImage:responseObject forState:UIControlStateNormal];
    } failure:^(NSError *error) {
        
    }];
}

- (void)getCodeData{
    if (!self.identity.length) {
        return;
    }
    void(^handle)(id response) = ^(id response){
        self.verifyMannager.requestJsonString = response;
    };
    
    NSString *account;
    NSString *phone;
    if ([self isKindOfClass:[LoginAccountCenterView class]]) {
        account = self.identity;
    }else if ([self isKindOfClass:[LoginPhoneCenterView class]]){
        phone = self.identity;
    }
    [NET_DATA_MANAGER requestVerifyAuthorTypeAccount:account Phone:phone Success:^(id responseObject) {
        handle(responseObject);
    } failure:^(NSError *error) {
        self.verifyType = VerifyAuthorType_UNVERIFY;
    }];
}

#pragma mark - VerifyManagerDelegate
-(void)verifyManager:(VerifyManager *)manager updateVerifyAuthorType:(VerifyAuthorType)type{
    self.verifyType = type;
}

-(void)verifyManager:(VerifyManager *)manager verificationSuccessParamers:(NSDictionary *)paramers authorType:(VerifyAuthorType)type{
    self.verifyBlock ? self.verifyBlock(paramers) : nil;
}

-(void)verifyManager:(VerifyManager *)manager verificationFaildAuthorType:(VerifyAuthorType)type{
    
}

#pragma mark - GET/SET


-(NSMutableDictionary *)paramers{
    if (!_paramers) {
        _paramers = @{}.mutableCopy;
    }
    return _paramers;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)setIdentity:(NSString *)identity{
    if ([_identity isEqualToString:identity]) {
        return;
    }
    _identity = identity;
    [self getCodeData];
}

-(VerifyManager *)verifyMannager{
    if (!_verifyMannager) {
        _verifyMannager = [[VerifyManager alloc] init];
        _verifyMannager.delegate = self;
    }
    return _verifyMannager;
}
@end
