//
//  UserLoginLimitAlertInputCell.h
//  Bet365
//
//  Created by wind on 2020/2/2.
//  Copyright © 2020 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LimitModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserLoginLimitAlertInputCell : UITableViewCell

@property (nonatomic,strong) NSMutableDictionary *mutDic;

@property (weak, nonatomic) IBOutlet UILabel *titleLB;

@property (weak, nonatomic) IBOutlet UITextField *inputTF;

@end

NS_ASSUME_NONNULL_END
