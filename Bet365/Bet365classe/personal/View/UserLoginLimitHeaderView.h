//
//  UserLoginLimitHeaderView.h
//  Bet365
//
//  Created by wind on 2020/2/2.
//  Copyright © 2020 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserLoginLimitHeaderView : UITableViewHeaderFooterView

@property (weak, nonatomic) IBOutlet UIButton *cancelBT;

@end

NS_ASSUME_NONNULL_END
