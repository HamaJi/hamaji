//
//  LukeRegisterLbTFView.m
//  Bet365
//
//  Created by luke on 17/6/11.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeRegisterLbTFView.h"
#import "Bet365traceTextView.h"
#import "TimerManager.h"
#import "AjiTextView.h"
@interface LukeRegisterLbTFView()
@property (nonatomic,strong) UIButton *codeSendBtn;

@property (nonatomic,assign) NSUInteger sendingSpacing;

@property (nonatomic,strong) NSString *timeCountDownKey;

@property (nonatomic,assign) NSUInteger maxCountDown;

@property (nonatomic,strong) NSString *resendTitile;


@end

@implementation LukeRegisterLbTFView

- (instancetype)init
{
    if (self = [super init]) {
        self.backgroundColor = [UIColor skinViewBgColor];
    }
    return self;
}

+ (instancetype)registerLbTFViewWithTitle:(NSString *)title textEntry:(BOOL)textEntry placeholder:(NSString *)placeholder keyboardType:(UIKeyboardType)type
{
    
    LukeRegisterLbTFView *resigterView = [[LukeRegisterLbTFView alloc] init];
    UILabel *nameLabel = [resigterView addLabelWithTitle:title font:17 color:[UIColor skinTextItemNorColor]];
    CGFloat nameLbWidth = [title stringWidthWithFont:[UIFont systemFontOfSize:17]];
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(resigterView).offset(10);
        make.centerY.equalTo(resigterView);
        make.width.mas_equalTo(nameLbWidth + 10);
    }];
    
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectZero];
    textField.keyboardType = type;
    textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    if (placeholder) {
        NSAttributedString *string = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName : [UIColor skinTextItemNorSubColor],NSFontAttributeName : [UIFont systemFontOfSize:15]}];
        textField.attributedPlaceholder = string;
        //单独对placeholder做适应处理
//        UILabel *ad_frame = [textField valueForKey:@"_placeholderLabel"];
//        ad_frame.adjustsFontSizeToFitWidth = YES;
    }
    
    textField.secureTextEntry = textEntry;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.textAlignment = NSTextAlignmentRight;
    [resigterView addSubview:textField];
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(resigterView).offset(-10);
        make.centerY.equalTo(resigterView);
        make.leading.equalTo(nameLabel.mas_trailing);
    }];
    textField.textColor = [UIColor skinTextItemNorColor];
    resigterView.textfield = textField;
    [resigterView addLineView];
    
    return resigterView;
}

#pragma mark - 创建label
- (UILabel *)addLabelWithTitle:(NSString *)title font:(CGFloat )font color:(UIColor *)color
{
    UILabel *nameLabel = [[UILabel alloc] init];
    nameLabel.font = [UIFont systemFontOfSize:font];
    nameLabel.textColor = color;
    nameLabel.text = title;
    [self addSubview:nameLabel];
    return nameLabel;
}

- (void)addLineView
{
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = [UIColor skinViewBgColor];
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self).offset(10);
        make.trailing.bottom.equalTo(self);
        make.height.mas_equalTo(1);
    }];
}

+ (instancetype)registerLbBtnViewWithTitle:(NSString *)title btnImage:(NSString *)image btnTitle:(NSString *)btnTitle
{
    LukeRegisterLbTFView *resigterView = [[LukeRegisterLbTFView alloc] init];
    UILabel *nameLabel = [resigterView addLabelWithTitle:title font:17 color:[UIColor skinTextItemNorColor]];
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(resigterView).offset(10);
        make.centerY.equalTo(resigterView);
    }];
    
    LukeLeftBtn *btn = [LukeLeftBtn buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    [btn setTitle:btnTitle forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor skinTextItemNorColor] forState:UIControlStateNormal];
    [btn addTarget:resigterView action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
    [resigterView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(resigterView);
        make.trailing.equalTo(resigterView).offset(-20);
        make.leading.equalTo(nameLabel.mas_trailing).offset(10);
    }];
    resigterView.dateBtn = btn;
    
    [resigterView addLineView];
    
    return resigterView;
}

- (void)btnClick
{
    if (self.callBlock) {
        self.callBlock();
    }
}

+ (instancetype)registerLbSwitchViewWithTitle:(NSString *)title Switch:(BOOL)iSswitch font:(CGFloat)font
{
    LukeRegisterLbTFView *resigterView = [[LukeRegisterLbTFView alloc] init];
    UILabel *nameLabel = [resigterView addLabelWithTitle:title font:font color:[UIColor skinTextItemNorColor]];
    nameLabel.numberOfLines = 2;
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(resigterView).offset(10);
        make.width.mas_equalTo(WIDTH * 0.5);
        make.centerY.equalTo(resigterView);
    }];
    
    UISwitch *sw = [[UISwitch alloc] init];
    sw.on = iSswitch;
    [sw addTarget:resigterView action:@selector(switchClick:) forControlEvents:UIControlEventTouchUpInside];
    [resigterView addSubview:sw];
    [sw mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(resigterView).offset(-10);
        make.centerY.equalTo(resigterView);
    }];
    resigterView.sw = sw;
    [resigterView addLineView];
    
    return resigterView;
}

- (void)switchClick:(UISwitch *)sender
{
    if (self.switchBlock) {
        self.switchBlock(sender.on);
    }
}

+ (instancetype)registerLbBtnTextFieldViewWithTitle:(NSString *)title VerificationBtnImage:(NSString *)image target:(id)target action:(SEL)action
{
    LukeRegisterLbTFView *resigterView = [[LukeRegisterLbTFView alloc] init];
    UILabel *nameLabel = [resigterView addLabelWithTitle:title font:17 color:[UIColor skinTextItemNorColor]];
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(resigterView).offset(10);
        make.centerY.equalTo(resigterView);
    }];
    
    LukeLeftBtn *btn = [LukeLeftBtn buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [resigterView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.top.bottom.equalTo(resigterView);
        make.width.mas_equalTo(120);
    }];
    resigterView.dateBtn = btn;
    
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectZero];
    textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.textAlignment = NSTextAlignmentRight;
    NSAttributedString *string = [[NSAttributedString alloc] initWithString:@"请输入验证码" attributes:@{NSForegroundColorAttributeName : [UIColor skinTextItemNorSubColor],NSFontAttributeName : [UIFont systemFontOfSize:15]}];
    textField.attributedPlaceholder = string;
    [resigterView addSubview:textField];
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(btn.mas_leading);
        make.centerY.equalTo(resigterView);
        make.leading.equalTo(nameLabel.mas_trailing);
    }];
    textField.textColor = [UIColor skinTextItemNorColor];
    resigterView.textfield = textField;
    
    [resigterView addLineView];
    return resigterView;
}

+ (instancetype)registerCountdownBtnWithTitle:(NSString *)title
                                              ResendTitile:(NSString *)resendTitile
                                   TimeOutKey:(NSString *)timeOutKey
                                                  MaxCount:(NSUInteger) maxCount
                                   TapUpBlock:(BOOL(^)())tapUpBlock
                            vCodeSendingBlock:(void(^)(NSUInteger sendingSpacing))vCodeSendingBlock
                                        Placeholder:(NSString *)placeholder

{
    LukeRegisterLbTFView *resigterView = [[LukeRegisterLbTFView alloc] init];
    UILabel *nameLabel = [resigterView addLabelWithTitle:title font:17 color:[UIColor skinTextItemNorColor]];
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(resigterView).offset(10);
        make.centerY.equalTo(resigterView);
    }];
    
    UIButton * codeSendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [codeSendBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    codeSendBtn.backgroundColor = [UIColor skinViewBgColor];
    [codeSendBtn setTitleColor:[UIColor skinViewKitSelColor] forState:UIControlStateNormal];
    //        [_codeSendBtn setTitleColor:[UIColor skinViewKitDisColor] forState:UIControlStateDisabled];
    codeSendBtn.clipsToBounds = YES;
    codeSendBtn.layer.borderColor = [[UIColor skinViewKitSelColor] CGColor];
    codeSendBtn.layer.borderWidth = 1;
    codeSendBtn.layer.cornerRadius = 10;
    [codeSendBtn.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [codeSendBtn addTarget:resigterView action:@selector(vCodeSendingAciton:) forControlEvents:UIControlEventTouchUpInside];
    [resigterView addSubview:codeSendBtn];
    [codeSendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(resigterView);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(100);
        make.right.equalTo(resigterView).offset(-10);
    }];
    resigterView.codeSendBtn = codeSendBtn;
    
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectZero];
    textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.textAlignment = NSTextAlignmentRight;
    NSAttributedString *string = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName : [UIColor skinTextItemNorSubColor],NSFontAttributeName : [UIFont systemFontOfSize:15]}];
    textField.attributedPlaceholder = string;
    [resigterView addSubview:textField];
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(codeSendBtn.mas_leading);
        make.centerY.equalTo(resigterView);
        make.leading.equalTo(nameLabel.mas_trailing);
    }];
    textField.textColor = [UIColor skinTextItemNorColor];
    resigterView.textfield = textField;
    
    [resigterView addLineView];
    
    resigterView.vCodeDownSendingBlock = vCodeSendingBlock;
    resigterView.vCodeDownTapUpBlock = tapUpBlock;
    resigterView.timeCountDownKey = timeOutKey.length ? timeOutKey : @"SMSCODE";
    [TIMER_MANAGER addDelegate:resigterView];
    resigterView.maxCountDown = maxCount;
    resigterView.resendTitile = resendTitile;
    
    return resigterView;
}

+ (instancetype)registerCreateBtnViewWithTitle:(NSString *)title BtnImage:(NSString *)image target:(id)target action:(SEL)action
{
    LukeRegisterLbTFView *resigterView = [[LukeRegisterLbTFView alloc] init];
    UILabel *nameLabel = [resigterView addLabelWithTitle:title font:17 color:[UIColor skinTextItemNorColor]];
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(resigterView).offset(10);
        make.centerY.equalTo(resigterView);
    }];
    
    LukeLeftBtn *btn = [LukeLeftBtn buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor skinTextItemNorColor] forState:UIControlStateNormal];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [resigterView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(resigterView);
        make.trailing.equalTo(resigterView).offset(-20);
        make.leading.equalTo(nameLabel.mas_trailing).offset(10);
        make.height.mas_equalTo(30);
    }];
    resigterView.MoneyBtn = btn;
    
    [resigterView addLineView];
    
    return resigterView;
}

+ (instancetype)registerCreateTextFieldViewWithTitle:(NSString *)title textEntry:(BOOL)textEntry placeholder:(NSString *)placeholder keyboardType:(UIKeyboardType)type
{
    LukeRegisterLbTFView *resigterView = [[LukeRegisterLbTFView alloc] init];
    UILabel *nameLabel = [resigterView addLabelWithTitle:title font:17 color:[UIColor skinTextItemNorColor]];
    CGFloat nameLbWidth = [title stringWidthWithFont:[UIFont systemFontOfSize:17]];
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(resigterView).offset(10);
        make.centerY.equalTo(resigterView);
        make.width.mas_equalTo(nameLbWidth + 10);
    }];
    
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectZero];
    textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.keyboardType = type;
    if (placeholder) {
        NSAttributedString *string = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName : [UIColor skinTextItemNorSubColor],NSFontAttributeName : [UIFont systemFontOfSize:17]}];
        textField.attributedPlaceholder = string;
    }
    
    textField.secureTextEntry = textEntry;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [resigterView addSubview:textField];
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(resigterView).offset(-10);
        make.centerY.equalTo(resigterView);
        make.leading.equalTo(nameLabel.mas_trailing).offset(10);
    }];
    textField.textColor = [UIColor skinTextItemNorColor];
    resigterView.textfield = textField;
    [resigterView addLineView];
    
    return resigterView;
}

+ (instancetype)registerCreateTextViewWithTitle:(NSString *)title placeholder:(NSString *)placeholder MaxLength:(NSUInteger)maxLength ExceedLengthHandle:(void(^)())exceedLengthHandle{
    LukeRegisterLbTFView *resigterView = [[LukeRegisterLbTFView alloc] init];
    AjiTextView *text = [[AjiTextView alloc] initWithFrame:CGRectZero];
//    text.backgroundColor = [UIColor clearColor];
    text.text = title;
    text.placeholder = placeholder;
    text.font = [UIFont systemFontOfSize:16];
    text.placeholderFont = [UIFont systemFontOfSize:18];
    text.textColor = [UIColor skinTextItemNorColor];
    text.placeholderColor = [UIColor skinTextItemSelColor];
    text.maxLength = maxLength;
    
    [resigterView addSubview:text];
    [text mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(resigterView).offset(5);
        make.right.equalTo(resigterView).offset(-5);
    }];
    if (maxLength) {
        UILabel *mask = [UILabel setAllocLabelWithText:[NSString stringWithFormat:@"0/%li",maxLength] FontOfSize:14 rgbColor:0x000000];
        mask.textColor = [UIColor skinTextItemNorSubColor];
        [resigterView addSubview:mask];
        [mask mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(text.mas_bottom).offset(5);
            make.right.bottom.equalTo(resigterView).offset(-5);
        }];
        
        [mask setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisVertical];
        [mask setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisVertical];
        [text addTextLengthDidMaxHandler:^(AjiTextView *textView) {
            exceedLengthHandle ? exceedLengthHandle() : nil;
        }];
        
        [text addTextDidChangeHandler:^(AjiTextView *textView) {
            mask.text = [NSString stringWithFormat:@"%li/%li",textView.text.length,maxLength];
        }];
        
    }else{
        [text mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(resigterView).offset(-5);
        }];
    }
    resigterView.textfield = text;
    return resigterView;
}

+ (instancetype)CreateBtnViewWithTitle:(NSString *)title NormalImage:(NSString *)normalImage selectImage:(NSString *)selectImage target:(id)target action:(SEL)action
{
    LukeRegisterLbTFView *resigterView = [[LukeRegisterLbTFView alloc] init];
    UIImage *normal = [UIImage imageNamed:normalImage];
    normal = [normal imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImage *select = [UIImage imageNamed:selectImage];
    select = [select imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:normal forState:UIControlStateNormal];
    [btn setImage:select forState:UIControlStateSelected];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [btn.imageView setTintColor:[UIColor redColor]];
    [resigterView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(resigterView);
        make.leading.equalTo(resigterView).offset(10);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    resigterView.autoBtn = btn;
    
    UILabel *nameLabel = [resigterView addLabelWithTitle:title font:17 color:[UIColor skinTextItemNorColor]];
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(btn.mas_trailing);
        make.centerY.equalTo(resigterView);
    }];
    
    [resigterView addLineView];
    
    return resigterView;
}

//#pragma mark - UITextFieldDelegate
//-(void)textFieldDidEndEditing:(UITextField *)textField{
//    if (self.delegate && [self.delegate respondsToSelector:@selector(lukeRegisterLbTFViewdDidEndEditing:)]) {
//        [self.delegate lukeRegisterLbTFViewdDidEndEditing:textField];
//    }
//}
//- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason{
//    if (self.delegate && [self.delegate respondsToSelector:@selector(lukeRegisterLbTFViewdDidEndEditing:)]) {
//        [self.delegate lukeRegisterLbTFViewdDidEndEditing:textField];
//    }
//}
//#pragma mark - GET/SET
//-(void)setTextfield:(UITextField *)textfield{
//
//    _textfield = textfield;
//    dispatch_async(dispatch_get_main_queue(), ^{
//        _textfield.delegate = self;
//    });
//}

-(void)vCodeSendingAciton:(UIButton *)sender{
    if (self.vCodeDownTapUpBlock) {
        if (self.vCodeDownTapUpBlock()) {
            [self startTimingVcode];
        }
    }else{
        [self startTimingVcode];
    }
    
}

-(void)startTimingVcode{
    if (self.codeSendBtn && self.timeCountDownKey.length) {
        [TIMER_MANAGER addTimerWithMaxCount:self.maxCountDown andKey:self.timeCountDownKey andReduceScope:1];
    }
}
-(void)dealloc{
    if (self.codeSendBtn && self.timeCountDownKey.length) {
        [TIMER_MANAGER removeDelegate:self];
    }
}
-(void)timerWithKey:(NSString *)key andMaxCount:(NSInteger)maxCount andRestCount:(NSInteger)count{
    if([key isEqualToString:self.timeCountDownKey]){
        self.sendingSpacing = maxCount - count;
    }
}

-(void)setSendingSpacing:(NSUInteger)sendingSpacing{
    if (self.vCodeDownSendingBlock) {
        self.vCodeDownSendingBlock(sendingSpacing);
    }
    _sendingSpacing = sendingSpacing;
    
    [self.codeSendBtn setEnabled:(_sendingSpacing == 0)];
    if (_sendingSpacing > 0) {
        [self.codeSendBtn setTitle:[NSString stringWithFormat:@"%lis后重新获取",_sendingSpacing] forState:UIControlStateNormal];
    }else{
        [self.codeSendBtn setTitle:self.resendTitile forState:UIControlStateNormal];
    }
}

@end
