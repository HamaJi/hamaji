//
//  LukePersonCollectionViewCell.h
//  Bet365
//
//  Created by luke on 17/6/14.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LukePersonCollectionViewCell : UICollectionViewCell

- (void)craeteWithTitle:(NSString *)title Image:(NSString *)imageName index:(NSInteger)index;

@end
