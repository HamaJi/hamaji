//
//  LukeMaskView.m
//  Bet365
//
//  Created by luke on 17/6/13.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeMaskView.h"

@implementation LukeMaskView

+ (void)lukeMaskViewWithTitle:(NSString *)title
{
    LukeMaskView *view = [[self alloc] init];
    view.backgroundColor = [UIColor colorWithWhite:0 alpha:0.7];
    view.layer.cornerRadius = 5;
    view.layer.masksToBounds = YES;
    [view addLabelWith:title];
    view.center = [UIApplication sharedApplication].keyWindow.center;
    [[UIApplication sharedApplication].keyWindow addSubview:view];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [view removeFromSuperview];
    });
}

-(UILabel *)addLabelWith:(NSString *)title
{
    UILabel *textL = [[UILabel alloc]init];
    textL.font = [UIFont systemFontOfSize:14];
    textL.numberOfLines = 0;
    textL.text = title;
    textL.textAlignment = NSTextAlignmentCenter;
    textL.textColor = [UIColor whiteColor];
    [self addSubview:textL];
    return textL;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    UILabel *label = self.subviews[0];
    CGSize titleSize = [label.text boundingRectWithSize:CGSizeMake(WIDTH - 100, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size;
    label.frame = CGRectMake(10, 10, titleSize.width, titleSize.height);
    self.bounds = CGRectMake(0, 0, titleSize.width + 20, titleSize.height + 20);
}

@end
