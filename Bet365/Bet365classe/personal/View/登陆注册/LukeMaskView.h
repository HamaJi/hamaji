//
//  LukeMaskView.h
//  Bet365
//
//  Created by luke on 17/6/13.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LukeMaskView : UIView

+ (void)lukeMaskViewWithTitle:(NSString *)title;

@end
