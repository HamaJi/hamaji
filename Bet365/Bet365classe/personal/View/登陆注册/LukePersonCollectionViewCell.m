//
//  LukePersonCollectionViewCell.m
//  Bet365
//
//  Created by luke on 17/6/14.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukePersonCollectionViewCell.h"
#import "LukeUserAdapter.h"

@implementation LukePersonCollectionViewCell

- (void)craeteWithTitle:(NSString *)title Image:(NSString *)imageName index:(NSInteger)index
{
    [self.contentView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
    if (index < 2) {
        self.contentView.backgroundColor = JesseColor(238, 71, 58);
        UILabel *label = [self addLabelWithTitle:title];
        label.textColor = [UIColor whiteColor];
        label.font = [UIFont systemFontOfSize:19];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.top.equalTo(self.contentView).offset(10);
        }];
        
        UILabel *subLabel = [self addLabelWithTitle:nil];
        subLabel.textColor = [UIColor whiteColor];
        subLabel.font = [UIFont systemFontOfSize:17];
        if (index == 0) {
            subLabel.text = @"帐户余额";
        }else{
            subLabel.text = @"最新消息";
        }
        [subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.top.equalTo(label.mas_bottom).offset(5);
        }];
    }else{
        self.contentView.backgroundColor = [UIColor whiteColor];
        UIImageView *iconImage = nil;
        if (imageName.length > 0) {
            iconImage = [self addImageViewWithImageName:imageName];
            [iconImage mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self.contentView);
                make.top.equalTo(self.contentView).offset(5);
                make.size.mas_equalTo(CGSizeMake(50, 50));
            }];
        }
        
        UILabel *label = [self addLabelWithTitle:title];
        label.font = [UIFont systemFontOfSize:15];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            if (iconImage) {
                make.top.equalTo(iconImage.mas_bottom);
            }else{
                make.centerY.equalTo(self.contentView);
            }
        }];
    }
}

- (UILabel *)addLabelWithTitle:(NSString *)title
{
    UILabel *label = [[UILabel alloc] init];
    label.text = title;
    label.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:label];
    return label;
}

- (UIImageView *)addImageViewWithImageName:(NSString *)imageName
{
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:imageView];
    return imageView;
}

@end
