//
//  LukeRegisterLbTFView.h
//  Bet365
//
//  Created by luke on 17/6/11.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LukeLeftBtn.h"

@protocol LukeRegisterLbTFViewDelegate <NSObject>

-(void)lukeRegisterLbTFViewdDidEndEditing:(UITextField *)textField;

@end

@interface LukeRegisterLbTFView : UIView

+ (instancetype)registerLbTFViewWithTitle:(NSString *)title textEntry:(BOOL)textEntry placeholder:(NSString *)placeholder keyboardType:(UIKeyboardType)type;
/** 输入的textfield */
@property (nonatomic,strong) UITextField *textfield;

+ (instancetype)registerLbBtnViewWithTitle:(NSString *)title btnImage:(NSString *)image btnTitle:(NSString *)title;

+ (instancetype)registerCreateTextViewWithTitle:(NSString *)title placeholder:(NSString *)placeholder MaxLength:(NSUInteger)maxLength ExceedLengthHandle:(void(^)())exceedLengthHandle;

+ (instancetype)registerCountdownBtnWithTitle:(NSString *)title
                                 ResendTitile:(NSString *)resendTitile
                                   TimeOutKey:(NSString *)timeOutKey
                                     MaxCount:(NSUInteger) maxCount
                                   TapUpBlock:(BOOL(^)())tapUpBlock
                            vCodeSendingBlock:(void(^)(NSUInteger sendingSpacing))vCodeSendingBlock
                                  Placeholder:(NSString *)placeholder;
@property (nonatomic,copy) BOOL(^vCodeDownTapUpBlock)();

@property (nonatomic,copy) void(^vCodeDownSendingBlock)(NSUInteger sendingSpacing);
/**  */
@property (nonatomic,copy) void(^callBlock)();

/** 按钮 */
@property (nonatomic,weak) LukeLeftBtn *dateBtn;


+ (instancetype)registerLbSwitchViewWithTitle:(NSString *)title Switch:(BOOL)iSswitch font:(CGFloat)font;

/** 开关 */
@property (nonatomic,weak) UISwitch *sw;

/** 开关回调事件 */
@property (nonatomic,copy) void(^switchBlock)(BOOL);

+ (instancetype)registerLbBtnTextFieldViewWithTitle:(NSString *)title VerificationBtnImage:(NSString *)image target:(id)target action:(SEL)action;

/** 创建按钮挨着标题 */
+ (instancetype)registerCreateBtnViewWithTitle:(NSString *)title BtnImage:(NSString *)image target:(id)target action:(SEL)action;

/**  */
@property (nonatomic,weak) LukeLeftBtn *MoneyBtn;

/** 创建textfield挨着标题 */
+ (instancetype)registerCreateTextFieldViewWithTitle:(NSString *)title textEntry:(BOOL)textEntry placeholder:(NSString *)placeholder keyboardType:(UIKeyboardType)type;

+ (instancetype)CreateBtnViewWithTitle:(NSString *)title NormalImage:(NSString *)NormalImage selectImage:(NSString *)selectImage target:(id)target action:(SEL)action;



/**  */
@property (nonatomic,weak) UIButton *autoBtn;

@property (nonatomic,weak)id<LukeRegisterLbTFViewDelegate> delegate;


-(void)startTimingVcode;

@end
