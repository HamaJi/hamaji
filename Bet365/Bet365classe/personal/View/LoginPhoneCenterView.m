//
//  LoginPhoneCenterView.m
//  Bet365
//
//  Created by super_小鸡 on 2019/8/15.
//  Copyright © 2019 jesse. All rights reserved.
//
#import "LoginPhoneCenterView.h"
#import "LoginCenterView+Overrides.h"
#import <TCWebCodesSDK/TCWebCodesBridge.h>
#import "NetWorkMannager+Account.h"

@interface LoginPhoneCenterView()
<UITextFieldDelegate>

@property (nonatomic,strong) LukeRegisterLbTFView *phoneView;

@property (nonatomic,strong) LukeRegisterLbTFView *smsCodeView;

@property (nonatomic,strong) LukeRegisterLbTFView *VerifyView;

@property (nonatomic,strong) NSMutableDictionary *vCodeRequestParamers;

@end

@implementation LoginPhoneCenterView


-(void)addObser{
    [super addObser];
    self.phoneView.textfield.delegate = self;
    self.smsCodeView.textfield.delegate = self;
    self.VerifyView.textfield.delegate = self;
}

-(LukeRegisterLbTFView *)subVerifyView{
    return self.VerifyView;
}

-(NSString *)verifyImageValiCodeKey{
    return @"";
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    LoginRequestKey *key = nil;
    if ([textField isEqual:self.phoneView.textfield]) {
        key = kLRKeyLoginPhone;
        self.identity = textField.text;
    }else if ([textField isEqual:self.VerifyView.textfield]){
        key = kLRKeyVcode;
        self.vCode = self.verifyCodeView.textfield.text;
    }else if ([textField isEqual:self.smsCodeView.textfield]){
        key = kLRKeySMScode;
        _smsCode = self.smsCodeView.textfield.text;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(loginCenterView:TextFiledSubmitText:Key:)]) {
        [self.delegate loginCenterView:self TextFiledSubmitText:textField.text Key:key];
    }
}


-(void)requestSMScode{
    if (!self.identity.length) {
        [SVProgressHUD showErrorWithStatus:@"请填写手机号码"];
        return;
    }
    [NET_DATA_MANAGER requestLoginSMScodeByPhone:self.identity
                                   Paramers:self.vCodeRequestParamers
                                    Success:^(id responseObject) {
                                        [SVProgressHUD showSuccessWithStatus:@"验证码已发送至你手机"];
                                        [self.smsCodeView startTimingVcode];
    }
                                    failure:^(NSError *error) {
                                        [self updateVerifyImage];
                                        if (error.LIMITED_ONLY_PHONE_LOGIN_ERROR) {
                                            
                                        }
    }];
    
}

#pragma mark - GET/SET
-(LukeRegisterLbTFView *)phoneView{
    if (!_phoneView) {
        _phoneView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"电话号码" textEntry:NO placeholder:@"请输入电话号码" keyboardType:UIKeyboardTypePhonePad];
        _phoneView.layer.cornerRadius = 5;
        _phoneView.layer.masksToBounds = YES;
        
        _phoneView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
        _phoneView.layer.borderWidth = 1;
        _phoneView.textfield.text = AUTHDATA_MANAGER.phone;
        self.identity = _phoneView.textfield.text;
        [self addSubview:_phoneView];
        [_phoneView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset(20);
            make.left.equalTo(self).offset(10);
            make.right.equalTo(self).offset(-10);
            make.height.mas_equalTo(45);
        }];
        
    }
    return _phoneView;
}
-(LukeRegisterLbTFView *)smsCodeView{
    if (!_smsCodeView) {
        @weakify(self);
        _smsCodeView = [LukeRegisterLbTFView registerCountdownBtnWithTitle:@"验证码" ResendTitile:@"重新发送验证码" TimeOutKey:@"" MaxCount:60 TapUpBlock:^BOOL{
            @strongify(self);
            [self endEditing:YES];
            [self verifyAuther:^(BOOL success) {
                if (success) {
                    [self requestSMScode];
                }
            }];
            return NO;
        } vCodeSendingBlock:^(NSUInteger sendingSpacing) {
            
        } Placeholder:@"请输入验证码"];
        
        _smsCodeView.layer.cornerRadius = 5;
        _smsCodeView.layer.masksToBounds = YES;
        _smsCodeView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
        _smsCodeView.layer.borderWidth = 1;
        
        [self addSubview:_smsCodeView];
        [_smsCodeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.VerifyView.mas_bottom).offset(5);
            make.left.equalTo(self).offset(10);
            make.right.equalTo(self).offset(-10);
            make.height.mas_equalTo(45);
            make.bottom.equalTo(self).offset(-10);
        }];
    }
    return _smsCodeView;
}


-(LukeRegisterLbTFView *)VerifyView{
    if (!_VerifyView) {
        _VerifyView = [LukeRegisterLbTFView registerLbBtnTextFieldViewWithTitle:@"验证码" VerificationBtnImage:@"vaid" target:self action:@selector(updateVerifyImage)];
        _VerifyView.layer.cornerRadius = 5;
        _VerifyView.layer.masksToBounds = YES;
        _VerifyView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
        _VerifyView.layer.borderWidth = 1;
        _VerifyView.clipsToBounds = YES;
        [self addSubview:_VerifyView];
        [_VerifyView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.phoneView.mas_bottom).offset(5);
            make.leading.equalTo(self).offset(10);
            make.right.equalTo(self).offset(-10);
            make.height.mas_equalTo(0);
            
        }];
    }
    return _VerifyView;
}

#pragma mark - Overrides

/**
 身份信息验证
 */
-(void)verifyAuther:(void(^)(BOOL success))handle{
    if (!self.identity.length) {
        [SVProgressHUD showErrorWithStatus:@"请填写手机号码"];
        return;
    }
    [self.vCodeRequestParamers removeAllObjects];
    @weakify(self);
    self.verifyBlock = ^(NSDictionary * _Nonnull paramers) {
        @strongify(self);
        [self.vCodeRequestParamers addEntriesFromDictionary:paramers];
        handle ? handle(YES) : nil;
    };
    if (self.verifyType == VerifyAuthorType_IMAGE){
        if (!self.vCode.length) {
            [SVProgressHUD showErrorWithStatus:@"请填写图形验证码"];
            handle ? handle(NO) : nil;
        }else{
            [self.vCodeRequestParamers safeSetObject:self.vCode forKey:kLRKeyVcode];
            handle ? handle(YES) : nil;
        }
    }else if(self.verifyType == VerifyAuthorType_UNVERIFY){
        [self requestSMScode];
    }else{
        [self.verifyMannager launchVerify];
    }
}

#pragma mark - GET/SET

-(NSMutableDictionary *)vCodeRequestParamers{
    if (!_vCodeRequestParamers) {
        _vCodeRequestParamers = @{}.mutableCopy;
    }
    return _vCodeRequestParamers;
}

-(NSString *)phone{
    return self.identity;
}

-(void)setPhone:(NSString *)phone{
    if ([self.identity isEqualToString:phone]) {
        return;
    }
    self.identity = phone;
}
@end
