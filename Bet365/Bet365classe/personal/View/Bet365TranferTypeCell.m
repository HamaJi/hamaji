//
//  Bet365TranferTypeCell.m
//  Bet365
//
//  Created by luke on 2018/8/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Bet365TranferTypeCell.h"

@interface Bet365TranferTypeCell()
@property (weak, nonatomic) IBOutlet UILabel *toLabel;
@property (weak, nonatomic) IBOutlet UILabel *balance;
@end

@implementation Bet365TranferTypeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    self.toLabel.textColor = [UIColor skinTextItemNorColor];
    [self.balanceButton setTitleColor:[UIColor skinTextItemNorColor] forState:UIControlStateNormal];
    self.balance.textColor = [UIColor skinTextItemNorColor];
    // Initialization code
}

- (IBAction)balanceClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(typeClickBtnWithCell:)]) {
        [self.delegate typeClickBtnWithCell:self];
    }
}

- (void)setTypeCellWithTitle:(NSString *)title index:(NSInteger)index money:(NSString *)money
{
    if (index == 0) {
        self.toLabel.text = @"*余额转出";
    }else{
        self.toLabel.text = @"*余额转入";
    }
    [self.balanceButton setTitle:title forState:UIControlStateNormal];
    if (![title isEqualToString:@"账户余额"]) {
        self.balance.text = money;
    }else{
        self.balance.text = nil;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
