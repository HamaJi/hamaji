//
//  PersonPageCell.h
//  Bet365
//
//  Created by HHH on 2018/8/5.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>


@class PersonPageCell;

@protocol PersonPageCellDelegate <NSObject>

@required

-(void)PersonPageCell:(PersonPageCell *)cell didTapItem:(CenterMenuItemTemplate *)itemTemplate;

@optional

@end
@interface PersonPageCell : UITableViewCell

@property (nonatomic,weak)CenterMenuSectionTemplate *sectionMenu;

+(CGFloat )getCellHeightByItemsCount:(NSInteger)count;

+(NSString *)getCellIdentifierByIndex:(NSInteger)idx;

@property (nonatomic,weak)id <PersonPageCellDelegate>delegate;
@end
