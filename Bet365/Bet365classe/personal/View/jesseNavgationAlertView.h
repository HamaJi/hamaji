//
//  jesseNavgationAlertView.h
//  Lottery
//
//  Created by jesse on 2017/3/7.
//  Copyright © 2017年 pony. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol jesseAlertViewDlelegate<NSObject>
-(void)closeAlertView;
@end
@interface jesseNavgationAlertView : UIView
//初始化
+(instancetype)initShareManager;
//布局
-(void)initSetUIByFatherView:(UIView *)FatherView;
//展现
-(void)isopenAnimation;
//关闭
-(void)isCloseAnimation;
//类型
@property(nonatomic,copy)NSString *LotteryType;
//设置弹框代理
@property(nonatomic,weak)id <jesseAlertViewDlelegate> delegate;
@end
