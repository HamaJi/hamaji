//
//  LoginCenterView.h
//  Bet365
//
//  Created by super_小鸡 on 2019/8/15.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserDataManager.h"
#import "VerifyManager.h"
#if UIKIT_STRING_ENUMS
typedef NSString LoginRequestKey NS_EXTENSIBLE_STRING_ENUM;
#else
typedef NSString LoginRequestKey;
#endif

FOUNDATION_EXTERN LoginRequestKey *const kLRKeyAccount;

FOUNDATION_EXTERN LoginRequestKey *const kLRKeyLoginPhone;

FOUNDATION_EXTERN LoginRequestKey *const kLRKeyPassword;

FOUNDATION_EXTERN LoginRequestKey *const kLRKeyPwdWithoutMD5;

FOUNDATION_EXTERN LoginRequestKey *const kLRKeyValiCode;

FOUNDATION_EXTERN LoginRequestKey *const kLRKeyVcode;

FOUNDATION_EXTERN LoginRequestKey *const kLRKeySMScode;

NS_ASSUME_NONNULL_BEGIN

@class LoginCenterView;

@protocol LoginCenterViewDelegate <NSObject>

@optional

-(void)loginCenterView:(LoginCenterView *)centerView TextFiledSubmitText:(NSString *)text Key:(LoginRequestKey *)key;

-(void)loginCenterView:(LoginCenterView *)centerView LoginWithParamers:(NSDictionary *)paramers;

-(void)loginCenterView:(LoginCenterView *)centerView TapAction:(SEL)tapAction;

@end

typedef void(^LoginVerifyBlock)(NSDictionary *paramers);

@interface LoginCenterView : UIView

@property (nonatomic,weak) id<LoginCenterViewDelegate> delegate;

@property (nonatomic,copy)LoginVerifyBlock verifyBlock;

@property (nonatomic,strong) VerifyManager *verifyMannager;

@property (nonatomic,strong)NSMutableDictionary *paramers;

- (void)updateVerifyImage;

- (void)addObser;

- (void)getCodeData;

-(void)extractLoginParamers;

@end

NS_ASSUME_NONNULL_END
