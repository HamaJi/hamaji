//
//  LoginAccountCenterView.h
//  Bet365
//
//  Created by super_小鸡 on 2019/8/15.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginCenterView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginAccountCenterView : LoginCenterView

@property (nonatomic,assign) SEL remenberAction;

@property (nonatomic,assign) SEL forgetAction;

@property (nonatomic) NSString *account;

@property (nonatomic,strong) NSString *password;

@end

NS_ASSUME_NONNULL_END
