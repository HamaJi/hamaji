//
//  Bet36TranferMoneyCell.m
//  Bet365
//
//  Created by luke on 2018/8/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Bet36TranferMoneyCell.h"

@implementation Bet36TranferMoneyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.backgroundColor = [UIColor skinViewScreenColor];
    self.topContainView.backgroundColor = [UIColor skinViewContentColor];
    self.topContainView.layer.borderColor = [UIColor skinViewContentColor].CGColor;
    
    self.moneyLeadingLabel.textColor = [UIColor skinTextItemNorColor];
    self.moneyRightLabel.textColor = [UIColor skinTextItemNorColor];
//    [self.moneyTextField setValue:[UIColor skinTextItemNorSubColor] forKeyPath:@"_placeholderLabel.textColor"];
    self.moneyTextField.textColor = [UIColor skinTextItemNorColor];
    
        // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
