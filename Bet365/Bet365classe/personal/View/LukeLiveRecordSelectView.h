//
//  LukeLiveRecordSelectView.h
//  Bet365
//
//  Created by luke on 2017/11/20.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LukeLiveRecordSelectDelegate<NSObject>

- (void)liveRecordSelectCancelClick;

- (void)liveRecordSelectWithAccount:(NSString *)account code:(NSString *)code;

@end

@interface LukeLiveRecordSelectView : UIView
/** 代理 */
@property (nonatomic,weak) id<LukeLiveRecordSelectDelegate>delegate;

@end
