//
//  jesseNavgationAlertView.m
//  Lottery
//
//  Created by jesse on 2017/3/7.
//  Copyright © 2017年 pony. All rights reserved.
//

#import "jesseNavgationAlertView.h"
#import "AppDelegate.h"

static jesseNavgationAlertView *manager=nil;
@interface jesseNavgationAlertView()
@property(nonatomic,strong)UIButton *openWardbutton;//开奖历史
@property(nonatomic,strong)UIButton *PlayinTroduceButton;//玩法介绍
@property(nonatomic,strong)jesseNavgationAlertView *alertView;//本身manager
@end
@implementation jesseNavgationAlertView
#pragma mark--初始化控制
+(instancetype)initShareManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[jesseNavgationAlertView alloc]init];
    });
    return manager;
}
#pragma mark--添加合适的initManager
-(void)initSetUIByFatherView:(UIView *)FatherView
{
    [[UIApplication sharedApplication].keyWindow addSubview:self.alertView];
    
}
#pragma mark--展现框动画
-(void)isopenAnimation
{
    [self.alertView setHidden:NO];
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animation.duration = .6;
    animation.fromValue = [NSNumber numberWithFloat:0];
    animation.toValue = [NSNumber numberWithFloat:1.0];
    [self.alertView.layer addAnimation:animation forKey:nil];
}
#pragma mark--关闭框动画
-(void)isCloseAnimation
{
    CABasicAnimation *animation1 = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animation1.duration = .6;
    animation1.fromValue = [NSNumber numberWithFloat:1.0];
    animation1.toValue = [NSNumber numberWithFloat:0];
    [self.alertView.layer addAnimation:animation1 forKey:nil];
    [NSTimer scheduledTimerWithTimeInterval:.4 repeats:NO block:^(NSTimer * _Nonnull timer) {
        [self.alertView setHidden:YES];
    }];
}
#pragma mark--初始化本身
-(jesseNavgationAlertView *)alertView
{
    if (_alertView==nil) {
        _alertView = [[jesseNavgationAlertView alloc]init];
        _alertView.frame = CGRectMake(WIDTH-95, 64, 90,70);
        _alertView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.8];
        _alertView.layer.cornerRadius = 5;
        _alertView.layer.masksToBounds = YES;
        [_alertView setHidden:YES];
        [_alertView addSubview:self.openWardbutton];
        [_alertView addSubview:self.PlayinTroduceButton];
    }
    return _alertView;
}
#pragma mark--开奖历史buttonUI
-(UIButton *)openWardbutton
{
    if (_openWardbutton==nil) {
        _openWardbutton = [UIButton buttonWithType:UIButtonTypeCustom];
        _openWardbutton.frame = CGRectMake(0, 0, 90, 35);
        [_openWardbutton setTitle:@"开奖历史" forState:UIControlStateNormal];
        [_openWardbutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_openWardbutton addTarget:self action:@selector(ClickHistoryWardBy:) forControlEvents:UIControlEventTouchUpInside];
        _openWardbutton.titleLabel.font = [UIFont systemFontOfSize:13];
        
    }
    return _openWardbutton;
}
#pragma mark--玩法介绍buttonUI
-(UIButton *)PlayinTroduceButton
{
    if (_PlayinTroduceButton==nil) {
        _PlayinTroduceButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _PlayinTroduceButton.frame = CGRectMake(0, 35, 90, 35);
        [_PlayinTroduceButton setTitle:@"玩法介绍" forState:UIControlStateNormal];
        [_PlayinTroduceButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_PlayinTroduceButton addTarget:self action:@selector(ClickPlayerTroduceBy:) forControlEvents:UIControlEventTouchUpInside];
        _PlayinTroduceButton.titleLabel.font = [UIFont systemFontOfSize:13];

    }
    return _PlayinTroduceButton;
}
//#pragma mark--开奖历史Click
//-(void)ClickHistoryWardBy:(UIButton *)button
//{
//    jesseKsHistoryViewController *history = [[jesseKsHistoryViewController alloc]init];
//    history.view.backgroundColor = [UIColor whiteColor];
//    history.navigationItem.title = @"快3开奖历史";
//    AppDelegate *delegate = (id)[UIApplication sharedApplication].delegate;
//    LTYTabBarController *tab = delegate.tabbar;
//    LTYNavController *buyC = tab.viewControllers[0];
//    [buyC pushViewController:history animated:YES];
//    [[jesseNavgationAlertView initShareManager]isCloseAnimation];
//    [self.delegate closeAlertView];
//}
//#pragma mark--玩法介绍Click
//-(void)ClickPlayerTroduceBy:(UIButton *)button
//{
//    jessewebViewController *web = [[jessewebViewController alloc]init];
//    web.whwebUrl = @"https://api.8888.com/images/gkuai3_wanfa.png";
//    web.navigationItem.title = self.LotteryType;
//    AppDelegate *delegate = (id)[UIApplication sharedApplication].delegate;
//    LTYTabBarController *tab = delegate.tabbar;
//    LTYNavController *buyC = tab.viewControllers[0];
//    [buyC pushViewController:web animated:YES];
//    [[jesseNavgationAlertView initShareManager]isCloseAnimation];
//    [self.delegate closeAlertView];
//}
@end
