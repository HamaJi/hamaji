//
//  LoginPhoneCenterView.h
//  Bet365
//
//  Created by super_小鸡 on 2019/8/15.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginCenterView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginPhoneCenterView : LoginCenterView

@property (nonatomic) NSString *phone;

@property (nonatomic,strong,readonly) NSString *smsCode;

@end

NS_ASSUME_NONNULL_END
