//
//  Bet365TranferAccountCell.m
//  Bet365
//
//  Created by luke on 2018/8/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Bet365TranferAccountCell.h"

@interface Bet365TranferAccountCell()
@property (weak, nonatomic) IBOutlet UILabel *moneyLb;
@property (weak, nonatomic) IBOutlet UILabel *moneyLeadingLabel;
@property (weak, nonatomic) IBOutlet UIButton *reclaimBtn;

@end

@implementation Bet365TranferAccountCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.moneyLeadingLabel.textColor = [UIColor skinTextItemNorColor];
    self.moneyLb.textColor = [UIColor skinTextItemNorColor];
    [self.reclaimBtn setTitleColor:[UIColor skinViewKitNorColor] forState:UIControlStateNormal];
    [self.reclaimBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];
    @weakify(self);
    [RACObserve(USER_DATA_MANAGER.userInfoData, money) subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        self.moneyLb.text = [NSString roundingWithFloat:USER_DATA_MANAGER.userInfoData.money];
    }];
}


- (IBAction)reclaimClick:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(reclaimLiveAmount)]) {
        [self.delegate reclaimLiveAmount];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
