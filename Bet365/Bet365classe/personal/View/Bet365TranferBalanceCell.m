//
//  Bet365TranferBalanceCell.m
//  Bet365
//
//  Created by luke on 2018/8/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Bet365TranferBalanceCell.h"
#import "LukeLiveRecordItem.h"

@interface Bet365TranferBalanceCell()
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@end

@implementation Bet365TranferBalanceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.typeLabel.textColor = [UIColor skinTextItemNorColor];
    self.moneyLabel.textColor = [UIColor skinTextItemNorColor];
    [self.submitBtn setTitleColor:[UIColor skinViewKitNorColor] forState:UIControlStateNormal];
    [self.submitBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];
    // Initialization code
}
- (IBAction)typeClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(queryBalanceWithCell:)]) {
        [self.delegate queryBalanceWithCell:self];
    }
}

- (void)setItem:(LukeLiveGamesItem *)item
{
    _item = item;
    self.typeLabel.text = item.name;
    self.moneyLabel.text = item.balance ? StringFormatWithFloat([item.balance floatValue]) : nil;
    if (item.isFix && [item.isFix integerValue] == 0) {
        [self.submitBtn setTitle:@"维护中" forState:UIControlStateNormal];
    }else{
        [self.submitBtn setTitle:@"查询" forState:UIControlStateNormal];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
