//
//  UserLoginLimitAlertView.h
//  Bet365
//
//  Created by wind on 2020/2/2.
//  Copyright © 2020 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LimitModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface UserLoginLimitAlertView : UIView

@property (nonatomic,strong) NSDictionary *params;
@property (nonatomic,strong) NSArray *ipLocationVerifyFields;
@property (nonatomic,assign) NSInteger ipLocationVerifyCount;

@end

NS_ASSUME_NONNULL_END
