//
//  PersonPageCellItemView.h
//  Bet365
//
//  Created by HHH on 2018/8/5.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonPageCellItemView : UIView

@property (weak, nonatomic) IBOutlet UILabel *bugeView;

@property (assign, nonatomic) NSUInteger buge;

@property (nonatomic) NSString *iconUrl;

@property (nonatomic) NSString *titile;

@end
