//
//  Bet365SecurityTableViewCell.h
//  Bet365
//
//  Created by package_mr.chicken on 2019/1/15.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LukeRegisterLbTFView.h"
NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, securityStatus){
    NormalSecurity_Type,
    SecurityCode_Type
};
typedef void (^securityBlock)(NSString *content,NSString *key);
typedef void (^reloadCode)(UIButton *codeB);
@interface Bet365SecurityTableViewCell : UITableViewCell
- (void)SecurityType:(securityStatus)type ExhibTitle:(NSString *)title subtitle:(NSString *)titles keyC:(NSString *)keys forSecurityComplement:(securityBlock)complement reloadCode:(reloadCode)codeComplement;
@property(nonatomic,weak)LukeRegisterLbTFView *exbinView;//展示类
@end

NS_ASSUME_NONNULL_END
