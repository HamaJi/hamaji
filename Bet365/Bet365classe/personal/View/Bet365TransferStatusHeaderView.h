//
//  Bet365TransferStatusHeaderView.h
//  Bet365
//
//  Created by luke on 2019/11/27.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LukeLiveRecordItem.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TransferStatusHeaderViewDelegate <NSObject>

- (void)transferStatusHandel;

@end

@interface Bet365TransferStatusHeaderView : UITableViewHeaderFooterView

- (void)transferTitle:(NSString *)title index:(NSInteger)index status:(TransferStatus)status;

@property (nonatomic,weak) id<TransferStatusHeaderViewDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
