//
//  Bet365RecordViewCell.m
//  Bet365
//
//  Created by luke on 2018/8/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Bet365RecordViewCell.h"
#import "Bet365CpHistoryModel.h"
#import "LukeUserAdapter.h"

@interface Bet365RecordViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *gameLb;
@property (weak, nonatomic) IBOutlet UILabel *betLb;
@property (weak, nonatomic) IBOutlet UILabel *winOrLoseLb;
@property (weak, nonatomic) IBOutlet UILabel *validLb;

@end

@implementation Bet365RecordViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.gameLb.textColor = [UIColor skinTextItemNorSubColor];
    self.betLb.textColor = [UIColor skinTextItemNorColor];
    self.validLb.textColor = [UIColor skinTextItemNorColor];
    self.gameLb.font = [UIFont systemFontOfSize:IS_IPHONE_5s ? 13 : 15];
    self.betLb.font = [UIFont systemFontOfSize:IS_IPHONE_5s ? 13 : 15];
    self.winOrLoseLb.font = [UIFont systemFontOfSize:IS_IPHONE_5s ? 13 : 15];
    self.validLb.font = [UIFont systemFontOfSize:IS_IPHONE_5s ? 13 : 15];
    self.contentView.backgroundColor = [UIColor skinViewBgColor];
}

- (void)setCpModel:(Bet365CpHistoryModel *)cpModel
{
    _cpModel = cpModel;
    self.gameLb.text = cpModel.statTime;
    self.betLb.text = [LukeUserAdapter decimalNumberWithId:[cpModel.bettingMoney doubleValue]];
    self.winOrLoseLb.text = [LukeUserAdapter decimalNumberWithId:[cpModel.winOrcloseMoney doubleValue]];
    if ([cpModel.winOrcloseMoney doubleValue] < 0) {
        self.winOrLoseLb.textColor = JesseColor(22, 130, 24);
    }else if ([cpModel.winOrcloseMoney doubleValue] > 0) {
        self.winOrLoseLb.textColor = JesseColor(251, 16, 30);
    }else{
        self.winOrLoseLb.textColor = [UIColor blackColor];
    }
    self.validLb.text = [LukeUserAdapter decimalNumberWithId:[cpModel.validMoney doubleValue]];
}

- (void)setGameModel:(Bet365GameReportListModel *)gameModel
{
    _gameModel = gameModel;
    if ([gameModel.gameCode isEqualToString:@"hgSport"]) {
        self.gameLb.text = @"皇冠体育";
    }else if ([gameModel.gameCode isEqualToString:@"wzry"]) {
        self.gameLb.text = @"王者荣耀";
    }else if ([gameModel.gameCode isEqualToString:@"sb"]) {
        self.gameLb.text = @"沙巴体育";
    }else if ([gameModel.gameCode isEqualToString:@"jb"]) {
        self.gameLb.text = @"金宝棋牌";
    }else if ([gameModel.gameCode isEqualToString:@"ky"]) {
        self.gameLb.text = @"开元棋牌";
    }else if ([gameModel.gameCode isEqualToString:@"qly"]) {
        self.gameLb.text = @"棋乐游";
    }else if ([gameModel.gameCode isEqualToString:@"lucky"]) {
        self.gameLb.text = @"幸运棋牌";
    }else if ([gameModel.gameCode isEqualToString:@"nw"]) {
        self.gameLb.text = @"新世界棋牌";
    }else if ([gameModel.gameCode isEqualToString:@"wz"]) {
        self.gameLb.text = @"王者棋牌";
    }else if ([gameModel.gameCode isEqualToString:@"dfw"]) {
        self.gameLb.text = @"大富翁";
    }else if ([gameModel.gameCode isEqualToString:@"pt2"]) {
        self.gameLb.text = @"SW电子";
    }else if ([gameModel.gameCode isEqualToString:@"avia"]) {
        self.gameLb.text = @"泛亚电竞";
    }else{
        self.gameLb.text = [gameModel.gameCode uppercaseString];
    }
    self.betLb.text = [LukeUserAdapter decimalNumberWithId:[gameModel.betAmount doubleValue]];
    self.winOrLoseLb.text = [LukeUserAdapter decimalNumberWithId:-1 * [gameModel.winAmount doubleValue]];
    if ([gameModel.winAmount doubleValue] < 0) {
        self.winOrLoseLb.textColor = JesseColor(22, 130, 24);
    }else if ([gameModel.winAmount doubleValue] > 0) {
        self.winOrLoseLb.textColor = JesseColor(251, 16, 30);
    }else{
        self.winOrLoseLb.textColor = [UIColor blackColor];
    }
    self.validLb.text = [LukeUserAdapter decimalNumberWithId:[gameModel.validAmount doubleValue]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
