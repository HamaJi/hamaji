//
//  PersonPageHeaderView.h
//  Bet365
//
//  Created by HHH on 2018/8/5.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, PersonPageHeaderViewAction) {
    PersonPageHeaderViewAction_RECHARGE = 0,        //充值
    PersonPageHeaderViewAction_DRAW,                //提现
    PersonPageHeaderViewAction_GROWRULE,
    PersonPageHeaderViewAction_GROWTYPE,
};

@class PersonPageHeaderView;

@protocol PersonHeaderViewDelegate <NSObject>

@required

-(void)PersonPageHeaderView:(PersonPageHeaderView *)headView didTapHeaderAction:(PersonPageHeaderViewAction)actionType;

-(void)PersonPageHeaderView:(PersonPageHeaderView *)headView submitSigin:(BOOL)sigin;

@optional

@end

@interface PersonPageHeaderView : UIView


@property (nonatomic,weak)id<PersonHeaderViewDelegate>delegate;

@property (nonatomic,assign)NSInteger currentValue;

@property (nonatomic,assign)NSInteger totalValueInLevel;

@property (nonatomic,assign)NSInteger totalValue;


@property (nonatomic)NSInteger level;

@property (nonatomic,assign)BOOL hasSignIn;


@property (nonatomic,assign)BOOL isShowGrowthLevel;
@end
