//
//  Bet365TranferBalanceCell.h
//  Bet365
//
//  Created by luke on 2018/8/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Bet365TranferBalanceCell;
@class LukeLiveGamesItem;

@protocol TranferBalanceCellDelegate <NSObject>

- (void)queryBalanceWithCell:(Bet365TranferBalanceCell *)cell;

@end

@interface Bet365TranferBalanceCell : UITableViewCell
/** 模型 */
@property (nonatomic,strong) LukeLiveGamesItem *item;
/** 代理 */
@property (nonatomic,weak) id<TranferBalanceCellDelegate>delegate;
@end
