//
//  LukeRechargeOrderCell.m
//  Bet365
//
//  Created by luke on 2017/11/20.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeRechargeOrderCell.h"
#import "LukeRechargeOrderItem.h"

@implementation LukeRechargeOrderCell
{
    UILabel *_addTimeLb;
    UILabel *_amountLb;
    UILabel *_statusLb;
    /** 蛤蟆吉19.2.22 充值记录新增remarks字段展示*/
    UILabel *_remarksLb;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    _addTimeLb = [UILabel addLabelWithFont:IS_IPHONE_5s ? 14 : 16 color:YCZColor(80, 89, 99) title:nil];
    [self.contentView addSubview:_addTimeLb];
    [_addTimeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.leading.equalTo(self.contentView).offset(10);
    }];
    
    _amountLb = [UILabel addLabelWithFont:IS_IPHONE_5s ? 14 : 16 color:YCZColor(80, 89, 99) title:nil];
    [self.contentView addSubview:_amountLb];
    [_amountLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_addTimeLb.mas_bottom).offset(10);
        make.leading.equalTo(self.contentView).offset(10);
    }];
    
    _statusLb = [UILabel addLabelWithFont:IS_IPHONE_5s ? 14 : 16 color:YCZColor(80, 89, 99) title:nil];
    [self.contentView addSubview:_statusLb];
    [_statusLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_amountLb.mas_top);
        make.leading.equalTo(self.contentView).offset(WIDTH * 0.5);
        make.left.mas_greaterThanOrEqualTo(_amountLb.mas_right);
    }];
    
    _remarksLb = [UILabel addLabelWithFont:IS_IPHONE_5s ? 14 : 16 color:YCZColor(80, 89, 99) title:nil];
    [self.contentView addSubview:_remarksLb];
    [_remarksLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_amountLb.mas_bottom).offset(10);
        make.left.equalTo(self.contentView).offset(10);
        make.bottom.equalTo(self.contentView).offset(-10);
    }];
    
}

- (void)setItem:(LukeRechargeOrderItem *)item
{
    _item = item;
    
    _addTimeLb.text = [NSString stringWithFormat:@"订单时间 %@",item.addTime];
    _amountLb.text = [NSString stringWithFormat:@"充值金额 %@元",StringFormatWithFloat(item.totalAmount)];
    if (item.status == 1 || item.status == 2) {
        _statusLb.attributedText = [NSMutableAttributedString setTitle:@"状态" colorTitle:@"受理中" color:[UIColor colorWithHexString:@"#07d00b"]];
    }else if (item.status == 3){
        _statusLb.attributedText = [NSMutableAttributedString setTitle:@"状态" colorTitle:@"已入款" color:[UIColor colorWithHexString:@"#e71010"]];
    }else{
        _statusLb.attributedText = [NSMutableAttributedString setTitle:@"状态" colorTitle:@"已取消" color:[UIColor colorWithHexString:@"#908e8e"]];
    }
}

@end
