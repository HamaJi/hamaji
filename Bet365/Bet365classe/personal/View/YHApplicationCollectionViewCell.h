//
//  YHApplicationCollectionViewCell.h
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/28.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PrmtLobbyModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YHApplicationCollectionViewCell : UICollectionViewCell

@property (nonatomic,weak) PrmtYHApplication *model;

@end

NS_ASSUME_NONNULL_END
