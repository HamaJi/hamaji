//
//  LukeReportMagSelectView.m
//  Bet365
//
//  Created by luke on 17/6/26.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeRechargeOrderSelectView.h"
#import "LukeRegisterLbTFView.h"
#import "LukeUserAdapter.h"
#import "LukeMaskView.h"

@interface LukeRechargeOrderSelectView ()
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *accountView;
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *fanshuiView;
/**  */
@property (nonatomic,weak) UIView *contentView;
/**  */
@property (nonatomic,strong) NSArray *tempArrs;
/** 是否代理 */
@property (nonatomic,assign) BOOL isDL;
/** 装名称的数组 */
@property (nonatomic,strong) NSArray *orderArr;

@end

@implementation LukeRechargeOrderSelectView
- (NSArray *)tempArrs
{
    if (_tempArrs == nil) {
        _tempArrs = @[@"全部",@"转账汇款",@"在线支付",@"人工充值"];
    }
    return _tempArrs;
}
- (NSArray *)orderArr
{
    if (_orderArr == nil) {
        _orderArr = @[@{@"key" : @"all",@"value" : @"全部"},@{@"key" : @"1",@"value" : @"转账汇款"},@{@"key" : @"2",@"value" : @"在线支付"},@{@"key" : @"3",@"value" : @"人工充值"}];
    }
    return _orderArr;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.backgroundColor = [JesseGrayColor(10) colorWithAlphaComponent:0.3];
    UIView *contentView = [[UIView alloc] init];
    contentView.backgroundColor = JesseGrayColor(238);
    [self addSubview:contentView];
    self.contentView = contentView;
    
    UILabel *lb = [self addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:@"筛选条件"];
    [contentView addSubview:lb];
    [lb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(contentView).offset(10);
        make.leading.equalTo(contentView).offset(10);
        make.height.mas_equalTo(20);
    }];
    
    LukeRegisterLbTFView *fanshuiView = [LukeRegisterLbTFView registerLbBtnViewWithTitle:@"充值模式" btnImage:@"right_lage" btnTitle:@"全部"];
    fanshuiView.callBlock = ^{
        [BRStringPickerView showStringPickerWithTitle:@"" dataSource:self.tempArrs defaultSelValue:nil isAutoSelect:NO themeColor:[UIColor skinNaviBgColor] resultBlock:^(id selectValue) {
            [self.fanshuiView.dateBtn setTitle:selectValue forState:UIControlStateNormal];
        }];
    };
    [contentView addSubview:fanshuiView];
    [fanshuiView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lb.mas_bottom).offset(5);
        make.leading.trailing.equalTo(contentView);
        make.height.mas_equalTo(40);
    }];
    self.fanshuiView = fanshuiView;
    
    UIButton *sureBtn = [self addButtonWithTitle:@"搜索" font:17 color:[UIColor whiteColor] target:self action:@selector(sureClick)];
    sureBtn.backgroundColor = [UIColor redColor];
    sureBtn.layer.cornerRadius = 5;
    sureBtn.layer.masksToBounds = YES;
    [contentView addSubview:sureBtn];
    [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(fanshuiView.mas_bottom).offset(100);
        make.leading.equalTo(contentView).offset(10);
        make.trailing.equalTo(contentView).offset(-10);
        make.height.mas_equalTo(40);
        make.bottom.mas_equalTo(contentView.mas_bottom).offset(-10 - SafeAreaBottomHeight);
    }];
    
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.bottom.equalTo(self);
    }];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn addTarget:self action:@selector(cancelClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cancelBtn];
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.top.equalTo(self);
        make.bottom.equalTo(contentView.mas_top);
    }];
}

- (void)cancelClick
{
    if ([self.delegate respondsToSelector:@selector(rechargeOrderSelectCancelClick)]) {
        [self.delegate rechargeOrderSelectCancelClick];
    }
}

- (void)sureClick
{
    if ([self.delegate respondsToSelector:@selector(rechargeOrderSelectWithAccount:key:)]) {
        __block NSString *code = nil;
        [self.orderArr enumerateObjectsUsingBlock:^(NSDictionary *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([self.fanshuiView.dateBtn.currentTitle isEqualToString:obj[@"value"]]) {
                code = obj[@"key"];
                *stop = YES;
            }
        }];
        [self.delegate rechargeOrderSelectWithAccount:self.accountView.textfield.text key:code];
    }
}

#pragma mark - 创建button
- (UIButton *)addButtonWithTitle:(NSString *)title font:(CGFloat)font color:(UIColor *)color target:(id)target action:(SEL)action
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.backgroundColor = [UIColor whiteColor];
    btn.titleLabel.font = [UIFont systemFontOfSize:font];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:color forState:UIControlStateNormal];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn];
    return btn;
}
//创建label
- (UILabel *)addLabelWithFont:(CGFloat)font color:(UIColor *)color title:(NSString *)title
{
    UILabel *label = [[UILabel alloc] init];
    label.text = title;
    label.textColor = color;
    label.font = [UIFont systemFontOfSize:font];
    return label;
}

@end



