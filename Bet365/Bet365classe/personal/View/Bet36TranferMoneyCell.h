//
//  Bet36TranferMoneyCell.h
//  Bet365
//
//  Created by luke on 2018/8/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Bet36TranferMoneyCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *moneyTextField;
@property (weak, nonatomic) IBOutlet UIView *topContainView;
@property (weak, nonatomic) IBOutlet UILabel *moneyRightLabel;

@property (weak, nonatomic) IBOutlet UILabel *moneyLeadingLabel;
@end
