//
//  YEBaoHistoryTableViewCell.m
//  Bet365
//
//  Created by super_小鸡 on 2019/6/10.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "YEBaoHistoryTableViewCell.h"
#import <pop/POP.h>

@interface YEBaoHistoryTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIView *reteContentView;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@property (weak, nonatomic) IBOutlet UILabel *rateLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeLayConstraintWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rateLayConstraintWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeLayConstraintLeading;
@end

@implementation YEBaoHistoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.backgroundColor = [UIColor skinViewScreenColor];
    self.backgroundColor = [UIColor skinViewScreenColor];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setRate:(NSNumber *)rate BetweenMinRete:(NSNumber *)minRate MaxRate:(NSNumber *)maxRate{
    self.rateLabel.text=  [rate stringValue];
    
    double dRate = [rate doubleValue];
    double dMinRate = [minRate doubleValue];
    double dMaxRate = [maxRate doubleValue];
    
    CGFloat maxWidth = WIDTH - 2.0 *self.timeLayConstraintLeading.constant - self.timeLayConstraintWidth.constant;
    CGFloat minWidth = [[minRate stringValue] getTextWidthWithFont:[UIFont systemFontOfSize:17]].width + 16;
    
    CGFloat diffrenceWidth = 0.0;
    float diffrenceDuration = 0.0;
    if (dMaxRate - dMinRate > 0.00001) {
        diffrenceWidth = (maxWidth - minWidth) * (dRate - dMinRate) / (dMaxRate - dMinRate);
        diffrenceDuration = 2.0 * (dRate - dMinRate) / (dMaxRate - dMinRate);
    }
    
    if (self.rateLayConstraintWidth.constant != (minWidth + diffrenceWidth)) {
        [UIView animateWithDuration:diffrenceDuration animations:^{
            self.rateLayConstraintWidth.constant = minWidth + diffrenceWidth;
            [self layoutIfNeeded];
        }];
    }
    
}

-(void)setDate:(NSDate *)date{
     self.timeLabel.text = [date getTimeFormatStringWithFormat:@"yyyy-MM-dd"];
}

-(void)setIsFirst:(BOOL)isFirst{
    
    if (isFirst) {
        self.bgImageView.image = [UIImage skinGradientSelImage];
    }else{
        self.bgImageView.image = [UIImage createImageWithColor:[UIColor lightGrayColor]];
    }
}
@end
