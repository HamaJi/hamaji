//
//  jessepersonProtocolBet365TableViewCell.m
//  Bet365
//
//  Created by jesse on 2017/4/15.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "jessepersonProtocolBet365TableViewCell.h"
@interface jessepersonProtocolBet365TableViewCell()
@property(nonatomic,strong)UILabel *desLabel;
@end
@implementation jessepersonProtocolBet365TableViewCell


#pragma mark--初始化
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self==[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self desLabel];
    }
    return self;
}
#pragma mark--初始化描述Label
-(UILabel *)desLabel
{
    if (_desLabel==nil) {
        _desLabel = [[UILabel alloc]init];
        _desLabel.font = [UIFont systemFontOfSize:14];
        _desLabel.textAlignment = NSTextAlignmentLeft;
        _desLabel.textColor = [UIColor darkGrayColor];
        _desLabel.numberOfLines=0;
        [self.contentView addSubview:_desLabel];
        [_desLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).offset(5);
            make.top.mas_equalTo(self.contentView.mas_top).offset(5);
            make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(-5);
            make.right.mas_equalTo(self.contentView.mas_right).offset(-5);
        }];
    }
    return _desLabel;
}
-(void)updateDesProtocolBy:(NSString *)des
{
    self.desLabel.text = des;
}
@end
