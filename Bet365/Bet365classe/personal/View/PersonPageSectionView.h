//
//  PersonPageSectionView.h
//  Bet365
//
//  Created by HHH on 2018/8/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonPageSectionView : UITableViewHeaderFooterView
@property (nonatomic)NSString *tittile;
+(NSString *)identifier;
+(CGFloat)getHeight;
@end
