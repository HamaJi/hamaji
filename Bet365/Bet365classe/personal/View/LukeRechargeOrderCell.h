//
//  LukeRechargeOrderCell.h
//  Bet365
//
//  Created by luke on 2017/11/20.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LukeRechargeOrderItem;

@interface LukeRechargeOrderCell : UITableViewCell
/** 模型 */
@property (nonatomic,strong) LukeRechargeOrderItem *item;

@end
