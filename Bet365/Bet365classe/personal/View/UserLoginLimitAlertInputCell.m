//
//  UserLoginLimitAlertInputCell.m
//  Bet365
//
//  Created by wind on 2020/2/2.
//  Copyright © 2020 jesse. All rights reserved.
//

#import "UserLoginLimitAlertInputCell.h"

@interface UserLoginLimitAlertInputCell () <UITextFieldDelegate>

@end

@implementation UserLoginLimitAlertInputCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.inputTF.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setMutDic:(NSMutableDictionary *)mutDic{
    _mutDic = mutDic;
    
    self.titleLB.text = mutDic[@"title"];
    self.inputTF.placeholder = [NSString stringWithFormat:@"请输入%@",mutDic[@"title"]];
    self.inputTF.text = mutDic[@"content"];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];

    [self.mutDic safeSetObject:searchStr forKey:@"content"];
    return YES;
}

@end
