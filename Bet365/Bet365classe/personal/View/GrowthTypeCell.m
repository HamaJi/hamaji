//
//  GrowthTypeCell.m
//  Bet365
//
//  Created by luke on 2019/11/4.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "GrowthTypeCell.h"
#import "GrowthTypeModel.h"

@interface GrowthTypeCell ()
@property (weak, nonatomic) IBOutlet UILabel *growthLevelLb;
@property (weak, nonatomic) IBOutlet UILabel *addTypeLb;
@property (weak, nonatomic) IBOutlet UILabel *addDescLb;
@property (weak, nonatomic) IBOutlet UILabel *addTimeLb;
@property (weak, nonatomic) IBOutlet UILabel *growthValueLb;
@property (weak, nonatomic) IBOutlet UILabel *addGrowthLb;

@end

@implementation GrowthTypeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(GrowthTypeListModel *)model
{
    _model = model;
    self.growthLevelLb.attributedText = [self getTitle:@"等级:" SubTitle:StringFormatWithInteger(model.growthLevel)];
    self.addTimeLb.attributedText = [self getTitle:@"时间:" SubTitle:model.addTime];
    self.addDescLb.attributedText = [self getTitle:@"备注:" SubTitle:[model.addDesc isNoBlankString] ? model.addDesc : @""];
    self.growthValueLb.attributedText = [self getTitle:@"当前成长值:" SubTitle:StringFormatWithInteger(model.growthValue)];
    self.addGrowthLb.attributedText = [self getTitle:@"成长值增加:" SubTitle:StringFormatWithInteger(model.addGrowth)];
    self.addTypeLb.attributedText = [self getTitle:@"成长方式:" SubTitle:model.addType];
}

- (NSMutableAttributedString *)getTitle:(NSString *)title SubTitle:(NSString *)subTitle
{
    NSString *att = StringFormatWithStr(title, subTitle);
    NSMutableAttributedString *maStr = [[NSAttributedString alloc] initWithString:att].mutableCopy;
    [maStr addColor:JesseColor(204, 80, 47) substring:subTitle];
    [maStr addFont:[UIFont systemFontOfSize:14] substring:subTitle];
    return maStr;
}

@end
