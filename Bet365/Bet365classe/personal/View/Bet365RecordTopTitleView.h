//
//  Bet365RecordTopTitleView.h
//  Bet365
//
//  Created by luke on 2018/8/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Bet365RecordTopTitleView : UIView

- (instancetype)initWithTitles:(NSArray *)titles titleColor:(UIColor *)color backgroundColor:(UIColor *)backgroundColor;

@end
