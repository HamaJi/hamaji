//
//  LukeRechargeOrderSelectView.h
//  Bet365
//
//  Created by luke on 2017/11/20.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LukeRechargeOrderSelectViewDelegate<NSObject>

- (void)rechargeOrderSelectCancelClick;

- (void)rechargeOrderSelectWithAccount:(NSString *)account key:(NSString *)key;

@end

@interface LukeRechargeOrderSelectView : UIView

/** 代理 */
@property (nonatomic,weak) id<LukeRechargeOrderSelectViewDelegate>delegate;

@end
