//
//  jesseAlertView.m
//  SIXLotteryCX
//
//  Created by Oliver on 2017/2/24.
//  Copyright © 2017年 Jesse. All rights reserved.
//

#import "jesseAlertView.h"
@interface jesseAlertView()
@property(nonatomic,strong)UILabel *textL;
@property(nonatomic,strong)UILabel *subtextL;
@end
@implementation jesseAlertView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = frame;
        [self textL];
        [self subtextL];
    }
    return self;
}
#pragma mark--懒加载构建
-(UILabel *)textL
{
    if (_textL == nil) {
        _textL = [[UILabel alloc]init];
        _textL.font = [UIFont systemFontOfSize:14];
        _textL.textAlignment = NSTextAlignmentCenter;
        _textL.text = @"该账号已被占用";
        _textL.textColor = [UIColor whiteColor];
        [self addSubview:_textL];
        [_textL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(0);
            make.right.mas_equalTo(self.mas_right).offset(0);
            make.top.mas_equalTo(self.mas_top).offset(15);
            make.height.mas_equalTo(20);
        }];
    }
    return _textL;
}
#pragma mark--构建子lable显示
-(UILabel *)subtextL
{
    if (_subtextL == nil) {
        _subtextL = [[UILabel alloc]init];
        _subtextL.font = [UIFont systemFontOfSize:11];
        _subtextL.textColor = [UIColor whiteColor];
        _subtextL.text=@"请重新输入";
        _subtextL.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_subtextL];
        [_subtextL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.mas_left).offset(0);
            make.top.mas_equalTo(self.mas_top).offset(35);
            make.right.mas_equalTo(self.mas_right).offset(0);
            make.height.mas_equalTo(20);
        }];
    }
    return _subtextL;
}
-(void)updateContentBy:(NSString *)objc AndSubString:(NSString *)sub
{
    self.textL.text = objc;
    self.subtextL.text = sub;
}
@end
