//
//  LukeLiveRecordCell.m
//  Bet365
//
//  Created by luke on 2017/11/20.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeLiveRecordCell.h"
#import "LukeLiveRecordItem.h"
#import "LukeUserAdapter.h"
#import "Bet365LivePageData.h"
#import "Bet365ElectronicModel.h"

@implementation LukeLiveRecordCell
{
    UILabel *billNoLb;
    UILabel *gameKindLb;
    UILabel *settleLb;
    UILabel *betAmountLb;
    UILabel *winAmountLb;
    UILabel *gmtBetTimeLb;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    billNoLb = [self addLabel];
    billNoLb.numberOfLines = 0;
    [billNoLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.leading.equalTo(self.contentView).offset(10);
        make.trailing.equalTo(self.contentView).offset(-10);
    }];
    
    gameKindLb = [self addLabel];
    gameKindLb.numberOfLines = 0;
    [gameKindLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(billNoLb.mas_bottom).offset(10);
        make.leading.equalTo(billNoLb.mas_leading);
        make.trailing.equalTo(self.contentView).offset(-WIDTH * 0.5);
    }];
    
    settleLb = [self addLabel];
    [settleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(gameKindLb);
        make.leading.equalTo(self.contentView).offset(WIDTH * 0.5);
    }];
    
    betAmountLb = [self addLabel];
    [betAmountLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(gameKindLb.mas_bottom).offset(10);
        make.leading.equalTo(gameKindLb.mas_leading);
    }];
    
    winAmountLb = [self addLabel];
    [winAmountLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(betAmountLb.mas_top);
        make.leading.equalTo(self.contentView).offset(WIDTH * 0.5);
    }];
    
    gmtBetTimeLb = [self addLabel];
    [gmtBetTimeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(betAmountLb.mas_bottom).offset(10);
        make.leading.equalTo(betAmountLb.mas_leading);
        make.bottom.equalTo(self.contentView).offset(-10);
    }];
}

- (void)setCellItem:(LukeLiveRecordItem *)item code:(NSString *)code
{
    billNoLb.text = [NSString stringWithFormat:@"下注单号 %@",item.billNo];
    __block NSString *codeStr = nil;
    [[Bet365LivePageData shareIntance].data enumerateObjectsUsingBlock:^(Bet365ElectronicModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([item.gameType isEqualToString:obj.gameType]) {
            codeStr = obj.chineseName;
            *stop = YES;
        }
    }];
    
    gameKindLb.text = [NSString stringWithFormat:@"游戏类型 %@",codeStr.length > 0 ? codeStr : ((item.gameType && item.gameType.length > 0) ? item.gameType : item.gameKind)];
    switch (item.settle) {
        case -1:
            settleLb.attributedText = [self setSettleLabelWithStr:@"注单状态 " settle:@"已撤单" color:[LukeUserAdapter colorWithHexString:@"#906944"]];
            break;
        case 0:
            settleLb.attributedText = [self setSettleLabelWithStr:@"注单状态 " settle:@"未结算" color:[LukeUserAdapter colorWithHexString:@"#07d00b"]];
            break;
        case 1:
            settleLb.attributedText = [self setSettleLabelWithStr:@"注单状态 " settle:@"已结算" color:[LukeUserAdapter colorWithHexString:@"#e2bc03"]];
            break;
        case 2:
            settleLb.attributedText = [self setSettleLabelWithStr:@"注单状态 " settle:@"待处理" color:[LukeUserAdapter colorWithHexString:@"#e2bc03"]];
            break;
        case 3:
            settleLb.attributedText = [self setSettleLabelWithStr:@"注单状态 " settle:@"失败" color:[LukeUserAdapter colorWithHexString:@"#e2bc03"]];
            break;
        default:
            break;
    }
    betAmountLb.text = [NSString stringWithFormat:@"下注金额 %@",item.betAmount];
    UIColor *color = nil;
    if ([item.winAmount doubleValue] < 0) {
        color = JesseColor(22, 130, 24);
    }else if ([item.winAmount doubleValue] > 0) {
        color = JesseColor(251, 16, 30);
    }else{
        color = [UIColor blackColor];
    }
    winAmountLb.attributedText = [NSMutableAttributedString setTitle:@"输赢 " colorTitle:[LukeUserAdapter decimalNumberWithId:[self dealNumberWithNum:item.winAmount]] color:color];
    gmtBetTimeLb.text = [NSString stringWithFormat:@"下注时间 %@",item.gmtBetTime];
}


- (CGFloat)dealNumberWithNum:(id)str
{
    if (![str isEqual:[NSNull null]]) {
        if ([str doubleValue] <= 0) {
            return fabs([str doubleValue]);
        }else{
            return -[str doubleValue];
        }
    }else{
        return 0;
    }
}

- (NSMutableAttributedString *)setSettleLabelWithStr:(NSString *)str settle:(NSString *)settle color:(UIColor *)color
{
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@",str,settle]];
    [attStr setAttributes:@{NSForegroundColorAttributeName : color} range:NSMakeRange(str.length + 1, settle.length)];
    return attStr;
}

//创建label
- (UILabel *)addLabel
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = YCZColor(80, 89, 99);
    label.font = [UIFont systemFontOfSize:IS_IPHONE_5s ? 14 : 16];
    [self.contentView addSubview:label];
    return label;
}

@end
