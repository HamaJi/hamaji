//
//  Bet365TranferTypeCell.h
//  Bet365
//
//  Created by luke on 2018/8/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Bet365TranferTypeCell;

@protocol TranferTypeCellDelegate <NSObject>

- (void)typeClickBtnWithCell:(Bet365TranferTypeCell *)cell;

@end

@interface Bet365TranferTypeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *balanceButton;
/** 代理 */
@property (nonatomic,weak) id<TranferTypeCellDelegate>delegate;

- (void)setTypeCellWithTitle:(NSString *)title index:(NSInteger)index money:(NSString *)money;

@end
