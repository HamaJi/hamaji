//
//  jesseLogTextFieldBet365.m
//  Bet365
//
//  Created by jesse on 2017/4/13.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "jesseLogTextFieldBet365.h"

@implementation jesseLogTextFieldBet365

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0,0, WIDTH,44)];
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - 60, 7,50, 30)];
    [button setTitle:@"完成"forState:UIControlStateNormal];
    [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    button.layer.borderColor = [UIColor redColor].CGColor;
    button.layer.borderWidth =1;
    button.layer.cornerRadius =3;
    UIBarButtonItem *finishButton = [[UIBarButtonItem alloc]initWithCustomView:button];
    UIBarButtonItem *flexButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [bar setItems:@[flexButton,flexButton,finishButton]];
    self.inputAccessoryView = bar;
    [button addTarget:self action:@selector(completedButtonClick:)forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)completedButtonClick:(UIButton *)button
{
    [self resignFirstResponder];
}
//重写方法
-(CGRect)textRectForBounds:(CGRect)bounds
{
    CGRect inset = CGRectMake(bounds.origin.x+1, bounds.origin.y, bounds.size.width-25, bounds.size.height);
    return inset;
}
- (CGRect)editingRectForBounds:(CGRect)bounds
{
    CGRect inset = CGRectMake(bounds.origin.x+1, bounds.origin.y, bounds.size.width-25, bounds.size.height);
    return inset;
    
}

@end
