//
//  LukeLiveRecordCell.h
//  Bet365
//
//  Created by luke on 2017/11/20.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LukeLiveRecordItem;

@interface LukeLiveRecordCell : UITableViewCell

- (void)setCellItem:(LukeLiveRecordItem *)item code:(NSString *)code;

@end
