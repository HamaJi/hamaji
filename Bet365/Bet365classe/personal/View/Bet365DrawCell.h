//
//  Bet365DrawCell.h
//  Bet365
//
//  Created by luke on 2018/8/9.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Bet365DrawModel;
@interface Bet365DrawCell : UITableViewCell
/** 模型 */
@property (nonatomic,strong) Bet365DrawModel *model;

@end
