//
//  Bet365TranferAccountCell.h
//  Bet365
//
//  Created by luke on 2018/8/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TranferAccountCellReclaimLiveAmountDelegate <NSObject>

- (void)reclaimLiveAmount;

@end

@interface Bet365TranferAccountCell : UITableViewCell

@property (nonatomic,weak) id<TranferAccountCellReclaimLiveAmountDelegate>delegate;

@end
