//
//  LukeNoticeCell.m
//  Bet365
//
//  Created by luke on 17/6/19.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeNoticeCell.h"
#import "LukeNoticeItem.h"

@implementation LukeNoticeCell
{
    UILabel *titleLabel;
    UILabel *subTitle;
    UILabel *timeLabel;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.contentView.backgroundColor = [UIColor skinViewBgColor];
    self.backgroundColor = [UIColor skinViewBgColor];
    titleLabel = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorColor] title:nil];
    titleLabel.numberOfLines = 0;
    [self.contentView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.leading.equalTo(self.contentView).offset(10);
        make.trailing.equalTo(self.contentView).offset(-5);
    }];
    
    subTitle = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:nil];
    [self.contentView addSubview:subTitle];
    [subTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(titleLabel);
        make.top.equalTo(titleLabel.mas_bottom).offset(10);
        make.bottom.equalTo(self.contentView).offset(-10);
    }];
    
    timeLabel = [UILabel addLabelWithFont:14 color:[UIColor skinTextItemNorSubColor] title:nil];
    [self.contentView addSubview:timeLabel];
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(self.contentView);
        make.centerY.equalTo(subTitle);
    }];
}

- (void)setItem:(LukeNoticeItem *)item
{
    _item = item;
    titleLabel.text = item.noticeTitle;
    timeLabel.text = item.updateTime;
    subTitle.text = item.typeName;
}

@end
