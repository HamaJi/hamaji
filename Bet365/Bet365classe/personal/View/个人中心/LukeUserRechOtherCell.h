//
//  LukeUserRechOtherCell.h
//  Bet365
//
//  Created by luke on 17/7/18.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LukeUserWithdrawItem;

@interface LukeUserRechOtherCell : UITableViewCell
/** 模型 */
@property (nonatomic,strong) LukeUserWithdrawItem *UserWithdrawItem;

@end
