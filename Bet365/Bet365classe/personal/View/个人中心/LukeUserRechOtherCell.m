//
//  LukeUserRechOtherCell.m
//  Bet365
//
//  Created by luke on 17/7/18.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeUserRechOtherCell.h"
#import "LukeUserWithdrawItem.h"

@implementation LukeUserRechOtherCell

- (void)setUserWithdrawItem:(LukeUserWithdrawItem *)UserWithdrawItem
{
    _UserWithdrawItem = UserWithdrawItem;
    [self.contentView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
    UILabel *bankLb = [self addLabelWithFont:16 color:[UIColor blackColor]];
    [bankLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.leading.equalTo(self.contentView).offset(10);
    }];
    
    UILabel *cashOrderNoLb = [self addLabelWithFont:16 color:JesseGrayColor(130)];
    [cashOrderNoLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bankLb.mas_bottom).offset(10);
        make.leading.equalTo(self.contentView).offset(10);
    }];
    
    UILabel *rechPersonLb = [self addLabelWithFont:16 color:JesseGrayColor(130)];
    [rechPersonLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cashOrderNoLb.mas_bottom).offset(10);
        make.leading.equalTo(self.contentView).offset(10);
    }];
    
    UILabel *rechStatusLb = [self addLabelWithFont:16 color:[UIColor redColor]];
    [rechStatusLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(rechPersonLb);
        make.trailing.equalTo(self.contentView).offset(-10);
    }];
    
    UILabel *counterFeeLb = [self addLabelWithFont:16 color:JesseGrayColor(130)];
    [counterFeeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(rechPersonLb.mas_bottom).offset(10);
        make.leading.equalTo(self.contentView).offset(10);
    }];
    
    UILabel *approveMoneyLb = [self addLabelWithFont:16 color:JesseGrayColor(130)];
    [approveMoneyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(counterFeeLb.mas_bottom).offset(10);
        make.leading.equalTo(bankLb.mas_leading);
        make.bottom.equalTo(self.contentView).offset(-10);
    }];
    
    UILabel *operatorTimeLb = [self addLabelWithFont:15 color:JesseGrayColor(130)];
    [operatorTimeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(self.contentView).offset(-10);
        make.centerY.equalTo(approveMoneyLb);
    }];
    
    bankLb.text = [NSString stringWithFormat:@"收款银行：%@",UserWithdrawItem.bankName];
    cashOrderNoLb.text = [NSString stringWithFormat:@"收款帐号：%@",UserWithdrawItem.bankCard];
    rechPersonLb.text = [NSString stringWithFormat:@"取款金额：¥%ld",UserWithdrawItem.cashMoney];
    counterFeeLb.text = [NSString stringWithFormat:@"手续费：¥%ld",UserWithdrawItem.counterFee];
    approveMoneyLb.text = [NSString stringWithFormat:@"出款金额：¥%ld",UserWithdrawItem.approveMoney];
    operatorTimeLb.text = UserWithdrawItem.cashStatus == 1 ? UserWithdrawItem.operatorTime : UserWithdrawItem.addTime;
    
    switch (UserWithdrawItem.cashStatus) {
        case 1:
            rechStatusLb.text = @"未受理";
            break;
        case 2:
            rechStatusLb.text = @"受理中";
            break;
        case 3:
            rechStatusLb.text = @"已出款";
            break;
        case 4:
            rechStatusLb.text = @"已取消";
            break;
        case 5:
            rechStatusLb.text = @"已拒绝";
            break;
        case 6:
            rechStatusLb.text = @"已撤销";
            break;
        default:
            break;
    }
}


//创建label
- (UILabel *)addLabelWithFont:(CGFloat)font color:(UIColor *)color
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = color;
    label.font = [UIFont systemFontOfSize:font];
    [self.contentView addSubview:label];
    return label;
}

@end
