//
//  LukeLoginPasswordView.h
//  Bet365
//
//  Created by luke on 17/6/27.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetWorkMannager+Account.h"

@interface LukeLoginPasswordView : UIView

- (instancetype)initWithTitle:(NSString *)title type:(PersonPassWordType)type;

@end
