//
//  LukeSpreadCell.h
//  Bet365
//
//  Created by luke on 17/6/30.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LukeSpreadItem;

@protocol LukeSpreadCellDelegate <NSObject>

- (void)LukeSpreadCellSelectButtonWithItem:(LukeSpreadItem *)item index:(NSInteger)index;

@end

@interface LukeSpreadCell : UITableViewCell
/** 模型 */
@property (nonatomic,strong) LukeSpreadItem *item;
/** 代理 */
@property (nonatomic,weak) id<LukeSpreadCellDelegate>delegate;

@end
