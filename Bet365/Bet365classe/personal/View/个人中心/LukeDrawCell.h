//
//  LukeDrawCell.h
//  Bet365
//
//  Created by luke on 17/6/20.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LukeDrawItem;

@interface LukeDrawCell : UITableViewCell
/** 模型 */
@property (nonatomic,strong) LukeDrawItem *item;

@end
