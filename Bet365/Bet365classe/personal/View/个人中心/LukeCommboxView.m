//
//  LukeCommboxView.m
//  Bet365
//
//  Created by luke on 17/6/29.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeCommboxView.h"

static NSString * const cellID = @"cell";
@interface LukeCommboxView ()<UITableViewDelegate,UITableViewDataSource>
/**  */
@property (nonatomic,weak) UITableView *tableView;
/**  */
@property (nonatomic,strong) NSArray *datas;

@end

@implementation LukeCommboxView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    UITableView *tableView = [[UITableView alloc] init];
    tableView.layer.borderColor = JesseGrayColor(224).CGColor;
    tableView.layer.borderWidth = 1;
    tableView.layer.cornerRadius = 2;
    tableView.layer.masksToBounds = YES;
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.bounces = NO;
    tableView.tableFooterView = [UIView new];
    tableView.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0);
    tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    tableView.rowHeight = 30;
    [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellID];
    [self addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    self.tableView = tableView;
}

- (void)setTitleDatas:(NSArray *)titleDatas
{
    self.datas = titleDatas;
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.datas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    cell.textLabel.text = self.datas[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.boxBlock) {
        self.boxBlock(indexPath.row);
    }
}

@end
