//
//  LukeSpreadCell.m
//  Bet365
//
//  Created by luke on 17/6/30.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeSpreadCell.h"
#import "LukeSpreadItem.h"


@implementation LukeSpreadCell
{
    UILabel *linkLabel;
    UILabel *spreadTypeNameLb;
    UILabel *visitCountLb;
    UILabel *userTypeLb;
    UILabel *registCountLb;
    UILabel *addTimeLb;
    UILabel *codeLb;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.contentView.backgroundColor = [UIColor skinViewBgColor];
    
    linkLabel = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 16 color:[UIColor skinTextItemNorColor] textAlignment:NSTextAlignmentLeft title:nil];
    linkLabel.numberOfLines = 2;
    [linkLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.leading.equalTo(self.contentView).offset(10);
        make.trailing.equalTo(self.contentView).offset(-10);
    }];
    
    spreadTypeNameLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 16 color:[UIColor skinTextItemNorColor] textAlignment:NSTextAlignmentLeft title:nil];
    [spreadTypeNameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(linkLabel.mas_bottom).offset(10);
        make.left.mas_equalTo(self.contentView.mas_left).offset(WIDTH*0.5);
    }];
    
    codeLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 16 color:[UIColor skinTextItemNorColor] textAlignment:NSTextAlignmentLeft title:nil];
    codeLb.numberOfLines = 0;
    codeLb.lineBreakMode = NSLineBreakByCharWrapping;
    [codeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(linkLabel.mas_bottom).offset(10);
        make.leading.equalTo(linkLabel);
        make.trailing.equalTo(spreadTypeNameLb.mas_leading).offset(-5);
    }];
    
    visitCountLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 16 color:[UIColor skinTextItemNorColor] textAlignment:NSTextAlignmentLeft title:nil];
    [visitCountLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(codeLb.mas_bottom).offset(10);
        make.leading.equalTo(linkLabel.mas_leading);
    }];
    
    userTypeLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 16 color:[UIColor skinTextItemNorColor] textAlignment:NSTextAlignmentLeft title:nil];
    [userTypeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(visitCountLb.mas_top);
        make.leading.equalTo(self.contentView).offset(WIDTH * 0.5);
    }];
    
    registCountLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 16 color:[UIColor skinTextItemNorColor] textAlignment:NSTextAlignmentLeft title:nil];
    [registCountLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(visitCountLb.mas_bottom).offset(10);
        make.leading.equalTo(linkLabel.mas_leading);
    }];
    
    addTimeLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 16 color:[UIColor skinTextItemNorColor] textAlignment:NSTextAlignmentLeft title:nil];
    [addTimeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(registCountLb.mas_bottom).offset(10);
        make.leading.equalTo(self.contentView).offset(10);
    }];
    
    NSMutableArray *temp = [NSMutableArray array];
    NSArray *titlesArr = @[@"复制地址",@"二维码",@"编辑",@"删除"];
    for (int i = 0; i < titlesArr.count; i ++) {
        UIButton *btn = [self addButtonWithTitle:titlesArr[i] font:IS_IPHONE_5s ? 14 : 16 color:[UIColor skinTextItemNorColor] backgroundColor:nil target:self action:@selector(btnClick:)];
        btn.layer.cornerRadius = 3;
        btn.layer.masksToBounds = YES;
        btn.layer.borderWidth = 1;
        btn.layer.borderColor = [UIColor skinTextItemNorSubColor].CGColor;
        btn.tag = i + 2222;
        if (i == 3) {
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];

//            btn.backgroundColor = [UIColor skinViewKitSelColor];
        }
        [temp addObject:btn];
    }
    [temp mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:2 leadSpacing:10 tailSpacing:10];
    [temp mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(addTimeLb.mas_bottom).offset(5);
        make.height.mas_equalTo(30);
        make.bottom.equalTo(self.contentView).offset(-5);
    }];
}

- (void)setItem:(LukeSpreadItem *)item
{
    _item = item;
    NSString *mainUrl = nil;
    if (BET_CONFIG.config.app_spread_domain.length > 0) {
        mainUrl = BET_CONFIG.config.app_spread_domain;
    }else if (item.externalUrl.length > 0) {
        mainUrl = item.externalUrl;
    }else{
        mainUrl = @"";
    }
    linkLabel.text = [NSString stringWithFormat:@"推广链接：%@/%ld",mainUrl,item.SpreaID];
    spreadTypeNameLb.text = [NSString stringWithFormat:@"推广页面：%@",item.spreadTypeName];
    visitCountLb.text = [NSString stringWithFormat:@"访问人数：%ld",item.visitCount];
    codeLb.text = [NSString stringWithFormat:@"推广码：%@",item.code];
    if (item.userType == 0) {
        userTypeLb.text = @"开户类型：会员";
    }else{
        userTypeLb.text = @"开户类型：代理";
    }
    registCountLb.text = [NSString stringWithFormat:@"注册人数：%ld",item.registCount];
    addTimeLb.text = [NSString stringWithFormat:@"生成时间：%@",item.addTime];
}

- (void)btnClick:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(LukeSpreadCellSelectButtonWithItem:index:)]) {
        [self.delegate LukeSpreadCellSelectButtonWithItem:self.item index:sender.tag - 2222];
    }
}

#pragma mark - 创建button
- (UIButton *)addButtonWithTitle:(NSString *)title font:(CGFloat)font color:(UIColor *)color backgroundColor:(UIColor *)backgroundColor target:(id)target action:(SEL)action
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font = [UIFont systemFontOfSize:font];
    [btn setTitle:title forState:UIControlStateNormal];
    btn.backgroundColor = backgroundColor;
    [btn setTitleColor:color forState:UIControlStateNormal];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:btn];
    return btn;
}

//创建label
- (UILabel *)addLabelWithFont:(CGFloat)font color:(UIColor *)color textAlignment:(NSTextAlignment)textAlignment title:(NSString *)title
{
    UILabel *label = [[UILabel alloc] init];
    label.text = title;
    label.textColor = color;
    label.font = [UIFont systemFontOfSize:font];
    label.textAlignment = textAlignment;
    [self.contentView addSubview:label];
    return label;
}

@end
