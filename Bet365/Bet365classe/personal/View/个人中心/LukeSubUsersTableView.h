//
//  LukeSubUsersTableView.h
//  Bet365
//
//  Created by luke on 17/7/9.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LukeSubUsersItem;

@protocol LukeSubUsersTableViewDelegate <NSObject>

- (void)SubUsersTableViewSelectWithIndex:(NSInteger)index;

- (void)SubUsersTableViewCancelClick;

@end

@interface LukeSubUsersTableView : UIView

- (instancetype)initWithFrame:(CGRect)frame item:(LukeSubUsersItem *)item account:(LukeSubUsersItem *)account;
/** 代理 */
@property (nonatomic,weak) id<LukeSubUsersTableViewDelegate>delegate;

@end
