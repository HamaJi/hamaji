//
//  LukePromotionView.h
//  Bet365
//
//  Created by luke on 17/6/28.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LukePromotionViewDelegate <NSObject>

- (void)reloadLukeRebatesViewController;

@end
@interface LukePromotionView : UIView
/** 代理 */
@property (nonatomic,weak) id<LukePromotionViewDelegate>delegete;

@end
