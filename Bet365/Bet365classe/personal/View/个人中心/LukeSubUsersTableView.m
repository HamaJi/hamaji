//
//  LukeSubUsersTableView.m
//  Bet365
//
//  Created by luke on 17/7/9.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeSubUsersTableView.h"
#import "LukeSubUsersItem.h"

@interface LukeSubUsersTableView()

@end

@implementation LukeSubUsersTableView
- (instancetype)initWithFrame:(CGRect)frame item:(LukeSubUsersItem *)item account:(LukeSubUsersItem *)account
{
    if (self = [super initWithFrame:frame]) {
        [self setupUIWithItem:item account:account];
    }
    return self;
}

- (void)setupUIWithItem:(LukeSubUsersItem *)item account:(LukeSubUsersItem *)account
{
    self.backgroundColor = [JesseGrayColor(10) colorWithAlphaComponent:0.3];
    NSArray *titleArrs = nil;
    if ([BET_CONFIG.config.site_rebate_model isEqualToString:@"0"] || !BET_CONFIG.config.site_rebate_model) {
        if ([item.account isEqualToString:USER_DATA_MANAGER.userInfoData.account]) {
            titleArrs = @[@"会员详情",@"团队总览",@"帐变记录"];
        }else if (!account && item.isDl) {
            titleArrs = @[@"会员详情",@"团队总览",@"帐变记录",@"返点设定",@"查看下级"];
        }else{
            titleArrs = @[@"会员详情",@"团队总览",@"帐变记录",@"返点设定"];
        }
    }else{
        if ([item.account isEqualToString:USER_DATA_MANAGER.userInfoData.account]) {
            titleArrs = @[@"会员详情",@"团队总览",@"帐变记录"];
        }else if (!account && item.isDl) {
            titleArrs = @[@"会员详情",@"团队总览",@"帐变记录",@"查看下级"];
        }else{
            titleArrs = @[@"会员详情",@"团队总览",@"帐变记录"];
        }
    }

    UIView *buttonView = [[UIView alloc] init];
    buttonView.backgroundColor = JesseGrayColor(238);
    [self addSubview:buttonView];
    UIButton *lastBtn = nil;
    UIView *lastLineView = nil;
    for (int i = 0; i < titleArrs.count; i ++) {
        UIButton *btn = [UIButton addButtonWithTitle:titleArrs[i] font:17 color:[UIColor blackColor] target:self action:@selector(btnClick:)];
        btn.backgroundColor = [UIColor whiteColor];
        btn.tag = 11111 + i;
        [buttonView addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(buttonView);
            make.height.mas_equalTo(40);
            if (lastLineView) {
                make.top.equalTo(lastLineView.mas_bottom);
            }else{
                make.top.equalTo(buttonView);
            }
        }];
        
        if (i < titleArrs.count - 1) {
            UIView *lineView = [self addLineView];
            [buttonView addSubview:lineView];
            [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.leading.trailing.equalTo(buttonView);
                make.top.equalTo(btn.mas_bottom);
                make.height.mas_equalTo(1);
            }];
            lastLineView = lineView;
        }
        lastBtn = btn;
    }
    
    UIButton *cancelBtn = [UIButton addButtonWithTitle:@"取消" font:17 color:[UIColor blackColor] target:self action:@selector(cancelClick)];
    cancelBtn.backgroundColor = [UIColor whiteColor];
    [buttonView addSubview:cancelBtn];
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lastBtn.mas_bottom).offset(10);
        make.leading.trailing.equalTo(buttonView);
        make.height.mas_equalTo(40);
        make.bottom.equalTo(buttonView.mas_bottom).offset(-SafeAreaBottomHeight);
    }];
    
    [buttonView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.bottom.equalTo(self);
    }];
}

- (void)cancelClick
{
    if ([self.delegate respondsToSelector:@selector(SubUsersTableViewCancelClick)]) {
        [self.delegate SubUsersTableViewCancelClick];
    }
}

- (void)btnClick:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(SubUsersTableViewSelectWithIndex:)]) {
        [self.delegate SubUsersTableViewSelectWithIndex:sender.tag - 11111];
    }
}

//创建线条
- (UIView *)addLineView
{
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = JesseGrayColor(238);
    [self addSubview:lineView];
    return lineView;
}
@end
