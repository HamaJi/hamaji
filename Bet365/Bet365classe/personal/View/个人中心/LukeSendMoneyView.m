//
//  LukeSendMoneyView.m
//  Bet365
//
//  Created by luke on 17/7/9.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeSendMoneyView.h"
#import "LukeSubUsersItem.h"
#import "LukeRegisterLbTFView.h"
#import "NetWorkMannager+Account.h"

@interface LukeSendMoneyView ()
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *accountView;
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *fanshuiView;
/**  */
@property (nonatomic,weak) UIView *contentView;
/** 处理后的数组 */
@property (nonatomic,strong) NSMutableArray *rebateArrs;
/**  */
@property (nonatomic,strong) LukeSubUsersItem *item;

@end

@implementation LukeSendMoneyView

- (instancetype)initWithFrame:(CGRect)frame item:(LukeSubUsersItem *)item
{
    if (self = [super initWithFrame:frame]) {
        self.item = item;
        [self setupUIWithItem:item];
    }
    return self;
}

- (void)setupUIWithItem:(LukeSubUsersItem *)item
{
    self.backgroundColor = [JesseGrayColor(10) colorWithAlphaComponent:0.3];
    UIView *contentView = [[UIView alloc] init];
    contentView.backgroundColor = JesseGrayColor(238);
    [self addSubview:contentView];
    self.contentView = contentView;
    
    LukeRegisterLbTFView *accountView = [LukeRegisterLbTFView registerLbBtnViewWithTitle:@"用户名" btnImage:nil btnTitle:item.account];
    [contentView addSubview:accountView];
    [accountView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(contentView);
        make.leading.trailing.equalTo(contentView);
        make.height.mas_equalTo(40);
    }];
    self.accountView = accountView;
    
    LukeRegisterLbTFView *fanshuiView = [LukeRegisterLbTFView registerLbBtnViewWithTitle:@"返水比率：" btnImage:@"right_lage" btnTitle:nil];
    fanshuiView.callBlock = ^{
        [self getRebateData];
    };
    [contentView addSubview:fanshuiView];
    [fanshuiView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(accountView.mas_bottom);
        make.leading.trailing.equalTo(contentView);
        make.height.mas_equalTo(40);
    }];
    self.fanshuiView = fanshuiView;
    
    UIButton *sureBtn = [UIButton addButtonWithTitle:@"修改" font:17 color:[UIColor whiteColor] target:self action:@selector(sureClick)];
    sureBtn.backgroundColor = [UIColor redColor];
    sureBtn.layer.cornerRadius = 5;
    sureBtn.layer.masksToBounds = YES;
    [contentView addSubview:sureBtn];
    [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(fanshuiView.mas_bottom).offset(100);
        make.leading.equalTo(contentView).offset(10);
        make.trailing.equalTo(contentView).offset(-10);
        make.height.mas_equalTo(40);
        make.bottom.mas_equalTo(contentView.mas_bottom).offset(-10 - SafeAreaBottomHeight);
    }];
    
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.bottom.equalTo(self);
    }];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn addTarget:self action:@selector(cancelClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cancelBtn];
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.top.equalTo(self);
        make.bottom.equalTo(contentView.mas_top);
    }];
}

- (void)getRebateData
{
    [SVProgressHUD show];
    [NET_DATA_MANAGER requestGetRebateRangeWithUserId:self.item.userId Success:^(id responseObject) {
        NSArray *rebeateArr = (NSArray *)responseObject;
        [self.rebateArrs removeAllObjects];
        if (rebeateArr.count > 0) {
            for (int i = [[rebeateArr lastObject] floatValue] * 10; i >= [[rebeateArr firstObject] floatValue] * 10; i --) {
                [self.rebateArrs addObject:[[NSString stringWithFormat:@"%d.%d",(i / 10),(i % 10)] mutableCopy]];
            }
            [BRStringPickerView showStringPickerWithTitle:@"返水比率" dataSource:self.rebateArrs defaultSelValue:nil isAutoSelect:NO themeColor:[UIColor skinNaviBgColor] resultBlock:^(id selectValue) {
                [self.fanshuiView.dateBtn setTitle:selectValue forState:UIControlStateNormal];
            }];
        }
        [SVProgressHUD dismiss];
    } failure:^(NSError *error) {
        NSLog(@"%@",error.msg);
        [SVProgressHUD dismiss];
    }];
}

- (NSMutableArray *)rebateArrs
{
    if (_rebateArrs == nil) {
        _rebateArrs = [NSMutableArray array];
    }
    return _rebateArrs;
}

- (void)cancelClick
{
    if ([self.delegate respondsToSelector:@selector(sendMoneyCancelClick)]) {
        [self.delegate sendMoneyCancelClick];
    }
}

- (void)sureClick
{
    if ([self.delegate respondsToSelector:@selector(sendMoneyLoadWithAccount:rebate:)]) {
        [self.delegate sendMoneyLoadWithAccount:self.item.userId rebate:self.fanshuiView.dateBtn.currentTitle];
    }
}
@end
