//
//  LukeRecordCell.m
//  Bet365
//
//  Created by luke on 17/6/21.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeTraceCell.h"
#import "LukeTraceItem.h"
#import "AppDelegate.h"
#import "KFCPHomeGameJsonModel.h"

@interface LukeTraceCell ()

@end

@implementation LukeTraceCell
{
    UIView *lottteryView;
    UILabel *cateNameLb;
    UILabel *timeLabel;
    UILabel *oddsNameLb;
    UILabel *openNumLb;
    UILabel *turnNumLb;
    UILabel *finishCountLb;
    UILabel *totalMoneyLb;
    UILabel *modifiedLb;
    UILabel *resultLb;
    UILabel *userNameLb;
    UILabel *orderNoLb;
    UILabel *betInfoLb;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    lottteryView = [[UIView alloc] init];
    [self.contentView addSubview:lottteryView];
    [lottteryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    
    orderNoLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [orderNoLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lottteryView).offset(10);
        make.leading.equalTo(lottteryView).offset(10);
    }];
    
    cateNameLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [cateNameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(orderNoLb.mas_bottom).offset(10);
        make.leading.equalTo(lottteryView).offset(10);
    }];
    
    timeLabel = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cateNameLb.mas_top);
        make.leading.equalTo(lottteryView).offset(WIDTH * 0.5);
    }];
    
    oddsNameLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [oddsNameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cateNameLb.mas_bottom).offset(10);
        make.leading.equalTo(cateNameLb.mas_leading);
    }];
    
    modifiedLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [modifiedLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(oddsNameLb.mas_top);
        make.leading.equalTo(timeLabel.mas_leading);
    }];
    
    turnNumLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [turnNumLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(oddsNameLb.mas_bottom).offset(10);
        make.leading.equalTo(cateNameLb.mas_leading);
    }];
    
    userNameLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [userNameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(turnNumLb.mas_top);
        make.leading.equalTo(timeLabel.mas_leading);
    }];

    openNumLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [openNumLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(turnNumLb.mas_bottom).offset(10);
        make.leading.equalTo(orderNoLb.mas_leading);
    }];
    
    totalMoneyLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [totalMoneyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(openNumLb.mas_top);
        make.leading.equalTo(timeLabel.mas_leading);
    }];
    
    finishCountLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [finishCountLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(openNumLb.mas_bottom).offset(10);
        make.leading.equalTo(orderNoLb.mas_leading);
    }];
    
    resultLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [resultLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(finishCountLb.mas_top);
        make.leading.equalTo(timeLabel.mas_leading);
    }];
    
    betInfoLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    betInfoLb.numberOfLines = 0;
    [betInfoLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(finishCountLb.mas_bottom).offset(10);
        make.leading.equalTo(orderNoLb.mas_leading);
        make.trailing.equalTo(lottteryView).offset(-10);
        make.bottom.equalTo(lottteryView).offset(-10);
    }];
}

#pragma mark - 彩种玩法的处理
- (void)Getlottery
{
    NSString *str = nil;
    for (KFCPHomeGameJsonModel *model in LOTTERY_FACTORY.lotteryData) {
        if (self.item.gameId == [model.GameId integerValue]) {
            str = model.name;
        }
    }
    cateNameLb.text = [NSString stringWithFormat:@"彩种 %@",str];
}

- (void)GetStatusForLabel
{
    if (self.item.status == 0) {
        modifiedLb.text = @"状态 进行中";
    }else if (self.item.status == 1){
        modifiedLb.text = @"状态 已完成";
    }else{
        modifiedLb.text = @"状态 撤销";
    }
}

- (void)setItem:(LukeTraceItem *)item
{
    _item = item;
    [self Getlottery];
    [self GetStatusForLabel];
    timeLabel.text = item.addTime;
    oddsNameLb.text = StringFormatWithStr(@"游戏玩法 ", item.cateName);
    openNumLb.text = StringFormatWithStrInteger(@"追号期数 ", item.traceCount);
    turnNumLb.text = StringFormatWithStr(@"开始期数 ", item.turnNum);
    finishCountLb.text = StringFormatWithStrInteger(@"完成期数 ", item.finishCount);
    totalMoneyLb.attributedText = [NSMutableAttributedString setTitle:@"总投金额" colorTitle:StringFormatWithFloat(item.traceTotalMoney) color:[UIColor redColor]];
    resultLb.attributedText = [NSMutableAttributedString setTitle:@"完成金额" colorTitle:StringFormatWithFloat(item.finishMoney) color:[UIColor redColor]];
    userNameLb.text = StringFormatWithStr(@"用户 ", item.account);
    orderNoLb.text = StringFormatWithStr(@"注单编号 ", item.orderNo);
    betInfoLb.text = StringFormatWithStr(@"投注内容 ", item.betInfo);
}

//创建label
- (UILabel *)addLabelWithFont:(CGFloat)font color:(UIColor *)color title:(NSString *)title
{
    UILabel *label = [[UILabel alloc] init];
    label.text = title;
    label.textColor = color;
    label.font = [UIFont systemFontOfSize:font];
    [lottteryView addSubview:label];
    return label;
}

@end
