//
//  LukeScreenView.h
//  Bet365
//
//  Created by luke on 17/6/20.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LukeScreenViewDelegate <NSObject>

- (void)reloadRecordDataWithUser:(NSString *)user gameID:(NSString *)gameID model:(NSString *)model;
@optional
- (void)screenViewSwitchWithBool:(BOOL)sw;

- (void)screenViewCancelClick;

@end

@interface LukeScreenView : UIView
/** 代理 */
@property (nonatomic,weak) id<LukeScreenViewDelegate>delegate;

- (instancetype)initWithFrame:(CGRect)frame isDL:(BOOL)isDl isTouzhu:(BOOL)isTZ;

@end
