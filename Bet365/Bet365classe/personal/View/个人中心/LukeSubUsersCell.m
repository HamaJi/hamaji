//
//  LukeSubUsersCell.m
//  Bet365
//
//  Created by luke on 17/7/1.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeSubUsersCell.h"
#import "LukeSubUsersItem.h"
#import "LukeUserAdapter.h"

@implementation LukeSubUsersCell
{
    UILabel *accountLb;
    UILabel *isDlLb;
    UILabel *growthLevelLb;
    UILabel *moneyLb;
    UILabel *rebateLb;
    UILabel *stateLb;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    accountLb = [self addLabelWithFont:15 color:[UIColor blackColor] textAlignment:NSTextAlignmentCenter title:nil];
    isDlLb = [self addLabelWithFont:15 color:[UIColor skinTextItemNorColor] textAlignment:NSTextAlignmentCenter title:nil];
    growthLevelLb = [self addLabelWithFont:15 color:[UIColor skinTextItemNorColor] textAlignment:NSTextAlignmentCenter title:nil];
    moneyLb = [self addLabelWithFont:15 color:[UIColor skinTextItemNorColor] textAlignment:NSTextAlignmentCenter title:nil];
    rebateLb = [self addLabelWithFont:15 color:[UIColor skinTextItemNorColor] textAlignment:NSTextAlignmentCenter title:nil];
    stateLb = [self addLabelWithFont:15 color:[UIColor skinTextItemNorColor] textAlignment:NSTextAlignmentCenter title:nil];
    
    [self.contentView.subviews mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:0 leadSpacing:0 tailSpacing:0];
    [self.contentView.subviews mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.bottom.equalTo(self.contentView).offset(-10);
    }];
    
}

- (void)setItem:(LukeSubUsersItem *)item
{
    _item = item;
    accountLb.text = item.account;
    if (item.isDl) {
        isDlLb.text = @"代理";
        accountLb.textColor = [UIColor skinTextItemSelColor];
    }else{
        isDlLb.text = @"会员";
        accountLb.textColor = [UIColor skinTextItemNorColor];
    }
    growthLevelLb.text = StringFormatWithInteger(item.growthLevel);
    moneyLb.text = [LukeUserAdapter formatFloat:[NSString stringWithFormat:@"%f",item.money]];
    rebateLb.text = [NSString stringWithFormat:@"%.1f",item.rebate];
    if (item.state == 1) {
        stateLb.text = @"正常";
    }else if (item.state == 2){
        stateLb.text = @"冻结";
    }else{
        stateLb.text = @"停用";
    }
}

//创建label
- (UILabel *)addLabelWithFont:(CGFloat)font color:(UIColor *)color textAlignment:(NSTextAlignment)textAlignment title:(NSString *)title
{
    UILabel *label = [[UILabel alloc] init];
    label.numberOfLines = 0;
    label.text = title;
    label.textColor = color;
    label.font = [UIFont systemFontOfSize:font];
    label.textAlignment = textAlignment;
    [self.contentView addSubview:label];
    return label;
}

@end
