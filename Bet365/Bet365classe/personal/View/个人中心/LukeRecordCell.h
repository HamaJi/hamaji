//
//  LukeRecordCell.h
//  Bet365
//
//  Created by luke on 17/6/21.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>


@class LukeRecordItem;

@protocol CPRecordStreamlineCellDelegate <NSObject>

- (void)cpreloadCancelNumWithBool:(BOOL)isAdd;

@end

@interface CPRecordStreamlineCell : UITableViewCell
/** 模型 */
@property (nonatomic,strong) LukeRecordItem *item;
/** 代理 */
@property (nonatomic,weak) id<CPRecordStreamlineCellDelegate>delegate;

@end

@protocol LukeRecordCellDelegate <NSObject>

- (void)reloadCancelNumWithBool:(BOOL)isAdd;

@end

@interface LukeRecordCell : UITableViewCell
/** 模型 */
@property (nonatomic,strong) LukeRecordItem *item;
/** 代理 */
@property (nonatomic,weak) id<LukeRecordCellDelegate>delegate;

@end
