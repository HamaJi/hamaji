//
//  LukeReportMagCell.h
//  Bet365
//
//  Created by luke on 17/6/26.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LukeReportMagItem;

@interface LukeReportMagCell : UITableViewCell
/** 模型 */
@property (nonatomic,strong) LukeReportMagItem *item;

@end
