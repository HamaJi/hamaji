//
//  LukeReportMagSelectView.m
//  Bet365
//
//  Created by luke on 17/6/26.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeReportMagSelectView.h"
#import "LukeRegisterLbTFView.h"
#import "LukeUserAdapter.h"
#import "NetWorkMannager+Account.h"
#import "Bet365CpHistoryModel.h"

@interface LukeReportMagSelectView ()
/** 类型数组 */
@property (nonatomic,strong) NSArray *typeArrs;
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *accountView;
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *fanshuiView;
/**  */
@property (nonatomic,weak) UIView *contentView;
/**  */
@property (nonatomic,strong) NSMutableArray *tempArrs;
/**  */
@property (nonatomic,strong) NSArray *modelArrs;

@end

@implementation LukeReportMagSelectView
- (NSMutableArray *)tempArrs
{
    if (_tempArrs == nil) {
        _tempArrs = [NSMutableArray array];
        [self.modelArrs enumerateObjectsUsingBlock:^(Bet365TransListModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [_tempArrs addObject:[obj.desc mutableCopy]];
        }];
//        for (NSDictionary *dict in self.typeArrs) {
//            [_tempArrs addObject:[dict[@"value"] mutableCopy]];
//        }
    }
    return _tempArrs;
}

- (NSArray *)typeArrs
{
    if (_typeArrs == nil) {
        _typeArrs = @[@{@"value": @"转账充值", @"key" : @"1"},
                      @{@"value": @"线上支付", @"key" : @"2"},
                      @{@"value": @"彩票下注", @"key" : @"3"},
                      @{@"value": @"彩票派彩", @"key" : @"4"},
                      @{@"value": @"用户提款", @"key" : @"6"},
                      @{@"value": @"撤单返款", @"key" : @"7"},
                      @{@"value": @"提款失败", @"key" : @"8"},
                      @{@"value": @"退佣(分红)", @"key" : @"10"},
                      @{@"value": @"后台转入", @"key" : @"11"},
                      @{@"value": @"后台转出", @"key" : @"12"},
                      @{@"value": @"注册送彩", @"key" : @"16"},
                      @{@"value": @"代理返点", @"key" : @"17"},
                      @{@"value": @"人工存入", @"key" : @"18"},
                      @{@"value": @"给予返水", @"key" : @"19"},
                      @{@"value": @"活动优惠", @"key" : @"20"},
                      @{@"value": @"追号返款", @"key" : @"21"},
                      @{@"value": @"系统奖励", @"key" : @"22"},
                      @{@"value": @"代理日工资", @"key" : @"23"},
                      @{@"value": @"代理扶持", @"key" : @"24"},
                      @{@"value": @"其他充值", @"key" : @"25"},
                      @{@"value": @"打和返款", @"key" : @"26"},
                      @{@"value": @"冲销返水", @"key" : @"27"},
                      @{@"value": @"人工提出", @"key" : @"28"},
                      @{@"value": @"追号扣款", @"key" : @"29"},
                      @{@"value": @"优惠扣除", @"key" : @"30"},
                      @{@"value": @"其他扣除", @"key" : @"31"},
                      @{@"value": @"冲销派奖", @"key" : @"32"},
                      @{@"value": @"额度转入", @"key" : @"33"},
                      @{@"value": @"额度转出", @"key" : @"34"},
                      @{@"value": @"体育下注", @"key" : @"35"},
                      @{@"value": @"体育结算", @"key" : @"36"},
                      @{@"value": @"违规退还本金", @"key" : @"37"},
                      @{@"value": @"重新结算", @"key" : @"38"},
                      @{@"value": @"取消订单", @"key" : @"39"},
                      @{@"value": @"真人返点", @"key" : @"40"},
                      @{@"value": @"真人返点回收", @"key" : @"41"},
                      @{@"value": @"电竞王者荣耀下注", @"key" : @"42"},
                      @{@"value": @"电竞王者荣耀重新结算", @"key" : @"43"},
                      @{@"value": @"电竞王者荣耀取消订单", @"key" : @"44"}];
    }
    return _typeArrs;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self getTransList];
    }
    return self;
}

- (void)getTransList
{
    [SVProgressHUD showWithStatus:@"加载中..."];
    [NET_DATA_MANAGER requestGetTransListSuccess:^(id responseObject) {
        [SVProgressHUD dismiss];
        self.modelArrs = [Bet365TransListModel mj_objectArrayWithKeyValuesArray:responseObject];
        [self setupUI];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [SVProgressHUD dismiss];
    }];
}

- (void)setupUI
{
    self.backgroundColor = [JesseGrayColor(10) colorWithAlphaComponent:0.3];
    UIView *contentView = [[UIView alloc] init];
    contentView.backgroundColor = [UIColor skinViewScreenColor];
    [self addSubview:contentView];
    self.contentView = contentView;
    
    UILabel *lb = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:@"筛选条件"];
    [contentView addSubview:lb];
    [lb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(contentView).offset(10);
        make.leading.equalTo(contentView).offset(10);
        make.height.mas_equalTo(20);
    }];
    
    if (USER_DATA_MANAGER.userInfoData.isDl) {
        LukeRegisterLbTFView *accountView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"用户名" textEntry:NO placeholder:@"用户名" keyboardType:UIKeyboardTypeDefault];
        [contentView addSubview:accountView];
        [accountView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(lb.mas_bottom).offset(5);
            make.leading.trailing.equalTo(contentView);
            make.height.mas_equalTo(40);
        }];
        self.accountView = accountView;
    }
    
    LukeRegisterLbTFView *fanshuiView = [LukeRegisterLbTFView registerLbBtnViewWithTitle:@"类型" btnImage:@"right_lage" btnTitle:nil];
    fanshuiView.callBlock = ^{
        [BRStringPickerView showStringPickerWithTitle:@"类型" dataSource:self.tempArrs defaultSelValue:nil isAutoSelect:NO themeColor:[UIColor skinNaviBgColor] resultBlock:^(id selectValue) {
            [self.fanshuiView.dateBtn setTitle:selectValue forState:UIControlStateNormal];
        }];
    };
    [contentView addSubview:fanshuiView];
    [fanshuiView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (USER_DATA_MANAGER.userInfoData.isDl) {
            make.top.equalTo(self.accountView.mas_bottom);
        }else{
            make.top.equalTo(lb.mas_bottom).offset(5);
        }
        make.leading.trailing.equalTo(contentView);
        make.height.mas_equalTo(40);
    }];
    self.fanshuiView = fanshuiView;
    
    UIButton *sureBtn = [UIButton addButtonWithTitle:@"搜索" font:17 color:[UIColor skinViewKitNorColor] target:self action:@selector(sureClick)];
    [sureBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];

    sureBtn.layer.cornerRadius = 5;
    sureBtn.layer.masksToBounds = YES;
    [contentView addSubview:sureBtn];
    [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(fanshuiView.mas_bottom).offset(100);
        make.leading.equalTo(contentView).offset(10);
        make.trailing.equalTo(contentView).offset(-10);
        make.height.mas_equalTo(40);
        make.bottom.mas_equalTo(contentView.mas_bottom).offset(-10 - SafeAreaBottomHeight);
    }];
    
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.bottom.equalTo(self);
    }];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn addTarget:self action:@selector(cancelClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cancelBtn];
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.top.equalTo(self);
        make.bottom.equalTo(contentView.mas_top);
    }];
}

- (void)cancelClick
{
    if ([self.delegate respondsToSelector:@selector(ReportMagSelectViewCancelClick)]) {
        [self.delegate ReportMagSelectViewCancelClick];
    }
}

- (void)sureClick
{
    if (USER_DATA_MANAGER.userInfoData.isDl) {
        if (self.accountView.textfield.isFirstResponder && self.accountView.textfield.text.length > 0) {
            if (![LukeUserAdapter validateUserName:self.accountView.textfield.text]) {
                [SVProgressHUD showErrorWithStatus:@"输入的帐号有误，请重新输入！"];
                self.accountView.textfield.text = nil;
                return;
            }
        }
    }
    if ([self.delegate respondsToSelector:@selector(ReportMagSelectViewWithAccount:type:)]) {
        __block NSString *type = nil;
        [self.modelArrs enumerateObjectsUsingBlock:^(Bet365TransListModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.desc isEqualToString:self.fanshuiView.dateBtn.currentTitle]) {
                type = StringFormatWithInteger(obj.type);
                *stop = YES;
            }
        }];
//        [self.typeArrs enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//            if ([obj[@"value"] isEqualToString:self.fanshuiView.dateBtn.currentTitle]) {
//                type = obj[@"key"];
//                *stop = YES;
//            }
//        }];
        [self.delegate ReportMagSelectViewWithAccount:self.accountView.textfield.text type:type];
    }
}

@end
