//
//  LukeDrawCell.m
//  Bet365
//
//  Created by luke on 17/6/20.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeDrawCell.h"
#import "LukeDrawItem.h"
#import "LukeUserAdapter.h"

@implementation LukeDrawCell
{
    UILabel *startDateEndLb;
    UILabel *rechMoneyLb;
    UILabel *discountMoneyLb;
    UILabel *cpDmlLb;
    UILabel *sportsDmlLb;
    UILabel *videoDmlLb;
    UILabel *discountDmlLb;
    UILabel *discountDeductLb;
    UILabel *discountAdoptLb;
    UILabel *mormDmlLb;
    UILabel *relaxQuotaLb;
    UILabel *mormDeductLb;
    UILabel *mormAdoptLb;
    UILabel *allDeductLb;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.contentView.backgroundColor = [UIColor clearColor];
    
    UILabel *drawDateLabel = [self addLabelWithFont:15 color:[UIColor blackColor] title:@"存款日期区间"];
    [drawDateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.leading.equalTo(self.contentView).offset(10);
    }];
    
    startDateEndLb = [self addLabelWithFont:15 color:JesseGrayColor(85) title:nil];
    [startDateEndLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(drawDateLabel.mas_bottom).offset(10);
        make.leading.equalTo(self.contentView).offset(10);
    }];
    
    rechMoneyLb = [self addLabelWithFont:15 color:JesseGrayColor(85) title:nil];
    [rechMoneyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(startDateEndLb.mas_bottom).offset(10);
        make.leading.equalTo(self.contentView).offset(10);
    }];
    
    discountMoneyLb = [self addLabelWithFont:15 color:JesseGrayColor(85) title:nil];
    [discountMoneyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(startDateEndLb.mas_bottom).offset(10);
        make.leading.equalTo(self.contentView).offset(WIDTH * 0.5);
    }];
    
    UIView *dateLineView = [self addLineView];
    [dateLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(discountMoneyLb.mas_bottom).offset(5);
        make.leading.equalTo(self.contentView).offset(10);
        make.trailing.equalTo(self.contentView);
        make.height.mas_equalTo(1);
    }];
    
    UILabel *valueLabel = [self addLabelWithFont:15 color:[UIColor blackColor] title:@"有效投注"];
    [valueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(dateLineView.mas_bottom).offset(5);
        make.leading.equalTo(self.contentView).offset(10);
    }];

    cpDmlLb = [self addLabelWithFont:15 color:JesseGrayColor(85) title:nil];
    [cpDmlLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(valueLabel.mas_bottom).offset(10);
        make.leading.equalTo(self.contentView).offset(10);
    }];
    
    sportsDmlLb = [self addLabelWithFont:15 color:JesseGrayColor(85) title:nil];
    [sportsDmlLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(valueLabel.mas_bottom).offset(10);
        make.leading.equalTo(self.contentView).offset(WIDTH * 0.5);
    }];
    
    videoDmlLb = [self addLabelWithFont:15 color:JesseGrayColor(85) title:nil];
    [videoDmlLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cpDmlLb.mas_bottom).offset(10);
        make.leading.equalTo(cpDmlLb);
    }];
    
    UIView *valueLineView = [self addLineView];
    [valueLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(videoDmlLb.mas_bottom).offset(5);
        make.leading.equalTo(self.contentView).offset(10);
        make.trailing.equalTo(self.contentView);
        make.height.mas_equalTo(1);
    }];
    
    UILabel *discountLb = [self addLabelWithFont:15 color:[UIColor blackColor] title:@"优惠流水审核"];
    [discountLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(valueLineView.mas_bottom).offset(5);
        make.leading.equalTo(self.contentView).offset(10);
    }];
    
    discountDmlLb = [self addLabelWithFont:15 color:JesseGrayColor(85) title:nil];
    [discountDmlLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(discountLb.mas_bottom).offset(10);
        make.leading.equalTo(self.contentView).offset(10);
    }];
    
    discountDeductLb = [self addLabelWithFont:15 color:JesseGrayColor(85) title:nil];
    [discountDeductLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(discountDmlLb.mas_bottom).offset(10);
        make.leading.equalTo(discountDmlLb);
    }];
    
    discountAdoptLb = [self addLabelWithFont:15 color:JesseGrayColor(85) title:nil];
    [discountAdoptLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(discountDeductLb.mas_top);
        make.leading.equalTo(self.contentView).offset(WIDTH * 0.5);
    }];
    
    UIView *discountLineView = [self addLineView];
    [discountLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(discountAdoptLb.mas_bottom).offset(5);
        make.leading.equalTo(self.contentView).offset(10);
        make.trailing.equalTo(self.contentView);
        make.height.mas_equalTo(1);
    }];
    
    UILabel *normalLb = [self addLabelWithFont:15 color:[UIColor blackColor] title:@"常态流水审核"];
    [normalLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(discountLineView.mas_bottom).offset(5);
        make.leading.equalTo(self.contentView).offset(10);
    }];
    
    mormDmlLb = [self addLabelWithFont:15 color:JesseGrayColor(85) title:nil];
    [mormDmlLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(normalLb.mas_bottom).offset(10);
        make.leading.equalTo(self.contentView).offset(10);
    }];
    
    relaxQuotaLb = [self addLabelWithFont:15 color:JesseGrayColor(85) title:nil];
    [relaxQuotaLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(mormDmlLb.mas_top);
        make.leading.equalTo(self.contentView).offset(WIDTH * 0.5);
    }];
    
    mormDeductLb = [self addLabelWithFont:15 color:JesseGrayColor(85) title:nil];
    [mormDeductLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(mormDmlLb.mas_bottom).offset(10);
        make.leading.equalTo(self.contentView).offset(10);
    }];
    
    mormAdoptLb = [self addLabelWithFont:15 color:JesseGrayColor(85) title:nil];
    [mormAdoptLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(mormDeductLb.mas_top);
        make.leading.equalTo(self.contentView).offset(WIDTH * 0.5);
    }];
    
    UIView *deductLineView = [self addLineView];
    [deductLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(mormAdoptLb.mas_bottom).offset(5);
        make.leading.equalTo(self.contentView).offset(10);
        make.trailing.equalTo(self.contentView);
        make.height.mas_equalTo(1);
    }];
    
    allDeductLb = [self addLabelWithFont:15 color:[UIColor blackColor] title:nil];
    [allDeductLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(deductLineView.mas_bottom).offset(5);
        make.leading.equalTo(self.contentView).offset(10);
        make.bottom.equalTo(self.contentView).offset(-10);
    }];
}

- (void)setItem:(LukeDrawItem *)item
{
    _item = item;
    startDateEndLb.text = [NSString stringWithFormat:@"%@ ~ %@",item.beginTime,item.endTime];
    rechMoneyLb.text = [NSString stringWithFormat:@"存款金额 %ld",item.rechMoney];
    discountMoneyLb.text = [NSString stringWithFormat:@"存款优惠 %ld",item.discountMoney];
    
    cpDmlLb.text = [NSString stringWithFormat:@"彩票 %ld",item.cpDml];
    sportsDmlLb.text = [NSString stringWithFormat:@"体育 %ld",item.sportsDml];
    videoDmlLb.text = [NSString stringWithFormat:@"真人 %ld",item.videoDml];
    
    discountDmlLb.text = [NSString stringWithFormat:@"优惠打码量 %ld",item.discountDml];
    discountDeductLb.text = [NSString stringWithFormat:@"优惠扣除金额 %ld",item.discountDeduct];
    NSString *AdoptStr = @"状态";
    if (item.discountAdopt) {
        NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ 通过",AdoptStr]];
        [attStr setAttributes:@{NSForegroundColorAttributeName : JesseColor(12, 134, 12)} range:NSMakeRange(3, attStr.length - 3)];
        discountAdoptLb.attributedText = attStr;
    }else{
        NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ 不通过",AdoptStr]];
        [attStr setAttributes:@{NSForegroundColorAttributeName : [UIColor redColor]} range:NSMakeRange(3, attStr.length - 3)];
        discountAdoptLb.attributedText = attStr;
    }
    
    mormDmlLb.text = [NSString stringWithFormat:@"常态打码量 %ld",item.mormDml];
    relaxQuotaLb.text = [NSString stringWithFormat:@"放宽额度 %ld",item.relaxQuota];
    mormDeductLb.text = [NSString stringWithFormat:@"常态扣除金额 %ld",item.mormDeduct];
    if (item.mormAdopt) {
        NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ 通过",AdoptStr]];
        [attStr setAttributes:@{NSForegroundColorAttributeName : JesseColor(12, 134, 12)} range:NSMakeRange(3, attStr.length - 3)];
        mormAdoptLb.attributedText = attStr;
    }else{
        NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ 不通过",AdoptStr]];
        [attStr setAttributes:@{NSForegroundColorAttributeName : [UIColor redColor]} range:NSMakeRange(3, attStr.length - 3)];
        mormAdoptLb.attributedText = attStr;
    }
    
    allDeductLb.text = [NSString stringWithFormat:@"共扣除余额 %ld",item.allDeduct];
}

- (UIView *)addLineView
{
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = JesseGrayColor(247);
    [self.contentView addSubview:lineView];
    return lineView;
}
//创建label
- (UILabel *)addLabelWithFont:(CGFloat)font color:(UIColor *)color title:(NSString *)title
{
    UILabel *label = [[UILabel alloc] init];
    label.text = title;
    label.textColor = color;
    label.font = [UIFont systemFontOfSize:font];
    [self.contentView addSubview:label];
    return label;
}
@end
