//
//  LukeRecordCell.m
//  Bet365
//
//  Created by luke on 17/6/21.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeRecordCell.h"
#import "LukeRecordItem.h"
#import "LukeUserAdapter.h"
#import "AppDelegate.h"
#import "KFCPHomeGameJsonModel.h"

#import "UIColor+extenColor.h"

@implementation CPRecordStreamlineCell
{
    UIButton *deleteBtn;
    UIView *lottteryView;
    UILabel *oddsNameLb;
    UILabel *totalMoneyLb;
    UILabel *resultLb;
    UILabel *turnNumLb;
}

- (void)delegaClick:(UIButton *)sender
{
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.item.isSelectCancel = YES;
    }else{
        self.item.isSelectCancel = NO;
    }
    if ([self.delegate respondsToSelector:@selector(cpreloadCancelNumWithBool:)]) {
        [self.delegate cpreloadCancelNumWithBool:sender.selected];
    }
}

- (void)setupUI
{
    deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteBtn.hidden = YES;
    [deleteBtn setImage:[UIImage imageNamed:@"register_disagree"] forState:UIControlStateNormal];
    [deleteBtn setImage:[UIImage imageNamed:@"register_agree"] forState:UIControlStateSelected];
    [deleteBtn addTarget:self action:@selector(delegaClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:deleteBtn];
    [deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.leading.equalTo(self.contentView).offset(10);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    lottteryView = [[UIView alloc] init];
    [self.contentView addSubview:lottteryView];
    [lottteryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    
    turnNumLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [turnNumLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lottteryView).offset(10);
        make.leading.equalTo(lottteryView).offset(10);
    }];
    
    totalMoneyLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [totalMoneyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(turnNumLb.mas_top);
        make.leading.equalTo(lottteryView).offset(WIDTH * 0.5);
    }];
    
    oddsNameLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [oddsNameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(turnNumLb.mas_bottom).offset(10);
        make.leading.equalTo(turnNumLb.mas_leading);
        make.bottom.equalTo(lottteryView).offset(-10);
    }];
    
    resultLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [resultLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(oddsNameLb.mas_top);
        make.leading.equalTo(totalMoneyLb.mas_leading);
    }];
}

#pragma mark - 撤销操作的处理
- (void)handleCancel
{
    if (self.item.isCancel) {
        if (self.item.status == 0 && [LukeUserAdapter timeCompareWithOtherTime:self.item.betEndTime] == NSOrderedAscending) {
            deleteBtn.hidden = NO;
        }else{
            deleteBtn.hidden = YES;
        }
        lottteryView.transform = CGAffineTransformMakeTranslation(30, 0);
    }else{
        deleteBtn.hidden = YES;
        lottteryView.transform = CGAffineTransformIdentity;
    }
    
    if (self.item.isSelectCancel) {
        deleteBtn.selected = YES;
    }else{
        deleteBtn.selected = NO;
    }
}

- (void)setItem:(LukeRecordItem *)item
{
    _item = item;
    [self handleCancel];
    oddsNameLb.text = [NSString stringWithFormat:@"玩法 %@",item.cateName];
    turnNumLb.text = [NSString stringWithFormat:@"奖期 %@",item.turnNum];
    totalMoneyLb.attributedText = [self getStrWithTitle:@"投注金额 " lastStr:[LukeUserAdapter formatFloat:[NSString stringWithFormat:@"%f",item.totalMoney]] color:JesseColor(89, 156, 34)];
    resultLb.attributedText = [self getStrWithTitle:@"中奖金额 " lastStr:[LukeUserAdapter formatFloat:[NSString stringWithFormat:@"%f",item.reward]] color:[UIColor redColor]];
}

#pragma mark -  富文本
- (NSMutableAttributedString *)getStrWithTitle:(NSString *)title lastStr:(NSString *)lastStr color:(UIColor *)color
{
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",title,lastStr]];
    [attStr setAttributes:@{NSForegroundColorAttributeName : color} range:NSMakeRange(title.length, lastStr.length)];
    return attStr;
}

//创建label
- (UILabel *)addLabelWithFont:(CGFloat)font color:(UIColor *)color title:(NSString *)title
{
    UILabel *label = [[UILabel alloc] init];
    label.text = title;
    label.textColor = color;
    label.font = [UIFont systemFontOfSize:font];
    [lottteryView addSubview:label];
    return label;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupUI];
    }
    return self;
}

@end

@interface LukeRecordCell ()

@end

@implementation LukeRecordCell
{
    UIButton *deleteBtn;
    UIView *lottteryView;
    UILabel *cateNameLb;
    UILabel *timeLabel;
    UILabel *oddsNameLb;
    UILabel *openNumLb;
    UILabel *totalMoneyLb;
    UILabel *modifiedLb;
    UILabel *resultLb;
    UILabel *userNameLb;
    UILabel *orderNoLb;
    UILabel *betInfoLb;
    UILabel *turnNumLb;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupUI];
    }
    return self;
}

- (void)delegaClick:(UIButton *)sender
{
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.item.isSelectCancel = YES;
    }else{
        self.item.isSelectCancel = NO;
    }
    if ([self.delegate respondsToSelector:@selector(reloadCancelNumWithBool:)]) {
        [self.delegate reloadCancelNumWithBool:sender.selected];
    }
}

- (void)setupUI
{
    deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteBtn.hidden = YES;
    [deleteBtn setImage:[UIImage imageNamed:@"register_disagree"] forState:UIControlStateNormal];
    [deleteBtn setImage:[UIImage imageNamed:@"register_agree"] forState:UIControlStateSelected];
    [deleteBtn addTarget:self action:@selector(delegaClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:deleteBtn];
    [deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.leading.equalTo(self.contentView).offset(10);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    lottteryView = [[UIView alloc] init];
    [self.contentView addSubview:lottteryView];
    [lottteryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    
    orderNoLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [orderNoLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lottteryView).offset(10);
        make.leading.equalTo(lottteryView).offset(10);
    }];
    
    cateNameLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [cateNameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(orderNoLb.mas_bottom).offset(10);
        make.leading.equalTo(lottteryView).offset(10);
    }];
    
    timeLabel = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cateNameLb.mas_top);
        make.leading.equalTo(lottteryView).offset(WIDTH * 0.5);
    }];
    
    oddsNameLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [oddsNameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cateNameLb.mas_bottom).offset(10);
        make.leading.equalTo(cateNameLb.mas_leading);
    }];
    
    turnNumLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [turnNumLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(oddsNameLb.mas_top);
        make.leading.equalTo(timeLabel.mas_leading);
    }];
    
    totalMoneyLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [totalMoneyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(oddsNameLb.mas_bottom).offset(10);
        make.leading.equalTo(cateNameLb.mas_leading);
    }];
    
    modifiedLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [modifiedLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(totalMoneyLb.mas_top);
        make.leading.equalTo(timeLabel.mas_leading);
    }];
    
    resultLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [resultLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(totalMoneyLb.mas_bottom).offset(10);
        make.leading.equalTo(cateNameLb.mas_leading);
    }];
    
    userNameLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [userNameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(resultLb.mas_top);
        make.leading.equalTo(timeLabel.mas_leading);
    }];
    
    openNumLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    [openNumLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(resultLb.mas_bottom).offset(10);
        make.leading.equalTo(orderNoLb.mas_leading);
        make.trailing.equalTo(lottteryView).offset(-10);
    }];
    
    betInfoLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 15 color:JesseGrayColor(85) title:nil];
    betInfoLb.numberOfLines = 2;
    [betInfoLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(openNumLb.mas_bottom).offset(10);
        make.leading.equalTo(cateNameLb.mas_leading);
        make.trailing.equalTo(lottteryView).offset(-10);
        make.bottom.equalTo(lottteryView).offset(-10);
    }];
}

#pragma mark - 彩种玩法的处理
- (void)Getlottery
{
    NSString *str = nil;
    for (KFCPHomeGameJsonModel *model in LOTTERY_FACTORY.lotteryData) {
        if (self.item.gameId == [model.GameId integerValue]) {
            str = model.name;
            break;
        }
    }
    if (self.item.model == 0) {
        if ([str isNoBlankString]) {
            cateNameLb.text = [NSString stringWithFormat:@"彩种 %@[官]",str];
        } else {
            cateNameLb.text = @"彩种 [官]";
        }
        
    }else{
        if ([str isNoBlankString]) {
            cateNameLb.text = [NSString stringWithFormat:@"彩种 %@[信]",str];
        } else {
            cateNameLb.text = @"彩种 [信]";
        }
    }
}

- (void)GetStatusForLabel
{
    
    if (self.item.status == 0 && [LukeUserAdapter timeCompareWithOtherTime:self.item.betEndTime] == NSOrderedAscending) {
        
        if (BET_CONFIG.config.hy_is_cancel && [BET_CONFIG.config.hy_is_cancel isEqualToString:@"1"]) {
            modifiedLb.attributedText = [self getStrWithTitle:@"状态 " lastStr:@"可撤销" color:[UIColor colorWithHexString:@"#2c77ba"]];
        }else{
            modifiedLb.attributedText = [self getStrWithTitle:@"状态 " lastStr:@"未开奖" color:[UIColor colorWithHexString:@"#e2bc03"]];
        }
    }else if (self.item.status == 1 && self.item.result == 0){
        modifiedLb.attributedText = [self getStrWithTitle:@"状态 " lastStr:@"未中奖" color:[UIColor colorWithHexString:@"#908e8e"]];
    }else if (self.item.status == 1 && self.item.result == 1){
        modifiedLb.attributedText = [self getStrWithTitle:@"状态 " lastStr:@"已派送" color:[UIColor colorWithHexString:@"#e71010"]];
    }else if (self.item.status == 1 && self.item.result == 2){
        modifiedLb.attributedText = [self getStrWithTitle:@"状态 " lastStr:@"和局" color:[UIColor colorWithHexString:@"#07d00b"]];
    }else if (self.item.status == 0){
        modifiedLb.attributedText = [self getStrWithTitle:@"状态 " lastStr:@"未开奖" color:[UIColor colorWithHexString:@"#e2bc03"]];
    }else if (self.item.status == 2){
        modifiedLb.attributedText = [self getStrWithTitle:@"状态 " lastStr:@"撤销" color:[UIColor colorWithHexString:@"#906944"]];
    }else if (self.item.status == 3){
        modifiedLb.attributedText = [self getStrWithTitle:@"状态 " lastStr:@"删除" color:[UIColor colorWithHexString:@"#b9cca6"]];
    }else if (self.item.status == 4){
        modifiedLb.attributedText = [self getStrWithTitle:@"状态 " lastStr:@"追号停止" color:[UIColor colorWithHexString:@"#b9cca6"]];
    }else{
        modifiedLb.text = @"状态";
    }
    
    [self handleCancel];
}

#pragma mark - 撤销操作的处理
- (void)handleCancel
{
    if (self.item.isCancel) {
        if (self.item.status == 0 && [LukeUserAdapter timeCompareWithOtherTime:self.item.betEndTime] == NSOrderedAscending) {
            deleteBtn.hidden = NO;
        }else{
            deleteBtn.hidden = YES;
        }
        lottteryView.transform = CGAffineTransformMakeTranslation(30, 0);
    }else{
        deleteBtn.hidden = YES;
        lottteryView.transform = CGAffineTransformIdentity;
    }
    
    if (self.item.isSelectCancel) {
        deleteBtn.selected = YES;
    }else{
        deleteBtn.selected = NO;
    }
}

- (void)setItem:(LukeRecordItem *)item
{
    _item = item;
    [self Getlottery];
    [self GetStatusForLabel];
    timeLabel.text = item.addTime;
    oddsNameLb.text = [NSString stringWithFormat:@"玩法 %@",item.cateName];
    if (item.openNum) {
        openNumLb.text = [NSString stringWithFormat:@"开奖号码 %@",item.openNum];
    }else{
        openNumLb.text = @"开奖号码";
    }
    turnNumLb.text = [NSString stringWithFormat:@"奖期 %@",item.turnNum];
    totalMoneyLb.attributedText = [self getStrWithTitle:@"投注金额 " lastStr:[LukeUserAdapter formatFloat:[NSString stringWithFormat:@"%f",item.totalMoney]] color:JesseColor(89, 156, 34)];
    resultLb.attributedText = [self getStrWithTitle:@"中奖金额 " lastStr:[LukeUserAdapter formatFloat:[NSString stringWithFormat:@"%f",item.reward]] color:[UIColor redColor]];
    userNameLb.text = [NSString stringWithFormat:@"用户 %@",item.userName];
    orderNoLb.text = [NSString stringWithFormat:@"注单编号 %@",item.orderNo];
    betInfoLb.text = [NSString stringWithFormat:@"投注内容 %@%@",item.betInfo,item.poschooseName.length > 0 ? [NSString stringWithFormat:@"(%@)",item.poschooseName] : @""];
}

#pragma mark -  富文本
- (NSMutableAttributedString *)getStrWithTitle:(NSString *)title lastStr:(NSString *)lastStr color:(UIColor *)color
{
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",title,lastStr]];
    [attStr setAttributes:@{NSForegroundColorAttributeName : color} range:NSMakeRange(title.length, lastStr.length)];
    return attStr;
}

//创建label
- (UILabel *)addLabelWithFont:(CGFloat)font color:(UIColor *)color title:(NSString *)title
{
    UILabel *label = [[UILabel alloc] init];
    label.numberOfLines = 0;
    label.text = title;
    label.textColor = color;
    label.font = [UIFont systemFontOfSize:font];
    [lottteryView addSubview:label];
    return label;
}

@end
