//
//  LukePushMessageCell.m
//  Bet365
//
//  Created by luke on 17/6/15.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukePushMessageCell.h"
#import "LukePushMessageItem.h"

@implementation LukePushMessageCell
{
    UIImageView *iconImage;
    UILabel *titleLabel;
    UILabel *subTitle;
    UILabel *timeLabel;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.contentView.backgroundColor = [UIColor skinViewBgColor];
    self.backgroundColor = [UIColor skinViewBgColor];
    
    UIImage *image = [[UIImage imageNamed:@"red"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];  // 设置根据TintColor渲染图片
    iconImage = [[UIImageView alloc] initWithImage:image];
    [iconImage setTintColor:[UIColor skinViewKitSelColor]];
    [self.contentView addSubview:iconImage];
    [iconImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.leading.equalTo(self.contentView).offset(10);
        make.size.mas_equalTo(CGSizeMake(10, 10));
    }];
    
    titleLabel = [self addLabelWithFont:17 color:[UIColor skinTextItemNorColor] textAlignment:NSTextAlignmentLeft];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(iconImage.mas_top);
        make.leading.equalTo(iconImage.mas_trailing).offset(10);
    }];
    
//    subTitle = [self addLabelWithFont:14 color:[UIColor blackColor] textAlignment:NSTextAlignmentLeft];
//    [subTitle mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.leading.equalTo(self.contentView).offset(WIDTH * 0.4);
//        make.top.equalTo(self.contentView).offset(5);
//        make.trailing.equalTo(self.contentView);
//    }];
    
    timeLabel = [self addLabelWithFont:14 color:[UIColor skinTextItemNorSubColor] textAlignment:NSTextAlignmentLeft];
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.contentView).offset(10);
        make.bottom.equalTo(self.contentView).offset(-10);
    }];
}

- (void)setItem:(LukePushMessageItem *)item
{
    _item = item;
    if (item.readStatus == 0) {
        [iconImage mas_updateConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(10, 10));
        }];
    }else{
        [iconImage mas_updateConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(0, 0));
        }];
    }
    titleLabel.text = item.messageTitle;
//    subTitle.text = item.messageContent;
    timeLabel.text = item.addTime;
}

- (UILabel *)addLabelWithFont:(CGFloat)font color:(UIColor *)color textAlignment:(NSTextAlignment)textAlignment
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = color;
    label.font = [UIFont systemFontOfSize:font];
    label.textAlignment = textAlignment;
    [self.contentView addSubview:label];
    return label;
}

@end
