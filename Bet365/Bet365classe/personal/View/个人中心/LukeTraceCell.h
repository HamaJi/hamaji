//
//  LukeTraceCell.h
//  Bet365
//
//  Created by luke on 17/6/24.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LukeTraceItem;

@interface LukeTraceCell : UITableViewCell

/** 模型 */
@property (nonatomic,strong) LukeTraceItem *item;

@end
