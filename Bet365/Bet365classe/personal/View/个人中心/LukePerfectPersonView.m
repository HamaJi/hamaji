//
//  LukePerfectPersonView.m
//  Bet365
//
//  Created by luke on 17/6/27.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukePerfectPersonView.h"
#import "LukeRegisterLbTFView.h"
#import "LukeUserAdapter.h"
#import "NetWorkMannager+Account.h"
#import "PersonEditLimitModel.h"
#import "NSString+MacRegexCategory.h"
#import "Bet365VerifyImagCodeViewController.h"
@interface LukePerfectPersonView ()
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *fullNameView;
/** 出生日期 */
@property (nonatomic,weak) LukeRegisterLbTFView *birthdayView;
/** 会员微信 */
@property (nonatomic,weak) LukeRegisterLbTFView *wechatView;
/** 会员电话 */
@property (nonatomic,weak) LukeRegisterLbTFView *phoneView;
/** 会员邮箱 */
@property (nonatomic,weak) LukeRegisterLbTFView *emailView;
/** 会员QQ */
@property (nonatomic,weak) LukeRegisterLbTFView *qqView;
/** 模型 */
@property (nonatomic,strong) PersonEditLimitModel *model;

/** 会员QQ */
@property (nonatomic,weak) LukeRegisterLbTFView *smsCodeView;

@end

@implementation LukePerfectPersonView
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor skinViewScreenColor];
        [self setupUI];
        [self queryPeronData];
        [self addRAC];
        
    }
    return self;
}

- (void)addRAC
{
    @weakify(self);
    [[RACObserve(self, model) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        [self.fullNameView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo([self.model.fullName intValue] != 2 ? 40 : 0);
        }];
        [self.birthdayView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo([self.model.birthday intValue] != 2 ? 40 : 0);
        }];
        [self.phoneView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo([self.model.phone intValue] != 2 ? 40 : 0);
        }];
        [self.smsCodeView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo([self.model.phone intValue] != 2 ? 40 : 0);
        }];
        [self.emailView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo([self.model.email intValue] != 2 ? 40 : 0);
        }];
        [self.qqView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo([self.model.qq intValue] != 2 ? 40 : 0);
        }];
        [self.wechatView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo([self.model.weixin intValue] != 2 ? 40 : 0);
        }];
    }];
}

- (void)queryPeronData
{
    [SVProgressHUD showWithStatus:@"加载数据中..."];
    [NET_DATA_MANAGER requestEditUserInfoLimitCache:nil Success:^(id responseObject) {
        [SVProgressHUD dismiss];
        self.model = [PersonEditLimitModel mj_objectWithKeyValues:responseObject];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [SVProgressHUD dismiss];
    }];
}

- (void)setupUI
{
    UILabel *titleLb = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:@"完善你的资料，方便客服人员为你提供更好的服务！"];
    titleLb.numberOfLines = 2;
    [self addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(10);
        make.leading.equalTo(self).offset(10);
        make.trailing.equalTo(self).offset(-10);
    }];
    
    LukeRegisterLbTFView *accountView = [LukeRegisterLbTFView registerLbBtnViewWithTitle:@"帐号" btnImage:nil btnTitle:USER_DATA_MANAGER.userInfoData.account];
    [self addSubview:accountView];
    [accountView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.equalTo(self);
        make.top.equalTo(titleLb.mas_bottom).offset(5);
        make.height.mas_equalTo(40);
    }];
    
    LukeRegisterLbTFView *fullNameView = [LukeRegisterLbTFView registerCreateTextFieldViewWithTitle:@"姓名" textEntry:NO placeholder:@"请输入姓名" keyboardType:UIKeyboardTypeDefault];
    fullNameView.clipsToBounds = YES;
    if ([USER_DATA_MANAGER.userInfoData.fullName isNoBlankString]) {
        fullNameView.textfield.text = USER_DATA_MANAGER.userInfoData.fullName;
        if (fullNameView.textfield.text.length) {
            fullNameView.textfield.enabled = NO;
        }
    }
    [self addSubview:fullNameView];
    [fullNameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(accountView.mas_bottom);
        make.leading.trailing.equalTo(self);
        make.height.mas_equalTo(0);
    }];
    self.fullNameView = fullNameView;
    
    LukeRegisterLbTFView *birthdayView = [LukeRegisterLbTFView registerLbBtnViewWithTitle:@"出生日期" btnImage:@"right_lage" btnTitle:nil];
    if ([USER_DATA_MANAGER.userInfoData.birthday isNoBlankString]) {
        [birthdayView.dateBtn setTitle:USER_DATA_MANAGER.userInfoData.birthday forState:UIControlStateNormal];
    }
    birthdayView.clipsToBounds = YES;
    @weakify(self);
    birthdayView.callBlock = ^{
        @strongify(self);
        if (![USER_DATA_MANAGER.userInfoData.birthday isNoBlankString]) {
            [self.viewController showDatePickerdefaultSelValue:nil minDate:nil resultBlock:^(NSString *selectValue) {
                [self.birthdayView.dateBtn setTitle:selectValue forState:UIControlStateNormal];
            }];
        }
    };
    [self addSubview:birthdayView];
    [birthdayView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(fullNameView.mas_bottom);
        make.leading.trailing.equalTo(self);
        make.height.mas_equalTo(0);
    }];
    self.birthdayView = birthdayView;
    
    LukeRegisterLbTFView *emailView = [LukeRegisterLbTFView registerCreateTextFieldViewWithTitle:@"邮箱" textEntry:NO placeholder:@"请输邮箱" keyboardType:UIKeyboardTypeDefault];
    emailView.clipsToBounds = YES;
    if ([USER_DATA_MANAGER.userInfoData.email isNoBlankString]) {
        NSMutableString *emailStr = [NSMutableString stringWithString:USER_DATA_MANAGER.userInfoData.email];
//        if (emailStr.length > 7) {
//            [emailStr replaceCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
//        }
        emailView.textfield.text = emailStr;
        if (emailView.textfield.text.length) {
            emailView.textfield.enabled = NO;
        }
    }
    [self addSubview:emailView];
    [emailView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(birthdayView.mas_bottom);
        make.leading.trailing.equalTo(self);
        make.height.mas_equalTo(0);
    }];
    self.emailView = emailView;
    
    LukeRegisterLbTFView *phoneView = [LukeRegisterLbTFView registerCreateTextFieldViewWithTitle:@"电话" textEntry:NO placeholder:@"请输入手机号码" keyboardType:UIKeyboardTypeNumberPad];
    phoneView.clipsToBounds = YES;
    if ([USER_DATA_MANAGER.userInfoData.phone isNoBlankString]) {
        NSMutableString *phoneStr = [NSMutableString stringWithString:USER_DATA_MANAGER.userInfoData.phone];
//        if (phoneStr.length > 7) {
//            [phoneStr replaceCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
//        }
        phoneView.textfield.text = phoneStr;
        if (phoneView.textfield.text.length) {
            phoneView.textfield.enabled = NO;
        }
    }
    [self addSubview:phoneView];
    [phoneView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(emailView.mas_bottom);
        make.leading.trailing.equalTo(self);
        make.height.mas_equalTo(0);
    }];
    self.phoneView = phoneView;
    
    self.smsCodeView = [LukeRegisterLbTFView registerCountdownBtnWithTitle:@"验证码" ResendTitile:@"重新发送验证码" TimeOutKey:@"" MaxCount:60 TapUpBlock:^BOOL{
        @strongify(self);
        [self endEditing:YES];
        [self showVerifyImageCode:@""];
//        [self verifyAuther:^(BOOL success) {
//            if (success) {
//                [self requestSMScode];
//            }
//        }];
        
        return NO;
    } vCodeSendingBlock:^(NSUInteger sendingSpacing) {
        
    } Placeholder:@"请输入验证码"];
    
    self.smsCodeView.layer.cornerRadius = 5;
    self.smsCodeView.layer.masksToBounds = YES;
    self.smsCodeView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
    self.smsCodeView.layer.borderWidth = 1;
    
    [self addSubview:self.smsCodeView];
    [self.smsCodeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(phoneView.mas_bottom);
        make.leading.trailing.equalTo(self);
        make.height.mas_equalTo(0);
    }];
    
    LukeRegisterLbTFView *qqView = [LukeRegisterLbTFView registerCreateTextFieldViewWithTitle:@"QQ" textEntry:NO placeholder:@"请输入QQ号" keyboardType:UIKeyboardTypeNumberPad];
    qqView.clipsToBounds = YES;
    if ([USER_DATA_MANAGER.userInfoData.qq isNoBlankString]) {
        NSMutableString *qqStr = [NSMutableString stringWithString:USER_DATA_MANAGER.userInfoData.qq];
//        if (qqStr.length > 7) {
//            [qqStr replaceCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
//        }
        qqView.textfield.text = qqStr;
        if (qqView.textfield.text.length) {
            qqView.textfield.enabled = NO;
        }
    }
    [self addSubview:qqView];
    [qqView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.smsCodeView.mas_bottom);
        make.leading.trailing.equalTo(self);
        make.height.mas_equalTo(0);
    }];
    self.qqView = qqView;
    
    LukeRegisterLbTFView *wechatView = [LukeRegisterLbTFView registerCreateTextFieldViewWithTitle:@"微信" textEntry:NO placeholder:@"请输入微信号" keyboardType:UIKeyboardTypeDefault];
    wechatView.clipsToBounds = YES;
    if ([USER_DATA_MANAGER.userInfoData.weixin isNoBlankString]) {
        NSMutableString *weixinStr = [NSMutableString stringWithString:USER_DATA_MANAGER.userInfoData.weixin];
//        if (weixinStr.length > 7) {
//            [weixinStr replaceCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
//        }
        wechatView.textfield.text = weixinStr;
        if (wechatView.textfield.text.length) {
            wechatView.textfield.enabled = NO;
        }
    }
    [self addSubview:wechatView];
    [wechatView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(qqView.mas_bottom);
        make.leading.trailing.equalTo(self);
        make.height.mas_equalTo(0);
    }];
    self.wechatView = wechatView;
    
    UIButton *sureBtn = [UIButton addButtonWithTitle:@"确认" font:15 color:[UIColor skinViewKitNorColor] target:self action:@selector(sureClick)];
    [sureBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];
    [self addSubview:sureBtn];
    sureBtn.layer.cornerRadius = 5;
    sureBtn.layer.masksToBounds = YES;
    [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(wechatView.mas_bottom).offset(40);
        make.centerX.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(WIDTH - 20, 40));
    }];
}

-(void)showVerifyImageCode:(NSString *)url{
    Bet365VerifyImagCodeViewController *vc = [[Bet365VerifyImagCodeViewController alloc] initWithNibName:@"Bet365VerifyImagCodeViewController" bundle:nil];
    [[NAVI_MANAGER getCurrentVC] presentPopViewController:vc completion:nil];
    vc.submitBlock = ^BOOL(NSString * _Nonnull code) {
        return YES;
    };
    vc.resetBlock = ^{
        
    };
    vc.imageUrl = url;
}

-(void)getModifyPhoneVerifyType:(void(^)())completed{
    
    [NET_DATA_MANAGER requestGetUserInfoVerifyType:@"" Success:^(id responseObject) {
        
    } failure:^(NSError *error) {
        
    }];
}
- (void)sureClick
{
    NSString *phoneStr = self.phoneView.textfield.text;
    NSString *emailStr = self.emailView.textfield.text;
    NSString *qqStr = self.qqView.textfield.text;
    NSString *wechatStr = self.wechatView.textfield.text;
    if ([self.model.fullName integerValue] == 2) {
        
    }else if ([self.model.fullName intValue] == 1 && self.fullNameView.textfield.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"姓名必填哦"];
        return;
    }else if (![LukeUserAdapter validateNickname:self.fullNameView.textfield.text] && self.fullNameView.textfield.text.length > 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入正确的姓名!"];
        return;
    }
    
    if ([self.model.birthday integerValue] == 2) {
        
    }else if ([self.model.birthday intValue] == 1 && self.birthdayView.dateBtn.currentTitle.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"出生日期必选哦"];
        return;
    }
    
    if ([self.model.phone integerValue] == 2) {
        
    }else if ([self.model.phone intValue] == 1 && self.phoneView.textfield.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"电话必填哦"];
        return;
    }else if (![LukeUserAdapter validateMobile:phoneStr] && phoneStr.length > 0) {
        if ([USER_DATA_MANAGER.userInfoData.phone isNoBlankString]) {
            
        }else{
            [SVProgressHUD showErrorWithStatus:@"输入的手机号码有误，请重新输入"];
            return;
        }
    }
    if ([self.model.email integerValue] == 2) {
        
    }else if ([self.model.email intValue] == 1 && self.emailView.textfield.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"邮箱必填哦"];
        return;
    }else if (![LukeUserAdapter validateEmail:emailStr] && emailStr.length > 0) {
        if ([USER_DATA_MANAGER.userInfoData.email isNoBlankString]) {
            
        }else{
            [SVProgressHUD showErrorWithStatus:@"输入的邮箱有误，请重新输入"];
            return;
        }
    }
    if ([self.model.qq integerValue] == 2) {
        
    }else if ([self.model.qq intValue] == 1 && self.qqView.textfield.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"QQ必填哦"];
        return;
    }else if (![LukeUserAdapter validateIsValidQQ:qqStr] && qqStr.length > 0) {
        if ([USER_DATA_MANAGER.userInfoData.qq isNoBlankString]) {
            
        }else{
            [SVProgressHUD showErrorWithStatus:@"输入的qq有误，请重新输入"];
            return;
        }
    }
    if ([self.model.weixin integerValue] == 2) {
        
    }else if ([self.model.weixin intValue] == 1 && self.wechatView.textfield.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"微信必填哦"];
        return;
    }else if (self.wechatView.textfield.text.length > 0) {
        if ([USER_DATA_MANAGER.userInfoData.weixin isNoBlankString]) {
            
        }else{
            BOOL isChinses = [wechatStr isContainChinese];
            BOOL isSymbol = [wechatStr isContainSpecialSymbol];
            if (isChinses || isSymbol) {
                [SVProgressHUD showErrorWithStatus:@"输入的微信有误，请重新输入"];
                self.wechatView.textfield.text = nil;
                return;
            }
        }
    }
    [SVProgressHUD showWithStatus:@"添加中..."];
    [NET_DATA_MANAGER requestPostModifyUserInfoWithfullName:self.fullNameView.textfield.text phone:phoneStr qq:qqStr weixin:wechatStr birthday:self.birthdayView.dateBtn.currentTitle email:emailStr Success:^(id responseObject) {
        [USER_DATA_MANAGER requestGetUserInfoCompleted:^(BOOL success) {
            [SVProgressHUD dismiss];
            if (success) {
                [self.viewController.navigationController popViewControllerAnimated:YES];
                [SVProgressHUD showSuccessWithStatus:@"修改成功"];
            }
        }];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [SVProgressHUD dismiss];
    }];
}

@end

