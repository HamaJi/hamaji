//
//  LukeSendMoneyView.h
//  Bet365
//
//  Created by luke on 17/7/9.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LukeSubUsersItem;
@protocol LukeSendMoneyViewDelegate <NSObject>

- (void)sendMoneyLoadWithAccount:(NSNumber *)account rebate:(NSString *)rebate;

- (void)sendMoneyCancelClick;

@end
@interface LukeSendMoneyView : UIView

- (instancetype)initWithFrame:(CGRect)frame item:(LukeSubUsersItem *)item;
/** 代理 */
@property (nonatomic,weak) id<LukeSendMoneyViewDelegate>delegate;

@end
