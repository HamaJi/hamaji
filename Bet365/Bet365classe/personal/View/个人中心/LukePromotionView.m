//
//  LukePromotionView.m
//  Bet365
//
//  Created by luke on 17/6/28.
//  Copyright © 2017年 jesse. All rights reserved.
#import "LukePromotionView.h"
#import "LukeRegisterLbTFView.h"
#import "LukeSpreadInfoItem.h"
#import "SportsModuleShare.h"
#import "NetWorkMannager+Account.h"

@interface LukePromotionView ()
/** 奖金组 */
@property (nonatomic,weak) LukeRegisterLbTFView *moneyView;
/** 推广页面 */
@property (nonatomic,weak) LukeRegisterLbTFView *PromotionView;
/** 会员类型 */
@property (nonatomic,weak) LukeRegisterLbTFView *typeView;
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *timeView;
/**  */
@property (nonatomic,strong) NSArray *temp;

@end

@implementation LukePromotionView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [SVProgressHUD showWithStatus:@"获取数据中"];
        [self getDataCompleted:^(BOOL finish) {
            [SVProgressHUD dismiss];
            self.temp = [[LukeSpreadInfoItem mj_keyValuesArrayWithObjectArray:SportsShareInstance.spreadInfoArrs] getNewArrayWithKey:@"name"];
            [self setupUI];
        }];
    }
    return self;
}

- (void)getDataCompleted:(void (^)(BOOL))completed
{
    dispatch_group_t group = dispatch_group_create();
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (SportsShareInstance.spreadInfoArrs.count == 0) {
            [NET_DATA_MANAGER requestGetSpreadTypesSuccess:^(id responseObject) {
                SportsShareInstance.spreadInfoArrs = [LukeSpreadInfoItem mj_objectArrayWithKeyValuesArray:responseObject];
                dispatch_semaphore_signal(sema);
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
            }];
        }else{
            dispatch_semaphore_signal(sema);
        }
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (SportsShareInstance.rebateArrs.count == 0) {
            [NET_DATA_MANAGER requestgetDlSubRebateRangeSuccess:^(id responseObject) {
                NSArray *rangeArr = (NSArray *)responseObject;
                if (rangeArr.count > 1) {
                    NSInteger endNum = [[rangeArr lastObject] floatValue] * 10;
                    NSInteger startNum = [[rangeArr firstObject] floatValue] * 10;
                    NSMutableArray *temp = [NSMutableArray array];
                    for (int i = endNum; i >= startNum; i --) {
                        [temp addObject:[NSString stringWithFormat:@"%d.%d",(i / 10),(i % 10)]];
                    }
                    SportsShareInstance.rebateArrs = temp;
                }
                dispatch_semaphore_signal(sema);
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
            }];
        }else{
            dispatch_semaphore_signal(sema);
        }
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        completed ? completed(YES) : nil;
    });
}

- (void)setupUI
{
    
    UILabel *basicLabel = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:@"基本信息"];
    [self addSubview:basicLabel];
    [basicLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(10);
        make.leading.equalTo(self).offset(10);
    }];
    
    LukeRegisterLbTFView *PromotionView = [LukeRegisterLbTFView registerCreateBtnViewWithTitle:@"推广页面：" BtnImage:@"right_lage" target:self action:@selector(PromotionClick)];
    [PromotionView.MoneyBtn setTitle:[self.temp firstObject] forState:UIControlStateNormal];
    [self addSubview:PromotionView];
    [PromotionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(basicLabel.mas_bottom).offset(5);
        make.leading.trailing.equalTo(self);
        make.height.mas_equalTo(40);
    }];
    self.PromotionView = PromotionView;
    
    NSString *typeTitle = @"会员";
    if ([BET_CONFIG.config.dl_can_create_dl isEqualToString:@"1"] || !BET_CONFIG.config.dl_can_create_dl) {
        typeTitle = @"代理";
    }
    LukeRegisterLbTFView *typeView = [LukeRegisterLbTFView registerCreateBtnViewWithTitle:@"会员类型：" BtnImage:@"right_lage" target:self action:@selector(typeClick)];
    [typeView.MoneyBtn setTitle:typeTitle forState:UIControlStateNormal];
    [self addSubview:typeView];
    [typeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(PromotionView.mas_bottom);
        make.leading.trailing.equalTo(self);
        make.height.mas_equalTo(40);
    }];
    self.typeView = typeView;
    
    LukeRegisterLbTFView *ProMotion = [LukeRegisterLbTFView registerCreateTextFieldViewWithTitle:@"推广码:" textEntry:NO placeholder:@"可选" keyboardType:UIKeyboardTypeDefault];
    ProMotion.clipsToBounds = YES;
    ProMotion.textfield.keyboardType = UIKeyboardTypeDefault;
    [self addSubview:ProMotion];
    [ProMotion mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(typeView.mas_bottom);
        make.leading.trailing.equalTo(self);
        make.height.mas_equalTo((!BET_CONFIG.config.dl_modify_swtich || [BET_CONFIG.config.dl_modify_swtich isEqualToString:@"1"]) ? 40 : 0);
    }];
    self.timeView = ProMotion;
    
    if ([BET_CONFIG.config.site_rebate_model isEqualToString:@"0"] || !BET_CONFIG.config.site_rebate_model) {
        UILabel *moneyLabel = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:@"奖金组"];
        [self addSubview:moneyLabel];
        [moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(ProMotion.mas_bottom).offset(10);
            make.leading.equalTo(self).offset(10);
        }];
        
        LukeRegisterLbTFView *moneyView = [LukeRegisterLbTFView registerCreateBtnViewWithTitle:@"选择奖金组：" BtnImage:@"right_lage" target:self action:@selector(moneyClick)];
        [moneyView.MoneyBtn setTitle:[SportsShareInstance.rebateArrs firstObject] forState:UIControlStateNormal];
        [self addSubview:moneyView];
        [moneyView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(moneyLabel.mas_bottom).offset(5);
            make.leading.trailing.equalTo(self);
            make.height.mas_equalTo(40);
        }];
        self.moneyView = moneyView;
    }
    
    UILabel *titleLabel = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:@"奖金组一旦上调，无法降低，请谨慎选择"];
    [self addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        if ([BET_CONFIG.config.site_rebate_model isEqualToString:@"0"] || !BET_CONFIG.config.site_rebate_model) {
            make.top.equalTo(self.moneyView.mas_bottom).offset(10);
        }else{
            make.top.equalTo(self.timeView.mas_bottom).offset(10);
        }
        make.leading.equalTo(self).offset(10);
    }];
    
    CGFloat btnW = (WIDTH - 30) / 2;
    UIButton *linkBtn = [self addButtonWithTitle:@"生成链接" font:15 color:[UIColor skinViewKitNorColor] backgroundColor:[UIColor skinViewKitSelColor] target:self action:@selector(linkClick:)];
    linkBtn.tag = 909;
    [self addSubview:linkBtn];
    [linkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLabel.mas_bottom).offset(40);
        make.centerX.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(btnW, 40));
    }];
    
//    UIButton *wechatlinkBtn = [self addButtonWithTitle:@"微信链接" font:15 color:[UIColor skinViewKitSelColor] backgroundColor:[UIColor skinViewKitNorColor] target:self action:@selector(linkClick:)];
//    wechatlinkBtn.tag = 910;
//    [self addSubview:wechatlinkBtn];
//    [wechatlinkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(titleLabel.mas_bottom).offset(40);
//        make.leading.equalTo(linkBtn.mas_trailing).offset(10);
//        make.size.mas_equalTo(CGSizeMake(btnW, 40));
//    }];
}

- (void)linkClick:(UIButton *)sender
{
    if (!BET_CONFIG.config.dl_modify_swtich || [BET_CONFIG.config.dl_modify_swtich isEqualToString:@"1"]) {
        if (self.timeView.textfield.text.length < [BET_CONFIG.config.dl_min_code_num integerValue]) {
            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"推广码最少%@位数",BET_CONFIG.config.dl_min_code_num]];
            return;
        }
    }
    NSString *rebateStr = nil;
    if ([BET_CONFIG.config.site_rebate_model isEqualToString:@"0"] || !BET_CONFIG.config.site_rebate_model){
        rebateStr = self.moneyView.MoneyBtn.currentTitle;
    }
    __block NSInteger spreadType = 0;
    for (LukeSpreadInfoItem *item in SportsShareInstance.spreadInfoArrs) {
        if ([self.PromotionView.MoneyBtn.currentTitle isEqualToString:item.name]) {
            spreadType = item.SpreadId;
            break;
        }
    }
    [NET_DATA_MANAGER requestPostCreateSpreadInfoWithType:sender.tag == 909 ? SpreadInfoCreateTypeUrl : SpreadInfoCreateTypeWechat userType:[self.typeView.MoneyBtn.currentTitle isEqualToString:@"会员"] ? SpreadInfoUserTypeHuiYuan : SpreadInfoUserTypeDaiLi externalUrl:nil spreadType:spreadType rebate:rebateStr ? rebateStr : [USER_DATA_MANAGER.userInfoData.rebate stringValue] code:self.timeView.textfield.text Success:^(id responseObject) {
        [SVProgressHUD showSuccessWithStatus:@"添加成功"];
        if ([self.delegete respondsToSelector:@selector(reloadLukeRebatesViewController)]) {
            [self.delegete reloadLukeRebatesViewController];
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (void)PromotionClick
{
    
    [BRStringPickerView showStringPickerWithTitle:nil dataSource:self.temp defaultSelValue:nil isAutoSelect:NO themeColor:[UIColor skinViewKitSelColor] resultBlock:^(id selectValue) {
        [self.PromotionView.MoneyBtn setTitle:selectValue forState:UIControlStateNormal];
    }];
}

- (void)typeClick
{
    if ([BET_CONFIG.config.dl_can_create_dl isEqualToString:@"1"] || !BET_CONFIG.config.dl_can_create_dl) {
        [BRStringPickerView showStringPickerWithTitle:nil dataSource:@[@"代理",@"会员"] defaultSelValue:nil isAutoSelect:NO themeColor:[UIColor skinViewKitSelColor] resultBlock:^(id selectValue) {
            [self.typeView.MoneyBtn setTitle:selectValue forState:UIControlStateNormal];
        }];
    }
}

- (void)moneyClick
{
    [BRStringPickerView showStringPickerWithTitle:nil dataSource:SportsShareInstance.rebateArrs defaultSelValue:nil isAutoSelect:NO themeColor:[UIColor skinViewKitSelColor] resultBlock:^(id selectValue) {
        [self.moneyView.MoneyBtn setTitle:selectValue forState:UIControlStateNormal];
    }];
}

- (UIButton *)addButtonWithTitle:(NSString *)title font:(CGFloat)font color:(UIColor *)color backgroundColor:(UIColor *)backgroundColor target:(id)target action:(SEL)action
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.backgroundColor = backgroundColor;
    btn.titleLabel.font = [UIFont systemFontOfSize:font];
    [btn setTitle:title forState:UIControlStateNormal];
    btn.layer.cornerRadius = 2;
    btn.layer.masksToBounds = YES;
    [btn setTitleColor:color forState:UIControlStateNormal];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn];
    return btn;
}

@end
