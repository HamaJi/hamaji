//
//  LukeNoticeCell.h
//  Bet365
//
//  Created by luke on 17/6/19.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LukeNoticeItem;

@interface LukeNoticeCell : UITableViewCell
/** 模型 */
@property (nonatomic,strong) LukeNoticeItem *item;

@end
