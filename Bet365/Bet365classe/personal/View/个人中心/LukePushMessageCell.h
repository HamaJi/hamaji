//
//  LukePushMessageCell.h
//  Bet365
//
//  Created by luke on 17/6/15.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LukePushMessageItem;

@interface LukePushMessageCell : UITableViewCell
/** 模型  */
@property (nonatomic,strong) LukePushMessageItem *item;

@end
