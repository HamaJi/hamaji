//
//  LukeCommboxView.h
//  Bet365
//
//  Created by luke on 17/6/29.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LukeCommboxView : UIView

/** 下拉数组 */
@property (nonatomic,strong) NSArray *titleDatas;

/** 回调 */
@property (nonatomic,copy) void(^boxBlock)(NSInteger);

@end
