//
//  LukeScreenView.m
//  Bet365
//
//  Created by luke on 17/6/20.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeScreenView.h"
#import "LukeRegisterLbTFView.h"
#import "LukeGameItem.h"
#import "LukeMaskView.h"
#import "LukeUserAdapter.h"
#import "AuthData.h"
#import "KFCPHomeGameJsonModel.h"

@interface LukeScreenView ()
/** 彩种游戏 */
@property (nonatomic,weak) LukeRegisterLbTFView *gameView;
/** 用户名 */
@property (nonatomic,weak) LukeRegisterLbTFView *nameView;
/**  */
@property (nonatomic,weak) UIView *contentView;
/** 只装游戏名字的数组 */
@property (nonatomic,strong) NSMutableArray *tempGameArrs;
/** 是否代理 */
@property (nonatomic,assign) BOOL isDl;
/** 是否是投注记录 */
@property (nonatomic,assign) BOOL isTZ;

@end

@implementation LukeScreenView
- (NSMutableArray *)tempGameArrs
{
    if (_tempGameArrs == nil) {
        _tempGameArrs = [NSMutableArray array];
    }
    return _tempGameArrs;
}

- (instancetype)initWithFrame:(CGRect)frame isDL:(BOOL)isDl isTouzhu:(BOOL)isTZ
{
    if (self = [super initWithFrame:frame]) {
        self.isDl = isDl;
        self.isTZ = isTZ;
        [self setupUI];
    }
    return self;
}

- (void)handelGameArr
{
    [self.tempGameArrs removeAllObjects];
    [self.tempGameArrs addObject:[[NSString stringWithFormat:@"%@",@"全部游戏"] mutableCopy]];
    for (KFCPHomeGameJsonModel *item in LOTTERY_FACTORY.lotteryData) {
        if (self.isTZ) {
            if ([item.isOffcial intValue] == 1) {
                [self.tempGameArrs addObject:[NSString stringWithFormat:@"%@[官]",item.name]];
            }
            if ([item.isCredit intValue] == 1) {
                [self.tempGameArrs addObject:[NSString stringWithFormat:@"%@[信]",item.name]];
            }
        }else{
            [self.tempGameArrs addObject:[NSString stringWithFormat:@"%@",item.name]];
        }
    }
    [BRStringPickerView showStringPickerWithTitle:nil dataSource:self.tempGameArrs defaultSelValue:nil isAutoSelect:NO themeColor:[UIColor skinNaviBgColor] resultBlock:^(id selectValue) {
        [self.gameView.MoneyBtn setTitle:selectValue forState:UIControlStateNormal];
    }];
}

- (void)setupUI
{
    self.backgroundColor = [JesseGrayColor(30) colorWithAlphaComponent:0.3];
    UIView *contentView = [[UIView alloc] init];
    contentView.backgroundColor = JesseColor(239, 239, 244);
    [self addSubview:contentView];
    self.contentView = contentView;
    
    UILabel *lb = [self addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:@"筛选条件"];
    [contentView addSubview:lb];
    [lb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(contentView).offset(10);
        make.top.equalTo(contentView).offset(10);
        make.height.mas_equalTo(20);
    }];
    
    if (self.isDl) {
        LukeRegisterLbTFView *nameView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"用户名" textEntry:NO placeholder:@"用户名" keyboardType:UIKeyboardTypeDefault];
        [contentView addSubview:nameView];
        [nameView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(contentView);
            make.top.equalTo(lb.mas_bottom).offset(5);
            make.height.mas_equalTo(40);
        }];
        self.nameView = nameView;
    }
    
    LukeRegisterLbTFView *gameView = [LukeRegisterLbTFView registerCreateBtnViewWithTitle:@"彩种游戏" BtnImage:@"right_lage" target:self action:@selector(gameClick)];
    [gameView.MoneyBtn setTitle:@"全部游戏" forState:UIControlStateNormal];
    [contentView addSubview:gameView];
    [gameView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (self.isDl) {
            make.top.mas_equalTo(self.nameView.mas_bottom);
        }else{
            make.top.mas_equalTo(lb.mas_bottom).offset(5);
        }
        make.leading.trailing.equalTo(contentView);
        make.height.mas_equalTo(40);
    }];
    self.gameView = gameView;
    
    LukeRegisterLbTFView *swView = nil;
    if (self.isTZ) {
        swView = [LukeRegisterLbTFView registerLbSwitchViewWithTitle:@"精简模式" Switch:AUTHDATA_MANAGER.streamline font:17];
        swView.switchBlock = ^(BOOL isSw){
            AUTHDATA_MANAGER.streamline = isSw;
            [AUTHDATA_MANAGER save];
            if ([self.delegate respondsToSelector:@selector(screenViewSwitchWithBool:)]) {
                [self.delegate screenViewSwitchWithBool:isSw];
            }
        };
        [contentView addSubview:swView];
        [swView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(gameView.mas_bottom);
            make.leading.trailing.equalTo(contentView);
            make.height.mas_equalTo(40);
        }];
    }
    
    UIButton *searchBtn = [self addButtonWithTitle:@"搜索" font:15 color:[UIColor whiteColor] backgroundColor:[UIColor skinViewKitSelColor] target:self action:@selector(searchClick)];
    searchBtn.layer.cornerRadius = 5;
    searchBtn.layer.masksToBounds = YES;
    [searchBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];
    [contentView addSubview:searchBtn];
    [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        if (self.isTZ) {
            make.top.equalTo(swView.mas_bottom).offset(70);
        }else{
            make.top.equalTo(gameView.mas_bottom).offset(70);
        }
        make.leading.equalTo(contentView).offset(20);
        make.trailing.equalTo(contentView).offset(-20);
        make.height.mas_equalTo(40);
        make.bottom.equalTo(contentView).offset(-10 - SafeAreaBottomHeight);
    }];
    
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.bottom.trailing.equalTo(self);
    }];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn addTarget:self action:@selector(cancelClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cancelBtn];
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.equalTo(self);
        make.bottom.equalTo(contentView.mas_top);
    }];
}

- (void)gameClick
{
    [self handelGameArr];
}

- (void)cancelClick
{
    if ([self.delegate respondsToSelector:@selector(screenViewCancelClick)]) {
        [self.delegate screenViewCancelClick];
    }
}

- (void)searchClick
{
    if ([self.delegate respondsToSelector:@selector(reloadRecordDataWithUser:gameID:model:)]) {
        NSString *str = nil;
        NSString *model = nil;
        if ([self.gameView.MoneyBtn.currentTitle isEqualToString:@"全部游戏"]) {
            str = nil;
        }else{
            for (KFCPHomeGameJsonModel *item in LOTTERY_FACTORY.lotteryData) {
                if (self.isTZ) {
                    if ([[self.gameView.MoneyBtn.currentTitle substringToIndex:self.gameView.MoneyBtn.currentTitle.length - 3] isEqualToString:item.name]) {
                        str = item.GameId;
                        if ([self.gameView.MoneyBtn.currentTitle containsString:@"信"]) {
                            model = @"1";
                        }else if ([self.gameView.MoneyBtn.currentTitle containsString:@"官"]){
                            model = @"0";
                        }
                        break;
                    }
                }else{
                    if ([self.gameView.MoneyBtn.currentTitle isEqualToString:item.name]) {
                        str = item.GameId;
                        break;
                    }
                }
            }
        }
        [self.delegate reloadRecordDataWithUser:self.nameView.textfield.text gameID:str model:model];
    }
}

#pragma mark - 创建button
- (UIButton *)addButtonWithTitle:(NSString *)title font:(CGFloat)font color:(UIColor *)color backgroundColor:(UIColor *)backgroundColor target:(id)target action:(SEL)action
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font = [UIFont systemFontOfSize:font];
    [btn setTitle:title forState:UIControlStateNormal];
    btn.backgroundColor = backgroundColor;
    [btn setTitleColor:color forState:UIControlStateNormal];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return btn;
}
//创建label
- (UILabel *)addLabelWithFont:(CGFloat)font color:(UIColor *)color title:(NSString *)title
{
    UILabel *label = [[UILabel alloc] init];
    label.text = title;
    label.textColor = color;
    label.font = [UIFont systemFontOfSize:font];
    return label;
}

@end
