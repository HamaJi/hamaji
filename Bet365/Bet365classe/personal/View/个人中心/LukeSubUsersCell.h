//
//  LukeSubUsersCell.h
//  Bet365
//
//  Created by luke on 17/7/1.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LukeSubUsersItem;

@interface LukeSubUsersCell : UITableViewCell
/** 模型 */
@property (nonatomic,strong) LukeSubUsersItem *item;

@end
