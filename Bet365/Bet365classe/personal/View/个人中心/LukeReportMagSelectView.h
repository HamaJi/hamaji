//
//  LukeReportMagSelectView.h
//  Bet365
//
//  Created by luke on 17/6/26.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LukeReportMagSelectViewDelegate <NSObject>

- (void)ReportMagSelectViewWithAccount:(NSString *)account type:(NSString *)typeID;

- (void)ReportMagSelectViewCancelClick;

@end

@interface LukeReportMagSelectView : UIView
/** 代理 */
@property (nonatomic,weak) id<LukeReportMagSelectViewDelegate>delegate;

@end
