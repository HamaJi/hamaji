//
//  LukeReportMagCell.m
//  Bet365
//
//  Created by luke on 17/6/26.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeReportMagCell.h"
#import "LukeReportMagItem.h"

@interface LukeReportMagCell ()
/**  */
@property (nonatomic,strong) NSArray *typeArrs;

@end

@implementation LukeReportMagCell
{
    UILabel *accountLb;
    UILabel *addTimeLb;
    UILabel *moneyLb;
    UILabel *moneyLabel;
    UILabel *tranTypeLb;
    UILabel *balanceLb;
    /** 蛤蟆吉19.2.22 充值记录新增remarks字段展示*/
    UILabel *_remarksLb;
    UILabel *contentLb;
}
- (NSArray *)typeArrs
{
    if (_typeArrs == nil) {
        _typeArrs = @[@{@"value": @"转账充值", @"key" : @"1"},
                      @{@"value": @"线上支付", @"key" : @"2"},
                      @{@"value": @"彩票下注", @"key" : @"3"},
                      @{@"value": @"彩票派彩", @"key" : @"4"},
                      @{@"value": @"用户提款", @"key" : @"6"},
                      @{@"value": @"撤单返款", @"key" : @"7"},
                      @{@"value": @"提款失败", @"key" : @"8"},
                      @{@"value": @"退佣(分红)", @"key" : @"10"},
                      @{@"value": @"后台转入", @"key" : @"11"},
                      @{@"value": @"后台转出", @"key" : @"12"},
                      @{@"value": @"注册送彩", @"key" : @"16"},
                      @{@"value": @"代理返点", @"key" : @"17"},
                      @{@"value": @"人工存入", @"key" : @"18"},
                      @{@"value": @"给予返水", @"key" : @"19"},
                      @{@"value": @"活动优惠", @"key" : @"20"},
                      @{@"value": @"追号返款", @"key" : @"21"},
                      @{@"value": @"系统奖励", @"key" : @"22"},
                      @{@"value": @"代理日工资", @"key" : @"23"},
                      @{@"value": @"代理扶持", @"key" : @"24"},
                      @{@"value": @"其他充值", @"key" : @"25"},
                      @{@"value": @"打和返款", @"key" : @"26"},
                      @{@"value": @"冲销返水", @"key" : @"27"},
                      @{@"value": @"人工提出", @"key" : @"28"},
                      @{@"value": @"追号扣款", @"key" : @"29"},
                      @{@"value": @"优惠扣除", @"key" : @"30"},
                      @{@"value": @"其他扣除", @"key" : @"31"},
                      @{@"value": @"冲销派奖", @"key" : @"32"},
                      @{@"value": @"额度转入", @"key" : @"33"},
                      @{@"value": @"额度转出", @"key" : @"34"},
                      @{@"value": @"体育下注", @"key" : @"35"},
                      @{@"value": @"体育结算", @"key" : @"36"},
                      @{@"value": @"违规退还本金", @"key" : @"37"},
                      @{@"value": @"重新结算", @"key" : @"38"},
                      @{@"value": @"取消订单", @"key" : @"39"},
                      @{@"value": @"真人返点", @"key" : @"40"},
                      @{@"value": @"真人返点回收", @"key" : @"41"},
                      @{@"value": @"电竞王者荣耀下注", @"key" : @"42"},
                      @{@"value": @"电竞王者荣耀重新结算", @"key" : @"43"},
                      @{@"value": @"电竞王者荣耀取消订单", @"key" : @"44"}];
    }
    return _typeArrs;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    addTimeLb = [UILabel addLabelWithFont:17 color:[UIColor blackColor] title:nil];
    [self.contentView addSubview:addTimeLb];
    [addTimeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(7);
        make.leading.equalTo(self.contentView).offset(10);
    }];
    
    accountLb = [UILabel addLabelWithFont:17 color:[UIColor blackColor] title:nil];
    [self.contentView addSubview:accountLb];
    [accountLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(addTimeLb.mas_bottom).offset(7);
        make.leading.equalTo(self.contentView).offset(10);
    }];
    
    moneyLb = [UILabel addLabelWithFont:17 color:[UIColor blackColor] title:nil];
    [self.contentView addSubview:moneyLb];
    [moneyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(accountLb.mas_top);
        make.leading.equalTo(self.contentView).offset(WIDTH * 0.5);
    }];
    
    tranTypeLb = [UILabel addLabelWithFont:17 color:[UIColor blackColor] title:nil];
    [self.contentView addSubview:tranTypeLb];
    tranTypeLb.numberOfLines = 0;
    [tranTypeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(accountLb.mas_bottom).offset(7);
        make.leading.equalTo(self.contentView).offset(10);
        make.trailing.equalTo(self.contentView).offset(-WIDTH * 0.5);
    }];
    
    balanceLb = [UILabel addLabelWithFont:17 color:[UIColor blackColor] title:nil];
    [self.contentView addSubview:balanceLb];
    [balanceLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(tranTypeLb.mas_top);
        make.leading.equalTo(self.contentView).offset(WIDTH * 0.5);
    }];
    
    contentLb = [UILabel addLabelWithFont:17 color:[UIColor blackColor] title:nil];
    contentLb.numberOfLines = 0;
    contentLb.preferredMaxLayoutWidth = WIDTH - 20;
    [self.contentView addSubview:contentLb];
    [contentLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(tranTypeLb.mas_bottom).offset(7);
        make.leading.equalTo(self.contentView).offset(10);
        make.bottom.equalTo(self.contentView).offset(-7);
    }];
}

- (void)setItem:(LukeReportMagItem *)item
{
    _item = item;
    accountLb.text = [NSString stringWithFormat:@"用户: %@",item.account];
    addTimeLb.text = [NSString stringWithFormat:@"时间: %@",item.addTime];
    if ([item.money floatValue] < 0) {
        moneyLb.attributedText = [NSMutableAttributedString setTitle:@"支出" colorTitle:[LukeUserAdapter decimalNumberWithId:fabs([item.money doubleValue])] color:[UIColor greenColor]];
    }else if ([item.money floatValue] > 0) {
        moneyLb.attributedText = [NSMutableAttributedString setTitle:@"收入" colorTitle:[LukeUserAdapter decimalNumberWithId:fabs([item.money doubleValue])] color:[UIColor greenColor]];
    }else{
        moneyLb.text = @"支出 0";
    }
    
    for (NSDictionary *dict in self.typeArrs) {
        if (item.tranType == [dict[@"key"] integerValue]) {
            tranTypeLb.attributedText = [NSMutableAttributedString setTitle:@"账变类型" colorTitle:dict[@"value"] color:[UIColor colorWithHexString:@"#a7b800"]];
        }
    }
    if (!tranTypeLb.attributedText.length) {
        tranTypeLb.text = @"账变类型";
    }
    CGFloat f = [item.balance doubleValue] + [item.money doubleValue];
    balanceLb.text = [NSString stringWithFormat:@"余额: %@",[LukeUserAdapter decimalNumberWithId:f]];
    contentLb.text = [NSString stringWithFormat:@"内容: %@",item.content];
}

@end
