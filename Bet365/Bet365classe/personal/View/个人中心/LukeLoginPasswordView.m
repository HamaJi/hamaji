//
//  LukeLoginPasswordView.m
//  Bet365
//
//  Created by luke on 17/6/27.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeLoginPasswordView.h"
#import "LukeRegisterLbTFView.h"
#import "NSString+MacRegexCategory.h"
@interface LukeLoginPasswordView ()<UITextFieldDelegate>
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *oldPasswordView;
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *passWordView;
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *PWSureView;
/**  */
@property (nonatomic,assign) PersonPassWordType type;

@end

@implementation LukeLoginPasswordView

- (instancetype)initWithTitle:(NSString *)title type:(PersonPassWordType)type
{
    if (self = [super init]) {
        self.type = type;
        [self setupUIWithTitle:title];
    }
    return self;
}

- (void)setupUIWithTitle:(NSString *)title
{
    UILabel *titleLb = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:title];
    [self addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(10);
        make.leading.equalTo(self).offset(10);
    }];
    
    if (self.type == PersonPassWordTypeLogin || (self.type == PersonPassWordTypeDraw && [USER_DATA_MANAGER.userInfoData.fundPwd isNoBlankString])) {
        LukeRegisterLbTFView *oldPasswordView = [LukeRegisterLbTFView registerCreateTextFieldViewWithTitle:@"当前密码" textEntry:YES placeholder:@"请输入当前密码" keyboardType:self.type == PersonPassWordTypeLogin ? UIKeyboardTypeDefault : UIKeyboardTypeNumberPad];
        oldPasswordView.textfield.delegate = self;
        [self addSubview:oldPasswordView];
        [oldPasswordView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(self);
            make.top.equalTo(titleLb.mas_bottom).offset(5);
            make.height.mas_equalTo(40);
        }];
        self.oldPasswordView = oldPasswordView;
    }
    
    LukeRegisterLbTFView *newPasswordView = [LukeRegisterLbTFView registerCreateTextFieldViewWithTitle:@"新密码" textEntry:YES placeholder:@"请输入新密码" keyboardType:self.type == PersonPassWordTypeLogin ? UIKeyboardTypeDefault : UIKeyboardTypeNumberPad];
    newPasswordView.textfield.delegate = self;
    [self addSubview:newPasswordView];
    [newPasswordView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (self.type == PersonPassWordTypeLogin || (self.type == PersonPassWordTypeDraw && [USER_DATA_MANAGER.userInfoData.fundPwd isNoBlankString])){
            make.top.equalTo(self.oldPasswordView.mas_bottom);
        }else{
            make.top.equalTo(titleLb.mas_bottom).offset(5);
        }
        make.leading.trailing.equalTo(self);
        make.height.mas_equalTo(40);
    }];
    self.passWordView = newPasswordView;
    
    LukeRegisterLbTFView *newPasswordSureView = [LukeRegisterLbTFView registerCreateTextFieldViewWithTitle:@"密码确认" textEntry:YES placeholder:@"请再次输入新密码" keyboardType:self.type == PersonPassWordTypeLogin ? UIKeyboardTypeDefault : UIKeyboardTypeNumberPad];
    newPasswordSureView.textfield.delegate = self;
    [self addSubview:newPasswordSureView];
    [newPasswordSureView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.equalTo(self);
        make.top.equalTo(newPasswordView.mas_bottom);
        make.height.mas_equalTo(40);
    }];
    self.PWSureView = newPasswordSureView;
    
    UIButton *sureBtn = [UIButton addButtonWithTitle:@"确认" font:15 color:[UIColor skinViewKitNorColor] target:self action:@selector(sureClick)];
    [sureBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];
    [self addSubview:sureBtn];
    sureBtn.layer.cornerRadius = 5;
    sureBtn.layer.masksToBounds = YES;
    [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(newPasswordSureView.mas_bottom).offset(50);
        make.centerX.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(WIDTH - 20, 40));
    }];
}

- (void)sureClick
{
    if (self.oldPasswordView && self.oldPasswordView.textfield.text.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"当前密码必填哦"];
        return;
    }

    if (self.type == PersonPassWordTypeLogin && ![self.passWordView.textfield.text isPermissionllyPsw]) {
        return;
    }
    if (self.PWSureView.textfield.text.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"确认密码必填哦"];
        return;
    }
    if (self.type == PersonPassWordTypeLogin || (self.type == PersonPassWordTypeDraw && [USER_DATA_MANAGER.userInfoData.fundPwd isNoBlankString])) {
        if ([self.passWordView.textfield.text isEqualToString:self.oldPasswordView.textfield.text]) {
            [SVProgressHUD showErrorWithStatus:@"新密码不能同旧密码相同,请重新输入"];
            return;
        }
        if (![self.passWordView.textfield.text isEqualToString:self.PWSureView.textfield.text]) {
            [SVProgressHUD showErrorWithStatus:@"两次输入的密码不一致"];
            return;
        }
    }
    NSString *oldPassword = nil;
    NSString *newPassword = nil;
    if (self.type == PersonPassWordTypeLogin) {
        if (BET_CONFIG.config.user_password_type && [BET_CONFIG.config.user_password_type isEqualToString:@"0"]) {
            oldPassword = [self.oldPasswordView.textfield.text MD5];
            newPassword = [self.PWSureView.textfield.text MD5];
        }else{
            oldPassword = self.oldPasswordView.textfield.text;
            newPassword = self.PWSureView.textfield.text;
        }
    }else{
        oldPassword = self.oldPasswordView ? self.oldPasswordView.textfield.text : @"";
        newPassword = self.PWSureView.textfield.text;
    }
    [SVProgressHUD showWithStatus:@"修改中..."];
    [NET_DATA_MANAGER requestPostUpdatePwdByOldPassword:oldPassword NewPassword:newPassword type:self.type Success:^(id responseObject) {
        //更新所有个人配置信息
        [USER_DATA_MANAGER requestGetUserInfoCompleted:^(BOOL success) {
        }];
        [[LukeLoginPasswordView findViewController:self].navigationController popViewControllerAnimated:YES];
        [SVProgressHUD showSuccessWithStatus:@"修改成功"];
        [SVProgressHUD dismiss];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [SVProgressHUD dismiss];
    }];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self.type == PersonPassWordTypeDraw) {
        return textField.text.length - range.length + string.length < 5;
    }else if (self.type == PersonPassWordTypeLogin){
       
        return YES;
    }else{
        return YES;
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField{

}
@end
