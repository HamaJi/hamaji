//
//  LoginAccountCenterView.m
//  Bet365
//
//  Created by super_小鸡 on 2019/8/15.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "LoginCenterView+Overrides.h"
#import "LoginAccountCenterView.h"
#import "LoginCenterView+Overrides.h"

@interface LoginAccountCenterView()<UITextFieldDelegate>

/** 密码 */
@property (nonatomic,strong) LukeRegisterLbTFView *passwordView;
/** 会员帐号 */
@property (nonatomic,strong) LukeRegisterLbTFView *nameView;
/** 验证码 */
@property (nonatomic,strong) LukeRegisterLbTFView *VerifyView;

@property (nonatomic,strong) UIButton *rememberBtn;

@property (nonatomic,strong) UIButton *forgetBtn;

@end

@implementation LoginAccountCenterView

#pragma mark - Overrides

-(void)verifyAuther:(void (^)(BOOL))handle{
    @weakify(self);
    self.verifyBlock = ^(NSDictionary * _Nonnull paramers) {
        @strongify(self);
        [self.paramers addEntriesFromDictionary:paramers];
        handle ? handle(YES) : nil;
    };
    if (self.verifyType == VerifyAuthorType_IMAGE){
        if (!self.vCode.length) {
            handle ? handle(NO) : nil;
            [SVProgressHUD showErrorWithStatus:@"请填写图形验证码"];
            return;
        }
        [self.paramers safeSetObject:self.vCode forKey:kLRKeyValiCode];
        handle ? handle(YES) : nil;
    }else if(self.verifyType == VerifyAuthorType_UNVERIFY){
        handle ? handle(YES) : nil;
    }else{
        [self.verifyMannager launchVerify];
    }
}

-(void)addObser{
    [super addObser];
    self.nameView.textfield.delegate = self;
    self.passwordView.textfield.delegate = self;
    self.VerifyView.textfield.delegate = self;
    
}

-(LukeRegisterLbTFView *)subVerifyView{
    return self.VerifyView;
}

-(NSString *)verifyImageValiCodeKey{
    return @"";
}

-(void)remenberAction:(UIButton *)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(loginCenterView:TapAction:)]) {
        [self.delegate loginCenterView:self TapAction:self.remenberAction];
        sender.selected = !sender.selected;
        AUTHDATA_MANAGER.isRemember = sender.selected;
        [AUTHDATA_MANAGER save];
    }
}

-(void)forgetPswAction:(UIButton *)sender{
    if(self.delegate && [self.delegate respondsToSelector:@selector(loginCenterView:TapAction:)]){
        [self.delegate loginCenterView:self TapAction:self.forgetAction];
    }
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    LoginRequestKey *key = nil;
    if ([textField isEqual:self.nameView.textfield]) {
        key = kLRKeyAccount;
        self.identity = textField.text;
    }else if ([textField isEqual:self.passwordView.textfield]) {
        key = kLRKeyPassword;
        self.password = self.passwordView.textfield.text;
    }else if ([textField isEqual:self.VerifyView.textfield]) {
        key = kLRKeyValiCode;
        self.vCode = self.VerifyView.textfield.text;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(loginCenterView:TextFiledSubmitText:Key:)]) {
        [self.delegate loginCenterView:self TextFiledSubmitText:textField.text Key:key];
    }
}


-(LukeRegisterLbTFView *)nameView{
    if (!_nameView) {
        _nameView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"帐号" textEntry:NO placeholder:@"请输入用户名" keyboardType:UIKeyboardTypeDefault];
        if (AUTHDATA_MANAGER.isRemember) {
            _nameView.textfield.text = AUTHDATA_MANAGER.account;
            self.identity = _nameView.textfield.text;
        }
        _nameView.layer.cornerRadius = 5;
        _nameView.layer.masksToBounds = YES;
        
        _nameView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
        _nameView.layer.borderWidth = 1;
        [self addSubview:_nameView];
        [_nameView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset(20);
            make.width.mas_equalTo(WIDTH - 20);
            make.leading.equalTo(self).offset(10);
            make.right.equalTo(self).offset(-10);
            make.height.mas_equalTo(45);
        }];
    }
    return _nameView;
}

-(LukeRegisterLbTFView *)passwordView{
    if (!_passwordView) {
        _passwordView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"密码" textEntry:YES placeholder:@"请输入密码" keyboardType:UIKeyboardTypeDefault];
        if (AUTHDATA_MANAGER.isRemember) {
            _passwordView.textfield.text = AUTHDATA_MANAGER.passWord;
            self.password = _passwordView.textfield.text;
        }
        _passwordView.layer.cornerRadius = 5;
        _passwordView.layer.masksToBounds = YES;
        _passwordView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
        _passwordView.layer.borderWidth = 1;
        [self addSubview:_passwordView];
        [_passwordView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.nameView.mas_bottom).offset(5);
            make.width.mas_equalTo(WIDTH - 20);
            make.leading.equalTo(self).offset(10);
            make.right.equalTo(self).offset(-10);
            make.height.mas_equalTo(45);
        }];
    }
    return _passwordView;
}

-(LukeRegisterLbTFView *)VerifyView{
    if (!_VerifyView) {
        _VerifyView = [LukeRegisterLbTFView registerLbBtnTextFieldViewWithTitle:@"验证码" VerificationBtnImage:@"vaid" target:self action:@selector(updateVerifyImage)];
        _VerifyView.layer.cornerRadius = 5;
        _VerifyView.layer.masksToBounds = YES;
        _VerifyView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
        _VerifyView.layer.borderWidth = 1;
        _VerifyView.clipsToBounds = YES;
        [self addSubview:_VerifyView];
        [_VerifyView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.passwordView.mas_bottom).offset(5);
            make.width.mas_equalTo(WIDTH - 20);
            make.leading.equalTo(self).offset(10);
            make.right.equalTo(self).offset(-10);
            make.height.mas_equalTo(0);
        }];
    }
    return _VerifyView;
}

-(UIButton *)rememberBtn{
    if (!_rememberBtn) {
        _rememberBtn = [UIButton addButtonWithTitle:@"记住帐号密码" font:15 color:[UIColor skinTextItemNorColor] target:self action:@selector(remenberAction:)];
        _rememberBtn.backgroundColor = [UIColor clearColor];
        [_rememberBtn setImage:[UIImage imageNamed:@"register_agree"] forState:UIControlStateSelected];
        [_rememberBtn setImage:[UIImage imageNamed:@"register_disagree"] forState:UIControlStateNormal];
        [_rememberBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -5)];
        _rememberBtn.selected = AUTHDATA_MANAGER.isRemember;
        [self addSubview:_rememberBtn];
        [_rememberBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(self).offset(10);
            make.top.equalTo(self.VerifyView.mas_bottom).offset(10);
            make.bottom.equalTo(self).offset(-10);
            make.height.mas_equalTo(20);
        }];
    }
    return _rememberBtn;
}

-(UIButton *)forgetBtn{
    if (!_forgetBtn) {
        _forgetBtn = [UIButton addButtonWithTitle:@"忘记密码" font:15 color:YCZGrayColor(150) target:self action:@selector(forgetPswAction:)];
        _forgetBtn.backgroundColor = [UIColor clearColor];
        [self addSubview:_forgetBtn];
        [_forgetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.trailing.equalTo(self).offset(-10);
            make.top.equalTo(self.VerifyView.mas_bottom).offset(10);
            make.bottom.equalTo(self).offset(-10);
            make.height.mas_equalTo(20);
        }];

    }
    return _forgetBtn;
}

-(void)setPassword:(NSString *)password{
    if ([password isEqualToString:_password]) {
        return;
    }
    _password = password;
}

-(void)setAccount:(NSString *)account{
    if ([self.identity isEqualToString:account]) {
        return;
    }
    self.identity = account;
}
-(NSString *)account{
    return self.identity;
}

-(void)setRemenberAction:(SEL)remenberAction{
    _remenberAction = remenberAction;
    [self rememberBtn];
}

-(void)setForgetAction:(SEL)forgetAction{
    _forgetAction = forgetAction;
    [self forgetBtn];
}
@end





