//
//  YEBaoHistoryTableViewCell.h
//  Bet365
//
//  Created by super_小鸡 on 2019/6/10.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YEBaoInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface YEBaoHistoryTableViewCell : UITableViewCell

@property (nonatomic) NSDate *date;

@property (nonatomic)BOOL isFirst;

-(void)setRate:(NSNumber *)rate BetweenMinRete:(NSNumber *)minRate MaxRate:(NSNumber *)maxRate;

@end

NS_ASSUME_NONNULL_END
