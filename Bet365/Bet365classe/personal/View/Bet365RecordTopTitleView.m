//
//  Bet365RecordTopTitleView.m
//  Bet365
//
//  Created by luke on 2018/8/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Bet365RecordTopTitleView.h"
#import "HMSegmentedControl+Bet365.h"

@implementation Bet365RecordTopTitleView

- (instancetype)initWithTitles:(NSArray *)titles titleColor:(UIColor *)color backgroundColor:(UIColor *)backgroundColor
{
    if (self = [super init]) {
        HMSegmentedControl *segmentControl = [[HMSegmentedControl alloc] initWithSectionTitles:titles];
        segmentControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
        segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationNone;
        segmentControl.backgroundColor = backgroundColor;
        segmentControl.titleTextAttributes = @{NSForegroundColorAttributeName : color,NSFontAttributeName : [UIFont systemFontOfSize:IS_IPHONE_5s ? 15 : 17]};
        [self addSubview:segmentControl];
        [segmentControl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return self;
}

@end
