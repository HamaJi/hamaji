//
//  PersonPageSectionView.m
//  Bet365
//
//  Created by HHH on 2018/8/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "PersonPageSectionView.h"
@interface PersonPageSectionView()
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)UILabel *titileLabel;
@end
@implementation PersonPageSectionView
+(NSString *)identifier{
    return [NSString stringWithFormat:@"%@", [self class]];
}
+(CGFloat)getHeight{
    return 40;
}
-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

-(UIView *)bgView{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, WIDTH - 20, [PersonPageSectionView getHeight])];

        _bgView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_bgView];
        
        //    UIRectCornerTopLeft     = 1 << 0,  左上角
        //    UIRectCornerTopRight    = 1 << 1,  右上角
        //    UIRectCornerBottomLeft  = 1 << 2,  左下角
        //    UIRectCornerBottomRight = 1 << 3,  右下角
        //    UIRectCornerAllCorners  = ~0UL     全部角
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_bgView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(8,8)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = _bgView.frame;
        maskLayer.path = maskPath.CGPath;
        _bgView.layer.mask = maskLayer;
        _bgView.backgroundColor = [UIColor skinViewBgColor];
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView);
        }];
    }
    return _bgView;
}
-(UILabel *)titileLabel{
    if (!_titileLabel) {
        _titileLabel = [UILabel setAllocLabelWithText:@"--" FontOfSize:15 rgbColor:0x000000];
        _titileLabel.textColor = [UIColor skinTextHeadColor];
        [self.bgView addSubview:_titileLabel];
        [_titileLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bgView).offset(20);
            make.centerY.equalTo(self.bgView);
        }];
    }
    return _titileLabel;
}

-(void)setTittile:(NSString *)tittile{
    self.titileLabel.text = tittile ? tittile : @"";
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
