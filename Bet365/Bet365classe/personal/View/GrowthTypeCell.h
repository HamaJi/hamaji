//
//  GrowthTypeCell.h
//  Bet365
//
//  Created by luke on 2019/11/4.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GrowthTypeListModel;

NS_ASSUME_NONNULL_BEGIN

@interface GrowthTypeCell : UITableViewCell
@property (nonatomic,strong) GrowthTypeListModel *model;

@end

NS_ASSUME_NONNULL_END
