//
//  LukeReportMagSelectView.m
//  Bet365
//
//  Created by luke on 17/6/26.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeLiveRecordSelectView.h"
#import "LukeRegisterLbTFView.h"
#import "LukeLiveRecordItem.h"
#import "LukeMaskView.h"
#import "SportsModuleShare.h"

@interface LukeLiveRecordSelectView ()<UITextFieldDelegate>
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *accountView;
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *fanshuiView;
/**  */
@property (nonatomic,weak) UIView *contentView;
/** 装名称的数组 */
@property (nonatomic,strong) NSMutableArray *gameArr;

@end

@implementation LukeLiveRecordSelectView
- (NSMutableArray *)gameArr
{
    if (_gameArr == nil) {
        _gameArr = [NSMutableArray array];
        for (LukeLiveGamesItem *item in ALL_DATA.operatingGameList) {
            if (![item.code isEqualToString:@"wzry"]) {
                [_gameArr addObject:[item.name mutableCopy]];
            }
        }
    }
    return _gameArr;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.backgroundColor = [JesseGrayColor(10) colorWithAlphaComponent:0.3];
    UIView *contentView = [[UIView alloc] init];
    contentView.backgroundColor = JesseGrayColor(238);
    [self addSubview:contentView];
    self.contentView = contentView;
    
    UILabel *lb = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:@"筛选条件"];
    [contentView addSubview:lb];
    [lb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(contentView).offset(10);
        make.leading.equalTo(contentView).offset(10);
        make.height.mas_equalTo(20);
    }];
    
    LukeRegisterLbTFView *fanshuiView = [LukeRegisterLbTFView registerLbBtnViewWithTitle:@"游戏信息" btnImage:@"right_lage" btnTitle:[self.gameArr firstObject]];
    fanshuiView.callBlock = ^{
        [BRStringPickerView showStringPickerWithTitle:@"游戏信息" dataSource:self.gameArr defaultSelValue:nil isAutoSelect:NO themeColor:[UIColor skinNaviBgColor] resultBlock:^(id selectValue) {
            [self.fanshuiView.dateBtn setTitle:selectValue forState:UIControlStateNormal];
        }];
    };
    [contentView addSubview:fanshuiView];
    [fanshuiView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lb.mas_bottom).offset(5);
        make.leading.trailing.equalTo(contentView);
        make.height.mas_equalTo(40);
    }];
    self.fanshuiView = fanshuiView;
    
    UIButton *sureBtn = [UIButton addButtonWithTitle:@"搜索" font:17 color:[UIColor whiteColor] target:self action:@selector(sureClick)];
    sureBtn.backgroundColor = [UIColor redColor];
    sureBtn.layer.cornerRadius = 5;
    sureBtn.layer.masksToBounds = YES;
    [contentView addSubview:sureBtn];
    [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(fanshuiView.mas_bottom).offset(100);
        make.leading.equalTo(contentView).offset(10);
        make.trailing.equalTo(contentView).offset(-10);
        make.height.mas_equalTo(40);
        make.bottom.mas_equalTo(contentView.mas_bottom).offset(-10 - SafeAreaBottomHeight);
    }];
    
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.bottom.equalTo(self);
    }];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn addTarget:self action:@selector(cancelClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cancelBtn];
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.top.equalTo(self);
        make.bottom.equalTo(contentView.mas_top);
    }];
}

- (void)cancelClick
{
    if ([self.delegate respondsToSelector:@selector(liveRecordSelectCancelClick)]) {
        [self.delegate liveRecordSelectCancelClick];
    }
}

- (void)sureClick
{
    if ([self.delegate respondsToSelector:@selector(liveRecordSelectWithAccount:code:)]) {
        __block NSString *code = nil;
        [ALL_DATA.operatingGameList enumerateObjectsUsingBlock:^(LukeLiveGamesItem  *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([self.fanshuiView.dateBtn.currentTitle isEqualToString:obj.name]) {
                code = obj.code;
                *stop = YES;
            }
        }];
        [self.delegate liveRecordSelectWithAccount:self.accountView.textfield.text code:code];
    }
}

@end


