//
//  LoginCenterView+Overrides.h
//  Bet365
//
//  Created by super_小鸡 on 2019/8/16.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "LoginCenterView.h"
#import "LukeRegisterLbTFView.h"
#import "VerifyManager.h"
NS_ASSUME_NONNULL_BEGIN

@interface LoginCenterView ()

@property (nonatomic,getter=subVerifyView) LukeRegisterLbTFView *verifyCodeView;

@property (nonatomic,assign) VerifyAuthorType verifyType;

@property (nonatomic,strong) NSString *identity;

@property (nonatomic) NSString *vCode;

-(NSString *)verifyImageValiCodeKey;

-(void)verifyAuther:(void(^)(BOOL success))handle;

@end

NS_ASSUME_NONNULL_END
