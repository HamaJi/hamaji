//
//  Bet365TransferStatusHeaderView.m
//  Bet365
//
//  Created by luke on 2019/11/27.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "Bet365TransferStatusHeaderView.h"

@interface Bet365TransferStatusHeaderView ()

@property (weak, nonatomic) IBOutlet UILabel *titleLb;

@property (weak, nonatomic) IBOutlet UIButton *transferBtn;

@end

@implementation Bet365TransferStatusHeaderView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.titleLb.textColor = [UIColor skinTextHeadColor];
    [self.transferBtn setTitleColor:[UIColor skinViewKitNorColor] forState:UIControlStateNormal];
    [self.transferBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];
}

- (IBAction)transferClick:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(transferStatusHandel)]) {
        [self.delegate transferStatusHandel];
    }
}

- (void)transferTitle:(NSString *)title index:(NSInteger)index status:(TransferStatus)status
{
    self.titleLb.text = title;
    if (status == TransferStatus_Manual) {
        [self.transferBtn setTitle:@"切换为自动额度转换模式" forState:UIControlStateNormal];
        if (index != 3) {
            self.transferBtn.hidden = YES;
        }else{
            if (BET_CONFIG.config.user_transfer_stauts && [BET_CONFIG.config.user_transfer_stauts isEqualToString:@"2"]) {
                self.transferBtn.hidden = NO;
            }else{
                self.transferBtn.hidden = YES;
            }
        }
    }else{
        [self.transferBtn setTitle:@"切换为手动额度转换" forState:UIControlStateNormal];
        if (index == 1) {
            if (BET_CONFIG.config.user_transfer_stauts && [BET_CONFIG.config.user_transfer_stauts isEqualToString:@"2"]) {
                self.transferBtn.hidden = NO;
            }else{
                self.transferBtn.hidden = YES;
            }
        }else{
            self.transferBtn.hidden = YES;
        }
    }
}

@end
