//
//  Bet365RecordViewCell.h
//  Bet365
//
//  Created by luke on 2018/8/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Bet365CpHistoryModel;
@class Bet365GameReportListModel;
@interface Bet365RecordViewCell : UITableViewCell
/** 彩票模型 */
@property (nonatomic,strong) Bet365CpHistoryModel *cpModel;
/**  */
@property (nonatomic,strong) Bet365GameReportListModel *gameModel;
@end
