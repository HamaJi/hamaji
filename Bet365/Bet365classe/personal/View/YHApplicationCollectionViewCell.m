//
//  YHApplicationCollectionViewCell.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/28.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "YHApplicationCollectionViewCell.h"

@interface YHApplicationCollectionViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UILabel *titileLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintHeight;
@end

@implementation YHApplicationCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setModel:(PrmtYHApplication *)model{
    _model = model;
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:model.banner] placeholderImage:nil];
    self.titileLabel.text = @"立即申请";
    self.titileLabel.backgroundColor = [UIColor skinViewKitSelColor];
    self.titileLabel.textColor = [UIColor skinViewKitNorColor];
}

@end
