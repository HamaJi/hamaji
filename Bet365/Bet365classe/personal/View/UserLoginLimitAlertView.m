//
//  UserLoginLimitAlertView.m
//  Bet365
//
//  Created by wind on 2020/2/2.
//  Copyright © 2020 jesse. All rights reserved.
//

#import "UserLoginLimitAlertView.h"
#import "UserLoginLimitHeaderView.h"
#import "UserLoginLimitAlertInputCell.h"
#import "UserLoginLimitFooterView.h"
#import "UITableView+Reuse.h"
#import "NetWorkMannager+Account.h"

@interface UserLoginLimitAlertView()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,assign) BOOL hasUserName;

@end

@implementation UserLoginLimitAlertView

- (instancetype)init{
    if (self = [super init]) {
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
        
        self.tableView = [[UITableView alloc] init];
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.sectionHeaderHeight = 50;
        self.tableView.sectionFooterHeight = 80;
        self.tableView.rowHeight = 44;
        self.tableView.scrollEnabled = NO;
        self.tableView.layer.masksToBounds = YES;
        self.tableView.layer.cornerRadius = 5;
        
        [self addSubview:self.tableView];
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(15);
            make.right.mas_offset(-15);
            make.centerY.mas_equalTo(self);
            make.height.mas_equalTo(50+80);
        }];
        
        [self.tableView jy_registerHeaderFooterClass:[UserLoginLimitHeaderView class]];
        [self.tableView jy_registerHeaderFooterClass:[UserLoginLimitFooterView class]];
        [self.tableView jy_registerClass:[UserLoginLimitAlertInputCell class]];
        
        self.dataArray = [NSMutableArray array];
    }
    return self;
}

- (void)setParams:(NSDictionary *)params{
    _params = params;
    
    if (params[@"account"]) {
        self.hasUserName = YES;
    }else{
        self.hasUserName = NO;
        
        {
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            [dic safeSetObject:@"account" forKey:@"keyName"];
            [dic safeSetObject:@"用户名" forKey:@"title"];
            [self.dataArray addObject:dic];
        }        
        
        {
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            [dic safeSetObject:@"password" forKey:@"keyName"];
            [dic safeSetObject:@"密码" forKey:@"title"];
            [self.dataArray addObject:dic];
        }
    }
}

- (void)setIpLocationVerifyFields:(NSArray *)ipLocationVerifyFields{
    _ipLocationVerifyFields = ipLocationVerifyFields;
    
    for (NSString *string in ipLocationVerifyFields) {
        NSArray *array = [string componentsSeparatedByString:@"="];

        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        [dic safeSetObject:array[0] forKey:@"keyName"];
        [dic safeSetObject:array[1] forKey:@"title"];
        [self.dataArray addObject:dic];
    }
    
    [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(50+80+self.dataArray.count*44);
    }];
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UserLoginLimitHeaderView *userLoginLimitHeaderView = [self.tableView jy_dequeueHeaderFooterClass:[UserLoginLimitHeaderView class]];
    [userLoginLimitHeaderView.cancelBT addTarget:self action:@selector(cancelBTAction:) forControlEvents:UIControlEventTouchUpInside];
    return userLoginLimitHeaderView;
    
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UserLoginLimitFooterView *userLoginLimitFooterView = [self.tableView jy_dequeueHeaderFooterClass:[UserLoginLimitFooterView class]];
    [userLoginLimitFooterView.kefuBT addTarget:self action:@selector(kefuBTAction:) forControlEvents:UIControlEventTouchUpInside];
    [userLoginLimitFooterView.confirmBT addTarget:self action:@selector(confirmBTAction:) forControlEvents:UIControlEventTouchUpInside];
    return userLoginLimitFooterView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserLoginLimitAlertInputCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserLoginLimitAlertInputCell"];
    cell.mutDic = self.dataArray[indexPath.row];
    return cell;
}

- (void)cancelBTAction:(id)sender{
    [self removeFromSuperview];
}

- (void)kefuBTAction:(id)sender {
    [self removeFromSuperview];
    [UIViewController routerJumpToUrl:BET_CONFIG.common_config.zxkfPath];
}

- (void)confirmBTAction:(id)sender{
    [self postUserVerifyIpLocationFields];
}

- (void)postUserVerifyIpLocationFields{
    NSMutableDictionary *mutDic = [NSMutableDictionary dictionary];
    for (NSInteger i=0; i<self.dataArray.count; i++) {
        NSDictionary *dic = self.dataArray[i];
        [mutDic safeSetObject:dic[@"content"] forKey:dic[@"keyName"]];
    }
    
    if (self.hasUserName) {
        if (mutDic.allKeys.count < self.ipLocationVerifyCount) {
            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"请输入至少%ld项",self.ipLocationVerifyCount]];
            return;
        }
    }else{
        if (!mutDic[@"account"]) {
            [SVProgressHUD showErrorWithStatus:@"请输入用户名"];
            return;
        }
        
        if (!mutDic[@"password"]) {
            [SVProgressHUD showErrorWithStatus:@"请输入用密码"];
            return;
        }
        
        if (mutDic.allKeys.count -2 < self.ipLocationVerifyCount) {
            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"请输入至少%ld项",self.ipLocationVerifyCount]];
            return;
        }
    }
    
    
    NSMutableDictionary *mutableDictionary = [NSMutableDictionary dictionary];
    
    if (self.hasUserName) {
        [mutableDictionary safeSetObject:self.params[@"account"] forKey:@"account"];
        [mutableDictionary safeSetObject:self.params[@"kLRKeyPwdWithoutMD5"] forKey:@"password"];
    }else{
        [mutableDictionary safeSetObject:mutDic[@"account"] forKey:@"account"];
        [mutableDictionary safeSetObject:mutDic[@"password"] forKey:@"password"];
    }
    
    [mutableDictionary safeSetObject:mutDic forKey:@"verifyValues"];
    
    [SVProgressHUD showWithStatus:@"加载中..."];
    [NET_DATA_MANAGER postUserVerifyIpLocationFields:mutableDictionary Success:^(id responseObject) {
        [SVProgressHUD dismiss];
        [SVProgressHUD showSuccessWithStatus:@"验证成功"];
        [self removeFromSuperview];
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

@end
