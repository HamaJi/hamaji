//
//  Bet365CpRecordTopView.m
//  Bet365
//
//  Created by luke on 2018/8/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Bet365CpRecordTopView.h"

@implementation Bet365CpRecordTopView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.backgroundColor = [UIColor skinViewBgColor];
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    searchBtn.layer.cornerRadius = 5;
    searchBtn.layer.masksToBounds = YES;
    searchBtn.backgroundColor = [UIColor whiteColor];
    UIImage *image = [[UIImage imageNamed:@"search"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [searchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    [searchBtn setTitleColor:[UIColor skinViewKitSelColor] forState:UIControlStateNormal];
    searchBtn.titleLabel.font = [UIFont systemFontOfSize:IS_IPHONE_5s ? 15 : 17];
    [searchBtn setImage:image forState:UIControlStateNormal];
    [searchBtn setImagePosition:LXMImagePositionLeft spacing:10];
    [searchBtn.imageView setTintColor:[UIColor skinViewKitSelColor]];
    [searchBtn addTarget:self action:@selector(searchClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:searchBtn];
    
    UIButton *cancelBtn = nil;
    if (BET_CONFIG.config.hy_is_cancel && [BET_CONFIG.config.hy_is_cancel isEqualToString:@"1"]) {
        cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancelBtn setTitle:@"撤单" forState:UIControlStateNormal];
        [cancelBtn setTitle:@"取消" forState:UIControlStateSelected];
        cancelBtn.layer.cornerRadius = 5;
        cancelBtn.layer.masksToBounds = YES;
        cancelBtn.backgroundColor = JesseColor(127, 140, 141);
        [cancelBtn addTarget:self action:@selector(cancelClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:cancelBtn];
        [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.trailing.equalTo(self).offset(-8);
            make.top.equalTo(self).offset(6);
            make.bottom.equalTo(self).offset(-6);
            make.width.mas_equalTo(80);
        }];
        self.cancelBtn = cancelBtn;
    }
    
    [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self).offset(8);
        make.top.equalTo(self).offset(8);
        make.bottom.equalTo(self).offset(-8);
        make.trailing.equalTo(self).offset(cancelBtn ? -100 : -8);
    }];
}

- (void)searchClick
{
    if ([self.delegate respondsToSelector:@selector(CpRecordTopViewSearch)]) {
        [self.delegate CpRecordTopViewSearch];
    }
}

- (void)cancelClick:(UIButton *)sender
{
    sender.selected = !sender.selected;
    if ([self.delegate respondsToSelector:@selector(CpRecordTopViewCancel:)]) {
        [self.delegate CpRecordTopViewCancel:sender.selected];
    }
}

@end
