//
//  Bet365SecurityTableViewCell.m
//  Bet365
//
//  Created by package_mr.chicken on 2019/1/15.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "Bet365SecurityTableViewCell.h"
#import "LukeMaskView.h"
@interface Bet365SecurityTableViewCell()
@property(nonatomic,copy)securityBlock statusComplement;
@property(nonatomic,copy)reloadCode codeStatusComplement;
@property(nonatomic, copy)NSString *key;
@end
@implementation Bet365SecurityTableViewCell
//初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self==[super initWithStyle:style
                   reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
/**void处理**/
-(void)SecurityType:(securityStatus)type ExhibTitle:(NSString *)title subtitle:(NSString *)titles keyC:(nonnull NSString *)keys forSecurityComplement:(nonnull securityBlock)complement reloadCode:(nonnull reloadCode)codeComplement{
    self.key = keys;
    self.statusComplement = complement;
    self.codeStatusComplement = codeComplement;
    //删除当前表层UI内容
    [self.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    self.contentView.userInteractionEnabled = YES;
    
    self.exbinView = (type == NormalSecurity_Type)?
    [LukeRegisterLbTFView registerLbTFViewWithTitle:title textEntry:NO placeholder:titles keyboardType:UIKeyboardTypeDefault] :
    [LukeRegisterLbTFView registerLbBtnTextFieldViewWithTitle:title VerificationBtnImage:@"vaid" target:self action:@selector(VerifyClick)];
    
    if ([keys isEqualToString:@"surepassword"]||[keys isEqualToString:@"password"]){
        self.exbinView.textfield.secureTextEntry = YES;
    }
    self.exbinView.userInteractionEnabled = YES;
    self.exbinView.layer.cornerRadius = 5;
    self.exbinView.layer.masksToBounds = YES;
    self.exbinView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
    self.exbinView.layer.borderWidth = 1;

    @weakify(self);
    [[self.exbinView.textfield rac_textSignal] subscribeNext:^(NSString * _Nullable x) {
        @strongify(self);
        self.statusComplement(x, self.key);
    }];
    [self.contentView addSubview:self.exbinView];
    [self.exbinView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(5);
        make.height.mas_equalTo(40);
        make.leading.mas_equalTo(self.contentView.mas_leading).offset(10);
        make.trailing.mas_equalTo(self.contentView.mas_trailing).offset(-10);
    }];
    //刷新验证码
    self.codeStatusComplement(self.exbinView.dateBtn);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/*codeRequest*/
-(void)VerifyClick{
    self.codeStatusComplement?self.codeStatusComplement(self.exbinView.dateBtn):nil;
}

@end
