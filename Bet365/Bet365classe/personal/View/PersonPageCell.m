//
//  PersonPageCell.m
//  Bet365
//
//  Created by HHH on 2018/8/5.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "PersonPageCell.h"
#import "PersonPageCellItemView.h"
#import <SDWebImage/UIButton+WebCache.h>
@interface PersonPageCell()
@property (weak, nonatomic) IBOutlet UIView *itemsContentView;

@end
@implementation PersonPageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self addObserver];
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor =[UIColor clearColor];
    self.itemsContentView.backgroundColor = [UIColor skinViewBgColor];

    // Initialization code
}
+(CGFloat )getCellHeightByItemsCount:(NSInteger)count{

    return (WIDTH - 20 ) / 4.0 * ceil(count / 4.0);
}

+(NSString *)getCellIdentifierByIndex:(NSInteger)idx{
    NSString *identitfier = [NSString stringWithFormat:@"%@%li", [self class],idx];
    return identitfier;
}

#pragma mark - Events
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - GET/SET
-(void)setSectionMenu:(CenterMenuSectionTemplate *)sectionMenu{
    if ([_sectionMenu isEqual:sectionMenu]) {
        return;
    }
    _sectionMenu = sectionMenu;
    [self.itemsContentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    @weakify(self);
    if (!self.itemsContentView.subviews.count) {
        CGFloat wh = (WIDTH - 20) / 4.0;
        [sectionMenu.menuList enumerateObjectsUsingBlock:^(CenterMenuItemTemplate * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            CGFloat top = idx / 4;
            CGFloat mould = idx % 4;
            PersonPageCellItemView *item = [PersonPageCellItemView instantiateFromNib];
            item.iconUrl = obj.icon;
            item.titile = obj.name;
            
            
            [self.itemsContentView addSubview:item];
            [item mas_makeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(wh, wh));
                make.left.equalTo(self.itemsContentView.mas_left).offset(wh * mould);
                make.top.equalTo(self.itemsContentView.mas_top).offset(wh * top);
            }];
            [item addTapGestureRecognizer:^(UITapGestureRecognizer *recognizer, NSString *gestureId) {
                NSInteger tag = [gestureId integerValue];
                @strongify(self);
                if (self.delegate && [self.delegate respondsToSelector:@selector(PersonPageCell:didTapItem:)]) {
                    [self.delegate PersonPageCell:self didTapItem:[self.sectionMenu.menuList safeObjectAtIndex:tag]];
                }
            } tapGestureId:[NSString stringWithFormat:@"%li",idx]];
        }];
        [self setRectCorner];
    }
}

-(void)addObserver{
    @weakify(self);
    [[RACObserve(USER_DATA_MANAGER, messegesCount) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        NSInteger count = [x integerValue];
        @strongify(self);
        [self.sectionMenu.menuList enumerateObjectsUsingBlock:^(CenterMenuItemTemplate * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            PersonPageCellItemView *itemView = [[self.itemsContentView subviews] safeObjectAtIndex:idx];
            if (itemView) {
                if (obj.isBuge) {
                    itemView.buge = count;
                }else{
                    itemView.buge = 0;
                }
            }
        }];
    }];
}
#pragma mark - Private
-(void)setRectCorner{
    self.itemsContentView.frame = CGRectMake(0, 0, WIDTH - 20, [PersonPageCell getCellHeightByItemsCount:self.itemsContentView.subviews.count]);
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.itemsContentView.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(8,8)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.itemsContentView.bounds;
    maskLayer.path = maskPath.CGPath;
    self.itemsContentView.layer.mask = maskLayer;
}
@end
