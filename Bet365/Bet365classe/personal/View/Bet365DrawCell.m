//
//  Bet365DrawCell.m
//  Bet365
//
//  Created by luke on 2018/8/9.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Bet365DrawCell.h"
#import "Bet365DrawModel.h"
#import "LukeUserAdapter.h"

@interface Bet365DrawCell ()
@property (weak, nonatomic) IBOutlet UILabel *resonLb;
@property (weak, nonatomic) IBOutlet UILabel *cashOrderNoLb;
@property (weak, nonatomic) IBOutlet UILabel *addTimeLb;
@property (weak, nonatomic) IBOutlet UILabel *operatorTimeLb;
@property (weak, nonatomic) IBOutlet UILabel *bankCardLb;

@property (weak, nonatomic) IBOutlet UILabel *cashMoneyTopLb;
@property (weak, nonatomic) IBOutlet UILabel *cashMoneyLb;

@property (weak, nonatomic) IBOutlet UILabel *counterFeeTopLb;
@property (weak, nonatomic) IBOutlet UILabel *counterFeeLb;

@property (weak, nonatomic) IBOutlet UILabel *approveMoneyTopLb;
@property (weak, nonatomic) IBOutlet UILabel *approveMoneyLb;
@property (weak, nonatomic) IBOutlet UILabel *memoLabel;

@end

@implementation Bet365DrawCell

- (void)awakeFromNib {
    self.resonLb.textColor = [UIColor skinTextItemNorColor];
    self.addTimeLb.textColor = [UIColor skinTextItemNorColor];
    self.operatorTimeLb.textColor = [UIColor skinTextItemNorColor];
    self.bankCardLb.textColor = [UIColor skinTextItemNorColor];
    self.cashMoneyLb.textColor = [UIColor skinTextItemNorColor];
    self.counterFeeLb.textColor = [UIColor skinTextItemNorColor];
    self.approveMoneyLb.textColor = [UIColor skinTextItemNorColor];
    self.cashMoneyTopLb.textColor = [UIColor skinTextItemNorColor];
    self.counterFeeTopLb.textColor = [UIColor skinTextItemNorColor];
    self.approveMoneyTopLb.textColor = [UIColor skinTextItemNorColor];
    self.memoLabel.textColor = [UIColor skinTextItemNorColor];

    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(Bet365DrawModel *)model
{
//    self.cashOrderNoLb.attributedText
    _model = model;
    NSString *cashStatus = nil;
    UIColor *color = nil;
    if (model.cashMode == 2){
        cashStatus = @"后台扣款";
        color = JesseColor(41, 140, 44);
        self.resonLb.text = nil;
        self.operatorTimeLb.text = [NSString stringWithFormat:@"出款时间 %@",model.operatorTime];
    }else if (model.cashStatus == 1 || model.cashStatus == 2) {
        cashStatus = @"受理中";
        color = JesseColor(252, 30, 44);
        self.resonLb.text = nil;
        self.operatorTimeLb.text = nil;
    }else if (model.cashStatus == 3){
        cashStatus = @"已出款";
        color = JesseColor(41, 140, 44);
        self.resonLb.text = nil;
        self.operatorTimeLb.text = [NSString stringWithFormat:@"出款时间 %@",model.operatorTime];
    }else if (model.cashStatus == 4){
        cashStatus = @"已取消";
        color = JesseGrayColor(149);
        self.resonLb.text = StringFormatWithStr(@"取消原因: ", [model.approveReason isNoBlankString] ? model.approveReason : @"");
        self.operatorTimeLb.text = [NSString stringWithFormat:@"取消时间 %@",model.operatorTime];
    }else if (model.cashStatus == 5){
        cashStatus = @"拒绝出款";
        color = JesseColor(252, 30, 44);
        self.resonLb.text = nil;
        self.operatorTimeLb.text = nil;
    }else{
        cashStatus = @"";
        color = JesseGrayColor(149);
        self.resonLb.text = nil;
        self.operatorTimeLb.text = nil;
    }
    self.cashOrderNoLb.attributedText = [NSMutableAttributedString setTitle:[NSString stringWithFormat:@"订单号 %@",model.cashOrderNo] colorTitle:cashStatus color:color];
    self.addTimeLb.text = [NSString stringWithFormat:@"订单时间 %@",model.addTime];
    if (model.bankCard) {
        self.bankCardLb.text = [NSString stringWithFormat:@"银行卡号 %@【%@】",model.bankCard,model.bankName];
    }else{
        self.bankCardLb.text = nil;
    }
    self.cashMoneyLb.text = [LukeUserAdapter decimalNumberWithId:[model.cashMoney doubleValue]];
    self.counterFeeLb.text = [LukeUserAdapter decimalNumberWithId:[model.counterFee doubleValue]];
    self.approveMoneyLb.text = [LukeUserAdapter decimalNumberWithId:[model.approveMoney doubleValue]];
    if (self.model.userMemo.length) {
        self.memoLabel.text = [NSString stringWithFormat:@"备注：%@",self.model.userMemo];
    }else{
        self.memoLabel.text = @"";
    }
    //现在接口获取的是业主后台备注的信息，而不是用户在提现时填写的备注
    //所以这里先不展示备注
    self.memoLabel.text = @"";
    self.memoLabel.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
