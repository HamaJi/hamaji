//
//  Bet365CpRecordTopView.h
//  Bet365
//
//  Created by luke on 2018/8/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol Bet365CpRecordTopViewDelegate <NSObject>

- (void)CpRecordTopViewSearch;

- (void)CpRecordTopViewCancel:(BOOL)isCancel;

@end

@interface Bet365CpRecordTopView : UIView
/** 代理 */
@property (nonatomic,weak) id<Bet365CpRecordTopViewDelegate>delegate;
/**  */
@property (nonatomic,weak) UIButton *cancelBtn;

@end
