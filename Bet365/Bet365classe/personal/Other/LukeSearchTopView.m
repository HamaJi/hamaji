//
//  LukeSearchTopView.m
//  Bet365
//
//  Created by luke on 17/7/9.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeSearchTopView.h"

#define BtnW WIDTH / 3
@implementation LukeSearchTopView

- (instancetype)initWithFrame:(CGRect)frame firstDateStr:(NSString *)firstDateStr secondDateStr:(NSString *)secondDateStr title:(NSString *)title
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor skinViewBgColor];
        [self setupUIWithfirstDateStr:firstDateStr secondDateStr:secondDateStr title:title];
    }
    return self;
}

- (void)setupUIWithfirstDateStr:(NSString *)firstDateStr secondDateStr:(NSString *)secondDateStr title:(NSString *)title
{
    
    LukeCustomBtn *firstDateBtn = [self addCustomButtonWithTitle:firstDateStr font:IS_IPHONE_5s ? 14 : 16 color:[UIColor skinTextItemNorColor] Image:@"arrow_sorted_down" target:self action:@selector(firstDateClick)];
    [firstDateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.centerX.equalTo(self).offset(-(BtnW - firstDateBtn.ycz_width) * 0.5 - BtnW * 0.5);
        make.centerY.equalTo(self);
    }];
    self.firstDateBtn = firstDateBtn;
    
    UIView *lineView1 = [self addLineView];
    [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(8);
        make.bottom.equalTo(self).offset(-8);
        make.width.mas_equalTo(1);
        make.centerX.equalTo(self).offset(-BtnW * 0.5);
    }];
    
    LukeCustomBtn *secondDateBtn = [self addCustomButtonWithTitle:secondDateStr font:IS_IPHONE_5s ? 14 : 16 color:[UIColor skinTextItemNorColor] Image:@"arrow_sorted_down" target:self action:@selector(secondDateClick)];
    [secondDateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.center.equalTo(self);
    }];
    self.secondDateBtn = secondDateBtn;
    
    UIView *lineView2 = [self addLineView];
    [lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self).offset(BtnW * 0.5);
        make.top.equalTo(self).offset(8);
        make.bottom.equalTo(self).offset(-8);
        make.width.mas_equalTo(1);
    }];
    
    UIButton *searchBtn = [self addButtonWithTitle:title font:IS_IPHONE_5s ? 14 : 16 color:[UIColor skinTextItemNorColor] borderColor:[UIColor skinTextItemNorSubColor] target:self action:@selector(searchClick)];
    [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(lineView2.mas_trailing).offset(10);
        make.top.equalTo(self).offset(6);
        make.bottom.equalTo(self).offset(-6);
        make.trailing.equalTo(self).offset(-10);
    }];
    
    UIView *bottomLineView = [self addLineView];
    [self addSubview:bottomLineView];
    [bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.bottom.equalTo(self);
        make.height.mas_equalTo(1);
    }];
}

- (void)firstDateClick
{
    if ([self.delegate respondsToSelector:@selector(SearchTopViewfirstDateButtonClick)]) {
        [self.delegate SearchTopViewfirstDateButtonClick];
    }
}

- (void)secondDateClick
{
    if ([self.delegate respondsToSelector:@selector(SearchTopViewsecondDateButtonClick)]) {
        [self.delegate SearchTopViewsecondDateButtonClick];
    }
}

- (void)searchClick
{
    if ([self.delegate respondsToSelector:@selector(SearchTopViewSearchButtonClick)]) {
        [self.delegate SearchTopViewSearchButtonClick];
    }
}

//创建线条
- (UIView *)addLineView
{
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = [UIColor skinViewKitNorColor];
    [self addSubview:lineView];
    return lineView;
}

#pragma mark - 创建button
- (UIButton *)addButtonWithTitle:(NSString *)title font:(CGFloat)font color:(UIColor *)color borderColor:(UIColor *)borderColor target:(id)target action:(SEL)action
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font = [UIFont systemFontOfSize:font];
    [btn setTitle:title forState:UIControlStateNormal];
    btn.layer.borderWidth = 1;
    btn.layer.borderColor = borderColor.CGColor;
    btn.layer.cornerRadius = 3;
    btn.layer.masksToBounds = YES;
    [btn setTitleColor:color forState:UIControlStateNormal];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn];
    return btn;
}

- (LukeCustomBtn *)addCustomButtonWithTitle:(NSString *)title font:(CGFloat)font color:(UIColor *)color Image:(NSString *)image target:(id)target action:(SEL)action
{
    LukeCustomBtn *btn = [LukeCustomBtn buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font = [UIFont systemFontOfSize:font];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    [btn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 5, 0)];
    [btn setTitleColor:color forState:UIControlStateNormal];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn];
    return btn;
}
@end
