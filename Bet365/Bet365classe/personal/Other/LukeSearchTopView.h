//
//  LukeSearchTopView.h
//  Bet365
//
//  Created by luke on 17/7/9.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LukeCustomBtn.h"

@protocol LukeSearchTopViewDelegate <NSObject>

@optional
- (void)SearchTopViewfirstDateButtonClick;

@optional
- (void)SearchTopViewsecondDateButtonClick;

- (void)SearchTopViewSearchButtonClick;

@end

@interface LukeSearchTopView : UIView

- (instancetype)initWithFrame:(CGRect)frame firstDateStr:(NSString *)firstDateStr secondDateStr:(NSString *)secondDateStr title:(NSString *)title;
/** 开始时间的按钮 */
@property (nonatomic,weak) LukeCustomBtn *firstDateBtn;
/** 结束时间的按钮 */
@property (nonatomic,weak) LukeCustomBtn *secondDateBtn;
/** 代理 */
@property (nonatomic,weak) id<LukeSearchTopViewDelegate>delegate;

@end
