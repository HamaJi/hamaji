//
//  LukeDateSearchView.m
//  Bet365
//
//  Created by luke on 17/7/9.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeDateSearchView.h"

@implementation LukeDateSearchView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.backgroundColor = [JesseGrayColor(10) colorWithAlphaComponent:0.3];
    
    UIView *contentView = [[UIView alloc] init];
    contentView.backgroundColor = [UIColor whiteColor];
    [self addSubview:contentView];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.backgroundColor = [UIColor whiteColor];
    [datePicker addTarget:self action:@selector(dateChange:) forControlEvents:UIControlEventValueChanged];
    [contentView addSubview:datePicker];
    [datePicker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.bottom.equalTo(contentView);
        make.height.mas_equalTo(HEIGHT * 0.5);
    }];
    self.datePicker = datePicker;
    
    UIButton *cancelBtn = [self addButtonWithTitle:@"取消" font:15 color:JesseColor(4, 190, 2) image:nil target:self action:@selector(cancelClick)];
    [contentView addSubview:cancelBtn];
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(datePicker.mas_top);
        make.leading.equalTo(contentView).offset(10);
    }];
    
    UIButton *okBtn = [self addButtonWithTitle:@"确认" font:15 color:JesseColor(4, 190, 2) image:nil target:self action:@selector(okClick)];
    [contentView addSubview:okBtn];
    [okBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(datePicker.mas_top);
        make.trailing.equalTo(contentView).offset(-10);
    }];
    
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.bottom.equalTo(self);
        make.height.mas_equalTo(HEIGHT * 0.55);
    }];
}

- (void)dateChange:(UIDatePicker *)datePicker
{
    if ([self.delegate respondsToSelector:@selector(DateSearchViewChangeWithDatePicker:)]) {
        [self.delegate DateSearchViewChangeWithDatePicker:datePicker];
    }
}

- (void)cancelClick
{
    if ([self.delegate respondsToSelector:@selector(DateSearchViewcancelClick)]) {
        [self.delegate DateSearchViewcancelClick];
    }
}

- (void)okClick
{
    if ([self.delegate respondsToSelector:@selector(DateSearchViewOkClick)]) {
        [self.delegate DateSearchViewOkClick];
    }
}

#pragma mark - 创建button
- (UIButton *)addButtonWithTitle:(NSString *)title font:(CGFloat)font color:(UIColor *)color image:(NSString *)image target:(id)target action:(SEL)action
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font = [UIFont systemFontOfSize:font];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    [btn setTitleColor:color forState:UIControlStateNormal];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return btn;
}
@end
