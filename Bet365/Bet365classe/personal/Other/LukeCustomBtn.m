//
//  LukeCustomBtn.m
//  Bet365
//
//  Created by luke on 17/6/18.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeCustomBtn.h"

@implementation LukeCustomBtn

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.titleLabel.ycz_x = 0;
    //[self.titleLabel sizeToFit];
    self.imageView.ycz_x = CGRectGetMaxX(self.titleLabel.frame) + 5;
}

@end
