//
//  LukeDateSearchView.h
//  Bet365
//
//  Created by luke on 17/7/9.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LukeDateSearchViewDelegate <NSObject>

- (void)DateSearchViewChangeWithDatePicker:(UIDatePicker *)datePicker;

- (void)DateSearchViewcancelClick;

- (void)DateSearchViewOkClick;

@end

@interface LukeDateSearchView : UIView
/** 代理 */
@property (nonatomic,weak) id<LukeDateSearchViewDelegate>delegate;
/**  */
@property (nonatomic,weak) UIDatePicker *datePicker;

@end
