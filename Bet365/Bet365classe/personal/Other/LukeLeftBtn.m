//
//  LukeLeftBtn.m
//  Bet365
//
//  Created by luke on 17/6/27.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeLeftBtn.h"

@implementation LukeLeftBtn

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.titleLabel.ycz_x = 0;
    self.imageView.ycz_x = self.ycz_width - self.imageView.ycz_width;
}

@end
