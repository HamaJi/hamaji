//
//  Bet365LivePageData.m
//  Bet365
//
//  Created by luke on 2018/6/23.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Bet365LivePageData.h"
#import "Bet365ElectronicModel.h"
#import "NetWorkMannager+Account.h"

static Bet365LivePageData *_live = nil;

@implementation Bet365LivePageData

+ (instancetype)shareIntance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _live = [[Bet365LivePageData alloc] init];
    });
    return _live;
}

- (void)queryDataWithLiveCode:(NSString *)liveCode Completed:(void (^)())completed
{
    [NET_DATA_MANAGER requestGetLiveQstPage:1 Rows:5000 liveCode:liveCode Success:^(id responseObject) {
        _live.data = [Bet365ElectronicModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
        completed ? completed() : nil;
    } failure:^(NSError *error) {
        completed ? completed() : nil;
    }];
}

@end
