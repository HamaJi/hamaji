//
//  YEBaoTransferViewController.m
//  Bet365
//
//  Created by super_小鸡 on 2019/6/9.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "YEBaoTransferViewController.h"

@interface YEBaoTransferViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *topContentView;
@property (weak, nonatomic) IBOutlet UIView *bottomContentView;

@property (weak, nonatomic) IBOutlet UILabel *titileLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLeadingLabel;

@property (weak, nonatomic) IBOutlet UITextField *moneyTf;
@property (weak, nonatomic) IBOutlet UIButton *allBtn;

@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UITextField *pswTextField;

@property (weak, nonatomic) IBOutlet UILabel *errorMsgLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomLayoutFConstraintHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pswLayoutFConstraintHeight;
@property (weak, nonatomic) IBOutlet UIView *pswContentView;
@end

@implementation YEBaoTransferViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titileLabel.textColor = [UIColor skinTextItemNorColor];
    self.moneyLeadingLabel.textColor = [UIColor skinTextItemNorColor];
    self.moneyTf.textColor = [UIColor skinTextItemNorSubColor];
    self.allBtn.titleLabel.textColor = [UIColor skinViewKitSelColor];
    self.submitBtn.titleLabel.textColor = [UIColor skinViewKitNorColor];
    [self.submitBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];
    self.errorMsgLabel.textColor = [UIColor skinViewKitSelColor];
    self.topContentView.backgroundColor = [UIColor skinViewBgColor];
    self.bottomContentView.backgroundColor = [UIColor skinViewBgColor];
    self.pswContentView.backgroundColor = [UIColor skinViewBgColor];
    self.pswTextField.textColor = [UIColor skinTextItemNorColor];
    NSAttributedString *pswString = [[NSAttributedString alloc] initWithString:@"请输入取款密码" attributes:@{NSForegroundColorAttributeName : [UIColor skinTextItemNorSubColor],NSFontAttributeName : [UIFont systemFontOfSize:15]}];
    
    self.pswTextField.attributedPlaceholder = pswString;
//    UILabel *pswLabel = [self.pswTextField valueForKey:@"_placeholderLabel"];
//    pswLabel.adjustsFontSizeToFitWidth = YES;
    
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    if (self.inBlock) {
        self.titileLabel.text = @"转入金额(元)";
        [self.submitBtn setTitle:@"确认转入" forState:UIControlStateNormal];
        [self.submitBtn setTitle:@"确认转入" forState:UIControlStateDisabled];
    }else if (self.outBlock){
        self.titileLabel.text = @"转出金额(元)";
        [self.submitBtn setTitle:@"确认转出" forState:UIControlStateNormal];
        [self.submitBtn setTitle:@"确认转出" forState:UIControlStateDisabled];
    }
    [self addObserver];
    [self instanNaviBar];
    // Do any additional setup after loading the view from its nib.
}

-(void)instanNaviBar{
    self.naviRightItem = Bet365NaviRightItemBalance;
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setTitle:@"返回" forState:UIControlStateNormal];
    UIImage *img = [UIImage imageNamed:@ "left_lage" ];
    img = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [leftBtn setImage:img forState:UIControlStateNormal];
    [leftBtn setTintColor:[UIColor skinNaviTintColor]];
    [leftBtn setTitleColor:[UIColor skinNaviTintColor] forState:UIControlStateNormal];
    [leftBtn setImagePosition:LXMImagePositionLeft spacing:5];
    [leftBtn addTarget:self action:@selector(dismissAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftButton;
}

#pragma mark - Private
-(void)addObserver{
    @weakify(self);
    [[RACSignal combineLatest:@[[RACObserve(USER_DATA_MANAGER.userInfoData, money) distinctUntilChanged],
                                [RACObserve(self, info) distinctUntilChanged]]] subscribeNext:^(RACTuple * _Nullable x) {
        @strongify(self);
        if (self.inBlock) {
            double diffrenceMoney = [self.info.maxMoney doubleValue] -
            [[self.info.money decimalNumberByRoundingDownString] doubleValue];
            if (diffrenceMoney > 0) {
                if ([[USER_DATA_MANAGER.userInfoData.money decimalNumberByRoundingDown] doubleValue] - diffrenceMoney < 0) {
                    self.moneyTf.placeholder = [NSString stringWithFormat:@"本次最多可转入:%@",[USER_DATA_MANAGER.userInfoData.money decimalNumberByRoundingDownString]];
                }else{
//                    NSNumber *max = [NSNumber numberWithDouble:diffrenceMoney];
//                    self.moneyTf.placeholder = [NSString stringWithFormat:@"本次最多可转入:%@",[max decimalNumberByRoundingDownString]];
                    NSString *max = StringFormatWithFloat(diffrenceMoney);
                    self.moneyTf.placeholder = [NSString stringWithFormat:@"本次最多可转入:%@",[max decimalNumberRoundingString]];
                }
                
            }else{
                self.moneyTf.placeholder = @"本次最多可转入0元";
                self.errorMsgLabel.text = [NSString stringWithFormat:@"已超出余额宝累计最大限额:%@",[self.info.maxMoney decimalNumberByRoundingDownString]];
            }
        }else if (self.outBlock){
            self.moneyTf.placeholder = [NSString stringWithFormat:@"本次最多可转出:%@",[NSString roundingWithFloat:self.info.money]];
        }
    }];
    
    [[RACObserve(self.errorMsgLabel, text) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        [UIView animateWithDuration:0.2 animations:^{
            if ([x length]) {
                self.bottomLayoutFConstraintHeight.constant = 35.0;
            }else{
                self.bottomLayoutFConstraintHeight.constant = 0.0;
            }
            [self.view layoutIfNeeded];
        }];
    }];

    self.moneyTf.delegate = self;

}

-(BOOL)primiteAmound:(NSString *)text{
    NSNumber *money = [text decimalNumberRounding];
    BOOL overstepBalance = NO;
    BOOL overstepMax = NO;
    BOOL isNumber = [[text decimalNumberRoundingString] isShouldNumber];
    if (self.inBlock) {
        overstepBalance = ([USER_DATA_MANAGER.userInfoData.money compare:money] == NSOrderedAscending);
        NSNumber *totalModel = [NSNumber numberWithDouble:([money doubleValue] + [[self.info.money decimalNumberByRoundingDownString] doubleValue])];
        overstepMax = ([self.info.maxMoney compare:totalModel] == NSOrderedAscending);
    }else if (self.outBlock){
        overstepBalance = ([self.info.money compare:money] == NSOrderedAscending);
    }
    [self.submitBtn setEnabled:!overstepBalance && isNumber && !overstepMax];
    
    if (overstepMax){
        self.errorMsgLabel.text = [NSString stringWithFormat:@"已超出余额宝累计最大限额:%@",[self.info.maxMoney decimalNumberByRoundingDownString]];
        return NO;
    }
    if (![text length]) {
        self.errorMsgLabel.text = @"";
    }
    if (!isNumber) {
        self.errorMsgLabel.text = @"输入金额不合法";
        return NO;
    }
    {
        NSInteger flag = 0;
        const NSInteger limited = 2;
        for (NSInteger i = text.length - 1; i >= 0; i--) {
            if ([text characterAtIndex:i] == '.') {
                if (flag > limited) {
                    [SVProgressHUD showErrorWithStatus:@"输入金额最小为【分】"];
                    return NO;
                }
                break;
            }
            flag++;
        }
    }
    if (overstepBalance){
        self.errorMsgLabel.text = @"输入金额超出";
        return NO;
    }
    self.errorMsgLabel.text = @"";
    return YES;
    
}

#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (!string.length) {
        return YES;
    }
    NSMutableString *text = textField.text.mutableCopy;
    [text replaceCharactersInRange:range withString:string];
    return [self primiteAmound:text];
}
#pragma mark - Events

-(void)dismissAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)allAction:(id)sender {
    if (self.inBlock) {
        double diffrenceMoney = [self.info.maxMoney doubleValue] - [[self.info.money decimalNumberByRoundingDownString] doubleValue];
        if (diffrenceMoney > 0) {
            if ([[USER_DATA_MANAGER.userInfoData.money decimalNumberByRoundingDown] doubleValue] - diffrenceMoney < 0) {
                self.moneyTf.text = [USER_DATA_MANAGER.userInfoData.money decimalNumberByRoundingDownString];
            }else{
                NSNumber *max = [NSNumber numberWithDouble:diffrenceMoney];
                self.moneyTf.text = [max decimalNumberByRoundingDownString];
            }
            
        }else{
            return;
        }
    }else if (self.outBlock){
        self.moneyTf.text = [NSString roundingWithFloat:self.info.money];
    }
    [self primiteAmound:self.moneyTf.text];
}
- (IBAction)submitAction:(id)sender {
    if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"VHY"]) {
        [SVProgressHUD showErrorWithStatus:@"您的账号不能使用余额宝，请联系客服处理"];
        return;
    }
    NSString *palading = self.inBlock != nil ? @"转入" : @"转出";
    
    if (!self.moneyTf.text.length) {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"请输入%@金额",palading]];
        return;
    }else if (![self.moneyTf.text isShouldNumber]){
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"您输入的%@金额不合法",palading]];
        return;
    }
    double money = [[self.moneyTf.text decimalNumberRounding] doubleValue];
    if (self.inBlock &&
              [[USER_DATA_MANAGER.userInfoData.money decimalNumberByRoundingDown] doubleValue] < money){
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"您输入%@金额超支",palading]];
        return;
    }else if (self.outBlock &&
              [[self.info.money decimalNumberByRoundingDown] doubleValue] < money){
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"您输入%@金额超支",palading]];
        return;
    }
    
    if (self.outBlock) {
        if (!self.pswTextField.text.length) {
            [SVProgressHUD showErrorWithStatus:@"请输入取款密码"];
            return;
        }
        self.outBlock(money,self.pswTextField.text);
    }else if (self.inBlock){
        self.inBlock(money);
        
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)setOutBlock:(YEBaoTransferOutBlock)outBlock{
    _outBlock = outBlock;
    if (_outBlock) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.pswLayoutFConstraintHeight.constant = 35.0;
        });
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
