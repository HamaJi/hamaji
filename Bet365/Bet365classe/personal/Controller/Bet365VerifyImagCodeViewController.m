//
//  Bet365VerifyImagCodeViewController.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/12/16.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "Bet365VerifyImagCodeViewController.h"
#import "PresentingBottomAnimator.h"
#import "DismissingBottomAnimator.h"
#import "NetWorkMannager+Account.h"
#import <pop/POP.h>

@interface Bet365VerifyImagCodeViewController ()

@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UILabel *titileLabel;

@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@property (weak, nonatomic) IBOutlet UITextField *textField;

@property (weak, nonatomic) IBOutlet UIImageView *imagView;

@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@property (nonatomic, strong) POPBasicAnimation *sizeAnimation;

@property (nonatomic, strong) POPBasicAnimation *cornerRadiusAnimation;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintBtnHeight;

@property (nonatomic,strong)NSURLSessionTask *imageCodeTask;

@end

@implementation Bet365VerifyImagCodeViewController

- (void)viewDidLoad {

    [super viewDidLoad];
    [self setUI];
    [self updateImageCode];
    @weakify(self);
    [self.textField.rac_textSignal subscribeNext:^(NSString * _Nullable x) {
        @strongify(self);
        self.layoutConstraintBtnHeight.priority = 1000;
        if ([x length] >= 4 && self.layoutConstraintBtnHeight.constant != 40) {
            self.layoutConstraintBtnHeight.constant = 40;
            [self.submitBtn.layer pop_addAnimation:self.sizeAnimation forKey:@"sizeAnimation"];
        }
    }];;
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - Events
- (IBAction)submitAction:(id)sender {
    if (!self.submitBlock) {
        return;
    }
    if (!self.textField.text.length ) {
        return;
    }
    BOOL dismiss = self.submitBlock(self.textField.text);
    if (dismiss) {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }
}

#pragma mark - Private
-(void)setUI{
    self.imagView.image = [UIImage imageNamed:@"vaid"];
    self.contentView.backgroundColor = [UIColor skinViewScreenColor];
    self.titileLabel.textColor = [UIColor skinViewKitSelColor];
    self.textField.textColor = [UIColor skinTextItemNorColor];
    NSAttributedString *string = [[NSAttributedString alloc] initWithString:@"请输入验证码" attributes:@{NSForegroundColorAttributeName : [UIColor skinTextItemNorSubColor],NSFontAttributeName : [UIFont systemFontOfSize:15]}];
    self.textField.attributedPlaceholder = string;
    [self.submitBtn setTintColor:[UIColor skinViewKitSelColor]];
    [self.submitBtn setImage:[[UIImage imageNamed:@"send"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.submitBtn setBackgroundColor:[UIColor skinViewKitNorColor]];
    self.submitBtn.borderColor = [UIColor skinViewKitSelColor];
    [self.closeBtn setTintColor:[UIColor skinViewKitSelColor]];
    [self.closeBtn setImage:[[UIImage imageNamed:@"chat_close_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    
}

-(void)addObserver{
    @weakify(self);
    [self.imagView addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
        @strongify(self);
        [self updateImageCode];
        self.resetBlock ? self.resetBlock() : nil;
    }];
    [self.textField.rac_textSignal subscribeNext:^(NSString * _Nullable x) {
        @strongify(self);
        [self.submitBtn setEnabled:!x.length];
    }];
}

- (IBAction)closeAction:(id)sender {
    [self.imageCodeTask cancel];
    [self.submitBtn.layer pop_removeAllAnimations];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)updateImageCode{
    self.textField.text = @"";
    @weakify(self);
    self.imageCodeTask = [NET_DATA_MANAGER requestGetVCodeByRandom:YES Success:^(id responseObject) {
        @strongify(self);
        if (responseObject) {
            if([responseObject isKindOfClass:[UIImage class]]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([responseObject isKindOfClass:[UIImage class]]) {
                        self.imagView.image = responseObject;
                    }else{
                        self.imagView.image = [UIImage imageNamed:@"vaid"];
                    }
                });
            }
        }
    } failure:^(NSError *error) {
        self.imagView.image = [UIImage imageNamed:@"vaid"];
    }];
}
#pragma mark - UIViewControllerTransitioningDelegate
- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source{
    
    return [PresentingBottomAnimator new];
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    return [DismissingBottomAnimator new];
}

#pragma mark - GET/SET
-(POPBasicAnimation *)sizeAnimation{
    if (!_sizeAnimation) {
        _sizeAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerSize];
        _sizeAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(self.submitBtn.bounds.size.width, 40.0)];
        _sizeAnimation.fromValue = [NSValue valueWithCGSize:CGSizeMake(40.0, 40.0)];
        _sizeAnimation.duration = 0.75;
        @weakify(self)
        [_sizeAnimation setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
            @strongify(self);
            if (finished) {
                [self.submitBtn.layer pop_addAnimation:self.cornerRadiusAnimation forKey:@"cornerRadiusAnimation"];
            }
        }];
    }
    return _sizeAnimation;
}

-(POPBasicAnimation *)cornerRadiusAnimation{
    if (!_cornerRadiusAnimation) {
        _cornerRadiusAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerCornerRadius];
        _cornerRadiusAnimation.toValue = @(8.0);
        _cornerRadiusAnimation.fromValue = @(20.0);
        _cornerRadiusAnimation.duration = 0.25;
    }
    return _cornerRadiusAnimation;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

