//
//  jessepersonProtocolBet365ViewController.m
//  Bet365
//
//  Created by jesse on 2017/4/15.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "jessepersonProtocolBet365ViewController.h"
#import "jessepersonProtocolBet365TableViewCell.h"
static NSString *const registerProtocolCell = @"protocolCell";
@interface jessepersonProtocolBet365ViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *dataTableview;
@property(nonatomic,copy)NSString *Protocol;
@end

@implementation jessepersonProtocolBet365ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    [self setNav];
}
#pragma mark--setNav
-(void)setNav
{
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(0, 20, WIDTH, 40)];
    backView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:backView];
    UIButton *leftB = [UIButton buttonWithType:UIButtonTypeCustom];
    leftB.frame = CGRectMake(10, 10, 20, 20);
    [leftB setImage:[UIImage imageNamed:@"left_lage_select"] forState:UIControlStateNormal];
    [leftB addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [backView addSubview:leftB];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(40, 10, 100, 20)];
    label.font = [UIFont systemFontOfSize:16];
    label.text = @"用户协议";
    label.adjustsFontSizeToFitWidth = YES;
    label.textColor = [UIColor blackColor];
    label.textAlignment = NSTextAlignmentLeft;
    [backView addSubview:label];
    [self dataTableview];
}
#pragma mark--dissmiss
-(void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark--tableviewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect bounds = [self.Protocol boundingRectWithSize:CGSizeMake(WIDTH-16, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesDeviceMetrics attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil];
    return bounds.size.height+30;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    jessepersonProtocolBet365TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:registerProtocolCell forIndexPath:indexPath];
    [cell updateDesProtocolBy:self.Protocol];
    return cell;
}
#pragma mark--初始化tab
-(UITableView *)dataTableview
{
    if (_dataTableview==nil) {
        _dataTableview = [[UITableView alloc]initWithFrame:CGRectMake(0, 70, WIDTH, HEIGHT-70
                                                                      ) style:UITableViewStylePlain];
        _dataTableview.backgroundColor = [UIColor skinViewScreenColor];
        _dataTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
        _dataTableview.dataSource = self;
        _dataTableview.delegate = self;
        _dataTableview.allowsSelection = NO;
        _dataTableview.showsVerticalScrollIndicator = NO;
        _dataTableview.showsHorizontalScrollIndicator = NO;
        [self.view addSubview:_dataTableview];
        [_dataTableview registerClass:[jessepersonProtocolBet365TableViewCell class] forCellReuseIdentifier:registerProtocolCell];
    }
    return _dataTableview;
}
-(NSString *)Protocol
{
    return @"* 為避免於本網站投注時之爭議，請會員務必於進入網站前詳閱本娛樂场所定之各項規則，客戶一經「我同意」進入本網站進行投注時，即被為已接受本娛樂场的所有協議與規則。\n * 會員有責任確保自己的帳戶以及登入資料的保密性，以會員帳號及密碼進行的任何網上投注，將被視為有效。敬請不定時做密碼變更之動作。若帳號密碼被盜用，進行的投注，本公司一概不負賠償責任。\n*本公司保留不定時更改本協定或遊戲規則或保密條例，更改之條款將從更改發生後指定之日起生效，並保留一切有爭議事項及最後的決策權。\n* 用戶須達到居住地國家法律規定之合法年齡方可使用線上娛樂場或網站。\n*網上投注如未能成功提交，投注將被視為無效。\n* 凡玩家於出牌途中且尚無結果前自動或強制斷線時，並不影響比賽之結果。\n*如遇發生不可抗拒之災害，駭客入侵，網絡問題造成數據丟失的情況，以本公司公告為最終方案。\n* 特此聲明，本公司將會對所有的電子交易進行記錄，如有任何爭議，本公司將會以注單記錄為準。\n* 本公司保留更改、修改現有條款或增加任何適當條款的權利！\n* 無論在任何情況下，本网站具有最終的解釋權。\n*无风险投注：为了更好的完善长久合作关系，公司将严厉禁止个人/团体无风险投注. (1)例如同时投注单双或是同时投注位数超过2/3以上的号码,禁止会员存在同一项游戏同局购买同一个位数【单，小】，【双，大】【庄，闲】【红，黑】对冲或对打投注，该买法也将视为无风险投注,敬请注意下级会员是否存在洗码套取公司相关代理与会员优惠 一经发现将永久取消代理与会员的会员资格没收所有资金.(2)例如，轮盘游戏中轮盘下注不可超过二分之一，换成号码数不得超过一半哦，超过规定视为无风险投注，谢谢！\n* 敬请注意下级会员是否存在洗码套取公司相关代理与会员优惠 一经发现将永久取消代理与会员的会员资格\n* 若經本公司發現會員以不正當手法'利用外掛程式'進行投注或以任何非正常方式進行的個人、團體投注有損公司利益之投注情事發生，本公司保留權利取消該類注單以及注單產生之紅利，並停用該會員帳號。\n*若本公司發現會員有重複申請帳號行為時，保留取消、收回會員所有優惠紅利，以及優惠紅利所產生的盈利之權利。每位玩家、每一住址、每一電子郵箱、每一電話號碼、相同支付卡/信用卡號碼，以及共享電腦環境(例如:網吧、其他公共用電腦等)只能夠擁有一個會員帳號，各項優惠只適用於每位客戶在本公司唯一的帳戶。若本公司发现会员有重复申请帐号行为时，保留取消、收回会员所有优惠红利，以及优惠红利所产生的盈利之权利。\n* 本公司若发现您在同系统的娱乐城上开设多个会员账户，并进行套利下注；本公司有权取消您的会员账号并将所有下注营利取消!";
}
@end
