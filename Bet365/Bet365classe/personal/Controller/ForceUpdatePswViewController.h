//
//  ForceUpdatePswViewController.h
//  Bet365
//
//  Created by adnin on 2019/4/3.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ForceUpdatePswSubmitHandle)(NSString *newPsw);

typedef void(^ForceUpdatePswCloseHandle)();

typedef void(^ForceUpdatePswServerHandle)();

@interface ForceUpdatePswViewController : Bet365ViewController

@property (nonatomic,copy)ForceUpdatePswSubmitHandle submitHandle;

@property (nonatomic,copy)ForceUpdatePswCloseHandle closeHandle;

@property (nonatomic,copy)ForceUpdatePswServerHandle serverHandle;


@end
