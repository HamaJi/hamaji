//
//  LukeForgetViewController.m
//  Bet365
//
//  Created by luke on 17/6/12.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeForgetViewController.h"
#import "LukeRegisterLbTFView.h"
#import "LukeRegisterViewController.h"
#import "UIViewControllerSerializing.h"

@interface LukeForgetViewController ()<UITextFieldDelegate,UIViewControllerSerializing>
/** 取款密码 */
@property (nonatomic,weak) LukeRegisterLbTFView *passwordView;
/** 会员帐号 */
@property (nonatomic,weak) LukeRegisterLbTFView *nameView;
/** 验证码 */
@property (nonatomic,weak) LukeRegisterLbTFView *VerifyView;

@end

RouterKey *const kRouterForget = @"forget";

@implementation LukeForgetViewController

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    if (BET_CONFIG.common_config.forgetPasswordTip.length > 0) {
        [[NAVI_MANAGER getCurrentVC] alertWithSureTitle:@"温馨提示" message:BET_CONFIG.common_config.forgetPasswordTip];
        return NO;
    }
    return YES;
}

+(void)load{
    [self registerJumpRouterKey:kRouterForget toHandle:^(NSDictionary *parameters) {
        LukeForgetViewController *forgetVC = [[LukeForgetViewController alloc] init];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:forgetVC animated:YES];
        forgetVC.hidesBottomBarWhenPushed = YES;
    }];
    
    [self registerInstanceRouterKey:kRouterForget toHandle:^UIViewController *(NSDictionary *parameters) {
        LukeForgetViewController *forgetVC = [[LukeForgetViewController alloc] init];
        return forgetVC;
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    self.view.backgroundColor = [UIColor skinViewScreenColor];
}

- (void)setupUI
{
    UILabel *titleLb = [UILabel addLabelWithFont:15 color:[UIColor grayColor] title:@"请先核对你的取款密码"];
    [self.view addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.view).offset(10);
        make.top.equalTo(self.view).offset(0 + 10);
    }];
    
    LukeRegisterLbTFView *nameView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"会员帐号" textEntry:NO placeholder:nil keyboardType:UIKeyboardTypeDefault];
    nameView.layer.cornerRadius = 5;
    nameView.layer.masksToBounds = YES;
    nameView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
    nameView.layer.borderWidth = 1;
    nameView.textfield.delegate = self;
    [self.view addSubview:nameView];
    [nameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLb.mas_bottom).offset(5);
        make.width.mas_equalTo(WIDTH - 20);
        make.leading.equalTo(self.view).offset(10);
        make.height.mas_equalTo(40);
    }];
    self.nameView = nameView;
    
    LukeRegisterLbTFView *passwordView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"取款密码" textEntry:YES placeholder:nil keyboardType:UIKeyboardTypeNumberPad];
    passwordView.layer.cornerRadius = 5;
    passwordView.layer.masksToBounds = YES;
    passwordView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
    passwordView.layer.borderWidth = 1;
    passwordView.textfield.delegate = self;
    [self.view addSubview:passwordView];
    [passwordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(nameView.mas_bottom).offset(5);
        make.width.mas_equalTo(WIDTH - 20);
        make.leading.equalTo(self.view).offset(10);
        make.height.mas_equalTo(40);
    }];
    self.passwordView = passwordView;
    
    LukeRegisterLbTFView *VerifyView = [LukeRegisterLbTFView registerLbBtnTextFieldViewWithTitle:@"验证码" VerificationBtnImage:@"vaid" target:self action:@selector(queryData)];
    VerifyView.layer.cornerRadius = 5;
    VerifyView.layer.masksToBounds = YES;
    VerifyView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
    VerifyView.layer.borderWidth = 1;
    [self.view addSubview:VerifyView];
    VerifyView.textfield.delegate = self;
    [VerifyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(passwordView.mas_bottom).offset(5);
        make.width.mas_equalTo(WIDTH - 20);
        make.leading.equalTo(self.view).offset(10);
        make.height.mas_equalTo(40);
    }];
    self.VerifyView = VerifyView;
    
    UILabel *descLabel = [UILabel addLabelWithFont:13 color:[UIColor blackColor] title:nil];
    [self.view addSubview:descLabel];
    descLabel.numberOfLines = 2;
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:@"*会员帐号及取款密码核对正确后，请联系客服重置密码！"];
    [attStr setAttributes:@{NSForegroundColorAttributeName : [UIColor redColor]} range:NSMakeRange(0, 1)];
    descLabel.attributedText = attStr;
    [descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.view).offset(10);
        make.top.equalTo(VerifyView.mas_bottom).offset(10);
        make.trailing.equalTo(self.VerifyView).offset(-10);
    }];
    
    UIButton *sureBtn = [UIButton addButtonWithTitle:@"确认" font:15 color:[UIColor whiteColor] target:self action:@selector(sureClick)];
     [sureBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];
    sureBtn.layer.cornerRadius = 5;
    sureBtn.layer.masksToBounds = YES;
    [self.view addSubview:sureBtn];
    [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(descLabel.mas_bottom).offset(15);
        make.leading.equalTo(self.view).offset(10);
        make.trailing.equalTo(self.view).offset(-10);
        make.height.mas_equalTo(40);
    }];
    //UI处理完毕刷新验证码
    [SVProgressHUD showWithStatus:@"验证码获取中..."];
    [self queryData];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.passwordView.textfield) {
        return textField.text.length - range.length + string.length < 5;
    }else{
        return YES;
    }
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
//    if (textField == self.VerifyView.textfield) {
//        [self queryData];
//    }//文本焦点验证码取消
}

- (void)queryData
{
    
    [USER_DATA_MANAGER getVCodeImgByRandom:YES Completed:^(UIImage *codeImg) {
        [self.VerifyView.dateBtn setBackgroundImage:codeImg forState:UIControlStateNormal];
        [SVProgressHUD dismissWithCompletion:nil];
    }];
}

- (void)sureClick
{
    if (self.nameView.textfield.text.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"帐号必填哦"];
        return;
    }else if (self.passwordView.textfield.text.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"取款密码必填哦"];
        return;
    }else if (self.VerifyView.textfield.text.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"验证码必填哦"];
        return;
    }else{
        [SVProgressHUD showWithStatus:@"验证中..."];
        [USER_DATA_MANAGER requestFundPassWordByUserAccount:self.nameView.textfield.text
                                               CashPassword:self.passwordView.textfield.text
                                                      vCode:self.VerifyView.textfield.text Completed:^(BOOL success) {
                                                          if (!success) {
                                                              [self queryData];
                                                          }
                                                      }];
    }
}

@end
