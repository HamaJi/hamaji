//
//  Bet365SecurityViewController.h
//  Bet365
//
//  Created by package_mr.chicken on 2019/1/14.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^customComplement)();
typedef void (^backHome)();
@interface Bet365SecurityViewController : Bet365ViewController
@property (nonatomic,strong)NSArray *security_msg;
@property (nonatomic,strong)NSMutableDictionary *personDic;
@property(nonatomic,copy)customComplement customC;
@property(nonatomic,copy)backHome backC;
@end
