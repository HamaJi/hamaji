//
//  Bet365SecurityViewController.m
//  Bet365
//
//  Created by package_mr.chicken on 2019/1/14.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "Bet365SecurityViewController.h"
#import "LukeRegisterLbTFView.h"
#import "Bet365SecurityTableViewCell.h"
#import "NetWorkMannager.h"
#import "NetWorkMannager+Account.h"
#import "LukeUserAdapter.h"
#import "LukeMaskView.h"
//#import "KFCPHomeViewController.h"
@interface Bet365SecurityViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *total_SuperviewHeight;
@property (weak, nonatomic) IBOutlet UITableView *content_tab;
@property (strong,nonatomic) NSMutableDictionary *parameterDic;//参数存储
@property (strong,nonatomic) UIButton *dateButton;//验证码按钮
@end
static NSString *const registerSecurity = @"registerSecurityCell";
@implementation Bet365SecurityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self registerCell];
    [self setUI];
}

-(void)registerCell{
    [self.content_tab registerClass:[Bet365SecurityTableViewCell class] forCellReuseIdentifier:registerSecurity];
    self.content_tab.dataSource = self;
    self.content_tab.delegate = self;
    self.content_tab.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.content_tab.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.content_tab.bounces = NO;
    
}
-(void)setUI{
    NSInteger moreCount = ((NSArray *)self.security_msg[0]).count;
    CGFloat moreDisHeight = (moreCount-3)*50;
    self.total_SuperviewHeight.constant = self.total_SuperviewHeight.constant+moreDisHeight;
}
//懒加载
-(NSMutableDictionary *)parameterDic{
    if (_parameterDic==nil) {
        _parameterDic = [[NSMutableDictionary alloc] init];
    }
    return _parameterDic;
}
//网络任务
/*验证码获取*/
-(void)requesetCodeImage:(void(^)(UIImage *image))images{
    [NET_DATA_MANAGER requestGetVCodeByRandom:YES Success:^(id responseObject) {
        images?images(responseObject):nil;
    } failure:^(NSError *error) {
    }];
}
//重置信息
-(void)resetMyMessage{
    
    [self.personDic setObject:self.parameterDic forKey:@"verifyValues"];
    [SVProgressHUD showWithStatus:@"加载中..."];
    @weakify(self);
    [NET_DATA_MANAGER postInitPwdParamter:self.personDic Success:^(id responseObject) {
        [USER_DATA_MANAGER storageCurrenLoginMessage:responseObject forCompleted:^(BOOL success) {
            @strongify(self);
            [SVProgressHUD dismiss];
            [self dismissViewControllerAnimated:YES completion:^{
                self.backC();
            }];
        }];
    } failure:^(NSError *error) {
        @strongify(self);
        if (error.valicodeError) {
            [self requesetCodeImage:^(UIImage *image) {
                [self.dateButton setBackgroundImage:image forState:UIControlStateNormal];
            }];
        }
        [SVProgressHUD dismiss];
    }];
 
}
//任务处理
/**删除**/
- (IBAction)delete:(id)sender {
    [self resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}
//任务处理——确定
- (IBAction)sure_security:(id)sender {
    
    NSString *password = self.parameterDic[@"password"];
    NSString *surepassword = self.parameterDic[@"surepassword"];
    NSString *verifyCode = self.parameterDic[@"verifyCode"];
    NSString *fullName = self.parameterDic[@"fullName"];
    NSString *phone = self.parameterDic[@"phone"];
    if (![password isPermissionllyPsw]) {
        return;
    }
    if (![password isEqualToString:surepassword]) {
        [SVProgressHUD showErrorWithStatus:@"新密码与确认密码不一致"];
        return;
    }
    if (phone.length && ![LukeUserAdapter validateMobile:phone]) {
        [LukeMaskView lukeMaskViewWithTitle:@"输入的电话号码不规范"];
        return;
    }
    if (BET_CONFIG.config.user_password_type && [BET_CONFIG.config.user_password_type isEqualToString:@"0"]) {
        [self.personDic setObject:[password md5] forKey:@"password"];
    }else{
        [self.personDic setObject:password forKey:@"password"];
    }
    
    [self resetMyMessage];
}
//在线客服
- (IBAction)user_securityService:(id)sender {
    @weakify(self);
    [self dismissViewControllerAnimated:YES completion:^{
        @strongify(self);
        self.customC();
    }];
}
//代理
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return ((NSArray *)self.security_msg[0]).count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Bet365SecurityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:registerSecurity forIndexPath:indexPath];
    NSString *key = self.security_msg[0][indexPath.row];
    @weakify(self);
    [cell SecurityType:([key isEqualToString:@"verifyCode"])?SecurityCode_Type:NormalSecurity_Type ExhibTitle:self.security_msg[1][indexPath.row] subtitle:self.security_msg[1][indexPath.row] keyC:self.security_msg[0][indexPath.row] forSecurityComplement:^(NSString * _Nonnull content, NSString * _Nonnull key) {
        @strongify(self);
        if ([key isEqualToString:@"verifyCode"]) {
            self.dateButton = cell.exbinView.dateBtn;
            [self.personDic safeSetObject:content forKey:@"verifyCode"];
        }
        if (content.length) {
            [self.parameterDic safeSetObject:content forKey:key];
        }else{
            [self.parameterDic safeRemoveObjectForKey:key];
        }
    } reloadCode:^(UIButton * _Nonnull codeB) {
        @strongify(self);
        self.dateButton = codeB;
        [self requesetCodeImage:^(UIImage *image) {
            [codeB setBackgroundImage:image forState:UIControlStateNormal];
        }];
    
    }];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
