//
//  jesseTestPlayViewController.m
//  Bet365
//
//  Created by jesse on 2017/12/20.
//  Copyright © 2017年 jesse. All rights reserved.
//
#import "jesseTestPlayViewController.h"
#import "LukeRegisterLbTFView.h"
#import "NetWorkMannager+Account.h"
#import "UIViewControllerSerializing.h"

@interface jesseTestPlayViewController ()<UITextFieldDelegate,UIViewControllerSerializing>
/** 密码 */
@property (nonatomic,weak) LukeRegisterLbTFView *passwordView;
/** 验证码 */
@property (nonatomic,weak) LukeRegisterLbTFView *VerifyView;
@end

RouterKey *const kRouterRegisterTest = @"registTest";

@implementation jesseTestPlayViewController

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    return !USER_DATA_MANAGER.isLogin;
}

+(void)load{
    [self registerJumpRouterKey:kRouterRegisterTest toHandle:^(NSDictionary *parameters) {
        if (BET_CONFIG.registerConfig.trailUserValidCode) {
            jesseTestPlayViewController *testPlay = [[jesseTestPlayViewController alloc]init];
            testPlay.hidesBottomBarWhenPushed = YES;
            [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:testPlay animated:YES];
        }else{
            [USER_DATA_MANAGER requestTestRegister:^(BOOL success) {
            }];
        }
    }];
    
    [self registerInstanceRouterKey:kRouterRegisterTest toHandle:^UIViewController *(NSDictionary *parameters) {
        jesseTestPlayViewController *testPlay = [[jesseTestPlayViewController alloc]init];
        return testPlay;
    }];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    [self setupUI];
}

- (void)setupUI
{
    LukeRegisterLbTFView *passwordView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"用户密码" textEntry:YES placeholder:@"6-12位英文字母和数字" keyboardType:UIKeyboardTypeDefault];
    passwordView.layer.cornerRadius = 5;
    passwordView.layer.masksToBounds = YES;
    passwordView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
    passwordView.layer.borderWidth = 1;
    passwordView.textfield.delegate = self;
    [self.view addSubview:passwordView];
    [passwordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.width.mas_equalTo(WIDTH - 20);
        make.leading.equalTo(self.view).offset(10);
        make.height.mas_equalTo(45);
    }];
    self.passwordView = passwordView;
    
    LukeRegisterLbTFView *VerifyView = [LukeRegisterLbTFView registerLbBtnTextFieldViewWithTitle:@"验证码" VerificationBtnImage:@"vaid" target:self action:@selector(VerifyClick)];
    VerifyView.layer.cornerRadius = 5;
    VerifyView.layer.masksToBounds = YES;
    VerifyView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
    VerifyView.layer.borderWidth = 1;
    [self.view addSubview:VerifyView];
    VerifyView.textfield.delegate = self;
    [VerifyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(passwordView.mas_bottom).offset(5);
        make.width.mas_equalTo(WIDTH - 20);
        make.leading.equalTo(self.view).offset(10);
        make.height.mas_equalTo(45);
    }];
    self.VerifyView = VerifyView;
    
    UIButton *sureBtn = [UIButton addButtonWithTitle:@"试玩登入" font:15 color:[UIColor whiteColor] target:self action:@selector(registerTestPlay:)];
    [sureBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];
    sureBtn.layer.cornerRadius = 3;
    sureBtn.layer.masksToBounds = YES;
    [self.view addSubview:sureBtn];
    [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(VerifyView.mas_bottom).offset(25);
        make.leading.equalTo(self.view).offset(10);
        make.trailing.equalTo(self.view).offset(-10);
        make.height.mas_equalTo(45);
    }];
    
    UIButton *backBtn = [UIButton addButtonWithTitle:@"返回首页" font:15 color:[UIColor whiteColor] target:self action:@selector(backClick)];
    [backBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];
    backBtn.layer.cornerRadius = 3;
    backBtn.layer.masksToBounds = YES;
    [self.view addSubview:backBtn];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(sureBtn.mas_bottom).offset(25);
        make.leading.equalTo(self.view).offset(10);
        make.trailing.equalTo(self.view).offset(-10);
        make.height.mas_equalTo(45);
    }];
}

- (void)backClick
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.VerifyView.textfield) {
        [self queryVerifyData];
    }
}

- (void)queryVerifyData
{
    [SVProgressHUD showWithStatus:@"验证码获取中..."];
    [NET_DATA_MANAGER requestGetVCodeByRandom:YES Success:^(id responseObject) {
        [self.VerifyView.dateBtn setBackgroundImage:responseObject forState:UIControlStateNormal];
        [SVProgressHUD dismiss];
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

- (void)VerifyClick
{
    [self queryVerifyData];
}

-(void)registerTestPlay:(UIButton *)testPlay{
    
    if (self.passwordView.textfield.text.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"密码必填哦"];
        return;
    }
    if (self.passwordView.textfield.text.length < 6 || self.passwordView.textfield.text.length > 12) {
        [SVProgressHUD showErrorWithStatus:@"密码必须6~12位字符哦"];
        return;
    }
    
    if (self.VerifyView.textfield.text.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"验证码必填哦"];
        return;
    }
    NSString *password = self.passwordView.textfield.text;
    if (BET_CONFIG.config.user_password_type && [BET_CONFIG.config.user_password_type isEqualToString:@"0"]) {
        password = [self.passwordView.textfield.text MD5];
    }
    [USER_DATA_MANAGER requestTestRegisterByPassword:password ValiCode:self.VerifyView.textfield.text Completed:^(BOOL success) {
        if (!success) {
            [self queryVerifyData];
        }
    }];
}

@end
