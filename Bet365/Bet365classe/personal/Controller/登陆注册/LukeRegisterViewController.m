//
//  LukeRegisterViewController.m
//  Bet365
//
//  Created by luke on 17/6/11.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeRegisterViewController.h"
#import "LukeRegisterLbTFView.h"
#import "jesseLogBet365ViewController.h"
#import "LukeUserAdapter.h"
#import "LukeMaskView.h"
#import "RedEnvelopeController.h"
#import "Bet365Tool.h"
#import "NetWorkMannager+Account.h"
#import "jesseTestPlayViewController.h"
#import "VerifyManager.h"
@interface LukeRegisterViewController ()
<UITextFieldDelegate,VerifyManagerDelegate>
/** 活动scrollview */
@property (nonatomic,weak) UIScrollView *scrollView;
/** 帐号 */
@property (nonatomic,weak) LukeRegisterLbTFView *nameView;
/** 密码 */
@property (nonatomic,weak) LukeRegisterLbTFView *passwordView;
/** 确认密码 */
@property (nonatomic,weak) LukeRegisterLbTFView *passwordSureView;
/** 推广码 */
@property (nonatomic,weak) LukeRegisterLbTFView *intrCodeView;
/** 验证码 */
@property (nonatomic,weak) LukeRegisterLbTFView *VerifyView;
/** 真实姓名 */
@property (nonatomic,weak) LukeRegisterLbTFView *realNameView;
/** 取款密码 */
@property (nonatomic,weak) LukeRegisterLbTFView *moneyPasswordView;
/** 邮箱 */
@property (nonatomic,weak) LukeRegisterLbTFView *emailView;
/** 电话 */
@property (nonatomic,weak) LukeRegisterLbTFView *phoneView;
/** qq */
@property (nonatomic,weak) LukeRegisterLbTFView *QQView;
/** 微信 */
@property (nonatomic,weak) LukeRegisterLbTFView *wechatView;
/** 出生日期 */
@property (nonatomic,weak) LukeRegisterLbTFView *birthdayView;
/** 手机验证码 */
@property (nonatomic,weak) LukeRegisterLbTFView *smsCodeView;
/** 确认按钮 */
@property (nonatomic,weak) UIButton *sureBtn;
/** 在线客服按钮 */
@property (nonatomic,weak) UIButton *serviceBtn;

@property (nonatomic,strong)UILabel *vipMeansToastLabel;

@property (nonatomic,strong) VerifyManager *verifyManager;

@property (nonatomic,strong)RegisterInfoData *info;

@property (nonatomic,assign) VerifyAuthorType verifyType;

@end

RouterKey *const kRouterRegister = @"register";

@implementation LukeRegisterViewController

+(void)load{
    [self registerJumpRouterKey:kRouterRegister toHandle:^(NSDictionary *parameters) {
        LukeRegisterViewController *registerC = [[LukeRegisterViewController alloc]init];
        registerC.view.backgroundColor = [UIColor whiteColor];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:registerC animated:YES];
        registerC.hidesBottomBarWhenPushed = YES;
    }];
    
    [self registerInstanceRouterKey:kRouterRegister toHandle:^UIViewController *(NSDictionary *parameters) {
        LukeRegisterViewController *registerC = [[LukeRegisterViewController alloc]init];
        registerC.view.backgroundColor = [UIColor whiteColor];
        return registerC;
    }];
    
}

#pragma mark - dealloc
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    [self getCodeData];
    [self setupUI];
    [self addRAC];
}

- (void)getCodeData
{
//    if (self.isCode) {
//        self.VerifyView.textfield.text = @"";
//    }
//    [SVProgressHUD showWithStatus:@"配置获取中..."];
//    [NET_DATA_MANAGER requestGetCheckoutErrorCountWithType:InfoCheckoutErrorCountType_Register Success:^(id responseObject) {
//        self.isCode = [responseObject boolValue];
//        [SVProgressHUD dismiss];
//    } failure:^(NSError *error) {
//        [SVProgressHUD dismiss];
//        NSLog(@"%@",error);
//    }];
    
    [self.sureBtn setEnabled:NO];
    [NET_DATA_MANAGER requestRigisterVerifyTypeSuccess:^(id responseObject) {
        NSMutableString *verifyTypeStr = [[NSMutableString alloc] initWithString:responseObject];
        self.verifyManager.requestJsonString = verifyTypeStr;
        [self.sureBtn setEnabled:YES];
    } failure:^(NSError *error) {
        [self.sureBtn setEnabled:YES];
    }];
}


- (void)addRAC
{
    @weakify(self);
//    [RACObserve(self, isCode) subscribeNext:^(id  _Nullable x) {
//        @strongify(self);
//        if ([x boolValue]) {
//            [self queryVaildData];//获取验证码
//            [self.VerifyView mas_updateConstraints:^(MASConstraintMaker *make) {
//                make.height.mas_equalTo(40);
//            }];
//        }else{
//            [self.VerifyView mas_updateConstraints:^(MASConstraintMaker *make) {
//                make.height.mas_equalTo(0);
//            }];
//        }
//    }];
    
    [RACObserve(self, verifyType) subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([x integerValue] == VerifyAuthorType_TENCENT) {
                [self.VerifyView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(0);
                }];
            }else if ([x integerValue] == VerifyAuthorType_IMAGE){
                [self.VerifyView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(45);
                }];
                /* 直接刷新验证码 */
                [self queryVaildData];
                
            }else if ([x integerValue] == VerifyAuthorType_GT3){
                [self.VerifyView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(0);
                }];
            }else{
                [self.VerifyView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(0);
                }];
            }
            
        });
        
    }];
    
    [RACObserve(BET_CONFIG, registerConfig) subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        [self.intrCodeView mas_updateConstraints:^(MASConstraintMaker *make) {
            if (BET_CONFIG.registerConfig.intrMobileCode) {
                if ([BET_CONFIG.registerConfig.intrMobileCode intValue]!=2) {
                    make.height.mas_equalTo(40);
                }else{
                    make.height.mas_equalTo(0);
                }
            }else{
                if(BET_CONFIG.registerConfig.intrCode!=2){
                    make.height.mas_equalTo(40);
                }else{
                    make.height.mas_equalTo(0);
                }
            }
        }];
        
        [self.realNameView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(BET_CONFIG.registerConfig.fullName != 2 ? 40 : 0);
        }];
        [self.moneyPasswordView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo((!BET_CONFIG.registerConfig.fundPwd || BET_CONFIG.registerConfig.fundPwd != 2) ? 40 : 0);
        }];
        [self.birthdayView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(BET_CONFIG.registerConfig.birthday != 2 ? 40 : 0);
        }];
        
        [self.emailView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(BET_CONFIG.registerConfig.email != 2 ? 40 : 0);
        }];
        
        [self.phoneView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(BET_CONFIG.registerConfig.phone != 2 ? 40 : 0);
        }];
        
        [self.smsCodeView mas_updateConstraints:^(MASConstraintMaker *make) {
            if ((BET_CONFIG.registerConfig.phone != 2) && BET_CONFIG.registerConfig.registerPhone == 1) {
                make.height.mas_equalTo(40);
            }else{
                make.height.mas_equalTo(0);
            }
        }];
        
        [self.QQView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(BET_CONFIG.registerConfig.qq != 2 ? 40 : 0);
        }];
        
        [self.wechatView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(BET_CONFIG.registerConfig.weixin != 2 ? 40 : 0);
        }];
    }];
}

- (void)setupUI{
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.backgroundColor = [UIColor skinViewScreenColor];
    [self.view addSubview:scrollView];
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.leading.trailing.bottom.equalTo(self.view);
        make.width.mas_equalTo(WIDTH);
    }];
    self.scrollView = scrollView;
    
    UILabel *nameLb = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:@"会员账号"];
    [self.scrollView addSubview:nameLb];
    [nameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.scrollView).offset(10);
        make.top.equalTo(self.scrollView).offset(15);
    }];
    

    LukeRegisterLbTFView *nameView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"用户帐号"
                                                                           textEntry:NO
                                                                         placeholder:(BET_CONFIG.registerConfig.userAccountMessage && [NSString noNilWithString:BET_CONFIG.registerConfig.userAccountMessage].length) ? BET_CONFIG.registerConfig.userAccountMessage : @"4-10个英文字母以及数字"
                                                                        keyboardType:UIKeyboardTypeDefault];
    nameView.layer.cornerRadius = 5;
    nameView.layer.masksToBounds = YES;
    nameView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
    nameView.layer.borderWidth = 1;
    nameView.textfield.delegate = self;
    [scrollView addSubview:nameView];
    [nameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(nameLb.mas_bottom).offset(5);
        make.height.mas_equalTo(40);
        make.width.mas_equalTo(WIDTH - 20);
        make.leading.equalTo(scrollView).offset(10);
    }];
    self.nameView = nameView;
    
    NSUInteger min = BET_CONFIG.registerConfig.isComplexPassword ? 8 : 6;
    NSUInteger max = BET_CONFIG.registerConfig.isComplexPassword ? 30 : 0;
    
    NSString *placeholder = [NSString stringWithFormat:@"%li~%li位英文字母和数字",min,max];
    if (max == 0) {
        placeholder = [NSString stringWithFormat:@"至少%li位英文字母和数字",min];
    }
    LukeRegisterLbTFView *passwordView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"登录密码" textEntry:YES placeholder:placeholder keyboardType:UIKeyboardTypeDefault];
    passwordView.layer.cornerRadius = 5;
    passwordView.layer.masksToBounds = YES;
    passwordView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
    passwordView.layer.borderWidth = 1;
    passwordView.textfield.delegate = self;
    [scrollView addSubview:passwordView];
    [passwordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(nameView.mas_bottom).offset(2);
        make.width.mas_equalTo(WIDTH - 20);
        make.leading.equalTo(scrollView).offset(10);
        make.height.mas_equalTo(40);
    }];
    self.passwordView = passwordView;
    
    /**增加提示语**/
    UILabel *security_Alert = [[UILabel alloc] init];
    [security_Alert setTextColor:[UIColor redColor]];
    [security_Alert setFont:[UIFont systemFontOfSize:15]];
    [security_Alert setText:@"请输入8～30位英文和数字,且不能包含连续数字"];
    security_Alert.adjustsFontSizeToFitWidth = YES;
    [scrollView addSubview:security_Alert];
    [security_Alert mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(passwordView.mas_bottom);
        make.leading.trailing.equalTo(passwordView);
        make.height.mas_equalTo(max == 0 ? 0 : 30);
    }];
    
    LukeRegisterLbTFView *passwordSureView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"确认密码" textEntry:YES placeholder:@"请再输入一次密码" keyboardType:UIKeyboardTypeDefault];
    passwordSureView.layer.cornerRadius = 5;
    passwordSureView.layer.masksToBounds = YES;
    passwordSureView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
    passwordSureView.layer.borderWidth = 1;
    passwordSureView.textfield.delegate = self;
    [scrollView addSubview:passwordSureView];
    [passwordSureView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(security_Alert.mas_bottom);
        make.width.mas_equalTo(WIDTH - 20);
        make.leading.equalTo(scrollView).offset(10);
        make.height.mas_equalTo(40);
    }];
    self.passwordSureView = passwordSureView;
//    @weakify(self);
//    void(^intrCodeCreat)() = ^(){
//        @strongify(self);
        LukeRegisterLbTFView *intrCodeView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"推广码" textEntry:NO placeholder:([BET_CONFIG.registerConfig.intrMobileCode intValue]==1)?@"(必填)请输入推广码":@"(选填)请输入推广码" keyboardType:UIKeyboardTypeDefault];
        intrCodeView.textfield.text = [AUTHDATA_MANAGER.intr isNoBlankString] ? AUTHDATA_MANAGER.intr : nil;
        intrCodeView.textfield.enabled = ![AUTHDATA_MANAGER.intr isNoBlankString];
        if (BET_CONFIG.PromotionCode&&[BET_CONFIG.PromotionCodeState boolValue]) {
            intrCodeView.textfield.enabled =([BET_CONFIG.PromotionCodeState boolValue])?NO:YES;
            intrCodeView.textfield.text = ([BET_CONFIG.PromotionCodeState boolValue])?BET_CONFIG.PromotionCode:@"";
        }
        intrCodeView.textfield.delegate = self;
        intrCodeView.layer.cornerRadius = 5;
        intrCodeView.layer.masksToBounds = YES;
        intrCodeView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
        intrCodeView.layer.borderWidth = 1;
        [self.scrollView addSubview:intrCodeView];
        [intrCodeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(passwordSureView.mas_bottom).offset(2);
            make.leading.trailing.equalTo(self.scrollView).offset(10);
            make.width.mas_equalTo(WIDTH - 20);
            make.height.mas_equalTo(0);
        }];
        self.intrCodeView = intrCodeView;
//    };
//    if (BET_CONFIG.registerConfig.intrMobileCode) {
//        if ([BET_CONFIG.registerConfig.intrMobileCode intValue]!=2) {
//            intrCodeCreat();
//        }
//    }else{
//        if(BET_CONFIG.registerConfig.intrCode!=2)intrCodeCreat();
//    }
    
    LukeRegisterLbTFView *VerifyView = [LukeRegisterLbTFView registerLbBtnTextFieldViewWithTitle:@"验证码" VerificationBtnImage:@"vaid" target:self action:@selector(VerifyClick)];
    VerifyView.layer.cornerRadius = 5;
    VerifyView.layer.masksToBounds = YES;
    VerifyView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
    VerifyView.layer.borderWidth = 1;
    [scrollView addSubview:VerifyView];
    VerifyView.textfield.delegate = self;
    [VerifyView mas_makeConstraints:^(MASConstraintMaker *make) {
//        if (BET_CONFIG.registerConfig.intrMobileCode) {
//            if([BET_CONFIG.registerConfig.intrMobileCode integerValue]!=2) {
//                make.top.equalTo(self.intrCodeView.mas_bottom).offset(5);
//            }else{
//                make.top.equalTo(self.passwordSureView.mas_bottom).offset(5);
//            }
//        }else{
//            if (BET_CONFIG.registerConfig.intrCode != 2) {
//                make.top.equalTo(self.intrCodeView.mas_bottom).offset(5);
//            }else{
//                make.top.equalTo(self.passwordSureView.mas_bottom).offset(5);
//            }
//        }
        make.top.equalTo(self.intrCodeView.mas_bottom).offset(2);
        make.width.mas_equalTo(WIDTH - 20);
        make.leading.equalTo(scrollView).offset(10);
        make.height.mas_equalTo(0);
    }];
    self.VerifyView = VerifyView;
    
    UILabel *countLabel = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:@"会员资料"];
    [self.scrollView addSubview:countLabel];
    [countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.VerifyView.mas_bottom).offset(15);
        make.leading.equalTo(self.scrollView).offset(10);
    }];
    self.vipMeansToastLabel = countLabel;
    
//    if (BET_CONFIG.registerConfig.fullName != 2) {
        LukeRegisterLbTFView *realNameView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"真实姓名" textEntry:NO placeholder:(BET_CONFIG.registerConfig.userNameRegisterMessage && BET_CONFIG.registerConfig.userNameRegisterMessage.length > 0) ? BET_CONFIG.registerConfig.userNameRegisterMessage : @"与你的真实姓名相同" keyboardType:UIKeyboardTypeDefault];
        realNameView.layer.cornerRadius = 5;
        realNameView.layer.masksToBounds = YES;
        realNameView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
        realNameView.layer.borderWidth = 1;
        realNameView.textfield.delegate = self;
        [self.scrollView addSubview:realNameView];
        [realNameView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(countLabel.mas_bottom).offset(2);
            make.width.mas_equalTo(WIDTH - 20);
            make.leading.equalTo(scrollView).offset(10);
            make.height.mas_equalTo(0);
        }];
        self.realNameView = realNameView;
//    }
    
//    if (!BET_CONFIG.registerConfig.fundPwd || BET_CONFIG.registerConfig.fundPwd != 2) {
        LukeRegisterLbTFView *moneyPasswordView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"取款密码" textEntry:NO placeholder:@"4位纯数字" keyboardType:UIKeyboardTypePhonePad];
        moneyPasswordView.layer.cornerRadius = 5;
        moneyPasswordView.layer.masksToBounds = YES;
        moneyPasswordView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
        moneyPasswordView.layer.borderWidth = 1;
        moneyPasswordView.textfield.delegate = self;
        [scrollView addSubview:moneyPasswordView];
        [moneyPasswordView mas_makeConstraints:^(MASConstraintMaker *make) {
//            if (BET_CONFIG.registerConfig.fullName != 2) {
//                make.top.equalTo(self.realNameView.mas_bottom).offset(5);
//            }else{
//                make.top.equalTo(countLabel.mas_bottom).offset(5);
//            }
            make.top.equalTo(self.realNameView.mas_bottom).offset(2);
            make.width.mas_equalTo(WIDTH - 20);
            make.leading.equalTo(scrollView).offset(10);
            make.height.mas_equalTo(0);
        }];
        self.moneyPasswordView = moneyPasswordView;
//    }
    
//    if (BET_CONFIG.registerConfig.birthday != 2) {
        LukeRegisterLbTFView *birthdayView = [LukeRegisterLbTFView registerLbBtnViewWithTitle:@"出生日期" btnImage:@"right_lage" btnTitle:nil];
        birthdayView.layer.cornerRadius = 5;
        birthdayView.layer.masksToBounds = YES;
        birthdayView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
        birthdayView.layer.borderWidth = 1;
        [scrollView addSubview:birthdayView];
        @weakify(self);
        birthdayView.callBlock = ^{
            @strongify(self);
            [self showDatePickerdefaultSelValue:nil minDate:nil resultBlock:^(NSString *selectValue) {
                [self.birthdayView.dateBtn setTitle:selectValue forState:UIControlStateNormal];
                
            }];
        };
        [birthdayView mas_makeConstraints:^(MASConstraintMaker *make) {
//            if (!BET_CONFIG.registerConfig.fundPwd || BET_CONFIG.registerConfig.fundPwd != 2){
//                make.top.equalTo(self.moneyPasswordView.mas_bottom).offset(5);
//            }else if (BET_CONFIG.registerConfig.fullName != 2){
//                make.top.equalTo(self.realNameView.mas_bottom).offset(5);
//            }else{
//                make.top.equalTo(countLabel.mas_bottom).offset(5);
//            }
            make.top.equalTo(self.moneyPasswordView.mas_bottom).offset(2);
            make.width.mas_equalTo(WIDTH - 20);
            make.leading.equalTo(scrollView).offset(10);
            make.height.mas_equalTo(0);
        }];
        self.birthdayView = birthdayView;
//    }
    
//    if (BET_CONFIG.registerConfig.email != 2) {
        LukeRegisterLbTFView *emailView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"邮箱" textEntry:NO placeholder:[BET_CONFIG.registerConfig.mailPlaceHolder isNoBlankString] ? BET_CONFIG.registerConfig.mailPlaceHolder : nil keyboardType:UIKeyboardTypeEmailAddress];
        emailView.layer.cornerRadius = 5;
        emailView.layer.masksToBounds = YES;
        emailView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
        emailView.layer.borderWidth = 1;
        emailView.textfield.delegate = self;
        [scrollView addSubview:emailView];
        emailView.textfield.delegate = self;
        [emailView mas_makeConstraints:^(MASConstraintMaker *make) {
//            if (BET_CONFIG.registerConfig.birthday != 2) {
//                make.top.equalTo(self.birthdayView.mas_bottom).offset(5);
//            }else if (!BET_CONFIG.registerConfig.fundPwd || BET_CONFIG.registerConfig.fundPwd != 2){
//                make.top.equalTo(self.moneyPasswordView.mas_bottom).offset(5);
//            }else if (BET_CONFIG.registerConfig.fullName != 2){
//                make.top.equalTo(self.realNameView.mas_bottom).offset(5);
//            }else{
//                make.top.equalTo(countLabel.mas_bottom).offset(5);
//            }
            make.top.equalTo(self.birthdayView.mas_bottom).offset(2);
            make.width.mas_equalTo(WIDTH - 20);
            make.leading.equalTo(scrollView).offset(10);
            make.height.mas_equalTo(0);
        }];
        self.emailView = emailView;
//    }
    
//    if (BET_CONFIG.registerConfig.phone != 2) {
        LukeRegisterLbTFView *phoneView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"手机号码" textEntry:NO placeholder:[BET_CONFIG.registerConfig.phonePlaceholder isNoBlankString] ? BET_CONFIG.registerConfig.phonePlaceholder : nil keyboardType:UIKeyboardTypeNumberPad];
        phoneView.layer.cornerRadius = 5;
        phoneView.layer.masksToBounds = YES;
        phoneView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
        phoneView.layer.borderWidth = 1;
        phoneView.textfield.delegate = self;
        [scrollView addSubview:phoneView];
        [phoneView mas_makeConstraints:^(MASConstraintMaker *make) {
//            if (BET_CONFIG.registerConfig.email != 2) {
//                make.top.equalTo(self.emailView.mas_bottom).offset(5);
//            }else if (BET_CONFIG.registerConfig.birthday != 2) {
//                make.top.equalTo(self.birthdayView.mas_bottom).offset(5);
//            }else if (!BET_CONFIG.registerConfig.fundPwd || BET_CONFIG.registerConfig.fundPwd != 2){
//                make.top.equalTo(self.moneyPasswordView.mas_bottom).offset(5);
//            }else if (BET_CONFIG.registerConfig.fullName != 2){
//                make.top.equalTo(self.realNameView.mas_bottom).offset(5);
//            }else{
//                make.top.equalTo(countLabel.mas_bottom).offset(5);
//            }
            make.top.equalTo(self.emailView.mas_bottom).offset(2);
            make.width.mas_equalTo(WIDTH - 20);
            make.leading.equalTo(scrollView).offset(10);
            make.height.mas_equalTo(0);
        }];
        self.phoneView = phoneView;
//    }
    LukeRegisterLbTFView *smsCodeView = [LukeRegisterLbTFView registerCountdownBtnWithTitle:@"短信验证" ResendTitile:@"重新发送验证码" TimeOutKey:@"" MaxCount:60 TapUpBlock:^BOOL{
        @strongify(self);
        [self requestSMScode];
        return NO;
    } vCodeSendingBlock:^(NSUInteger sendingSpacing) {
        
    } Placeholder:@"请输入验证码"];
    smsCodeView.layer.cornerRadius = 5;
    smsCodeView.layer.masksToBounds = YES;
    smsCodeView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
    smsCodeView.layer.borderWidth = 1;
    [self.scrollView addSubview:smsCodeView];
    [smsCodeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.phoneView.mas_bottom).offset(2);
        make.width.mas_equalTo(WIDTH - 20);
        make.leading.equalTo(scrollView).offset(10);
        make.height.mas_equalTo(0);
    }];
    self.smsCodeView = smsCodeView;
    
//    if (BET_CONFIG.registerConfig.qq != 2) {
        LukeRegisterLbTFView *QQView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"QQ帐号" textEntry:NO placeholder:[BET_CONFIG.registerConfig.qqPlaceholder isNoBlankString] ? BET_CONFIG.registerConfig.qqPlaceholder : nil keyboardType:UIKeyboardTypeNumberPad];
        QQView.layer.cornerRadius = 5;
        QQView.layer.masksToBounds = YES;
        QQView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
        QQView.layer.borderWidth = 1;
        QQView.textfield.delegate = self;
        [scrollView addSubview:QQView];
        [QQView mas_makeConstraints:^(MASConstraintMaker *make) {
//            if (BET_CONFIG.registerConfig.phone != 2) {
//                make.top.equalTo(self.phoneView.mas_bottom).offset(5);
//            }else if (BET_CONFIG.registerConfig.email != 2) {
//                make.top.equalTo(self.emailView.mas_bottom).offset(5);
//            }else if (BET_CONFIG.registerConfig.birthday != 2) {
//                make.top.equalTo(self.birthdayView.mas_bottom).offset(5);
//            }else if (!BET_CONFIG.registerConfig.fundPwd || BET_CONFIG.registerConfig.fundPwd != 2){
//                make.top.equalTo(self.moneyPasswordView.mas_bottom).offset(5);
//            }else if (BET_CONFIG.registerConfig.fullName != 2){
//                make.top.equalTo(self.realNameView.mas_bottom).offset(5);
//            }else{
//                make.top.equalTo(countLabel.mas_bottom).offset(5);
//            }
            make.top.equalTo(self.smsCodeView.mas_bottom).offset(2);
            make.width.mas_equalTo(WIDTH - 20);
            make.leading.equalTo(scrollView).offset(10);
            make.height.mas_equalTo(0);
        }];
        self.QQView = QQView;
//    }
    
//    if (BET_CONFIG.registerConfig.weixin != 2) {
        LukeRegisterLbTFView *wechatView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"微信帐号" textEntry:NO placeholder:[BET_CONFIG.registerConfig.wechatPlaceholder isNoBlankString] ? BET_CONFIG.registerConfig.wechatPlaceholder : nil keyboardType:UIKeyboardTypeDefault];
        wechatView.layer.cornerRadius = 5;
        wechatView.layer.masksToBounds = YES;
        wechatView.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
        wechatView.layer.borderWidth = 1;
        wechatView.textfield.delegate = self;
        [scrollView addSubview:wechatView];
        [wechatView mas_makeConstraints:^(MASConstraintMaker *make) {
//            if (BET_CONFIG.registerConfig.qq != 2) {
//                make.top.equalTo(self.QQView.mas_bottom).offset(5);
//            }else if (BET_CONFIG.registerConfig.phone != 2) {
//                make.top.equalTo(self.phoneView.mas_bottom).offset(5);
//            }else if (BET_CONFIG.registerConfig.email != 2) {
//                make.top.equalTo(self.emailView.mas_bottom).offset(5);
//            }else if (BET_CONFIG.registerConfig.birthday != 2) {
//                make.top.equalTo(self.birthdayView.mas_bottom).offset(5);
//            }else if (!BET_CONFIG.registerConfig.fundPwd || BET_CONFIG.registerConfig.fundPwd != 2){
//                make.top.equalTo(self.moneyPasswordView.mas_bottom).offset(5);
//            }else if (BET_CONFIG.registerConfig.fullName != 2){
//                make.top.equalTo(self.realNameView.mas_bottom).offset(5);
//            }else{
//                make.top.equalTo(countLabel.mas_bottom).offset(5);
//            }
            make.top.equalTo(self.QQView.mas_bottom).offset(2);
            make.width.mas_equalTo(WIDTH - 20);
            make.leading.equalTo(scrollView).offset(10);
            make.height.mas_equalTo(0);
        }];
        self.wechatView = wechatView;
//    }
    
    UIButton *agreeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [agreeBtn setTitle:@"我已年满18周岁，并且同意接受" forState:UIControlStateNormal];
    [agreeBtn setTitleColor:[UIColor skinTextItemNorColor] forState:UIControlStateNormal];
    agreeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [agreeBtn setImage:[UIImage imageNamed:@"register_agree"] forState:UIControlStateSelected];
    [agreeBtn setImage:[UIImage imageNamed:@"register_disagree"] forState:UIControlStateNormal];
    [agreeBtn addTarget:self action:@selector(agreeClick:) forControlEvents:UIControlEventTouchUpInside];
    agreeBtn.selected = YES;
    [scrollView addSubview:agreeBtn];
    [agreeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        if (BET_CONFIG.registerConfig.weixin != 2) {
//            make.top.equalTo(self.wechatView.mas_bottom).offset(10);
//        }else if (BET_CONFIG.registerConfig.qq != 2) {
//            make.top.equalTo(self.QQView.mas_bottom).offset(10);
//        }else if (BET_CONFIG.registerConfig.phone != 2) {
//            make.top.equalTo(self.phoneView.mas_bottom).offset(10);
//        }else if (BET_CONFIG.registerConfig.email != 2) {
//            make.top.equalTo(self.emailView.mas_bottom).offset(10);
//        }else if (BET_CONFIG.registerConfig.birthday != 2) {
//            make.top.equalTo(self.birthdayView.mas_bottom).offset(10);
//        }else if (!BET_CONFIG.registerConfig.fundPwd || BET_CONFIG.registerConfig.fundPwd != 2){
//            make.top.equalTo(self.moneyPasswordView.mas_bottom).offset(10);
//        }else if (BET_CONFIG.registerConfig.fullName != 2){
//            make.top.equalTo(self.realNameView.mas_bottom).offset(10);
//        }else{
//            make.top.equalTo(countLabel.mas_bottom).offset(10);
//        }
        make.top.equalTo(self.wechatView.mas_bottom).offset(10);
        make.leading.equalTo(scrollView).offset(15);
    }];
    
    UIButton *lawBtn = [UIButton addButtonWithTitle:@"《开户协议》" font:15 color:[UIColor skinViewKitSelColor] target:self action:@selector(lawClick)];
    [scrollView addSubview:lawBtn];
    [lawBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(agreeBtn);
        make.leading.equalTo(agreeBtn.mas_trailing);
    }];
    
    UIButton *sureBtn = [UIButton addButtonWithTitle:@"立即注册" font:15 color:[UIColor skinViewKitNorColor] target:self action:@selector(sureClick)];
    [scrollView addSubview:sureBtn];
    [sureBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];
    
    sureBtn.layer.cornerRadius = 3;
    sureBtn.layer.masksToBounds = YES;
    [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(agreeBtn.mas_bottom).offset(20);
        make.centerX.equalTo(self.scrollView);
        make.size.mas_equalTo(CGSizeMake(WIDTH - 20, 40));
    }];
    self.sureBtn = sureBtn;
    
    UIButton *registerBtn = [UIButton addButtonWithTitle:@"已有账号,直接登录" font:15 color:[UIColor skinViewKitSelColor] target:self action:@selector(pushLogin)];
    registerBtn.backgroundColor = [UIColor skinViewKitNorColor];
    registerBtn.layer.cornerRadius = 3;
    registerBtn.layer.masksToBounds = YES;
    registerBtn.layer.borderWidth = 1;
    registerBtn.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
    [self.scrollView addSubview:registerBtn];
    [registerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(sureBtn.mas_bottom).offset(10);
        make.centerX.equalTo(self.scrollView);
        make.size.mas_equalTo(CGSizeMake(WIDTH - 20, 40));
    }];
    
    UIButton *serviceBtn = [UIButton addButtonWithTitle:@"在线客服" font:15 color:[UIColor skinViewKitSelColor] target:self action:@selector(customService)];
    serviceBtn.backgroundColor = [UIColor skinViewKitNorColor];
    serviceBtn.layer.cornerRadius = 3;
    serviceBtn.layer.masksToBounds = YES;
    serviceBtn.layer.borderWidth = 1;
    serviceBtn.layer.borderColor = [UIColor skinViewKitSelColor].CGColor;
    [self.scrollView addSubview:serviceBtn];
    [serviceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(registerBtn.mas_bottom).offset(10);
        make.centerX.equalTo(self.scrollView);
        make.size.mas_equalTo(CGSizeMake(WIDTH - 20, 40));
    }];
    
    UIButton *testBtn = [UIButton addButtonWithTitle:@"免费试玩" font:15 color:[UIColor skinViewKitSelColor] target:self action:@selector(testPlay)];
    testBtn.backgroundColor = [UIColor skinViewKitNorColor];
    testBtn.layer.cornerRadius = 3;
    testBtn.layer.masksToBounds = YES;
    testBtn.layer.borderWidth = 1;
    testBtn.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
    [self.scrollView addSubview:testBtn];
    [testBtn mas_makeConstraints:^(MASConstraintMaker *make){
        make.top.equalTo(serviceBtn.mas_bottom).offset(10);
        make.centerX.equalTo(self.scrollView);
        make.size.mas_equalTo(CGSizeMake(WIDTH - 20, 40));
    }];
    
    UIButton *backBtn = [UIButton addButtonWithTitle:@"返回首页" font:15 color:[UIColor skinViewKitSelColor] target:self action:@selector(backClick)];
    backBtn.backgroundColor = [UIColor skinViewKitNorColor];
    backBtn.layer.cornerRadius = 3;
    backBtn.layer.masksToBounds = YES;
    backBtn.layer.borderWidth = 1;
    backBtn.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
    [self.scrollView addSubview:backBtn];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(testBtn.mas_bottom).offset(10);
        make.centerX.equalTo(self.scrollView);
        make.size.mas_equalTo(CGSizeMake(WIDTH - 20, 40));
        make.bottom.equalTo(self.scrollView).offset(-20);
    }];

    [self.scrollView layoutIfNeeded];
}

-(void)requestSMScode{
    if (!self.phoneView.textfield.text.length) {
        [SVProgressHUD showErrorWithStatus:@"请填写手机号码"];
        return;
    }
    [SVProgressHUD showWithStatus:@"发送中..."];
    [NET_DATA_MANAGER requestPostRegisterSMSCodeByPhone:self.phoneView.textfield.text Paramers:nil Success:^(id responseObject) {
        [SVProgressHUD dismiss];
        [SVProgressHUD showSuccessWithStatus:@"验证码已发送至你手机"];
        [self.smsCodeView startTimingVcode];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [SVProgressHUD dismiss];
    }];
}

- (void)backClick
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/**客服跳转**/
- (void)customService{
    [UIViewController routerJumpToUrl:BET_CONFIG.common_config.zxkfPath];
}

- (void)lawClick{
//    RedEnvelopeController *vc = [[RedEnvelopeController alloc] init];
//    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
//    [self presentViewController:nav animated:YES completion:nil];
    [UIViewController routerJumpToUrl:StringFormatWithStr(SerVer_Url, @"/page/include/register_xieyi.html")];
}

- (void)agreeClick:(UIButton *)sender
{
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.sureBtn.enabled = YES;
        [UIView animateWithDuration:.25 animations:^{
             [self.sureBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];
        }];
    }else{
        self.sureBtn.enabled = NO;
        [UIView animateWithDuration:.25 animations:^{
            [self.sureBtn setBackgroundImage:[UIImage createImageWithColor:RegisterButtonDisElbleBackColor] forState:UIControlStateNormal];
        }];
    }
}

- (void)VerifyClick
{
    [self.VerifyView.textfield setText:@""];
    [self queryVaildData];
}
/**已有账户直接登陆**/
- (void)pushLogin{
    jesseLogBet365ViewController *login = [[jesseLogBet365ViewController alloc] init];
    [self.navigationController pushViewController:login animated:YES];
    login.hidesBottomBarWhenPushed = YES;
}
/**试玩**/
- (void)testPlay{
    if (BET_CONFIG.registerConfig.trailUserValidCode) {
        jesseTestPlayViewController *testPlay = [[jesseTestPlayViewController alloc]init];
        [self.navigationController pushViewController:testPlay animated:YES];
        testPlay.hidesBottomBarWhenPushed = YES;
    }else{
        [USER_DATA_MANAGER requestTestRegister:^(BOOL success) {
            
        }];
    }
}
#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.moneyPasswordView.textfield) {
        return textField.text.length - range.length + string.length < 5;
    }if (textField == self.nameView.textfield) {
        return textField.text.length - range.length + string.length < 13;
    }else{
        return YES;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
//    if (textField == self.VerifyView.textfield){
//        [self queryVaildData];
//    }点击焦点文本取消
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.text.length == 0) {
        return;
    }
    if (textField == self.nameView.textfield) {
        if ([LukeUserAdapter validateUserName:self.nameView.textfield.text]) {
            [SVProgressHUD showWithStatus:@"验证中..."];
            [NET_DATA_MANAGER requestCheckUniqueType:0 val:self.nameView.textfield.text Success:^(id responseObject) {
                if ([responseObject isEqualToString:@"true"]) {
                    [SVProgressHUD showErrorWithStatus:@"该帐号已存在"];
                }else{
                    [SVProgressHUD dismiss];
                }
            } failure:^(NSError *error) {
                [SVProgressHUD dismiss];
            }];
        }else{
            self.nameView.textfield.text = nil;
            [LukeMaskView lukeMaskViewWithTitle:@"输入的帐号有误，请重新输入!"];
        }
    }else if (textField == self.realNameView.textfield) {
        if ([LukeUserAdapter validateNickname:self.realNameView.textfield.text]) {

        }else{
            [LukeMaskView lukeMaskViewWithTitle:@"请输入正确的姓名!"];
            self.realNameView.textfield.text = nil;
        }
    }else if (textField == self.intrCodeView.textfield) {
        if(![LukeUserAdapter loginUserName:self.intrCodeView.textfield.text]) {
            [LukeMaskView lukeMaskViewWithTitle:@"推广码输入有误"];
            self.intrCodeView.textfield.text = nil;
        }
    }else if (textField == self.moneyPasswordView.textfield) {
        if (![LukeUserAdapter validateIsOnlyManyNumber:self.moneyPasswordView.textfield.text withFigure:4]) {
            [LukeMaskView lukeMaskViewWithTitle:@"输入的取款密码有误"];
            self.moneyPasswordView.textfield.text = nil;
        }
    }else if (textField == self.emailView.textfield) {
        if (![LukeUserAdapter validateEmail:self.emailView.textfield.text]) {
            [LukeMaskView lukeMaskViewWithTitle:@"输入的邮箱有误"];
            self.emailView.textfield.text = nil;
        }
    }else if (textField == self.QQView.textfield) {
        if (![LukeUserAdapter validateIsValidQQ:self.QQView.textfield.text]) {
            [LukeMaskView lukeMaskViewWithTitle:@"输入的qq有误"];
        }
    }else if (textField == self.wechatView.textfield){
        if ([self.wechatView.textfield.text containsString:@"*"]) {
            [LukeMaskView lukeMaskViewWithTitle:@"禁止包含特殊字符*"];
            self.wechatView.textfield.text = nil;
        }
    }
}

#pragma mark - 验证码请求
- (void)queryVaildData
{
    [NET_DATA_MANAGER requestGetVCodeByRandom:YES Success:^(id responseObject) {
        [self.VerifyView.dateBtn setBackgroundImage:responseObject forState:UIControlStateNormal];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (void)sureClick
{
    if (self.nameView.textfield.text.length && self.passwordView.textfield.text.length && self.passwordSureView.textfield.text.length) {
        
        if (![self.passwordView.textfield.text isPermissionllyPsw]) {
            return;
        }
        if (![self.passwordView.textfield.text isEqualToString:self.passwordSureView.textfield.text]) {
            self.passwordSureView.textfield.text = nil;
            [LukeMaskView lukeMaskViewWithTitle:@"两次输入的密码不一致"];
            return;
        }
        
        self.info = nil;
        NSString *passW = nil;
        NSString *passSureW = nil;
        if (BET_CONFIG.config.user_password_type && [BET_CONFIG.config.user_password_type isEqualToString:@"0"]) {
            passW = [self.passwordView.textfield.text MD5];
            passSureW = [self.passwordSureView.textfield.text MD5];
        }else{
            passW = self.passwordView.textfield.text;
            passSureW = self.passwordSureView.textfield.text;
        }
        
        self.info.account = self.nameView.textfield.text;
        self.info.password = passW;
        self.info.confirmPassword = passSureW;
        //推广码
        if (BET_CONFIG.registerConfig.intrMobileCode) {
            if ([BET_CONFIG.registerConfig.intrMobileCode integerValue]==1) {
                if (self.intrCodeView.textfield.text.length == 0) {
                    [LukeMaskView lukeMaskViewWithTitle:@"推广码必填哦"];
                    return;
                }
                self.info.intrCode = [self.intrCodeView.textfield.text removeSpaceAndNewline];

            }else if ([BET_CONFIG.registerConfig.intrMobileCode integerValue] == 0 && self.intrCodeView.textfield.text.length > 0){
                self.info.intrCode = [self.intrCodeView.textfield.text removeSpaceAndNewline];
            }
        }else{
            if (BET_CONFIG.registerConfig.intrCode == 1) {
                if (self.intrCodeView.textfield.text.length == 0) {
                    [LukeMaskView lukeMaskViewWithTitle:@"推广码必填哦"];
                    return;
                }
                self.info.intrCode = [self.intrCodeView.textfield.text removeSpaceAndNewline];
            }else if (BET_CONFIG.registerConfig.intrCode == 0 && self.intrCodeView.textfield.text.length > 0) {
                self.info.intrCode = [self.intrCodeView.textfield.text removeSpaceAndNewline];
            }
        }
        
        //真实姓名
        if (BET_CONFIG.registerConfig.fullName != 2 && self.realNameView.textfield.text.length) {
            self.info.fullName = self.realNameView.textfield.text.length ? self.realNameView.textfield.text : @"";
        }else if (BET_CONFIG.registerConfig.fullName == 1 && self.realNameView.textfield.text.length <= 0) {
            [SVProgressHUD showErrorWithStatus:@"真实姓名必填哦"];
            return;
        }
        
        //取款密码
        if ((!BET_CONFIG.registerConfig.fundPwd || BET_CONFIG.registerConfig.fundPwd != 2) && self.moneyPasswordView.textfield.text.length) {
            self.info.fundPwd = self.moneyPasswordView.textfield.text.length ? self.moneyPasswordView.textfield.text : @"";
        }else if (BET_CONFIG.registerConfig.fundPwd == 1 && self.moneyPasswordView.textfield.text.length <= 0) {
            [SVProgressHUD showErrorWithStatus:@"取款密码必填哦"];
            return;
        }
        
        //出生日期
        if (BET_CONFIG.registerConfig.birthday != 2 && self.birthdayView.dateBtn.currentTitle.length) {
            self.info.birthday = self.birthdayView.dateBtn.currentTitle.length ? self.birthdayView.dateBtn.currentTitle : @"";
        }else if (BET_CONFIG.registerConfig.birthday == 1 && self.birthdayView.dateBtn.currentTitle.length <= 0) {
            [SVProgressHUD showErrorWithStatus:@"出生日期必选哦"];
            return;
        }
        
        //邮箱
        if (BET_CONFIG.registerConfig.email != 2 && self.emailView.textfield.text.length) {
            self.info.email = self.emailView.textfield.text.length ? self.emailView.textfield.text : @"";
        }else if (BET_CONFIG.registerConfig.email == 1 && self.emailView.textfield.text.length <= 0) {
            [SVProgressHUD showErrorWithStatus:@"邮箱必填哦"];
            return;
        }
        
        //电话
        if (BET_CONFIG.registerConfig.phone != 2 && self.phoneView.textfield.text.length) {
            self.info.phone = self.phoneView.textfield.text.length ? self.phoneView.textfield.text : @"";
        }else if (BET_CONFIG.registerConfig.phone == 1 && self.phoneView.textfield.text.length <= 0) {
            [SVProgressHUD showErrorWithStatus:@"电话必填哦"];
            return;
        }
        
        if (BET_CONFIG.registerConfig.phone != 2 && self.smsCodeView.textfield.text.length) {
            self.info.registerPhoneCode = self.smsCodeView.textfield.text.length ? self.smsCodeView.textfield.text : @"";
        }else if (BET_CONFIG.registerConfig.phone == 1 && BET_CONFIG.registerConfig.registerPhone == 1 && self.smsCodeView.textfield.text.length <= 0) {
            [SVProgressHUD showErrorWithStatus:@"短信验证码不能为空"];
            return;
        }
        
        //qq
        if (BET_CONFIG.registerConfig.qq != 2 && self.QQView.textfield.text.length) {
            self.info.qq = self.QQView.textfield.text.length ? self.QQView.textfield.text : @"";
        }else if (BET_CONFIG.registerConfig.qq == 1 && self.QQView.textfield.text.length <= 0) {
            [SVProgressHUD showErrorWithStatus:@"QQ必填哦"];
            return;
        }
        
        //微信
        if (BET_CONFIG.registerConfig.weixin != 2 && self.wechatView.textfield.text.length) {
            self.info.weixin = self.wechatView.textfield.text.length ? self.wechatView.textfield.text : @"";
        }else if (BET_CONFIG.registerConfig.weixin == 1 && self.wechatView.textfield.text.length <= 0) {
            [SVProgressHUD showErrorWithStatus:@"微信必填哦"];
            return;
        }
        if(self.verifyType == VerifyAuthorType_IMAGE){
            self.info.vCode = self.VerifyView.textfield.text;
            if (!self.info.vCode.length) {
                [SVProgressHUD showErrorWithStatus:@"请输入图形验证码"];
                return;
            }
        }
        [self.verifyManager launchVerify];
    }else{
        [SVProgressHUD showErrorWithStatus:@"请完善信息"];
    }
}

#pragma mark - 注册请求
- (void)queryRegisterBy:(RegisterInfoData *)infoData
{
    [SVProgressHUD showWithStatus:@"注册中..."];
    [USER_DATA_MANAGER requestRegisteBy:infoData Completed:^(BOOL success) {
        [SVProgressHUD dismiss];
        if (success) {
            if (self.smsCodeView.textfield.text.length > 0 && self.phoneView.textfield.text.length > 0) {
                [NET_DATA_MANAGER requestModifyBindingPhone:self.phoneView.textfield.text smsCode:self.smsCodeView.textfield.text Success:^(id responseObject) {
                    
                } failure:^(NSError *error) {
                    
                }];
            }
            [self.navigationController popToRootViewControllerAnimated:YES];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.tabBarController.selectedIndex = 0;
                [UIViewController routerJumpToUrl:kRouterNoticeRegister];
                [SVProgressHUD showSuccessWithStatus:@"注册成功"];
            });
        }else{
            [self getCodeData];
        }
    }];
}


#pragma mark - VerifyManagerDelegate
-(void)verifyManager:(VerifyManager *)manager updateVerifyAuthorType:(VerifyAuthorType)type{
    self.verifyType = type;
}

-(void)verifyManager:(VerifyManager *)manager verificationSuccessParamers:(NSDictionary *)paramers authorType:(VerifyAuthorType)type{
    self.info.Ticket = paramers[@"ticket"];
    self.info.Randstr = paramers[@"randstr"];
    self.info.geetest_challenge = paramers[@"geetest_challenge"];
    self.info.geetest_seccode = paramers[@"geetest_seccode"];
    self.info.geetest_validate = paramers[@"geetest_validate"];
    [self queryRegisterBy:self.info];
}

-(void)verifyManager:(VerifyManager *)manager verificationFaildAuthorType:(VerifyAuthorType)type{
    
}
#pragma mark - GET/SET

-(VerifyManager *)verifyManager{
    if (!_verifyManager) {
        _verifyManager = [[VerifyManager alloc] init];
        _verifyManager.delegate = self;
    }
    return _verifyManager;
}

-(RegisterInfoData *)info{
    if (!_info) {
        _info = [[RegisterInfoData alloc] init];
    }
    return _info;
}
@end
