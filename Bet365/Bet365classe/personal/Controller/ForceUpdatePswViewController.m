//
//  ForceUpdatePswViewController.m
//  Bet365
//
//  Created by adnin on 2019/4/3.
//  Copyright © 2019年 jesse. All rights reserved.
//

#import "ForceUpdatePswViewController.h"

@interface ForceUpdatePswViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titileLabel;
@property (weak, nonatomic) IBOutlet UIButton *subMitBtn;
@property (weak, nonatomic) IBOutlet UIButton *serverBtn;
@property (weak, nonatomic) IBOutlet UITextField *surePswTf;
@property (weak, nonatomic) IBOutlet UITextField *pswTf;
@end


@implementation ForceUpdatePswViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Events
- (IBAction)closeAction:(id)sender {
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:^{
        self.closeHandle ? self.closeHandle() : nil;
    }];
}

- (IBAction)serverAction:(id)sender {
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:^{
        self.serverHandle ? self.serverHandle() : nil;
    }];
    
}

- (IBAction)submitAction:(id)sender {
    
    [self.view endEditing:YES];
    if (!self.pswTf.text.length) {
        [SVProgressHUD showErrorWithStatus:@"请输入【新密码】"];
        return;
    }
    
    if (![self.pswTf.text isPermissionllyPsw]) {
        return;
    }

    if (!self.surePswTf.text.length) {
        [SVProgressHUD showErrorWithStatus:@"请输入【确认密码】"];
        return;
    }
    
    if (![self.pswTf.text isEqualToString:self.surePswTf.text]) {
        [SVProgressHUD showErrorWithStatus:@"确认密码不一致"];
        return;
    }
    self.submitHandle ? self.submitHandle(self.pswTf.text) : nil;
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
