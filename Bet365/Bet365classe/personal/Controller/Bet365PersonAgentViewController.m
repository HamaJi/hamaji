//
//  Bet365PersonAgentViewController.m
//  Bet365
//
//  Created by luke on 2018/1/20.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Bet365PersonAgentViewController.h"
#import "LukeTeamCountViewController.h"

@interface Bet365PersonAgentTableHeaderView : UIView

@end

@implementation Bet365PersonAgentTableHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]){
        self.backgroundColor = YCZColor(239, 239, 244);
        UIImageView *personImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"person"]];
        [self addSubview:personImage];
        [personImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.leading.equalTo(self).offset(30);
            make.size.mas_equalTo(CGSizeMake(50, 50));
        }];
        
        UILabel *accountLb = [[UILabel alloc] init];
        accountLb.font = [UIFont systemFontOfSize:IS_IPHONE_5s ? 15 : 17];
        accountLb.textColor = [UIColor blackColor];
        [self addSubview:accountLb];
        [accountLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(personImage.mas_centerY);
            make.leading.equalTo(personImage.mas_trailing).offset(20);
        }];
        accountLb.attributedText = [self setTitleWithTitle:@"用户帐号: " subTitle:USER_DATA_MANAGER.userInfoData.account];
    }
    return self;
}

- (NSMutableAttributedString *)setTitleWithTitle:(NSString *)title subTitle:(NSString *)subtitle
{
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",title,subtitle]];
    [attStr setAttributes:@{NSForegroundColorAttributeName : YCZGrayColor(100)} range:NSMakeRange(0, title.length)];
    return attStr;
}

@end

@interface Bet365PersonAgentViewController ()<UITableViewDelegate,UITableViewDataSource>
/** 数据数组 */
@property (nonatomic,strong) NSArray *dataArrs;
/**  */
@property (nonatomic,strong) UITableView *tableView;
/**  */
@property (nonatomic,strong) Bet365PersonAgentTableHeaderView *headerView;
/**  */
@property (nonatomic,strong) NSArray *Controllers;

@end

@implementation Bet365PersonAgentViewController
- (NSArray *)Controllers
{
    if (_Controllers == nil) {
        _Controllers = @[@"LukeSubUsersViewController",@"LukeRegistrManViewController",@"LukeRebatesViewController",@"LukeTeamCountViewController"];
    }
    return _Controllers;
}
- (Bet365PersonAgentTableHeaderView *)headerView
{
    if (_headerView == nil) {
        _headerView = [[Bet365PersonAgentTableHeaderView alloc] init];
        _headerView.ycz_height = 150;
    }
    return _headerView;
}
- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.backgroundColor = [UIColor skinViewScreenColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.tableHeaderView = self.headerView;
        _tableView.tableFooterView = [UIView new];
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
    }
    return _tableView;
}
- (NSArray *)dataArrs
{
    if (_dataArrs == nil) {
        _dataArrs = @[@"用户列表",@"注册管理",@"推广链接",@"团队总览"];
    }
    return _dataArrs;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = YCZColor(239, 239, 244);
    [self tableView];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArrs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * const tableViewID = @"tableViewID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tableViewID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:tableViewID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = self.dataArrs[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.contentView.backgroundColor = [UIColor whiteColor];
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (indexPath.row == self.Controllers.count - 1) {
//        LukeTeamCountViewController *subVC = [[LukeTeamCountViewController alloc] init];
//        subVC.subAccount = USER_DATA_MANAGER.userInfoData.uid;
//        subVC.type = @"team";
//        [self.navigationController pushViewController:subVC animated:YES];
//    }else{
//        UIViewController *VC = [[NSClassFromString(self.Controllers[indexPath.row]) alloc] init];
//        [self.navigationController pushViewController:VC animated:YES];
//    }
//}

@end
