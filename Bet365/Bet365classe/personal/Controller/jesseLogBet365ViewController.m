//
//  jesseLogBet365ViewController.m
//  Bet365
//
//  Created by jesse on 2017/4/13.
//  Copyright © 2017年 jesse. All rights reserved.
#import "jesseLogBet365ViewController.h"
#import "LukeRegisterViewController.h"
#import "LukeForgetViewController.h"
#import "Bet365TabbarViewController.h"

#import "jesseTestPlayViewController.h"
#import "LukeUserAdapter.h"
#import "NetWorkMannager+Account.h"
#import "Bet365Tool.h"
#import "NSString+transSegmentationClass.h"

#import "SwitchSlideButton.h"

#import "LoginPhoneCenterView.h"
#import "LoginAccountCenterView.h"

#import "UserLoginLimitAlertView.h"

typedef NS_ENUM(NSInteger, LoginPattern) {
    LoginPattern_ACCOUNT = 0,
    LoginPattern_PHONE,
};

typedef NS_ENUM(NSInteger, ValiCodeState) {
    ValiCodeState_FAILD = 0,
    ValiCodeState_EDITING,
    ValiCodeState_UNVERIFY
};

@interface jesseLogBet365ViewController ()
<alertBlockDelegate,
SwitchSlideButtonDelegate,
LoginCenterViewDelegate>

@property (nonatomic,strong)UIButton *rememberBtn;

@property (nonatomic,strong)UIButton *serviceBtn;

@property (nonatomic,strong)UIButton *sureBtn;

@property (nonatomic,assign)LoginPattern loginPattern;

@property (nonatomic,strong) SwitchSlideButton *switchBtn;

@property (nonatomic,strong) LoginPhoneCenterView *phoneCenterView;

@property (nonatomic,strong) LoginAccountCenterView *accountCenterView;
/**验证信息配置*/
@property(nonatomic,strong) NSArray *security_config;
@property (nonatomic,strong) NSDictionary *paramers;


@end

RouterKey *const kRouterLogin = @"login";

@implementation jesseLogBet365ViewController

+(void)load{
    
    [self registerJumpRouterKey:kRouterLogin toHandle:^(NSDictionary *parameters) {
        [USER_DATA_MANAGER shouldPush2Login];
    }];
    
    [self registerInstanceRouterKey:kRouterLogin toHandle:^UIViewController *(NSDictionary *parameters) {
        jesseLogBet365ViewController *vc = [[jesseLogBet365ViewController alloc] init];
        return vc;
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self setupUI];
    
    [self addRAC];
    if (BET_CONFIG.userSettingData.fv.length) {
        [self requestLoginFV:BET_CONFIG.userSettingData.fv];
    }
    
}
-(void)dealloc{
    
}
#pragma mark - private
- (void)setupUI{



    self.sureBtn = [UIButton addButtonWithTitle:@"登录" font:15 color:[UIColor skinViewKitNorColor] target:self action:@selector(loginClick:)];
    [self.sureBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];
    self.sureBtn.layer.cornerRadius = 3;
    self.sureBtn.layer.masksToBounds = YES;
    [self.view addSubview:self.sureBtn];
//
//    self.loginPattern = LoginPattern_ACCOUNT;
//    [self.switchBtn setIndex:0 Animated:YES];
//    [self.accountCenterView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.switchBtn.mas_bottom).offset(10);
//        make.left.right.equalTo(self.view);
//    }];
//
//    [self.phoneCenterView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.switchBtn.mas_bottom).offset(10);
//        make.left.right.equalTo(self.view);
//    }];
//
//    [self.sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.accountCenterView.mas_bottom).offset(15);
//        make.leading.equalTo(self.view).offset(10);
//        make.trailing.equalTo(self.view).offset(-10);
//        make.height.mas_equalTo(45);
//    }];
//
//    self.accountCenterView.alpha = 1.0;
//    self.accountCenterView.hidden = NO;
//    self.phoneCenterView.alpha = 0.0;
//    self.phoneCenterView.hidden = YES;
    
    if ([BET_CONFIG.limit.loginPhone integerValue] == 1) {
        self.loginPattern = LoginPattern_ACCOUNT;
        [self.switchBtn setIndex:0 Animated:YES];
        [self.accountCenterView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.switchBtn.mas_bottom).offset(10);
            make.left.right.equalTo(self.view);
        }];

        [self.phoneCenterView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.switchBtn.mas_bottom).offset(10);
            make.left.right.equalTo(self.view);
        }];

        [self.sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.accountCenterView.mas_bottom).offset(15);
            make.leading.equalTo(self.view).offset(10);
            make.trailing.equalTo(self.view).offset(-10);
            make.height.mas_equalTo(45);
        }];

        self.accountCenterView.alpha = 1.0;
        self.accountCenterView.hidden = NO;
        self.phoneCenterView.alpha = 0.0;
        self.phoneCenterView.hidden = YES;
    }else{
        self.loginPattern = LoginPattern_ACCOUNT;
        [self.accountCenterView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view).offset(20);
            make.left.right.equalTo(self.view);
        }];

        [self.sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.accountCenterView.mas_bottom).offset(15);
            make.leading.equalTo(self.view).offset(10);
            make.trailing.equalTo(self.view).offset(-10);
            make.height.mas_equalTo(45);
        }];
    }
    
    UIButton *registerBtn = [UIButton addButtonWithTitle:@"立即注册" font:15 color:[UIColor skinViewKitSelColor] target:self action:@selector(registerPushController)];
    registerBtn.backgroundColor = [UIColor skinViewKitNorColor];
    registerBtn.layer.cornerRadius = 3;
    registerBtn.layer.masksToBounds = YES;
    registerBtn.layer.borderWidth = 1;
    registerBtn.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
    [self.view addSubview:registerBtn];
    [registerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.sureBtn.mas_bottom).offset(15);
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-10);
        make.height.mas_equalTo(45);
    }];
    
    UIButton *serviceBtn = [UIButton addButtonWithTitle:@"在线客服" font:15 color:[UIColor skinViewKitSelColor] target:self action:@selector(customService)];
    serviceBtn.backgroundColor = [UIColor skinViewKitNorColor];
    serviceBtn.layer.cornerRadius = 3;
    serviceBtn.layer.masksToBounds = YES;
    serviceBtn.layer.borderWidth = 1;
    serviceBtn.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
    [self.view addSubview:serviceBtn];
    [serviceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(registerBtn.mas_bottom).offset(15);
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-10);
        make.height.mas_equalTo(45);
    }];
    
    UIButton *testBtn = [UIButton addButtonWithTitle:@"免费试玩" font:15 color:[UIColor skinViewKitSelColor] target:self action:@selector(testPlay)];
    testBtn.backgroundColor = [UIColor skinViewKitNorColor];
    testBtn.layer.cornerRadius = 3;
    testBtn.layer.masksToBounds = YES;
    testBtn.layer.borderWidth = 1;
    testBtn.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
    [self.view addSubview:testBtn];
    [testBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(serviceBtn.mas_bottom).offset(15);
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-10);
        make.height.mas_equalTo(45);
    }];
    
    UIButton *backBtn = [UIButton addButtonWithTitle:@"返回首页" font:15 color:[UIColor skinViewKitSelColor] target:self action:@selector(backClick)];
    backBtn.backgroundColor = [UIColor skinViewKitNorColor];
    backBtn.layer.cornerRadius = 3;
    backBtn.layer.masksToBounds = YES;
    backBtn.layer.borderWidth = 1;
    backBtn.layer.borderColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1].CGColor;
    [self.view addSubview:backBtn];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(testBtn.mas_bottom).offset(15);
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-10);
        make.height.mas_equalTo(45);
    }];
}

- (void)addRAC
{
    @weakify(self);
    [RACObserve(self, security_config) subscribeNext:^(NSArray *security_c) {
        @strongify(self);
        if (security_c) {
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict safeSetObject:[self.paramers objectForKey:kLRKeyAccount] forKey:@"account"];
            [dict safeSetObject:[self.paramers objectForKey:kLRKeyPwdWithoutMD5] forKey:@"oldPassword"];
            [self initPwdAlertPushParameter:security_c withForpersonMessage:dict clickOther:^{
                [self customService];
            }];
        }
    } completed:^{
        
    }];
    
}

- (void)backClick{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)rememberBtnClick{

}

- (void)getLoginLimit{
    [SVProgressHUD showWithStatus:@"加载中..."];
    [NET_DATA_MANAGER requestLoginLimitSuccess:^(NSDictionary *responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject.allKeys containsObject:@"initPwdVerifyFields"]) {
            NSString *verifyFie = responseObject[@"initPwdVerifyFields"];
            if (verifyFie) {
                self.security_config = [@"" securityParmater:verifyFie];
            }
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

- (void)getLoginLimitForIpLocationVerify{
    [SVProgressHUD showWithStatus:@"加载中..."];
    [NET_DATA_MANAGER requestLoginLimitSuccess:^(NSDictionary *responseObject) {
        [SVProgressHUD dismiss];
        [self ipLocationVerifyWithresponseObject:responseObject];
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

#pragma mark - LoginCenterViewDelegate
-(void)loginCenterView:(LoginCenterView *)centerView TextFiledSubmitText:(NSString *)text Key:(LoginRequestKey *)key{
    
}

-(void)loginCenterView:(LoginCenterView *)centerView LoginWithParamers:(NSDictionary *)paramers{
    if (paramers) {
        [self requestLoginWithParamers:paramers];
    }
}

-(void)loginCenterView:(LoginCenterView *)centerView TapAction:(SEL)tapAction{
    ((void (*)(id, SEL))[self methodForSelector:tapAction])(self, tapAction);
}


#pragma mark - private 登陆
- (void)loginClick:(UIButton *)sender{
    [self.view endEditing:YES];
    if (self.loginPattern == LoginPattern_PHONE) {
        [self.phoneCenterView extractLoginParamers];
    }else if (self.loginPattern == LoginPattern_ACCOUNT){
        [self.accountCenterView extractLoginParamers];
    }
}

/**
 执行登录

 */
- (void)requestLoginWithParamers:(NSDictionary *)paramers{
    self.paramers = paramers;
    void(^loginRequestSuccess)() = ^(){
        
        if ([paramers containsObjectForKey:kLRKeyAccount]) {
            /** 账号密码缓存**/
            AUTHDATA_MANAGER.account = [paramers objectForKey:kLRKeyAccount];
            AUTHDATA_MANAGER.passWord = [paramers objectForKey:kLRKeyPwdWithoutMD5];
            [AUTHDATA_MANAGER save];
        }else if ([paramers containsObjectForKey:kLRKeyLoginPhone]){
            AUTHDATA_MANAGER.phone = [paramers objectForKey:kLRKeyLoginPhone];
            [AUTHDATA_MANAGER save];
        }
        
        [[NAVI_MANAGER getCurrentVC].navigationController popToRootViewControllerAnimated:NO];
        [NAVI_MANAGER getCurrentVC].tabBarController.selectedIndex = 0;
        if (![BET_CONFIG.config.vhy_login_tip isNoBlankString] && [USER_DATA_MANAGER.userInfoData.type isEqualToString:@"VHY"]) {
            //            @weakify(self);
            [self alertWithTitle:@"此账号为推广账号并非正常会员" message:@"推广账号为虚假账号,资金提交将自动入款!" cancelActionTitle:@"取消" cancelHandler:nil sureActionTitle:@"确认" sureHandler:^(UIAlertAction *action) {
                //                @strongify(self);
                //                [self getLoginLimit];
            }];
        }
        [UIViewController routerJumpToUrl:kRouterNoticeLogin];
    };
    void(^loginRequestFailed)(NSError *error) = ^(NSError *error){
        if (error.init_pwd){
            @weakify(self);
            [self alertWithTitle:@"新平台升级" message:@"为了保证你的账号安全,请进行安全认证并进行修改你的登录密码!" cancelActionTitle:@"取消" cancelHandler:nil sureActionTitle:@"确认" sureHandler:^(UIAlertAction *action) {
                @strongify(self);
                [self getLoginLimit];
            }];
        }else if (error.LIMITED_ONLY_PHONE_LOGIN_ERROR == YES &&
                  [BET_CONFIG.limit.loginPhone integerValue] == 1){
            [self.switchBtn setIndex:1 Animated:YES];
            [self tapUpSwitchSlideButton:self.switchBtn Index:1];
            [SVProgressHUD showErrorWithStatus:@"当前账号已绑定过手机，请使用手机号登陆"];
        }else if (error.limit_IPlocation){
            UIAlertController *alertVc = [UIAlertController alertControllerWithTitle:@"温馨提示" message:error.message preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *sure = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                if (error.url_link.length > 0) {
                    [UIViewController routerJumpToUrl:error.url_link];
                }
            }];
            [alertVc addAction:sure];
            [self presentViewController:alertVc animated:YES completion:nil];
        }else if (error.limit_IPlocation_verify){
            [self getLoginLimitForIpLocationVerify];
        }else{
            if (self.loginPattern == LoginPattern_ACCOUNT) {
                [self.accountCenterView getCodeData];
            }else if (self.loginPattern == LoginPattern_PHONE){
                [self.phoneCenterView getCodeData];
            }
            [SVProgressHUD showErrorWithStatus:error.msg];
        }
    };

    [USER_DATA_MANAGER requestLoginWithParamers:paramers Completed:^(BOOL success, NSError *error) {
        if (success && !error) {
            loginRequestSuccess();
        }else{
            loginRequestFailed(error);
        }
    } NeedChangePswHandle:^(BOOL success) {
        self.accountCenterView.password = @"";
    }];

}

- (void)requestLoginFV:(NSString *)fv{
    void(^loginRequestSuccess)() = ^(){
       
        [[NAVI_MANAGER getCurrentVC].navigationController popToRootViewControllerAnimated:NO];
        [NAVI_MANAGER getCurrentVC].tabBarController.selectedIndex = 0;
        if (![BET_CONFIG.config.vhy_login_tip isNoBlankString] && [USER_DATA_MANAGER.userInfoData.type isEqualToString:@"VHY"]) {
            //            @weakify(self);
            [self alertWithTitle:@"此账号为推广账号并非正常会员" message:@"推广账号为虚假账号,资金提交将自动入款!" cancelActionTitle:@"取消" cancelHandler:nil sureActionTitle:@"确认" sureHandler:^(UIAlertAction *action) {
                //                @strongify(self);
                //                [self getLoginLimit];
            }];
        }
        [UIViewController routerJumpToUrl:kRouterNoticeLogin];
    };
    void(^loginRequestFailed)(NSError *error) = ^(NSError *error){
        if (error.init_pwd){
            @weakify(self);
            [self alertWithTitle:@"新平台升级" message:@"为了保证你的账号安全,请进行安全认证并进行修改你的登录密码!" cancelActionTitle:@"取消" cancelHandler:nil sureActionTitle:@"确认" sureHandler:^(UIAlertAction *action) {
                @strongify(self);
                [self getLoginLimit];
            }];
        }else if (error.LIMITED_ONLY_PHONE_LOGIN_ERROR == YES &&
                  [BET_CONFIG.limit.loginPhone integerValue] == 1){
            [self.switchBtn setIndex:1 Animated:YES];
            [self tapUpSwitchSlideButton:self.switchBtn Index:1];
            [SVProgressHUD showErrorWithStatus:@"当前账号已绑定过手机，请使用手机号登陆"];
        }else if (error.limit_IPlocation){
            UIAlertController *alertVc = [UIAlertController alertControllerWithTitle:@"温馨提示" message:error.message preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *sure = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                if (error.url_link.length > 0) {
                    [UIViewController routerJumpToUrl:error.url_link];
                }
            }];
            [alertVc addAction:sure];
            [self presentViewController:alertVc animated:YES completion:nil];
        }else if (error.limit_IPlocation_verify){
            [self getLoginLimitForIpLocationVerify];
        }else{
            if (self.loginPattern == LoginPattern_ACCOUNT) {
                [self.accountCenterView getCodeData];
            }else if (self.loginPattern == LoginPattern_PHONE){
                [self.phoneCenterView getCodeData];
            }
            [SVProgressHUD showErrorWithStatus:error.msg];
        }
    };
    
    [USER_DATA_MANAGER requestLoginFV:fv Completed:^(BOOL success, NSError *error) {
        if (success && !error) {
            loginRequestSuccess();
        }else{
            loginRequestFailed(error);
        }
    } NeedChangePswHandle:^(BOOL success) {
        self.accountCenterView.password = @"";
    }];
    

}

#pragma mark - UITextFieldDelegate


#pragma mark - SwitchSlideButtonDelegate
-(BOOL)tapUpSwitchSlideButton:(SwitchSlideButton *)button Index:(NSUInteger)idx{
    self.loginPattern = idx;
    if (idx == LoginPattern_ACCOUNT) {
        self.accountCenterView.hidden = NO;
        [UIView animateWithDuration:0.4 animations:^{
            [self.sureBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.leading.equalTo(self.view).offset(10);
                make.trailing.equalTo(self.view).offset(-10);
                make.height.mas_equalTo(45);
                make.top.equalTo(self.accountCenterView.mas_bottom).offset(15);
            }];

            self.accountCenterView.alpha = 1.0;
            self.phoneCenterView.alpha = 0.0;
        } completion:^(BOOL finished) {
            self.phoneCenterView.hidden = YES;
        }];
        [self.view layoutIfNeeded];
    }else if (idx == LoginPattern_PHONE){
        self.phoneCenterView.hidden = NO;
        [UIView animateWithDuration:0.4 animations:^{
            [self.sureBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.leading.equalTo(self.view).offset(10);
                make.trailing.equalTo(self.view).offset(-10);
                make.height.mas_equalTo(45);
                make.top.equalTo(self.accountCenterView.mas_bottom).offset(15);
            }];
            self.accountCenterView.alpha = 0.0;
            self.phoneCenterView.alpha = 1.0;
        } completion:^(BOOL finished) {
            self.accountCenterView.hidden = YES;
        }];
        [self.view layoutIfNeeded];
    }
    return YES;
}


//注册跳转
-(void)registerPushController
{
    LukeRegisterViewController *registerC = [[LukeRegisterViewController alloc]init];
    registerC.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController pushViewController:registerC animated:YES];
}
//忘记密码跳转
-(void)forgetPushController
{
    if (BET_CONFIG.common_config.forgetPasswordTip.length > 0) {
        [self alertWithSureTitle:@"温馨提示" message:BET_CONFIG.common_config.forgetPasswordTip];
    }else{
        LukeForgetViewController *forgetVC = [[LukeForgetViewController alloc] init];
        [self.navigationController pushViewController:forgetVC animated:YES];
    }
}
/**试玩跳转**/
-(void)testPlay{
    if (BET_CONFIG.registerConfig.trailUserValidCode) {
        jesseTestPlayViewController *testPlay = [[jesseTestPlayViewController alloc]init];
        [self.navigationController pushViewController:testPlay animated:YES];
    }else{
        [USER_DATA_MANAGER requestTestRegister:^(BOOL success) {
            
        }];
    }
}

//     ip地址变更校验
- (void)ipLocationVerifyWithresponseObject:(NSDictionary*)responseObject{
    if ([responseObject[@"ipLocationVerifyTurnOn"] isKindOfClass:[NSNumber class]] && [responseObject[@"ipLocationVerifyTurnOn"] isEqualToNumber:@1]) {
        UserLoginLimitAlertView *userLoginLimitAlertView = [[UserLoginLimitAlertView alloc] init];
        userLoginLimitAlertView.params = [self.paramers copy];
        userLoginLimitAlertView.ipLocationVerifyFields = [responseObject[@"ipLocationVerifyFields"] componentsSeparatedByString:@"|"];
        userLoginLimitAlertView.ipLocationVerifyCount = [responseObject[@"ipLocationVerifyCount"] integerValue];

        [[UIApplication sharedApplication].keyWindow addSubview:userLoginLimitAlertView];
        [userLoginLimitAlertView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsZero);
        }];
    }
}

/**客服跳转**/
-(void)customService{
    [UIViewController routerJumpToUrl:BET_CONFIG.common_config.zxkfPath];
}

#pragma mark - GET/SET
-(SwitchSlideButton *)switchBtn{
    if(!_switchBtn){
        _switchBtn = [[SwitchSlideButton alloc] init];
        [self.view addSubview:_switchBtn];
        [_switchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view).offset(20);
            make.left.equalTo(self.view).offset(30);
            make.right.equalTo(self.view).offset(-30);
            make.height.mas_equalTo(40);
        }];
        _switchBtn.backgroundColor = [UIColor skinViewBgColor];
        _switchBtn.slideBackgroundImage = [UIImage skinGradientSelImage];
        _switchBtn.titileColor = [UIColor skinViewKitSelColor];
        _switchBtn.titileSelColor = [UIColor skinViewKitNorColor];
        _switchBtn.list = @[@"账号登录",@"手机登录"];
        _switchBtn.delegate = self;
    }
    return _switchBtn;
}

-(LoginPhoneCenterView *)phoneCenterView{
    if (!_phoneCenterView) {
        _phoneCenterView = [[LoginPhoneCenterView alloc] init];
        _phoneCenterView.delegate = self;
        [self.view addSubview:_phoneCenterView];
    }
    return _phoneCenterView;
}

-(LoginAccountCenterView *)accountCenterView{
    if (!_accountCenterView) {
        _accountCenterView = [[LoginAccountCenterView alloc] init];
        _accountCenterView.delegate = self;
        _accountCenterView.remenberAction = @selector(rememberBtnClick);
        _accountCenterView.forgetAction = @selector(forgetPushController);
        [self.view addSubview:_accountCenterView];
    }
    return _accountCenterView;
}

@end
