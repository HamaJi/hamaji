//
//  YHApplicationPopViewController.h
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/28.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "Bet365ViewController.h"
#import "PrmtLobbyModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YHApplicationPopViewController : Bet365ViewController

@property (nonatomic,strong) PrmtYHApplication *model;

@end

NS_ASSUME_NONNULL_END
