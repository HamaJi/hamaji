//
//  YEBaoTransferViewController.h
//  Bet365
//
//  Created by super_小鸡 on 2019/6/9.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YEBaoInfo.h"

NS_ASSUME_NONNULL_BEGIN


typedef void(^YEBaoTransferInBlock)(double money);

typedef void(^YEBaoTransferOutBlock)(double money,NSString *psw);

@interface YEBaoTransferViewController : Bet365ViewController

@property (nonatomic,strong) YEBaoInfo *info;

@property (nonatomic,copy) YEBaoTransferInBlock inBlock;

@property (nonatomic,copy) YEBaoTransferOutBlock outBlock;

@end

NS_ASSUME_NONNULL_END
