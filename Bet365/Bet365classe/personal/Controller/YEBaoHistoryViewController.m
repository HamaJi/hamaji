//
//  YEBaoHistoryViewController.m
//  Bet365
//
//  Created by super_小鸡 on 2019/6/10.
//  Copyright © 2019 jesse. All rights reserved.
//
#pragma mark - Controller
#import "YEBaoHistoryViewController.h"

#pragma mark - View
#import "YEBaoHistoryTableViewCell.h"

#pragma mark - Command
#import "YEBaoInfo.h"
#import "NetWorkMannager+Account.h"



@interface YEBaoHistoryViewController ()
<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (assign, nonatomic) NSUInteger pageIdx;

@property (strong, nonatomic) NSMutableArray <YEBaoInerestInfo *>*dataList;

@property (strong, nonatomic) NSNumber *minRate;

@property (strong, nonatomic) NSNumber *maxRate;

@end

@implementation YEBaoHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    self.tableView.backgroundColor = [UIColor skinViewScreenColor];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshHistory)];
    
//    [self.tableView registerNib:[UINib nibWithNibName:@"YEBaoHistoryTableViewCell" bundle:nil] forCellReuseIdentifier:@"YEBaoHistoryTableViewCell"];
    double money = [[self.interestMoney decimalNumberByRoundingDown] doubleValue];
    self.moneyLabel.text = [NSString stringWithFormat:@"%.2f",money];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView.mj_header beginRefreshing];
    });
    [self instanNaviBar];
    // Do any additional setup after loading the view from its nib.
}

-(void)instanNaviBar{
    self.naviRightItem = Bet365NaviRightItemBalance;
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setTitle:@"返回" forState:UIControlStateNormal];
    UIImage *img = [UIImage imageNamed:@ "left_lage" ];
    img = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [leftBtn setImage:img forState:UIControlStateNormal];
    [leftBtn setTintColor:[UIColor skinNaviTintColor]];
    [leftBtn setTitleColor:[UIColor skinNaviTintColor] forState:UIControlStateNormal];
    [leftBtn setImagePosition:LXMImagePositionLeft spacing:5];
    [leftBtn addTarget:self action:@selector(dismissAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftButton;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    YEBaoHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YEBaoHistoryTableViewCell"];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"YEBaoHistoryTableViewCell" owner:self options:nil].firstObject;
    }
    
    YEBaoInerestInfo *inerestInfo = [self.dataList safeObjectAtIndex:indexPath.row];
    cell.date = inerestInfo.date;
    cell.isFirst = indexPath.row == 0;
    [cell setRate:inerestInfo.interestMoney
   BetweenMinRete:self.minRate
          MaxRate:self.maxRate];
    return cell;
}

#pragma mark - Events
-(void)dismissAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - GET/SET
-(NSMutableArray<YEBaoInerestInfo *> *)dataList{
    if (!_dataList) {
        _dataList = @[].mutableCopy;
    }
    return _dataList;
}


#pragma mark - NetRequest
-(void)refreshHistory{
    self.pageIdx = 0;
    [self getHistory];
}
-(void)getHistory{
    [NET_DATA_MANAGER requestYEBaoHistoryWithPage:self.pageIdx + 1 Rows:30 Success:^(id responseObject) {
        if (self.pageIdx == 0) {
            [self.dataList removeAllObjects];
        }
        self.pageIdx += 1;
        NSError *error;
        NSMutableArray <YEBaoInerestInfo *>*list = [[MTLJSONAdapter modelsOfClass:YEBaoInerestInfo.class fromJSONArray:responseObject[@"data"] error:&error] mutableCopy];
        [list enumerateObjectsUsingBlock:^(YEBaoInerestInfo * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.interestMoney = [obj.interestMoney decimalNumberByRoundingDown];
        }];
        [self.dataList addObjectsFromArray:list];
        [self.dataList enumerateObjectsUsingBlock:^(YEBaoInerestInfo * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (idx == 0) {
                self.minRate = obj.interestMoney;
                self.maxRate = obj.interestMoney;
            }
            if ([self.minRate compare:obj.interestMoney] == NSOrderedDescending) {
                self.minRate = obj.interestMoney;
            }
            if ([self.maxRate compare:obj.interestMoney] == NSOrderedAscending) {
                self.maxRate = obj.interestMoney;
            }
        }];
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
