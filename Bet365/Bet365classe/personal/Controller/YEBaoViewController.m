//
//  YEBaoViewController.m
//  Bet365
//
//  Created by super_小鸡 on 2019/6/7.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "YEBaoViewController.h"

#pragma mark - View
#import "AAChartKit.h"

#pragma mark - Command
#import "NetWorkMannager+Account.h"
#import "UIViewControllerSerializing.h"
#import "YEBaoInfo.h"
#import "NetWorkMannager+activity.h"
#pragma mark - ViewController
#import "YEBaoTransferViewController.h"
#import "YEBaoHistoryViewController.h"

@interface YEBaoViewController ()
<UIViewControllerSerializing,
AAChartViewEventDelegate>

#pragma mark - UI
@property (strong, nonatomic) AAChartView *lineChartView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIView *headView;
@property (weak, nonatomic) IBOutlet UIImageView *headBgImageView;

@property (weak, nonatomic) IBOutlet UILabel *yestodayIncomeTopLabel;

@property (weak, nonatomic) IBOutlet UILabel *yestodayIncomeLabel;

@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;

@property (weak, nonatomic) IBOutlet UIButton *balanceHidenBtn;

@property (weak, nonatomic) IBOutlet UIImageView *lineImageView;


@property (weak, nonatomic) IBOutlet UILabel *totalIncomeTopLabel;

@property (weak, nonatomic) IBOutlet UILabel *incomProportionTopLabel;

@property (weak, nonatomic) IBOutlet UILabel *achievementTopLabel;



@property (weak, nonatomic) IBOutlet UILabel *totalIncomeLabel;

@property (weak, nonatomic) IBOutlet UILabel *incomProportionLabel;

@property (weak, nonatomic) IBOutlet UILabel *achievementLabel;

@property (weak, nonatomic) IBOutlet UILabel *instructionsLabel;

@property (weak, nonatomic) IBOutlet UIButton *withDrawBtn;

@property (weak, nonatomic) IBOutlet UIButton *topUpBtn;

#pragma mark - Data

@property (nonatomic, strong) AAChartModel *aaChartModel;

@property (strong, nonatomic) YEBaoInfo *info;

@end

@implementation YEBaoViewController

RouterKey *const kRouterYEBao = @"yubao";

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    if (USER_DATA_MANAGER.isLogin) {
        if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"TEST"]) {
            [SVProgressHUD showErrorWithStatus:@"试玩账号无法使用余额宝，请登录平台账号"];
            return NO;
        }
//        if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"VHY"]) {
//            [SVProgressHUD showErrorWithStatus:@"您的账号不能使用余额宝，请联系客服处理"];
//            return NO;
//        }
    }
    return [USER_DATA_MANAGER shouldPush2Login];
}

+(void)load{
    
    [self registerJumpRouterKey:kRouterYEBao toHandle:^(NSDictionary *parameters) {
        YEBaoViewController *vc = [[YEBaoViewController alloc] initWithNibName:@"YEBaoViewController" bundle:nil];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
        vc.title = @"余额宝";
    }];
    
    [self registerInstanceRouterKey:kRouterYEBao toHandle:^UIViewController *(NSDictionary *parameters) {
        YEBaoViewController *vc = [[YEBaoViewController alloc] initWithNibName:@"YEBaoViewController" bundle:nil];
        vc.title = @"余额宝";
        return vc;
    }];
}

- (void)viewDidLoad {
    self.contentView.userInteractionEnabled = YES;
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.scrollView addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self.scrollView);
        make.width.equalTo(self.scrollView.mas_width);
        make.height.mas_equalTo(700);
    }];
    [self.scrollView setContentSize:CGSizeMake(0, 700)];
    [self requestAllCompleted:^(BOOL isSuccess) {
        if (isSuccess) {
            [self lineChartDataSouce];
        }
    }];
    @weakify(self);

    [self.headBgImageView setBackgroundColor:[UIColor skinViewContentColor]];
    [self.headBgImageView addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
        @strongify(self);
        [self headBgAction];
    }];
    self.yestodayIncomeLabel.textColor = [UIColor skinTextItemNorColor];
    self.yestodayIncomeTopLabel.textColor = [UIColor skinTextItemNorColor];
    self.balanceLabel.textColor = [UIColor skinTextItemNorColor];
    
    self.totalIncomeTopLabel.textColor = [UIColor skinTextItemNorColor];
    self.incomProportionTopLabel.textColor = [UIColor skinTextItemNorColor];
    self.achievementTopLabel.textColor = [UIColor skinTextItemNorColor];
    
    self.totalIncomeLabel.textColor = [UIColor skinTextItemNorColor];
    self.incomProportionLabel.textColor = [UIColor skinTextItemNorColor];
    self.achievementLabel.textColor = [UIColor skinTextItemNorColor];
    
    self.instructionsLabel.textColor = [UIColor skinTextItemNorColor];
    
    
    [self addObserver];
    
    self.lineImageView.backgroundColor = [UIColor skinViewKitSelColor];
//    [self.topUpBtn setBackgroundImage:[UIImage imageNamed:@"hor_btn_nor"] forState:UIControlStateNormal];
//    [self.topUpBtn setImage:[UIImage imageNamed:@"hor_yebao_recharge_hightligth"] forState:UIControlStateNormal];
//    [self.topUpBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
//    [self.topUpBtn setImagePosition:LXMImagePositionLeft spacing:5];
//
//    [self.withDrawBtn setBackgroundImage:[UIImage imageNamed:@"hor_btn_sel"] forState:UIControlStateNormal];
//    [self.withDrawBtn setImage:[UIImage imageNamed:@"hor_yebao_redrwa_hightligth"] forState:UIControlStateNormal];
//    [self.withDrawBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
//    [self.withDrawBtn setImagePosition:LXMImagePositionLeft spacing:5];
    
    [self.topUpBtn setTitleColor:[UIColor skinViewKitNorColor] forState:UIControlStateNormal];
    [self.topUpBtn setBackgroundColor:[UIColor skinViewKitSelColor]];
    
    [self.withDrawBtn setTitleColor:[UIColor skinViewKitSelColor] forState:UIControlStateNormal];
    [self.withDrawBtn setBackgroundColor:[UIColor skinViewKitNorColor]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (([USER_DATA_MANAGER.userInfoData.transferStatus integerValue] == 1 || [BET_CONFIG.config.user_transfer_stauts isEqualToString:@"1"]) && USER_DATA_MANAGER.isLogin) {
        [NET_DATA_MANAGER requestGetfreeTransfer:@"self" success:^(id responseObject) {
            
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }
}

-(void)requestAllCompleted:(void(^)(BOOL isSuccess))completed{
    dispatch_group_t group = dispatch_group_create();
    __block BOOL isSuccess = YES;
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [USER_DATA_MANAGER requestUserStatusCompleted:^(BOOL success) {
            if (isSuccess) {
                isSuccess = success;
            }
            dispatch_semaphore_signal(sema);
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [self getInfoCompleted:^(BOOL success) {
            if (isSuccess) {
                isSuccess = success;
            }
            dispatch_semaphore_signal(sema);
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        completed ? completed(isSuccess) : nil;
    });
}

-(void)addObserver{
    @weakify(self);
    [RACObserve(BET_CONFIG.userSettingData, yeBaoBalanceHiden) subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        [self.balanceHidenBtn setSelected:BET_CONFIG.userSettingData.yeBaoBalanceHiden];
        if (BET_CONFIG.userSettingData.yeBaoBalanceHiden) {
            self.balanceLabel.text = @"总余额:****";
        }else{
            self.balanceLabel.text = [NSString stringWithFormat:@"总余额:%@",[self.info.money decimalNumberByRoundingDownString]];
        }
    }];
}

-(void)lineChartDataSouce{
    
    __block NSNumber *maxYaxis = [NSNumber numberWithInteger:ceil([self.info.interestBaseRate doubleValue])];
    NSMutableArray <NSNumber *>*yAxisList = @[].mutableCopy;
    [self.info.interest enumerateObjectsUsingBlock:^(YEBaoInerestInfo * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.interestRate compare:maxYaxis] == NSOrderedDescending) {
            maxYaxis = obj.interestRate;
        }
        [yAxisList safeAddObject:obj.interestRate];
    }];
    
    
    NSMutableArray <NSString *>*xAxisList = @[].mutableCopy;
    [self.info.interest enumerateObjectsUsingBlock:^(YEBaoInerestInfo * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *mmdd = [obj.date getTimeFormatStringWithFormat:@"MM/dd"];
        [xAxisList safeAddObject:mmdd];
    }];
    
    self.lineChartView.delegate = self;
    self.lineChartView.scrollEnabled = NO;//禁用 AAChartView 滚动效果
    //    设置aaChartVie 的内容高度(content height)
    //    self.lineChartView.contentHeight = chartViewHeight*2;
    //    设置aaChartVie 的内容宽度(content  width)
    //    self.lineChartView.contentWidth = chartViewWidth*2;
//    [self.view addSubview:self.lineChartView];
    self.lineChartView.backgroundColor = [UIColor skinViewScreenColor];
    
    
    //设置 AAChartView 的背景色是否为透明
    self.lineChartView.isClearBackgroundColor = YES;
    
    self.aaChartModel = AAChartModel.new
    .chartTypeSet(AAChartTypeLine)//图表类型
    .titleSet(@"")//图表主标题
    .subtitleSet(@"")//图表副标题
    .colorsThemeSet(@[@"#1790FF"])//设置主体颜色数组
    .backgroundColorSet(@"#FFFFFF")
//    .touchEventEnabledSet(true)//支持用户点击事件
    .legendEnabledSet(NO)
    .markerSymbolStyleSet(AAChartSymbolStyleTypeInnerBlank)//设置折线连接点样式为:边缘白色
    .seriesSet(@[
                 AASeriesElement.new
                 .nameSet(@"收益率")
                 .dataSet(yAxisList)
                ]
               );
    self.aaChartModel
    .xAxisCrosshairWidthSet(@01.2)//Zero width to disable crosshair by default
    .xAxisCrosshairColorSet(@"#778899")//浅石板灰准星线
    .xAxisCrosshairDashStyleTypeSet(AAChartLineDashStyleTypeShortDot)
    
    .yAxisCrosshairColorSet(@"#778899")
    .yAxisAllowDecimalsSet(NO)
    .yAxisMaxSet(maxYaxis)
    .yAxisMinSet(@0)
    .yAxisTickIntervalSet(@(ceil([maxYaxis integerValue] / 6.0)))
    .yAxisLineWidthSet(@1)//Y轴轴线线宽为0即是隐藏Y轴轴线
    .yAxisTitleSet(@"")//设置 Y 轴标题
    .yAxisGridLineWidthSet(@1);//y轴横向分割线宽度为0(即是隐藏分割线);
    self.aaChartModel.categories = xAxisList;//设置 X 轴坐标文字内容
    [self.lineChartView aa_drawChartWithChartModel:_aaChartModel];
    
    
}


#pragma mark - Events
- (IBAction)balanceHidenAciton:(UIButton *)sender {
    
    BET_CONFIG.userSettingData.yeBaoBalanceHiden = !BET_CONFIG.userSettingData.yeBaoBalanceHiden;
    [BET_CONFIG.userSettingData save];
}

- (void)headBgAction{
    YEBaoHistoryViewController *vc = [[YEBaoHistoryViewController alloc] initWithNibName:@"YEBaoHistoryViewController" bundle:nil];
    
    Bet365NavViewController *navi = [[Bet365NavViewController alloc] initWithRootViewController:vc];
    [self presentViewController:navi animated:YES completion:nil];
    vc.interestMoney = self.info.totalInterest;
    
}

- (IBAction)withDrawAction:(UIButton *)sender {
    if (![self.info.isOpen boolValue]) {
        [SVProgressHUD showErrorWithStatus:@"余额宝功能已关闭"];
        return;
    }
    YEBaoTransferViewController *vc = [[YEBaoTransferViewController alloc] initWithNibName:@"YEBaoTransferViewController" bundle:nil];
    Bet365NavViewController *navi = [[Bet365NavViewController alloc] initWithRootViewController:vc];
    [self presentViewController:navi animated:YES completion:nil];
    @weakify(self);
    vc.outBlock = ^(double money, NSString * _Nonnull psw) {
        @strongify(self);
        if (psw.length) {
            [self submitTransferOut:money Password:psw Completed:^(BOOL success) {
                if (success) {
                    [SVProgressHUD showSuccessWithStatus:@"转出成功"];
                    [self requestAllCompleted:^(BOOL isSuccess) {
                        if (isSuccess) {
                            [self lineChartDataSouce];
                        }
                    }];
                }
            }];
        }else{
            [SVProgressHUD showErrorWithStatus:@"请输入取款密码"];
        }
        
    };
    vc.info = self.info;
    
}

- (IBAction)topUpAction:(UIButton *)sender {
    YEBaoTransferViewController *vc = [[YEBaoTransferViewController alloc] initWithNibName:@"YEBaoTransferViewController" bundle:nil];
    Bet365NavViewController *navi = [[Bet365NavViewController alloc] initWithRootViewController:vc];
    [self presentViewController:navi animated:YES completion:nil];
    @weakify(self);
    vc.inBlock = ^(double money) {
        @strongify(self);
        [SVProgressHUD showWithStatus:@""];
        [self submitTransferIn:money Completed:^(BOOL success) {
            if (success) {
                [SVProgressHUD showSuccessWithStatus:@"转入成功"];
                [self requestAllCompleted:^(BOOL isSuccess) {
                    if (isSuccess) {
                        [self lineChartDataSouce];
                    }
                }];
            }
        }];
    };
    vc.info = self.info;
    
}



#pragma mark - GET/SET
-(void)setInfo:(YEBaoInfo *)info{
    if (!info) {
        return;
    }
    
    _info = info;
    dispatch_async(dispatch_get_main_queue(), ^{
        BET_CONFIG.userSettingData.yeBaoBalanceHiden = [@(BET_CONFIG.userSettingData.yeBaoBalanceHiden) boolValue];
        
        YEBaoInerestInfo *last = _info.interest.lastObject;
        self.yestodayIncomeLabel.text = ([last.interestMoney doubleValue] != 0.0) ? [last.interestMoney decimalNumberByRoundingDownString] : @"暂无收益";
        self.totalIncomeLabel.text = [_info.totalInterest decimalNumberByRoundingDownString];
        self.incomProportionLabel.text = [NSString stringWithFormat:@"%.2f",[_info.interestBaseRate doubleValue]];
        self.achievementLabel.text = [NSString stringWithFormat:@"%.2f",[_info.interestActiveRate doubleValue]];
        
        /*
         * 收益说明
         */
        NSString *baseIntruStrTitile = @"基准收益率:";
        NSString *baseIntruStr = @"余额宝的基础收益率";
        NSString *tips = @"提示:";
        
        NSString *dynamicIntruStrTitile = @"活跃绩点:";
//        NSString *dynamicIntruStr = [NSString stringWithFormat:@"若当日有投注任何游戏，余额宝收益为基准收益率(%.2f%%)，记上活跃绩点(%.2f%%)，即为%.2f%%",
//                                     [_info.interestBaseRate doubleValue],
//                                     [_info.interestActiveRate doubleValue],
//                                     [_info.interestBaseRate doubleValue] + [_info.interestActiveRate doubleValue]];
        NSString *dmlStr = [NSString stringWithFormat:@"若当日产生的活跃打码量达到%.2f元，余额宝收益为基准收益率(%.2f%%)，记上活跃绩点(%.2f%%)，即为%.2f%%",
                                    [_info.activeDml doubleValue],
                                    [_info.interestBaseRate doubleValue],
                                    [_info.interestActiveRate doubleValue],
                                    [_info.interestBaseRate doubleValue] + [_info.interestActiveRate doubleValue]];
        NSString *tipsContent = @"(余额x实际收益率)<0.01 将不会展示收益";
        
        NSString *instrustionStr = [NSString stringWithFormat:@"%@%@\n%@%@\n%@\n%@\n余额宝收益计算时间：\n1、余额宝收益派发时间为每天凌晨5:00(北京时间：五点);\n2、每单笔转入的金额收益时间为24小时(自然时间);\n3、当日时间(自然时间：00:00~23:59:59)内转入不计算统计资格时间;\n\n 例如：\n单笔转入时间周一内(自然时间：00:00~23:59:59),\n统计收益资格时间为周二的二十四时(北京时间23:59:59),\n首次派发时间则为周三凌晨5：00(北京时间：五点)",baseIntruStrTitile,baseIntruStr,dynamicIntruStrTitile,dmlStr,tips,tipsContent];
        
        NSMutableAttributedString *mInstrustionStr = [[NSMutableAttributedString alloc] initWithString:instrustionStr];
        [mInstrustionStr addColor:COLOR_WITH_HEX(0xEE2C42) substring:baseIntruStrTitile];
        [mInstrustionStr addColor:COLOR_WITH_HEX(0xEE2C42) substring:dynamicIntruStrTitile];
        [mInstrustionStr addColor:COLOR_WITH_HEX(0xEE2C42) substring:tips];
        [mInstrustionStr addFontWithName:@"AppleGothic" size:12 substring:instrustionStr];
        self.instructionsLabel.attributedText = mInstrustionStr;
        [self.view layoutIfNeeded];
    });
}

-(AAChartView *)lineChartView{
    if (!_lineChartView) {
        _lineChartView = [[AAChartView alloc] init];
        [self.contentView addSubview:_lineChartView];
        [_lineChartView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.top.equalTo(self.headView.mas_bottom).offset(10);
            make.bottom.equalTo(self.bottomView.mas_top).offset(-10);
        }];
    }
    return _lineChartView;
}


#pragma mark - NetRequest
-(void)getInfoCompleted:(void(^)(BOOL success))completed{
    [NET_DATA_MANAGER requestYEBaoInfosuccess:^(id responseObject) {
        NSError *error;
        YEBaoInfo *info = [MTLJSONAdapter modelOfClass:YEBaoInfo.class fromJSONDictionary:responseObject error:&error];
        if (info) {
            NSMutableArray <YEBaoInerestInfo *>*last7daysModelList = @[].mutableCopy;
            NSDate *yestday = [[NSDate date] offsetDays:-1];
            for (int i = 6; i>=0; i--) {
                YEBaoInerestInfo *model = [[YEBaoInerestInfo alloc] init];
                model.interestRate = info.interestBaseRate;
                model.date = [yestday offsetDays:(-i)];
                [last7daysModelList addObject:model];
            }
            if (!info.interest.count) {
                /** 无数据**/
                [info.interest addObjectsFromArray:last7daysModelList];
            }else {
                NSMutableArray <YEBaoInerestInfo *>*mDatList = info.interest.mutableCopy;
                [last7daysModelList enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(YEBaoInerestInfo * _Nonnull ltObj, NSUInteger ltIdx, BOOL * _Nonnull ltStop) {
                    __block BOOL hasContain = NO;
                    [mDatList enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(YEBaoInerestInfo * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        if ([ltObj.date isSameDay:obj.date]) {
                            hasContain = YES;
                            *stop = YES;
                        }
                    }];
                    if (!hasContain) {
                        [info.interest addObject:ltObj];
                    }
                }];
            }
            [info.interest sortUsingComparator:^NSComparisonResult(YEBaoInerestInfo *  _Nonnull obj1, YEBaoInerestInfo *  _Nonnull obj2) {
                if ([obj1.date timeIntervalSince1970] > [obj2.date timeIntervalSince1970]) {
                    return NSOrderedDescending;
                }
                return NSOrderedAscending;
            }];
            if (info.interest.count > 7) {
                [info.interest removeObjectsInRange:NSMakeRange(0, info.interest.count - 7)];
            }
        }
        self.info = info;
        completed ? completed(info && !error) : nil;
    } failure:^(NSError *error) {
        completed ? completed(NO) : nil;
    }];
}

-(void)submitTransferIn:(double)money Completed:(void(^)(BOOL success))completed{
    [NET_DATA_MANAGER requestYEBaoTransferIn:money Success:^(id responseObject) {
        completed ? completed(YES) : nil;
    } failure:^(NSError *error) {
        completed ? completed(NO) : nil;
    }];
}

-(void)submitTransferOut:(double)money Password:(NSString *)psw Completed:(void(^)(BOOL success))completed{
    [NET_DATA_MANAGER requestYEBaoTransferOut:money Password:psw Success:^(id responseObject) {
        completed ? completed(YES) : nil;
    } failure:^(NSError *error) {
        completed ? completed(NO) : nil;
    }];
}

@end
