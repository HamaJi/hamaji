//
//  Bet365VerifyImagCodeViewController.h
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/12/16.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "Bet365ViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef BOOL(^VerifyCodeSubmitBlock)(NSString *code);

typedef void(^VerifyCodeResetBlock)();

@interface Bet365VerifyImagCodeViewController : Bet365ViewController<UIViewControllerTransitioningDelegate>

@property (nonatomic,copy) VerifyCodeSubmitBlock submitBlock;

@property (nonatomic,copy) VerifyCodeResetBlock resetBlock;

-(void)updateImageCode;

@end

NS_ASSUME_NONNULL_END
