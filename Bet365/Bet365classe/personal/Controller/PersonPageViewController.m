//
//  PersonPageViewController.m
//  Bet365
//
//  Created by HHH on 2018/7/14.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "PersonPageViewController.h"
#import "PersonPageAction.h"

#import "PersonPageHeaderView.h"
#import "LukeTraceViewController.h"
#import "LukeTeamCountViewController.h" //总览
#import "Bet365RecordViewController.h" //总的投注记录
#import "LukeRecordViewController.h" //彩票投注记录
#import "LukeRechargeOrderViewController.h" //充值订单
#import "LukeDrawViewController.h" //提款 #import "LukeTraceViewController.h"  //追号记录
#import "LukeInfoViewController.h"  //个人资料
#import "LukeNoticeViewController.h" //游戏公告
#import "LukeTotalMoneyViewController.h"  //账户余额
#import "LukePushMessageViewController.h"  //最新消息
#import "LukeReportMagViewController.h" //账户明细
#import "LukeSubUsersViewController.h" //用户列表
#import "LukeRegistrManViewController.h" //注册管理
#import "LukeRebatesViewController.h"  //推广链接
#import "KFCPActivityController.h"
#import "jesseGameIntroduce.h"  //玩法说明
#import "Bet365BankCardViewController.h" //银行卡
#import "Bet365GameReportViewController.h" //
#import "Bet365DrawViewController.h"  //提款记录
#import "Bet365TransferController.h"
#import "Bet365RechargeController.h"
#import "Bet365RechargeController.h"
#import "LukeDrawMoneyViewController.h"
#import "Bet365ActivityHallController.h"
#import "NetWorkMannager+activity.h"
#import "ChatViewController.h"
#import "Bet365-Bridging-Header.h"
#define SuppressPerformSelectorLeakWarning(Stuff) \
do { \
    _Pragma("clang diagnostic push") \
    _Pragma("clang diagnostic ignored \
            "-Warc-performSelector-leaks\"") \
    Stuff; \
    _Pragma("clang diagnostic pop") \
} while (0)

@interface PersonPageViewController ()<PersonPageActionDelegate,UIViewControllerSerializing>

@property (strong, nonatomic) UITableView *tableView;

@property (strong, nonatomic) PersonPageHeaderView *headView;

@property (strong,nonatomic)PersonPageAction *action;

@end

RouterKey *const kRouterCenter = @"center";

@implementation PersonPageViewController

+ (BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters {
    return [USER_DATA_MANAGER shouldPush2Login];
}

+(void)load{
    [self registerJumpRouterKey:kRouterCenter toHandle:^(NSDictionary *parameters) {
        PersonPageViewController *vc = [[PersonPageViewController alloc] init];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }];
    
    [self registerInstanceRouterKey:kRouterCenter toHandle:^UIViewController *(NSDictionary *parameters) {
        PersonPageViewController *vc = [[PersonPageViewController alloc] init];
        return vc;
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    self.naviLoginStatus = Bet365NaviLogoItemStatus_DEFAULTS;
    self.action.delegate = self;
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.action per_viewWillAppear];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
//    [self setNavTranslucent:NO alpha:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PersonPageActionDelegate
-(void)PersonPageAction:(PersonPageAction *)action navigationBarAlpha:(CGFloat)alpha{
//    [self setNavTranslucent:NO alpha:alpha];
}
-(void)PersonPageAction:(PersonPageAction *)action TapItem:(CenterMenuItemTemplate *)item{
    RouterKey *appLink = item.appLink;
    [UIViewController routerJumpToUrl:item.appLink];
//    ((void (*)(id, SEL))[self methodForSelector:itemSel])(self, itemSel);
}

-(void)PersonPageAction:(PersonPageAction *)action headBgViewHeight:(CGFloat)scale{
//    CGFloat height = 246.0 / (750.0 / WIDTH);
    CGFloat height = 405 / (750.0 / WIDTH);
    [UIView animateWithDuration:0 animations:^{
        CGFloat haha = height - scale;
        if (haha < 0) {
            haha = 0;
        }
//        self.bgView.ycz_height = haha;
    }];
}



#pragma mark - Action
/**
 充值
 */
-(void)recharge{

    [UIViewController routerJumpToUrl:kRouterRecharge];

}

/**
 提现
 */
-(void)draw{

    [UIViewController routerJumpToUrl:kRouterDraw];

}

/**
 额度转换
 */
-(void)switchQuota{

    [UIViewController routerJumpToUrl:kRouterConver];

}

/**
 借呗
 */
-(void)lend{
    
    [UIViewController routerJumpToUrl:kRouterLoan];
    
}

/**
 充值记录
 */
-(void)rechargeNotice{
    LukeRechargeOrderViewController *vc = [[LukeRechargeOrderViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;;
}

/**
 提现记录
 */
-(void)drawNotice{
    [self.navigationController pushViewController:[[Bet365DrawViewController alloc] init] animated:YES];
}

/**
 玩法说明
 */
-(void)gameDirections{
    
    /**GameID**/
    jesseGameIntroduce *introd = [[jesseGameIntroduce alloc] init];
    [self.navigationController pushViewController:introd animated:YES];
}


/**
 个人信息
 */
-(void)accountInfo{
    if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"TEST"]) {
        [SVProgressHUD showErrorWithStatus:@"试玩帐号不能进入个人信息"];
    }else {
        [self.navigationController pushViewController:[[LukeInfoViewController alloc] init] animated:YES];
    }
}

/**
 个人总览
 */
-(void)accountOverView{
    LukeTeamCountViewController *teamVC = [[LukeTeamCountViewController alloc] init];
    teamVC.rwsType = PersonTeamRwsType_Person;
    [self.navigationController pushViewController:teamVC animated:YES];
}

/**
 银行卡
 */
-(void)bankList{
    
    if (USER_DATA_MANAGER.userInfoData.userBank && [USER_DATA_MANAGER.userInfoData.userBank aji_getAllkey].count) {
        [self.navigationController pushViewController:[[Bet365BankCardViewController alloc] init] animated:YES];
    }else{
        [self.navigationController pushViewController:[[LukeDrawMoneyViewController alloc] init] animated:YES];
    }
}

/**
 账变记录
 */
-(void)accountNotes{
    [self.navigationController pushViewController:[[LukeReportMagViewController alloc] init] animated:YES];
}

/**
 游戏公告
 */
-(void)gameNote{
    [self.navigationController pushViewController:[[LukeNoticeViewController alloc] init] animated:YES];
}

/**
 未读消息
 */
-(void)unreadMessage{
    [self.navigationController pushViewController:[[LukePushMessageViewController alloc] init] animated:YES];
}

/**
 红包
 */
-(void)redEnvelope{
    if (BET_CONFIG.common_config.isRedHref.length <= 0) {
        [UIViewController routerJumpToUrl:ROUTER_REDPAGE];
    }else{
        [UIViewController routerJumpToUrl:BET_CONFIG.common_config.isRedHref];
    }
}
/**
 代理说明
 */
-(void)personAgend{
    [UIViewController routerJumpToUrl:kRouterAgentDeclare];
    
}
/**
 彩票查询
 */
-(void)lottery{
    [self.navigationController pushViewController:[[LukeRecordViewController alloc] init] animated:YES];
}

/**
 追号记录
 */
-(void)deliver{
    [self.navigationController pushViewController:[[LukeTraceViewController alloc] init] animated:YES];
}

/**
 真人游戏
 */
-(void)zhenrenGame{
    Bet365GameReportViewController *reportVC = [[Bet365GameReportViewController alloc] init];
    reportVC.type = GameCenterType_live;
    [self.navigationController pushViewController:reportVC animated:YES];
}

/**
 电竞游戏
 */
-(void)eSportGame{
    Bet365GameReportViewController *reportVC = [[Bet365GameReportViewController alloc] init];
    reportVC.type = GameCenterType_ele;
    [self.navigationController pushViewController:reportVC animated:YES];
}

/**
 棋牌游戏
 */
-(void)cheeseGame{
    Bet365GameReportViewController *reportVC = [[Bet365GameReportViewController alloc] init];
    reportVC.type = GameCenterType_chess;
    [self.navigationController pushViewController:reportVC animated:YES];
}


/**
 体育游戏
 */
- (void)sportGame
{
    Bet365GameReportViewController *reportVC = [[Bet365GameReportViewController alloc] init];
    reportVC.type = GameCenterType_sports;
    [self.navigationController pushViewController:reportVC animated:YES];
}

/**
 用户列表
 */
-(void)deleUserlist{
    [self.navigationController pushViewController:[[LukeSubUsersViewController alloc] init] animated:YES];
}

/**
 注册管理
 */
-(void)deleRegisterMember{
    [self.navigationController pushViewController:[[LukeRegistrManViewController alloc] init] animated:YES];
}

/**
 推广链接
 */
-(void)deleLink{
    [self.navigationController pushViewController:[[LukeRebatesViewController alloc] init] animated:YES];
}

/**
 团队总览
 */
-(void)deleTeamOverView{
    LukeTeamCountViewController *teamVC = [[LukeTeamCountViewController alloc] init];
    teamVC.rwsType = PersonTeamRwsType_Team;
    [self.navigationController pushViewController:teamVC animated:YES];
}

/**
 活动大厅
 */
- (void)activityHall{
    Bet365ActivityHallController *vc = [[Bet365ActivityHallController alloc] init];
    vc.type = ActivityType_Prmt;
    [self.navigationController pushViewController:vc animated:YES];
}

/**
 我的活动
 */
- (void)myActivity{
    Bet365ActivityHallController *vc = [[Bet365ActivityHallController alloc] init];
    vc.type = ActivityType_My_Activity;
    [self.navigationController pushViewController:vc animated:YES];
}

/**
 我的抽奖
 */
- (void)myLottery{
    Bet365ActivityHallController *vc = [[Bet365ActivityHallController alloc] init];
    vc.type = ActivityType_My_Lottery;
    [self.navigationController pushViewController:vc animated:YES];
}

/**
 抽奖结果
 */
- (void)myRedenvelope{
    Bet365ActivityHallController *vc = [[Bet365ActivityHallController alloc] init];
    vc.type = ActivityType_My_redenvelope;
    [self.navigationController pushViewController:vc animated:YES];
}

/**
 聊天室
 */
- (void)pushChat{
    [UIViewController routerJumpToUrl:kRouterChatRoom];

}


#pragma mark - GET/SET
-(PersonPageAction *)action{
    if (!_action) {
        _action = [[PersonPageAction alloc] init];
        _action.tableView = self.tableView;
        _action.headView = self.headView;
    }
    return _action;
}
//-(NSMutableArray<OldPersonPageSection *> *)group{
//    if (!_group) {
//        _group = [[NSMutableArray alloc] initWithArray:@[[OldPersonPageSection creatItemTitile:@"常用工具"
//                                                                                   hexColor:0xF4BB1B
//                                                                                      items:@[
//                                                                                              [OldPersonPageItem creatItemTitile:@"额度转换" ImgName:@"person_transfer" Sel:@selector(switchQuota)],
//                                                                                              [OldPersonPageItem creatItemTitile:@"充值记录" ImgName:@"person_recharge_record" Sel:@selector(rechargeNotice)],
//                                                                                              [OldPersonPageItem creatItemTitile:@"提现记录" ImgName:@"person_withdraw-order" Sel:@selector(drawNotice)],
//                                                                                              [OldPersonPageItem creatItemTitile:@"玩法说明" ImgName:@"person_rule" Sel:@selector(gameDirections)],
//                                                                                              ]
//                                                          ],
//                                                         [OldPersonPageSection creatItemTitile:@"个人信息"
//                                                                                   hexColor:0x62BAE6
//                                                                                      items:@[
//                                                                                              
//                                                                                              [OldPersonPageItem creatItemTitile:@"个人资料" ImgName:@"person_user_info" Sel:@selector(accountInfo)],
//                                                                                              [OldPersonPageItem creatItemTitile:@"个人总览" ImgName:@"person_summary" Sel:@selector(accountOverView)],
//                                                                                              [OldPersonPageItem creatItemTitile:@"银行卡" ImgName:@"person_bank" Sel:@selector(bankList)],
//                                                                                              [OldPersonPageItem creatItemTitile:@"账变记录" ImgName:@"person_account_bill" Sel:@selector(accountNotes)],
//                                                                                              [OldPersonPageItem creatItemTitile:@"公告信息" ImgName:@"person_game_notice" Sel:@selector(gameNote)],
//                                                                                              [OldPersonPageItem creatItemTitile:@"未读信息" ImgName:@"person_message" Sel:@selector(unreadMessage)],
//                                                                                               [OldPersonPageItem creatItemTitile:@"红包" ImgName:@"redBag" Sel:@selector(redEnvelope)]
//                                                                                              ]
//                                                          ],
//                                                         [OldPersonPageSection creatItemTitile:@"投注记录"
//                                                                                   hexColor:0xEF473A
//                                                                                      items:@[
//                                                                                              [OldPersonPageItem creatItemTitile:@"彩票查询" ImgName:@"person_cp" Sel:@selector(lottery)],
//                                                                                              [OldPersonPageItem creatItemTitile:@"追号查询" ImgName:@"person_chase" Sel:@selector(deliver)],
//                                                                                              [OldPersonPageItem creatItemTitile:@"真人游戏" ImgName:@"person_live" Sel:@selector(zhenrenGame)],
//                                                                                              [OldPersonPageItem creatItemTitile:@"体育游戏" ImgName:@"person_sport" Sel:@selector(sportGame)],
//                                                                                              [OldPersonPageItem creatItemTitile:@"电子游戏" ImgName:@"person_electronic" Sel:@selector(eSportGame)],
//                                                                                              [OldPersonPageItem creatItemTitile:@"棋牌游戏" ImgName:@"person_chess" Sel:@selector(cheeseGame)]
//                                                                                              
//                                                                                              
//                                                                                              ]
//                                                          ],
//                                                         [OldPersonPageSection creatItemTitile:@"代理中心"
//                                                                                   hexColor:0xEF473A
//                                                                                      items:@[
//                                                              [OldPersonPageItem creatItemTitile:@"代理说明" ImgName:@"person_agend" Sel:@selector(personAgend)],                                [OldPersonPageItem creatItemTitile:@"用户列表" ImgName:@"person_agent_user_list" Sel:@selector(deleUserlist)],
//                                                                                              [OldPersonPageItem creatItemTitile:@"注册管理" ImgName:@"person_agent_add_user" Sel:@selector(deleRegisterMember)],
//                                                                                              [OldPersonPageItem creatItemTitile:@"推广链接" ImgName:@"person_agent_spread" Sel:@selector(deleLink)],
//                                                                                              [OldPersonPageItem creatItemTitile:@"团队总览" ImgName:@"person_agent_team_summary" Sel:@selector(deleTeamOverView)]]
//                                                          ],
//                                                         [OldPersonPageSection creatItemTitile:@"优惠活动"
//                                                                                   hexColor:0xEF473A
//                                                                                      items:@[
//                                                                                              [OldPersonPageItem creatItemTitile:@"活动大厅" ImgName:@"person_activity_hall" Sel:@selector(activityHall)],
//                                                                                              [OldPersonPageItem creatItemTitile:@"我的活动" ImgName:@"person_my_activity" Sel:@selector(myActivity)],
//                                                                                              [OldPersonPageItem creatItemTitile:@"我的抽奖" ImgName:@"person_my_lottery" Sel:@selector(myLottery)],
//                                                                                              [OldPersonPageItem creatItemTitile:@"抽奖结果" ImgName:@"person_my_redenvelope" Sel:@selector(myRedenvelope)]]
//                                                          ]]
//                  ];
//        NSInteger idx = _group[PersonPageTableViewSection_INFODATA].items.count;
//        _group[PersonPageTableViewSection_INFODATA].items[idx - 2].isBuge = YES;
//    }
//    
//    return _group;
//}

-(UITableView *)tableView{
    if (!_tableView) {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
        [self.view addSubview:imageView];
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self.view);
            make.width.equalTo(imageView.mas_height).multipliedBy(750.0 / 246.0);
        }];
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.bottom.equalTo(self.view);
        }];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator  = NO;
        _tableView.backgroundColor = [UIColor clearColor];
//#ifdef __IPHONE_11_0
//        if ([_tableView respondsToSelector:@selector(setContentInsetAdjustmentBehavior:)]) {
//            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
//        }
//#endif
        
    }
    return _tableView;
}

-(PersonPageHeaderView *)headView{
    if (!_headView) {
        _headView = [PersonPageHeaderView instantiateFromNib];
    }
    return _headView;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
