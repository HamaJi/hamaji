//
//  LoginByPhoneViewController.m
//  Bet365
//
//  Created by super_小鸡 on 2019/8/10.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "LoginByPhoneViewController.h"
#import "TimerManager.h"
#import "LukeRegisterViewController.h"

@interface LoginByPhoneViewController ()
<TimerManagerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;

@property (weak, nonatomic) IBOutlet UITextField *vCodeTextField;

@property (weak, nonatomic) IBOutlet UIButton *sendCodeBtn;

@property (weak, nonatomic) IBOutlet UIButton *accountBtn;

@property (weak, nonatomic) IBOutlet UIButton *registerBtn;

@property (weak, nonatomic) IBOutlet UIButton *loginBtn;

@property (nonatomic, strong) NSNumber *sendingSpacing;

@end

@implementation LoginByPhoneViewController

NSString *const kCodeTimerKey = @"LoginByPhoneViewControllerKey";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    
    self.phoneTextField.textColor = [UIColor skinTextItemNorColor];
    
    self.vCodeTextField.textColor = [UIColor skinTextItemNorColor];
    
    [self.sendCodeBtn setTitleColor:[UIColor skinTextItemNorColor] forState:UIControlStateNormal];
    [self.sendCodeBtn setTitleColor:[UIColor skinTextItemNorSubColor] forState:UIControlStateDisabled];
    [self.sendCodeBtn setBackgroundImage:[UIImage imageWithColor:[UIColor skinViewKitNorColor]] forState:UIControlStateNormal];
    [self.sendCodeBtn setBackgroundImage:[UIImage imageWithColor:[UIColor skinViewKitDisColor]] forState:UIControlStateDisabled];
    
    [self.loginBtn setTitleColor:[UIColor skinViewKitNorColor] forState:UIControlStateNormal];
    [self.loginBtn setTitleColor:[UIColor skinViewKitDisColor] forState:UIControlStateDisabled];
    [self.loginBtn setBackgroundImage:[UIImage imageWithColor:[UIColor skinViewKitNorColor]] forState:UIControlStateNormal];
    [self.loginBtn setBackgroundImage:[UIImage imageWithColor:[UIColor skinViewKitDisColor]] forState:UIControlStateDisabled];
    
    [self.accountBtn setTitleColor:[UIColor skinTextItemNorColor] forState:UIControlStateNormal];
    
    [self.registerBtn setTitleColor:[UIColor skinTextItemNorColor] forState:UIControlStateNormal];
    
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void)dealloc{
    [TIMER_MANAGER removeTimerWithKey:kCodeTimerKey];
    [TIMER_MANAGER removeDelegate:self];
}

-(void)addObeser{
    [TIMER_MANAGER addDelegate:self];
    @weakify(self);
    [[RACSignal combineLatest:@[self.phoneTextField.rac_textSignal,
                                [RACObserve(self, sendingSpacing) distinctUntilChanged]] reduce:^id (NSString *text,NSNumber *sendingSpacing){
        return @([text isMobileNumber] && [sendingSpacing integerValue] == 0);
    }] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        if ([self.sendingSpacing integerValue]) {
            [self.sendCodeBtn setTitle:[NSString stringWithFormat:@"%lis后重新获取",[self.sendingSpacing integerValue]] forState:UIControlStateDisabled];
        }else{
            [self.sendCodeBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
        }
        [self.sendCodeBtn setEnabled:[x boolValue]];
    }];
}

#pragma mark - TimerManagerDelegate
-(void)timerWithKey:(NSString *)key andMaxCount:(NSInteger)maxCount andRestCount:(NSInteger)count{
    self.sendingSpacing = [NSNumber numberWithInteger:count];
}

#pragma mark - Events
- (IBAction)sendVCodeAction:(UIButton *)sender {
    [TIMER_MANAGER addTimerWithMaxCount:60 andKey:kCodeTimerKey andReduceScope:1];
}

- (IBAction)accountAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)registerAction:(id)sender {
    LukeRegisterViewController *registerC = [[LukeRegisterViewController alloc]init];
    registerC.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController pushViewController:registerC animated:YES];
}

- (IBAction)loginAction:(id)sender {
    
}

-(NSNumber *)sendingSpacing{
    if (!_sendingSpacing) {
        _sendingSpacing = [NSNumber numberWithInteger:[TIMER_MANAGER currentCountWithKey:kCodeTimerKey]];
    }
    return _sendingSpacing;
}

@end
