//
//  YHApplicationPopViewController.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/28.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "YHApplicationPopViewController.h"
#import "NetWorkMannager+Prmt.h"
#import "PresentingTopAnimator.h"
#import "DismissingTopAnimator.h"

@interface YHApplicationPopViewController ()

@property (strong, nonatomic) UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *applyBtn;
@property (weak, nonatomic) IBOutlet UILabel *titileLabel;
@property (weak, nonatomic) IBOutlet UIView *titileView;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintHeight;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation YHApplicationPopViewController

- (IBAction)closeAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titileLabel.text = @"申请优惠";
    
    UIImageView *bg = [[UIImageView alloc] initWithImage:[UIImage skinGradientHorNaviImage]];
    [self.titileView insertSubview:bg atIndex:0];
    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(bg.superview);
    }];
    self.titileLabel.textColor = [UIColor skinTextTitileColor];
    UIImage *img = [UIImage imageNamed:@ "chat_close_icon" ];
    img = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.closeBtn setImage:img forState:UIControlStateNormal];
    [self.closeBtn setTintColor:[UIColor skinNaviTintColor]];
    [self.applyBtn setTitleColor:[UIColor skinViewKitNorColor] forState:UIControlStateNormal];
    [self.applyBtn setBackgroundColor:[UIColor skinViewKitSelColor]];
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)applyAction:(id)sender {
    [SVProgressHUD showWithStatus:@"申请中..."];
    [NET_DATA_MANAGER requestPrmtApply:self.model.activeId Success:^(id responseObject) {
        if ([[responseObject objectForKey:@"succeed"] boolValue]) {
            [self dismissViewControllerAnimated:YES completion:^{
                if ([[responseObject objectForKey:@"approved"] boolValue]) {
                    [SVProgressHUD showInfoWithStatus:[NSString stringWithFormat:@"申请成功，恭喜您本次获得%.2f元彩金，请在活动彩金记录中查看详情！",[[responseObject objectForKey:@"amount"] floatValue]]];
                }else{
                    [SVProgressHUD showInfoWithStatus:@"申请成功，工作人员审核通过后将派发您的彩金，您可以在活动彩金记录中跟踪进度，请耐心等待！"];
                }
            }];
        }else{
            [SVProgressHUD showErrorWithStatus:[responseObject objectForKey:@"message"]];
        }
        
    } failure:^(NSError *error) {
        
    }];
}



-(void)setModel:(PrmtYHApplication *)model{
    _model = model;
    @weakify(self);
    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:model.details] options:0 progress:nil
                                                        completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                                                            if (finished && !error && image) {
                                                                @strongify(self);
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    CGFloat height = self.scrollView.ycz_width / (image.size.width / image.size.height) * 1.0;
                                                                    self.imageView.image = image;
                                                                    [self.scrollView addSubview:self.imageView];
                                                                    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                                                                        make.left.top.width.equalTo(self.scrollView);
                                                                        make.height.mas_equalTo(height);
                                                                    }];
                                                                    
                                                                    self.layoutConstraintHeight.constant = (self.view.ycz_height < height + 120.0) ? self.view.ycz_height - 120.0 : height;
                                                                    self.scrollView.contentSize = CGSizeMake(0, height);
                                                                    
                                                                });
                                                            }
                                                        }];
    
}

-(UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _imageView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
