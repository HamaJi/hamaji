//
//  LukeLiveRecordViewController.h
//  Bet365
//
//  Created by luke on 2017/11/12.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LukeLiveRecordViewController : Bet365ViewController
/** 筛选赛事 */
@property (nonatomic,copy) NSString *code;
/** 起始时间 */
@property (nonatomic,copy) NSString *beginDateStr;
/** 结束时间 */
@property (nonatomic,copy) NSString *endDateStr;

@end
