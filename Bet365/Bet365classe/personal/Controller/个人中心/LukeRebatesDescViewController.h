//
//  LukeRebatesDescViewController.h
//  Bet365
//
//  Created by luke on 17/6/30.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LukeSpreadItem;

@protocol RebatesDesControllerDelegate <NSObject>

- (void)updateSpreadSuccess;

@end

@interface LukeRebatesDescViewController : Bet365ViewController

/** 模型 */
@property (nonatomic,strong) LukeSpreadItem *item;

/** 代理 */
@property (nonatomic,weak) id<RebatesDesControllerDelegate>delegate;

@end
