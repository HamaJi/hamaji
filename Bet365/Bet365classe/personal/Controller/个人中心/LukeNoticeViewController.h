//
//  LukeNoticeViewController.h
//  Bet365
//
//  Created by luke on 17/6/15.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, NoticeType) {
    /** 过滤公告*/
    NoticeType_Filter,
    /** 所有公告*/
    NoticeType_All
};

@interface LukeNoticeViewController : Bet365ViewController
/**  */
@property (nonatomic,assign) NoticeType noticeType;
@end
