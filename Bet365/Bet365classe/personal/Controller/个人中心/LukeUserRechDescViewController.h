//
//  LukeUserRechDescViewController.h
//  Bet365
//
//  Created by luke on 17/7/11.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LukeUserRechItem;
@class LukeUserWithdrawItem;

@interface LukeUserRechDescViewController : Bet365ViewController
/**  */
@property (nonatomic,strong) LukeUserRechItem *userRechItem;

/**  */
@property (nonatomic,strong) LukeUserWithdrawItem *userWithdrawItem;

@end
