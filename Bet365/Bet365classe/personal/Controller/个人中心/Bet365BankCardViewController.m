//
//  Bet365BankCardViewController.m
//  Bet365
//
//  Created by luke on 2018/8/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Bet365BankCardViewController.h"
#import "Bet365ReleaseBankController.h"
#import "PresentingBottomAnimator.h"
#import "DismissingBottomAnimator.h"
#import "LukeDrawMoneyViewController.h"

@interface Bet365BankCardViewController ()
<UIViewControllerTransitioningDelegate,
BankCardDelegate,
ReleaseBankControllerDelegate,
UIViewControllerSerializing>
@property (weak, nonatomic) IBOutlet UILabel *titileLabel;
@property (weak, nonatomic) IBOutlet UILabel *head0Label;
@property (weak, nonatomic) IBOutlet UILabel *head1Label;
@property (weak, nonatomic) IBOutlet UILabel *head2Label;
@property (weak, nonatomic) IBOutlet UILabel *head3Label;
@property (weak, nonatomic) IBOutlet UILabel *head4Label;


@property (weak, nonatomic) IBOutlet UIButton *bankBtn;
@property (weak, nonatomic) IBOutlet UIButton *bankCardBtn;
@property (weak, nonatomic) IBOutlet UIButton *bankAddressBtn;
@property (weak, nonatomic) IBOutlet UIButton *bankTime;
@property (weak, nonatomic) IBOutlet UIView *bankView;

@property (weak, nonatomic) IBOutlet UIButton *cancleBtn;

@property (weak, nonatomic) IBOutlet UIButton *editBtn;

@end

@implementation Bet365BankCardViewController

RouterKey *const kRouterUserBank = @"userBank";

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    return [USER_DATA_MANAGER shouldPush2Login];
}

+(void)load{
    [self registerJumpRouterKey:kRouterUserBank toHandle:^(NSDictionary *parameters) {
        if (USER_DATA_MANAGER.userInfoData.userBank && [USER_DATA_MANAGER.userInfoData.userBank aji_getAllkey].count) {
            Bet365BankCardViewController *vc = [[Bet365BankCardViewController alloc] initWithNibName:@"Bet365BankCardViewController" bundle:nil];
            vc.hidesBottomBarWhenPushed = YES;
            [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc
                                                                        animated:YES];
            vc.hidesBottomBarWhenPushed = YES;
        }else{
            LukeDrawMoneyViewController *vc = [[LukeDrawMoneyViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
            vc.hidesBottomBarWhenPushed = YES;
        }
    }];
    
//    [self registerInstanceRouterKey:kRouterUserBank toHandle:^UIViewController *(NSDictionary *parameters) {
//         Bet365BankCardViewController *vc = [[Bet365BankCardViewController alloc] initWithNibName:@"Bet365BankCardViewController" bundle:nil];
//        return vc;
//    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    self.titileLabel.textColor = [UIColor skinViewKitSelColor];
    self.head0Label.textColor = [UIColor skinTextItemNorSubColor];
    self.head1Label.textColor = [UIColor skinTextItemNorSubColor];
    self.head2Label.textColor = [UIColor skinTextItemNorSubColor];
    self.head3Label.textColor = [UIColor skinTextItemNorSubColor];
    self.head4Label.textColor = [UIColor skinTextItemNorSubColor];
    
    [self.bankBtn setTitleColor:[UIColor skinTextItemNorColor] forState:UIControlStateNormal];
    [self.bankBtn setBackgroundColor:[UIColor skinViewBgColor]];
    
    [self.bankCardBtn setTitleColor:[UIColor skinTextItemNorColor] forState:UIControlStateNormal];
    [self.bankCardBtn setBackgroundColor:[UIColor skinViewBgColor]];
    
    [self.bankAddressBtn setTitleColor:[UIColor skinTextItemNorColor] forState:UIControlStateNormal];
    [self.bankAddressBtn setBackgroundColor:[UIColor skinViewBgColor]];
    
    [self.bankTime setTitleColor:[UIColor skinTextItemNorColor] forState:UIControlStateNormal];
    [self.bankTime setBackgroundColor:[UIColor skinViewBgColor]];
    
    [self.cancleBtn setTitleColor:[UIColor skinViewKitNorColor] forState:UIControlStateNormal];
    [self.cancleBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];
    
    [self.editBtn setTitleColor:[UIColor skinViewKitSelColor] forState:UIControlStateNormal];
    [self.editBtn setBackgroundColor:[UIColor skinViewKitNorColor]];
    
    [self updateData];
    self.bankView.backgroundColor = [UIColor skinViewScreenColor];
    if (BET_CONFIG.config.is_user_bank_modifiable && [BET_CONFIG.config.is_user_bank_modifiable isEqualToString:@"1"]) {
        self.bankView.hidden = NO;
    }else{
        self.bankView.hidden = YES;
    }
    
    
}

- (void)updateData
{
    [self.bankBtn setTitle:USER_DATA_MANAGER.userInfoData.userBank.bankName forState:UIControlStateNormal];
    [self.bankCardBtn setTitle:USER_DATA_MANAGER.userInfoData.userBank.cardNo forState:UIControlStateNormal];
    [self.bankAddressBtn setTitle:USER_DATA_MANAGER.userInfoData.userBank.subAddress forState:UIControlStateNormal];
    [self.bankTime setTitle:USER_DATA_MANAGER.userInfoData.userBank.updateTime forState:UIControlStateNormal];
}

- (IBAction)releaseBindBankCard:(id)sender {
    Bet365ReleaseBankController *VC = [[Bet365ReleaseBankController alloc] init];
    VC.modalPresentationStyle = UIModalPresentationCustom;
    VC.transitioningDelegate = self;
    VC.delegate = self;
    [self presentViewController:VC animated:YES completion:nil];
}

- (IBAction)modifyBankCard:(id)sender {
    LukeDrawMoneyViewController *vc = [[LukeDrawMoneyViewController alloc] init];
    vc.type = DrawBankCardType_Modify;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
    vc.hidesBottomBarWhenPushed = YES;
}

#pragma mark - BankCardDelegate
- (void)bankCardModify
{
    [self updateData];
}

#pragma mark - ReleaseBankControllerDelegate
- (void)releaseBankPop
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIViewControllerTransitioningDelegate
-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    return [DismissingBottomAnimator new];
}
-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    return [PresentingBottomAnimator new];
}

@end
