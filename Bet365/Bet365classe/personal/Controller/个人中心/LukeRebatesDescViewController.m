//
//  LukePromotionView.m
//  Bet365
//
//  Created by luke on 17/6/28.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeRebatesDescViewController.h"
#import "LukeRegisterLbTFView.h"
#import "LukeSpreadInfoItem.h"
#import "LukeSpreadItem.h"
#import "LukeUserAdapter.h"
#import "SportsModuleShare.h"
#import "NetWorkMannager+Account.h"

@interface LukeRebatesDescViewController ()
/** 奖金组 */
@property (nonatomic,weak) LukeRegisterLbTFView *moneyView;
/** 推广页面 */
@property (nonatomic,weak) LukeRegisterLbTFView *PromotionView;
/** 会员类型 */
@property (nonatomic,weak) LukeRegisterLbTFView *typeView;
/** 有效期限 */
@property (nonatomic,weak) LukeRegisterLbTFView *timeView;
/**  */
@property (nonatomic,strong) NSArray *temp;

@end

@implementation LukeRebatesDescViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.temp = [[LukeSpreadInfoItem mj_keyValuesArrayWithObjectArray:SportsShareInstance.spreadInfoArrs] getNewArrayWithKey:@"name"];
    [self setupUI];
}

- (void)setupUI
{
    self.view.backgroundColor = JesseColor(239, 239, 244);
    UIView *topView = [[UIView alloc] init];
    topView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(0);
    }];
    
    UILabel *titleLabel = [UILabel addLabelWithFont:18 color:[UIColor blackColor] title:@"修改推广链接"];
    [topView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(statusHeight + 10);
        make.centerX.equalTo(self.view);
    }];
    
    UILabel *basicLabel = [UILabel addLabelWithFont:15 color:JesseGrayColor(137) title:@"基本信息"];
    [self.view addSubview:basicLabel];
    [basicLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topView.mas_bottom).offset(10);
        make.leading.equalTo(self.view).offset(10);
    }];
    
    LukeRegisterLbTFView *PromotionView = [LukeRegisterLbTFView registerCreateBtnViewWithTitle:@"推广页面：" BtnImage:@"right_lage" target:self action:@selector(PromotionClick)];
    [PromotionView.MoneyBtn setTitle:self.item.spreadTypeName forState:UIControlStateNormal];
    [self.view addSubview:PromotionView];
    [PromotionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(basicLabel.mas_bottom).offset(5);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    self.PromotionView = PromotionView;
    
    LukeRegisterLbTFView *typeView = [LukeRegisterLbTFView registerCreateBtnViewWithTitle:@"会员类型：" BtnImage:@"right_lage" target:self action:@selector(typeClick)];
    [typeView.MoneyBtn setTitle:self.item.userType == 0 ? @"会员" : @"代理" forState:UIControlStateNormal];
    [self.view addSubview:typeView];
    [typeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(PromotionView.mas_bottom);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    self.typeView = typeView;

    if ([BET_CONFIG.config.site_rebate_model isEqualToString:@"0"] || !BET_CONFIG.config.site_rebate_model) {
        
        UILabel *moneyLabel = [UILabel addLabelWithFont:15 color:JesseGrayColor(137) title:@"奖金组"];
        [self.view addSubview:moneyLabel];
        [moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(typeView.mas_bottom).offset(10);
            make.leading.equalTo(self.view).offset(10);
        }];
        
        LukeRegisterLbTFView *moneyView = [LukeRegisterLbTFView registerCreateBtnViewWithTitle:@"选择奖金组：" BtnImage:@"right_lage" target:self action:@selector(moneyClick)];
        [moneyView.MoneyBtn setTitle:[self.item.rebate stringValue] forState:UIControlStateNormal];
        [self.view addSubview:moneyView];
        [moneyView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(moneyLabel.mas_bottom).offset(5);
            make.leading.trailing.equalTo(self.view);
            make.height.mas_equalTo(40);
        }];
        self.moneyView = moneyView;
    }
    
    CGFloat btnW = (WIDTH - 6) / 2;
    UIButton *linkBtn = [UIButton addButtonWithTitle:@"取消" font:15 color:[UIColor whiteColor] target:self action:@selector(linkClick)];
    linkBtn.backgroundColor = JesseColor(0, 122, 255);
    [self.view addSubview:linkBtn];
    [linkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-SafeAreaBottomHeight);
        make.leading.equalTo(self.view).offset(1);
        make.size.mas_equalTo(CGSizeMake(btnW, 35));
    }];
    
    UIButton *wechatlinkBtn = [UIButton addButtonWithTitle:@"保存" font:15 color:[UIColor whiteColor] target:self action:@selector(wechatlinkClick)];
    wechatlinkBtn.backgroundColor = JesseColor(221, 82, 77);
    [self.view addSubview:wechatlinkBtn];
    [wechatlinkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-SafeAreaBottomHeight);
        make.trailing.equalTo(self.view).offset(-1);
        make.size.mas_equalTo(CGSizeMake(btnW, 35));
    }];
}

//保存
- (void)wechatlinkClick
{
    self.item.spreadTypeName = self.PromotionView.MoneyBtn.currentTitle;
    self.item.userType = [self.typeView.MoneyBtn.currentTitle isEqualToString:@"会员"] ? 0 : 1;
    self.item.rebate = [NSNumber numberWithDouble:[self.moneyView.MoneyBtn.currentTitle doubleValue]];
    for (LukeSpreadInfoItem *item in SportsShareInstance.spreadInfoArrs) {
        if ([self.PromotionView.MoneyBtn.currentTitle isEqualToString:item.name]) {
            self.item.spreadType = item.SpreadId;
            break;
        }
    }
    [SVProgressHUD showWithStatus:@"修改中..."];
    [NET_DATA_MANAGER requestPostUpdateByAgentIdUrlWithModel:self.item url:[NSString stringWithFormat:@"%@/%ld",SerVer_Url,self.item.SpreaID] Success:^(id responseObject) {
        if ([self.delegate respondsToSelector:@selector(updateSpreadSuccess)]) {
            [self.delegate updateSpreadSuccess];
        }
        [self linkClick];
        [SVProgressHUD dismiss];
        [SVProgressHUD showSuccessWithStatus:@"修改成功"];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [SVProgressHUD dismiss];
    }];
}

//取消
- (void)linkClick
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)PromotionClick
{
    [BRStringPickerView showStringPickerWithTitle:nil dataSource:self.temp defaultSelValue:nil isAutoSelect:NO themeColor:[UIColor skinNaviBgColor] resultBlock:^(id selectValue) {
        [self.PromotionView.MoneyBtn setTitle:selectValue forState:UIControlStateNormal];
    }];
}

- (void)typeClick
{
    if ([BET_CONFIG.config.dl_can_create_dl isEqualToString:@"1"] || !BET_CONFIG.config.dl_can_create_dl) {
        [BRStringPickerView showStringPickerWithTitle:nil dataSource:@[@"代理",@"会员"] defaultSelValue:nil isAutoSelect:NO themeColor:[UIColor skinNaviBgColor] resultBlock:^(id selectValue) {
            [self.typeView.MoneyBtn setTitle:selectValue forState:UIControlStateNormal];
        }];
    }
}

- (void)moneyClick
{
    [BRStringPickerView showStringPickerWithTitle:nil dataSource:SportsShareInstance.rebateArrs defaultSelValue:nil isAutoSelect:NO themeColor:[UIColor skinNaviBgColor] resultBlock:^(id selectValue) {
        [self.moneyView.MoneyBtn setTitle:selectValue forState:UIControlStateNormal];
    }];
}

@end
