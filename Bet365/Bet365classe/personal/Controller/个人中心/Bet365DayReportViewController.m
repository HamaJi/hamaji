//
//  Bet365DayReportViewController.m
//  Bet365
//
//  Created by luke on 2018/8/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Bet365DayReportViewController.h"
#import "Bet365RecordTopTitleView.h"
#import "Bet365RecordViewCell.h"
#import "NetWorkMannager+Account.h"
#import "Bet365CpHistoryModel.h"
#import "Bet365CpHistoryViewController.h"

@interface Bet365DayReportViewController ()<
UITableViewDelegate,
UITableViewDataSource>
/**  */
@property (nonatomic,strong) Bet365RecordTopTitleView *topTilteView;
/**  */
@property (nonatomic,strong) UITableView *tableView;
/**  */
@property (nonatomic,strong) NSArray *dayReportArrs;

@end

@implementation Bet365DayReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    [self queryData];
}

- (void)queryData
{
    [SVProgressHUD show];
    [NET_DATA_MANAGER requestGetCpHistorySuccess:^(id responseObject) {
        self.dayReportArrs = [Bet365CpHistoryModel mj_objectArrayWithKeyValuesArray:responseObject];
        [self.tableView reloadData];
        self.tableView.isShowEmpty = YES;
        [SVProgressHUD dismiss];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [SVProgressHUD dismiss];
        self.tableView.isShowEmpty = YES;
    }];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dayReportArrs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Bet365RecordViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Bet365RecordViewCell"];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"Bet365RecordViewCell" owner:self options:nil].firstObject;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.cpModel = self.dayReportArrs[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Bet365CpHistoryModel *model = self.dayReportArrs[indexPath.row];
    Bet365CpHistoryViewController *cpVC = [[Bet365CpHistoryViewController alloc] init];
    cpVC.date = model.statTime;
    [self.navigationController pushViewController:cpVC animated:YES];
    cpVC.hidesBottomBarWhenPushed = YES;
}

#pragma mark - 懒加载
- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        if ([_tableView respondsToSelector:@selector(layoutMargins)]) {
            _tableView.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        if ([_tableView respondsToSelector:@selector(separatorInset)]) {
            _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        _tableView.backgroundColor = [UIColor skinViewScreenColor];
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.bottom.trailing.equalTo(self.view);
            make.top.equalTo(self.topTilteView.mas_bottom);
        }];
        _tableView.titleForEmpty = @"没有数据";
    }
    return _tableView;
}
- (Bet365RecordTopTitleView *)topTilteView
{
    if (_topTilteView == nil) {
        _topTilteView = [[Bet365RecordTopTitleView alloc] initWithTitles:@[@"日期",@"下注金额",@"输赢金额",@"有效投注"] titleColor:[UIColor blackColor] backgroundColor:JesseColor(239, 239, 244)];
        [self.view addSubview:_topTilteView];
        [_topTilteView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(self.view);
            make.height.mas_equalTo(40);
            make.top.equalTo(self.view);
        }];
    }
    return _topTilteView;
}

@end
