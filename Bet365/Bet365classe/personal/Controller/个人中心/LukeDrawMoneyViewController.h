//
//  LukeDrawMoneyViewController.h
//  Bet365
//
//  Created by luke on 17/6/15.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetWorkMannager+Account.h"

@protocol BankCardDelegate <NSObject>
@optional
- (void)bankCardModify;

@end

@interface LukeDrawMoneyViewController : Bet365ViewController
/**  */
@property (nonatomic,assign) DrawBankCardType type;
/**  */
@property (nonatomic,weak) id<BankCardDelegate>delegate;

@end
