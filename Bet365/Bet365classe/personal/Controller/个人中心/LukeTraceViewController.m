//
//  LukeTraceViewController.m
//  Bet365
//
//  Created by luke on 17/6/15.
//  Copyright © 2017年 jesse. All rights reserved.
//追号记录

#import "LukeTraceViewController.h"
#import "LukeScreenView.h"
#import "LukeTraceItem.h"
#import "LukeTraceCell.h"
#import "LukeTraceDescViewController.h"
#import "LukeSearchTopView.h"
#import "NetWorkMannager+Account.h"
#import "UIViewControllerSerializing.h"
#import "HMSegmentedControl+Bet365.h"

static NSString * const LukeTraceCellID = @"LukeTraceCell";
@interface LukeTraceViewController ()
<UITableViewDelegate,
UITableViewDataSource,
LukeScreenViewDelegate,
LukeSearchTopViewDelegate,
UIViewControllerSerializing>
/** 头部view */
@property (nonatomic,strong) LukeSearchTopView *searchTopView;
/** 筛选view */
@property (nonatomic,strong) LukeScreenView *screenView;
/** tableview */
@property (nonatomic,strong) UITableView *tableView;
/**  */
@property (nonatomic,weak) HMSegmentedControl *segmentControl;
/** 开始时间 */
@property (nonatomic,copy) NSString *startDateStr;
/** 结束时间 */
@property (nonatomic,copy) NSString *endDateStr;
/** 页数 */
@property (nonatomic,assign) NSInteger page;
/** 数据源数组 */
@property (nonatomic,strong) NSMutableArray *recordArrs;
/** account */
@property (nonatomic,copy) NSString *account;
/** gameId */
@property (nonatomic,copy) NSString *gameId;
/**  */
@property (nonatomic,assign) UserTraceStatus resultId;

@end

RouterKey *const kRouterChaseRecord = @"chaseRecord";

@implementation LukeTraceViewController

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    return [USER_DATA_MANAGER shouldPush2Login];
}

+(void)load{
    
    [self registerJumpRouterKey:kRouterChaseRecord toHandle:^(NSDictionary *parameters) {
        LukeTraceViewController *vc = [[LukeTraceViewController alloc] init];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }];
    
    [self registerInstanceRouterKey:kRouterChaseRecord toHandle:^UIViewController *(NSDictionary *parameters) {
        LukeTraceViewController *vc = [[LukeTraceViewController alloc] init];
        return vc;
    }];
    
    
    
}
- (LukeSearchTopView *)searchTopView{
    if (_searchTopView == nil) {
        _searchTopView = [[LukeSearchTopView alloc] initWithFrame:CGRectZero firstDateStr:[NSDate getNewTimeFormat:@"yyyy/MM/dd"] secondDateStr:[NSDate getNewTimeFormat:@"yyyy/MM/dd"] title:@"筛选"];
        _searchTopView.delegate = self;
        [self.view addSubview:_searchTopView];
        [_searchTopView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_top);
            make.leading.trailing.equalTo(self.view);
            make.height.mas_equalTo(40);
        }];
    }
    return _searchTopView;
}

#pragma mark - LukeSearchTopViewDelegate
- (void)SearchTopViewfirstDateButtonClick
{
    [self showDatePickerdefaultSelValue:nil minDate:[NSDate offsetDays:-8 fromDate:[NSDate dateUTC]] resultBlock:^(NSString *selectValue) {
        self.startDateStr = selectValue;
        [self.searchTopView.firstDateBtn setTitle:[[NSDate dateWithString:selectValue format:@"yyyy-MM-dd"] stringWithFormat:@"yyyy/MM/dd"] forState:UIControlStateNormal];
        self.page = 1;
        [self.tableView.mj_header beginRefreshing];
    }];
}

- (void)SearchTopViewsecondDateButtonClick
{
    [self showDatePickerdefaultSelValue:nil minDate:[NSDate offsetDays:-8 fromDate:[NSDate dateUTC]] resultBlock:^(NSString *selectValue) {
        self.endDateStr = selectValue;
        [self.searchTopView.secondDateBtn setTitle:[[NSDate dateWithString:selectValue format:@"yyyy-MM-dd"] stringWithFormat:@"yyyy/MM/dd"] forState:UIControlStateNormal];
        self.page = 1;
        [self.tableView.mj_header beginRefreshing];
    }];
}

- (void)SearchTopViewSearchButtonClick
{
    [UIView animateWithDuration:0.3 animations:^{
        self.screenView.transform = CGAffineTransformMakeTranslation(0, -HEIGHT);
    }];
}

- (NSMutableArray *)recordArrs
{
    if (_recordArrs == nil) {
        _recordArrs = [NSMutableArray array];
    }
    return _recordArrs;
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.backgroundColor = [UIColor skinViewScreenColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedRowHeight = 160;
        _tableView.tableFooterView = [UIView new];
        [_tableView registerClass:[LukeTraceCell class] forCellReuseIdentifier:LukeTraceCellID];
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.segmentControl.mas_bottom);
            make.leading.trailing.bottom.equalTo(self.view);
        }];
    }
    return _tableView;
}
- (LukeScreenView *)screenView
{
    if (_screenView == nil) {
        _screenView = [[LukeScreenView alloc] initWithFrame:CGRectZero isDL:USER_DATA_MANAGER.userInfoData.isDl isTouzhu:NO];
        _screenView.delegate = self;
        [self.view addSubview:_screenView];
        [_screenView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_bottom);
            make.leading.trailing.equalTo(self.view);
            make.height.mas_equalTo(HEIGHT);
        }];
    }
    return _screenView;
}

#pragma mark - LukeScreenViewDelegate
- (void)reloadRecordDataWithUser:(NSString *)user gameID:(NSString *)gameID model:(NSString *)model
{
    [self screenViewCancelClick];
    self.account = user;
    self.gameId = gameID;
    self.page = 1;
    [self.tableView.mj_header beginRefreshing];
}

- (void)screenViewCancelClick
{
    [UIView animateWithDuration:0.3 animations:^{
        self.screenView.transform = CGAffineTransformIdentity;
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setupRefresh];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor skinViewScreenColor];
}

- (void)setupUI
{
    self.view.backgroundColor = [UIColor whiteColor];
    self.endDateStr = [NSDate getNewTimeFormat:[NSDate ymdFormat]];
    self.startDateStr = self.endDateStr;
    self.resultId = UserTraceStatusAll;
    NSArray *titles = @[@"所有状态",@"中奖",@"未中奖"];
    HMSegmentedControl *segmentControl = [HMSegmentedControl createSegmentWithBackgroundColor:[UIColor blackColor] IndicatorColor:[UIColor redColor] selectedTitleColor:[UIColor redColor] titleColor:[UIColor whiteColor] titleArrs:titles addTarget:self action:@selector(segmentControlClick:)];
    [self.view addSubview:segmentControl];
    [segmentControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.searchTopView.mas_bottom);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    self.segmentControl = segmentControl;
}

- (void)segmentControlClick:(HMSegmentedControl *)segment
{
    NSInteger index = segment.selectedSegmentIndex;
    switch (index) {
        case 0:
            self.resultId = UserTraceStatusAll;
            break;
        case 1:
            self.resultId = UserTraceStatusWin;
            break;
        case 2:
            self.resultId = UserTraceStatusLose;
            break;
        default:
            break;
    }
    self.page = 1;
    [self.tableView.mj_header beginRefreshing];
}

- (void)setupRefresh
{
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self queryData];
    }];
    
    [self.tableView.mj_header beginRefreshing];
    
    self.tableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        self.page ++;
        [self queryData];
    }];
}

#pragma mark - 网络请求数据
- (void)queryData
{
    self.tableView.isShowEmpty = NO;
    [NET_DATA_MANAGER requestGetPursueByAccount:self.account Page:self.page Rows:10 StartDate:self.startDateStr EndDate:self.endDateStr gameId:self.gameId result:self.resultId Success:^(id responseObject) {
        if (self.page == 1) {
            [self.recordArrs removeAllObjects];
        }
        [self.recordArrs addObjectsFromArray:[LukeTraceItem mj_objectArrayWithKeyValuesArray:responseObject[@"data"]]];
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        self.tableView.isShowEmpty = YES;
        self.tableView.titleForEmpty = @"暂无追号记录";
        self.tableView.descriptionForEmpty = @"未找到您提交的追号记录";
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        self.tableView.isShowEmpty = YES;
        self.tableView.titleForEmpty = @"数据错误";
        self.tableView.descriptionForEmpty = @"请检查您的网络";
    }];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.recordArrs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LukeTraceCell *cell = [tableView dequeueReusableCellWithIdentifier:LukeTraceCellID];
    cell.item = self.recordArrs[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LukeTraceDescViewController *descVC = [[LukeTraceDescViewController alloc] init];
    
    descVC.item = self.recordArrs[indexPath.row];
    
    [self.navigationController pushViewController:descVC animated:YES];
    descVC.hidesBottomBarWhenPushed = YES;
}

@end
