//
//  LukePushMessageViewController.h
//  Bet365
//
//  Created by luke on 17/6/15.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PushMessageViewControllerDelegate <NSObject>

@optional

- (void)reloadMessage;

@end

@interface LukePushMessageViewController : Bet365ViewController
/** 代理 */
@property (nonatomic,weak) id<PushMessageViewControllerDelegate>delegate;

@end
