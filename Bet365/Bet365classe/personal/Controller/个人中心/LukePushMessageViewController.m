//
//  LukePushMessageViewController.m
//  Bet365
//
//  Created by luke on 17/6/15.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukePushMessageViewController.h"
#import "LukePushMessageCell.h"
#import "LukePushMessageItem.h"
#import "LukePushMagDescViewController.h"
#import "NetWorkMannager+Account.h"
#import "UIViewControllerSerializing.h"

static NSString * const LukePushMessageCellID = @"LukePushMessageCell";

@interface LukePushMessageViewController ()
<UITableViewDelegate,
UITableViewDataSource,
UIViewControllerSerializing>
/** 数据源数组 */
@property (nonatomic,strong) NSArray *messageDatas;
/**  */
@property (nonatomic,strong) UITableView *tableView;

@end

RouterKey *const kRouterMessage = @"message";

@implementation LukePushMessageViewController

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    return [USER_DATA_MANAGER shouldPush2Login];
}

+(void)load{
    [self registerJumpRouterKey:kRouterMessage toHandle:^(NSDictionary *parameters) {
        LukePushMessageViewController *vc = [[LukePushMessageViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
    }];
    
    [self registerInstanceRouterKey:kRouterMessage toHandle:^UIViewController *(NSDictionary *parameters) {
        LukePushMessageViewController *vc = [[LukePushMessageViewController alloc] init];
        return vc;
    }];
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.backgroundColor = [UIColor skinViewScreenColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.rowHeight = 60;
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _tableView.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0);
        [_tableView registerClass:[LukePushMessageCell class] forCellReuseIdentifier:LukePushMessageCellID];
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view).offset(0);
            make.leading.trailing.bottom.equalTo(self.view);
        }];
        _tableView.titleForEmpty = @"暂无消息";
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self queryData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismissWithCompletion:nil];
}

#pragma mark - 请求数据
- (void)queryData
{
    [SVProgressHUD showWithStatus:@"获取数据中..."];
    [NET_DATA_MANAGER requestGetMessageListSuccess:^(id responseObject) {
        self.messageDatas = [LukePushMessageItem mj_objectArrayWithKeyValuesArray:responseObject];
        [self.tableView reloadData];
        [SVProgressHUD dismissWithDelay:0.3];
        self.tableView.isShowEmpty = YES;
    } failure:^(NSError *error) {
        NSLog(@"%@",error.msg);
        [SVProgressHUD dismiss];
        self.tableView.isShowEmpty = YES;
    }];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.messageDatas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LukePushMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:LukePushMessageCellID];
    cell.item = self.messageDatas[indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LukePushMessageItem *item = self.messageDatas[indexPath.row];
    LukePushMagDescViewController *descVC = [[LukePushMagDescViewController alloc] init];
    descVC.item = item;
    [SVProgressHUD showWithStatus:@"读取中..."];
    [NET_DATA_MANAGER requestGetReadMessageWithId:item.PushID Success:^(id responseObject) {
        [SVProgressHUD dismiss];
        [self.navigationController pushViewController:descVC animated:YES];
        if ([self.delegate respondsToSelector:@selector(reloadMessage)]) {
            [self.delegate reloadMessage];
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error.msg);
    }];
}

@end
