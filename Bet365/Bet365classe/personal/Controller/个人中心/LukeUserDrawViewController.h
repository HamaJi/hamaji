//
//  LukeUserDrawViewController.h
//  Bet365
//
//  Created by luke on 17/6/20.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Bet365UserDrawModel.h"
@interface LukeUserDrawViewController : Bet365ViewController
- (void)getDrawMessage:(Bet365UserDrawModel *)userModel;//传递进行处理
@end
