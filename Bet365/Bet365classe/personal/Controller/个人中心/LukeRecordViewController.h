//
//  LukeRecordViewController.h
//  Bet365
//
//  Created by luke on 17/6/15.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LukeRecordViewController : Bet365ViewController
-(void)enterLotteryRecord:(NSString *)gameId;
@end
