//
//  LukeTeamCountViewController.m
//  Bet365
//
//  Created by luke on 17/6/27.
//  Copyright © 2017年 jesse. All rights reserved.
//个人总览

#import "LukeTeamCountViewController.h"
#import "Bet365PersonalRwsModel.h"
#import "UIViewControllerSerializing.h"

@interface LukeTeamCountViewController ()<UIViewControllerSerializing>
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIButton *endButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLb;
@property (weak, nonatomic) IBOutlet UILabel *teamCountLb;
@property (weak, nonatomic) IBOutlet UILabel *registCountLb;
@property (weak, nonatomic) IBOutlet UILabel *dlRebateLb;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *teamLayoutConst;
@property (weak, nonatomic) IBOutlet UILabel *moneyLb;
@property (weak, nonatomic) IBOutlet UILabel *rechMoneyLb;
@property (weak, nonatomic) IBOutlet UILabel *discountMoneyLb;
@property (weak, nonatomic) IBOutlet UILabel *withdrawMoneyLb;
@property (weak, nonatomic) IBOutlet UILabel *cpBettingMoneyLb;
@property (weak, nonatomic) IBOutlet UILabel *cpRebateMoneyLb;
@property (weak, nonatomic) IBOutlet UILabel *cpWinOrcloseMoneyLb;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sportLayoutCont;
@property (weak, nonatomic) IBOutlet UILabel *sportBettingMoneyLb;
@property (weak, nonatomic) IBOutlet UILabel *sportRebateMoneyLb;
@property (weak, nonatomic) IBOutlet UILabel *sportWinOrcloseMoneyLb;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *liveLayoutConst;
@property (weak, nonatomic) IBOutlet UILabel *liveBettingMoneyLb;
@property (weak, nonatomic) IBOutlet UILabel *liveRebateMoneyLb;
@property (weak, nonatomic) IBOutlet UILabel *liveWinOrcloseMoneyLb;
@property (weak, nonatomic) IBOutlet UILabel *totalMoneyLb;
@property (weak, nonatomic) IBOutlet UILabel *totalTitleLb;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dlViewLayoutConst;
@property (weak, nonatomic) IBOutlet UILabel *dlRebateLabel;
@property (weak, nonatomic) IBOutlet UILabel *dlBonuusLabel;
@property (weak, nonatomic) IBOutlet UILabel *dlDayWageLabel;

@property (weak, nonatomic) IBOutlet UILabel *promotionBonusLabel;
@property (weak, nonatomic) IBOutlet UILabel *promotionBonusUserCountLabel;

/** 开始时间 */
@property (nonatomic,copy) NSString *startDateStr;
/** 结束时间 */
@property (nonatomic,copy) NSString *endDateStr;
/** 模型 */
@property (nonatomic,strong) Bet365PersonalRwsModel *model;

@end

RouterKey *const kRouterAgentPersonal = @"agent/personal";

RouterKey *const kRouterAgentShowTeam = @"agent/showTeam";


@implementation LukeTeamCountViewController

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    return [USER_DATA_MANAGER shouldPush2Login];
}

+(void)load{
    
    [self registerJumpRouterKey:kRouterAgentPersonal toHandle:^(NSDictionary *parameters) {
        LukeTeamCountViewController *teamVC = [[LukeTeamCountViewController alloc] init];
        teamVC.rwsType = PersonTeamRwsType_Person;
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:teamVC animated:YES];
        teamVC.hidesBottomBarWhenPushed = YES;
    }];
    
    [self registerJumpRouterKey:kRouterAgentShowTeam toHandle:^(NSDictionary *parameters) {
        LukeTeamCountViewController *teamVC = [[LukeTeamCountViewController alloc] init];
        teamVC.rwsType = PersonTeamRwsType_Team;
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:teamVC animated:YES];
        teamVC.hidesBottomBarWhenPushed = YES;
    }];
    
    [self registerInstanceRouterKey:kRouterAgentPersonal toHandle:^UIViewController *(NSDictionary *parameters) {
        LukeTeamCountViewController *teamVC = [[LukeTeamCountViewController alloc] init];
        teamVC.rwsType = PersonTeamRwsType_Person;
        return teamVC;
    }];
    
    [self registerInstanceRouterKey:kRouterAgentShowTeam toHandle:^UIViewController *(NSDictionary *parameters) {
        LukeTeamCountViewController *teamVC = [[LukeTeamCountViewController alloc] init];
        teamVC.rwsType = PersonTeamRwsType_Team;
        return teamVC;
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.endDateStr = [NSDate getNewTimeFormat:@"yyyy-MM-dd"];
    self.startDateStr = self.endDateStr;
    [self.startButton setImagePosition:LXMImagePositionRight spacing:10];
    [self.endButton setImagePosition:LXMImagePositionRight spacing:10];
    [self.startButton setTitle:self.endDateStr forState:UIControlStateNormal];
    [self.endButton setTitle:self.endDateStr forState:UIControlStateNormal];
    [self queryData];
    self.view.backgroundColor = [UIColor skinViewScreenColor];
}

- (IBAction)startBtnClick:(UIButton *)sender {
    [self showDatePickerdefaultSelValue:sender.currentTitle minDate:nil resultBlock:^(NSString *selectValue) {
        self.startDateStr = selectValue;
        [sender setTitle:selectValue forState:UIControlStateNormal];
        [self queryData];
    }];
}

- (IBAction)endBtnClick:(UIButton *)sender {
    [self showDatePickerdefaultSelValue:sender.currentTitle minDate:nil resultBlock:^(NSString *selectValue) {
        self.endDateStr = selectValue;
        [sender setTitle:selectValue forState:UIControlStateNormal];
        [self queryData];
    }];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [SVProgressHUD dismiss];
}

- (void)queryData
{
    [SVProgressHUD showWithStatus:@"刷新中..."];
    [NET_DATA_MANAGER requestGetRwsWithUserId:self.userId PersonTeamRwsType:self.rwsType beginDate:self.startDateStr endDate:self.endDateStr Success:^(id responseObject) {
        self.model = [Bet365PersonalRwsModel mj_objectWithKeyValues:responseObject];
        [SVProgressHUD dismiss];
        [self setModelForUILabel];
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

- (void)setModelForUILabel
{
    if (self.rwsType == PersonTeamRwsType_Team) {
        self.teamCountLb.text = self.model.teamCount ? [self.model.teamCount stringValue] : @"0";
        self.registCountLb.text = self.model.registCount ? [self.model.registCount stringValue] : @"0";
        CGFloat dlTotal = [self.model.dlRebate floatValue] + [self.model.dlBonuus floatValue] + [self.model.dlDayWage floatValue];
        self.dlRebateLb.text = StringFormatWithFloat(dlTotal);
        self.titleLb.text = @"团队总览";
        self.dlViewLayoutConst.constant = 0;
    }else{
        self.teamLayoutConst.constant = 0;
        self.titleLb.text = @"个人总览";
        if (USER_DATA_MANAGER.userInfoData.isDl) {
            self.dlRebateLabel.text = self.model.dlRebate ? StringFormatWithFloat([self.model.dlRebate floatValue]) : @"0.00";
            self.dlBonuusLabel.text = self.model.dlBonuus ? StringFormatWithFloat([self.model.dlBonuus floatValue]) : @"0.00";
            self.dlDayWageLabel.text = self.model.dlDayWage ? StringFormatWithFloat([self.model.dlDayWage floatValue]) : @"0.00";
        }else{
            self.dlViewLayoutConst.constant = 0;
        }
    }
    self.moneyLb.text = self.model.money ? StringFormatWithFloat([self.model.money floatValue]) : @"0.00";
    self.rechMoneyLb.text = self.model.rwReport.rechMoney ? StringFormatWithFloat([self.model.rwReport.rechMoney floatValue]) : @"0.00";
    self.discountMoneyLb.text = self.model.rwReport.withdrawMoney ? StringFormatWithFloat([self.model.rwReport.withdrawMoney floatValue]) : @"0.00";
    CGFloat drawMoney = [self.model.rwReport.otherDiscount floatValue] + [self.model.rwReport.rechDiscount floatValue];
    self.withdrawMoneyLb.text = StringFormatWithFloat(drawMoney);
    self.cpBettingMoneyLb.text = self.model.cpBetReport.bettingMoney ? StringFormatWithFloat([self.model.cpBetReport.bettingMoney floatValue]) : @"0.00";
    self.cpRebateMoneyLb.text = self.model.cpBetReport.rebateMoney ? StringFormatWithFloat([self.model.cpBetReport.rebateMoney floatValue]) : @"0.00";
    self.cpWinOrcloseMoneyLb.text = self.model.cpBetReport.winOrcloseMoney ? StringFormatWithFloat([self.model.cpBetReport.winOrcloseMoney floatValue] !=0 ? ([self.model.cpBetReport.winOrcloseMoney floatValue] * -1) : 0) : @"0.00";
    if (BET_CONFIG.common_config.isDP) {
        self.sportBettingMoneyLb.text = self.model.ftBetReport.bettingMoney ? StringFormatWithFloat([self.model.ftBetReport.bettingMoney floatValue]) : @"0.00";
        self.sportRebateMoneyLb.text = self.model.ftBetReport.rebateMoney ? StringFormatWithFloat([self.model.ftBetReport.rebateMoney floatValue]) : @"0.00";
        self.sportWinOrcloseMoneyLb.text = self.model.ftBetReport.winOrcloseMoney ? StringFormatWithFloat([self.model.ftBetReport.winOrcloseMoney floatValue] != 0 ? ([self.model.ftBetReport.winOrcloseMoney floatValue] * -1) : 0) : @"0.00";
        self.liveBettingMoneyLb.text = self.model.liveBetReport.bettingMoney ? StringFormatWithFloat([self.model.liveBetReport.bettingMoney floatValue]) : @"0.00";
        self.liveRebateMoneyLb.text = self.model.liveBetReport.rebateMoney ? StringFormatWithFloat([self.model.liveBetReport.rebateMoney floatValue]) : @"0.00";
        CGFloat winOrcloseMoney = [self.model.liveBetReport.rebateMoney floatValue] - [self.model.liveBetReport.winOrcloseMoney floatValue];
        self.liveWinOrcloseMoneyLb.text = StringFormatWithFloat(winOrcloseMoney);
    }else{
        self.sportLayoutCont.constant = 0;
        self.liveLayoutConst.constant = 0;
    }
    
    self.promotionBonusLabel.text = self.model.promotionBonus ? [self.model.promotionBonus decimalNumberByRoundingDownString] : @"0.0";
    self.promotionBonusUserCountLabel.text = self.model.promotionBonusUserCount ? [self.model.promotionBonusUserCount decimalNumberByRoundingDownString] : @"0.0";
    
    if (USER_DATA_MANAGER.userInfoData.isDl) {
        self.totalTitleLb.text = @"净盈利 = 游戏输赢 + 代理总计 + 优惠总额 + 活动彩金";
    }else{
        self.totalTitleLb.text = @"净盈利 = 游戏输赢 + 优惠总额 + 活动彩金";
    }
    CGFloat winMoney = [self.model.cpBetReport.winOrcloseMoney floatValue] * -1 + [self.model.ftBetReport.winOrcloseMoney floatValue] * -1 + [self.model.liveBetReport.winOrcloseMoney floatValue] * -1 + [self.model.liveBetReport.rebateMoney floatValue] + [self.model.dlRebate floatValue] + [self.model.rwReport.rechDiscount floatValue] + [self.model.rwReport.otherDiscount floatValue] + [self.model.dlDayWage floatValue] + [self.model.dlBonuus floatValue] + [self.model.promotionBonus floatValue];
    self.totalMoneyLb.text = StringFormatWithFloat(winMoney);
    
}

#pragma mark - 懒加载
- (NSNumber *)userId
{
    if (_userId == nil) {
        _userId = USER_DATA_MANAGER.userInfoData.userId;
    }
    return _userId;
}

@end

