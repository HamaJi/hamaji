//
//  LukeSubUsersSearchViewController.m
//  Bet365
//
//  Created by luke on 17/7/23.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeSubUsersSearchViewController.h"
#import "LukeRegisterLbTFView.h"

@interface LukeSubUsersSearchViewController ()
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *nameView;
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *minView;
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *maxView;
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *minTimeView;
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *maxTimeView;

@end

@implementation LukeSubUsersSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
}

- (void)backClick
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setupUI
{
    self.view.backgroundColor = JesseColor(239, 239, 244);
    UIView *contentView = [[UIView alloc] init];
    contentView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(0);
    }];
    
    UIButton *backBtn = [UIButton addButtonWithTitle:nil font:17 color:nil target:self action:@selector(backClick)];
    [backBtn setImage:[UIImage imageNamed:@"left_lage"] forState:UIControlStateNormal];
    [contentView addSubview:backBtn];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(contentView).offset(10);
        make.top.equalTo(contentView).offset(statusHeight + 10);
    }];
    
    UILabel *titleLb = [UILabel addLabelWithFont:17 color:[UIColor blackColor] title:@"用户搜索"];
    titleLb.textAlignment = NSTextAlignmentCenter;
    [contentView addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(contentView);
        make.centerY.equalTo(backBtn);
    }];
    
    LukeRegisterLbTFView *nameView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"用户名" textEntry:NO placeholder:@"用户名" keyboardType:UIKeyboardTypeDefault];
    [self.view addSubview:nameView];
    [nameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(contentView.mas_bottom).offset(10);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    self.nameView = nameView;
    
    UILabel *moneyLb = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:@"余额"];
    [self.view addSubview:moneyLb];
    [moneyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(nameView.mas_bottom).offset(10);
        make.leading.equalTo(self.view).offset(10);
    }];
    
    LukeRegisterLbTFView *minView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"从" textEntry:NO placeholder:@"最小余额" keyboardType:UIKeyboardTypeNumberPad];
    [self.view addSubview:minView];
    [minView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(moneyLb.mas_bottom).offset(5);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    self.minView = minView;
    
    LukeRegisterLbTFView *maxView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"至" textEntry:NO placeholder:@"最大余额" keyboardType:UIKeyboardTypeNumberPad];
    [self.view addSubview:maxView];
    [maxView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(minView.mas_bottom);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    self.maxView = maxView;
    
    UILabel *timeLb = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:@"登录时间"];
    [self.view addSubview:timeLb];
    [timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(maxView.mas_bottom).offset(10);
        make.leading.equalTo(self.view).offset(10);
    }];
    
    LukeRegisterLbTFView *minTimeView = [LukeRegisterLbTFView registerLbBtnViewWithTitle:@"从" btnImage:@"right_lage" btnTitle:@"起始时间"];
    @weakify(self);
    minTimeView.callBlock = ^{
        @strongify(self);
        [self showDatePickerdefaultSelValue:nil minDate:nil resultBlock:^(NSString *selectValue) {
            [self.minTimeView.dateBtn setTitle:selectValue forState:UIControlStateNormal];
        }];
    };
    [self.view addSubview:minTimeView];
    [minTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(timeLb.mas_bottom).offset(5);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    self.minTimeView = minTimeView;
    
    LukeRegisterLbTFView *maxTimeView = [LukeRegisterLbTFView registerLbBtnViewWithTitle:@"至" btnImage:@"right_lage" btnTitle:@"结束时间"];
    maxTimeView.callBlock = ^{
        @strongify(self);
        [self showDatePickerdefaultSelValue:nil minDate:nil resultBlock:^(NSString *selectValue) {
            [self.maxTimeView.dateBtn setTitle:selectValue forState:UIControlStateNormal];
        }];
    };
    [self.view addSubview:maxTimeView];
    [maxTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(minTimeView.mas_bottom);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    self.maxTimeView = maxTimeView;
    
    UIButton *searchBtn = [UIButton addButtonWithTitle:@"搜索" font:17 color:[UIColor whiteColor] target:self action:@selector(searchClick)];
    searchBtn.backgroundColor = [UIColor redColor];
    searchBtn.layer.cornerRadius = 5;
    searchBtn.layer.masksToBounds = YES;
    [self.view addSubview:searchBtn];
    [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.view).offset(10);
        make.trailing.equalTo(self.view).offset(-10);
        make.height.mas_equalTo(40);
        make.bottom.equalTo(self.view).offset(-30);
    }];
}

- (void)searchClick
{
    if ([self.delegate respondsToSelector:@selector(reloadSubUserSearchWithAccount:moneyFrom:moneyTo:startDate:endDate:)]) {
        [self.delegate reloadSubUserSearchWithAccount:self.nameView.textfield.text moneyFrom:self.minView.textfield.text moneyTo:self.maxView.textfield.text startDate:[self.minTimeView.dateBtn.currentTitle containsString:@"-"] ? self.minTimeView.dateBtn.currentTitle : nil endDate:[self.maxTimeView.dateBtn.currentTitle containsString:@"-"] ? self.maxTimeView.dateBtn.currentTitle : nil];
        [self backClick];
    }
}

@end
