//
//  Bet365ReleaseBankController.h
//  Bet365
//
//  Created by luke on 2018/11/10.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ReleaseBankControllerDelegate <NSObject>

- (void)releaseBankPop;

@end

@interface Bet365ReleaseBankController : Bet365ViewController

/**  */
@property (nonatomic,weak) id<ReleaseBankControllerDelegate>delegate;

@end
