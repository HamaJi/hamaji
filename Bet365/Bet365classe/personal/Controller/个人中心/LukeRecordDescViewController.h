//
//  LukeRecordDescViewController.h
//  Bet365
//
//  Created by luke on 17/6/22.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LukeRecordItem;

@interface LukeRecordDescViewController : Bet365ViewController
/** 模型 */
@property (nonatomic,strong) LukeRecordItem *item;

@end
