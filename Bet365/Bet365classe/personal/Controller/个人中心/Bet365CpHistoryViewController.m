//
//  LukeRecordViewController.m
//  Bet365
//
//  Created by luke on 17/6/15.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "Bet365CpHistoryViewController.h"
#import "LukeScreenView.h"
#import "LukeRecordItem.h"
#import "LukeRecordCell.h"
#import "LukeRecordDescViewController.h"
#import "LukeDateSearchView.h"
#import "NetWorkMannager+Account.h"

static NSString * const LukeRecordCellID = @"LukeRecordCell";
static NSString * const CPRecordStreamlineCellID = @"CPRecordStreamlineCell";

@interface Bet365CpHistoryViewController ()<
UITableViewDelegate,
UITableViewDataSource,
LukeScreenViewDelegate>
/** 筛选view */
@property (nonatomic,strong) LukeScreenView *screenView;
/** tableview */
@property (nonatomic,strong) UITableView *tableView;
/** 页数 */
@property (nonatomic,assign) int page;
/** 数据源数组 */
@property (nonatomic,strong) NSMutableArray *recordArrs;
/**  */
@property (nonatomic,copy) NSString *gameId;
/**  */
@property (nonatomic,copy) NSString *account;
/**  */
@property (nonatomic,copy) NSString *model;
/**  */
@property (nonatomic,weak) UIButton *topBtn;

@end

@implementation Bet365CpHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setupUI];
    [self setupRefresh];
}

- (void)setupUI
{
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    
    UIImage *image = [[UIImage imageNamed:@"search"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    UIButton *topBtn = [UIButton addButtonWithTitle:@"搜索" font:IS_IPHONE_5s ? 15 : 17 color:[UIColor skinViewKitSelColor] target:self action:@selector(searchTopClick)];
    topBtn.backgroundColor = [UIColor skinViewBgColor];
    topBtn.layer.cornerRadius = 5;
    topBtn.layer.masksToBounds = YES;
    [topBtn setImagePosition:LXMImagePositionLeft spacing:10];
    [topBtn setImage:image forState:UIControlStateNormal];
    [topBtn.imageView setTintColor:[UIColor skinViewKitSelColor]];
    [self.view addSubview:topBtn];
    [topBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.view).offset(5);
        make.trailing.equalTo(self.view).offset(-5);
        make.height.mas_equalTo(35);
        make.top.equalTo(self.view).offset(STATUSAND_NAVGATION_HEIGHT + 8);
    }];
    self.topBtn = topBtn;
}

- (void)searchTopClick
{
    [UIView animateWithDuration:0.3 animations:^{
        self.screenView.transform = CGAffineTransformMakeTranslation(0, -HEIGHT);
    }];
}

- (void)setupRefresh
{
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self queryData];
    }];
    
    [self.tableView.mj_header beginRefreshing];
    self.tableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        self.page ++;
        [self queryData];
    }];
}

#pragma mark - 网络请求数据
- (void)queryData
{
    [NET_DATA_MANAGER requestGetCpHistoryListAccount:self.account Page:self.page Rows:10 date:self.date gameId:self.gameId model:self.model Success:^(id responseObject) {
        if (self.page == 1) {
            [self.recordArrs removeAllObjects];
        }
        [self.recordArrs addObjectsFromArray:[LukeRecordItem mj_objectArrayWithKeyValuesArray:responseObject[@"data"]]];
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        self.tableView.isShowEmpty = YES;
    } failure:^(NSError *error) {
        NSLog(@"%@",error.msg);
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        self.tableView.isShowEmpty = YES;
    }];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.recordArrs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *account = USER_DATA_MANAGER.userInfoData.account;
    BOOL isSw = [[NSUserDefaults standardUserDefaults] boolForKey:account];
    if (isSw) {
        CPRecordStreamlineCell *cell = [tableView dequeueReusableCellWithIdentifier:CPRecordStreamlineCellID];
        cell.item = self.recordArrs[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        LukeRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:LukeRecordCellID];
        cell.item = self.recordArrs[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LukeRecordDescViewController *descVC = [[LukeRecordDescViewController alloc] init];
    descVC.item = self.recordArrs[indexPath.row];
    [self.navigationController pushViewController:descVC animated:YES];
    descVC.hidesBottomBarWhenPushed = YES;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *account = USER_DATA_MANAGER.userInfoData.account;
    BOOL isSw = [[NSUserDefaults standardUserDefaults] boolForKey:account];
    if (isSw) {
        return 60;
    }else{
        return 120;
    }
}

- (LukeScreenView *)screenView
{
    if (_screenView == nil) {
        _screenView = [[LukeScreenView alloc] initWithFrame:CGRectZero isDL:USER_DATA_MANAGER.userInfoData.isDl isTouzhu:YES];
        _screenView.delegate = self;
        [self.view addSubview:_screenView];
        [_screenView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_bottom);
            make.leading.trailing.equalTo(self.view);
            make.height.mas_equalTo(HEIGHT);
        }];
    }
    return _screenView;
}

#pragma mark - LukeScreenViewDelegate
- (void)reloadRecordDataWithUser:(NSString *)user gameID:(NSString *)gameID model:(NSString *)model
{
    [self screenViewCancelClick];
    self.account = user;
    self.gameId = gameID;
    self.model = model;
    self.page = 1;
    [self.tableView.mj_header beginRefreshing];
}

- (void)screenViewCancelClick
{
    [UIView animateWithDuration:0.3 animations:^{
        self.screenView.transform = CGAffineTransformIdentity;
    }];
}

- (void)screenViewSwitchWithBool:(BOOL)sw
{
    [self.tableView reloadData];
}
- (NSMutableArray *)recordArrs
{
    if (_recordArrs == nil) {
        _recordArrs = [NSMutableArray array];
    }
    return _recordArrs;
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.backgroundColor = [UIColor skinViewScreenColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.tableFooterView = [UIView new];
        if ([_tableView respondsToSelector:@selector(layoutMargins)]) {
            _tableView.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        if ([_tableView respondsToSelector:@selector(separatorInset)]) {
            _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        [_tableView registerClass:[LukeRecordCell class] forCellReuseIdentifier:LukeRecordCellID];
        [_tableView registerClass:[CPRecordStreamlineCell class] forCellReuseIdentifier:CPRecordStreamlineCellID];
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.topBtn.mas_bottom).offset(8);
            make.leading.trailing.bottom.equalTo(self.view);
        }];
        _tableView.titleForEmpty = @"没有数据";
    }
    return _tableView;
}

@end

