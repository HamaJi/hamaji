//
//  LukeSportRecordViewController.m
//  Bet365
//
//  Created by luke on 2017/11/7.
//  Copyright © 2017年 jesse. All rights reserved.
//充值订单

#import "LukeRechargeOrderViewController.h"
#import "LukeSearchTopView.h"
#import "LukeRechargeOrderSelectView.h"
#import "LukeRechargeOrderItem.h"
#import "LukeRechargeOrderCell.h"
#import "LukeRechargeOrderDescViewController.h"
#import "NetWorkMannager+Account.h"
#import "UIViewControllerSerializing.h"
#import "HMSegmentedControl+Bet365.h"

static NSString * const LukeRechargeOrderCellID = @"LukeRechargeOrderCell";
@interface LukeRechargeOrderViewController ()
<LukeSearchTopViewDelegate,
UITableViewDelegate,
UITableViewDataSource,
LukeRechargeOrderSelectViewDelegate,
TimerManagerDelegate,
UIViewControllerSerializing>
/**  */
@property (nonatomic,strong) UITableView *tableView;
/** 顶部时间选择view */
@property (nonatomic,strong) LukeSearchTopView *searchView;
/** 起始时间 */
@property (nonatomic,copy) NSString *beginDateStr;
/** 结束时间 */
@property (nonatomic,copy) NSString *endDateStr;
/** 数据数组 */
@property (nonatomic,strong) NSMutableArray *dataList;
/** 页数 */
@property (nonatomic,assign) NSInteger pages;
/** 筛选 */
@property (nonatomic,strong) LukeRechargeOrderSelectView *selectView;
/** 筛选赛事 */
@property (nonatomic,copy) NSString *mode;
/**  */
@property (nonatomic,assign) PersonalRechargeOrderStatus status;
/**  */
@property (nonatomic,weak) HMSegmentedControl *segmentControl;

@end

RouterKey *const kRouterRechargeRcord = @"recharge-record";

@implementation LukeRechargeOrderViewController

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    return [USER_DATA_MANAGER shouldPush2Login];
}

+(void)load{
    [self registerJumpRouterKey:kRouterRechargeRcord toHandle:^(NSDictionary *parameters) {
        LukeRechargeOrderViewController *vc = [[LukeRechargeOrderViewController alloc] init];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }];
    
    [self registerInstanceRouterKey:kRouterRechargeRcord toHandle:^UIViewController *(NSDictionary *parameters) {
        LukeRechargeOrderViewController *vc = [[LukeRechargeOrderViewController alloc] init];
        return vc;
    }];
}

- (NSMutableArray *)dataList
{
    if (_dataList == nil) {
        _dataList = [NSMutableArray array];
    }
    return _dataList;
}

- (LukeRechargeOrderSelectView *)selectView
{
    if (_selectView == nil) {
        _selectView = [[LukeRechargeOrderSelectView alloc] init];
        _selectView.delegate = self;
        [self.view addSubview:_selectView];
        [_selectView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(self.view);
            make.top.equalTo(self.view.mas_bottom);
            make.height.mas_equalTo(HEIGHT);
        }];
    }
    return _selectView;
}

#pragma mark - LukeSportSelectViewDelegate
- (void)rechargeOrderSelectCancelClick
{
    [UIView animateWithDuration:0.25 animations:^{
        self.selectView.transform = CGAffineTransformIdentity;
    }];
}

- (void)rechargeOrderSelectWithAccount:(NSString *)account key:(NSString *)key
{
    if ([key isEqualToString:@"all"]) {
        self.mode = nil;
    }else{
        self.mode = key;
    }
    [self rechargeOrderSelectCancelClick];
    self.pages = 1;
    [self.tableView.mj_header beginRefreshing];
}

- (LukeSearchTopView *)searchView
{
    if (_searchView == nil) {
        _searchView = [[LukeSearchTopView alloc] initWithFrame:CGRectZero firstDateStr:[NSDate getNewTimeFormat:@"yyyy/MM/dd"] secondDateStr:[NSDate getNewTimeFormat:@"yyyy/MM/dd"] title:@"筛选"];
        _searchView.delegate = self;
        [self.view addSubview:_searchView];
        [_searchView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(self.view);
            make.top.equalTo(self.view).offset(0);
            make.height.mas_equalTo(40);
        }];
    }
    return _searchView;
}

#pragma mark - LukeSearchTopViewDelegate
- (void)SearchTopViewfirstDateButtonClick
{
    [self showDatePickerdefaultSelValue:nil minDate:[NSDate offsetDays:-8 fromDate:[NSDate dateUTC]] resultBlock:^(NSString *selectValue) {
        self.pages = 1;
        self.beginDateStr = selectValue;
        [self.searchView.firstDateBtn setTitle:[[NSDate dateWithString:selectValue format:@"yyyy-MM-dd"] stringWithFormat:@"yyyy/MM/dd"] forState:UIControlStateNormal];
        [self.tableView.mj_header beginRefreshing];
    }];
}

- (void)SearchTopViewsecondDateButtonClick
{
    [self showDatePickerdefaultSelValue:nil minDate:[NSDate offsetDays:-8 fromDate:[NSDate dateUTC]] resultBlock:^(NSString *selectValue) {
        self.pages = 1;
        self.endDateStr = selectValue;
        [self.searchView.secondDateBtn setTitle:[[NSDate dateWithString:selectValue format:@"yyyy-MM-dd"] stringWithFormat:@"yyyy/MM/dd"] forState:UIControlStateNormal];
        [self.tableView.mj_header beginRefreshing];
    }];
}

- (void)SearchTopViewSearchButtonClick
{
    [UIView animateWithDuration:0.25 animations:^{
        self.selectView.transform = CGAffineTransformMakeTranslation(0, -HEIGHT);
    }];
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView registerClass:[LukeRechargeOrderCell class] forCellReuseIdentifier:LukeRechargeOrderCellID];
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedRowHeight = 80;
        _tableView.tableFooterView = [UIView new];
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.bottom.equalTo(self.view);
            make.top.equalTo(self.segmentControl.mas_bottom);
        }];
        _tableView.backgroundColor = [UIColor skinViewScreenColor];
    }
    return _tableView;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LukeRechargeOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:LukeRechargeOrderCellID];
    cell.item = self.dataList[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LukeRechargeOrderDescViewController *descVC = [[LukeRechargeOrderDescViewController alloc] init];
    descVC.item = self.dataList[indexPath.row];
    [self presentViewController:descVC animated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setupRefresh];
    [TIMER_MANAGER addDelegate:self];
    [TIMER_MANAGER addCycleTimerWithKey:USER_RECHARGEORDER andReduceScope:30];
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)cycleTimerWithKey:(NSString *)key RestCount:(NSInteger)count
{
    if ([key isEqualToString:USER_RECHARGEORDER]) {
        [self.tableView.mj_header beginRefreshing];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [TIMER_MANAGER removeDelegate:self];
    
    [TIMER_MANAGER removeTimerWithKey:USER_RECHARGEORDER];
}

- (void)dealloc
{
    [TIMER_MANAGER removeDelegate:self];
    
    [TIMER_MANAGER removeTimerWithKey:USER_RECHARGEORDER];
}

- (void)setupRefresh
{
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.pages = 1;
        [self queryData];
    }];
    [self.tableView.mj_header beginRefreshing];
    
    self.tableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        self.pages ++;
        [self queryData];
    }];
}

- (void)setupUI
{
    self.beginDateStr = [NSDate getNewTimeFormat:[NSDate ymdFormat]];
    self.endDateStr = self.beginDateStr;
    NSArray *titles = @[@"全部",@"已完成",@"失败"];
    HMSegmentedControl *segmentControl = [HMSegmentedControl createSegmentWithBackgroundColor:[UIColor blackColor] IndicatorColor:[UIColor redColor] selectedTitleColor:[UIColor redColor] titleColor:[UIColor whiteColor] titleArrs:titles addTarget:self action:@selector(segmentClick:)];
    [self.view addSubview:segmentControl];
    [segmentControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.searchView.mas_bottom);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    self.segmentControl = segmentControl;
}

- (void)segmentClick:(HMSegmentedControl *)segment
{
    NSInteger index = segment.selectedSegmentIndex;
    switch (index) {
        case 0:
            self.status = PersonalRechargeOrderStatusAll;
            break;
        case 1:
            self.status = PersonalRechargeOrderStatusFinish;
            break;
        case 2:
            self.status = PersonalRechargeOrderStatusLose;
            break;
        default:
            break;
    }
    self.pages = 1;
    [self.tableView.mj_header beginRefreshing];
}

- (void)queryData
{
    self.tableView.isShowEmpty = NO;
    [NET_DATA_MANAGER requestGetRechargeWithPage:self.pages Rows:10 dateFrom:self.beginDateStr dateTo:self.endDateStr mode:self.mode status:self.status Success:^(id responseObject) {
        if (self.pages == 1) {
            [self.dataList removeAllObjects];
        }
        [self.dataList addObjectsFromArray:[LukeRechargeOrderItem mj_objectArrayWithKeyValuesArray:responseObject[@"data"]]];
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        self.tableView.isShowEmpty = YES;
        self.tableView.titleForEmpty = @"无记录";
        self.tableView.descriptionForEmpty = @"您暂未提交充值申请";
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        self.tableView.isShowEmpty = YES;
        self.tableView.titleForEmpty = @"数据异常";
        self.tableView.descriptionForEmpty = @"请检查网络设置";
        NSLog(@"%@",error);
    }];
}

@end


