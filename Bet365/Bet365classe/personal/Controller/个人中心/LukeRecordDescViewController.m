//
//  LukeRecordDescViewController.m
//  Bet365
//
//  Created by luke on 17/6/22.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeRecordDescViewController.h"
#import "LukeRecordItem.h"
#import "AppDelegate.h"
#import "KFCPHomeGameJsonModel.h"
#import "LukeUserAdapter.h"
#import "UIColor+extenColor.h"

#define labelW  WIDTH / 5
@interface LukeRecordDescViewController ()
{
    UILabel *gameNameLb;
    UILabel *statusLb;
    UILabel *orderNoLb;
    UILabel *betInfoLb;
}
/**  */
@property (nonatomic,weak) UILabel *titleLb;
/** 底部view */
@property (nonatomic,strong) UIView *bottomView;
/**  */
@property (nonatomic,weak) UIScrollView *scrollView;

@end

@implementation LukeRecordDescViewController
- (UIView *)bottomView
{
    if (_bottomView == nil) {
        _bottomView = [[UIView alloc] init];
        _bottomView.backgroundColor = [UIColor blackColor];
        [self.scrollView addSubview:_bottomView];
        UILabel *lb1 = [self addLbWithFont:IS_IPHONE_5s ? 14 : 16 color:[UIColor whiteColor] title:@"奖级名称" textAlignment:NSTextAlignmentCenter];
        [_bottomView addSubview:lb1];
        UILabel *lb2 = [self addLbWithFont:IS_IPHONE_5s ? 14 : 16 color:[UIColor whiteColor] title:@"号码" textAlignment:NSTextAlignmentCenter];
        [_bottomView addSubview:lb2];
        UILabel *lb3 = [self addLbWithFont:IS_IPHONE_5s ? 14 : 16 color:[UIColor whiteColor] title:@"倍数" textAlignment:NSTextAlignmentCenter];
        [_bottomView addSubview:lb3];
        UILabel *lb4 = [self addLbWithFont:IS_IPHONE_5s ? 14 : 16 color:[UIColor whiteColor] title:@"奖金" textAlignment:NSTextAlignmentCenter];
        [_bottomView addSubview:lb4];
        UILabel *lb5 = [self addLbWithFont:IS_IPHONE_5s ? 14 : 16 color:[UIColor whiteColor] title:@"中奖金额" textAlignment:NSTextAlignmentCenter];
        [_bottomView addSubview:lb5];
        [_bottomView.subviews mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:0 leadSpacing:0 tailSpacing:0];
        [_bottomView.subviews mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_bottomView);
        }];
    }
    return _bottomView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.title = IS_IPHONE_5s ? @"" : @"注单详情";
    [self setupUI];
    [self setLastView];
}

- (void)setLastView
{
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.equalTo(self.scrollView);
        make.width.mas_equalTo(WIDTH);
        make.height.mas_equalTo(40);
        make.top.equalTo(self.titleLb.mas_bottom).offset(10);
    }];
    NSArray *oddsArr = [self.item.odds componentsSeparatedByString:@","];
    NSArray *oddsNameArr = [self.item.oddsName componentsSeparatedByString:@","];
    __block UIView *lastView = nil;
    [oddsArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIView *oddsView = [self createViewWithcateName:[self.item.oddsName containsString:@","] ? oddsNameArr[idx] : self.item.cateName betInfo:self.item.betInfo multiple:[LukeUserAdapter decimalNumberWithId:self.item.multiple] odds:[LukeUserAdapter formatFloat:oddsArr[idx]] money:[NSString stringWithFormat:@"%.2f",[oddsArr[idx] floatValue] * self.item.multiple]];
        [self.scrollView addSubview:oddsView];
        [oddsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(self.scrollView);
            make.width.mas_equalTo(WIDTH);
            if (lastView) {
                make.top.equalTo(lastView.mas_bottom).offset(5);
            }else{
                make.top.equalTo(self.bottomView.mas_bottom).offset(10);
            }
        }];
        lastView = oddsView;
    }];
    [lastView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.scrollView.mas_bottom);
    }];
}

- (UIView *)createViewWithcateName:(NSString *)cateName betInfo:(NSString *)betInfo multiple:(NSString *)multiple odds:(NSString *)odds money:(NSString *)money
{
    UIView *betView = [[UIView alloc] init];
    betView.backgroundColor = [UIColor whiteColor];
    UILabel *betInfoLabel = [self addLbWithFont:16 color:[UIColor blackColor] title:betInfo textAlignment:NSTextAlignmentCenter];
    betInfoLabel.numberOfLines = 0;
    [betView addSubview:betInfoLabel];
    [betInfoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(betView);
        make.leading.equalTo(betView).offset(labelW);
        make.width.mas_equalTo(labelW);
    }];
    
    UILabel *cateNameLabel = [self addLbWithFont:IS_IPHONE_5s ? 15 : 16 color:[UIColor blackColor] title:cateName textAlignment:NSTextAlignmentCenter];
    cateNameLabel.numberOfLines = 0;
    [betView addSubview:cateNameLabel];
    [cateNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(betInfoLabel);
        make.leading.equalTo(betView);
        make.width.mas_equalTo(labelW);
    }];
    
    UILabel *multipleLabel = [self addLbWithFont:IS_IPHONE_5s ? 15 : 16 color:[UIColor blackColor] title:multiple textAlignment:NSTextAlignmentCenter];
    [betView addSubview:multipleLabel];
    [multipleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(betInfoLabel);
        make.leading.equalTo(betInfoLabel.mas_trailing);
        make.width.mas_equalTo(labelW);
    }];
    
    UILabel *oddsLabel = [self addLbWithFont:IS_IPHONE_5s ? 15 : 16 color:[UIColor blackColor] title:odds textAlignment:NSTextAlignmentCenter];
    [betView addSubview:oddsLabel];
    [oddsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(betInfoLabel);
        make.leading.equalTo(multipleLabel.mas_trailing);
        make.width.mas_equalTo(labelW);
    }];
    
    UILabel *canWinMoneyLabel = [self addLbWithFont:IS_IPHONE_5s ? 15 : 16 color:[UIColor blackColor] title:money textAlignment:NSTextAlignmentCenter];
    [betView addSubview:canWinMoneyLabel];
    [canWinMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(betInfoLabel);
        make.leading.equalTo(oddsLabel.mas_trailing);
        make.width.mas_equalTo(labelW);
    }];
    
    return betView;
}

- (void)setupUI
{
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:scrollView];
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.leading.bottom.trailing.equalTo(self.view);
        make.width.mas_equalTo(WIDTH);
    }];
    self.scrollView = scrollView;
    
    orderNoLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:[NSString stringWithFormat:@"注单编号 %@",self.item.orderNo]];
    [orderNoLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(scrollView).offset(10);
        make.leading.equalTo(scrollView).offset(10);
    }];
    
    gameNameLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:nil];
    [gameNameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(orderNoLb.mas_bottom).offset(10);
        make.leading.equalTo(scrollView).offset(10);
    }];
    
    UILabel *timeLabel = [self addLabelWithFont:15 color:JesseGrayColor(77) title:self.item.addTime];
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(gameNameLb.mas_top);
        make.leading.equalTo(scrollView).offset(WIDTH * 0.5);
    }];
    
    UILabel *cateNameLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:[NSString stringWithFormat:@"玩法 %@",self.item.cateName]];
    [cateNameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(gameNameLb.mas_bottom).offset(10);
        make.leading.equalTo(scrollView).offset(10);
    }];
    
    UILabel *turnNumLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:[NSString stringWithFormat:@"奖期 %@",self.item.turnNum]];
    [turnNumLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cateNameLb.mas_top);
        make.leading.equalTo(scrollView).offset(WIDTH * 0.5);
    }];
    
    UILabel *totalMoneyLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:nil];
    totalMoneyLb.attributedText = [self getStrWithTitle:@"投注金额 " lastStr:[LukeUserAdapter formatFloat:[NSString stringWithFormat:@"%f",self.item.totalMoney]] color:[UIColor redColor]];
    [totalMoneyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cateNameLb.mas_bottom).offset(10);
        make.leading.equalTo(scrollView).offset(10);
    }];
    
    statusLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:nil];
    [statusLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(totalMoneyLb.mas_top);
        make.leading.equalTo(scrollView).offset(WIDTH * 0.5);
    }];
    
    UILabel *rewardLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:[NSString stringWithFormat:@"注单奖金 %@",[LukeUserAdapter formatFloat:[NSString stringWithFormat:@"%f",self.item.reward]]]];
    [rewardLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(totalMoneyLb.mas_bottom).offset(10);
        make.leading.equalTo(scrollView).offset(10);
    }];
    
    UILabel *accountLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:[NSString stringWithFormat:@"用户 %@",self.item.account]];
    [accountLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(rewardLb.mas_top);
        make.leading.equalTo(scrollView).offset(WIDTH * 0.5);
    }];
    
    UILabel *totalNumsLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:[NSString stringWithFormat:@"投注注数 %ld",self.item.totalNums]];
    [totalNumsLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(rewardLb.mas_bottom).offset(10);
        make.leading.equalTo(scrollView).offset(10);
    }];
    
    UILabel *multipleLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:nil];
    [multipleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(totalNumsLb.mas_top);
        make.leading.equalTo(scrollView).offset(WIDTH * 0.5);
    }];
    
    if (self.item.betModel == 0) {
        multipleLb.text = [NSString stringWithFormat:@"倍数模式 %@倍，元",[LukeUserAdapter formatFloat:[NSString stringWithFormat:@"%f",self.item.multiple]]];
    }else if (self.item.betModel == 1) {
        multipleLb.text = [NSString stringWithFormat:@"倍数模式 %@倍，角",[LukeUserAdapter formatFloat:[NSString stringWithFormat:@"%f",self.item.multiple]]];
    }else{
        multipleLb.text = [NSString stringWithFormat:@"倍数模式 %@倍，分",[LukeUserAdapter formatFloat:[NSString stringWithFormat:@"%f",self.item.multiple]]];
    }
    
    UILabel *rebateMoneyLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:[NSString stringWithFormat:@"动态返点 %@",[LukeUserAdapter formatFloat:[NSString stringWithFormat:@"%f",self.item.rebateMoney]]]];
    [rebateMoneyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(totalNumsLb.mas_bottom).offset(10);
        make.leading.equalTo(scrollView).offset(10);
    }];
    
    UILabel *openNumLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:self.item.openNum ? [NSString stringWithFormat:@"开奖号码 %@",self.item.openNum] : @"开奖号码"];
    openNumLb.preferredMaxLayoutWidth = WIDTH - 20;
    [openNumLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(rebateMoneyLb.mas_bottom).offset(10);
        make.leading.equalTo(scrollView).offset(10);
        make.trailing.equalTo(scrollView).offset(-10);
    }];
    
    betInfoLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:[NSString stringWithFormat:@"下注内容 %@%@",self.item.betInfo,self.item.poschooseName.length > 0 ? [NSString stringWithFormat:@"(%@)",self.item.poschooseName] : @""]];
    betInfoLb.numberOfLines = 0;
    [betInfoLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(openNumLb.mas_bottom).offset(10);
        make.leading.equalTo(scrollView).offset(10);
        make.trailing.equalTo(scrollView).offset(-10);
        make.width.mas_equalTo(WIDTH - 20);
    }];
    
    [self Getlottery];
    
    [self GetStatusForLabel];
    
    UIView *lineView = [self addLineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(betInfoLb.mas_bottom).offset(10);
        make.leading.trailing.equalTo(scrollView);
        make.height.mas_equalTo(1);
    }];
    
    UILabel *titleLb = [self addLabelWithFont:IS_IPHONE_5s ? 14 : 16 color:[UIColor blackColor] title:@"可能中奖情况"];
    titleLb.textAlignment = NSTextAlignmentCenter;
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineView.mas_bottom).offset(10);
        make.centerX.equalTo(scrollView);
    }];
    self.titleLb = titleLb;
    
}

#pragma mark - 彩种玩法的处理
- (void)Getlottery
{
    NSString *str = nil;
    for (KFCPHomeGameJsonModel *model in LOTTERY_FACTORY.lotteryData) {
        if (self.item.gameId == [model.GameId integerValue]) {
            str = model.name;
        }
    }
    if (self.item.model == 0) {
        gameNameLb.text = [NSString stringWithFormat:@"彩种 %@[官]",str];
    }else{
        gameNameLb.text = [NSString stringWithFormat:@"彩种 %@[信]",str];
    }
}

- (void)GetStatusForLabel
{
    if (self.item.status == 0 && [LukeUserAdapter timeCompareWithOtherTime:self.item.betEndTime] == NSOrderedAscending) {

        if (BET_CONFIG.config.hy_is_cancel && [BET_CONFIG.config.hy_is_cancel isEqualToString:@"1"]) {
            statusLb.attributedText = [self getStrWithTitle:@"状态 " lastStr:@"可撤销" color:[UIColor colorWithHexString:@"#2c77ba"]];
        }else{
            statusLb.attributedText = [self getStrWithTitle:@"状态 " lastStr:@"未开奖" color:[UIColor colorWithHexString:@"#e2bc03"]];
        }
    }else if (self.item.status == 1 && self.item.result == 0){
        statusLb.attributedText = [self getStrWithTitle:@"状态 " lastStr:@"未中奖" color:[UIColor colorWithHexString:@"#908e8e"]];
    }else if (self.item.status == 1 && self.item.result == 1){
        statusLb.attributedText = [self getStrWithTitle:@"状态 " lastStr:@"已派送" color:[UIColor colorWithHexString:@"#e71010"]];
    }else if (self.item.status == 1 && self.item.result == 2){
        statusLb.attributedText = [self getStrWithTitle:@"状态 " lastStr:@"和局" color:[UIColor colorWithHexString:@"#07d00b"]];
    }else if (self.item.status == 0){
        statusLb.attributedText = [self getStrWithTitle:@"状态 " lastStr:@"未开奖" color:[UIColor colorWithHexString:@"#e2bc03"]];
    }else if (self.item.status == 2){
        statusLb.attributedText = [self getStrWithTitle:@"状态 " lastStr:@"撤销" color:[UIColor colorWithHexString:@"#906944"]];
    }else if (self.item.status == 3){
        statusLb.attributedText = [self getStrWithTitle:@"状态 " lastStr:@"删除" color:[UIColor colorWithHexString:@"#b9cca6"]];
    }else if (self.item.status == 4){
        statusLb.attributedText = [self getStrWithTitle:@"状态 " lastStr:@"追号停止" color:[UIColor colorWithHexString:@"#b9cca6"]];
    }
}

#pragma mark -  富文本
- (NSMutableAttributedString *)getStrWithTitle:(NSString *)title lastStr:(NSString *)lastStr color:(UIColor *)color
{
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",title,lastStr]];
    [attStr setAttributes:@{NSForegroundColorAttributeName : color} range:NSMakeRange(title.length, lastStr.length)];
    return attStr;
}

//创建线条
- (UIView *)addLineView
{
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = JesseColor(228, 225, 225);
    [self.view addSubview:lineView];
    return lineView;
}

- (UILabel *)addLbWithFont:(CGFloat)font color:(UIColor *)color title:(NSString *)title textAlignment:(NSTextAlignment)textAlignment
{
    UILabel *label = [[UILabel alloc] init];
    label.textAlignment = textAlignment;
    label.text = title;
    label.textColor = color;
    label.font = [UIFont systemFontOfSize:font];
    return label;
}

//创建label
- (UILabel *)addLabelWithFont:(CGFloat)font color:(UIColor *)color title:(NSString *)title
{
    UILabel *label = [[UILabel alloc] init];
    label.numberOfLines = 0;
    label.text = title;
    label.textColor = color;
    label.font = [UIFont systemFontOfSize:font];
    [self.scrollView addSubview:label];
    return label;
}

@end
