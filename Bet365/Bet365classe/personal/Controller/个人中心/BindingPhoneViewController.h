//
//  BindingPhoneViewController.h
//  Bet365
//
//  Created by super_小鸡 on 2019/9/7.
//  Copyright © 2019 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, BindingPhoneType) {
    BindingPhoneType_CANCEL = 0,
    BindingPhoneType_MODIFY,
};

@interface BindingPhoneViewController : UIViewController

@property (nonatomic,assign) BindingPhoneType bindingType;

@end

NS_ASSUME_NONNULL_END
