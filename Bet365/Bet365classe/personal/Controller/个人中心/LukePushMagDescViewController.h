//
//  LukePushMagDescViewController.h
//  Bet365
//
//  Created by luke on 17/6/15.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LukePushMessageItem;
@class LukeNoticeItem;

@interface LukePushMagDescViewController : Bet365ViewController
/**  模型 */
@property (nonatomic,strong) LukePushMessageItem *item;
/** 公告模型 */
@property (nonatomic,strong) LukeNoticeItem *noticeItem;

@end
