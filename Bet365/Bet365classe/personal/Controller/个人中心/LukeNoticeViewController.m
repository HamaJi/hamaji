//
//  LukeNoticeViewController.m
//  Bet365
//
//  Created by luke on 17/6/15.
//  Copyright © 2017年 jesse. All rights reserved.
//游戏公告

#import "LukeNoticeViewController.h"
#import "LukeNoticeItem.h"
#import "LukeNoticeCell.h"
#import "LukePushMagDescViewController.h"
#import "NetWorkMannager+Account.h"

static NSString * const LukeNoticeCellID = @"LukeNoticeCell";

@interface LukeNoticeViewController ()
<UITableViewDelegate,
UITableViewDataSource,
UIViewControllerSerializing>
/** 获取的数据数组 */
@property (nonatomic,strong) NSArray *noticeDatas;
/**  */
@property (nonatomic,strong) UITableView *tableView;

@end

@implementation LukeNoticeViewController

RouterKey *const kRouterNotice = @"notice";

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    return [USER_DATA_MANAGER shouldPush2Login];
}
+(void)load{
    [self registerJumpRouterKey:kRouterNotice toHandle:^(NSDictionary *parameters) {
        LukeNoticeViewController *vc = [[LukeNoticeViewController alloc] init];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }];
    
    [self registerInstanceRouterKey:kRouterNotice toHandle:^UIViewController *(NSDictionary *parameters) {
        LukeNoticeViewController *vc = [[LukeNoticeViewController alloc] init];
        return vc;
    }];
}
- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.backgroundColor = [UIColor skinViewScreenColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _tableView.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0);
        [_tableView registerClass:[LukeNoticeCell class] forCellReuseIdentifier:LukeNoticeCellID];
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        _tableView.titleForEmpty = @"暂无公告";
    }
    return _tableView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    [self queryData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismissWithCompletion:nil];
}

- (void)queryData
{
    self.tableView.isShowEmpty = NO;
    [SVProgressHUD showWithStatus:@"获取数据中..."];
    [NET_DATA_MANAGER requestGetNoticeListSuccess:^(id responseObject) {
        NSMutableArray *tempArr = [NSMutableArray arrayWithArray:[LukeNoticeItem mj_objectArrayWithKeyValuesArray:responseObject]];
        NSArray *noticeArr = [NSArray arrayWithArray:tempArr];
        for (LukeNoticeItem *item in noticeArr) {
            if (item.status == 1) {
                [tempArr removeObject:item];
            }
        }
        self.noticeDatas = tempArr;
        [self.tableView reloadData];
        [SVProgressHUD dismiss];
        self.tableView.isShowEmpty = YES;
    } failure:^(NSError *error) {
        NSLog(@"%@",error.msg);
        [SVProgressHUD dismiss];
        self.tableView.isShowEmpty = YES;
    }];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.noticeDatas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LukeNoticeCell *cell = [tableView dequeueReusableCellWithIdentifier:LukeNoticeCellID];
    cell.item = self.noticeDatas[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LukePushMagDescViewController *VC = [[LukePushMagDescViewController alloc] init];
    VC.noticeItem = self.noticeDatas[indexPath.row];
    [self.navigationController pushViewController:VC animated:YES];
    VC.hidesBottomBarWhenPushed = YES;
}

@end
