//
//  LukeRechargeOrderDescViewController.h
//  Bet365
//
//  Created by luke on 2017/11/21.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LukeRechargeOrderItem;
@interface LukeRechargeOrderDescViewController : Bet365ViewController
/** 模型 */
@property (nonatomic,strong) LukeRechargeOrderItem *item;

@end
