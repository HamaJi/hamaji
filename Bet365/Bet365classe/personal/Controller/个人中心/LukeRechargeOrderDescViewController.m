//
//  LukeRechargeOrderDescViewController.m
//  Bet365
//
//  Created by luke on 2017/11/21.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeRechargeOrderDescViewController.h"
#import "LukeRechargeOrderItem.h"
#import "NetWorkMannager+Account.h"

@interface RechargeOrderDiscountTypeList : NSObject

@property (nonatomic,assign) NSInteger orderId;

@property (nonatomic,copy) NSString *addTime;

@property (nonatomic,copy) NSString *name;

@property (nonatomic,copy) NSString *updateTime;
@property (nonatomic,copy) NSString *remarks;
@property (nonatomic,assign) NSInteger dataType;
@property (nonatomic,assign) NSInteger rechargeFlag;
@property (nonatomic,assign) NSInteger value;
@property (nonatomic,assign) NSInteger sort;
@property (nonatomic,assign) NSInteger status;

@end

@implementation RechargeOrderDiscountTypeList

+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"orderId" : @"id"};
}

@end

@interface LukeRechargeOrderDescViewController ()
/**  */
@property (nonatomic,weak) UIView *topView;
/**  */
@property (nonatomic,strong) NSArray *orderArrs;

@end

@implementation LukeRechargeOrderDescViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupTopView];
    
    [self queryData];
}

- (void)queryData
{
    [SVProgressHUD showWithStatus:@"获取数据中..."];
    [NET_DATA_MANAGER requestGetRechargeDiscountTypeListSuccess:^(id responseObject) {
        [SVProgressHUD dismiss];
        self.orderArrs = [RechargeOrderDiscountTypeList mj_objectArrayWithKeyValuesArray:responseObject];
        [self setupUI];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [SVProgressHUD dismiss];
    }];
}

- (void)setupTopView
{
    self.view.backgroundColor = YCZGrayColor(238);
    UIView *topView = [[UIView alloc] init];
    topView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.top.equalTo(self.view);
        make.height.mas_equalTo(40 + statusHeight);
    }];
    self.topView = topView;
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"left_lage"] forState:UIControlStateNormal];
    [backBtn setTitle:@"返回" forState:UIControlStateNormal];
    [backBtn setTitleColor:YCZColor(80, 89, 99) forState:UIControlStateNormal];
    [backBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:backBtn];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(topView).offset(40 * 0.2);
        make.leading.equalTo(topView).offset(10);
    }];
    
    UILabel *titleLb = [[UILabel alloc] init];
    titleLb.text = @"充值详情";
    titleLb.textColor = [UIColor blackColor];
    [topView addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(topView);
        make.centerY.equalTo(backBtn);
    }];
}

- (void)backClick
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setupUI
{
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.backgroundColor = YCZGrayColor(238);
    [self.view addSubview:scrollView];
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.bottom.equalTo(self.view);
        make.width.mas_equalTo(WIDTH);
        make.top.equalTo(self.topView.mas_bottom);
    }];
    
    UILabel *titleLb = [[UILabel alloc] init];
    titleLb.text = @"订单信息";
    titleLb.font = [UIFont systemFontOfSize:IS_IPHONE_5s ? 14 : 16];
    titleLb.textColor = YCZColor(80, 89, 99);
    [scrollView addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(scrollView).offset(10);
        make.leading.equalTo(scrollView).offset(10);
    }];
    
    UIView *orderNoView = [self createViewWithTitle:@"订单号" subTitle:self.item.orderNo color:YCZColor(80, 89, 99)];
    [scrollView addSubview:orderNoView];
    [orderNoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLb.mas_bottom).offset(8);
        make.leading.trailing.equalTo(scrollView);
        make.width.mas_equalTo(WIDTH);
        make.height.mas_equalTo(40);
    }];
    
    UIView *amountView = [self createViewWithTitle:@"充值金额" subTitle:[NSString stringWithFormat:@"%@ 元",StringFormatWithFloat(self.item.amount)] color:YCZColor(80, 89, 99)];
    [scrollView addSubview:amountView];
    [amountView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(orderNoView.mas_bottom);
        make.leading.trailing.equalTo(scrollView);
        make.width.mas_equalTo(WIDTH);
        make.height.mas_equalTo(40);
    }];
    
    NSString *statusStr = nil;
    UIColor *color = nil;
    if (self.item.status == 1 || self.item.status == 2) {
        statusStr = @"受理中";
        color = [LukeUserAdapter colorWithHexString:@"#07d00b"];
    }else if (self.item.status == 3){
        statusStr = @"已入款";
        color = [LukeUserAdapter colorWithHexString:@"#e71010"];
    }else{
        statusStr = @"已取消";
        color = [LukeUserAdapter colorWithHexString:@"#908e8e"];
    }
    UIView *statusView = [self createViewWithTitle:@"状态" subTitle:statusStr color:color];
    [scrollView addSubview:statusView];
    [statusView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(amountView.mas_bottom);
        make.leading.trailing.equalTo(scrollView);
        make.width.mas_equalTo(WIDTH);
        make.height.mas_equalTo(40);
    }];
    
    UIView *discountAmountView = [self createViewWithTitle:@"优惠金额" subTitle:[NSString stringWithFormat:@"%@ 元",StringFormatWithFloat(self.item.discountAmount)] color:YCZColor(80, 89, 99)];
    [scrollView addSubview:discountAmountView];
    [discountAmountView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(statusView.mas_bottom);
        make.leading.trailing.equalTo(scrollView);
        make.width.mas_equalTo(WIDTH);
        make.height.mas_equalTo(40);
    }];
    
    __block NSString *discountType = nil;
    [self.orderArrs enumerateObjectsUsingBlock:^(RechargeOrderDiscountTypeList * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (self.item.discountType && [self.item.discountType integerValue] == obj.value) {
            discountType = obj.name;
            *stop = YES;
        }
    }];
    UIView *payTypeNameView = [self createViewWithTitle:@"优惠类型" subTitle:discountType ? discountType : @"" color:YCZColor(80, 89, 99)];
    [scrollView addSubview:payTypeNameView];
    [payTypeNameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(discountAmountView.mas_bottom);
        make.leading.trailing.equalTo(scrollView);
        make.width.mas_equalTo(WIDTH);
        make.height.mas_equalTo(40);
    }];
    
    UIView *totalAmountView = [self createViewWithTitle:@"充值总金额" subTitle:[NSString stringWithFormat:@"%@ 元",StringFormatWithFloat(self.item.totalAmount)] color:YCZColor(80, 89, 99)];
    [scrollView addSubview:totalAmountView];
    [totalAmountView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(payTypeNameView.mas_bottom);
        make.leading.trailing.equalTo(scrollView);
        make.width.mas_equalTo(WIDTH);
        make.height.mas_equalTo(40);
    }];
    
    NSString *mode = nil;
    if (self.item.mode == 1) {
        mode = @"转账汇款";
    }else if (self.item.mode == 2){
        mode = @"在线支付";
    }else if (self.item.mode == 3){
        mode = @"人工充值";
    }else{
        mode = @"后台转入(非正常入款)";
    }
    UIView *modeView = [self createViewWithTitle:@"充值模式" subTitle:mode color:YCZColor(80, 89, 99)];
    [scrollView addSubview:modeView];
    [modeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(totalAmountView.mas_bottom);
        make.leading.trailing.equalTo(scrollView);
        make.width.mas_equalTo(WIDTH);
        make.height.mas_equalTo(40);
    }];
    UIView *payTypeView = [self createViewWithTitle:@"支付类型" subTitle:self.item.payTypeName color:YCZColor(80, 89, 99)];
    [scrollView addSubview:payTypeView];
    [payTypeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(modeView.mas_bottom);
        make.leading.trailing.equalTo(scrollView);
        make.width.mas_equalTo(WIDTH);
        make.height.mas_equalTo(40);
    }];
    UIView *remarksView = [self createViewWithTitle:@"备注" subTitle:self.item.remarks.length > 0 ? self.item.remarks : @"" color:YCZColor(80, 89, 99)];
    [scrollView addSubview:remarksView];
    [remarksView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(payTypeView.mas_bottom);
        make.leading.trailing.equalTo(scrollView);
        make.width.mas_equalTo(WIDTH);
        make.height.mas_equalTo(40);
    }];
    
    UIView *addTimeView = [self createViewWithTitle:@"订单时间" subTitle:self.item.addTime color:YCZColor(80, 89, 99)];
    [scrollView addSubview:addTimeView];
    [addTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(remarksView.mas_bottom);
        make.leading.trailing.equalTo(scrollView);
        make.width.mas_equalTo(WIDTH);
        make.height.mas_equalTo(40);
    }];
    UIView *timeStatusView = [self createViewWithTitle:self.item.status == 4 ? @"取消时间" : @"入款时间" subTitle:self.item.auditTime color:YCZColor(80, 89, 99)];
    [scrollView addSubview:timeStatusView];
    [timeStatusView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(addTimeView.mas_bottom);
        make.leading.trailing.equalTo(scrollView);
        make.width.mas_equalTo(WIDTH);
        make.height.mas_equalTo(40);
        make.bottom.equalTo(scrollView);
    }];
}

- (UIView *)createViewWithTitle:(NSString *)title subTitle:(NSString *)subTitle color:(UIColor *)color {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *titleLb = [[UILabel alloc] init];
    titleLb.text = title;
    titleLb.font = [UIFont systemFontOfSize:IS_IPHONE_5s ? 14 : 16];
    titleLb.textColor = [UIColor blackColor];
    [view addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(view);
        make.leading.equalTo(view).offset(10);
    }];
    [titleLb setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
    [titleLb setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
    
    UILabel *subTitleLb = [[UILabel alloc] init];
    subTitleLb.numberOfLines = 0;
    subTitleLb.text = subTitle;
    subTitleLb.textColor = color;
    subTitleLb.textAlignment = NSTextAlignmentRight;
    subTitleLb.font = [UIFont systemFontOfSize:IS_IPHONE_5s ? 14 : 16];
    [view addSubview:subTitleLb];
    [subTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(view);
        make.left.equalTo(titleLb.mas_right).offset(10);
        make.trailing.equalTo(view).offset(-10);
    }];
    [subTitleLb setContentHuggingPriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
    [subTitleLb setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
    
    
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = YCZGrayColor(160);
    [view addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.trailing.equalTo(view);
        make.leading.equalTo(view).offset(10);
        make.height.mas_equalTo(1);
    }];
    return view;
}

@end
