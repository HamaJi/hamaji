//
//  LukeUserDrawViewController.m
//  Bet365
//
//  Created by luke on 17/6/20.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeUserDrawViewController.h"
#import "LukeUserAdapter.h"
#import "LukeRegisterLbTFView.h"
#import "NetWorkMannager+Account.h"
#import "Bet365UserDrawModel.h"
#import "LukeMaskView.h"

@interface LukeUserDrawViewController ()<UITextFieldDelegate>
/** 取款密码 */
@property (nonatomic,strong) LukeRegisterLbTFView *passwordView;
/** 取款金额 */
@property (nonatomic,strong) LukeRegisterLbTFView *moneyView;
/** 验证码 */
@property (nonatomic,strong) LukeRegisterLbTFView *VerifyView;

@property (nonatomic,strong) LukeRegisterLbTFView *memoView;
/** 取款金额 */
@property (nonatomic,assign) NSInteger getMoneyF;
/** 手续费 */
@property (nonatomic,assign) CGFloat feeF;
/** 实收金额 */
@property (nonatomic,assign) CGFloat realMoney;
/**  */
@property (nonatomic,weak) UILabel *getMoneyLb;
/** 模型 */
@property (nonatomic,strong) Bet365UserDrawModel *model;

@end

@implementation LukeUserDrawViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}
- (void)getDrawMessage:(Bet365UserDrawModel *)userModel{
    self.model = userModel;
    [self setupUI];
}
- (void)queryData
{
    [SVProgressHUD showWithStatus:@"获取数据中..."];
    [NET_DATA_MANAGER requestGetQueryOutMoneyIndexSuccess:^(id responseObject) {
        self.model = [Bet365UserDrawModel mj_objectWithKeyValues:responseObject];
        [SVProgressHUD dismiss];
        [self setupUI];
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

- (void)setupUI
{
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.backgroundColor = [UIColor skinViewScreenColor];
    [self.view addSubview:scrollView];
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
        make.width.mas_equalTo(WIDTH);
    }];
    
    UILabel *todayLabel = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:nil];
    [scrollView addSubview:todayLabel];
    [todayLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(scrollView).offset(10);
        make.leading.equalTo(scrollView).offset(10);
    }];
    
    todayLabel.attributedText = [self dealWithTitle:@"今日已提款" middleNum:self.model.count ? [self.model.count stringValue] : @"0" middleTitle:@"次,总计" endNum:self.model.sumMoney ? [self.model.sumMoney stringValue] : @"0" endTitle:@"元"];
    
    LukeRegisterLbTFView *deductView = [LukeRegisterLbTFView registerLbBtnViewWithTitle:@"当前余额" btnImage:nil btnTitle:[NSString roundingWithFloat:USER_DATA_MANAGER.userInfoData.money]];
    [scrollView addSubview:deductView];
    [deductView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(todayLabel.mas_bottom).offset(5);
        make.leading.trailing.equalTo(scrollView);
        make.size.mas_equalTo(CGSizeMake(WIDTH, 40));
    }];
    
    UILabel *accountLabel = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:@"绑定的帐户"];
    [scrollView addSubview:accountLabel];
    [accountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(deductView.mas_bottom).offset(10);
        make.leading.equalTo(scrollView).offset(10);
    }];
    
    UIView *bankView = [[UIView alloc] init];
    bankView.backgroundColor = [UIColor skinViewBgColor];
    [scrollView addSubview:bankView];
    
    UILabel *bankNameLabel = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:USER_DATA_MANAGER.userInfoData.userBank.bankName];
    [bankView addSubview:bankNameLabel];
    [bankNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bankView).offset(10);
        make.leading.equalTo(bankView).offset(10);
    }];
    
    UILabel *cardNoLabel = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:USER_DATA_MANAGER.userInfoData.userBank.cardNo];
    [bankView addSubview:cardNoLabel];
    [cardNoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bankNameLabel.mas_bottom).offset(10);
        make.leading.equalTo(bankView).offset(10);
    }];
    
    UILabel *subAddressLabel = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:USER_DATA_MANAGER.userInfoData.userBank.subAddress];
    [bankView addSubview:subAddressLabel];
    [subAddressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cardNoLabel.mas_bottom).offset(10);
        make.leading.equalTo(bankView).offset(10);
        make.bottom.equalTo(bankView.mas_bottom).offset(-10);
    }];
    [bankView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(accountLabel.mas_bottom).offset(5);
        make.leading.trailing.equalTo(scrollView);
        make.width.mas_equalTo(WIDTH);
    }];
    
    UILabel *getMoneyTitleLb = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:@"取款金额"];
    [scrollView addSubview:getMoneyTitleLb];
    [getMoneyTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bankView.mas_bottom).offset(10);
        make.left.equalTo(scrollView).offset(10);
    }];
    
    UILabel *getMoneyLb = [UILabel addLabelWithFont:16 color:[UIColor skinTextItemNorSubColor] title:nil];
    getMoneyLb.numberOfLines = 0;
    [scrollView addSubview:getMoneyLb];
    [getMoneyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(getMoneyTitleLb.mas_bottom);
        make.leading.equalTo(scrollView).offset(10);
        make.trailing.equalTo(scrollView).offset(-10);
    }];
    self.getMoneyF = 0;
    self.getMoneyLb = getMoneyLb;
    [self dealMoney];
    
    LukeRegisterLbTFView *moneyView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"取款金额" textEntry:NO placeholder:nil keyboardType:UIKeyboardTypeNumberPad];
    moneyView.textfield.delegate = self;
    [scrollView addSubview:moneyView];
    [moneyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(getMoneyLb.mas_bottom).offset(10);
        make.leading.trailing.equalTo(scrollView);
        make.size.mas_equalTo(CGSizeMake(WIDTH, 40));
    }];
    self.moneyView = moneyView;
    
    LukeRegisterLbTFView *passwordView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"取款密码" textEntry:YES placeholder:@"4位纯数字" keyboardType:UIKeyboardTypeNumberPad];
    passwordView.textfield.delegate = self;
    [scrollView addSubview:passwordView];
    [passwordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(moneyView.mas_bottom);
        make.leading.trailing.equalTo(scrollView);
        make.size.mas_equalTo(CGSizeMake(WIDTH, 40));
    }];
    self.passwordView = passwordView;
    
    if ([self.model.limit.vCode integerValue] == 1) {
        LukeRegisterLbTFView *VerifyView = [LukeRegisterLbTFView registerLbBtnTextFieldViewWithTitle:@"验证码" VerificationBtnImage:@"vaid" target:self action:@selector(VerifyClick)];
        [scrollView addSubview:VerifyView];
        VerifyView.textfield.delegate = self;
        [VerifyView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(self.view);
            make.top.equalTo(passwordView.mas_bottom);
            make.height.mas_equalTo(40);
        }];
        self.VerifyView = VerifyView;
        [self queryVaildData];//验证码获取
    }
    
    if ([self.model.limit.userMemo integerValue] > 0) {
        self.memoView = [LukeRegisterLbTFView registerCreateTextViewWithTitle:nil placeholder:[self.model.limit.userMemo integerValue] == 2 ? @"取款备注(*必填)" : @"取款备注(*选填)" MaxLength:100 ExceedLengthHandle:^{
            
        }];
        [scrollView addSubview:self.memoView];
        [self.memoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(self.view);
            if ([self.model.limit.vCode integerValue] == 1){
                make.top.equalTo(self.VerifyView.mas_bottom);
            }else{
                make.top.equalTo(passwordView.mas_bottom);
            }
            
            make.height.mas_equalTo(100);
            
        }];
    }
    
    UILabel *limitLabel = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:nil];
    [scrollView addSubview:limitLabel];
    [limitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        if ([self.model.limit.userMemo integerValue] > 0){
            make.top.equalTo(self.memoView.mas_bottom).offset(10);
        }else if ([self.model.limit.vCode integerValue] == 1){
            make.top.equalTo(self.VerifyView.mas_bottom).offset(10);
        }else{
            make.top.equalTo(passwordView.mas_bottom).offset(10);
        }
        make.leading.equalTo(scrollView).offset(10);
    }];
    limitLabel.attributedText = [self dealWithTitle:@"出款上限:" middleNum:[self.model.limit.singleMaxLimit stringValue] middleTitle:@"出款下限:" endNum:[self.model.limit.singleMinLimit stringValue] endTitle:nil];
    
    UILabel *totalLabel = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:nil];
    [scrollView addSubview:totalLabel];
    [totalLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(limitLabel.mas_bottom).offset(10);
        make.leading.equalTo(scrollView).offset(10);
    }];
    
    totalLabel.attributedText = [NSMutableAttributedString setTitle:@"当天提现的总额度为" colorTitle:[self.model.limit.dayMaxLimit stringValue] color:[UIColor redColor]];
    
    if (!self.model.limit.dayMaxCount || [self.model.limit.dayMaxCount integerValue] != 0) {
        UILabel *maxLabel = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:nil];
        maxLabel.numberOfLines = 2;
        [scrollView addSubview:maxLabel];
        [maxLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(totalLabel.mas_bottom).offset(10);
            make.leading.equalTo(scrollView).offset(10);
            make.trailing.equalTo(scrollView).offset(-10);
            make.width.mas_equalTo(WIDTH  - 20);
        }];
        NSString *counterFee = nil;
        if (self.model.limit.counterFeeMode) {
            counterFee = [self.model.limit.counterFeeMode integerValue] == 0 ? (self.model.limit.counterFee ? [self.model.limit.counterFee stringValue] : @"0") : [NSString stringWithFormat:@"取现金额的%@%%",(self.model.limit.counterFee ? [self.model.limit.counterFee stringValue] : @"0")];
        }else{
            counterFee = @"0";
        }
        maxLabel.attributedText = [self dealWithTitle:@"如果你当天提现次数超过" middleNum:self.model.limit.dayMaxCount middleTitle:@"次,则每次提现加收手续费,手续费的金额为" endNum:counterFee endTitle:nil];
    }
    
    UIButton *sureBtn = [UIButton addButtonWithTitle:@"确认" font:15 color:[UIColor whiteColor] target:self action:@selector(sureClick:)];
    [scrollView addSubview:sureBtn];
    [sureBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];
    sureBtn.layer.cornerRadius = 5;
    sureBtn.layer.masksToBounds = YES;
    [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(totalLabel.mas_bottom).offset(60);
        make.centerX.equalTo(scrollView);
        make.size.mas_equalTo(CGSizeMake(WIDTH - 20, 40));
        make.bottom.equalTo(scrollView).offset(-300);
    }];
}
- (void)VerifyClick
{
    [self.VerifyView.textfield setText:@""];
    [self queryVaildData];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.passwordView.textfield) {
        return textField.text.length - range.length + string.length < 5;
    }else if (textField == self.moneyView.textfield) {
        if ([LukeUserAdapter validateNumber:string] && string.length > 0) {
            if (self.getMoneyF > 0) {
                self.getMoneyF *= 10;
                self.getMoneyF += [string integerValue];
            }else{
                self.getMoneyF = [string integerValue];
            }
            [self dealMoney];
            return YES;
        }else{
            self.getMoneyF /= 10;
            self.moneyView.textfield.text = self.getMoneyF == 0 ? nil : [NSString stringWithFormat:@"%ld",self.getMoneyF];
            [self dealMoney];
            return NO;
        }
    }else{
        return YES;
    }
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if (textField == self.moneyView.textfield) {
        self.getMoneyF = 0;
        [self dealMoney];
    }
    return YES;
}

- (void)dealMoney
{
    if ([self.model.count integerValue] >= [self.model.limit.dayMaxCount integerValue] && self.getMoneyF > 0) {
        if (!self.model.limit.counterFeeMode || [self.model.limit.counterFeeMode integerValue] == 0) {
            self.feeF = self.model.limit.counterFee ? [self.model.limit.counterFee floatValue] : 0;
        }else{
            self.feeF = self.getMoneyF * (self.model.limit.counterFee ? [self.model.limit.counterFee floatValue] : 0) * 0.01;
        }
    }else{
        self.feeF = 0;
    }
    if (!self.model.limit.dayMaxCount || [self.model.limit.dayMaxCount integerValue] == 0) {
        self.feeF = 0;
    }
    self.realMoney = self.getMoneyF - self.feeF - [self.model.sumDeductMoney floatValue];
    self.getMoneyLb.attributedText = [self dealMoneyWithGetMoney:[NSString stringWithFormat:@"%ld元",self.getMoneyF] feeTitle:@"-手续费" fee:[NSString stringWithFormat:@"%.2f元",self.feeF] dmlTitle:@"-打码量费用" dml:[NSString stringWithFormat:@"%.2f元",[self.model.sumDeductMoney floatValue]] realMoney:@"=实收金额" money:[NSString stringWithFormat:@"%.2f元",self.realMoney]];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
//    if (textField == self.VerifyView.textfield) {
//        [self queryVaildData];
//    }焦点文本获取二维码取消
}

- (void)queryVaildData
{
    [SVProgressHUD showWithStatus:@"验证码获取中..."];
    [NET_DATA_MANAGER requestGetVuserCodeSuccess:^(id responseObject) {
        [self.VerifyView.dateBtn setBackgroundImage:responseObject forState:UIControlStateNormal];
        [SVProgressHUD dismiss];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [SVProgressHUD dismiss];
    }];
}

#pragma mark - 转换富文本
- (NSMutableAttributedString *)dealMoneyWithGetMoney:(NSString *)getMoney feeTitle:(NSString *)feetitle fee:(NSString *)fee dmlTitle:(NSString *)dmlTitle dml:(NSString *)dml realMoney:(NSString *)realMoney money:(NSString *)money
{
    NSString *title = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",getMoney,feetitle,fee,dmlTitle,dml,realMoney,money];
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:title];
    [attStr setAttributes:@{NSForegroundColorAttributeName : [UIColor redColor]} range:NSMakeRange(0, getMoney.length)];
    [attStr setAttributes:@{NSForegroundColorAttributeName : [UIColor redColor]} range:NSMakeRange(getMoney.length + feetitle.length, fee.length)];
    [attStr setAttributes:@{NSForegroundColorAttributeName : [UIColor redColor]} range:NSMakeRange(getMoney.length + feetitle.length + fee.length + dmlTitle.length, dml.length)];
    [attStr setAttributes:@{NSForegroundColorAttributeName : [UIColor redColor]} range:NSMakeRange(title.length - money.length, money.length)];
    return attStr;
}
- (NSMutableAttributedString *)dealWithTitle:(NSString *)title middleNum:(NSString *)middleNum middleTitle:(NSString *)middleTitle endNum:(NSString *)endNum endTitle:(NSString *)endTitle
{
    NSString *midStr = [NSString stringWithFormat:@"%@",middleNum];
    NSString *str = [NSString stringWithFormat:@"%@%@",title,middleNum];
    if (middleTitle) {
        str = [NSString stringWithFormat:@"%@%@",str,middleTitle];
    }
    if (endNum) {
        str = [NSString stringWithFormat:@"%@%@",str,endNum];
    }
    if (endTitle) {
        str = [NSString stringWithFormat:@"%@%@",str,endTitle];
    }
    
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:str];
    [attStr setAttributes:@{NSForegroundColorAttributeName : [UIColor redColor]} range:NSMakeRange(title.length, midStr.length)];
    if (middleTitle) {
        NSString *middleStr = [NSString stringWithFormat:@"%@%@%@",title,middleNum,middleTitle];
        [attStr setAttributes:@{NSForegroundColorAttributeName : [UIColor redColor]} range:NSMakeRange(middleStr.length, endNum.length)];
    }
    return attStr;
}

- (void)sureClick:(UIButton *)sender
{
    if (self.moneyView.textfield.text.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"取款金额必填哦"];
        return;
    }else if (![self.moneyView.textfield.text isValidateNumber]){
        [SVProgressHUD showErrorWithStatus:@"取款金额只能为整数"];
        return;
    }
    
    if (self.passwordView.textfield.text.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"取款密码必填哦"];
        return;
    }
    if ([self.model.limit.vCode integerValue] == 1 && self.VerifyView.textfield.text.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"验证码必填哦"];
        return;
    }
    if (![LukeUserAdapter lastTriggerDateOfObject:sender greatThanInterval:1.5]) {
        return;
    }
    if ([self.model.limit.userMemo integerValue] == 2 && !self.memoView.textfield.text.length) {
        [SVProgressHUD showErrorWithStatus:@"取款备注必填哦"];
        return;
    }
    [SVProgressHUD showWithStatus:@"提款中..."];
    [NET_DATA_MANAGER requestPostMoneyIndexWithcashMoney:self.moneyView.textfield.text cashPassword:self.passwordView.textfield.text yzmNum:self.VerifyView.textfield.text UserMemo:self.memoView.textfield.text Success:^(id responseObject) {
        [SVProgressHUD dismiss];
        NSString *notice = nil;
        if (self.model.limit.withDrawNotice && [NSString noNilWithString:self.model.limit.withDrawNotice].length > 0) {
            notice = [NSString stringWithFormat:@"%@",self.model.limit.withDrawNotice];
        }else{
            notice = @"提交成功，请你三分钟之内与客服人员联系！";
        }
        [LukeMaskView lukeMaskViewWithTitle:notice];
        [self.navigationController popToRootViewControllerAnimated:YES];
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"%@",error);
//        [SVProgressHUD showErrorWithStatus:error.msg];
    }];
}

@end

