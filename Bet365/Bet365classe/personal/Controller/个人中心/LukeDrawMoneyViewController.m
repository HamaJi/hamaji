//
//  LukeDrawMoneyViewController.m
//  Bet365
//
//  Created by luke on 17/6/15.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeDrawMoneyViewController.h"
#import "LukeBankInformationItem.h"
#import "LukeUserAdapter.h"
#import "SportsModuleShare.h"
#import "Bet365UserDrawModel.h"

@interface LukeDrawMoneyViewController ()
<UITextFieldDelegate>
/** 银行信息数组 */
@property (nonatomic,strong) NSMutableArray *bankArrs;
/**  */
@property (nonatomic,strong) Bet365UserDrawModel *model;

@property (weak, nonatomic) IBOutlet UILabel *titileLabel;

@property (weak, nonatomic) IBOutlet UILabel *head0Laebl;
@property (weak, nonatomic) IBOutlet UILabel *head1Laebl;
@property (weak, nonatomic) IBOutlet UILabel *head2Laebl;

@property (weak, nonatomic) IBOutlet UIView *content0View;
@property (weak, nonatomic) IBOutlet UIView *content1View;
@property (weak, nonatomic) IBOutlet UIView *content2View;


@property (weak, nonatomic) IBOutlet UILabel *realNameLeadingLabel;
@property (weak, nonatomic) IBOutlet UITextField *realNameTextF;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *realNameViewHeight;

@property (weak, nonatomic) IBOutlet UILabel *bankBtnLeadingLabel;
@property (weak, nonatomic) IBOutlet UIButton *bankBtn;

@property (weak, nonatomic) IBOutlet UILabel *bankCardLeadingLabel;
@property (weak, nonatomic) IBOutlet UITextField *bankCardTextF;

@property (weak, nonatomic) IBOutlet UILabel *pswLeadingLabel;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextF;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passwordView;

@property (weak, nonatomic) IBOutlet UILabel *bankRegionLeadingLabel;
@property (weak, nonatomic) IBOutlet UIButton *bankRegionBtn;

@property (weak, nonatomic) IBOutlet UILabel *bankDetailLeadingLabel;
@property (weak, nonatomic) IBOutlet UITextField *bankDetailTextF;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bankDetailViewHeight;

@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *conView;

@end

@implementation LukeDrawMoneyViewController

- (NSMutableArray *)bankArrs
{
    if (_bankArrs == nil) {
        _bankArrs = [NSMutableArray array];
    }
    return _bankArrs;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.realNameTextF.text = USER_DATA_MANAGER.userInfoData.fullName;
    self.realNameTextF.enabled = ![USER_DATA_MANAGER.userInfoData.fullName isNoBlankString];
    if (self.type == DrawBankCardType_Add) {
        self.passwordView.constant = 0;
    }
    [self getLimit];
//    self.view.backgroundColor = [UIColor skinViewScreenColor];
//    self.scrollView.backgroundColor = [UIColor skinViewScreenColor];
//    self.conView.backgroundColor = [UIColor skinViewScreenColor];
    self.titileLabel.textColor = [UIColor skinViewKitSelColor];
//    self.head0Laebl.textColor = [UIColor skinTextItemNorSubColor];
//    self.head1Laebl.textColor = [UIColor skinTextItemNorSubColor];
//    self.head2Laebl.textColor = [UIColor skinTextItemNorSubColor];
//
//    self.content0View.backgroundColor = [UIColor skinViewBgColor];
//    self.content1View.backgroundColor = [UIColor skinViewBgColor];
//    self.content2View.backgroundColor = [UIColor skinViewBgColor];
//
//    self.realNameLeadingLabel.textColor = [UIColor skinTextItemNorColor];
//    self.bankBtnLeadingLabel.textColor = [UIColor skinTextItemNorColor];
//    self.bankCardLeadingLabel.textColor = [UIColor skinTextItemNorColor];
//    self.pswLeadingLabel.textColor = [UIColor skinTextItemNorColor];
//    self.bankRegionLeadingLabel.textColor = [UIColor skinTextItemNorColor];
//    self.bankDetailLeadingLabel.textColor = [UIColor skinTextItemNorColor];
    
    [self.submitBtn setTitleColor:[UIColor skinViewKitNorColor] forState:UIControlStateNormal];
    [self.submitBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];
}

- (IBAction)sureClick:(id)sender {
    [self sureClick];
}

- (void)getLimit
{
    [SVProgressHUD showWithStatus:@"获取数据中..."];
    [NET_DATA_MANAGER requestGetQueryOutMoneyIndexSuccess:^(id responseObject) {
        self.model = [Bet365UserDrawModel mj_objectWithKeyValues:responseObject];
        [SVProgressHUD dismiss];
        if ([self.model.limit.bankAddressLimit integerValue] == 2) {
            self.bankDetailViewHeight.constant = 0;
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

- (IBAction)bankCardData:(id)sender {
    [self queryData];
}

- (IBAction)bankAddressClick:(id)sender {
    [BRAddressPickerView showAddressPickerWithDefaultSelected:nil isAutoSelect:YES themeColor:[UIColor skinNaviBgColor] resultBlock:^(BRProvinceModel *province, BRCityModel *city, BRAreaModel *area) {
        [self.bankRegionBtn setTitle:[NSString stringWithFormat:@"%@ %@ %@",province.name,city.name,area.name] forState:UIControlStateNormal];
    }];
}

- (void)queryData
{
    if (SportsShareInstance.bankArrs.count == 0) {
        [NET_DATA_MANAGER requestGetUserBankSuccess:^(id responseObject) {
            [self.bankArrs addObjectsFromArray:[LukeBankInformationItem mj_objectArrayWithKeyValuesArray:responseObject[@"rech_bank"]]];
            [self delegatePayItem];
            [BRStringPickerView showStringPickerWithTitle:@"出款银行" dataSource:[NSArray getNewArrayWithArr:[LukeBankInformationItem mj_keyValuesArrayWithObjectArray:self.bankArrs] key:@"configKey"] defaultSelValue:nil isAutoSelect:YES themeColor:[UIColor skinNaviBgColor] resultBlock:^(id selectValue) {
                [self.bankBtn setTitle:selectValue forState:UIControlStateNormal];
            }];
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }else{
        [self.bankArrs addObjectsFromArray:SportsShareInstance.bankArrs];
        [self delegatePayItem];
        [BRStringPickerView showStringPickerWithTitle:@"出款银行" dataSource:[NSArray getNewArrayWithArr:[LukeBankInformationItem mj_keyValuesArrayWithObjectArray:self.bankArrs] key:@"configKey"] defaultSelValue:nil isAutoSelect:YES themeColor:[UIColor skinNaviBgColor] resultBlock:^(id selectValue) {
            [self.bankBtn setTitle:selectValue forState:UIControlStateNormal];
        }];
    }
}

#pragma mark - 删除支付宝，微信支付，财付通
- (void)delegatePayItem
{
    NSMutableArray *temp = [NSMutableArray arrayWithArray:self.bankArrs];
    for (LukeBankInformationItem *item in temp) {
        if ([item.configKey isEqualToString:@"支付宝"] || [item.configKey isEqualToString:@"微信支付"] ||[item.configKey isEqualToString:@"财付通"]) {
            [self.bankArrs removeObject:item];
        }
    }
    SportsShareInstance.bankArrs = self.bankArrs;
}

- (void)sureClick
{
    if (![USER_DATA_MANAGER.userInfoData.fullName isNoBlankString] && self.realNameTextF.text.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"真实姓名必填哦"];
        return;
    }
    if (![USER_DATA_MANAGER.userInfoData.fullName isNoBlankString] && ![LukeUserAdapter validateNickname:self.realNameTextF.text]) {
        [SVProgressHUD showErrorWithStatus:@"请输入正确姓名!"];
        return;
    }
    if (self.bankBtn.currentTitle.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"出款银行没选择哦"];
        return;
    }
    if (![LukeUserAdapter isBankCard:self.bankCardTextF.text]) {
        [SVProgressHUD showErrorWithStatus:@"输入的卡号有误，请重新输入！"];
        return;
    }
    if (self.type == DrawBankCardType_Modify) {
        if (self.passwordTextF.text.length <= 0) {
            [SVProgressHUD showErrorWithStatus:@"提款密码必填哦"];
            return;
        }
    }
    if (self.bankRegionBtn.currentTitle.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"开户地区必选哦"];
        return;
    }
    if (self.model.limit.bankAddressLimit && [self.model.limit.bankAddressLimit integerValue] == 1) {
        if (self.bankCardTextF.text.length <= 0) {
            [SVProgressHUD showErrorWithStatus:@"详细名称必填哦"];
            return;
        }
    }
    NSString *addrStr = [NSString stringWithFormat:@"%@%@",self.bankRegionBtn.currentTitle,self.bankDetailTextF.text.length > 0 ? self.bankDetailTextF.text : @""];
    [SVProgressHUD showWithStatus:self.type == DrawBankCardType_Add ? @"添加中..." : @"修改中..."];
    [NET_DATA_MANAGER requestPostModifyUserInfoWithBankName:self.bankBtn.currentTitle cardNo:self.bankCardTextF.text subAddress:addrStr fullName: [USER_DATA_MANAGER.userInfoData.fullName isNoBlankString] ? nil : self.realNameTextF.text cashPassword:self.passwordTextF.text type:self.type Success:^(id responseObject) {
        [SVProgressHUD dismiss];
        [SVProgressHUD showSuccessWithStatus:self.type == DrawBankCardType_Add ? @"添加成功" : @"修改成功"];
        [USER_DATA_MANAGER requestGetUserInfoCompleted:^(BOOL success) {
            [self.navigationController popViewControllerAnimated:YES];
            if (self.delegate && [self.delegate respondsToSelector:@selector(bankCardModify)]) {
                [self.delegate bankCardModify];
            }
        }];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [SVProgressHUD dismiss];
    }];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.passwordTextF) {
        return textField.text.length - range.length + string.length < 5;
    }else{
        return YES;
    }
}

@end
