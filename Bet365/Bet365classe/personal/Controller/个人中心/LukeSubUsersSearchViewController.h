//
//  LukeSubUsersSearchViewController.h
//  Bet365
//
//  Created by luke on 17/7/23.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LukeSubUsersSearchViewControllerDelegate <NSObject>

- (void)reloadSubUserSearchWithAccount:(NSString *)account moneyFrom:(NSString *)moneyFrom moneyTo:(NSString *)moneyTo startDate:(NSString *)startDate endDate:(NSString *)endDate;

@end

@interface LukeSubUsersSearchViewController : Bet365ViewController
/** 代理 */
@property (nonatomic,weak) id<LukeSubUsersSearchViewControllerDelegate>delegate;

@end
