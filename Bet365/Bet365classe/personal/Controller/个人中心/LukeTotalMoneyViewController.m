//
//  LukeTotalMoneyViewController.m
//  Bet365
//
//  Created by luke on 17/6/15.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeTotalMoneyViewController.h"
#import "LukeUserAdapter.h"

static NSString * const cellID = @"cell";
@interface LukeTotalMoneyViewController ()<UITableViewDataSource>
/** 数据数组 */
@property (nonatomic,strong) NSMutableArray *datas;
/** label标题数组 */
@property (nonatomic,strong) NSArray *lbArrs;
/**  */
@property (nonatomic,weak) UITableView *tableView;

@end

@implementation LukeTotalMoneyViewController
- (NSArray *)lbArrs
{
    if (_lbArrs == nil) {
        _lbArrs = @[@"总金额",@"现金余额"];
    }
    return _lbArrs;
}
- (NSMutableArray *)datas
{
    if (_datas == nil) {
        _datas = [NSMutableArray array];
    }
    return _datas;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    
    [self queryData];
}

- (void)setupUI
{
    UITableView *tableView = [[UITableView alloc] init];
    tableView.backgroundColor = [UIColor skinViewScreenColor];
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    self.tableView = tableView;
    
    UIView *footView = [[UIView alloc] init];
    footView.frame = CGRectMake(0, 0, 0, 70);
    
    UIButton *sureBtn = [UIButton addButtonWithTitle:@"更新" font:15 color:[UIColor whiteColor] target:self action:@selector(sureClick)];
    [sureBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];
    [footView addSubview:sureBtn];
    sureBtn.layer.cornerRadius = 3;
    sureBtn.layer.masksToBounds = YES;
    sureBtn.frame = CGRectMake(15, 30, WIDTH - 30, 40);
    self.tableView.tableFooterView = footView;
}

- (void)sureClick
{
    [self queryData];
}

#pragma mark - 网络请求
- (void)queryData
{
    [self.datas removeAllObjects];
    [SVProgressHUD showWithStatus:@"获取数据中..."];
    [USER_DATA_MANAGER requestGetUserInfoCompleted:^(BOOL success) {
        [SVProgressHUD dismissWithCompletion:nil];
        if (success) {
            [self.datas addObject:[LukeUserAdapter formatFloat:[NSString stringWithFormat:@"%.2f",USER_DATA_MANAGER.userInfoData.money]]];
            [self.datas addObject:[LukeUserAdapter formatFloat:[NSString stringWithFormat:@"%.2f",USER_DATA_MANAGER.userInfoData.money]]];
            [self.tableView reloadData];
        }
    }];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismissWithCompletion:nil];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.datas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.textLabel.text = self.lbArrs[indexPath.row];
    
    cell.detailTextLabel.text = self.datas[indexPath.row];
    
    return cell;
}

@end
