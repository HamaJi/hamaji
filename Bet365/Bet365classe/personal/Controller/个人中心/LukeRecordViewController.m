//
//  LukeRecordViewController.m
//  Bet365
//
//  Created by luke on 17/6/15.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeRecordViewController.h"
#import "LukeScreenView.h"
#import "LukeRecordItem.h"
#import "LukeRecordCell.h"
#import "LukeRecordDescViewController.h"
#import "LukeUserAdapter.h"
#import "LukeDateSearchView.h"
#import "Bet365CpRecordTopView.h"
#import "NetWorkMannager+Account.h"
#import "Bet365DayReportViewController.h"
#import "NSString+transSegmentationClass.h"
#import "HMSegmentedControl+Bet365.h"
#import "UIViewControllerSerializing.h"
static NSInteger totalNum = 0;
static NSString * const LukeRecordCellID = @"LukeRecordCell";
static NSString * const CPRecordStreamlineCellID = @"CPRecordStreamlineCell";

@interface LukeRecordViewController ()<
UITableViewDelegate,
UITableViewDataSource,
Bet365CpRecordTopViewDelegate,
LukeScreenViewDelegate,
LukeRecordCellDelegate,
CPRecordStreamlineCellDelegate,
UIViewControllerSerializing>
/**  */
@property (nonatomic,strong) Bet365CpRecordTopView *cpRecordTopView;
/** 筛选view */
@property (nonatomic,strong) LukeScreenView *screenView;
/** tableview */
@property (nonatomic,strong) UITableView *tableView;
/**  */
@property (nonatomic,strong) HMSegmentedControl *segmentControl;
/** 页数 */
@property (nonatomic,assign) int page;
/** 数据源数组 */
@property (nonatomic,strong) NSMutableArray *recordArrs;
/** 撤销view */
@property (nonatomic,strong) UIView *delegateView;
/**  */
@property (nonatomic,weak) UIButton *cancelOrderBtn;
/** 全选 */
@property (nonatomic,weak) UIButton *allSelectBtn;
/**  */
@property (nonatomic,copy) NSString *gameId;
/**  */
@property (nonatomic,copy) NSString *account;
/**  */
@property (nonatomic,copy) NSString *statusID;
/**  */
@property (nonatomic,copy) NSString *model;
/**  */
@property (nonatomic,assign) BOOL classification;
/**  */
@property (nonatomic,copy) NSString *result;

@property (nonatomic,strong) otherDataModel *otherModel;

@property (nonatomic,strong) UIView *footView;

@property (nonatomic,strong) UILabel *totalLb;

@property (nonatomic,strong) UILabel *validLb;

@end

@implementation LukeRecordViewController

RouterKey *const kRouterRecordCpbetlist = @"record/cp-bet-list";

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    return [USER_DATA_MANAGER shouldPush2Login];
}
+(void)load{
    [self registerJumpRouterKey:kRouterRecordCpbetlist toHandle:^(NSDictionary *parameters) {
        LukeRecordViewController *vc = [[LukeRecordViewController alloc] init];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }];
    
    [self registerInstanceRouterKey:kRouterRecordCpbetlist toHandle:^UIViewController *(NSDictionary *parameters) {
        LukeRecordViewController *vc = [[LukeRecordViewController alloc] init];
        return vc;
    }];
}
- (UIView *)delegateView
{
    if (_delegateView == nil) {
        _delegateView = [[UIView alloc] init];
        _delegateView.backgroundColor = [UIColor blackColor];
        [self.view addSubview:_delegateView];
        
        UIButton *allSelectBtn = [UIButton addButtonWithTitle:@"全选" font:15 color:JesseColor(227, 133, 54) target:self action:@selector(delegateClickAction)];
        allSelectBtn.layer.cornerRadius = 3;
        allSelectBtn.layer.masksToBounds = YES;
        allSelectBtn.layer.borderColor = JesseColor(227, 133, 54).CGColor;
        allSelectBtn.layer.borderWidth = 2;
        [_delegateView addSubview:allSelectBtn];
        [allSelectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(_delegateView).offset(10);
            make.top.equalTo(_delegateView).offset(5);
            make.bottom.equalTo(_delegateView).offset(-5);
            make.width.mas_equalTo(allSelectBtn.mas_height).multipliedBy(2);
        }];
        self.allSelectBtn = allSelectBtn;
        
        UIButton *cancelOrderBtn = [UIButton addButtonWithTitle:@"撤单(0)" font:15 color:[UIColor whiteColor] target:self action:@selector(cancelOrderClick)];
        cancelOrderBtn.layer.cornerRadius = 5;
        cancelOrderBtn.layer.masksToBounds = YES;
        cancelOrderBtn.backgroundColor = JesseColor(232, 147, 67);
        [_delegateView addSubview:cancelOrderBtn];
        [cancelOrderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.trailing.equalTo(_delegateView).offset(-10);
            make.top.equalTo(_delegateView).offset(5);
            make.bottom.equalTo(_delegateView).offset(-5);
            make.width.mas_equalTo(cancelOrderBtn.mas_height).multipliedBy(2);
        }];
        self.cancelOrderBtn = cancelOrderBtn;
        
        [_delegateView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(self.view);
            make.top.equalTo(self.view.mas_bottom);
            make.height.mas_equalTo(40);
        }];
    }
    return _delegateView;
}

#pragma mark - 全选
- (void)delegateClickAction
{
    if (self.cpRecordTopView.cancelBtn.selected) {
        totalNum = 0;
        self.allSelectBtn.selected = YES;
        for (LukeRecordItem *item in self.recordArrs) {
            if (item.status == 0 && [LukeUserAdapter timeCompareWithOtherTime:item.betEndTime] == NSOrderedAscending) {
                totalNum ++;
                item.isSelectCancel = YES;
            }
        }
        [self.tableView reloadData];
        [self.cancelOrderBtn setTitle:[NSString stringWithFormat:@"撤单(%ld)",totalNum] forState:UIControlStateNormal];
        if (totalNum == 0) {
            [SVProgressHUD showErrorWithStatus:@"没有可撤销注单"];
        }
    }
}

- (void)cancelOrderClick
{
    if (totalNum > 0) {
        [self alertWithTitle:@"确定撤单吗？" message:[NSString stringWithFormat:@"即将撤销%ld笔注单",totalNum] cancelActionTitle:@"取消" cancelHandler:nil sureActionTitle:@"确定" sureHandler:^(UIAlertAction *action) {
            NSMutableArray *MArr = [NSMutableArray array];
            for (LukeRecordItem *item in self.recordArrs) {
                if (item.isSelectCancel) {
                    [MArr addObject:item.orderNo];
                }
            }
            NSDictionary *parameters = @{@"orderNo" : [MArr componentsJoinedByString:@","]};
            [NET_DATA_MANAGER requestPostCpParameters:parameters Success:^(id responseObject) {
                [SVProgressHUD showSuccessWithStatus:@"撤销成功"];
                [self CpRecordTopViewCancel:NO];
                self.cpRecordTopView.cancelBtn.selected = NO;
                [self.tableView.mj_header beginRefreshing];
            } failure:^(NSError *error) {
                NSLog(@"%@",error.msg);
                [SVProgressHUD showErrorWithStatus:@"撤销失败"];
            }];
        }];
    }else{
        [SVProgressHUD showErrorWithStatus:@"还未选择需要撤销的注单"];
    }
}

#pragma mark - LukeRecordCellDelegate
- (void)reloadCancelNumWithBool:(BOOL)isAdd
{
    if (isAdd) {
        totalNum ++;
    }else{
        totalNum --;
    }
    [self.cancelOrderBtn setTitle:[NSString stringWithFormat:@"撤单(%ld)",totalNum] forState:UIControlStateNormal];
}

#pragma mark - CPRecordStreamlineCellDelegate
- (void)cpreloadCancelNumWithBool:(BOOL)isAdd
{
    if (isAdd) {
        totalNum ++;
    }else{
        totalNum --;
    }
    [self.cancelOrderBtn setTitle:[NSString stringWithFormat:@"撤单(%ld)",totalNum] forState:UIControlStateNormal];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    [self setupRefresh];
}

-(void)enterLotteryRecord:(NSString *)gameId{
    self.classification = YES;
    self.gameId = gameId;
}

- (void)segmentClick:(UISegmentedControl *)segment
{
    if (segment.selectedSegmentIndex == 0) {
        [self.footView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(40);
        }];
    }else{
        [self.footView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0);
        }];
    }
    switch (segment.selectedSegmentIndex) {
        case 0:
            self.statusID = nil;
            self.result = nil;
            break;
        case 1:
            self.statusID = @"0";
            self.result = nil;
            break;
        case 2:
            self.statusID = @"1";
            self.result = nil;
            break;
        case 3:
            self.result = @"0";
            self.statusID = nil;
            break;
        case 4:
            self.result = @"1";
            self.statusID = nil;
            break;
        case 5:
            if (BET_CONFIG.config.hy_is_cancel && [BET_CONFIG.config.hy_is_cancel isEqualToString:@"1"]) {
                self.statusID = @"2";
                self.result = nil;
            }else{
                self.segmentControl.selectedSegmentIndex = 0;
                self.statusID = nil;
                self.result = nil;
                Bet365DayReportViewController *dayReportVC = [[Bet365DayReportViewController alloc] init];
                [self.navigationController pushViewController:dayReportVC animated:YES];
                dayReportVC.hidesBottomBarWhenPushed = YES;
            }
            break;
        case 6:
        {
            self.segmentControl.selectedSegmentIndex = 0;
            self.statusID = nil;
            self.result = nil;
            Bet365DayReportViewController *dayReportVC = [[Bet365DayReportViewController alloc] init];
            [self.navigationController pushViewController:dayReportVC animated:YES];
            dayReportVC.hidesBottomBarWhenPushed = YES;
        }
            break;
        default:
            break;
    }
    self.page = 1;
    [self.tableView.mj_header beginRefreshing];
}

- (void)setupRefresh
{
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self queryData];
    }];
    
    [self.tableView.mj_header beginRefreshing];
    self.tableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        self.page ++;
        [self queryData];
    }];
}

#pragma mark - 网络请求数据
- (void)queryData
{
    @weakify(self);
    [NET_DATA_MANAGER requestGetDeliverByAccount:self.account Page:self.page Rows:10 StartDate:nil EndDate:nil gameId:self.gameId status:self.statusID model:self.model result:self.result Success:^(id responseObject) {
        @strongify(self);
        if (self.segmentControl.selectedSegmentIndex==0) {
            self.otherModel = [otherDataModel mj_objectWithKeyValues:responseObject[@"otherData"]];
            CGFloat reward = self.otherModel.totalSum.reward ? [self.otherModel.totalSum.reward floatValue] : 0;
            CGFloat totalMoney = self.otherModel.validSum.totalMoney ? [self.otherModel.validSum.totalMoney floatValue] : 0;
            CGFloat drawMoney = self.otherModel.totalSum.drawMoney ? [self.otherModel.totalSum.drawMoney floatValue] : 0;
            CGFloat total = reward - (totalMoney - drawMoney);
            NSString *totalTitle = [NSString stringWithFormat:@"盈亏%.2f",total];
            NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:totalTitle];
            [attStr addColor:[UIColor blackColor] substring:@"盈亏"];
            self.totalLb.attributedText = attStr;
            
            CGFloat valid = totalMoney - drawMoney;
            NSString *validTitle = [NSString stringWithFormat:@"有效投注总计%.2f",valid];
            NSMutableAttributedString *validStr = [[NSMutableAttributedString alloc] initWithString:validTitle];
            [validStr addColor:[UIColor blackColor] substring:@"有效投注总计"];
            self.validLb.attributedText = validStr;
        }
        if (self.page == 1) {
            [self.recordArrs removeAllObjects];
        }
        [self.recordArrs addObjectsFromArray:[LukeRecordItem mj_objectArrayWithKeyValuesArray:responseObject[@"data"]]];
        if (self.cpRecordTopView.cancelBtn.selected) {
            for (LukeRecordItem *item in self.recordArrs) {
                item.isCancel = YES;
            }
        }
        if (self.allSelectBtn.selected) {
            for (LukeRecordItem *item in self.recordArrs) {
                if (item.status == 0 && [LukeUserAdapter timeCompareWithOtherTime:item.betEndTime] == NSOrderedAscending) {
                    totalNum ++;
                    item.isSelectCancel = YES;
                }
            }
        }
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        self.tableView.isShowEmpty = YES;
    } failure:^(NSError *error) {
        NSLog(@"%@",error.msg);
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        self.tableView.isShowEmpty = YES;
    }];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.recordArrs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *account = USER_DATA_MANAGER.userInfoData.account;
    BOOL isSw = [[NSUserDefaults standardUserDefaults] boolForKey:account];
    if (isSw) {
        CPRecordStreamlineCell *cell = [tableView dequeueReusableCellWithIdentifier:CPRecordStreamlineCellID];
        cell.item = self.recordArrs[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
        return cell;
    }else{
        LukeRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:LukeRecordCellID];
        cell.item = self.recordArrs[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.cpRecordTopView.cancelBtn.selected) {
        
    }else{
        LukeRecordDescViewController *descVC = [[LukeRecordDescViewController alloc] init];
        descVC.item = self.recordArrs[indexPath.row];
        [self.navigationController pushViewController:descVC animated:YES];
        descVC.hidesBottomBarWhenPushed = YES;
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *account = USER_DATA_MANAGER.userInfoData.account;
    BOOL isSw = [[NSUserDefaults standardUserDefaults] boolForKey:account];
    if (isSw) {
        return 60;
    }else{
        return 120;
    }
}

#pragma mark - Bet365CpRecordTopViewDelegate
- (void)CpRecordTopViewSearch
{
    [UIView animateWithDuration:0.3 animations:^{
        self.screenView.transform = CGAffineTransformMakeTranslation(0, -HEIGHT);
    }];
}

- (void)CpRecordTopViewCancel:(BOOL)isCancel
{
    totalNum = 0;
    [self.cancelOrderBtn setTitle:[NSString stringWithFormat:@"撤单(%ld)",totalNum] forState:UIControlStateNormal];
    if (isCancel) {
        [UIView animateWithDuration:0.3 animations:^{
            self.delegateView.transform = CGAffineTransformMakeTranslation(0, -40);
        }];
        for (LukeRecordItem *item in self.recordArrs) {
            item.isCancel = YES;
        }
        [self.tableView reloadData];
    }else{
        self.allSelectBtn.selected = NO;
        [UIView animateWithDuration:0.3 animations:^{
            self.delegateView.transform = CGAffineTransformIdentity;
        }];
        for (LukeRecordItem *item in self.recordArrs) {
            item.isCancel = NO;
            item.isSelectCancel = NO;
        }
        [self.tableView reloadData];
    }
}

#pragma mark - 懒加载
- (Bet365CpRecordTopView *)cpRecordTopView
{
    if (_cpRecordTopView == nil) {
        _cpRecordTopView = [[Bet365CpRecordTopView alloc] init];
        _cpRecordTopView.delegate = self;
        [self.view addSubview:_cpRecordTopView];
        [_cpRecordTopView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.top.trailing.equalTo(self.view);
            make.height.mas_equalTo(50);
        }];
    }
    return _cpRecordTopView;
}

- (LukeScreenView *)screenView
{
    if (_screenView == nil) {
        _screenView = [[LukeScreenView alloc] initWithFrame:CGRectZero isDL:USER_DATA_MANAGER.userInfoData.isDl isTouzhu:YES];
        _screenView.delegate = self;
        [self.view addSubview:_screenView];
        [_screenView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_bottom);
            make.leading.trailing.equalTo(self.view);
            make.height.mas_equalTo(HEIGHT);
        }];
    }
    return _screenView;
}

#pragma mark - LukeScreenViewDelegate
- (void)reloadRecordDataWithUser:(NSString *)user gameID:(NSString *)gameID model:(NSString *)model
{
    [self screenViewCancelClick];
    self.account = user;
    self.gameId = gameID;
    self.model = model;
    self.page = 1;
    [self.tableView.mj_header beginRefreshing];
}

- (void)screenViewCancelClick
{
    [UIView animateWithDuration:0.3 animations:^{
        self.screenView.transform = CGAffineTransformIdentity;
    }];
}

- (void)screenViewSwitchWithBool:(BOOL)sw
{
    [self.tableView reloadData];
}

- (NSMutableArray *)recordArrs
{
    if (_recordArrs == nil) {
        _recordArrs = [NSMutableArray array];
    }
    return _recordArrs;
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.backgroundColor = [UIColor skinViewScreenColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.tableFooterView = [UIView new];
        if ([_tableView respondsToSelector:@selector(layoutMargins)]) {
            _tableView.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        if ([_tableView respondsToSelector:@selector(separatorInset)]) {
            _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        [_tableView registerClass:[LukeRecordCell class] forCellReuseIdentifier:LukeRecordCellID];
        [_tableView registerClass:[CPRecordStreamlineCell class] forCellReuseIdentifier:CPRecordStreamlineCellID];
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.segmentControl.mas_bottom);
            make.leading.trailing.equalTo(self.view);
            make.bottom.equalTo(self.footView.mas_top);
        }];
        _tableView.titleForEmpty = @"没有数据";
    }
    return _tableView;
}

- (UIView *)footView
{
    if (!_footView) {
        _footView = [[UIView alloc] init];
        _footView.clipsToBounds = YES;
        _footView.backgroundColor = JesseGrayColor(239);
        [self.view addSubview:_footView];
        [_footView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.bottom.equalTo(self.view);
            make.height.mas_equalTo(40);
        }];
        
        self.totalLb = [UILabel addLabelWithFont:14 color:[UIColor redColor] title:@"盈亏"];
        [_footView addSubview:self.totalLb];
        [self.totalLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_footView);
            make.leading.equalTo(_footView).offset(10);
        }];
        
        self.validLb = [UILabel addLabelWithFont:14 color:[UIColor redColor] title:@"有效投注总计"];
        [_footView addSubview:self.validLb];
        [self.validLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_footView);
            make.leading.equalTo(_footView).offset(WIDTH * 0.5);
        }];
    }
    return _footView;
}

- (HMSegmentedControl *)segmentControl
{
    if (!_segmentControl) {
        NSArray *titles = nil;
        if (BET_CONFIG.config.hy_is_cancel && [BET_CONFIG.config.hy_is_cancel isEqualToString:@"1"]) {
            titles = @[@"今日全部",@"未开奖",@"已开奖",@"未中奖",@"已中奖",@"已撤销",@"历史记录"];
        }else{
            titles = @[@"今日全部",@"未开奖",@"已开奖",@"未中奖",@"已中奖",@"历史记录"];
        }
        _segmentControl = [HMSegmentedControl createSegmentWithBackgroundColor:[UIColor blackColor] IndicatorColor:[UIColor redColor] selectedTitleColor:[UIColor redColor] titleColor:[UIColor whiteColor] titleArrs:titles addTarget:self action:@selector(segmentClick:)];
        [self.view addSubview:_segmentControl];
        [_segmentControl addTarget:self action:@selector(segmentClick:) forControlEvents:UIControlEventValueChanged];
        [_segmentControl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.cpRecordTopView.mas_bottom);
            make.leading.trailing.equalTo(self.view);
            make.height.mas_equalTo(self.classification ? 0 : 50);
        }];
    }
    return _segmentControl;
}

@end

