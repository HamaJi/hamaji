//
//  LukeInfoViewController.m
//  Bet365
//
//  Created by luke on 17/6/27.
//  Copyright © 2017年 jesse. All rights reserved.
//个人资料

#import "LukeInfoViewController.h"
#import "LukeLoginPasswordView.h"
#import "LukePerfectPersonViewController.h"
#import <MXSegmentedPager.h>
#import "UIViewControllerSerializing.h"
#import "BindingPhoneViewController.h"

@interface LukeInfoViewController ()
<MXSegmentedPagerDataSource,
UIViewControllerSerializing>
/** 当前titlesArr */
@property (nonatomic,strong) NSMutableArray *titlesArr;
@property(nonatomic,strong) MXSegmentedPager *segmentedPager;
/**  */
@property(nonatomic,strong) NSMutableDictionary *viewData;

@end

@implementation LukeInfoViewController

RouterKey *const kRouterPersonalData = @"personalData";

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"TEST"]) {
        [SVProgressHUD showErrorWithStatus:@"试玩帐号不能进入个人信息"];
        return NO;
    }else{
        return [USER_DATA_MANAGER shouldPush2Login];
    }
}

+(void)load{
    [self registerJumpRouterKey:kRouterPersonalData toHandle:^(NSDictionary *parameters) {
        LukeInfoViewController *vc = [[LukeInfoViewController alloc] init];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self segmentedPager];
    if (self.isDraw) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.segmentedPager.pager showPageAtIndex:1 animated:NO];
        });
    }
}

#pragma mark - MXSegmentedPagerDataSource
- (NSInteger)numberOfPagesInSegmentedPager:(MXSegmentedPager *)segmentedPager
{
    return self.titlesArr.count;
}

- (NSString *)segmentedPager:(MXSegmentedPager *)segmentedPager titleForSectionAtIndex:(NSInteger)index
{
    return self.titlesArr[index];
}

- (UIView *)segmentedPager:(MXSegmentedPager *)segmentedPager viewForPageAtIndex:(NSInteger)index
{
    return [self.viewData objectForKey:[self.titlesArr safeObjectAtIndex:index]];
}

#pragma mark - SET/GET
-(NSMutableArray *)titlesArr{
    if (_titlesArr==nil) {
        _titlesArr = [[NSMutableArray alloc] initWithArray:@[@"登录密码",@"取款密码"]];
        if (![USER_DATA_MANAGER.userInfoData.type isEqualToString:@"TEST"]) {
            [_titlesArr addObject:@"完善资料"];
            if ([BET_CONFIG.limit.loginPhone integerValue] == 1) {
                [_titlesArr addObject:@"手机号登录"];
            }
            
        }
    }
    return _titlesArr;
}

-(NSMutableDictionary *)viewData{
    if (!_viewData) {
        _viewData = [NSMutableDictionary dictionary];
        LukeLoginPasswordView *loginView = [[LukeLoginPasswordView alloc] initWithTitle:@"登录密码由6-12位字母或数字组成" type:PersonPassWordTypeLogin];
        [_viewData setObject:loginView forKey:[self.titlesArr safeObjectAtIndex:0]];
        LukeLoginPasswordView *drawView = [[LukeLoginPasswordView alloc] initWithTitle:@"取款密码由4位数字组成" type:PersonPassWordTypeDraw];
        [_viewData setObject:drawView forKey:[self.titlesArr safeObjectAtIndex:1]];
        if (![USER_DATA_MANAGER.userInfoData.type isEqualToString:@"TEST"]) {
            LukePerfectPersonViewController *userInfoVc = [[LukePerfectPersonViewController alloc] initWithNibName:@"LukePerfectPersonViewController" bundle:nil];
            [_viewData setObject:userInfoVc.view forKey:@"完善资料"];
            [self addChildViewController:userInfoVc];
            if ([BET_CONFIG.limit.loginPhone integerValue] == 1) {
                BindingPhoneViewController *bindingVc = [[BindingPhoneViewController alloc] initWithNibName:@"BindingPhoneViewController" bundle:nil];
                [_viewData setObject:bindingVc.view forKey:@"手机号登录"];
                [self addChildViewController:bindingVc];
            }
        }
    }
    return _viewData;
}
-(MXSegmentedPager *)segmentedPager{
    if (_segmentedPager == nil) {
        _segmentedPager = [[MXSegmentedPager alloc] init];
        _segmentedPager.dataSource = self;
        _segmentedPager.segmentedControl.selectionIndicatorColor = [UIColor skinViewKitSelColor];
        _segmentedPager.segmentedControl.selectionIndicatorHeight = 2;
        _segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
        _segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        _segmentedPager.segmentedControl.verticalDividerEnabled = NO;
        _segmentedPager.segmentedControl.userDraggable = YES;
        _segmentedPager.pager.scrollEnabled = NO;
        _segmentedPager.segmentedControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleFixed;
        // 非选状态
        _segmentedPager.segmentedControl.titleTextAttributes =
        @{NSForegroundColorAttributeName : [UIColor skinViewKitDisColor],
          NSFontAttributeName : [UIFont systemFontOfSize:15]};
        // 选中状态
        _segmentedPager.segmentedControl.selectedTitleTextAttributes =
        @{NSForegroundColorAttributeName : [UIColor skinViewKitSelColor],
          NSFontAttributeName : [UIFont systemFontOfSize:17]};
        _segmentedPager.segmentedControlEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        _segmentedPager.segmentedControl.backgroundColor = [UIColor skinViewBgColor];
        [self.view addSubview:_segmentedPager];
        [_segmentedPager mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
    }
    return _segmentedPager;
}

@end
