//
//  Bet365ReleaseBankController.m
//  Bet365
//
//  Created by luke on 2018/11/10.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Bet365ReleaseBankController.h"
#import "NetWorkMannager+Account.h"

@interface Bet365ReleaseBankController ()
<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *testField;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *titileLabel;
@property (weak, nonatomic) IBOutlet UIView *tfContentView;
@property (weak, nonatomic) IBOutlet UIButton *canleBtn;

@end

@implementation Bet365ReleaseBankController

- (void)viewDidLoad {
    [super viewDidLoad];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.testField.textColor = [UIColor skinTextItemNorColor];
        self.contentView.backgroundColor = [UIColor skinViewScreenColor];
        self.tfContentView.backgroundColor = [UIColor skinViewBgColor];
        [self.canleBtn setTitleColor:[UIColor skinViewKitNorColor] forState:UIControlStateNormal];
        [self.canleBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];
    });
}

- (IBAction)releaseClick:(id)sender {
    if (self.testField.text.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"提款密码必填哦"];
    }else{
        [SVProgressHUD showWithStatus:@"解绑中..."];
        [NET_DATA_MANAGER requestPostModifyUserInfoWithBankName:nil cardNo:nil subAddress:nil fullName:nil cashPassword:self.testField.text type:DrawBankCardType_Release Success:^(id responseObject) {
            [SVProgressHUD dismiss];
            [SVProgressHUD showSuccessWithStatus:@"解绑成功"];
            [USER_DATA_MANAGER requestGetUserInfoCompleted:^(BOOL success) {
                [self dismissClick:nil];
                if (self.delegate && [self.delegate respondsToSelector:@selector(releaseBankPop)]) {
                    [self.delegate releaseBankPop];
                }
            }];
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }
}

- (IBAction)dismissClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.testField) {
        return textField.text.length - range.length + string.length < 5;
    }else{
        return YES;
    }
}

@end

