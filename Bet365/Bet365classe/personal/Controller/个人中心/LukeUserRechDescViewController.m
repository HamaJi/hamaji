//
//  LukeUserRechDescViewController.m
//  Bet365
//
//  Created by luke on 17/7/11.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeUserRechDescViewController.h"
#import "LukeUserWithdrawItem.h"
#import "LukeUserRechItem.h"

@interface LukeUserRechDescViewController ()

@end

@implementation LukeUserRechDescViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton *backBtn = [UIButton addButtonWithTitle:@"详情" font:17 color:[UIColor blackColor] target:self action:@selector(backClick)];
    [backBtn setImage:[UIImage imageNamed:@"left_lage"] forState:UIControlStateNormal];
    [backBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(statusHeight + 10);
        make.leading.equalTo(self.view).offset(10);
    }];
    
    UIView *lineView = [self addLineView];
    [self.view addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backBtn.mas_bottom).offset(10);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(1);
    }];
}

- (void)backClick
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setUserRechItem:(LukeUserRechItem *)userRechItem
{
    _userRechItem = userRechItem;
    
    UIView *rechOrderNoView = [self addFirstLabelTitle:@"流水号" secondLabelTitle:userRechItem.rechOrderNo secondColor:JesseGrayColor(130)];
    [self.view addSubview:rechOrderNoView];
    [rechOrderNoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(64);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    NSString *typeName = nil;
    switch (userRechItem.rechMode) {
        case 1:
            typeName = @"银行转账";
            break;
        case 2:
            typeName = @"支付宝转账";
            break;
        case 3:
            typeName = @"微信转账";
            break;
        case 4:
            typeName = @"后台存入";
            break;
        case 5:
            typeName = @"第三方支付";
            break;
        default:
            break;
    }
    
    NSString *rechSourceStr = @"";
    switch (userRechItem.rechSource) {
        case 1:
            rechSourceStr = @"(存款优惠)";
            break;
        case 2:
            rechSourceStr = @"(彩金优惠)";
            break;
        case 3:
            rechSourceStr = @"(打码还水)";
            break;
        default:
            break;
    }
    UIView *rechTypeView = [self addFirstLabelTitle:@"充值方式" secondLabelTitle:[NSString stringWithFormat:@"%@%@",typeName,rechSourceStr] secondColor:JesseGrayColor(130)];
    [self.view addSubview:rechTypeView];
    [rechTypeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(rechOrderNoView.mas_bottom);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    
    UIView *rechNameView = [self addFirstLabelTitle:@"收款名称" secondLabelTitle:userRechItem.rechName.length ? userRechItem.rechName : @"" secondColor:JesseGrayColor(130)];
    [self.view addSubview:rechNameView];
    [rechNameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(rechTypeView.mas_bottom);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    
    UIView *payeeNameView = [self addFirstLabelTitle:@"收款人" secondLabelTitle:userRechItem.payeeName.length ? userRechItem.payeeName : @"" secondColor:JesseGrayColor(130)];
    [self.view addSubview:payeeNameView];
    [payeeNameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(rechNameView.mas_bottom);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    
    UIView *actualMoneyView = [self addFirstLabelTitle:@"充值总额" secondLabelTitle:[NSString stringWithFormat:@"¥%ld",userRechItem.actualMoney]secondColor:JesseGrayColor(130)];
    [self.view addSubview:actualMoneyView];
    [actualMoneyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(payeeNameView.mas_bottom);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    
    UIView *timeView = [self addFirstLabelTitle:@"操作时间" secondLabelTitle:userRechItem.rechStatus == 1 ? userRechItem.addTime : userRechItem.operatorTime secondColor:JesseGrayColor(130)];
    [self.view addSubview:timeView];
    [timeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(actualMoneyView.mas_bottom);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    
    NSString *statusStr = nil;
    switch (userRechItem.rechStatus) {
        case 1:
            statusStr = @"未受理";
            break;
        case 2:
            statusStr = @"受理中";
            break;
        case 3:
            statusStr = @"已入款";
            break;
        case 4:
            statusStr = @"已取消";
            break;
        default:
            break;
    }
    UIView *rechStatusView = [self addFirstLabelTitle:@"状态" secondLabelTitle:statusStr secondColor:[UIColor redColor]];
    [self.view addSubview:rechStatusView];
    [rechStatusView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(timeView.mas_bottom);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
}

- (void)setUserWithdrawItem:(LukeUserWithdrawItem *)userWithdrawItem
{
    _userWithdrawItem = userWithdrawItem;
    UIView *rechOrderNoView = [self addFirstLabelTitle:@"流水号" secondLabelTitle:userWithdrawItem.cashOrderNo secondColor:JesseGrayColor(130)];
    [self.view addSubview:rechOrderNoView];
    [rechOrderNoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(64);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    
    UIView *rechTypeView = [self addFirstLabelTitle:@"取款金额" secondLabelTitle:[NSString stringWithFormat:@"¥%ld",userWithdrawItem.cashMoney] secondColor:JesseGrayColor(130)];
    [self.view addSubview:rechTypeView];
    [rechTypeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(rechOrderNoView.mas_bottom);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    
    UIView *counterFeeView = [self addFirstLabelTitle:@"手续费" secondLabelTitle:[NSString stringWithFormat:@"¥%ld",userWithdrawItem.counterFee] secondColor:JesseGrayColor(130)];
    [self.view addSubview:counterFeeView];
    [counterFeeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(rechTypeView.mas_bottom);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    
    UIView *approveMoneyView = [self addFirstLabelTitle:@"出款金额" secondLabelTitle:[NSString stringWithFormat:@"¥%ld",userWithdrawItem.approveMoney] secondColor:JesseGrayColor(130)];
    [self.view addSubview:counterFeeView];
    [self.view addSubview:approveMoneyView];
    [approveMoneyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(counterFeeView.mas_bottom);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    
    UIView *rechNameView = [self addFirstLabelTitle:@"收款银行" secondLabelTitle:userWithdrawItem.bankName secondColor:JesseGrayColor(130)];
    [self.view addSubview:rechNameView];
    [rechNameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(approveMoneyView.mas_bottom);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    
    UIView *payeeNameView = [self addFirstLabelTitle:@"收款帐号" secondLabelTitle:userWithdrawItem.bankCard secondColor:JesseGrayColor(130)];
    [self.view addSubview:payeeNameView];
    [payeeNameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(rechNameView.mas_bottom);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    
    UIView *timeView = [self addFirstLabelTitle:@"操作时间" secondLabelTitle:userWithdrawItem.cashStatus == 1 ? userWithdrawItem.addTime : userWithdrawItem.operatorTime secondColor:JesseGrayColor(130)];
    [self.view addSubview:timeView];
    [timeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(payeeNameView.mas_bottom);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    
    NSString *statusStr = nil;
    switch (userWithdrawItem.cashStatus) {
        case 1:
            statusStr = @"未受理";
            break;
        case 2:
            statusStr = @"受理中";
            break;
        case 3:
            statusStr = @"已出款";
            break;
        case 4:
            statusStr = @"已取消";
            break;
        case 5:
            statusStr = @"已拒绝";
            break;
        case 6:
            statusStr = @"已撤销";
            break;
        default:
            break;
    }
    
    UIView *rechStatusView = [self addFirstLabelTitle:@"状态" secondLabelTitle:statusStr secondColor:[UIColor redColor]];
    [self.view addSubview:rechStatusView];
    [rechStatusView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(timeView.mas_bottom);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
}

- (UIView *)addFirstLabelTitle:(NSString *)firstTitle secondLabelTitle:(NSString *)secondTitle secondColor:(UIColor *)color
{
    UIView *view = [[UIView alloc] init];
    UILabel *firstLb = [self addLabelWithFont:15 color:[UIColor blackColor] textAlignment:NSTextAlignmentLeft title:firstTitle];
    [view addSubview:firstLb];
    [firstLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(view);
        make.leading.equalTo(view).offset(10);
    }];
    UILabel *secondLb = [self addLabelWithFont:15 color:color textAlignment:NSTextAlignmentRight title:secondTitle];
    [view addSubview:secondLb];
    [secondLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(view);
        make.trailing.equalTo(view).offset(-10);
    }];
    UIView *lineView = [self addLineView];
    [view addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.bottom.equalTo(view);
        make.height.mas_equalTo(1);
    }];
    return view;
}

//创建线条
- (UIView *)addLineView
{
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = JesseGrayColor(221);
    return lineView;
}
//创建label
- (UILabel *)addLabelWithFont:(CGFloat)font color:(UIColor *)color textAlignment:(NSTextAlignment)textAlignment title:(NSString *)title
{
    UILabel *label = [[UILabel alloc] init];
    label.text = title;
    label.textColor = color;
    label.font = [UIFont systemFontOfSize:font];
    label.textAlignment = textAlignment;
    return label;
}

@end
