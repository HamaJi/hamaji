//
//  Bet365TransferController.m
//  Bet365
//
//  Created by luke on 2018/8/29.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Bet365TransferController.h"
#import "NetWorkMannager+Account.h"
#import "Bet365TranferTypeCell.h"
#import "Bet365TranferBalanceCell.h"
#import "Bet36TranferMoneyCell.h"
#import "Bet365TranferAccountCell.h"
#import "LukeLiveRecordItem.h"
#import "UIViewControllerSerializing.h"
#import "NetWorkMannager+electronic.h"
#import "Bet365TransferStatusHeaderView.h"
#import "NetWorkMannager+activity.h"

@interface Bet365TransferController ()
<UITableViewDelegate,
UITableViewDataSource,
TranferTypeCellDelegate,
TranferBalanceCellDelegate,
UITextFieldDelegate,
UIViewControllerSerializing,
TransferStatusHeaderViewDelegate,
TranferAccountCellReclaimLiveAmountDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
/**  */
@property (nonatomic,strong) NSMutableArray *allLiveGames;
/** 组头标题 */
@property (nonatomic,strong) NSArray *sectionTitles;
/**  */
@property (nonatomic,strong) NSMutableArray *userTypeArrs;
/** 余额转入 */
@property (nonatomic,strong) NSMutableArray *toBalanceArrs;
/** 余额转出 */
@property (nonatomic,strong) NSMutableArray *fromBalanceArrs;
/**  */
@property (nonatomic,weak) Bet36TranferMoneyCell *moneyCell;
/**当前cell展示金额*/
@property(nonatomic,copy)NSString *moneyFiledValue;
/**当前game转换金额*/
@property(nonatomic,copy)NSString *changeBalance;
@property (weak, nonatomic) IBOutlet UIButton *cancleBtn;
@property (weak, nonatomic) IBOutlet UIButton *subMitBtn;

@property (nonatomic,assign) TransferStatus status;

@property (nonatomic,strong) UILabel *footerLb;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *footerViewLayHeight;

@end

RouterKey *const kRouterConver = @"conver";

@implementation Bet365TransferController

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"TEST"]) {
        [SVProgressHUD showErrorWithStatus:@"试玩帐号不能额度转换"];
        return NO;
    }else if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"VHY"]) {
        [SVProgressHUD showErrorWithStatus:@"您的账号没有权限，请联系您的上级开通"];
        return NO;
    }
    return [USER_DATA_MANAGER shouldPush2Login];
}

+(void)load{
    [self registerJumpRouterKey:kRouterConver toHandle:^(NSDictionary *parameters) {
        Bet365TransferController *vc = [[Bet365TransferController alloc] init];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }];
    
    [self registerInstanceRouterKey:kRouterConver toHandle:^UIViewController *(NSDictionary *parameters) {
        Bet365TransferController *vc = [[Bet365TransferController alloc] init];
        return vc;
    }];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.tableView registerNib:[UINib nibWithNibName:@"Bet365TransferStatusHeaderView" bundle:nil] forHeaderFooterViewReuseIdentifier:@"Bet365TransferStatusHeaderView"];
    self.tableView.sectionHeaderHeight = 44;
    self.tableView.rowHeight = 44;
    if ([self.tableView respondsToSelector:@selector(layoutMargins)]) {
        self.tableView.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    if ([self.tableView respondsToSelector:@selector(separatorInset)]) {
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    [self getConfigCompleted:^(BOOL success) {
        
    }];
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    [self.cancleBtn setBackgroundColor:[UIColor skinViewKitNorColor]];
    [self.subMitBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];
    
    [self.cancleBtn.titleLabel setTextColor:[UIColor skinViewKitSelColor]];
    [self.subMitBtn.titleLabel setTextColor:[UIColor skinViewKitNorColor]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

-(void)requestGetSystemConfigCompleted:(void(^)(NSDictionary *data))completed{
    [NET_DATA_MANAGER requestSystemConfigSuccess:^(id responseObject) {
        NSDictionary *dic = (NSDictionary *)responseObject;
        completed ? completed(dic) : nil;
    } failure:^(NSError *error) {
        completed ? completed(nil) : nil;
    }];
}

- (void)getAllLiveGamesCompleted:(void(^)(id responseObject))completed
{
    [NET_DATA_MANAGER requestGetAllLiveGamesShow:nil Cache:nil Success:^(id responseObject) {
        completed ? completed(responseObject) : nil;
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        completed ? completed(nil) : nil;
    }];

}

-(void)getConfigCompleted:(void(^)(BOOL success))completed{
    
    [SVProgressHUD showWithStatus:@"获取数据中..."];
    
    dispatch_group_t group = dispatch_group_create();
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [self requestGetSystemConfigCompleted:^(NSDictionary *data) {
            if (data) {
                [BET_CONFIG.config cleanUserData];
                [BET_CONFIG.config setDataValueForm:data];
                [BET_CONFIG.config save];
            }
            dispatch_semaphore_signal(sema);
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [self getAllLiveGamesCompleted:^(id responseObject) {
            if (responseObject) {
                NSArray *temp = [LukeLiveGamesItem mj_objectArrayWithKeyValuesArray:responseObject];
                for (LukeLiveGamesItem *item in temp) {
                    if (![item.code isEqualToString:@"wzry"] && ![item.code isEqualToString:@"lucky"]) {
                        item.name = StringFormatWithStr(item.name, @"余额");
                        [self.allLiveGames addObject:item];
                    }
                }
                [self.userTypeArrs safeReplaceObjectAtIndex:1 withObject:[self.fromBalanceArrs safeObjectAtIndex:1]];
            }
            dispatch_semaphore_signal(sema);
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
        if ([BET_CONFIG.config.user_transfer_stauts isEqualToString:@"1"]) {
            self.status = TransferStatus_auto;
        }else if ([BET_CONFIG.config.user_transfer_stauts isEqualToString:@"3"]) {
            self.status = TransferStatus_Manual;
        } else{
            self.status = [USER_DATA_MANAGER.userInfoData.transferStatus integerValue];
        }
        if (self.status == TransferStatus_auto) {
            self.tableView.tableFooterView = self.footerLb;
            self.footerViewLayHeight.constant = 0;
        }
        
        if (self.status == TransferStatus_Manual) {
            [self queryBalanceForCurrenGameCode:[self.allLiveGames firstObject] gameBalanceBlock:^(NSString *balance) {
                self.changeBalance = balance;
                [self.tableView reloadData];
            }];
        }else{
            [self.tableView reloadData];
        }
    });
}

- (IBAction)Reset:(UIButton *)sender {
    self.moneyCell.moneyTextField.text = nil;
    self.moneyFiledValue = nil;
}

- (IBAction)submit:(UIButton *)sender {
    __block NSString *from = self.userTypeArrs[0];
    __block NSString *to = self.userTypeArrs[1];
    if (to.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"未选择余额转入类型"];
    }else if (self.moneyFiledValue.length <= 0){
        [SVProgressHUD showErrorWithStatus:@"转换金额不能为空"];
    }else{
        __block LukeLiveGamesItem *item = nil;
        if ([from isEqualToString:@"账户余额"]) {
            from = @"self";
        }else{
            [self.allLiveGames enumerateObjectsUsingBlock:^(LukeLiveGamesItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([from isEqualToString:obj.name]) {
                    from = obj.code;
                    item = obj;
                    *stop = YES;
                }
            }];
        }
        if ([to isEqualToString:@"账户余额"]) {
            to = @"self";
        }else{
            [self.allLiveGames enumerateObjectsUsingBlock:^(LukeLiveGamesItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([to isEqualToString:obj.name]) {
                    to = obj.code;
                    item = obj;
                    *stop = YES;
                }
            }];
        }
        //进行转换成功后查询余额
        @weakify(self);
        void(^queryBalance)(NSString *itemCode) = ^(NSString *itemCode){
            @strongify(self);
            [NET_DATA_MANAGER requestGetGetBalanceWithLiveCode:itemCode Success:^(id responseObject) {
                [SVProgressHUD dismiss];
                item.balance = [NSNumber numberWithFloat:[responseObject floatValue]];
                self.changeBalance = [item.balance stringValue];
                self.moneyFiledValue = nil;
                [SVProgressHUD showSuccessWithStatus:@"额度转换成功"];
                [self.tableView reloadData];
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [SVProgressHUD dismiss];
            }];
        };
        [SVProgressHUD showWithStatus:@"额度转换中..."];
        [NET_DATA_MANAGER requestGetTransferWithFrom:from to:to amount:self.moneyFiledValue Success:^(id responseObject) {
            if (item) {
                if ([item.code isEqualToString:@"m8"]) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        queryBalance(item.code);
                    });
                }else{
                        queryBalance(item.code);
                }
            }
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
            [SVProgressHUD dismiss];
        }];
    }
}

#pragma mark - TranferTypeCellDelegate
- (void)typeClickBtnWithCell:(Bet365TranferTypeCell *)cell
{
    @weakify(self);
    void(^querySelector)(NSString *itemName) = ^(NSString *itemName){
        @strongify(self);
        NSEnumerator *enumG = [self.allLiveGames reverseObjectEnumerator];
        for (LukeLiveGamesItem *item in enumG) {
            if ([item.name isEqualToString:itemName]) {
                if (!item.balance) {
                    [self queryBalanceForCurrenGameCode:item gameBalanceBlock:^(NSString *balance) {
                        self.changeBalance = balance;
                        [self.tableView reloadData];
                    }];
                }else{
                    self.changeBalance = [item.balance stringValue];
                    [self.tableView reloadData];
                }
                return;
            }
        }
    };
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    if (indexPath.row == 0) {
        [BRStringPickerView showStringPickerWithTitle:@"额度转换类型" dataSource:self.fromBalanceArrs defaultSelValue:nil isAutoSelect:NO themeColor:[UIColor skinViewKitSelColor] resultBlock:^(id selectValue) {
            if ([selectValue isEqualToString:@"账户余额"]) {
                [self.userTypeArrs replaceObjectAtIndex:1 withObject:[self.fromBalanceArrs safeObjectAtIndex:1]];
                querySelector([self.fromBalanceArrs safeObjectAtIndex:1]);
            }else {
                [self.userTypeArrs replaceObjectAtIndex:1 withObject:@"账户余额"];
                querySelector(selectValue);
            }
            [self.userTypeArrs replaceObjectAtIndex:indexPath.row withObject:selectValue];
            [self.tableView reloadData];
        }];
    }else{
        [RACObserve(self, userTypeArrs) subscribeNext:^(id  _Nullable x) {
            [self.toBalanceArrs removeAllObjects];
            NSMutableArray *arr = (NSMutableArray *)x;
            if (![[arr firstObject] isEqualToString:@"账户余额"]) {
                [self.toBalanceArrs addObject:[@"账户余额" mutableCopy]];
            }else{
                for (LukeLiveGamesItem *item in self.allLiveGames) {
                    [self.toBalanceArrs addObject:item.name];
                }
            }
        }];
        [BRStringPickerView showStringPickerWithTitle:@"额度转换类型" dataSource:self.toBalanceArrs defaultSelValue:nil isAutoSelect:NO themeColor:[UIColor skinViewKitSelColor] resultBlock:^(id selectValue) {
            [self.userTypeArrs replaceObjectAtIndex:indexPath.row withObject:selectValue];
            querySelector(selectValue);
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }];
    }
}

#pragma mark - TranferBalanceCellDelegate
- (void)queryBalanceWithCell:(Bet365TranferBalanceCell *)cell
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    LukeLiveGamesItem *item = self.allLiveGames[indexPath.row];
    [self queryBalanceForCurrenGameCode:item gameBalanceBlock:^(NSString *balance) {
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }];
}

#pragma mark - TranferAccountCellReclaimLiveAmountDelegate
- (void)reclaimLiveAmount
{
    [SVProgressHUD showWithStatus:@"回收中..."];
    [NET_DATA_MANAGER requestPostReclaimLiveAmountSuccess:^(id responseObject) {
        [SVProgressHUD dismiss];
        [SVProgressHUD showSuccessWithStatus:@"操作成功"];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [SVProgressHUD dismiss];
    }];
}

-(void)queryBalanceForCurrenGameCode:(LukeLiveGamesItem *)item gameBalanceBlock:(void(^)(NSString *balance))balanceComplet{
    [SVProgressHUD showWithStatus:@"余额查询中..."];
    [NET_DATA_MANAGER requestGetGetBalanceWithLiveCode:item.code Success:^(id responseObject) {
        [SVProgressHUD dismiss];
        [SVProgressHUD showSuccessWithStatus:@"余额查询成功"];
        item.balance = [NSNumber numberWithFloat:[responseObject floatValue]];
        balanceComplet ? balanceComplet([item.balance stringValue]):nil;
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [SVProgressHUD dismiss];
        balanceComplet ? balanceComplet(nil):nil;
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.status == TransferStatus_Manual) {
        return 4;
    }else{
        return 2;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.status == TransferStatus_Manual) {
        if (section == TranferUserType_Type) {
            return 2;
        }else if (section == TranferUserType_Money || section == TranferUserType_Account) {
            return 1;
        }else {
            return self.allLiveGames.count;
        }
    }else{
        if (section == TranferUserType_Account - 2) {
            return 1;
        }else {
            return self.allLiveGames.count;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger sec = 0;
    NSInteger row = indexPath.row;
    if (self.status == TransferStatus_auto) {
        sec = indexPath.section + 2;
    }else{
        sec = indexPath.section;
    }
    if (sec == TranferUserType_Type) {
        Bet365TranferTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Bet365TranferTypeCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"Bet365TranferTypeCell" owner:self options:0] firstObject];
        }
        cell.delegate = self;
        [cell setTypeCellWithTitle:self.userTypeArrs[row] index:row money:self.changeBalance];
        return cell;
    }else if (sec == TranferUserType_Money) {
        Bet36TranferMoneyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Bet36TranferMoneyCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"Bet36TranferMoneyCell" owner:self options:0] firstObject];
            cell.moneyTextField.delegate = self;
        }
        cell.moneyTextField.text = self.moneyFiledValue?self.moneyFiledValue:@"";
        self.moneyCell = cell;
        return cell;
    }else if (sec == TranferUserType_Account) {
        Bet365TranferAccountCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Bet365TranferAccountCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"Bet365TranferAccountCell" owner:self options:0] firstObject];
        }
        cell.delegate = self;
        return cell;
    }else {
        Bet365TranferBalanceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Bet365TranferBalanceCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"Bet365TranferBalanceCell" owner:self options:0] firstObject];
        }
        cell.delegate = self;
        cell.item = self.allLiveGames[row];
        return cell;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    Bet365TransferStatusHeaderView *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Bet365TransferStatusHeaderView"];
    if ([BET_CONFIG.skin.skinName isEqualToString:[UIColor skinBlackName]]) {
        header.contentView.backgroundColor = [UIColor skinViewBgColor];
    }else{
        header.contentView.backgroundColor = [[UIColor skinViewContentColor] colorWithAlphaComponent:0.1];
    }
    NSString *title = nil;
    if (self.status == TransferStatus_Manual) {
        title = self.sectionTitles[section];
    }else{
        title = self.sectionTitles[section + 2];
    }
    [header transferTitle:title index:section status:self.status];
    header.delegate = self;
    return header;
}

#pragma mark - TransferStatusHeaderViewDelegate
- (void)transferStatusHandel
{
    void(^queryTransferStatus)() = ^(NSInteger stataus){
        [SVProgressHUD showWithStatus:@"转换中..."];
        [NET_DATA_MANAGER requestPostsetUserTransfer:stataus success:^(id responseObject) {
            [USER_DATA_MANAGER requestGetUserInfoCompleted:^(BOOL success) {
                [SVProgressHUD dismiss];
                [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"已切换为%@额度转换模式",stataus == 1 ? @"自动" : @"手动"]];
                self.status = [USER_DATA_MANAGER.userInfoData.transferStatus integerValue];
                self.tableView.tableFooterView = stataus == 1 ? self.footerLb : [UIView new];
                [self.tableView reloadData];
                self.footerViewLayHeight.constant = stataus == 1 ? 0 : 45;
            }];
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
            [SVProgressHUD dismiss];
        }];
    };
    if (self.status == TransferStatus_Manual) {
        [self alertWithTitle:@"温馨提示" message:@"提示：切换到自动额度转换前，请先通过手动转换把额度转移到现金余额" cancelActionTitle:@"取消" cancelHandler:nil sureActionTitle:@"确定" sureHandler:^(UIAlertAction *action) {
            queryTransferStatus(1);
        }];
    }else{
        queryTransferStatus(0);
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    self.moneyFiledValue = textField.text;
}
#pragma mark - 懒加载
- (NSMutableArray *)allLiveGames
{
    if (_allLiveGames == nil) {
        _allLiveGames = [NSMutableArray array];
    }
    return _allLiveGames;
}

- (NSArray *)sectionTitles
{
    if (_sectionTitles == nil) {
        _sectionTitles = @[@"额度转换类型",@"转换金额",@"账户信息",@"平台账户余额查询"];
    }
    return _sectionTitles;
}

- (NSMutableArray *)userTypeArrs
{
    if (_userTypeArrs == nil) {
        _userTypeArrs = [NSMutableArray arrayWithObjects:@"账户余额",@"",nil];
    }
    return _userTypeArrs;
}

- (NSMutableArray *)toBalanceArrs
{
    if (_toBalanceArrs == nil) {
        _toBalanceArrs = [NSMutableArray array];
    }
    return _toBalanceArrs;
}

- (NSMutableArray *)fromBalanceArrs
{
    if (_fromBalanceArrs == nil) {
        _fromBalanceArrs = [NSMutableArray arrayWithObjects:[@"账户余额" mutableCopy], nil];
        for (LukeLiveGamesItem *item in self.allLiveGames) {
            [_fromBalanceArrs addObject:item.name];
        }
    }
    return _fromBalanceArrs;
}

- (UILabel *)footerLb
{
    if (!_footerLb) {
        _footerLb = [UILabel addLabelWithFont:15 color:[UIColor redColor] title:@"温馨提示:你目前处于自动额度转换状态，无需额度转换，可以直接进入游戏哦~"];
        _footerLb.numberOfLines = 0;
        _footerLb.width = WIDTH;
        _footerLb.height = 40;
    }
    return _footerLb;
}

@end
