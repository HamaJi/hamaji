//
//  Bet365DrawViewController.m
//  Bet365
//
//  Created by luke on 2018/8/8.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Bet365DrawViewController.h"
#import "NetWorkMannager+Account.h"
#import "NSDictionary+Property.h"
#import "Bet365DrawModel.h"
#import "Bet365DrawCell.h"

@interface Bet365DrawViewController ()<
UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UIButton *startDateBtn;
@property (weak, nonatomic) IBOutlet UIButton *endDateBtn;
/**  */
@property (nonatomic,assign) NSInteger page;
/**  */
@property (nonatomic,strong) NSMutableArray *drawArrs;
/** 开始时间 */
@property (nonatomic,copy) NSString *startDate;
/** 结束时间 */
@property (nonatomic,copy) NSString *endDate;
@property (weak, nonatomic) IBOutlet UIView *topContentView;

@end

@implementation Bet365DrawViewController

RouterKey *const kRouterWithdrwalOrder = @"withdrawalOrder";

+(void)load{
    [self registerJumpRouterKey:kRouterWithdrwalOrder toHandle:^(NSDictionary *parameters) {
        Bet365DrawViewController *vc = [[Bet365DrawViewController alloc] initWithNibName:@"Bet365DrawViewController" bundle:nil];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }];
    
    [self registerInstanceRouterKey:kRouterWithdrwalOrder toHandle:^UIViewController *(NSDictionary *parameters) {
        Bet365DrawViewController *vc = [[Bet365DrawViewController alloc] initWithNibName:@"Bet365DrawViewController" bundle:nil];
        return vc;
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setupUI];
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    self.tableview.backgroundColor = [UIColor clearColor];
    self.topContentView.backgroundColor = [UIColor skinViewBgColor];
    
    UIImage *image = [[UIImage imageNamed:@"arrow_sorted_down"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.endDateBtn setImage:image forState:UIControlStateNormal];
    [self.endDateBtn.imageView setTintColor:[UIColor skinTextItemNorColor]];
    [self.endDateBtn.titleLabel setTintColor:[UIColor skinTextItemNorColor]];
    
    [self.startDateBtn setImage:image forState:UIControlStateNormal];
    [self.startDateBtn.imageView setTintColor:[UIColor skinTextItemNorColor]];
    [self.startDateBtn.titleLabel setTintColor:[UIColor skinTextItemNorColor]];
    

}

- (void)setupUI
{
    if ([self.tableview respondsToSelector:@selector(layoutMargins)]) {
        self.tableview.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    if ([self.tableview respondsToSelector:@selector(separatorInset)]) {
        self.tableview.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    self.tableview.rowHeight = UITableViewAutomaticDimension;
    self.tableview.tableFooterView = [UIView new];
    self.startDate = [NSDate getNewTimeFormat:@"yyyy-MM-dd"];
    self.endDate = self.startDate;
    [self.startDateBtn setTitle:self.startDate forState:UIControlStateNormal];
    [self.endDateBtn setTitle:self.startDate forState:UIControlStateNormal];
    [self.startDateBtn setImagePosition:LXMImagePositionRight spacing:10];
    [self.endDateBtn setImagePosition:LXMImagePositionRight spacing:10];
    [self setupRefresh];
}
- (IBAction)startDateClick:(UIButton *)sender {
    @weakify(self);
    [self showDatePickerdefaultSelValue:nil minDate:[[NSDate dateUTC] dateByAddingDays:-8] resultBlock:^(NSString *selectValue) {
        @strongify(self);
        self.startDate = selectValue;
        NSString *dateStr = [NSDate dateFormatterWithInputDateString:selectValue inputDateStringFormatter:[NSDate ymdFormat] outputDateStringFormatter:@"yyyy/MM/dd"];
        [sender setTitle:dateStr forState:UIControlStateNormal];
        self.page = 1;
        [self.tableview.mj_header beginRefreshing];
    }];
}
- (IBAction)endDateClick:(UIButton *)sender {
    @weakify(self);
    [self showDatePickerdefaultSelValue:nil minDate:[[NSDate dateUTC] dateByAddingDays:-8] resultBlock:^(NSString *selectValue) {
        @strongify(self);
        self.endDate = selectValue;
        NSString *dateStr = [NSDate dateFormatterWithInputDateString:selectValue inputDateStringFormatter:[NSDate ymdFormat] outputDateStringFormatter:@"yyyy/MM/dd"];
        [sender setTitle:dateStr forState:UIControlStateNormal];
        self.page = 1;
        [self.tableview.mj_header beginRefreshing];
    }];
}

- (void)setupRefresh
{
    self.tableview.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self queryData];
    }];
    
    self.tableview.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        self.page ++;
        [self queryData];
    }];
    
    [self.tableview.mj_header beginRefreshing];
}

- (void)queryData
{
    self.tableview.isShowEmpty = NO;
    [NET_DATA_MANAGER requestGetUserDrawPage:self.page Rows:10 StartDate:self.startDate EndDate:self.endDate Success:^(id responseObject) {
        if (self.page == 1) {
            [self.drawArrs removeAllObjects];
        }
        [self.drawArrs addObjectsFromArray:[Bet365DrawModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]]];
        [self.tableview reloadData];
        [self.tableview.mj_header endRefreshing];
        [self.tableview.mj_footer endRefreshing];
        self.tableview.isShowEmpty = YES;
        self.tableview.titleForEmpty = @"无记录";
        self.tableview.descriptionForEmpty = @"您暂未提交提现申请";
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [self.tableview.mj_header endRefreshing];
        [self.tableview.mj_footer endRefreshing];
        self.tableview.isShowEmpty = YES;
        self.tableview.titleForEmpty = @"数据异常";
        self.tableview.descriptionForEmpty = @"请检查网络设置";
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.drawArrs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Bet365DrawCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Bet365DrawCellID"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"Bet365DrawCell" owner:self options:nil] firstObject];
    }
    cell.model = self.drawArrs[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - 懒加载
- (NSMutableArray *)drawArrs
{
    if (_drawArrs == nil) {
        _drawArrs = [NSMutableArray array];
    }
    return _drawArrs;
}

@end
