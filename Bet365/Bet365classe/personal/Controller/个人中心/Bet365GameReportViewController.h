//
//  Bet365GameReportViewController.h
//  Bet365
//
//  Created by luke on 2018/8/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameCenterModel.h"

@interface Bet365GameReportViewController : Bet365ViewController
/** 类型 */
@property (nonatomic,assign) GameCenterType type;

@end
