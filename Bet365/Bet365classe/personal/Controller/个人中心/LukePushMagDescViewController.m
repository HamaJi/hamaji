//
//  LukePushMagDescViewController.m
//  Bet365
//
//  Created by luke on 17/6/15.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukePushMagDescViewController.h"
#import "LukePushMessageItem.h"
#import "LukeNoticeItem.h"
#import "NetWorkMannager+Account.h"
#import "BETAttributedTextView.h"
#import <WebKit/WebKit.h>

@interface LukePushMagDescViewController ()
<AttributedTextViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *titleLb;
@property (weak, nonatomic) IBOutlet UILabel *timeLb;
@property (weak, nonatomic) IBOutlet UIView *textView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *closeSubViewLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *closeDelegateLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fuzhiTariLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *closeLeadLayout;

/**  */
@property (nonatomic,strong) BETAttributedTextView *attributedView;
/**  */
@property (nonatomic,assign) CGRect viewMaxRect;
@end

@implementation LukePushMagDescViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setupUI];
}

- (IBAction)deleteClick:(id)sender {
    [SVProgressHUD showWithStatus:@"删除中..."];
    [NET_DATA_MANAGER requestGetRemoveMessageWithId:self.item.PushID Success:^(id responseObject) {
        [self closeClick:nil];
        [SVProgressHUD dismiss];
    } failure:^(NSError *error) {
        NSLog(@"%@",error.msg);
        [SVProgressHUD dismiss];
    }];
}

- (IBAction)closeClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)fuzhiClick:(id)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.item.messageContent;
    [SVProgressHUD showSuccessWithStatus:@"复制成功！"];
}


- (void)setupUI
{
    self.titleLb.text = self.item ? self.item.messageTitle : self.noticeItem.noticeTitle;
    self.timeLb.text = self.item ? self.item.addTime : self.noticeItem.updateTime;
    self.viewMaxRect = CGRectMake(0, 0, WIDTH - 30, CGFLOAT_HEIGHT_UNKNOWN);
    __block NSString *html = nil;
    if (self.noticeItem && ([self.noticeItem.noticeType integerValue] == 9 || [self.noticeItem.noticeType integerValue] == 7)) {
        NSDictionary *dict = [NSDictionary dictionaryWithJsonString:self.noticeItem.noticeContent];
        html = [dict objectForKey:@"default"];
        NSArray *subList = [dict objectForKey:@"subList"];
        [subList enumerateObjectsUsingBlock:^(NSDictionary  *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj[@"subKey"] integerValue] == [USER_DATA_MANAGER.userInfoData.hyLevel integerValue]) {
                html = [NSString stringWithFormat:@"%@<br />%@",html,obj[@"noticeContent"]];
            }
        }];
    }else{
        html = self.noticeItem.noticeContent;
    }
    NSString * htmlStr = self.item ? self.item.messageContent : html;
    self.attributedView = [BETAttributedTextView attributedTextViewWithMaxRect:self.viewMaxRect Html:htmlStr];
    [self.textView addSubview:self.attributedView];
    if (self.item) {
        self.closeDelegateLayout.priority = 1000;
        self.closeSubViewLayout.priority = 900;
        self.fuzhiTariLayout.priority = 1000;
        self.closeLeadLayout.priority = 900;
    }else{
        self.closeDelegateLayout.priority = 900;
        self.closeSubViewLayout.priority = 1000;
        self.fuzhiTariLayout.priority = 900;
        self.closeLeadLayout.priority = 1000;
    }
    
    if ([htmlStr containsString:@"<img "]) {
        self.attributedView.hidden = YES;
        WKWebView * webView = [WKWebView new];
        [webView loadHTMLString:htmlStr baseURL:nil];
        [self.textView addSubview:webView];
        [webView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.textView);
        }];
    }
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    @weakify(self);
    self.attributedView.sizeBlock = ^(CGSize textSize) {
        @strongify(self);
        if (textSize.height > self.textView.height) {
            self.attributedView.frame = CGRectMake(self.viewMaxRect.origin.x, self.viewMaxRect.origin.y, self.viewMaxRect.size.width, self.textView.height);
        }else{
           self.attributedView.frame = CGRectMake(self.viewMaxRect.origin.x, self.viewMaxRect.origin.y, self.viewMaxRect.size.width, textSize.height);
        }
    };
}

@end
