//
//  LukeRebateQRcodeViewController.m
//  Bet365
//
//  Created by luke on 17/7/10.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeRebateQRcodeViewController.h"
#import "NetWorkMannager+Account.h"

@interface LukeRebateQRcodeViewController ()
/**  */
@property (nonatomic,weak) UIImageView *scanImageView;

@end

@implementation LukeRebateQRcodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupUI];
    
    [self queryData];
}

- (void)setupUI
{
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"left_lage"] forState:UIControlStateNormal];
    [backBtn setTitle:@"返回" forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backBtn setImagePosition:LXMImagePositionLeft spacing:5];
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(statusHeight + 10);
        make.leading.equalTo(self.view).offset(10);
    }];
    
    UILabel *titleLb = [UILabel addLabelWithFont:17 color:[UIColor blackColor] title:@"二维码"];
    [self.view addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(backBtn);
        make.centerX.equalTo(self.view);
    }];
    
    UIView *lineView = [self addLineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLb.mas_bottom).offset(10);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(1);
    }];
    
    UIImageView *scanImageView = [[UIImageView alloc] init];
    scanImageView.userInteractionEnabled = YES;
    [self.view addSubview:scanImageView];
    [scanImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineView.mas_bottom).offset(40);
        make.leading.equalTo(self.view).offset(20);
        make.trailing.equalTo(self.view).offset(-20);
        make.height.mas_equalTo(scanImageView.mas_width);
    }];
    self.scanImageView = scanImageView;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick)];
    [self.scanImageView addGestureRecognizer:tap];
    
    UIButton *backButton = [UIButton addButtonWithTitle:@"返回" font:15 color:[UIColor whiteColor] target:self action:@selector(backClick)];
    backButton.backgroundColor = [UIColor redColor];
    backButton.layer.cornerRadius = 5;
    backButton.layer.masksToBounds = YES;
    [self.view addSubview:backButton];
    [backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-10 - SafeAreaBottomHeight);
        make.leading.equalTo(self.view).offset(10);
        make.trailing.equalTo(self.view).offset(-10);
        make.height.mas_equalTo(40);
    }];
}

- (void)tapClick
{
    [self alertWithTitle:@"温馨提示" message:@"是否保存二维码？" cancelActionTitle:@"取消" cancelHandler:nil sureActionTitle:@"确定" sureHandler:^(UIAlertAction *action) {
        [self loadImageFinished:self.scanImageView.image];
    }];
}

- (void)loadImageFinished:(UIImage *)image
{
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), (__bridge void *)self);
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    [SVProgressHUD showSuccessWithStatus:@"保存成功"];
}

- (void)queryData
{
    [SVProgressHUD showWithStatus:@"获取二维码..."];
    [NET_DATA_MANAGER requestGetQrcodeWithContent:self.url Success:^(id responseObject) {
        [SVProgressHUD dismiss];
        self.scanImageView.image = responseObject;
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [SVProgressHUD dismiss];
    }];
}

- (void)backClick
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

//创建线条
- (UIView *)addLineView
{
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = JesseGrayColor(221);
    [self.view addSubview:lineView];
    return lineView;
}

@end
