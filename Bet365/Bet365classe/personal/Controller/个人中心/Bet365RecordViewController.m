//
//  Bet365RecordViewController.m
//  Bet365
//
//  Created by luke on 2017/11/12.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "Bet365RecordViewController.h"
#import "LukeRecordViewController.h"
#import "LukeSportRecordViewController.h"
#import "LukeLiveRecordViewController.h"
#import "Bet365NavViewController.h"
#import "ElecWZRYRecordViewController.h"
#import "SportsModuleShare.h"
#import "Bet365GameReportViewController.h"
#import "LukeLiveRecordItem.h"
#import "NetWorkMannager+Account.h"
#import "LukeRecordViewController.h"
#import "UIViewControllerSerializing.h"

@interface Bet365RecordViewController ()<UITableViewDelegate,UITableViewDataSource,UIViewControllerSerializing>
/**  */
@property (nonatomic,weak) UIView *topView;
/**  */
@property (nonatomic,strong) UITableView *tableView;
/**  */
@property (nonatomic,strong) NSMutableArray *titlesArr;

@end

@implementation Bet365RecordViewController

RouterKey *const kRouterBetRecordChoose = @"betRecordChoose";

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    return [USER_DATA_MANAGER shouldPush2Login];
}

+(void)load{
    [self registerJumpRouterKey:kRouterBetRecordChoose toHandle:^(NSDictionary *parameters) {
        if (BET_CONFIG.common_config.isDP) {
            [[NAVI_MANAGER getCurrentVC] presentViewController:[[Bet365RecordViewController alloc] init] animated:YES completion:nil];
            
        }else{
            LukeRecordViewController *vc = [[LukeRecordViewController alloc] init];
            [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
            vc.hidesBottomBarWhenPushed = YES;
        }
    }];
}

- (NSMutableArray *)titlesArr
{
    if (_titlesArr == nil) {
        _titlesArr = [NSMutableArray array];
    }
    return _titlesArr;
}
- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.tableFooterView = [UIView new];
        if ([_tableView respondsToSelector:@selector(separatorInset)]) {
            _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        if ([_tableView respondsToSelector:@selector(layoutMargins)]) {
            _tableView.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.bottom.trailing.equalTo(self.view);
            make.top.equalTo(self.topView.mas_bottom);
        }];
    }
    return _tableView;
}
#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.titlesArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * const cellID = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    if (indexPath.row < 2){
        cell.textLabel.text = self.titlesArr[indexPath.row];
    }else{
        LukeLiveGamesItem *item = self.titlesArr[indexPath.row];
        cell.textLabel.text = item.name;
    }
    cell.textLabel.textColor = YCZColor(80, 89, 99);
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissViewControllerAnimated:YES completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            UIViewController *personVC = [NAVI_MANAGER getCurrentVC];
            if (indexPath.row == 0) {
                LukeRecordViewController *vc = [[LukeRecordViewController alloc] init];
                [personVC.navigationController pushViewController:vc animated:YES];
                vc.hidesBottomBarWhenPushed = YES;
            }else if (indexPath.row == 1) {
                
                LukeSportRecordViewController *vc = [[LukeSportRecordViewController alloc] init];
                [personVC.navigationController pushViewController:vc animated:YES];
                vc.hidesBottomBarWhenPushed = YES;

            }else{
                LukeLiveGamesItem *item = self.titlesArr[indexPath.row];
                if ([item.code isEqualToString:@"wzry"]) {
                    
                    ElecWZRYRecordViewController *vc = [[ElecWZRYRecordViewController alloc] init];
                    [personVC.navigationController pushViewController:vc animated:YES];
                    vc.hidesBottomBarWhenPushed = YES;

                }else{
                    LukeLiveRecordViewController *liveRecord = [[LukeLiveRecordViewController alloc] init];
                    liveRecord.code = item.code;
                    [personVC.navigationController pushViewController:liveRecord animated:YES];
                    liveRecord.hidesBottomBarWhenPushed = YES;
                }
            }
        });
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupTopView];
    
    [self getAllLiveGameData];
    self.view.backgroundColor = [UIColor skinViewScreenColor];
}

- (void)getAllLiveGameData
{
    [self.titlesArr addObjectsFromArray:@[@"彩票",@"体育"]];
    [self.titlesArr addObjectsFromArray:ALL_DATA.operatingGameList];
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

}

- (void)setupTopView
{
    self.view.backgroundColor = [UIColor whiteColor];
    UIView *topView = [[UIView alloc] init];
    topView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.top.equalTo(self.view);
        make.height.mas_equalTo(40 + statusHeight);
    }];
    self.topView = topView;
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = [[UIColor skinTextItemNorSubColor] colorWithAlphaComponent:0.1];
    [topView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.bottom.equalTo(topView);
        make.height.mas_equalTo(1);
    }];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"left_lage"] forState:UIControlStateNormal];
    [backBtn setTitle:@"返回" forState:UIControlStateNormal];
    [backBtn setTitleColor:YCZColor(80, 89, 99) forState:UIControlStateNormal];
    [backBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:backBtn];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(topView).offset(40 * 0.2);
        make.leading.equalTo(topView).offset(10);
    }];
}

- (void)backClick
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
