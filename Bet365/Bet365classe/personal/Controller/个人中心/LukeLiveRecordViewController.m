//
//  LukeSportRecordViewController.m
//  Bet365
//
//  Created by luke on 2017/11/7.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeLiveRecordViewController.h"
#import "LukeSearchTopView.h"
#import "LukeLiveRecordSelectView.h"
#import "LukeLiveRecordItem.h"
#import "LukeLiveRecordCell.h"
#import "Bet365LivePageData.h"
#import "NetWorkMannager+Account.h"

static NSString * const LukeLiveRecordCellID = @"LukeLiveRecordCell";
@interface LukeLiveRecordViewController ()
<LukeSearchTopViewDelegate,
UITableViewDelegate,
UITableViewDataSource,
LukeLiveRecordSelectDelegate>
/**  */
@property (nonatomic,strong) UITableView *tableView;
/** 顶部时间选择view */
@property (nonatomic,strong) LukeSearchTopView *searchView;
/** 数据数组 */
@property (nonatomic,strong) NSMutableArray *dataList;
/** 页数 */
@property (nonatomic,assign) NSInteger pages;
/** 筛选 */
@property (nonatomic,strong) LukeLiveRecordSelectView *selectView;
/**  */
@property (nonatomic,copy) NSString *userName;

@end

@implementation LukeLiveRecordViewController

- (NSMutableArray *)dataList
{
    if (_dataList == nil) {
        _dataList = [NSMutableArray array];
    }
    return _dataList;
}

- (LukeLiveRecordSelectView *)selectView
{
    if (_selectView == nil) {
        _selectView = [[LukeLiveRecordSelectView alloc] init];
        _selectView.delegate = self;
        [self.view addSubview:_selectView];
        [_selectView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(self.view);
            make.top.equalTo(self.view.mas_bottom);
            make.height.mas_equalTo(HEIGHT);
        }];
    }
    return _selectView;
}

#pragma mark - LukeSportSelectViewDelegate
- (void)liveRecordSelectCancelClick
{
    [UIView animateWithDuration:0.25 animations:^{
        self.selectView.transform = CGAffineTransformIdentity;
    }];
}

- (void)liveRecordSelectWithAccount:(NSString *)account code:(NSString *)code
{
    self.code = code;
    self.userName = account;
    [Bet365LivePageIntance queryDataWithLiveCode:self.code Completed:^{
        self.pages = 1;
        [self.tableView.mj_header beginRefreshing];
    }];
    [self liveRecordSelectCancelClick];
}

- (LukeSearchTopView *)searchView
{
    if (_searchView == nil) {
        _searchView = [[LukeSearchTopView alloc] initWithFrame:CGRectZero firstDateStr:[NSDate dateFormatterWithInputDateString:self.beginDateStr inputDateStringFormatter:[NSDate ymdFormat] outputDateStringFormatter:@"yyyy/MM/dd"] secondDateStr:[NSDate dateFormatterWithInputDateString:self.endDateStr inputDateStringFormatter:[NSDate ymdFormat] outputDateStringFormatter:@"yyyy/MM/dd"] title:@"筛选"];
        _searchView.delegate = self;
        [self.view addSubview:_searchView];
        [_searchView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(self.view);
            make.top.equalTo(self.view).offset(0);
            make.height.mas_equalTo(40);
        }];
    }
    return _searchView;
}

#pragma mark - LukeSearchTopViewDelegate
- (void)SearchTopViewfirstDateButtonClick
{
    [self showDatePickerdefaultSelValue:nil minDate:[NSDate offsetDays:-8 fromDate:[NSDate dateUTC]] resultBlock:^(NSString *selectValue) {
        self.beginDateStr = selectValue;
        [self.searchView.firstDateBtn setTitle:[[NSDate dateWithString:selectValue format:@"yyyy-MM-dd"] stringWithFormat:@"yyyy/MM/dd"] forState:UIControlStateNormal];
        self.pages = 1;
        [self.tableView.mj_header beginRefreshing];
    }];
}

- (void)SearchTopViewsecondDateButtonClick
{
    [self showDatePickerdefaultSelValue:nil minDate:[NSDate offsetDays:-8 fromDate:[NSDate dateUTC]] resultBlock:^(NSString *selectValue) {
        self.endDateStr = selectValue;
        [self.searchView.secondDateBtn setTitle:[[NSDate dateWithString:selectValue format:@"yyyy-MM-dd"] stringWithFormat:@"yyyy/MM/dd"] forState:UIControlStateNormal];
        self.pages = 1;
        [self.tableView.mj_header beginRefreshing];
    }];
}

- (void)SearchTopViewSearchButtonClick
{
    [UIView animateWithDuration:0.25 animations:^{
        self.selectView.transform = CGAffineTransformMakeTranslation(0, -HEIGHT);
    }];
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView registerClass:[LukeLiveRecordCell class] forCellReuseIdentifier:LukeLiveRecordCellID];
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedRowHeight = 200;
        _tableView.tableFooterView = [UIView new];
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.bottom.equalTo(self.view);
            make.top.equalTo(self.searchView.mas_bottom);
        }];
    }
    return _tableView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LukeLiveRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:LukeLiveRecordCellID];
    [cell setCellItem:self.dataList[indexPath.row] code:self.code];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self searchView];
    [self setupRefresh];
    [Bet365LivePageIntance queryDataWithLiveCode:self.code Completed:^{
        [self.tableView.mj_header beginRefreshing];
    }];
}

- (void)setupRefresh
{
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.pages = 1;
        [self queryData];
    }];
    self.tableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        self.pages ++;
        [self queryData];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)queryData
{
    self.tableView.isShowEmpty = NO;
    @weakify(self);
    [NET_DATA_MANAGER requestGetLiveBrUserName:self.userName Page:self.pages Rows:10 betStartDate:self.beginDateStr betEndDate:self.endDateStr gameCode:self.code Success:^(id responseObject){
        @strongify(self);
        if (self.pages == 1) {
            [self.dataList removeAllObjects];
        }
        [self.dataList addObjectsFromArray:[LukeLiveRecordItem mj_objectArrayWithKeyValuesArray:responseObject[@"data"]]];
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        self.tableView.titleForEmpty = @"没有投注记录";
        self.tableView.isShowEmpty = YES;
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        NSLog(@"%@",error.msg);
        self.tableView.titleForEmpty = @"网络异常，请检查网络";
        self.tableView.isShowEmpty = YES;
    }];
}

#pragma mark - 懒加载
- (NSString *)beginDateStr
{
    if (_beginDateStr == nil) {
        _beginDateStr = [NSDate getNewTimeFormat:[NSDate ymdFormat]];
    }
    return _beginDateStr;
}

- (NSString *)endDateStr
{
    if (_endDateStr == nil) {
        _endDateStr = self.beginDateStr;
    }
    return _endDateStr;
}

@end

