//
//  LukeDrawViewController.m
//  Bet365
//
//  Created by luke on 17/6/20.
//  Copyright © 2017年 jesse. All rights reserved.
#import "LukeDrawViewController.h"
#import "LukeDrawMoneyViewController.h"
#import "LukeDrawItem.h"
#import "LukeDrawCell.h"
#import "LukeUserDrawViewController.h"
#import "LukeMaskView.h"
#import "NetWorkMannager+Account.h"
#import "Bet365UserDrawModel.h"
#import "LukeInfoViewController.h"
#import "NetWorkMannager+activity.h"

static NSString *LukeDrawCellID = @"LukeDrawCell";

@interface LukeDrawViewController ()<UITableViewDataSource>
/**  */
@property (nonatomic,strong) LukeDrawMoneyViewController *drawMoneyVC;
/** tableview */
@property (nonatomic,strong) UITableView *tableView;
/**  */
@property (nonatomic,weak) UIButton *outMoneyBtn;
/** 请求返回的数据 */
@property (nonatomic,strong) NSArray *bankDatas;
/** 组头 */
@property (nonatomic,strong) UIView *headerView;
/** 请求返回的数据 */
@property (nonatomic,strong) NSDictionary *dict;
/** 模型 */
@property (nonatomic,strong) Bet365UserDrawAllModel *model;
/** 当前判断模型 */
@property (nonatomic,strong) Bet365UserDrawModel *drawModel;
@end

RouterKey *const kRouterDraw = @"draw";

@implementation LukeDrawViewController
+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"TEST"]) {
        [SVProgressHUD showErrorWithStatus:@"试玩帐号不能存取款项，请注册成为会员！"];
        return NO;
    }
    return [USER_DATA_MANAGER shouldPush2Login];
}

+(void)load{
    [self registerJumpRouterKey:kRouterDraw toHandle:^(NSDictionary *parameters) {
        
        LukeDrawViewController *vc = [[LukeDrawViewController alloc] init];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }];
    
    [self registerInstanceRouterKey:kRouterDraw toHandle:^UIViewController *(NSDictionary *parameters) {
        LukeDrawViewController *vc = [[LukeDrawViewController alloc] init];
        return vc;
    }];
    
}
- (UIView *)headerView
{
    if (_headerView == nil) {
        _headerView = [[UIView alloc] init];
        UILabel *deductLabel = [UILabel addLabelWithFont:16 color:[UIColor blackColor] title:nil];
        [_headerView addSubview:deductLabel];
        [deductLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_headerView).offset(10);
            make.leading.equalTo(_headerView).offset(10);
        }];
        
        UILabel *dateLabel = [UILabel addLabelWithFont:14 color:JesseGrayColor(85) title:nil];
        dateLabel.numberOfLines = 0;
        dateLabel.preferredMaxLayoutWidth = WIDTH - 20;
        [_headerView addSubview:dateLabel];
        [dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(deductLabel.mas_bottom).offset(10);
            make.leading.equalTo(_headerView).offset(10);
            make.trailing.equalTo(_headerView).offset(-10);
            make.bottom.equalTo(_headerView).offset(-10);
        }];
        
        if (self.drawModel.remainRequiredDml && self.model.remainRequiredDml) {
            if ([self.drawModel.remainRequiredDml floatValue] > 0) {
                deductLabel.text = @"未全部通过:";
                NSString *str = [NSString stringWithFormat:@"自上次提现后第一次充值起，截至%@，您的有效投注额为%.2f元，提现需扣除%.2f元。还需打码%.2f元即可通过全部流水审核。",[NSDate getNewTimeFormat:[NSDate ymdHmsFormat]],[self.model.sumAllDml floatValue],[self.model.sumAllDeduct floatValue],[self.model.remainRequiredDml floatValue]];
                NSMutableAttributedString *maStr = [[NSAttributedString alloc] initWithString:str].mutableCopy;
                [maStr addColor:[UIColor redColor] substring:[NSDate getNewTimeFormat:[NSDate ymdHmsFormat]]];
                [maStr addColor:[UIColor redColor] substring:StringFormatWithFloat([self.model.sumAllDml floatValue])];
                [maStr addColor:[UIColor redColor] substring:StringFormatWithFloat([self.model.sumAllDeduct floatValue])];
                [maStr addColor:[UIColor redColor] substring:StringFormatWithFloat([self.model.remainRequiredDml floatValue])];
                dateLabel.attributedText = maStr;
            }else{
                deductLabel.text = @"全部通过:";
                NSString *str = [NSString stringWithFormat:@"自上次提现后第一次充值起，截至%@，您的有效投注额为%.2f元，已通过全部流水审核。",[NSDate getNewTimeFormat:[NSDate ymdHmsFormat]],[self.model.sumAllDml floatValue]];
                NSMutableAttributedString *maStr = [[NSAttributedString alloc] initWithString:str].mutableCopy;
                [maStr addColor:[UIColor redColor] substring:[NSDate getNewTimeFormat:[NSDate ymdHmsFormat]]];
                [maStr addColor:[UIColor redColor] substring:StringFormatWithFloat([self.model.sumAllDml floatValue])];
                dateLabel.attributedText = maStr;
            }
        }else{
            NSString *str = [NSString stringWithFormat:@"自上次提现后第一次充值起，截至%@，您的有效投注额为%.2f元，提现需扣除%.2f元。",[NSDate getNewTimeFormat:[NSDate ymdHmsFormat]],[self.model.sumAllDml floatValue],[self.model.sumAllDeduct floatValue]];
            NSMutableAttributedString *maStr = [[NSAttributedString alloc] initWithString:str].mutableCopy;
            [maStr addColor:[UIColor redColor] substring:[NSDate getNewTimeFormat:[NSDate ymdHmsFormat]]];
            [maStr addColor:[UIColor redColor] substring:StringFormatWithFloat([self.model.sumAllDml floatValue])];
            [maStr addColor:[UIColor redColor] substring:StringFormatWithFloat([self.model.sumAllDeduct floatValue])];
            dateLabel.attributedText = maStr;
        }
        CGFloat height = [_headerView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
        _headerView.ycz_height = height;
        _headerView.ycz_width = WIDTH;
    }
    return _headerView;
}
- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.dataSource = self;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedRowHeight = 200;
        _tableView.tableFooterView = [UIView new];
        [_tableView registerClass:[LukeDrawCell class] forCellReuseIdentifier:LukeDrawCellID];
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.top.trailing.equalTo(self.view);
            make.bottom.equalTo(self.outMoneyBtn.mas_top).offset(-5);
        }];
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self addControllers];
    
    [self queryBankData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (([USER_DATA_MANAGER.userInfoData.transferStatus integerValue] == 1 || [BET_CONFIG.config.user_transfer_stauts isEqualToString:@"1"]) && USER_DATA_MANAGER.isLogin) {
        [NET_DATA_MANAGER requestGetfreeTransfer:@"self" success:^(id responseObject) {
            
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }
}

- (LukeDrawMoneyViewController *)extracted {
    return [LukeDrawMoneyViewController alloc];
}

- (void)addControllers
{
    [self addChildViewController:[[self extracted] init]];
}

- (void)setupUI
{
    UIButton *outMoneyBtn = [UIButton addButtonWithTitle:@"继续出款" font:15 color:[UIColor whiteColor] target:self action:@selector(outMoneyClick)];
    [outMoneyBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];
    outMoneyBtn.layer.cornerRadius = 5;
    outMoneyBtn.layer.masksToBounds = YES;
    [self.view addSubview:outMoneyBtn];
    [outMoneyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.view).offset(20);
        make.trailing.equalTo(self.view).offset(-20);
        make.bottom.equalTo(self.view).offset(-5 - SafeAreaBottomHeight);
        make.height.mas_equalTo(30);
    }];
    self.outMoneyBtn = outMoneyBtn;
}

- (void)outMoneyClick
{
    @weakify(self);
    [self checkSetdrawPassword:^{
        @strongify(self);
        LukeUserDrawViewController *draw = [[LukeUserDrawViewController alloc] init];
        [draw getDrawMessage:self.drawModel];
        [self.navigationController pushViewController:draw animated:YES];
        draw.hidesBottomBarWhenPushed = YES;
    }];
}

- (void)queryBankData
{
    if (USER_DATA_MANAGER.userInfoData.userBank && [USER_DATA_MANAGER.userInfoData.userBank aji_getAllkey].count) {
        [self queryAllBank];
    }else{
        [self addLukeDrawMoneyView];
    }
}

- (void)queryAllBank
{
    [SVProgressHUD showWithStatus:@"数据加载中..."];
    @weakify(self);
    void (^drawSetHide)() = ^(){
        @strongify(self);
        if (self.drawModel) {
            if (self.drawModel.limit.dayMaxCount && [self.drawModel.limit.dayMaxCount integerValue] == 0) {
                return;
            }
            if (self.drawModel.limit.allowDml&&self.drawModel.allDmlPass) {
                if ([self.drawModel.limit.allowDml integerValue]==1&&![self.drawModel.allDmlPass boolValue]) {
                    [self.outMoneyBtn setHidden:YES];
                    [[jesseGuideUIConfig initShareManager] alertView:NormalAlertType AlertImageName:nil title:@"温馨提示" detail:@"当前打码量不足,请在打码量足够后再申请提款!" TitleColor:JesseColor(125, 125, 125) andNextAlertOpen:NO];
                    return;
                }
            }
            if (self.drawModel.limit.dayMaxCountLimit) {
                if([self.drawModel.limit.dayMaxCountLimit integerValue]==1){
                    if([self.drawModel.count intValue]>=[self.drawModel.limit.dayMaxCount intValue]){
                        [self.outMoneyBtn setHidden:YES];
                        [[jesseGuideUIConfig initShareManager] alertView:NormalAlertType AlertImageName:nil title:@"温馨提示" detail:@"当天提现次数超限,请明天申请提款!" TitleColor:JesseColor(125, 125, 125) andNextAlertOpen:NO];
                        return;
                    }
                }
            }
            
        }
    };
    dispatch_group_t group = dispatch_group_create();//并发请求处理
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [NET_DATA_MANAGER requestGetUserWithdrawSuccess:^(id responseObject) {
            self.model = [Bet365UserDrawAllModel mj_objectWithKeyValues:responseObject];
            
            dispatch_semaphore_signal(sema);
        } failure:^(NSError *error) {
            
            dispatch_semaphore_signal(sema);
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [NET_DATA_MANAGER requestGetQueryOutMoneyIndexSuccess:^(id responseObject) {
            self.drawModel = [Bet365UserDrawModel mj_objectWithKeyValues:responseObject];
            dispatch_semaphore_signal(sema);
        } failure:^(NSError *error) {
            dispatch_semaphore_signal(sema);
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    });
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
        [self setupUI];//添加按钮
        self.tableView.tableHeaderView = self.headerView;
        [self.tableView reloadData];
        drawSetHide();//隐藏取款
        [self checkSetdrawPassword:nil];
    });
}
-(void)checkSetdrawPassword:(void(^)())complement{
    //检测是否已经配置取款密码
    if (!USER_DATA_MANAGER.userInfoData.fundPwd) {
        if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"HY"]) {
            LukeInfoViewController *infoC = [[LukeInfoViewController alloc] init];
            [infoC setIsDraw:YES];
            [self.navigationController pushViewController:infoC animated:YES];
            infoC.hidesBottomBarWhenPushed = YES;
            [SVProgressHUD showErrorWithStatus:@"请先设置取款密码!"];
            return ;
        }
    }
    complement?complement():nil;
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.model.rows.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LukeDrawCell *cell = [tableView dequeueReusableCellWithIdentifier:LukeDrawCellID];
    cell.item = self.model.rows[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)addLukeDrawMoneyView
{
    LukeDrawMoneyViewController *drawVC = self.childViewControllers[0];
    if (drawVC.isViewLoaded) return;
    [self.view addSubview:drawVC.view];
    [drawVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

@end
