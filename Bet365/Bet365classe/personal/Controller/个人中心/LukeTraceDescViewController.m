//
//  LukeRecordDescViewController.m
//  Bet365
//
//  Created by luke on 17/6/22.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeTraceDescViewController.h"
#import "LukeTraceItem.h"
#import "KFCPHomeGameJsonModel.h"
#import "LukeUserAdapter.h"
#import "NetWorkMannager+Account.h"

#define labelW  WIDTH / 3

@interface LukeTraceDescViewController ()
{
    UILabel *gameNameLb;
    UILabel *orderNoLb;
    UILabel *betInfoLb;
}
/**  */
@property (nonatomic,weak) UIScrollView *scrollView;
/** 请求的数据数组 */
@property (nonatomic,strong) NSArray *turnArrs;

@end

@implementation LukeTraceDescViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = IS_IPHONE_5s ? @"" : @"注单详情";
    [self queryData];
}

#pragma mark - 请求数据
- (void)queryData
{
    [SVProgressHUD showWithStatus:@"获取数据中..."];
    [NET_DATA_MANAGER requestGetTraceWithTraceOrderNo:self.item.orderNo Success:^(id responseObject) {
        self.turnArrs = [LukeTraceTurnItem mj_objectArrayWithKeyValuesArray:responseObject];
        [SVProgressHUD dismiss];
        [self setupUI];
        [self setupBottomView];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [SVProgressHUD dismiss];
    }];
}

- (void)setupBottomView
{
    UIView *topView = [[UIView alloc] init];
    topView.backgroundColor = [UIColor blackColor];
    [self.scrollView addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.equalTo(self.scrollView);
        make.width.mas_equalTo(WIDTH);
        make.top.equalTo(betInfoLb.mas_bottom).offset(10);
        make.height.mas_equalTo(40);
    }];
    
    UILabel *lb1 = [self addLbWithFont:IS_IPHONE_5s ? 15 : 16 color:[UIColor whiteColor] title:@"奖期" textAlignment:NSTextAlignmentCenter];
    [topView addSubview:lb1];
    UILabel *lb2 = [self addLbWithFont:IS_IPHONE_5s ? 15 : 16 color:[UIColor whiteColor] title:@"追号倍数" textAlignment:NSTextAlignmentCenter];
    [topView addSubview:lb2];
    UILabel *lb3 = [self addLbWithFont:IS_IPHONE_5s ? 15 : 16 color:[UIColor whiteColor] title:@"追号状态" textAlignment:NSTextAlignmentCenter];
    [topView addSubview:lb3];
    [topView.subviews mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:0 leadSpacing:0 tailSpacing:0];
    [topView.subviews mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(topView);
    }];
    
    UILabel *lastLabel;
    
    for (int i = 0; i < self.turnArrs.count; i ++) {
        LukeTraceTurnItem *turnItem = self.turnArrs[i];
        UILabel *label1 = [self addLbWithFont:17 color:[UIColor blackColor] title:turnItem.turnNum textAlignment:NSTextAlignmentCenter];
        [self.scrollView addSubview:label1];
        [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
            if (lastLabel) {
                make.top.equalTo(lastLabel.mas_bottom);
            }else{
                make.top.equalTo(topView.mas_bottom);
            }
            make.leading.equalTo(self.scrollView);
            make.size.mas_equalTo(CGSizeMake(labelW, 30));
        }];
        
        UILabel *label2 = [self addLbWithFont:17 color:[UIColor blackColor] title:[NSString stringWithFormat:@"%ld倍",turnItem.multiple] textAlignment:NSTextAlignmentCenter];
        [self.scrollView addSubview:label2];
        [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(label1.mas_top);
            make.leading.equalTo(label1.mas_trailing);
            make.size.mas_equalTo(CGSizeMake(labelW, 30));
        }];
        
        UILabel *label3 = [self addLbWithFont:17 color:[UIColor redColor] title:nil textAlignment:NSTextAlignmentCenter];
        [self.scrollView addSubview:label3];
        [label3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(label1.mas_top);
            make.leading.equalTo(label2.mas_trailing);
            make.size.mas_equalTo(CGSizeMake(labelW, 30));
        }];
        if (turnItem.status == 0) {
            label3.text = @"进行中";
        }else if (turnItem.status == 1) {
            label3.text = @"已完成";
        }else{
            label3.text = @"撤销";
        }
        
        lastLabel = label1;
    }
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(lastLabel.mas_bottom);
    }];
}

- (void)setupUI
{
    self.automaticallyAdjustsScrollViewInsets = NO;
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    [self.view addSubview:scrollView];
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.leading.trailing.bottom.equalTo(self.view);
        make.width.mas_equalTo(WIDTH);
    }];
    self.scrollView = scrollView;
    
    orderNoLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:[NSString stringWithFormat:@"追号编号 %@",self.item.orderNo]];
    [orderNoLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scrollView).offset(10);
        make.leading.equalTo(self.scrollView).offset(10);
    }];
    
    UILabel *accountLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:[NSString stringWithFormat:@"用户 %@",self.item.account]];
    [accountLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(orderNoLb.mas_bottom).offset(10);
        make.leading.equalTo(self.scrollView).offset(10);
    }];
    
    UILabel *timeLabel = [self addLabelWithFont:15 color:JesseGrayColor(77) title:self.item.addTime];
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(accountLb.mas_top);
        make.leading.equalTo(self.scrollView).offset(WIDTH * 0.5);
    }];
    
    gameNameLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:nil];
    [gameNameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(accountLb.mas_bottom).offset(10);
        make.leading.equalTo(self.scrollView).offset(10);
    }];
    
    UILabel *cateNameLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:[NSString stringWithFormat:@"玩法 %@",self.item.cateName]];
    [cateNameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(gameNameLb.mas_top);
        make.leading.equalTo(self.scrollView).offset(WIDTH * 0.5);
    }];
    
    UILabel *betModelLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:nil];
    [betModelLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(gameNameLb.mas_bottom).offset(10);
        make.leading.equalTo(self.scrollView).offset(10);
    }];
    
    if (self.item.betModel == 0) {
        betModelLb.text = @"游戏模式 元";
    }else if (self.item.betModel == 1) {
        betModelLb.text = @"游戏模式 角";
    }else{
        betModelLb.text = @"游戏模式 分";
    }
    
    UILabel *turnNumLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:[NSString stringWithFormat:@"开始期号 %@",self.item.turnNum]];
    [turnNumLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(betModelLb.mas_top);
        make.leading.equalTo(self.scrollView).offset(WIDTH * 0.5);
    }];
    
    UILabel *traceCountLb =  [self addLabelWithFont:15 color:JesseGrayColor(77) title:[NSString stringWithFormat:@"追号期数 %ld",self.item.traceCount]];
    [traceCountLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(betModelLb.mas_bottom).offset(10);
        make.leading.equalTo(self.scrollView).offset(10);
    }];
    
    UILabel *finishCountLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:[NSString stringWithFormat:@"完成期数 %ld",self.item.finishCount]];
    [finishCountLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(traceCountLb.mas_top);
        make.leading.equalTo(self.scrollView).offset(WIDTH * 0.5);
    }];
    
    UILabel *cancelCountLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:[NSString stringWithFormat:@"取消期数 %ld",self.item.cancelCount]];
    [cancelCountLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(traceCountLb.mas_bottom).offset(10);
        make.leading.equalTo(self.scrollView).offset(10);
    }];
    
    UILabel *finishMoneyLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:[NSString stringWithFormat:@"完成金额 %@",[LukeUserAdapter formatFloat:[NSString stringWithFormat:@"%f",self.item.finishMoney]]]];
    [finishMoneyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cancelCountLb.mas_top);
        make.leading.equalTo(self.scrollView).offset(WIDTH * 0.5);
    }];
    
    UILabel *winCountLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:[NSString stringWithFormat:@"中奖期数 %ld",self.item.winCount]];
    [winCountLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cancelCountLb.mas_bottom).offset(10);
        make.leading.equalTo(self.scrollView).offset(10);
    }];

    UILabel *traceTotalMoneyLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:[NSString stringWithFormat:@"完成金额 %@",[LukeUserAdapter formatFloat:[NSString stringWithFormat:@"%f",self.item.traceTotalMoney]]]];
    [traceTotalMoneyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(winCountLb.mas_top);
        make.leading.equalTo(self.scrollView).offset(WIDTH * 0.5);
    }];
    
    UILabel *winTraceMoneyLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:[NSString stringWithFormat:@"派奖总额 %@",self.item.odds]];
    [winTraceMoneyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(traceTotalMoneyLb.mas_bottom).offset(10);
        make.leading.equalTo(self.scrollView).offset(10);
    }];
    
    UILabel *cancelMoneyLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:[NSString stringWithFormat:@"取消金额 %@",[LukeUserAdapter formatFloat:[NSString stringWithFormat:@"%f",self.item.cancelMoney]]]];
    [cancelMoneyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(winTraceMoneyLb.mas_top);
        make.leading.equalTo(self.scrollView).offset(WIDTH * 0.5);
    }];
    
    UILabel *statusLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:nil];
    [statusLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(winTraceMoneyLb.mas_bottom).offset(10);
        make.leading.equalTo(self.scrollView).offset(10);
    }];
    if (self.item.status == 0) {
        statusLb.text = @"状态 进行中";
    }else if (self.item.status == 1){
        statusLb.text = @"状态 已完成";
    }else{
        statusLb.text = @"状态 撤销";
    }
    
    UILabel *stopAfterWinLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:nil];
    [stopAfterWinLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(statusLb.mas_top);
        make.leading.equalTo(self.scrollView).offset(WIDTH * 0.5);
    }];
    if (self.item.stopAfterWin == 1) {
        stopAfterWinLb.text = @"中后停止追号 是";
    }else{
        stopAfterWinLb.text = @"中后停止追号 否";
    }
    
    betInfoLb = [self addLabelWithFont:15 color:JesseGrayColor(77) title:[NSString stringWithFormat:@"追号内容 %@",self.item.betInfo]];
    betInfoLb.numberOfLines = 0;
    [betInfoLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(statusLb.mas_bottom).offset(10);
        make.leading.equalTo(self.view).offset(10);
        make.trailing.equalTo(self.view).offset(-10);
    }];
    
    [self Getlottery];
}

#pragma mark - 彩种玩法的处理
- (void)Getlottery
{
    NSString *str = nil;
    for (KFCPHomeGameJsonModel *model in LOTTERY_FACTORY.lotteryData) {
        if (self.item.gameId == [model.GameId integerValue]) {
            str = model.name;
        }
    }
    gameNameLb.text = [NSString stringWithFormat:@"彩种 %@",str];
}

//创建线条
- (UIView *)addLineView
{
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = JesseColor(228, 225, 225);
    [self.view addSubview:lineView];
    return lineView;
}

- (UILabel *)addLbWithFont:(CGFloat)font color:(UIColor *)color title:(NSString *)title textAlignment:(NSTextAlignment)textAlignment
{
    UILabel *label = [[UILabel alloc] init];
    label.textAlignment = textAlignment;
    label.text = title;
    label.textColor = color;
    label.font = [UIFont systemFontOfSize:font];
    return label;
}

//创建label
- (UILabel *)addLabelWithFont:(CGFloat)font color:(UIColor *)color title:(NSString *)title
{
    UILabel *label = [[UILabel alloc] init];
    label.text = title;
    label.textColor = color;
    label.font = [UIFont systemFontOfSize:font];
    [self.scrollView addSubview:label];
    return label;
}

@end
