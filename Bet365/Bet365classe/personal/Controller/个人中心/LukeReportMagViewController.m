//
//  LukeReportMagViewController.m
//  Bet365
//
//  Created by luke on 17/6/15.
//  Copyright © 2017年 jesse. All rights reserved.
//账户记录

#import "LukeReportMagViewController.h"
#import "LukeReportMagSelectView.h"
#import "LukeReportMagCell.h"
#import "LukeReportMagItem.h"
#import "LukeSearchTopView.h"
#import "LukeUserAdapter.h"
#import "NetWorkMannager+Account.h"

static NSString * const LukeReportMagCellID = @"LukeReportMagCell";

@interface LukeReportMagViewController ()
<UITableViewDelegate,
UITableViewDataSource,
LukeSearchTopViewDelegate,
LukeReportMagSelectViewDelegate,
UIViewControllerSerializing>
/** 开始时间 */
@property (nonatomic,copy) NSString *startDateStr;
/** 结束时间 */
@property (nonatomic,copy) NSString *endDateStr;
/** 头部view */
@property (nonatomic,strong) LukeSearchTopView *searchTopView;
/** 页数 */
@property (nonatomic,assign) NSInteger page;
/** 数据源数组 */
@property (nonatomic,strong) NSMutableArray *reportMagArrs;
/**  */
@property (nonatomic,strong) UITableView *tableView;
/**  */
@property (nonatomic,strong) LukeReportMagSelectView *selectView;
/**  */
@property (nonatomic,copy) NSString *typeID;
/** 总笔数 */
@property (nonatomic,weak) UILabel *totalCountLabel;
/** 总支出 */
@property (nonatomic,weak) UILabel *paySumLabel;
/** 总收入 */
@property (nonatomic,weak) UILabel *incomeSumLabel;
/** 模型 */
@property (nonatomic,strong) Bet365ReportItem *item;

@end

@implementation LukeReportMagViewController

RouterKey *const kRouterBills = @"bills";

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    return [USER_DATA_MANAGER shouldPush2Login];
}
+(void)load{
    [self registerJumpRouterKey:kRouterBills toHandle:^(NSDictionary *parameters) {
//        NSString *account = parameters[@"account"];
        LukeReportMagViewController *vc = [[LukeReportMagViewController alloc] init];
//        vc.account = account.length ? account : USER_DATA_MANAGER.userInfoData.account;
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }];
    
    [self registerInstanceRouterKey:kRouterBills toHandle:^UIViewController *(NSDictionary *parameters) {
//        NSString *account = parameters[@"account"];
        LukeReportMagViewController *vc = [[LukeReportMagViewController alloc] init];
//        vc.account = account.length ? account : USER_DATA_MANAGER.userInfoData.account;
        return vc;
    }];
    
}
- (LukeReportMagSelectView *)selectView
{
    if (_selectView == nil) {
        _selectView = [[LukeReportMagSelectView alloc] init];
        _selectView.delegate = self;
        [self.view addSubview:_selectView];
        [_selectView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_bottom);
            make.leading.trailing.equalTo(self.view);
            make.height.mas_equalTo(HEIGHT);
        }];
    }
    return _selectView;
}

#pragma mark - LukeReportMagSelectViewDelegate
- (void)ReportMagSelectViewWithAccount:(NSString *)account type:(NSString *)typeID
{
    self.account = account;
    self.typeID = typeID;
    [self ReportMagSelectViewCancelClick];
    self.page = 1;
    [self.tableView.mj_header beginRefreshing];
}

- (void)ReportMagSelectViewCancelClick
{
    [UIView animateWithDuration:0.3 animations:^{
        self.selectView.transform = CGAffineTransformIdentity;
    }];
}

- (LukeSearchTopView *)searchTopView
{
    if (_searchTopView == nil) {
        _searchTopView = [[LukeSearchTopView alloc] initWithFrame:CGRectZero firstDateStr:self.startDateStr secondDateStr:self.endDateStr title:@"筛选"];
        _searchTopView.delegate = self;
        [self.view addSubview:_searchTopView];
        [_searchTopView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view).offset(0);
            make.leading.trailing.equalTo(self.view);
            make.height.mas_equalTo(40);
        }];
    }
    return _searchTopView;
}

#pragma mark - LukeSearchTopViewDelegate
- (void)SearchTopViewfirstDateButtonClick
{
    [BRDatePickerView showDatePickerWithTitle:@"时间选择" dateType:BRDatePickerModeYMD defaultSelValue:self.startDateStr minDate:[[NSDate dateUTC] dateByAddingYears:-19] maxDate:[[NSDate dateUTC] dateByAddingYears:11] isAutoSelect:NO themeColor:[UIColor skinNaviBgColor] resultBlock:^(NSString *selectValue) {
        self.startDateStr = selectValue;
        [self.searchTopView.firstDateBtn setTitle:selectValue forState:UIControlStateNormal];
        self.page = 1;
        [self.tableView.mj_header beginRefreshing];
    }];
}

- (void)SearchTopViewsecondDateButtonClick
{
    [BRDatePickerView showDatePickerWithTitle:@"时间选择" dateType:BRDatePickerModeYMD defaultSelValue:self.endDateStr minDate:[[NSDate dateUTC] dateByAddingYears:-19] maxDate:[[NSDate dateUTC] dateByAddingYears:11] isAutoSelect:NO themeColor:[UIColor skinNaviBgColor] resultBlock:^(NSString *selectValue) {
        self.endDateStr = selectValue;
        [self.searchTopView.secondDateBtn setTitle:selectValue forState:UIControlStateNormal];
        self.page = 1;
        [self.tableView.mj_header beginRefreshing];
    }];
}

- (void)SearchTopViewSearchButtonClick
{
    [UIView animateWithDuration:0.3 animations:^{
        self.selectView.transform = CGAffineTransformMakeTranslation(0, -HEIGHT);
    }];
}
- (NSMutableArray *)reportMagArrs
{
    if (_reportMagArrs == nil) {
        _reportMagArrs = [NSMutableArray array];
    }
    return _reportMagArrs;
}
- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.backgroundColor = [UIColor skinViewScreenColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        if ([_tableView respondsToSelector:@selector(separatorInset)]) {
            _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        if ([_tableView respondsToSelector:@selector(layoutGuides)]) {
            _tableView.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedRowHeight = 160;
        [_tableView registerClass:[LukeReportMagCell class] forCellReuseIdentifier:LukeReportMagCellID];
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.bottom.equalTo(self.view);
            make.top.equalTo(self.view).offset(0 + 80);
        }];
    }
    return _tableView;
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.reportMagArrs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LukeReportMagCell *cell = [tableView dequeueReusableCellWithIdentifier:LukeReportMagCellID];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    cell.item = self.reportMagArrs[indexPath.row];
    
    return cell;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    
    [self setupRefresh];
    self.view.backgroundColor = [UIColor skinViewScreenColor];
}

- (void)setupUI
{
    self.endDateStr = [NSDate getNewTimeFormat:[NSDate ymdFormat]];
    self.startDateStr = self.endDateStr;
    [self searchTopView];
    
    UIView *topView = [[UIView alloc] init];
    topView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0 + 40);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    
    CGFloat labelW = WIDTH / 3;
    UILabel *totalCountLb = [UILabel addLabelWithFont:15 color:[UIColor whiteColor] title:@"总计"];
    totalCountLb.textAlignment = NSTextAlignmentCenter;
    [topView addSubview:totalCountLb];
    [totalCountLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topView);
        make.leading.equalTo(topView);
        make.size.mas_equalTo(CGSizeMake(labelW, 20));
    }];
    UILabel *totalCountLabel = [UILabel addLabelWithFont:15 color:[UIColor redColor] title:@"0笔"];
    totalCountLabel.textAlignment = NSTextAlignmentCenter;
    [topView addSubview:totalCountLabel];
    [totalCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(totalCountLb.mas_bottom);
        make.centerX.equalTo(totalCountLb);
    }];
    self.totalCountLabel = totalCountLabel;
    
    UILabel *paySumLb = [UILabel addLabelWithFont:15 color:[UIColor whiteColor] title:@"总支出"];
    paySumLb.textAlignment = NSTextAlignmentCenter;
    [topView addSubview:paySumLb];
    [paySumLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topView);
        make.centerX.equalTo(topView);
        make.size.mas_equalTo(CGSizeMake(labelW, 20));
    }];
    UILabel *paySumLabel = [UILabel addLabelWithFont:15 color:[UIColor redColor] title:@"0.00"];
    paySumLabel.textAlignment = NSTextAlignmentCenter;
    [topView addSubview:paySumLabel];
    [paySumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(paySumLb.mas_bottom);
        make.centerX.equalTo(paySumLb);
    }];
    self.paySumLabel = paySumLabel;
    
    UILabel *incomeSumLb = [UILabel addLabelWithFont:15 color:[UIColor whiteColor] title:@"总收入"];
    incomeSumLb.textAlignment = NSTextAlignmentCenter;
    [topView addSubview:incomeSumLb];
    [incomeSumLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topView);
        make.trailing.equalTo(topView);
        make.size.mas_equalTo(CGSizeMake(labelW, 20));
    }];
    UILabel *incomeSumLabel = [UILabel addLabelWithFont:15 color:[UIColor redColor] title:@"0.00"];
    incomeSumLabel.textAlignment = NSTextAlignmentCenter;
    [topView addSubview:incomeSumLabel];
    [incomeSumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(incomeSumLb.mas_bottom);
        make.centerX.equalTo(incomeSumLb);
    }];
    self.incomeSumLabel = incomeSumLabel;
}

- (void)setupRefresh
{
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self queryData];
    }];
    
    [self.tableView.mj_header beginRefreshing];
    
    self.tableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        self.page ++;
        [self queryData];
    }];
}

#pragma mark - 网络请求
- (void)queryData
{
    self.tableView.isShowEmpty = NO;
    [NET_DATA_MANAGER requestGetPageBillWithAccount:self.account beginDate:self.startDateStr endDate:self.endDateStr tranType:self.typeID page:self.page rows:20 Success:^(id responseObject) {
        self.item = [Bet365ReportItem mj_objectWithKeyValues:responseObject];
        self.totalCountLabel.text = self.item.totalCount ? [self.item.totalCount stringValue] : @"0笔";
        self.paySumLabel.text = self.item.otherData.paySum ? StringFormatWithFloat([self.item.otherData.paySum floatValue]) : @"0.00";
        self.incomeSumLabel.text = self.item.otherData.incomeSum ? StringFormatWithFloat([self.item.otherData.incomeSum floatValue]) : @"0.00";
        if (self.page == 1) {
            [self.reportMagArrs removeAllObjects];
        }
        [self.reportMagArrs addObjectsFromArray:[LukeReportMagItem mj_objectArrayWithKeyValuesArray:responseObject[@"data"]]];
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        self.tableView.isShowEmpty = YES;
        self.tableView.titleForEmpty = @"没有数据";
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        self.tableView.isShowEmpty = YES;
        self.tableView.titleForEmpty = @"网络异常";
    }];
}

@end
