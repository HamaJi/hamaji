//
//  LukeSubUsersViewController.m
//  Bet365
//
//  Created by luke on 17/6/27.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeSubUsersViewController.h"
#import "LukeSubUsersItem.h"
#import "LukeSubUsersCell.h"
#import "LukeTeamCountViewController.h"
#import "LukeReportMagViewController.h"
#import "LukeRegisterLbTFView.h"
#import "LukeSubUsersTableView.h"
#import "LukeSendMoneyView.h"
#import "LukeMaskView.h"
#import "LukeSubUsersSearchViewController.h"
#import "LukeUserAdapter.h"
#import "NetWorkMannager+Account.h"

static NSString * const LukeSubUsersCellID = @"LukeSubUsersCell";

@interface LukeSubUsersViewController ()
<UITableViewDataSource,
UITableViewDelegate,
LukeSubUsersTableViewDelegate,
LukeSendMoneyViewDelegate,
LukeSubUsersSearchViewControllerDelegate>
/**  */
@property (nonatomic,strong) UITableView *tableView;
/** 请求的数据数组 */
@property (nonatomic,strong) NSMutableArray *SubUserArrs;
/** 开始时间 */
@property (nonatomic,copy) NSString *startDateStr;
/** 结束时间 */
@property (nonatomic,copy) NSString *endDateStr;
/** 会员帐号 */
@property (nonatomic,copy) NSString *account;
/** 最小金额 */
@property (nonatomic,copy) NSString *minMoney;
/** 最大金额 */
@property (nonatomic,copy) NSString *maxMoney;
/** 会员详情 */
@property (nonatomic,strong) UIView *huiyuanView;
/** 会员模型 */
@property (nonatomic,strong) LukeSubUsersItem *subItem;
/** 页数 */
@property (nonatomic,assign) NSInteger page;
/**  */
@property (nonatomic,strong) LukeSubUsersTableView *subUserTableView;
/** 返水比率view */
@property (nonatomic,strong) LukeSendMoneyView *sendMoneyView;
/** 记录 */
@property (nonatomic,strong) LukeSubUsersItem *recordItem;
/**  */
@property (nonatomic,copy) NSString *subAccount;
@end

RouterKey *const kRouterAgentSubList = @"agent/subList";

@implementation LukeSubUsersViewController

+(void)load{
    
    [self registerJumpRouterKey:kRouterAgentSubList toHandle:^(NSDictionary *parameters) {
        NSString *value = parameters[@"account"];
        if (value.length) {
            if ([USER_DATA_MANAGER shouldPush2Login]) {
                LukeSubUsersViewController *vc = [[LukeSubUsersViewController alloc] init];
                [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
                vc.hidesBottomBarWhenPushed = YES;
            }
        }
    }];
    
    [self registerInstanceRouterKey:kRouterAgentSubList toHandle:^UIViewController *(NSDictionary *parameters) {
        LukeSubUsersViewController *vc = [[LukeSubUsersViewController alloc] init];
        return vc;
    }];
    
    
}

- (LukeSendMoneyView *)sendMoneyView
{
    if (_sendMoneyView == nil) {
        _sendMoneyView = [[LukeSendMoneyView alloc] initWithFrame:CGRectZero item:self.subItem];
        _sendMoneyView.delegate = self;
        [self.view addSubview:_sendMoneyView];
        [_sendMoneyView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_bottom);
            make.leading.trailing.equalTo(self.view);
            make.height.mas_equalTo(HEIGHT);
        }];
    }
    return _sendMoneyView;
}

#pragma mark - LukeSendMoneyViewDelegate
- (void)sendMoneyCancelClick
{
    [UIView animateWithDuration:0.3 animations:^{
        self.sendMoneyView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        [self.sendMoneyView removeFromSuperview];
        self.sendMoneyView = nil;
    }];
}
- (void)sendMoneyLoadWithAccount:(NSNumber *)account rebate:(NSString *)rebate
{
    [self sendMoneyCancelClick];
    [SVProgressHUD showWithStatus:@"修改中..."];
    [NET_DATA_MANAGER requestGetUpdateRebateWithUserId:account rebate:rebate Success:^(id responseObject) {
        [SVProgressHUD dismiss];
        self.page = 1;
        [self.tableView.mj_header beginRefreshing];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [SVProgressHUD dismiss];
    }];
}

- (LukeSubUsersTableView *)subUserTableView
{
    if (_subUserTableView == nil) {
        _subUserTableView = [[LukeSubUsersTableView alloc] initWithFrame:CGRectZero item:self.subItem account:self.recordItem];
        _subUserTableView.delegate = self;
        [self.view addSubview:_subUserTableView];
        [_subUserTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_bottom);
            make.leading.trailing.equalTo(self.view);
            make.height.mas_equalTo(HEIGHT);
        }];
    }
    return _subUserTableView;
}

#pragma mark - LukeSubUsersTableViewDelegate
- (void)SubUsersTableViewSelectWithIndex:(NSInteger)index
{
    [self SubUsersTableViewCancelClick];
    switch (index) {
        case 0:
        {
            [UIView animateWithDuration:0.3 animations:^{
                self.huiyuanView.transform = CGAffineTransformMakeTranslation(0, -HEIGHT);
            }];
            break;
        }
        case 1:
        {
            LukeTeamCountViewController *teamVC = [[LukeTeamCountViewController alloc] init];
            teamVC.userId = self.subItem.userId;
            teamVC.rwsType = PersonTeamRwsType_Team;
            [self.navigationController pushViewController:teamVC animated:YES];
            teamVC.hidesBottomBarWhenPushed = YES;
            break;
        }
        case 2:
        {
            LukeReportMagViewController *reportVC = [[LukeReportMagViewController alloc] init];
            reportVC.account = self.subItem.account;
            [self.navigationController pushViewController:reportVC animated:YES];
            reportVC.hidesBottomBarWhenPushed = YES;
            break;
        }
        case 3:
        {
            if (!(!BET_CONFIG.config.site_rebate_model || [BET_CONFIG.config.site_rebate_model isEqualToString:@"0"])) {
                self.account = nil;
                self.minMoney = nil;
                self.maxMoney = nil;
                self.startDateStr = nil;
                self.endDateStr = nil;
                self.recordItem = self.subItem;
                self.subAccount = self.subItem.account;
                self.page = 1;
                [self.tableView.mj_header beginRefreshing];
                return;
            }
            if ([[LukeUserAdapter decimalNumberWithId:self.subItem.rebate] isEqualToString:[LukeUserAdapter decimalNumberWithId:[USER_DATA_MANAGER.userInfoData.rebate doubleValue]]]) {
                [self alertWithSureTitle:@"无法更改用户返水" message:@"用户返水已为最高"];
            }else{
                [UIView animateWithDuration:0.3 animations:^{
                    self.sendMoneyView.transform = CGAffineTransformMakeTranslation(0, -HEIGHT);
                }];
            }
            break;
        }
        case 4:
        {
            self.account = nil;
            self.minMoney = nil;
            self.maxMoney = nil;
            self.startDateStr = nil;
            self.endDateStr = nil;
            self.recordItem = self.subItem;
            self.subAccount = self.subItem.account;
            self.page = 1;
            [self.tableView.mj_header beginRefreshing];
            break;
        }
        default:
            break;
    }
}

- (void)SubUsersTableViewCancelClick
{
    [UIView animateWithDuration:0.3 animations:^{
        self.subUserTableView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        [self.subUserTableView removeFromSuperview];
        self.subUserTableView = nil;
    }];
}

- (NSMutableArray *)SubUserArrs
{
    if (_SubUserArrs == nil) {
        _SubUserArrs = [NSMutableArray array];
    }
    return _SubUserArrs;
}
- (UIView *)huiyuanView
{
    if (_huiyuanView == nil) {
        _huiyuanView = [[UIView alloc] init];
        _huiyuanView.backgroundColor = [JesseGrayColor(10) colorWithAlphaComponent:0.3];
        [self.view addSubview:_huiyuanView];
        [_huiyuanView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(self.view);
            make.top.equalTo(self.view.mas_bottom);
            make.height.mas_equalTo(HEIGHT);
        }];
        
        UIView *contentView = [[UIView alloc] init];
        contentView.backgroundColor = JesseColor(239, 239, 244);
        [_huiyuanView addSubview:contentView];
        
        LukeRegisterLbTFView *accountView = [LukeRegisterLbTFView registerLbBtnViewWithTitle:@"用户帐号:" btnImage:nil btnTitle:self.subItem.account];
        [contentView addSubview:accountView];
        [accountView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.trailing.equalTo(contentView);
            make.height.mas_equalTo(40);
        }];
        
        LukeRegisterLbTFView *fullNameView = [LukeRegisterLbTFView registerLbBtnViewWithTitle:@"真实姓名:" btnImage:nil btnTitle:self.subItem.fullName];
        [contentView addSubview:fullNameView];
        [fullNameView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(accountView.mas_bottom);
            make.leading.trailing.equalTo(contentView);
            make.height.mas_equalTo(40);
        }];
        
        LukeRegisterLbTFView *typeView = [LukeRegisterLbTFView registerLbBtnViewWithTitle:@"用户类型:" btnImage:nil btnTitle:self.subItem.isDl ? @"代理" : @"会员"];
        [contentView addSubview:typeView];
        [typeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(fullNameView.mas_bottom);
            make.leading.trailing.equalTo(contentView);
            make.height.mas_equalTo(40);
        }];
        
        LukeRegisterLbTFView *loginTimeView = [LukeRegisterLbTFView registerLbBtnViewWithTitle:@"最后登录时间:" btnImage:nil btnTitle:self.subItem.loginTime];
        [contentView addSubview:loginTimeView];
        [loginTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(typeView.mas_bottom);
            make.leading.trailing.equalTo(contentView);
            make.height.mas_equalTo(40);
        }];
        
        UIButton *closeBtn = [UIButton addButtonWithTitle:@"关闭" font:17 color:[UIColor whiteColor] target:self action:@selector(closeClick)];
        closeBtn.backgroundColor = [UIColor redColor];
        closeBtn.layer.cornerRadius = 5;
        closeBtn.layer.masksToBounds = YES;
        [contentView addSubview:closeBtn];
        [closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(loginTimeView.mas_bottom).offset(20);
            make.leading.equalTo(contentView).offset(10);
            make.trailing.equalTo(contentView).offset(-10);
            make.height.mas_equalTo(40);
            make.bottom.equalTo(contentView.mas_bottom).offset(-10-SafeAreaBottomHeight);
        }];
        
        [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.bottom.equalTo(_huiyuanView);
        }];
    }
    return _huiyuanView;
}

- (void)closeClick
{
    [UIView animateWithDuration:0.3 animations:^{
        self.huiyuanView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        [self.huiyuanView removeFromSuperview];
        self.huiyuanView = nil;
    }];
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.backgroundColor = [UIColor skinViewScreenColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedRowHeight = 50;
        [_tableView registerClass:[LukeSubUsersCell class] forCellReuseIdentifier:LukeSubUsersCellID];
        _tableView.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0);
        _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _tableView.tableFooterView = [UIView new];
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.bottom.equalTo(self.view);
            make.top.equalTo(self.view).offset(75);
        }];
        _tableView.titleForEmpty = @"没有数据";
    }
    return _tableView;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.SubUserArrs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LukeSubUsersCell *cell = [tableView dequeueReusableCellWithIdentifier:LukeSubUsersCellID];
    cell.item = self.SubUserArrs[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIView *View = cell.contentView;
    View.backgroundColor = [UIColor skinViewBgColor];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.subItem = self.SubUserArrs[indexPath.row];
    [UIView animateWithDuration:0.3 animations:^{
        self.subUserTableView.transform = CGAffineTransformMakeTranslation(0, -HEIGHT);
    }];
}

- (void)queryData
{
    self.tableView.isShowEmpty = NO;
    [NET_DATA_MANAGER requestGetSubUsersWithStartDate:self.startDateStr endDate:self.endDateStr moneyFrom:self.minMoney moneyTo:self.maxMoney page:self.page rows:20 account:self.account subAccount:self.subAccount Success:^(id responseObject) {
        if (self.page == 1) {
            [self.SubUserArrs removeAllObjects];
        }
        [self.SubUserArrs addObjectsFromArray: [LukeSubUsersItem mj_objectArrayWithKeyValuesArray:responseObject[@"data"]]];
        [self.tableView reloadData];
        self.tableView.isShowEmpty = YES;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    } failure:^(NSError *error) {
        NSLog(@"%@",error.msg);
        self.tableView.isShowEmpty = YES;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    
    [self setRefresh];
    self.view.backgroundColor = [UIColor skinViewScreenColor];
}

- (void)setRefresh
{
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self queryData];
    }];
    [self.tableView.mj_header beginRefreshing];
    self.tableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        self.page ++;
        [self queryData];
    }];
}

//搜索
- (void)searchPresentClick
{
    LukeSubUsersSearchViewController *searchVC = [[LukeSubUsersSearchViewController alloc] init];
    searchVC.delegate = self;
    [self presentViewController:searchVC animated:YES completion:nil];
}

- (void)reloadSubUserSearchWithAccount:(NSString *)account moneyFrom:(NSString *)moneyFrom moneyTo:(NSString *)moneyTo startDate:(NSString *)startDate endDate:(NSString *)endDate
{
    self.recordItem = nil;
    self.account = account;
    self.minMoney = moneyFrom;
    self.maxMoney = moneyTo;
    self.startDateStr = startDate;
    self.endDateStr = endDate;
    self.page = 1;
    [self.tableView.mj_header beginRefreshing];
}

- (void)setupUI
{
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    
    UIImage *image = [[UIImage imageNamed:@"search"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    

    UIButton *searchBtn = [UIButton addButtonWithTitle:@"搜索" font:16 color:[UIColor skinViewKitSelColor] target:self action:@selector(searchPresentClick)];
    [searchBtn setImage:image forState:UIControlStateNormal];
    [searchBtn.imageView setTintColor:[UIColor skinViewKitSelColor]];

    [searchBtn setImagePosition:LXMImagePositionLeft spacing:10];
    searchBtn.layer.cornerRadius = 5;
    searchBtn.layer.masksToBounds = YES;
    searchBtn.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:searchBtn];
    [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(5);
        make.leading.equalTo(self.view).offset(5);
        make.trailing.equalTo(self.view).offset(-5);
        make.height.mas_equalTo(30);
    }];
    
    UIView *titleView = [[UIView alloc] init];
    titleView.backgroundColor = [UIColor skinViewContentColor];
    [self.view addSubview:titleView];
    [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(searchBtn.mas_bottom).offset(5);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(35);
    }];
    
    NSArray *titleArrs = @[@"用户名",@"类型",@"等级",@"余额",@"返点",@"状态"];
    for (int i = 0; i < titleArrs.count; i ++) {
        UILabel *label = [UILabel addLabelWithFont:15 color:[UIColor whiteColor] title:titleArrs[i]];
        label.textAlignment = NSTextAlignmentCenter;
        [titleView addSubview:label];
    }
    [titleView.subviews mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:0 leadSpacing:0 tailSpacing:0];
    [titleView.subviews mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(titleView);
    }];
    
}

#pragma mark - 懒加载
- (NSString *)subAccount
{
    if (_subAccount == nil) {
        _subAccount = USER_DATA_MANAGER.userInfoData.account;
    }
    return _subAccount;
}

@end

