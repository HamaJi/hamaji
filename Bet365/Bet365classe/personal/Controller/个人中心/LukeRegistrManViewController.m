//
//  LukeRegistrManViewController.m
//  Bet365
//
//  Created by luke on 17/6/27.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeRegistrManViewController.h"
#import "LukeRegisterLbTFView.h"
#import "LukeMaskView.h"
#import "LukeUserAdapter.h"
#import "NetWorkMannager+Account.h"
#import "SportsModuleShare.h"
#import "NSString+MacRegexCategory.h"
#import "UIViewControllerSerializing.h"
@interface LukeRegistrManViewController ()<UIViewControllerSerializing>
/** 用户类型 */
@property (nonatomic,weak) LukeRegisterLbTFView *accountTypeView;
/** 昵称 */
@property (nonatomic,weak) LukeRegisterLbTFView *nicknameView;
/** 真实姓名 */
@property (nonatomic,weak) LukeRegisterLbTFView *fullNameView;
/** 帐号 */
@property (nonatomic,weak) LukeRegisterLbTFView *accountView;
/** 密码 */
@property (nonatomic,weak) LukeRegisterLbTFView *passwordView;
/** 重复密码 */
@property (nonatomic,weak) LukeRegisterLbTFView *passwordSureView;
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *moneyView;
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *phoneView;
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *weixinView;
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *qqView;
/**  */
@property (nonatomic,weak) LukeRegisterLbTFView *emailView;

@end

RouterKey *const kRouterAgentAddUser = @"agent/addUser";

@implementation LukeRegistrManViewController

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    return [USER_DATA_MANAGER shouldPush2Login];
}

+(void)load{
    [self registerJumpRouterKey:kRouterAgentAddUser toHandle:^(NSDictionary *parameters) {
        LukeRegistrManViewController *vc = [[LukeRegistrManViewController alloc] init];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }];
    
    [self registerInstanceRouterKey:kRouterAgentAddUser toHandle:^UIViewController *(NSDictionary *parameters) {
        LukeRegistrManViewController *vc = [[LukeRegistrManViewController alloc] init];
        return vc;
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setupUI];
}

- (void)setupUI
{
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.backgroundColor = [UIColor skinViewScreenColor];
    [self.view addSubview:scrollView];
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.leading.trailing.bottom.equalTo(self.view);
        make.width.mas_equalTo(WIDTH);
    }];
    
    UILabel *titleLabel = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:@"添加下级用户"];
    [scrollView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(scrollView).offset(10);
        make.leading.equalTo(scrollView).offset(10);
    }];
    
    BOOL isSelect = NO;
    NSString *typeTitle = @"代理";
    if ([BET_CONFIG.config.dl_can_create_dl isEqualToString:@"1"] || !BET_CONFIG.config.dl_can_create_dl) {
        typeTitle = @"代理";
        isSelect = YES;
    }
    @weakify(self);
    LukeRegisterLbTFView *accountTypeView = [LukeRegisterLbTFView registerLbBtnViewWithTitle:@"用户类型" btnImage:@"right_lage" btnTitle:typeTitle];
    accountTypeView.callBlock = ^{
        if (isSelect) {
            @strongify(self);
            [BRStringPickerView showStringPickerWithTitle:nil dataSource:@[@"代理",@"会员"] defaultSelValue:typeTitle isAutoSelect:NO themeColor:[UIColor skinNaviBgColor] resultBlock:^(id selectValue) {
                [self.accountTypeView.dateBtn setTitle:selectValue forState:UIControlStateNormal];
            }];
        }
    };
    
    [scrollView addSubview:accountTypeView];
    [accountTypeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLabel.mas_bottom).offset(5);
        make.leading.trailing.equalTo(scrollView);
        make.size.mas_equalTo(CGSizeMake(WIDTH, 40));
    }];
    self.accountTypeView = accountTypeView;
    
    LukeRegisterLbTFView *nicknameView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"昵称" textEntry:NO placeholder:@"昵称" keyboardType:UIKeyboardTypeDefault];
    [scrollView addSubview:nicknameView];
    [nicknameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(accountTypeView.mas_bottom);
        make.leading.trailing.equalTo(scrollView);
        make.size.mas_equalTo(CGSizeMake(WIDTH, 40));
    }];
    self.nicknameView = nicknameView;
    
    LukeRegisterLbTFView *fullNameView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"真实姓名" textEntry:NO placeholder:@"须与你银行帐户名相同" keyboardType:UIKeyboardTypeDefault];
    [scrollView addSubview:fullNameView];
    [fullNameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(nicknameView.mas_bottom);
        make.leading.trailing.equalTo(scrollView);
        make.size.mas_equalTo(CGSizeMake(WIDTH, 40));
    }];
    self.fullNameView = fullNameView;
    
    LukeRegisterLbTFView *phoneView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"手机号码" textEntry:NO placeholder:@"请输入手机号码" keyboardType:UIKeyboardTypePhonePad];
    phoneView.clipsToBounds = YES;
    [scrollView addSubview:phoneView];
    [phoneView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(fullNameView.mas_bottom);
        make.leading.trailing.equalTo(scrollView);
        make.size.mas_equalTo(CGSizeMake(WIDTH, BET_CONFIG.registerConfig.phone != 2 ? 40 : 0));
    }];
    self.phoneView = phoneView;
    
//    LukeRegisterLbTFView *weixinView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"微信" textEntry:NO placeholder:@"请输入微信" keyboardType:UIKeyboardTypeDefault];
//    weixinView.clipsToBounds = YES;
//    [scrollView addSubview:weixinView];
//    [weixinView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(phoneView.mas_bottom);
//        make.leading.trailing.equalTo(scrollView);
//        make.size.mas_equalTo(CGSizeMake(WIDTH, BET_CONFIG.registerConfig.weixin != 2 ? 40 : 0));
//    }];
//    self.weixinView = weixinView;
//
//    LukeRegisterLbTFView *qqView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"QQ" textEntry:NO placeholder:@"请输入QQ" keyboardType:UIKeyboardTypePhonePad];
//    qqView.clipsToBounds = YES;
//    [scrollView addSubview:qqView];
//    [qqView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(weixinView.mas_bottom);
//        make.leading.trailing.equalTo(scrollView);
//        make.size.mas_equalTo(CGSizeMake(WIDTH, BET_CONFIG.registerConfig.qq != 2 ? 40 : 0));
//    }];
//    self.qqView = qqView;
//
//    LukeRegisterLbTFView *emailView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"邮箱" textEntry:NO placeholder:@"请输入邮箱" keyboardType:UIKeyboardTypeEmailAddress];
//    emailView.clipsToBounds = YES;
//    [scrollView addSubview:emailView];
//    [emailView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(qqView.mas_bottom);
//        make.leading.trailing.equalTo(scrollView);
//        make.size.mas_equalTo(CGSizeMake(WIDTH, BET_CONFIG.registerConfig.email != 2 ? 40 : 0));
//    }];
//    self.emailView = emailView;
    
    LukeRegisterLbTFView *accountView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"帐号" textEntry:NO placeholder:@"4-12个英文字母与数字" keyboardType:UIKeyboardTypeDefault];
    [accountView.textfield addTarget:self action:@selector(checkAccount:) forControlEvents:UIControlEventEditingDidEnd];
    [scrollView addSubview:accountView];
    [accountView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(phoneView.mas_bottom);
        make.leading.trailing.equalTo(scrollView);
        make.size.mas_equalTo(CGSizeMake(WIDTH, 40));
    }];
    self.accountView = accountView;
    
    LukeRegisterLbTFView *passwordView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"密码" textEntry:YES placeholder:@"密码" keyboardType:UIKeyboardTypeDefault];
    [passwordView.textfield addTarget:self action:@selector(checkPassWord:) forControlEvents:UIControlEventEditingDidEnd];
    [scrollView addSubview:passwordView];
    [passwordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(accountView.mas_bottom);
        make.leading.trailing.equalTo(scrollView);
        make.size.mas_equalTo(CGSizeMake(WIDTH, 40));
    }];
    self.passwordView = passwordView;
    
    LukeRegisterLbTFView *passwordSureView = [LukeRegisterLbTFView registerLbTFViewWithTitle:@"重复密码" textEntry:YES placeholder:@"请再输入一次密码" keyboardType:UIKeyboardTypeDefault];
    [scrollView addSubview:passwordSureView];
    [passwordSureView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(passwordView.mas_bottom);
        make.leading.trailing.equalTo(scrollView);
        make.size.mas_equalTo(CGSizeMake(WIDTH, 40));
    }];
    self.passwordSureView = passwordSureView;
    
    UILabel *selectLabel = nil;
    if ([BET_CONFIG.config.site_rebate_model isEqualToString:@"0"] || !BET_CONFIG.config.site_rebate_model) {
        
        UILabel *moneyLabel = [UILabel addLabelWithFont:15 color:[UIColor skinTextItemNorSubColor] title:@"奖金组"];
        [scrollView addSubview:moneyLabel];
        [moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(passwordSureView.mas_bottom).offset(10);
            make.leading.equalTo(scrollView).offset(10);
        }];
        
        LukeRegisterLbTFView *moneyView = [LukeRegisterLbTFView registerLbBtnViewWithTitle:@"选择奖金组" btnImage:@"right_lage" btnTitle:nil];
        moneyView.callBlock = ^{
            @strongify(self);
            [self getDlSubRebateRange];
        };
        [scrollView addSubview:moneyView];
        [moneyView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(moneyLabel.mas_bottom).offset(5);
            make.leading.trailing.equalTo(scrollView);
            make.size.mas_equalTo(CGSizeMake(WIDTH, 40));
        }];
        self.moneyView = moneyView;
        
        selectLabel = [UILabel addLabelWithFont:15 color:[UIColor skinViewKitSelColor] title:@"奖金组一旦上调，无法降低，请谨慎选择"];
        [scrollView addSubview:selectLabel];
        [selectLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(scrollView).offset(10);
            make.top.equalTo(moneyView.mas_bottom).offset(10);
        }];
    }
    
    UIButton *sureBtn = [UIButton addButtonWithTitle:@"添加" font:15 color:[UIColor skinViewKitNorColor] target:self action:@selector(sureClick:)];
    [sureBtn setBackgroundImage:[UIImage skinGradientSelImage] forState:UIControlStateNormal];
    [scrollView addSubview:sureBtn];
    sureBtn.layer.cornerRadius = 5;
    sureBtn.layer.masksToBounds = YES;
    [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        if ([BET_CONFIG.config.site_rebate_model isEqualToString:@"0"] || !BET_CONFIG.config.site_rebate_model) {
            make.top.equalTo(selectLabel.mas_bottom).offset(50);
        }else{
            make.top.equalTo(self.passwordSureView.mas_bottom).offset(50);
        }
        make.centerX.equalTo(scrollView);
        make.size.mas_equalTo(CGSizeMake(WIDTH - 20, 40));
        make.bottom.equalTo(scrollView.mas_bottom).offset(-150);
    }];
}

#pragma mark - 获取返点区间
- (void)getDlSubRebateRange
{
    if (SportsShareInstance.registrRebateArrs.count == 0) {
        [SVProgressHUD showWithStatus:@"获取奖金组"];
        [NET_DATA_MANAGER requestgetDlSubRebateRangeSuccess:^(id responseObject) {
            [SVProgressHUD dismiss];
            NSArray *rangeArr = (NSArray *)responseObject;
            if (rangeArr.count > 1) {
                double rebate = [USER_DATA_MANAGER.userInfoData.rebate floatValue];
                int total = rebate * 10;
                NSInteger endNum = [[rangeArr lastObject] floatValue] * 10;
                NSInteger startNum = [[rangeArr firstObject] floatValue] * 10;
                NSInteger teamNum = 1980 - (90 - endNum) * 2;
                NSMutableArray *temp = [NSMutableArray array];
                for (int i = endNum; i >= startNum; i --) {
                    [temp addObject:[NSString stringWithFormat:@"%d.%d0%%",(i / 10),(i % 10)]];
                }
                for (int i = 0; i < temp.count; i ++) {
                    [SportsShareInstance.registrRebateArrs addObject:[NSString stringWithFormat:@"%@---%ld",temp[i],(teamNum - 2 * i)]];
                }
                [BRStringPickerView showStringPickerWithTitle:nil dataSource:SportsShareInstance.registrRebateArrs defaultSelValue:nil isAutoSelect:NO themeColor:[UIColor skinNaviBgColor] resultBlock:^(id selectValue) {
                    [self.moneyView.dateBtn setTitle:selectValue forState:UIControlStateNormal];
                }];
            }
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
            [SVProgressHUD dismiss];
        }];
    }else{
        [BRStringPickerView showStringPickerWithTitle:nil dataSource:SportsShareInstance.registrRebateArrs defaultSelValue:nil isAutoSelect:NO themeColor:[UIColor skinNaviBgColor] resultBlock:^(id selectValue) {
            [self.moneyView.dateBtn setTitle:selectValue forState:UIControlStateNormal];
        }];
    }
}

#pragma mark - 验证帐号是否存在
- (void)checkAccount:(UITextField *)textField
{
    if (textField.text.length == 0) {
        return;
    }else if (textField.text.length<4){
        [SVProgressHUD showErrorWithStatus:@"账号少于四位,请重新输入"];
        return;
    }else if (textField.text.length>12){
        [SVProgressHUD showErrorWithStatus:@"不能大于12位,请重新输入"];
        return;
    }
    if ([LukeUserAdapter validateUserName:textField.text]) {
        [SVProgressHUD showWithStatus:@"验证中..."];
        [NET_DATA_MANAGER requestCheckUniqueType:0 val:textField.text Success:^(id responseObject) {
            if ([responseObject isEqualToString:@"true"]) {
                [SVProgressHUD showErrorWithStatus:@"该帐号已存在"];
            }
            [SVProgressHUD dismiss];
        } failure:^(NSError *error) {
            [SVProgressHUD dismiss];
            NSLog(@"%@",error);
        }];
    }else{
        self.accountView.textfield.text = nil;
        [SVProgressHUD showErrorWithStatus:@"输入的帐号有误，请重新输入！"];
    }
}
#pragma mark --进行正则处理判断
-(void)checkPassWord:(UITextField *)textFiled{
    
}
- (void)sureClick:(UIButton *)sender
{
    if (![LukeUserAdapter lastTriggerDateOfObject:sender greatThanInterval:1.5]) {
        return;
    }
    if (self.nicknameView.textfield.text.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"昵称必填哦"];
        return;
    }
    if (self.fullNameView.textfield.text.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"真实姓名必填哦"];
        return;
    }
    if (![LukeUserAdapter validateNickname:self.fullNameView.textfield.text]) {
        [SVProgressHUD showErrorWithStatus:@"真实姓名格式不对"];
        return;
    }
    if (BET_CONFIG.registerConfig.phone == 1) {
        if (self.phoneView.textfield.text.length <= 0) {
            [SVProgressHUD showErrorWithStatus:@"手机号码必填哦"];
            return;
        }else if (![LukeUserAdapter validateMobile:self.phoneView.textfield.text]) {
            [SVProgressHUD showErrorWithStatus:@"手机号码有误"];
            return;
        }
    }
//    if (BET_CONFIG.registerConfig.weixin == 1) {
//        if (self.weixinView.textfield.text.length <= 0) {
//            [SVProgressHUD showErrorWithStatus:@"微信必填哦"];
//            return;
//        }else if (![LukeUserAdapter validateWechat:self.weixinView.textfield.text]) {
//            [SVProgressHUD showErrorWithStatus:@"微信有误"];
//            return;
//        }
//    }
//    if (BET_CONFIG.registerConfig.qq == 1) {
//        if (self.qqView.textfield.text.length <= 0) {
//            [SVProgressHUD showErrorWithStatus:@"QQ必填哦"];
//            return;
//        }else if (![LukeUserAdapter validateIsValidQQ:self.weixinView.textfield.text]) {
//            [SVProgressHUD showErrorWithStatus:@"QQ有误"];
//            return;
//        }
//    }
//    if (BET_CONFIG.registerConfig.email == 1) {
//        if (self.emailView.textfield.text.length <= 0) {
//            [SVProgressHUD showErrorWithStatus:@"邮箱必填哦"];
//            return;
//        }else if (![LukeUserAdapter validateEmail:self.emailView.textfield.text]) {
//            [SVProgressHUD showErrorWithStatus:@"邮箱有误"];
//            return;
//        }
//    }
    if (self.accountView.textfield.text.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"帐号必填哦"];
        return;
    }
    if (![self.passwordView.textfield.text isPermissionllyPsw]) {
        return;
    }
    if (self.passwordSureView.textfield.text.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"重复密码必填哦"];
        return;
    }
    if (![self.passwordSureView.textfield.text isEqualToString:self.passwordView.textfield.text]) {
        [SVProgressHUD showErrorWithStatus:@"两次输入的密码不一致"];
        return;
    }
    NSString *rebateStr = nil;
    if ([BET_CONFIG.config.site_rebate_model isEqualToString:@"0"] || !BET_CONFIG.config.site_rebate_model) {
        if (self.moneyView.dateBtn.currentTitle.length <= 0) {
            [SVProgressHUD showErrorWithStatus:@"请选择奖金组"];
            return;
        }
        rebateStr = [self.moneyView.dateBtn.currentTitle substringToIndex:4];
    }
    NSString *password = nil;
    if (BET_CONFIG.config.user_password_type && [BET_CONFIG.config.user_password_type isEqualToString:@"0"]) {
        password = [self.passwordView.textfield.text MD5];
    }else{
        password = self.passwordView.textfield.text;
    }
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:@([self.accountTypeView.dateBtn.currentTitle isEqualToString:@"代理"]) forKey:@"userInfo.isDl"];
    if (self.nicknameView.textfield.text) {
        [parameters setObject:self.nicknameView.textfield.text forKey:@"userInfo.nickname"];
    }
    if (self.fullNameView.textfield.text) {
        [parameters setObject:self.fullNameView.textfield.text forKey:@"userInfo.fullName"];
    }
    if (self.accountView.textfield.text) {
        [parameters setObject:self.accountView.textfield.text forKey:@"userInfo.account"];
    }
    if (self.phoneView.textfield.text.length > 0) {
        [parameters setObject:self.phoneView.textfield.text forKey:@"userInfo.phone"];
    }
//    if (self.weixinView.textfield.text.length > 0) {
//        [parameters setObject:self.weixinView.textfield.text forKey:@"userInfo.weixin"];
//    }
//    if (self.qqView.textfield.text.length > 0) {
//        [parameters setObject:self.qqView.textfield.text forKey:@"userInfo.qq"];
//    }
//    if (self.emailView.textfield.text.length > 0) {
//        [parameters setObject:self.emailView.textfield.text forKey:@"userInfo.email"];
//    }
    if (password) {
        [parameters setObject:password forKey:@"userInfo.password"];
    }
    if (rebateStr) {
        [parameters setObject:rebateStr forKey:@"userInfo.rebate"];
    }
    [SVProgressHUD showWithStatus:@"添加中..."];
    [NET_DATA_MANAGER requestPostDlAddWithUserInfo:parameters Success:^(id responseObject) {
        [SVProgressHUD dismiss];
        [SVProgressHUD showSuccessWithStatus:@"添加成功"];
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [SVProgressHUD dismiss];
    }];
}

@end
