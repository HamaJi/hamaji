//
//  BindingPhoneViewController.m
//  Bet365
//
//  Created by super_小鸡 on 2019/9/7.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "BindingPhoneViewController.h"
#import "NetWorkMannager+Account.h"
#import "TimerManager.h"
#import "VerifyManager.h"
@interface BindingPhoneViewController ()
<UITextFieldDelegate,VerifyManagerDelegate>

#pragma mark - Kit
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UILabel *titileLalbel;

@property (weak, nonatomic) IBOutlet UIButton *bindingTypeBtn;

@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;

@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;

@property (weak, nonatomic) IBOutlet UILabel *smsCodeLabel;

@property (weak, nonatomic) IBOutlet UITextField *smsCodeTextField;

@property (weak, nonatomic) IBOutlet UIButton *smsCodeBtn;

@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@property (strong, nonatomic) BRStringPickerView  *pickerView;

@property (weak, nonatomic) IBOutlet UILabel *hasBingdingLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutConsttraintHeight;

#pragma mark - Data

@property (strong, nonatomic) NSMutableArray <NSString *>*pickData;

@property (nonatomic, strong) NSNumber *sendingSpacing;

@property (nonatomic) BOOL hasBinding;

@property (strong, nonatomic) VerifyManager *verifyManager;

@property (nonatomic,strong) NSURLSessionTask *verfifyTask;

@end

@implementation BindingPhoneViewController

NSString *const kSMSCodeUpdateKey = @"kSMSCodeUpdateKey";

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    [self addobser];
    [self updateBinding];
    // Do any additional setup after loading the view from its nib.
}
-(void)setUI{
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    self.hasBingdingLabel.textColor = [UIColor skinTextTitileColor];
    
    self.contentView.backgroundColor = [UIColor skinViewBgColor];
    self.titileLalbel.textColor = [UIColor skinTextItemNorColor];
    self.phoneLabel.textColor = [UIColor skinTextItemNorColor];
    self.smsCodeLabel.textColor = [UIColor skinTextItemNorColor];
    
    UIImage *right = [[UIImage imageNamed:@"right_lage"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    [self.bindingTypeBtn setImage:right forState:UIControlStateNormal];
//    [self.bindingTypeBtn setTintColor:[UIColor skinViewKitSelColor]];
    
    [self.phoneTextField setTextColor:[UIColor skinTextItemNorColor]];
    [self.smsCodeTextField setTextColor:[UIColor skinTextItemNorColor]];

    
    
    [self.smsCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    self.smsCodeBtn.backgroundColor = [UIColor skinViewKitSelColor];
    [self.smsCodeBtn setTitleColor:[UIColor skinViewKitNorColor] forState:UIControlStateNormal];
    self.smsCodeBtn.clipsToBounds = YES;
    self.smsCodeBtn.layer.borderColor = [[UIColor skinViewKitSelColor] CGColor];
    self.smsCodeBtn.layer.borderWidth = 1;
    self.smsCodeBtn.layer.cornerRadius = 15;
    [self.smsCodeBtn.titleLabel setFont:[UIFont systemFontOfSize:12]];
    
    [self.submitBtn setTitle:@"提交" forState:UIControlStateNormal];
    self.submitBtn.backgroundColor = [UIColor skinViewKitSelColor];
    [self.submitBtn setTitleColor:[UIColor skinViewKitNorColor] forState:UIControlStateNormal];
    self.submitBtn.clipsToBounds = YES;
    self.submitBtn.layer.borderColor = [[UIColor skinViewKitSelColor] CGColor];
    self.submitBtn.layer.borderWidth = 1;
    self.submitBtn.layer.cornerRadius = 10;
    [self.submitBtn.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [self.submitBtn setEnabled:NO];
    
    NSAttributedString *string = [[NSAttributedString alloc] initWithString:@"请输入手机号" attributes:@{NSForegroundColorAttributeName : [UIColor skinTextItemNorSubColor],NSFontAttributeName : [UIFont systemFontOfSize:15]}];
    self.phoneTextField.attributedPlaceholder = string;
//    UILabel *label = [self.phoneTextField valueForKey:@"_placeholderLabel"];
//    label.adjustsFontSizeToFitWidth = YES;
    
    string = [[NSAttributedString alloc] initWithString:@"请输短信验证码" attributes:@{NSForegroundColorAttributeName : [UIColor skinTextItemNorSubColor],NSFontAttributeName : [UIFont systemFontOfSize:15]}];
    self.smsCodeTextField.attributedPlaceholder = string;
//    label = [self.smsCodeTextField valueForKey:@"_placeholderLabel"];
//    label.adjustsFontSizeToFitWidth = YES;
    
    
}

-(void)updateBinding{
    if (!self.hasBinding) {
        self.bindingType = BindingPhoneType_MODIFY;
        self.phoneTextField.placeholder = @"请输入手机号";
        self.layoutConsttraintHeight.constant = 0;
    }else{
        self.bindingType = BindingPhoneType_CANCEL;
        
        NSAttributedString *string = [[NSAttributedString alloc] initWithString:[[USER_DATA_MANAGER.userInfoData.loginPhone componentsSeparatedByString:@"@"] safeObjectAtIndex:1] attributes:@{NSForegroundColorAttributeName : [UIColor skinTextItemNorSubColor],NSFontAttributeName : [UIFont systemFontOfSize:15]}];
        self.phoneTextField.attributedPlaceholder = string;
//        UILabel *label = [self.phoneTextField valueForKey:@"_placeholderLabel"];
//        label.adjustsFontSizeToFitWidth = YES;
        
        self.hasBingdingLabel.text = [NSString stringWithFormat:@"此账号一绑定手机号%@",string.string];
        self.layoutConsttraintHeight.constant = 20;
    }
}

-(void)dealloc{
    [TIMER_MANAGER removeDelegate:self];
}

-(void)addobser{
    
    @weakify(self);
    [[RACObserve(self, bindingType) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        BindingPhoneType type = [x integerValue];
        if (type == BindingPhoneType_CANCEL) {
            self.titileLalbel.text = @"解绑手机号";
        }else if (type == BindingPhoneType_MODIFY){
            if (self.hasBinding) {
                 self.titileLalbel.text = @"修改手机号";
            }else{
                 self.titileLalbel.text = @"绑定手机号";
            }
        }
    }];
    
    [[RACSignal combineLatest:@[self.phoneTextField.rac_textSignal,
                                [RACObserve(self, sendingSpacing) distinctUntilChanged]]
                       reduce:^id (NSString *text,NSNumber *sendingSpacing){
                           return @(text.length && [sendingSpacing integerValue] == 0);
                                }] subscribeNext:^(id  _Nullable x) {
                                    @strongify(self);
                                    if ([self.sendingSpacing integerValue]) {
                                        [self.smsCodeBtn setTitle:[NSString stringWithFormat:@"%lis后重新获取",[self.sendingSpacing integerValue]] forState:UIControlStateDisabled];
                                    }else{
                                        [self.smsCodeBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
                                    }
                                    [self.smsCodeBtn setEnabled:[x boolValue]];
                                }];
    
    [self.smsCodeTextField.rac_textSignal subscribeNext:^(NSString * _Nullable x) {
        @strongify(self);
        [self.submitBtn setEnabled:(x.length && self.phoneTextField.text)];
    }];
    
    
    
    [TIMER_MANAGER addDelegate:self];
    
}

-(void)timerWithKey:(NSString *)key andMaxCount:(NSInteger)maxCount andRestCount:(NSInteger)count{
    self.sendingSpacing = [NSNumber numberWithInteger:maxCount - count];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
}
#pragma mark - Events
- (IBAction)submitAction:(id)sender {
    if (!self.phoneTextField.text.length) {
        [SVProgressHUD showErrorWithStatus:@"请输入电话号码"];
        return;
    }
    if (!self.smsCodeTextField.text.length) {
        [SVProgressHUD showErrorWithStatus:@"请输入短信验证码"];
        return;
    }
    [self request];
}


- (IBAction)bindingAction:(id)sender {
    if (!self.hasBinding) {
        return;
    }
    NSString *value = [self.pickData safeObjectAtIndex:self.bindingType];
    @weakify(self);
    [BRStringPickerView showStringPickerWithTitle:value
                                       dataSource:self.pickData
                                  defaultSelValue:value
                                     isAutoSelect:YES
                                       themeColor:[UIColor skinNaviBgColor]
                                      resultBlock:^(id selectValue) {
                                          @strongify(self);
                                          [self.pickData enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                              if ([obj isEqualToString:selectValue]) {
                                                  self.bindingType = idx;
                                                  *stop = YES;
                                              }
                                          }];
    } cancelBlock:^{
        
    }];
}

- (IBAction)smsCodeAction:(id)sender {
    if ([self kBecomeFirstResponder]) {
        return;
    }
    if (!self.phoneTextField.text.length) {
        [SVProgressHUD showErrorWithStatus:@"请填写手机号码"];
        return;
    }
    [self requestGetVerifyType];
}

-(NSMutableArray<NSString *> *)pickData{
    if (!_pickData) {
        _pickData = @[].mutableCopy;
        if (self.hasBinding) {
            [_pickData addObject:@"解绑手机号"];
            [_pickData addObject:@"修改手机号"];
        }else{
            [_pickData addObject:@"绑定手机号"];
        }
    }
    return _pickData;
}

-(NSNumber *)sendingSpacing{
    if (!_sendingSpacing) {
        _sendingSpacing = [NSNumber numberWithInteger:[TIMER_MANAGER currentCountWithKey:kSMSCodeUpdateKey]];
    }
    return _sendingSpacing;
}

-(VerifyManager *)verifyManager{
    if (!_verifyManager) {
        _verifyManager = [[VerifyManager alloc] init];
        _verifyManager.launchImageCode = YES;
        _verifyManager.delegate = self;
    }
    return _verifyManager;
}

-(BOOL)hasBinding{
    if ([USER_DATA_MANAGER.userInfoData.loginPhone containsString:@"@****"]) {
        /*
         cef52e7bef4e845a74db72d54f6c1d5c@****5734
         若为此类格式，判定已绑定过手机
         */
        return YES;
    }else{
        return NO;
    }
}

#pragma mark - VerifyManagerDelegate
-(void)verifyManager:(VerifyManager *)manager updateVerifyAuthorType:(VerifyAuthorType)type{
    [self.verifyManager launchVerify];
}

-(void)verifyManager:(VerifyManager *)manager verificationSuccessParamers:(NSDictionary *)paramers authorType:(VerifyAuthorType)type{
    [self requestGetSMScode:paramers];
}

-(void)verifyManager:(VerifyManager *)manager verificationFaildAuthorType:(VerifyAuthorType)type{
    
}

#pragma mark - NetRequest
-(void)request{
    [self.bindingTypeBtn setEnabled:NO];
    [self.submitBtn setEnabled:NO];
    [self.smsCodeBtn setTitle:@"发送验证码" forState:UIControlStateDisabled];
    [self.smsCodeBtn setEnabled:NO];
    [SVProgressHUD showWithStatus:@"提交中..."];
    
    void(^requestSuccess)() = ^(){
        [self.bindingTypeBtn setEnabled:YES];
        [self.submitBtn setEnabled:YES];
        [self.smsCodeBtn setEnabled:YES];
        [SVProgressHUD showSuccessWithStatus:@"成功"];
        @weakify(self);
        [USER_DATA_MANAGER requestGetUserInfoCompleted:^(BOOL success) {
            @strongify(self);
            self.pickData = nil;
            [self updateBinding];
        }];
    };
    
    void(^requestFailed)(NSError *error) = ^(NSError *error){
        [self.bindingTypeBtn setEnabled:YES];
        [self.submitBtn setEnabled:YES];
        [self.smsCodeBtn setEnabled:YES];
    };
    
    if (self.bindingType == BindingPhoneType_CANCEL) {
        [NET_DATA_MANAGER requestCancleBindingPhone:self.phoneTextField.text smsCode:self.smsCodeTextField.text Success:^(id responseObject) {
            requestSuccess();
        } failure:^(NSError *error) {
            requestFailed(error);
        }];
    }else if (self.bindingType == BindingPhoneType_MODIFY){
        [NET_DATA_MANAGER requestModifyBindingPhone:self.phoneTextField.text smsCode:self.smsCodeTextField.text Success:^(id responseObject) {
            requestSuccess();
        } failure:^(NSError *error) {
            requestFailed(error);
        }];
    }
}


-(void)requestGetSMScode:(NSDictionary *)paramers{
    [self.smsCodeBtn setTitle:@"发送验证码" forState:UIControlStateDisabled];
    [self.smsCodeBtn setEnabled:NO];
    [NET_DATA_MANAGER requestUpdateSMScodeByPhone:self.phoneTextField.text
                                         Paramers:paramers
                                          Success:^(id responseObject) {
                                              [SVProgressHUD showSuccessWithStatus:@"验证码已发送至你手机"];
                                              [TIMER_MANAGER addTimerWithMaxCount:60 andKey:kSMSCodeUpdateKey andReduceScope:1];
                                          }
                                          failure:^(NSError *error) {
                                              [self requestGetVerifyType];
                                              [self.smsCodeBtn setEnabled:YES];
                                              if (error.LIMITED_ONLY_PHONE_LOGIN_ERROR) {
                                                  
                                              }
                                          }];
}

-(void)requestGetVerifyType{
    if (!self.phoneTextField.text.length) {
        return;
    }
    [SVProgressHUD showWithStatus:@"安全信息校验中。。。"];
    void(^handle)(id responseObject) = ^(id responseObject){
        self.verifyManager.requestJsonString = responseObject;
        self.verfifyTask = nil;
    };
    self.verfifyTask = [NET_DATA_MANAGER requestChangeSMSVerifyTypePhone:self.phoneTextField.text Success:^(id responseObject) {
        [SVProgressHUD dismiss];
        handle(responseObject);
    } failure:^(NSError *error) {
        handle(nil);
    }];
}

-(BOOL)kBecomeFirstResponder{
    BOOL isFirstResponder = [self.phoneTextField isFirstResponder] || [self.smsCodeTextField isFirstResponder];
    [self.view endEditing:YES];
    return isFirstResponder;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
