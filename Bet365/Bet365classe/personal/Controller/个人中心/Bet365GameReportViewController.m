//
//  Bet365DayReportViewController.m
//  Bet365
//
//  Created by luke on 2018/8/6.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "Bet365GameReportViewController.h"
#import "Bet365RecordTopTitleView.h"
#import "Bet365RecordViewCell.h"
#import "NetWorkMannager+Account.h"
#import "Bet365CpHistoryModel.h"
#import "LukeSearchTopView.h"
#import "LukeLiveRecordViewController.h"
#import "LukeSportRecordViewController.h"
#import "ElecWZRYRecordViewController.h"
#import "SportsModuleShare.h"
#import "LukeLiveRecordItem.h"
#import "UIViewControllerSerializing.h"
@interface Bet365GameReportViewController ()<
UITableViewDelegate,
UITableViewDataSource,
LukeSearchTopViewDelegate,
UIViewControllerSerializing>
/**  */
@property (nonatomic,strong) Bet365RecordTopTitleView *topTilteView;
/**  */
@property (nonatomic,strong) UITableView *tableView;
/**  */
@property (nonatomic,strong) NSArray *dayReportArrs;
/** 顶部时间选择 */
@property (nonatomic,strong) LukeSearchTopView *searchTopView;
/** 起始时间 */
@property (nonatomic,copy) NSString *beginDateStr;
/** 结束时间 */
@property (nonatomic,copy) NSString *endDateStr;

@end

RouterKey *const kRouterLiveBetRecord = @"record/live-report";

//record/live-report/chess
//record/live-report/sport
//record/live-report/live
//record/live-report/electronic
//record/live-report/vr


@implementation Bet365GameReportViewController

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    return [USER_DATA_MANAGER shouldPush2Login];
}

+(void)load{
    [self registerJumpRouterKey:kRouterLiveBetRecord toHandle:^(NSDictionary *parameters) {
        NSString *typeStr = [parameters objectForKey:@"type"];
        GameCenterType type;
        if ([typeStr isEqualToString:@"chess"]) {
            type = GameCenterType_chess;
        }else if ([typeStr isEqualToString:@"sport"]) {
            type = GameCenterType_sports;
        }else if ([typeStr isEqualToString:@"live"]) {
            type = GameCenterType_live;
        }else if ([typeStr isEqualToString:@"electronic"]) {
            type = GameCenterType_ele;
        }else {
            type = GameCenterType_vr;
        }
        Bet365GameReportViewController *reportVC = [[Bet365GameReportViewController alloc] init];
        reportVC.type = type;
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:reportVC animated:YES];
        reportVC.hidesBottomBarWhenPushed = YES;
    }];

    [self registerInstanceRouterKey:kRouterLiveBetRecord toHandle:^UIViewController *(NSDictionary *parameters) {
        NSString *typeStr = [parameters objectForKey:@"type"];
        GameCenterType type;
        if ([typeStr isEqualToString:@"chess"]) {
            type = GameCenterType_chess;
        }else if ([typeStr isEqualToString:@"sport"]) {
            type = GameCenterType_sports;
        }else if ([typeStr isEqualToString:@"live"]) {
            type = GameCenterType_live;
        }else if ([typeStr isEqualToString:@"electronic"]) {
            type = GameCenterType_ele;
        }else {
            type = GameCenterType_vr;
        }
        Bet365GameReportViewController *reportVC = [[Bet365GameReportViewController alloc] init];
        reportVC.type = type;
        return reportVC;
    }];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.beginDateStr = [NSDate getNewTimeFormat:[NSDate ymdFormat]];
    self.endDateStr = self.beginDateStr;
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    [self queryData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

#pragma mark - Private
- (NSMutableArray *)getAllLiveGamesWithGameCodes:(NSArray *)gameCodes
{
    NSMutableArray *temp = [NSMutableArray array];
    [ALL_DATA.operatingGameList enumerateObjectsUsingBlock:^(LukeLiveGamesItem * _Nonnull item, NSUInteger idx, BOOL * _Nonnull stop) {
        [gameCodes enumerateObjectsUsingBlock:^(NSString *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([item.code isEqualToString:obj]) {
                [temp addObject:item.code];
            }
        }];
    }];
    return temp;
}

- (void)queryData
{
    self.tableView.isShowEmpty = NO;
    NSString *gameCodes = nil;
    switch (self.type) {
        case GameCenterType_live:
        {
            NSMutableArray *gameArrs = [self getAllLiveGamesWithGameCodes:@[@"ag",@"bbin",@"lmg",@"og",@"dg",@"mg",@"ds",@"bg"]];
            gameCodes = [gameArrs componentsJoinedByString:@","];
        }
            break;
        case GameCenterType_vr:
        {
            gameCodes = @"vr";
        }
            break;
        case GameCenterType_ele:
        {
            NSMutableArray *gameArrs = [self getAllLiveGamesWithGameCodes:@[@"pt",@"cq9",@"jdb",@"hb",@"pg",@"ag",@"bbin",@"pt2",@"mg",@"gg",@"avia",@"im",@"ptNew",@"mg2",@"wzdz"]];
            //            if (BET_CONFIG.config.WZRYStatus) {
            //                if (![BET_CONFIG.config.WZRYStatus boolValue]) {
            //                    [gameArrs addObject:@"wzry"];
            //                }
            //            }else {
            //                if (BET_CONFIG.common_config.isWzry && [BET_CONFIG.common_config.isWzry boolValue]) {
            //                    [gameArrs addObject:@"wzry"];
            //                }
            //            }
            gameCodes = [gameArrs componentsJoinedByString:@","];
        }
            break;
        case GameCenterType_sports:
        {
            NSMutableArray *gameArrs = [self getAllLiveGamesWithGameCodes:@[@"hgSport",@"sb",@"m8"]];
            [gameArrs addObject:@"hgSport"];
            gameCodes = [gameArrs componentsJoinedByString:@","];
        }
            break;
        case GameCenterType_chess:
        {
            NSMutableArray *gameArrs = [self getAllLiveGamesWithGameCodes:@[@"qly",@"ky",@"lucky",@"wz",@"nw",@"dfw",@"as"]];
            gameCodes = [gameArrs componentsJoinedByString:@","];
        }
            break;
        default:
            break;
    }
    
    [NET_DATA_MANAGER requestGetGameStartDate:self.beginDateStr EndDate:self.endDateStr gameCodes:gameCodes Success:^(id responseObject) {
        self.dayReportArrs = [Bet365GameReportListModel mj_objectArrayWithKeyValuesArray:responseObject];
        [self.tableView reloadData];
        [SVProgressHUD dismiss];
        self.tableView.isShowEmpty = YES;
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        self.tableView.isShowEmpty = YES;
    }];
}

//- (void)queryData{
//    self.tableView.isShowEmpty = NO;
//    NSString *gameCodes = nil;
//    switch (self.type) {
//        case GameCenterType_live:
//        {
//            NSMutableArray *gameArrs = [ALL_DATA.allGameCodeClassifyData objectForKey:@"LIVE"];
//            gameCodes = [gameArrs componentsJoinedByString:@","];
//        }
//            break;
//        case GameCenterType_vr:
//        {
//            gameCodes = @"vr";
//        }
//            break;
//        case GameCenterType_ele:
//        {
//
//            NSMutableArray *gameArrs = @[].mutableCopy;
//            [gameArrs safeAddObjectsFromArray:[ALL_DATA.allGameCodeClassifyData objectForKey:@"DJ"]];
//            [gameArrs safeAddObjectsFromArray:[ALL_DATA.allGameCodeClassifyData objectForKey:@"DZ"]];
//            gameCodes = [gameArrs componentsJoinedByString:@","];
//        }
//            break;
//        case GameCenterType_sports:
//        {
//            NSMutableArray *gameArrs = [ALL_DATA.allGameCodeClassifyData objectForKey:@"SP"];
//            [gameArrs addObject:@"hgSport"];
//            gameCodes = [gameArrs componentsJoinedByString:@","];
//        }
//            break;
//        case GameCenterType_chess:
//        {
//            NSMutableArray *gameArrs = [ALL_DATA.allGameCodeClassifyData objectForKey:@"QP"];
//            gameCodes = [gameArrs componentsJoinedByString:@","];
//        }
//            break;
//        default:
//            break;
//    }
//
//    [NET_DATA_MANAGER requestGetGameStartDate:self.beginDateStr EndDate:self.endDateStr gameCodes:gameCodes Success:^(id responseObject) {
//        self.dayReportArrs = [Bet365GameReportListModel mj_objectArrayWithKeyValuesArray:responseObject];
//        [self.tableView reloadData];
//        [SVProgressHUD dismiss];
//        self.tableView.isShowEmpty = YES;
//    } failure:^(NSError *error) {
//        [SVProgressHUD dismiss];
//        self.tableView.isShowEmpty = YES;
//    }];
//}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dayReportArrs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Bet365RecordViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Bet365RecordViewCell"];
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"Bet365RecordViewCell" owner:self options:nil].firstObject;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.gameModel = self.dayReportArrs[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Bet365GameReportListModel *model = self.dayReportArrs[indexPath.row];
    if ([model.gameCode isEqualToString:@"hgSport"]) {
        LukeSportRecordViewController *sportVC = [[LukeSportRecordViewController alloc] init];
        sportVC.beginDateStr = self.beginDateStr;
        sportVC.endDateStr = self.endDateStr;
        sportVC.status = SportRecordStatusGet;
        sportVC.timeType = SportRecordTimeTypeSet;
        [self.navigationController pushViewController:sportVC animated:YES];
        sportVC.hidesBottomBarWhenPushed = YES;
    }else if ([model.gameCode isEqualToString:@"wzry"]) {
        ElecWZRYRecordViewController *wzryVC = [[ElecWZRYRecordViewController alloc] init];
        wzryVC.startDateTime = self.beginDateStr;
        wzryVC.endDateTime = self.endDateStr;
        [self.navigationController pushViewController:wzryVC animated:YES];
        wzryVC.hidesBottomBarWhenPushed = YES;
    }else{
        LukeLiveRecordViewController *liveRecord = [[LukeLiveRecordViewController alloc] init];
        liveRecord.code = model.gameCode;
        liveRecord.beginDateStr = self.beginDateStr;
        liveRecord.endDateStr = self.endDateStr;
        [self.navigationController pushViewController:liveRecord animated:YES];
        liveRecord.hidesBottomBarWhenPushed = YES;
    }
}

#pragma mark - LukeSearchTopViewDelegate
- (void)SearchTopViewSearchButtonClick
{
    [SVProgressHUD show];
    [self queryData];
}

- (void)SearchTopViewfirstDateButtonClick
{
    @weakify(self);
    [self showDatePickerdefaultSelValue:nil minDate:[[NSDate dateUTC] dateByAddingDays:-8] resultBlock:^(NSString *selectValue) {
        @strongify(self);
        self.beginDateStr = selectValue;
        [self.searchTopView.firstDateBtn setTitle:[NSDate dateFormatterWithInputDateString:selectValue inputDateStringFormatter:[NSDate ymdFormat] outputDateStringFormatter:@"yyyy/MM/dd"] forState:UIControlStateNormal];
        [SVProgressHUD show];
        [self queryData];
    }];
}

- (void)SearchTopViewsecondDateButtonClick
{
    @weakify(self);
    [self showDatePickerdefaultSelValue:nil minDate:[[NSDate dateUTC] dateByAddingDays:-8] resultBlock:^(NSString *selectValue) {
        @strongify(self);
        self.endDateStr = selectValue;
        [self.searchTopView.secondDateBtn setTitle:[NSDate dateFormatterWithInputDateString:selectValue inputDateStringFormatter:[NSDate ymdFormat] outputDateStringFormatter:@"yyyy/MM/dd"] forState:UIControlStateNormal];
        [SVProgressHUD show];
        [self queryData];
    }];
}

#pragma mark - 懒加载
- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        if ([_tableView respondsToSelector:@selector(layoutMargins)]) {
            _tableView.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        if ([_tableView respondsToSelector:@selector(separatorInset)]) {
            _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        _tableView.backgroundColor = [UIColor skinViewScreenColor];
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.bottom.trailing.equalTo(self.view);
            make.top.equalTo(self.topTilteView.mas_bottom);
        }];
        _tableView.titleForEmpty = @"没有数据";
        #if __IPHONE_OS_VERSION_MAX_ALLOWED >= 130000
        if (@available(iOS 13.0, *)) {
            _tableView.separatorColor = [UIColor systemGray3Color];
        }
        #endif
    }
    return _tableView;
}

- (Bet365RecordTopTitleView *)topTilteView
{
    if (_topTilteView == nil) {
        _topTilteView = [[Bet365RecordTopTitleView alloc] initWithTitles:@[@"游戏",@"下注金额",@"输赢金额",@"有效投注"] titleColor:[UIColor whiteColor] backgroundColor:[UIColor blackColor]];
        [self.view addSubview:_topTilteView];
        [_topTilteView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(self.view);
            make.height.mas_equalTo(40);
            make.top.equalTo(self.searchTopView.mas_bottom);
        }];
    }
    return _topTilteView;
}

- (LukeSearchTopView *)searchTopView
{
    if (_searchTopView == nil) {
        _searchTopView = [[LukeSearchTopView alloc] initWithFrame:CGRectZero firstDateStr:[NSDate getNewTimeFormat:@"yyyy/MM/dd"] secondDateStr:[NSDate getNewTimeFormat:@"yyyy/MM/dd"] title:@"查询"];
        _searchTopView.delegate = self;
        [self.view addSubview:_searchTopView];
        [_searchTopView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(self.view);
            make.height.mas_equalTo(40);
            make.top.equalTo(self.view);
        }];
    }
    return _searchTopView;
}

@end


