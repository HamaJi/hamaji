//
//  LukeTeamCountViewController.h
//  Bet365
//
//  Created by luke on 17/6/27.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetWorkMannager+Account.h"

@interface LukeTeamCountViewController : Bet365ViewController
/**  */
@property (nonatomic,assign) PersonTeamRwsType rwsType;
/**  */
@property (nonatomic,strong) NSNumber *userId;

@end
