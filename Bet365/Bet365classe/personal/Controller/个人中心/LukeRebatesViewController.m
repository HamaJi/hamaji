//
//  LukeRebatesViewController.m
//  Bet365
//
//  Created by luke on 17/6/27.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import "LukeRebatesViewController.h"
#import "LukeRegisterLbTFView.h"
#import "LukePromotionView.h"
#import "LukeSpreadItem.h"
#import "LukeSpreadCell.h"
#import "LukeRebatesDescViewController.h"
#import "LukeRebateQRcodeViewController.h"
#import "NetWorkMannager+Account.h"
#import "SportsModuleShare.h"
#import "LukeSpreadInfoItem.h"
#import "UIViewControllerSerializing.h"
#import "HMSegmentedControl+Bet365.h"

static NSString * const LukeSpreadCellID = @"LukeSpreadCell";

@interface LukeRebatesViewController ()
<UITableViewDelegate,
UITableViewDataSource,
LukeSpreadCellDelegate,
LukePromotionViewDelegate,
RebatesDesControllerDelegate,
UIViewControllerSerializing>
/** 推广链接 */
@property (nonatomic,strong) LukePromotionView *promotionView;
/**  */
@property (nonatomic,strong) UITableView *tableView;
/** 网络请求的数据数组 */
@property (nonatomic,strong) NSMutableArray *spreadDatas;
/** 页数 */
@property (nonatomic,assign) NSInteger page;
/**  */
@property (nonatomic,weak) HMSegmentedControl *segmentControl;
/**  */
@property (nonatomic,assign) BOOL isSetFresh;

@end


RouterKey *const kRouterAgentSpread = @"agent/spread";

@implementation LukeRebatesViewController

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    return [USER_DATA_MANAGER shouldPush2Login];
}

+(void)load{
    
    [self registerJumpRouterKey:kRouterAgentSpread toHandle:^(NSDictionary *parameters) {
        LukeRebatesViewController *vc = [[LukeRebatesViewController alloc] init];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }];
    
}
#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.spreadDatas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LukeSpreadCell *cell = [tableView dequeueReusableCellWithIdentifier:LukeSpreadCellID];
    cell.item = self.spreadDatas[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;
    return cell;
}

#pragma mark - LukeSpreadCellDelegate
- (void)LukeSpreadCellSelectButtonWithItem:(LukeSpreadItem *)item index:(NSInteger)index
{
    switch (index) {
        case 0:
        {
            NSString *mainUrl = nil;
            if (BET_CONFIG.config.app_spread_domain.length > 0) {
                mainUrl = BET_CONFIG.config.app_spread_domain;
            }else if (item.externalUrl.length > 0) {
                mainUrl =  item.externalUrl;
            }else{
                mainUrl = @"";
            }

            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string = [NSString stringWithFormat:@"%@/%ld",mainUrl,item.SpreaID];
            [SVProgressHUD showSuccessWithStatus:@"复制成功！"];
        }
            break;
        case 1:
        {
            NSString *mainUrl = nil;
            if (BET_CONFIG.config.app_spread_domain.length > 0) {
                mainUrl = BET_CONFIG.config.app_spread_domain;
            }else if (item.externalUrl.length > 0) {
                mainUrl =  item.externalUrl;
            }else{
                mainUrl = @"";
            }
            LukeRebateQRcodeViewController *qrcodeVC = [[LukeRebateQRcodeViewController alloc] init];
            qrcodeVC.url = [NSString stringWithFormat:@"%@/%ld",mainUrl,item.SpreaID];
            [self presentViewController:qrcodeVC animated:YES completion:nil];
        }
            break;
        case 2:
        {
            LukeRebatesDescViewController *descVC = [[LukeRebatesDescViewController alloc] init];
            descVC.delegate = self;
            descVC.item = item;
            [self presentViewController:descVC animated:YES completion:^{
                
            }];
        }
            break;
        case 3:
        {
            [self alertWithTitle:@"确定删除吗？" message:@"删除操作将不可恢复，不影响已经注册的会员" cancelActionTitle:@"取消" cancelHandler:nil sureActionTitle:@"确定" sureHandler:^(UIAlertAction *action) {
                [NET_DATA_MANAGER requestPostRemoveSpreadInfoWithId:item.SpreaID Success:^(id responseObject) {
                    [SVProgressHUD showSuccessWithStatus:@"删除成功"];
                    self.page = 1;
                    [self.tableView.mj_header beginRefreshing];
                } failure:^(NSError *error) {
                    
                }];
            }];
        }
            break;
        default:
            break;
    }
}

#pragma mark - RebatesDesControllerDelegate
- (void)updateSpreadSuccess
{
    self.page = 1;
    [self.tableView.mj_header beginRefreshing];
}

#pragma mark - LukePromotionViewDelegate
- (void)reloadLukeRebatesViewController
{
    self.segmentControl.selectedSegmentIndex = 1;
    [self segmentControlClick:self.segmentControl];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.translucent = YES;
}

- (void)setupUI
{
    HMSegmentedControl *segmentControl = [HMSegmentedControl createSegmentWithBackgroundColor:[UIColor skinViewBgColor]
                                                                               IndicatorColor:[UIColor skinViewKitSelColor]
                                                                           selectedTitleColor:[UIColor skinViewKitSelColor]
                                                                                   titleColor:[UIColor skinViewKitDisColor]
                                                                                    titleArrs:@[@"推广链接",@"推广管理"]
                                                                                    addTarget:self action:@selector(segmentControlClick:)];
    [self.view addSubview:segmentControl];
    [segmentControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.top.trailing.equalTo(self.view);
        make.height.mas_equalTo(40);
    }];
    self.segmentControl = segmentControl;
    [self promotionView];
}

- (void)segmentControlClick:(HMSegmentedControl *)segment
{
    if (segment.selectedSegmentIndex == 0) {
        self.promotionView.hidden = NO;
        _tableView.hidden = YES;
    }else{
        self.promotionView.hidden = YES;
        if (!self.isSetFresh) {
            self.isSetFresh = YES;
            [self setupRefresh];
        }else{
            self.tableView.hidden = NO;
            self.page = 1;
            [self.tableView.mj_header beginRefreshing];
        }
    }
}

- (void)setupRefresh
{
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self queryData];
    }];
    self.tableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        self.page ++;
        [self queryData];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)queryData
{
    [NET_DATA_MANAGER requestGetSpreadInfoWithPage:self.page rows:20 Success:^(id responseObject) {
        if (self.page == 1) {
            [self.spreadDatas removeAllObjects];
        }
        if (responseObject[@"data"]) {
            [self.spreadDatas addObjectsFromArray:[LukeSpreadItem mj_objectArrayWithKeyValuesArray:responseObject[@"data"]]];
            [self.tableView reloadData];
        }
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        self.tableView.isShowEmpty = YES;
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        self.tableView.isShowEmpty = YES;
    }];
}

#pragma mark - 懒加载
- (LukePromotionView *)promotionView
{
    if (_promotionView == nil) {
        _promotionView = [[LukePromotionView alloc] init];
        _promotionView.delegete = self;
        _promotionView.backgroundColor = [UIColor skinViewScreenColor];
        [self.view addSubview:_promotionView];
        [_promotionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.segmentControl.mas_bottom);
            make.leading.trailing.bottom.equalTo(self.view);
        }];
    }
    return _promotionView;
}
- (NSMutableArray *)spreadDatas
{
    if (_spreadDatas == nil) {
        _spreadDatas = [NSMutableArray array];
    }
    return _spreadDatas;
}
- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] init];
        _tableView.backgroundColor = [UIColor skinViewScreenColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        if ([_tableView respondsToSelector:@selector(layoutMargins)]) {
            _tableView.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        if ([_tableView respondsToSelector:@selector(separatorInset)]) {
            _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        [_tableView registerClass:[LukeSpreadCell class] forCellReuseIdentifier:LukeSpreadCellID];
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedRowHeight = 160;
        _tableView.estimatedSectionFooterHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.tableFooterView = [UIView new];
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.segmentControl.mas_bottom);
            make.leading.trailing.bottom.equalTo(self.view);
        }];
        _tableView.titleForEmpty = @"没有数据";
    }
    return _tableView;
}

@end
