//
//  YHApplicationViewController.m
//  Bet365
//
//  Created by 蛤蟆吉 on 2019/11/28.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "YHApplicationViewController.h"
#import "YHApplicationCollectionViewCell.h"
#import "PrmtLobbyModel.h"
#import "NetWorkMannager+Prmt.h"
#import "YHApplicationPopViewController.h"


@interface YHApplicationViewController ()
<UICollectionViewDelegate,
UICollectionViewDataSource,
UIViewControllerSerializing,
UIViewControllerTransitioningDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic,strong) PrmtLobbyModel *model;

@end

@implementation YHApplicationViewController

RouterKey *const kRouterYHPrmt = @"yhApplication";

RouterKey *const kRouterActivityHall = @"activity/activity_hall";

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    if (USER_DATA_MANAGER.isLogin) {
        if ([USER_DATA_MANAGER.userInfoData.type isEqualToString:@"TEST"]) {
            [SVProgressHUD showErrorWithStatus:@"试玩账号无法使用【优惠申请】，请登录平台账号"];
            return NO;
        }
    }
    return [USER_DATA_MANAGER shouldPush2Login];
}

+(void)load{
    
    [self registerJumpRouterKey:kRouterYHPrmt toHandle:^(NSDictionary *parameters) {
        YHApplicationViewController *vc = [[YHApplicationViewController alloc] initWithNibName:@"YHApplicationViewController" bundle:nil];
        vc.hidesBottomBarWhenPushed = YES;
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
    }];
    
    [self registerJumpRouterKey:kRouterActivityHall toHandle:^(NSDictionary *parameters) {
        YHApplicationViewController *vc = [[YHApplicationViewController alloc] initWithNibName:@"YHApplicationViewController" bundle:nil];
        vc.hidesBottomBarWhenPushed = YES;
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
    }];
    
    [self registerInstanceRouterKey:kRouterYHPrmt toHandle:^UIViewController *(NSDictionary *parameters) {
        YHApplicationViewController *vc = [[YHApplicationViewController alloc] initWithNibName:@"YHApplicationViewController" bundle:nil];
        return vc;
    }];
    
    [self registerInstanceRouterKey:kRouterActivityHall toHandle:^UIViewController *(NSDictionary *parameters) {
        YHApplicationViewController *vc = [[YHApplicationViewController alloc] initWithNibName:@"YHApplicationViewController" bundle:nil];
        return vc;
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    [self.collectionView registerNib:[UINib nibWithNibName:@"YHApplicationCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YHApplicationCollectionViewCell"];
    [self getData];
    
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return  1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.model.yhApplication.count;
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}
- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    
    return 10;
}
- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSUInteger count = 2;
    CGFloat width = (self.collectionView.ycz_width - 10 * (count + 1)) / (count * 1.0);
    return CGSizeMake(width, width * 0.75);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    YHApplicationCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YHApplicationCollectionViewCell" forIndexPath:indexPath];
    PrmtYHApplication *model = [self.model.yhApplication safeObjectAtIndex:indexPath.item];
    cell.model = model;
    return cell;
}

#pragma mark - UICollectionViewDelegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    PrmtYHApplication *model = [self.model.yhApplication safeObjectAtIndex:indexPath.item];
    YHApplicationPopViewController *vc = [[YHApplicationPopViewController alloc] initWithNibName:@"YHApplicationPopViewController" bundle:nil];
    vc.transitioningDelegate = self;
    vc.modalPresentationStyle = UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    vc.model = model;
}

#pragma mark - UIViewControllerTransitioningDelegate
- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source{
    return [PresentingTopAnimator new];
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    return [DismissingTopAnimator new];
}

#pragma mark - Request
-(void)getData{
    void(^handle)(id object) = ^(id object){
        NSError *error;
        self.model = [MTLJSONAdapter modelOfClass:PrmtLobbyModel.class fromJSONDictionary:object error:&error];
        [self.collectionView reloadData];
    };
    [NET_DATA_MANAGER requestPrmtLobbyCache:^(id responseCache) {
        handle(responseCache);
    } Success:^(id responseObject) {
        handle(responseObject);
    } failure:^(NSError *error) {
        
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
