//
//  Bet365GrowthController.m
//  Bet365
//
//  Created by luke on 2019/11/4.
//  Copyright © 2019 jesse. All rights reserved.
//

#import "Bet365GrowthController.h"
#import "LukeSearchTopView.h"
#import "NetWorkMannager+activity.h"
#import "GrowthTypeModel.h"
#import "GrowthTypeCell.h"

@interface Bet365GrowthController ()
<LukeSearchTopViewDelegate,
UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
/** 顶部时间选择 */
@property (nonatomic,strong) LukeSearchTopView *searchTopView;
/** 起始时间 */
@property (nonatomic,copy) NSString *beginDateStr;
/** 结束时间 */
@property (nonatomic,copy) NSString *endDateStr;
@property (nonatomic,strong) GrowthTypeModel *model;
@property (nonatomic,assign) NSInteger page;
@property (nonatomic,strong) NSMutableArray *datas;

@end

RouterKey *const kRouterGrowthValueDetailed = @"growthValueDetailed";

@implementation Bet365GrowthController

+(BOOL)beAllowedPushViewConrtoller:(NSDictionary *)parameters{
    return [USER_DATA_MANAGER shouldPush2Login];
}

+(void)load{
    
    [self registerJumpRouterKey:kRouterGrowthValueDetailed toHandle:^(NSDictionary *parameters) {
        Bet365GrowthController *vc = [[Bet365GrowthController alloc] init];
        [[NAVI_MANAGER getCurrentVC].navigationController pushViewController:vc animated:YES];
        vc.hidesBottomBarWhenPushed = YES;
    }];
    
    [self registerInstanceRouterKey:kRouterGrowthValueDetailed toHandle:^UIViewController *(NSDictionary *parameters) {
        Bet365GrowthController *vc = [[Bet365GrowthController alloc] init];
        return vc;
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.beginDateStr = [[NSDate dateUTC] stringWithFormat:[NSDate ymdFormat]];
    self.endDateStr = self.beginDateStr;
    [self searchTopView];
    self.tableView.tableFooterView = [UIView new];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.view.backgroundColor = [UIColor skinViewScreenColor];
    self.tableView.backgroundColor = [UIColor skinViewScreenColor];
    [self.tableView registerNib:[UINib nibWithNibName:@"GrowthTypeCell" bundle:nil] forCellReuseIdentifier:@"GrowthTypeCell"];
    [self setupRefresh];
}

- (void)setupRefresh
{
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self queryData];
    }];
    self.tableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        self.page ++;
        [self queryData];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)queryData
{
    self.tableView.isShowEmpty = YES;
//    [SVProgressHUD showWithStatus:@"加载中..."];
    [NET_DATA_MANAGER requestGetGrowthValueDetailedWithPage:self.page rows:8 startDate:self.beginDateStr endDate:self.endDateStr success:^(id responseObject) {
//        [SVProgressHUD dismiss];
        if (self.page == 1) {
            [self.datas removeAllObjects];
        }
        [self.datas addObjectsFromArray:[GrowthTypeListModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]]];
        [self.tableView reloadData];
        self.tableView.titleForEmpty = @"暂无信息";
        self.tableView.isShowEmpty = NO;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    } failure:^(NSError *error) {
//        [SVProgressHUD dismiss];
        NSLog(@"%@",error);
        self.tableView.isShowEmpty = NO;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.datas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GrowthTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GrowthTypeCell"];
    cell.model = self.datas[indexPath.row];
    return cell;
}

#pragma mark - LukeSearchTopViewDelegate
- (void)SearchTopViewSearchButtonClick
{
    [self queryData];
}

- (void)SearchTopViewfirstDateButtonClick
{
    @weakify(self);
    [self showDatePickerdefaultSelValue:nil minDate:[[NSDate dateUTC] dateByAddingDays:-30] resultBlock:^(NSString *selectValue) {
        @strongify(self);
        self.beginDateStr = selectValue;
        [self.searchTopView.firstDateBtn setTitle:selectValue forState:UIControlStateNormal];
        [self queryData];
    }];
}

- (void)SearchTopViewsecondDateButtonClick
{
    @weakify(self);
    [self showDatePickerdefaultSelValue:nil minDate:[[NSDate dateUTC] dateByAddingDays:-30] resultBlock:^(NSString *selectValue) {
        @strongify(self);
        self.endDateStr = selectValue;
        [self.searchTopView.secondDateBtn setTitle:selectValue forState:UIControlStateNormal];
        [self queryData];
    }];
}

#pragma mark - SET/GET
- (LukeSearchTopView *)searchTopView
{
    if (_searchTopView == nil) {
        _searchTopView = [[LukeSearchTopView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40) firstDateStr:self.beginDateStr secondDateStr:self.endDateStr title:@"查询"];
        _searchTopView.delegate = self;
        [self.topView addSubview:_searchTopView];
        [_searchTopView borderForColor:JesseGrayColor(200) borderWidth:1 borderType:UIBorderSideTypeBottom];
    }
    return _searchTopView;
}

- (NSMutableArray *)datas
{
    if (!_datas) {
        _datas = [NSMutableArray array];
    }
    return _datas;
}

@end
