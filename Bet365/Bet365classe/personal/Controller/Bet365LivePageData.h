//
//  Bet365LivePageData.h
//  Bet365
//
//  Created by luke on 2018/6/23.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>

#define Bet365LivePageIntance [Bet365LivePageData shareIntance]

@interface Bet365LivePageData : NSObject

+ (instancetype)shareIntance;

- (void)queryDataWithLiveCode:(NSString *)liveCode Completed:(void(^)())completed;

/**  */
@property (nonatomic,strong) NSArray *data;

@end
