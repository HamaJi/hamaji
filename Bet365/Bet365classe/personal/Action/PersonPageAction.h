//
//  PersonPageAction.h
//  Bet365
//
//  Created by HHH on 2018/7/14.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <VGParallaxHeader/UIScrollView+VGParallaxHeader.h>
#import "PersonPageHeaderView.h"
typedef NS_ENUM(NSInteger, PersonPageTableViewSection) {
    PersonPageTableViewSection_TOOL = 0,            //常用工具
    PersonPageTableViewSection_INFODATA,            //个人信息
    PPersonPageTableViewSection_DELIVER,            //投注记录
    PPersonPageTableViewSection_DELEGATE,           //代理中心
    PPersonPageTableViewSection_ACTIVITY,           //优惠活动
};
@class PersonPageAction;

@protocol PersonPageActionDelegate <NSObject>
@optional
-(void)PersonPageAction:(PersonPageAction *)action navigationBarAlpha:(CGFloat)alpha;

-(void)PersonPageAction:(PersonPageAction *)action headBgViewHeight:(CGFloat)scale;
@required
-(void)PersonPageAction:(PersonPageAction *)action TapItem:(CenterMenuItemTemplate *)item;

@end

@interface PersonPageAction : NSObject

@property (nonatomic,weak)id<PersonPageActionDelegate> delegate;

@property (nonatomic,weak) PersonPageHeaderView *headView;

@property (nonatomic,weak)UITableView *tableView;


-(void)per_viewWillAppear;
@end
