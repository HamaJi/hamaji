//
//  PersonPageAction.m
//  Bet365
//
//  Created by HHH on 2018/7/14.
//  Copyright © 2018年 jesse. All rights reserved.
//

#import "PersonPageAction.h"
#import "PersonPageCell.h"
#import "PersonPageSectionView.h"
#import "PersonPageViewController.h"
#import "UserSignInEntity+CoreDataProperties.h"
#define  PersonPageItemSpacing 0.0f
@interface PersonPageAction()

<UITableViewDelegate,
UITableViewDataSource,
UIScrollViewDelegate,
PersonPageCellDelegate,
PersonHeaderViewDelegate
>

@property (nonatomic,strong)UserCenterTemplateModel *userCenterMenu;

@property (nonatomic,strong)GrowthConfigModel *growthConfig;

@property (nonatomic,strong)GrowthLevelModel *growthLevelConfig;
@end

@implementation PersonPageAction

-(instancetype)init{
    if (self = [super init]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self addObserver];
        });
    }
    return self;
}

#pragma mark - Public
-(void)per_viewWillAppear{    
//    [self scrollViewDidScroll:self.collectionView];
    if (USER_DATA_MANAGER.isLogin) {
        [USER_DATA_MANAGER requestGetDeliverUnreadCount:nil];
        
        [USER_DATA_MANAGER requestGetPursueUnreadCount:nil];
        
        [USER_DATA_MANAGER requestUserStatusCompleted:nil];
        
        [USER_DATA_MANAGER requestGetNoticeList:nil];
        
        [USER_DATA_MANAGER requestGetUserInfoCompleted:nil];
        
        NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"account = %@",USER_DATA_MANAGER.userInfoData.account];
        NSArray *list = [[UserSignInEntity MR_findAllWithPredicate:predicate inContext:context] sortedArrayUsingComparator:^NSComparisonResult(UserSignInEntity * _Nonnull obj1, UserSignInEntity * _Nonnull obj2) {
            if ([obj1.signinTime timeIntervalSince1970] > [obj2.signinTime timeIntervalSince1970]) {
                return NSOrderedDescending;
            }else{
                return NSOrderedAscending;
            }
        }];
        UserSignInEntity *entity = [list lastObject];
        if ([entity.signinTime isToday]) {
            self.headView.hasSignIn = YES;
        }else{
            self.headView.hasSignIn = NO;
        }
    }
}

#pragma mark - Private
-(void)addObserver{
    @weakify(self);
    
    [[RACSignal merge:@[BET_CONFIG.updateSubject,
                        RACObserve(USER_DATA_MANAGER.userInfoData,isDl),
                        RACObserve(BET_CONFIG.common_config,isDP)]] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        [self updateData];
    }];
    
//    [[RACObserve(USER_DATA_MANAGER,isLogin) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
//        if ([x boolValue]) {
//
//
//        }
//    }];
    
    [[RACObserve(USER_DATA_MANAGER.userInfoData, growthLevel) distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        if (x) {
            NSUInteger index = [x integerValue];
            self.growthLevelConfig = [BET_CONFIG.growthLevelList safeObjectAtIndex:index];
            self.headView.level = [self.growthLevelConfig.level integerValue];
            
        }
    }];
    
    [[RACSignal combineLatest:@[[RACObserve(USER_DATA_MANAGER.userInfoData, growthValue) distinctUntilChanged],
                                [RACObserve(self, growthLevelConfig) distinctUntilChanged]] reduce:^id (NSNumber *growthValue,GrowthLevelModel *levelModel){
                                    @strongify(self);
                                    if (growthValue && levelModel) {
                                        return @(YES);
                                    }
                                    return @(NO);
                                }] subscribeNext:^(id  _Nullable x) {
                                    @strongify(self);
                                    if ([x boolValue]) {
                                        NSUInteger index = [self.growthLevelConfig.level integerValue];
                                        NSArray <GrowthLevelModel *>*previousList = [BET_CONFIG.growthLevelList safeSubarrayWithRange:NSMakeRange(0, index)];
                                        __block NSUInteger totalGrowth = [USER_DATA_MANAGER.userInfoData.growthValue integerValue];
                                        [previousList enumerateObjectsUsingBlock:^(GrowthLevelModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                            totalGrowth += [obj.growth integerValue];
                                        }];
                                        self.headView.totalValue = totalGrowth;
                                        self.headView.currentValue = [USER_DATA_MANAGER.userInfoData.growthValue integerValue];
                                        self.headView.totalValueInLevel = [self.growthLevelConfig.growth integerValue];
                                    }
                                }];
    
    RAC(self,growthConfig) = RACObserve(BET_CONFIG, growthConfigModel);
    
}

-(void)updateData{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.userCenterMenu = BET_CONFIG.userCenterMenu;
        [self.tableView reloadData];
    });
}

#pragma mark - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return menuTemplateList(self.userCenterMenu).count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return [PersonPageSectionView getHeight];
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 6;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [PersonPageCell getCellHeightByItemsCount:menuTemplateList(self.userCenterMenu)[indexPath.section].menuList.count];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    PersonPageSectionView *head = [tableView dequeueReusableHeaderFooterViewWithIdentifier:[PersonPageSectionView identifier]];
    if (!head) {
        head = [[PersonPageSectionView alloc] initWithReuseIdentifier:[PersonPageSectionView identifier]];
    }
    head.backgroundColor = [UIColor clearColor];
    head.tittile = menuTemplateList(self.userCenterMenu)[section].titile;
    return head;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identifier = [PersonPageCell getCellIdentifierByIndex:indexPath.section];
    PersonPageCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        [_tableView registerNib:[UINib nibWithNibName:@"PersonPageCell" bundle:nil] forCellReuseIdentifier:identifier];
        cell = [[NSBundle mainBundle] loadNibNamed:@"PersonPageCell" owner:self options:nil].firstObject;
        [cell setValue:identifier forKey:@"reuseIdentifier"];
        cell.delegate = self;
    }
    cell.sectionMenu = menuTemplateList(self.userCenterMenu)[indexPath.section];
    return cell;
}

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.tableView shouldPositionParallaxHeader];
    
    if (!self.delegate)
        return;
    CGFloat y = scrollView.contentOffset.y;
    if ([self.delegate respondsToSelector:@selector(PersonPageAction:navigationBarAlpha:)]) {

        if (y >= 0) {
            float alpha = MIN(1.0, y/30);
            [self.delegate PersonPageAction:self navigationBarAlpha:alpha];
        }
        else {
            [self.delegate PersonPageAction:self navigationBarAlpha:0];
        }
    }
    if ([self.delegate respondsToSelector:@selector(PersonPageAction:headBgViewHeight:)]){
        if (y > 123) {
            y = 123;
        }else if(y < 0){
            y = y * 0.75;
        }
        [self.delegate PersonPageAction:self headBgViewHeight:y];
    }
    
}

#pragma mark - PersonPageCellDelegate
-(void)PersonPageCell:(PersonPageCell *)cell didTapItem:(CenterMenuItemTemplate *)itemTemplate{
    if (self.delegate && [self.delegate respondsToSelector:@selector(PersonPageAction:TapItem:)]) {
        [self.delegate PersonPageAction:self TapItem:itemTemplate];
    }
}
#pragma mark - PersonHeaderViewDelegate
-(void)PersonPageHeaderView:(PersonPageHeaderView *)headView didTapHeaderAction:(PersonPageHeaderViewAction)actionType{
    switch (actionType) {
        case PersonPageHeaderViewAction_RECHARGE:
            
            [UIViewController routerJumpToUrl:kRouterRecharge];
            break;
        case PersonPageHeaderViewAction_DRAW:
            [UIViewController routerJumpToUrl:kRouterDraw];
            break;
        case PersonPageHeaderViewAction_GROWRULE:
        {
            NSInteger growth = [self.growthConfig.checkInOneDayGrowth integerValue];
            
            NSString *rule1 = [NSString stringWithFormat:@"1.每天签到累加%li点成长值;",growth];
            NSString *rule1sub = [NSString stringWithFormat:@"  如:第一天签到加%li点成长值，第二天加%li点成长值，第三天加%li点成长值，以此类推，中间断签，重新从%li点开始计算;",growth,growth*2,growth*3,growth];
            NSString *rule2 = [NSString stringWithFormat:@"2.签到每日上限是%li点成长值",[self.growthConfig.checkInMaxGrowth integerValue]];
            
            NSMutableAttributedString *attribute = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@\n%@",rule1,rule1sub,rule2]].mutableCopy;
            
            [attribute addColor:COLOR_WITH_HEX(0x000000) substring:rule1];
            [attribute addColor:[UIColor darkGrayColor] substring:rule1sub];
            [attribute addColor:COLOR_WITH_HEX(0x000000) substring:rule2];
            
            [attribute addFont:[UIFont systemFontOfSize:16] substring:rule1];
            [attribute addFont:[UIFont systemFontOfSize:14] substring:rule1sub];
            [attribute addFont:[UIFont systemFontOfSize:16] substring:rule2];
            
            [Bet365AlertSheet showChooseAlert:@"签到说明" AttributeMessage:attribute Items:@[@"确定"] Handler:^(NSInteger index) {
                
            }];
        }
            break;
            case PersonPageHeaderViewAction_GROWTYPE:
            [UIViewController routerJumpToUrl:kRouterGrowthValueDetailed];
            break;
        default:
            break;
    }
}

-(void)PersonPageHeaderView:(PersonPageHeaderView *)headView submitSigin:(BOOL)sigin{
    if (!sigin) {
        [USER_DATA_MANAGER requestSubmitSigin:^(BOOL success,NSUInteger addGrowth) {
            if (addGrowth) {
                NSUInteger currentValue = ([USER_DATA_MANAGER.userInfoData.growthValue integerValue] + addGrowth);
                if(currentValue >= [self.growthLevelConfig.growth integerValue]){
                    currentValue = currentValue - [self.growthLevelConfig.growth integerValue];
                    USER_DATA_MANAGER.userInfoData.growthLevel = [NSNumber numberWithInteger:([USER_DATA_MANAGER.userInfoData.growthLevel integerValue] + 1)];
                }
                USER_DATA_MANAGER.userInfoData.growthValue = [NSNumber numberWithInteger:currentValue];
            }
            self.headView.hasSignIn = success;
//            [USER_DATA_MANAGER requestGetUserInfoCompleted:nil];
        }];
    }else{
        [Bet365AlertSheet showChooseAlert:@"今日已签到，明天再来" Message:nil Items:@[@"确定"] Handler:nil];
    }
}
#pragma mark - Private


#pragma mark - GET/SET
-(void)setGrowthConfig:(GrowthConfigModel *)growthConfig{
    _growthConfig = growthConfig;
    dispatch_async(dispatch_get_main_queue(), ^{
        /** 强制关闭 签到 **/
//        {
//            [self.tableView setParallaxHeaderView:self.headView mode:VGParallaxHeaderModeFill height:110];
//        }
        /** 开启签到 **/
        {
            self.headView.isShowGrowthLevel = [_growthConfig.isShowGrowthLevel boolValue];
            if ([_growthConfig.isShowGrowthLevel boolValue]) {
                [self.tableView setParallaxHeaderView:self.headView mode:VGParallaxHeaderModeFill height:(170.0 / (375.0 / WIDTH) + 80)];
            }else{
                [self.tableView setParallaxHeaderView:self.headView mode:VGParallaxHeaderModeFill height:110];
            }
        }
        
    });
    
}
-(void)setTableView:(UITableView *)tableView{
    _tableView = tableView;
    _tableView.delegate = self;
    _tableView.dataSource = self;
}

-(void)setHeadView:(PersonPageHeaderView *)headView{
    _headView = headView;
    _headView.delegate = self;
}

NSArray <CenterMenuSectionTemplate *>*menuTemplateList(UserCenterTemplateModel *menuTemplate){
    NSMutableArray *list = [NSMutableArray arrayWithCapacity:2];
    [list addObjectsFromArray:menuTemplate.centerMenuList];
//    if (!BET_CONFIG.common_config.isDP) {
//        NSMutableArray *centerMenulist = [NSMutableArray arrayWithArray:menuTemplate.centerMenuList];
//        CenterMenuSectionTemplate *recordMenu = [centerMenulist safeObjectAtIndex:1];
//        NSMutableArray *dpArr = [NSMutableArray arrayWithArray:recordMenu.menuList];
//        if (dpArr.count > 2) {
//            [dpArr removeObjectsInRange:NSMakeRange(2, 5)];
//            recordMenu.menuList = dpArr;
//            [centerMenulist replaceObjectAtIndex:1 withObject:recordMenu];
//            [list addObjectsFromArray:centerMenulist];
//        }else{
//            [list addObjectsFromArray:centerMenulist];
//        }
//    }else{
//        [list addObjectsFromArray:menuTemplate.centerMenuList];
//    }
    if (USER_DATA_MANAGER.userInfoData.isDl) {
        [list addObjectsFromArray:menuTemplate.centerDlMenuList];
    }
    return list;
}
@end

