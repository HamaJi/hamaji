
//
//  AppDelegate.h
//  Bet365
//
//  Created by jesse on 2017/4/12.
//  Copyright © 2017年 jesse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Bet365TabbarViewController.h"
#import "UserDataManager.h"
#import <AVOSCloud.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@property(nonatomic,strong)Bet365TabbarViewController *tabbar;
@property(nonatomic,assign)BOOL BettingSwitch;//下注开关
@property(nonatomic,assign)NSInteger CurrenofficalLocal;//当前官方玩法定位
@property(nonatomic,strong)NSArray *currenOfficalBettingArr;//当前官方投注存储
@property(nonatomic,copy)NSString *currenType;//当前特殊玩法ID
@property(strong,nonatomic)NSArray *setConfig;/**位置config**/
@property(assign,nonatomic)BOOL changeUnit;/**官方滑条是否滑动**/
@property(assign,nonatomic)BOOL isFinishLotteryQueue;/**是否完成路子长龙加载处理**/

-(void)setRootViewControllerWithAppIdLaunch:(NSString *)appID;

@end
