//
//  AppDelegate.m
//  Bet365
//
//  Created by jesse on 2017/4/12.
//  Copyright © 2017年 jesse. All rights reserved.
//
#import "AppDelegate.h"
#import "Bet365TabbarViewController.h"
#import "JESSEKFCPAlgorithmRequestManager.h"
#import "Bet365NavViewController.h"
#import "KFCPRequestViewController.h"
#import "jesseAvdioToolManager.h"

#import <JPUSHService.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "NetCloudSwitchTool.h"
#import "NSURLSession+Metrics.h"
#import <PINCache/PINCache.h>
#import "ChatUtility.h"
#import "CrashSec.h"
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#import <AdSupport/AdSupport.h>
#import "AjiRequestTaskMetricsTool.h"
#endif

#import <MMDrawerController/MMDrawerController.h>
#import "ChatGroupViewController.h"
#import <JSPatchPlatform/JSPatch.h>
#import "LaunchAppIDViewController.h"
#import <XHLaunchAd/XHLaunchAd.h>
#import <libOpenInstallSDK/OpenInstallSDK.h>


@interface AppDelegate ()<JPUSHRegisterDelegate,XHLaunchAdDelegate,OpenInstallDelegate>

@property(nonatomic,strong)JGProgressHUD *hud;

@property (nonatomic,assign)BOOL didBecomeActive;

@property (nonatomic,strong) LaunchAppIDViewController *lanchAppidVC;
    
/**
 主视图挂起
 */
@property (nonatomic,assign) BOOL suspendDutiesdRoot;

    
@end

@implementation AppDelegate

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    if  ([OpenInstallSDK handLinkURL:url]){
        return YES;
    }
    if (self.window) {
        
        
        
//        @"wmzxapp://startapp.com?custom_code=WCP&logo=&host1=&host2=&host3="
        NSMutableString *last = [url absoluteString].mutableCopy;
        
        NSArray * urlTypes = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath(@"Info")][@"CFBundleURLTypes"];
        
        __block BOOL isContain = NO;
        [urlTypes enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull schemesData, NSUInteger dataIdx, BOOL * _Nonnull dataStop) {
            [((NSArray *)schemesData[@"CFBundleURLSchemes"]) enumerateObjectsUsingBlock:^(id  _Nonnull schemes, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([last containsString:schemes]) {
                    [last deleteString:schemes];
                    if ([last containsString:@"://startapp.com?"]) {
                        [last deleteString:@"://startapp.com?"];
                        isContain = YES;
                        *dataStop = YES;
                        *stop = YES;
                    }
                }
            }];
        }];
        
        if (!isContain) {
            return YES;
        }
        
        NSArray *paramerslist = [last componentsSeparatedByString:@"&"];
        __block NSString *appID = nil;
        [paramerslist enumerateObjectsUsingBlock:^(NSString *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj containsString:@"custom_code"]) {
                appID = [obj substringWithRange:NSMakeRange(@"custom_code=".length, obj.length - @"custom_code=".length)];
            }
        }];
        if (BET_CONFIG.userSettingData.defaultAppID.length &&
            [BET_CONFIG.userSettingData.defaultAppID isEqualToString:[appID uppercaseString]]) {
            return YES;
        }
        
        [self setRootViewControllerWithAppIdLaunch:appID];
        
    }
    return YES;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(nonnull NSDictionary *)options{
    if  ([OpenInstallSDK handLinkURL:url]){
        return YES;
    }
    return YES;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray * _Nullable))restorationHandler{
    //判断是否通过OpenInstall Universal Link 唤起App
    if ([OpenInstallSDK continueUserActivity:userActivity]){//如果使用了Universal link ，此方法必写
        return YES;
    }
    //其他第三方回调；
    return YES;
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

   
#ifdef DEBUG

#else
    if (BET_CONFIG.appId.length) {
        BET_CONFIG.userSettingData.defaultAppID = BET_CONFIG.appId;
    }
#endif
    
    /** 崩溃日志收集*/
    installUncaughtExceptionHandler();
    
    /** Fabric**/
    [Fabric with:@[[Crashlytics class]]];

    if (![AUTHDATA_MANAGER.intr isNoBlankString]) {
        NSString *intr = [UIPasteboard generalPasteboard].string;
        if ([intr hasPrefix:@"intr="] && intr.length > 5) {
            AUTHDATA_MANAGER.intr = [intr substringFromIndex:5];
            [AUTHDATA_MANAGER save];
        }
    }
    
    if (BET_CONFIG.userSettingData.updateBuildVersion != BET_CONFIG.buildVersion) {
        BET_CONFIG.userSettingData.updateBuildVersion = BET_CONFIG.buildVersion;
        [[PINCache sharedCache] removeAllObjects:^(PINCache * _Nonnull cache) {
            
        }];
    }
    //热更新
    [JSPatch startWithAppKey:@"0a4d4a5c9e6c3653"];
    [JSPatch setupRSAPublicKey:@"-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDJuw71/bYgJSd/aRaI9RJGZssq\ncm35oNHqv55qTfVTuLj3jfvwZwn6kuKygc7dIDJ83XCmqXI1AAPs0zs5nb+vzrUB\nzm8ZDVBYBpA3k7nWvn/nNg319ZVdS3dDYsDIRPA69APKM8Yz31CixPGWVf7EKicT\najEpZBya/sBddh573wIDAQAB\n-----END PUBLIC KEY-----"];
//    [JSPatch showDebugView];
    
    /**通知权限（角标，声音，弹框）**/
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert) completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted) {
            //获取用户是否同意开启通知
        }
    }];
    
    /** MagicalRecord**/
    [MagicalRecord setLoggingLevel:MagicalRecordLoggingLevelWarn];
    [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:@"BetCoreData.sqlite"];
    
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    
    
    [OpenInstallSDK initWithDelegate:self];
    
    /** 网络**/
    [NET_DATA_MANAGER checkNetworkStatusWithBlock:^(PPNetworkStatus status) {
        if ((status == PPNetworkStatusReachableViaWWAN || status == PPNetworkStatusReachableViaWiFi) &&
            !NET_CLOUD_SWITCH.hasRequest) {
            [NET_CLOUD_SWITCH requestAllCloud:^(BOOL showTabbar) {
                [Bet365Tool getNetworkIPAddressCompleted:nil];
            }];
        }else{
            
        }
    }];
    
    /** 极光**/
    [self setJPush:launchOptions];
    
    /** 工具类*/
    [Bet365Tool shareInstance];
    
    
    
//    /** 聊天**/
//    [CHAT_UTIL getChatClienInfo:^(ChatInfoModel *info) {
//        
//    }];
    
    /** 聊天红包领取过期删除**/
    NSTimeInterval time = [[NSDate dateAfterDate:[NSDate date] month:-1] timeIntervalSince1970];
    NSArray <RedPackEntity *>*list = [RedPackEntity MR_findAllWithPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"timeStamp < %f",time]]];
    [list enumerateObjectsUsingBlock:^(RedPackEntity * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj MR_deleteEntity];
    }];
    
    /** 云端**/
    if (!self.suspendDutiesdRoot && BET_CONFIG.userSettingData.defaultAppID){
        [NET_CLOUD_SWITCH requestAllCloud:^(BOOL showTabbar) {
            [Bet365Tool getNetworkIPAddressCompleted:nil];
        }];
    }
    
    if (SerVer_Url.length && !self.suspendDutiesdRoot) {
        self.window.rootViewController = self.tabbar;
        [self showLaunchAd];
    }else{
        self.window.rootViewController = self.lanchAppidVC;
    }
    [self.window makeKeyAndVisible];
    
    
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        NSURL *url = [NSURL URLWithString:@"app1://jsonString"];
//
//        if ([[UIApplication sharedApplication] canOpenURL:url]) {
//
//
//            if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
//                //iOS 10.0+
//                [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:^(BOOL success) {
//                }];
//            }else{
//                //iOS 2~10
//                [[UIApplication sharedApplication] openURL:url];
//            }
//        }
//    });
    
    
    
//    //聊天使用
//    dispatch_async(dispatch_get_main_queue(), ^{
//        GT_DEBUG_INIT;
//        GT_LOG_START("file1");
//        GT_DEBUG_SET_HIDDEN(NO);
//        GT_TIME_SWITCH_SET(YES);
//        GT_OC_OUT_REGISTER(@"CHAT", @"LOG");
//        GT_OC_OUT_DEFAULT_ON_AC(@"App Smoothness", @"App CPU", @"CHAT");
//        GT_AC_SHOW;
//    });
    
//    NSMutableArray *arrShortcutItem = (NSMutableArray *)[UIApplication sharedApplication].shortcutItems;
//
//    UIApplicationShortcutItem *shoreItem1 = [[UIApplicationShortcutItem alloc] initWithType:@"co.erplus.search" localizedTitle:@"搜索" localizedSubtitle:nil icon:[UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeSearch] userInfo:nil];
//    [arrShortcutItem addObject:shoreItem1];
//
//    UIApplicationShortcutItem *shoreItem2 = [[UIApplicationShortcutItem alloc] initWithType:@"co.erplus.newTask" localizedTitle:@"新建任务" localizedSubtitle:@"" icon:[UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeCompose] userInfo:nil];
//    [arrShortcutItem addObject:shoreItem2];
//
//    [UIApplication sharedApplication].shortcutItems = arrShortcutItem;
    return YES;
}

- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler {
    NSLog(@"name:%@\ntype:%@", shortcutItem.localizedTitle, shortcutItem.type);
    if ([shortcutItem.type isEqualToString:@"com.cleancache.tg"]) {
        [Bet365Tool removeAllCacheOnCompletion:^{
            [CrashlyticsKit crash];
        }];
    }
    
    
}



/**注册JSPush**/
-(void)setJPush:(NSDictionary *)launch{
    JPUSHRegisterEntity *entity = [[JPUSHRegisterEntity alloc] init];
    entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound;
    
    [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];
    
    NSString *advertisingId = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    
    // Required
    // init Push
    // notice: 2.1.5版本的SDK新增的注册方法，改成可上报IDFA，如果没有使用IDFA直接传nil
    // 如需继续使用pushConfig.plist文件声明appKey等配置内容，请依旧使用[JPUSHService setupWithOption:launchOptions]方式初始化。
    
    [JPUSHService setupWithOption:launch appKey:BET_CONFIG.jPushKey
                          channel:@"App store"
                 apsForProduction:@"1"
            advertisingIdentifier:advertisingId];
    
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    //注册Devicetoken
    [JPUSHService registerDeviceToken:deviceToken];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(nonnull NSDictionary *)userInfo{
    
    [JPUSHService handleRemoteNotification:userInfo];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    /** iOS8+ 推送点击响应**/
    NSDictionary * userInfo = notification.userInfo;
    if ([[userInfo objectForKey:CHAT_NOTIFCATION_KEY] isEqualToString:CHAT_NOTIFCATION_IDENTIFIER]) {
        [UIViewController routerJumpToUrl:kRouterChatScrollBottom];
    }
    
}
/**极光回调**/
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler {
    // Required
    NSDictionary * userInfo = notification.request.content.userInfo;
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
    if (BET_CONFIG.userSettingData.hideChatMessageNotification){
        return;
    }

    NSInteger notificationStatus = 0;
    if (@available(iOS 10.0, *)) {
        if (self.didBecomeActive) {
            if (![[NAVI_MANAGER getCurrentVC] isKindOfClass:[ChatViewController class]]) {
                if (!BET_CONFIG.userSettingData.openChatMeessageNotifacationSound) {
                    notificationStatus = UNNotificationPresentationOptionAlert;
                }else{
                    notificationStatus = UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionSound;
                }
            }
        }else{
            if (!BET_CONFIG.userSettingData.openChatMeessageNotifacationSound) {
                notificationStatus = UNNotificationPresentationOptionAlert|
                UNNotificationPresentationOptionBadge;
            }else{
                notificationStatus = UNNotificationPresentationOptionAlert |
                UNNotificationPresentationOptionSound |
                UNNotificationPresentationOptionBadge;
            }
        }
    }
    completionHandler(notificationStatus);
}
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    /** iOS10+推送点击**/
    if ([response.notification.request.identifier isEqualToString:CHAT_NOTIFCATION_IDENTIFIER]) {
        [UIViewController routerJumpToUrl:kRouterChatScrollBottom];
    }
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
    completionHandler();  // 系统要求执行这个方法
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    // Required, iOS 7 Support
    [JPUSHService handleRemoteNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
}

/**调用第三方软件回调信息**/
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}

#pragma mark - OpenInstallDelegate
-(void)getWakeUpParams:(OpeninstallData *)appData{
    if (appData.data) {//(动态唤醒参数)
        //e.g.如免填邀请码建立邀请关系、自动加好友、自动进入某个群组或房间等
    }
    if (appData.channelCode) {//(通过渠道链接或二维码唤醒会返回渠道编号)
        //e.g.可自己统计渠道相关数据等
    }
//    [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"OpenInstallSDK:\n动态参数：%@;\n渠道编号：%@",appData.data,appData.channelCode]];
}

- (void)getInstallParamsFromOpenInstall:(nullable NSDictionary *)params withError:(nullable NSError *)error{
    if ([params containsObjectForKey:@"intr"]) {
        AUTHDATA_MANAGER.intr = [params objectForKey:@"intr"];
    }
    
}
- (void)getWakeUpParamsFromOpenInstall:(nullable NSDictionary *)params withError:(nullable NSError *)error{
    if ([params containsObjectForKey:@"intr"]) {
        AUTHDATA_MANAGER.intr = [params objectForKey:@"intr"];
    }
}

#pragma mark - AppIDLaunchViewcontroller
-(void)setRootViewControllerWithAppIdLaunch:(NSString *)appID{
    [self hideLaunchAd];
    [[NAVI_MANAGER getCurrentVC].navigationController popToRootViewControllerAnimated:NO];
    [[NAVI_MANAGER getCurrentVC] dismissViewControllerAnimated:YES completion:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.suspendDutiesdRoot = YES;
        self.window.rootViewController = self.lanchAppidVC;
        appID ? (self.lanchAppidVC.appID = appID) : nil;
        [self.window makeKeyAndVisible];
    });
}
#pragma mark - 启动页
-(void)showLaunchAd{
    
    [BET_CONFIG getLaunchAdMent:^(LaunchAd *launchAd) {
        if (launchAd.type) {
            NSString *urlString = [NSString stringWithFormat:@"%@%@",SerVer_Url,launchAd.image];
            if ([XHLaunchAd checkImageInCacheWithURL:[NSURL URLWithString:urlString]]) {
                XHLaunchImageAdConfiguration *imageAdconfiguration = [XHLaunchImageAdConfiguration new];
                if (launchAd.showTime > 0) {
                    imageAdconfiguration.duration = launchAd.showTime;
                }else{
                    imageAdconfiguration.duration = 5;
                }
                imageAdconfiguration.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
                imageAdconfiguration.imageNameOrURLString = urlString;
                imageAdconfiguration.imageOption = XHLaunchAdImageRefreshCached;
                imageAdconfiguration.contentMode = UIViewContentModeScaleAspectFill;
                
                //广告显示完成动画
                imageAdconfiguration.showFinishAnimate =ShowFinishAnimateFadein;
                //跳过按钮类型
                imageAdconfiguration.skipButtonType = SkipTypeTimeText;
                //后台返回时,是否显示广告
                imageAdconfiguration.showEnterForeground = NO;
                //显示图片开屏广告
                [XHLaunchAd imageAdWithImageAdConfiguration:imageAdconfiguration delegate:self];
            }
        }
    }];
    
}
-(void)hideLaunchAd{
    [XHLaunchAd removeAndAnimated:NO];
}

#pragma mark - XHLaunchAdDelegate
-(void)xhLaunchAd:(XHLaunchAd *)launchAd clickAndOpenModel:(id)openModel clickPoint:(CGPoint)clickPoint{
    [UIViewController routerJumpToUrl:BET_CONFIG.launchAd.href];
}
#pragma mark -- 发送崩溃日志
- (void)sendExceptionLogWithData:(NSData *)data path:(NSString *)path {
    
    NSString *parameterString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.requestSerializer.timeoutInterval = 5.0f;
        //告诉AFN，支持接受 text/xml 的数据
        [AFJSONResponseSerializer serializer].acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
        NSString *urlString = @"http://appadmin.yibofafa666.com/api/app/error/upload4Ios";
        [manager POST:urlString parameters:@{@"code":BET_CONFIG.boardCode,@"log":parameterString,@"buidlVersion":[NSString stringWithFormat:@"%li",BET_CONFIG.buildVersion]} progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSFileManager *fileManger = [NSFileManager defaultManager];
            [fileManger removeItemAtPath:path error:nil];
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            JesseLog(@"……………………………………………………(*^错误日志上传失败^*)——————————————");
        }];
        
    });
    
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    [application setApplicationIconBadgeNumber:0];
    [[UIApplication sharedApplication] endBackgroundTask:UIBackgroundTaskInvalid];//结束后台任务
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    self.didBecomeActive = NO;
    [application setApplicationIconBadgeNumber:0];
    /** 后台**/
    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:nil];//开启后台任务
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    self.didBecomeActive = YES;
    /** 前台**/
    if (!CHAT_UTIL.clientInfo) {
        return;
    }
    [MMPopupView hideAll];
    if (CHAT_UTIL.connectStatus < ChatUtilityConnectStatus_CONNECTING && [NET_DATA_MANAGER currentNetworkStatus]) {
        [CHAT_UTIL webSocketResetConnect];
    }
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [JSPatch sync];
}

//当app被关闭的时候调用
- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [MagicalRecord cleanUp];
    
}

#pragma mark - GET/SET
-(Bet365TabbarViewController *)tabbar{
    if (!_tabbar) {
        _tabbar = [[Bet365TabbarViewController alloc]init];
    }
    return _tabbar;
}

-(LaunchAppIDViewController *)lanchAppidVC{
    if (!_lanchAppidVC) {
        _lanchAppidVC = [[LaunchAppIDViewController alloc] initWithNibName:@"LaunchAppIDViewController" bundle:nil];
        @weakify(self);
        _lanchAppidVC.updateAppId = ^{
            @strongify(self);
            if (![self.window.rootViewController isEqual:self.tabbar]) {
                self.window.rootViewController = self.tabbar;
                [self.window makeKeyAndVisible];
            } 
        };
    }
    return _lanchAppidVC;
}
@end

