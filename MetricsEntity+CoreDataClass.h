//
//  MetricsEntity+CoreDataClass.h
//  Bet365
//
//  Created by 蛤蟆吉 on 2020/1/5.
//  Copyright © 2020 jesse. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface MetricsEntity : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "MetricsEntity+CoreDataProperties.h"
